<?php
$area["accounting"] = array("description" => "Accounting", "userrole" => "[accounting]");
$area["cc"] = array("description" => "Competence%20Center", "userrole" => "[cc]");
$area["cr"] = array("description" => "Control%20Reports", "userrole" => "[cr]");
$area["Digital_Center"] = array("description" => "Digital%20Center", "userrole" => "[dc]");
$area["management"] = array("description" => "Management", "userrole" => "[mgmt_report]");
$area["Marketing"] = array("description" => "Marketing", "userrole" => "[marketing]");
$area["me"] = array("description" => "ME%20Projects", "userrole" => "[t_d]");
$area["procurement"] = array("description" => "Procurement", "userrole" => "[procurement]");
$area["Project_Engineering"] = array("description" => "Product%20Engineering", "userrole" => "[pe]");
$area["superuser"] = array("description" => "Superuser", "userrole" => "[superuser]");
$area["supplier_quality"] = array("description" => "Supplier%20Quality", "userrole" => "[quality]");
$area["tng_report"] = array("description" => "TNG%20Report", "userrole" => "[tng_report]");
$area["transfer"] = array("description" => "Transfer%20Team", "userrole" => "[t_location]");
?>
