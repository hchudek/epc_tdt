<?php

function create_dig_rep() {
   require_once("../../../../library/tools/addin_xml.php");
   session_start();

   $get = ($_REQUEST["get"] == "") ? date("Y-m", microtime(true)) : $_REQUEST["get"];
   $data = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_cr_rms_dig_rep_selected_month?open&restricttocategory=".$get."&count=99999&function=plain");
   $row = explode(":", $data);
   $key = explode(";", $row[0]);

   if(trim($data) == "<h2>No documents found</h2>") {
      return "No data";

   }

   $data = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rdo.user?open&count=99999&function=plain");
   $rdo = json_decode("{".substr($data, 0, strrpos($data, ","))."}", true);
   unset($data);


   // CREATE ARRAY --------------------------------------------------------------------------------------------
   for($i = 1; $i <= count($row); $i++) {
      if($row[$i] != "") {
         $cell = explode(";", $row[$i]);
         for($e = 0; $e <= count($cell); $e++) {
            if(isset($key[$e])) $data[$i - 1][strtolower($key[$e])] = trim(rawurldecode($cell[$e]));
         }      
      }
   }


   // ADD DATE CALCULATION ------------------------------------------------------------------------------------
   foreach($data as $k => $v) {
      $d[0] = strtotime($v["date samples"]);
      $d[1] = strtotime($v["dim report date"]);
      $d[2] = strtotime($v["date dig rep"]);
      $data[$k]["calc"]["lower"] = date("Y-m-d", max($d));
      if($v["assessment"] != "") {
         $a = strtotime($v["assessment"]);
         $data[$k]["calc"]["upper"] = date("Y-m-d", $a);
         $data[$k]["calc"]["workingdays"] = calc_workingdays($data[$k]["calc"]["lower"], $data[$k]["calc"]["upper"]);
      }
   }


   // ADD RDO -------------------------------------------------------------------------------------------------
   foreach($data as $k => $v) {
     $data[$k]["calc"]["rdo"] = (isset($rdo[$v["cr resp. eng / rms rel."]])) ? $rdo[$v["cr resp. eng / rms rel."]] : "Unknown";
   }

   $i = 0;
   $table[0] = $key;
   array_push($table[0], "Workingdays", "RDO");

   foreach($data as $v) {
      $i++;
      if(isset($v["calc"]["workingdays"])) {
         foreach($key as $k) {
            $table[$i][] = $v[strtolower($k)];
         }
         array_push($table[$i], $v["calc"]["workingdays"], $v["calc"]["rdo"]);
      }
   }


   // CREATE RDO AVERAGE ---------------------------------------------------------------------------------------
   $i = 0;
   foreach($data as $v) {
      if(isset($v["calc"]["workingdays"])) {
         $i++;
         if(!isset($report[$v["calc"]["rdo"]])) $report[$v["calc"]["rdo"]] = array($v["calc"]["rdo"], 0, 0, 0);
         $report[$v["calc"]["rdo"]][1] += $v["calc"]["workingdays"];
         $report[$v["calc"]["rdo"]][2] += 1;
         $report[$v["calc"]["rdo"]][3] = round($report[$v["calc"]["rdo"]][1] / $report[$v["calc"]["rdo"]][2], 4);
      }
   }

   $table2[] = array("RDO", "Workingdays", "Transactions", "Average");  
   foreach($report as $k => $v) {
      $table2[] = $v;
   }

   return $report;


}




//$xls = "dig_report_responsible1.xlsx";
//require_once("../../../../zip/create.php");



function calc_workingdays($lower, $upper) {

   $lower = strtotime($lower);
   $upper = strtotime($upper);

   $d = 0; while($lower < $upper) {
      if(!in_array(date("N", $lower), array(6, 7))) $d++;
      $lower += 86400;
   }   

   return $d;
}


?>