<?php

$unique_id = array();
function generate_uniqueid($len) {
   global $unique_id;
   for($i = 0; $i < $len; $i++) $unique .= chr(rand(97, 122));
   if(in_array($unique, $unique_id)) generate_uniqueid($len);
   else $unique_id[] = $unique;
   return $unique;
}

function my_error_handler() {
   return true;
}

function evaluate_domino_at_function($function) {
   $r_from=array("\"", "\n");
   $r_to=array("\\\"","");
   $function=str_replace($r_from,$r_to,$function);
   $return=split("%SEP%", file_get_contents($_SESSION["remote_domino_path_main"]."/a.n.evaluate_at_function?open=php&eval=".$function));
   return $return;
}

function get_text($id) {
   global $_application;
   if($_REQUEST["show_id"] == "true") $ret .= "<div style=\"position:absolute;\"><div style=\"position:relative;top:-8px;left:10px;background-color:rgb(244,200,200);\"><nobr>".$id."</nobr></div></div>";
   $ret .= $_SESSION["language"][$id];
   return $ret;
}


function module_info($type, $basename) {
   if($type == "name") $res = str_replace(".php","",substr($basename,5,strlen($basename)));
   else $res = substr($basename,0,4);
   return $res;
}

function create_field($name, $value, $type, $onblur) {

   if(is_array($value)) $value = implode($value);

   $res = "";
   if($type == "text") {
      $onblur = ($onblur != "") ? " onblur=\"".$onblur."\"" : "";
      $res = "<input type=\"".$type."\" name=\"".$name."\" value=\"".$value."\"".$onblur." />";
   }
   return $res;
}


function create_field_pot($name, $value, $type, $onblur) {

   if(is_array($value)) $value = implode($value);

   $res = "";
   if($type == "text") {
      $onblur = ($onblur != "") ? " onblur=\"".$onblur."\"" : "";
      $bg = ($_REQUEST["do"] == "1") ? "background-image:url(../../../../library/images/schaf.jpg)" : "";
      $res = "<textarea".$onblur." name=\"".$name."\" style=\"".$bg.";width:500px;height:80px;margin-bottom:10px;font:normal 12px century gothic;color:rgb(0,102,158);\">".$value."</textarea>";
   }
   return $res;
}

function check_userrole($access_user_role) {
   $access_user_role="[".$access_user_role."]";
   $return = (in_array($access_user_role, $_SESSION["remote_userroles"])) ? true : false;
   return $return;
}

function urlencode_array(&$item, $key) {
   $item = urldecode($item);
}

function rawurlencode_array(&$item, $key) {
   $item = rawurldecode($item);
}

function trim_array(&$item, $key) {
   $item = trim($item);
}

function format_date($t_date) {
   global $_application;
   $res = (strtotime($t_date) > 0) ? date($_application["preferences"]["settings"]["date_format"], strtotime($t_date)) : "&nbsp;";
   return $res;

}

// Authentifizierung durchführen
function get_domino_header($key, $http_header) {
   $http_header=$http_header."§§";
   $val = substr($http_header, strpos($http_header, $key) + strlen($key) + 2, strlen($http_header));
   return substr($val, 0, strpos($val, "§§"));
}


function htmlentities_html($v) {
   $htmlentities = get_html_translation_table(HTML_ENTITIES);
   unset($htmlentities["\""]);
   unset($htmlentities["<"]);
   unset($htmlentities[">"]);
   unset($htmlentities["&"]);

   return str_replace(array_keys($htmlentities), array_values($htmlentities), $v);

}

function set_html_title($val) {
   $_SESSION["html"]["title"] = $val;
   $_SESSION["html"]["stamp"] = $_SESSION["unique_page"];
}


function embed_selectbox($user, $empty, $value, $txt, $sel, $attr) {
   if(check_editable($user) || $user == false) {
      foreach($attr as $key => $val) {
         $add_attr .= " ".$key."=\"".$val."\"";
      }
      $html = "<select".$add_attr.">\r\n";
      if($empty) $html .= "<option></option>\r\n";
      $c = count($value); 
      for($i = 0; $i < $c; $i++) {
         $selected = ($sel == $value[$i]) ? " selected" : "";
         $html .= "<option value=\"".$value[$i]."\"$selected>".$txt[$i]."</option>\r\n";
      }
      $html .= "</select>\r\n";
   }
   else {
      $html = $sel;
   }
   return $html;
}



function embed_input($user, $value, $attr) {
   if(check_editable($user)  || $user == false) {
      foreach($attr as $key => $val) {
         $add_attr .= " ".$key."=\"".$val."\"";
      }
      $html = "<input".$add_attr." value=\"".$value."\" />\r\n";
   }
   else {
      $html = $value;
   }
   return $html;
}

function check_editable($userroles) {
   $editable = false;
   foreach($userroles as $val) {
      if(in_array($val, $_SESSION["remote_userroles"])) {
         $editable = true;
         break;
      }
   }
   return $editable;
}

function http_post($url, $data, $headers=null) {

   $opts["http"]["method"] = "POST";
   $opts["http"]["header"] = "Cookie: ".$_SESSION["header"]["Cookie"];
   $opts["http"]["content"] = $data;

   $st = stream_context_create($opts);
   $fp = fopen($url, "rb", false, $st);
   if(!$fp) return false;
   else return stream_get_contents($fp);
}


function tracker_get_pot_dates($pot, $inspection_pot, $tc_data) {

   global $_application;
   foreach($_application["process"]["subprocess"] as $key => $val) {
         if(strlen($key) == 5) {
            $_application["process"]["subprocess"][$key."0"] = $val;
            unset($_application["process"]["subprocess"][$key]);
         }
   }
   $tracker = (!$inspection_pot) ? $_SESSION["tracker"] : $inspection_pot;
   $db_path = (strtolower($tc_data["tracker"][archived]) != "no") ? $_SESSION["remote_domino_path_archive"] : $_SESSION["remote_domino_path_main"];
   $pot_date = generate_xml($db_path."/v.dates_by_tracker?open&count=999&restricttocategory=".$pot."@".$tracker."&function=xml:data");

   if(isset($pot_date["h2"])) {
      $pot_date = array();
   }
   else {
      $include_subprocesses = array_keys($_application["process"]["subprocess"]);
      foreach($pot_date as $key => $val) {

         if(!in_array($key, $include_subprocesses)) {
            unset($pot_date[$key]);
         }
         if(!isset($pot_date[$key][0])) $pot_date[$key] = array($pot_date[$key]);
      }
   }

   return $pot_date;
}


function calc_delta($date) {

   $planned = strtotime($date["revised"]);
   $actual = strtotime($date["actual"]);
   $r = 0;

   if($planned > $actual) {
      for($i = $actual; $i < $planned; $i = $i + 86400) {   
         if(!in_array(date("w", $i), array(0, 6))) $r++;
      }
   }
   else {
      for($i = $planned; $i < $actual; $i = $i + 86400) {   
         if(!in_array(date("w", $i), array(0, 6))) $r--;
      }
   }
   return $r;
}




?>