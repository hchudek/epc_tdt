<?php

header('HTTP/1.1 200 OK'); 
header('Content-type: text/plain'); 
header('Content-Disposition: attachment; filename="epc.xml"');

error_reporting(0);						// Keine Fehlermeldungen :-)



/* 
2010/06/14 CA

PFLICHT		app=[Name der Anwendung]			L�dt den Inhalt der Ansicht
ERG�NZEND	number=true || false				Nummeriert die Ansicht
ERG�NZEND	sortkey=[Sortierte Spalte]			�berschrift der Spalte, nicht Spaltennummer
ERG�NZEND	sortdir=asc || desc				Aufsteigende oder absteigende Sortierung
ERG�NZEND	count=[Anzahl der Dokumente]
PFLICHT		start=[Erstes angezeigtes Dokument]
ERG�NZEND	f0=[Filter]					Funktioniert �hnlich wie Kategorien!	
ERG�NZEND	f1=[Filter]					Funktioniert �hnlich wie Kategorien!	
ERG�NZEND	f2=[Filter]					Funktioniert �hnlich wie Kategorien!	
ERG�NZEND	f9=[Filter]					Readerfelder!	
ERG�NZEND	key=[lookup]					Funktioniert wie Domino-startkey	
ERG�NZEND	link=[Verlinkte Spalte]				�berschrift der Spalte, nicht Spaltennummer	
ERG�NZEND	hideline=Spalte 1, Spalte 2, ...		Versteckt Spalten (ben�tigt JS-Funktion in Notes)
ERG�NZEND	icon=true || false				Icon in Ansicht anzeigen	
ERG�NZEND	q=Searchstring
ERG�NZEND	view=Name der Ansicht					
*/


print "<?xml version=\"1.0\"?>\r\n";
print "<?mso-application progid=\"Excel.Sheet\"?>\r\n";
print "<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n";
print " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n";
print " xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\r\n";
print " xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n";
print " xmlns:html=\"http://www.w3.org/TR/REC-html40\">\r\n";
print " <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">\r\n";
print "  <Author>Lotus Domino</Author>\r\n";
print "  <LastAuthor>Lotus Domino</LastAuthor>\r\n";
print "  <Created></Created>\r\n";
print "  <Company>Tyco Electronics</Company>\r\n";
print "  <Version>11.9999</Version>\r\n";
print " </DocumentProperties>\r\n";
print " <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\r\n";
print "  <WindowHeight>12840</WindowHeight>\r\n";
print "  <WindowWidth>15315</WindowWidth>\r\n";
print "  <WindowTopX>360</WindowTopX>\r\n";
print "  <WindowTopY>105</WindowTopY>\r\n";
print "  <ProtectStructure>False</ProtectStructure>\r\n";
print "  <ProtectWindows>False</ProtectWindows>\r\n";
print " </ExcelWorkbook>\r\n";
print " <Styles>\r\n";
print "  <Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n";
print "   <Alignment ss:Vertical=\"Bottom\"/>\r\n";
print "   <Borders/>\r\n";
print "   <Font/>\r\n";
print "   <Interior/>\r\n";
print "   <NumberFormat/>\r\n";
print "   <Protection/>\r\n";
print "  </Style>\r\n";
print "  <Style ss:ID=\"s21\">\r\n";
print "  <Borders>\r\n";
print "      <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>\r\n";
print "  </Borders>\r\n";
print "  </Style>\r\n";
print " </Styles>\r\n";
print " <Worksheet ss:Name=\"EP&amp;C\">\r\n";
print "  <Table ss:ExpandedColumnCount=\"".count($column)."\" ss:ExpandedRowCount=\"".(count($f_data)+1)."\" x:FullColumns=\"1\"\r\n";
print "   x:FullRows=\"1\" ss:DefaultColumnWidth=\"80\">\r\n";
print "   <Row>\r\n";



function generate_extended_view($dsp_filter, $path, $column, $data, $_w, $module_id, $icon, $number, $filter, $columnselector, $navigator, $addline, $basecount, $uselink, $filter_descr, $kill_sort_view, $tbl_style) {

   global $_application;
   $_c=($filter==true) ? 3 : 3;
   $m_id=$module_id."_";

   $query=urldecode($_SERVER["QUERY_STRING"]);
   if($_REQUEST[$m_id."sortkey"]!="") $sortkey=$_REQUEST[$m_id."sortkey"]; else $sortkey=$column[1+$_c];
   if($_REQUEST[$m_id."sortdir"]=="asc") $sortdir=SORT_ASC; else $sortdir=SORT_DESC;
   if($_REQUEST[$m_id."count"]!="") $count=intval($_REQUEST[$m_id."count"]); else $count=$basecount;
   if($_REQUEST[$m_id."start"]!="") $start=intval($_REQUEST[$m_id."start"]); else $start=1;
   if($_REQUEST[$m_id."f0"]!="") $f0=strtolower($_REQUEST[$m_id."f0"]); else $f0="";
   if($_REQUEST[$m_id."f1"]!="") $f1=strtolower($_REQUEST[$m_id."f1"]); else $f1="";
   if($_REQUEST[$m_id."f2"]!="") $f2=strtolower(base64_decode($_REQUEST[$m_id."f2"])); else $f2="";
   if($_REQUEST[$m_id."f9"]!="") $f9=base64_decode($_REQUEST[$m_id."f9"]); else $f9="";
   if($_REQUEST[$m_id."key"]!="") $key=strtolower($_REQUEST[$m_id."key"]); else $key="";
   if($_REQUEST[$m_id."link"]!="") $link=$_REQUEST[$m_id."link"]; else $link=$column[1+$_c];
   if($_REQUEST[$m_id."hideline"]!="") $hideline=explode(",",$_REQUEST[$m_id."hideline"]); else $hideline=false;
   if($_REQUEST[$m_id."q"]!="") $q=$_REQUEST[$m_id."q"]; else $q=false;
   if($_REQUEST[$m_id."view"]!="") $view=$_REQUEST[$m_id."view"]; else $view=false;
   if($_REQUEST[$m_id."sort"]!="") $sort_column=explode(",",$_REQUEST[$m_id."sort"]); else $sort_column=false;
   $t_template=($_REQUEST[$m_id."template"]!="") ? "&template=".$_REQUEST[$m_id."template"] : ""; 
   $t_header=($_REQUEST[$m_id."header"]!="") ? "&header=".$_REQUEST[$m_id."header"] : ""; 

   unset($sc); for($i=0;$i<999;$i++) $sc[]=$i;

   // Dynamischer Filter aktivieren
   if($_REQUEST[$m_id."filter"] != "") $d_filter = base64_decode($_REQUEST[$m_id."filter"]); else $d_filter = false;
   if(!$d_filter == false) {
      for($i=0; $i < count($data); $i++) {
         $data[$i]["filter0"] = $data[$i][$d_filter];
      }
   }

   if($sort_column!=false) for($i=0;$i<=count($sort_column);$i++) $sc[$i+1]=$sort_column[$i]; 

   // Filter anwenden, wenn aktiviert
   $data_count = count($data);
   $i=0; while($i < $data_count) {
      $res=true;
      if($f0!="") if(strtolower($data[$i]["filter0"])!=$f0) $res=false;
      if($f1!="") if(strtolower($data[$i]["filter1"])!=$f1) $res=false;
      if($f2!="") if(strtolower($data[$i]["filter2"])!=$f2) $res=false;
      if($f9!="") if(strtolower($data[$i]["filter9"])!=$f9) $res=false;
      if($data[$i]["unid"]=="%del%") $res=false;
      if($res==true) $f_data[]=$data[$i];
      $i++;
   }


   // START: EXTENDED FILTER
   foreach($_REQUEST as $key => $attr) {
      if(substr($key, 0,7) == $m_id."ef") {
         $tmp_data = $f_data;
         unset($f_data);
         $sel_attr = substr($attr, strpos($attr, "$$") + 2, strlen($attr));
         $t_column_select = base64_decode(substr($attr, 0 , strpos($attr, "$$")));
         $f_ext = explode("$$", urldecode(base64_decode($sel_attr)));
         $data_count = count($data);
         $i=0; while($i < $data_count) {
            if(in_array($tmp_data[$i][$t_column_select], $f_ext)) $f_data[] = $tmp_data[$i];
            $i++;
         }
      }
   }
   // ENDE: EXTENDED FILTER



   // Suche durchf�hren, wenn Suchanfrage vorhanden
   if($q!=false) {
      $tmp_data=$f_data;
      $f_data=array();
      for($r=0;$r<=count($tmp_data);$r++) {
         $searchres=doViewSearch($q,$tmp_data[$r],$column,$hideline);
         if($searchres) $f_data[]=$tmp_data[$r];
      }
   }
   // Ansicht sortieren
   if($kill_sort_view != true || $_REQUEST[$m_id."sortkey"]!="") {
      $exit=false; $c=0; while($c<count($column) && $exit==false) {
         if($column[$c]==$sortkey) {
            $sorttype=($sort[$c]==0) ? SORT_STRING : SORT_NUMERIC;
            $exit=true;
         }
         $c++; 
      }
      foreach ($f_data as $sortcriteria) $sort_array[] = $sortcriteria[$sortkey];
      array_multisort($sort_array,$sortdir,$sorttype,$f_data);
   }

   // Lookup anwenden, wenn Lookup ausgef�hrt
   if($key!="") $start=searchFirstLine($key,$sortkey,$f_data);

   $t_view= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"navigation_bar_".$module_id."\" class=\"navigation_bar\">\r\n";
   $t_view.= "<tr>\r\n";
   //$t_view.= "<td width=\"1%\"><a href=\"addin/get_xls.php?".str_replace("&count=".$_REQUEST["count"],"",$_SERVER["QUERY_STRING"])."&count=99999\"><img src=\"../../../../library/images/icons/xls.png\" border=\"0\" style=\"border:none;margin-right:7px;margin-bottom:2px;\" /></td>";

   // Filter-Selector erstellen
   if($filter==true) {
      $t_view.= "<td width=\"18%\" style=\"background-color:#ffffff;\">";
      $t_view.= utf8_encode(generate_filter($data, "filter0", $filter_descr, $m_id));
      $t_view.= "</td>\r\n";
   }

   // Column-Selector erstellen
   if($columnselector==true) {
      $t_view.= "<td width=\"30%\" style=\"background-color:#ffffff;\">";
      $t_view.= utf8_encode(generate_simple_columnselector(count($column)-1-$_c, $count,$m_id));
      $t_view.= "</td>\r\n";
   }

   // Navigator erstellen
   if($navigator==true) {
      $t_view.= "<td align=\"right\" width=\"25%\" style=\"background-color:#ffffff;\">";
      $t_view.= utf8_encode(generate_navigation(count($f_data),$start,$count,$m_id));
      $t_view.= "</td>\r\n";
   }

   $t_view.= "</tr>\r\n";
   $t_view.= "</table>\r\n";


   // Keine Suchtreffer
   if(count($f_data)==0) {!
      $t_view.= "%%ADDLINE%%\r\n<div style=\"font:normal 12px trebuchet ms, verdana;\">No documents found<br /><br /><a href=\"?\" style=\"text-decoration:none;color:rgb(0,102,158);\">Clear filter</a></div>";
   }

   else {

      // Lookup erstellen
      $t_view.= utf8_encode(generate_lookup($column,$m_id));
      $t_view.= "%%ADDLINE%%\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl\" style=\"".$tbl_style."\">";
      $t_view.= "<tr>";

      // HTML-Kopzeile erstellen
      $isfilter=array();
      $tc=0;
      $disp_columns=($_REQUEST[$m_id."columns"]!="") ? $_REQUEST[$m_id."columns"]+$_c : count($column)-1;

      if($number==true) $t_view.= "<th id=\"v$0\" style=\"background-color:#ffffff;width:50px;text-align:center;border-right:solid 1px rgb(99,99,99);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>Num</td></tr></table></th>\r\n";
      if($icon==true) {

         foreach(explode("&", $_SERVER["QUERY_STRING"]) as $attr) {
            if(substr($attr, 0, 7) != $m_id."ef") $t_attr .= "&".$attr;
         }
         $t_attr = str_replace("&&", "&", $t_attr);

         $t_view.= "<th style=\"background-color:#ffffff;width:41px;vertical-align:top;border-right:solid 1px rgb(99,99,99)\">";
         $t_view.= "<nobr><a href=\"javascript:void(0);\" onclick=\"dsp=(document.getElementById('view_selector').style.visibility=='visible') ? 'hidden':'visible';document.getElementById('view_selector').style.visibility=dsp;document.getElementById('link_selector').style.visibility='hidden';\"><img src=\"../../../../library/images/icons/view.png\" style=\"filter:Gray();border:none;width:12px;margin-right:4px;\" /></a><img style=\"cursor:pointer;\" onclick=\"location.href='?".$t_attr."';\" src=\"../../../../library/images/icons/filter2.gif\" style=\"width:12px;\" /></nobr>";
         
        // $t_view.= "<a style=\"display:none;\" href=\"javascript:void(0);\" onclick=\"dsp=(document.getElementById('link_selector').style.visibility=='visible') ? 'hidden':'visible';document.getElementById('link_selector').style.visibility=dsp;document.getElementById('view_selector').style.visibility='hidden';\"><img src=\"../../../../library/images/icons/link.png\" style=\"filter:Gray();border:none;\" /></a></nobr></th>"; 
      }
      $t_key=false;
      for($c=4+$_c;$c<=$disp_columns+3;$c++) {
         $instr=strpos(strtolower(".".$column[$c]),"filter");
         if($instr!=1) {
            $tc++;
            $align=($sort[$sc[$tc]+3]>0) ? "right" : "left";
            $arr_up=($column[$sc[$tc]+3]==$sortkey && $sortdir==SORT_ASC && ($kill_sort_view!=true || $_REQUEST[$m_id."sortkey"]!="")) ? "../../../../library/images/arrow_up_high.png" : "../../../../library/images/arrow_up_reg.png"; $arr_up="<img src=\"".$arr_up."\" border=\"0\" style=\"display:inline;margin-right:2px;\" />";
            $arr_down=($column[$sc[$tc]+3]==$sortkey && $sortdir==SORT_DESC && ($kill_sort_view!=true || $_REQUEST[$m_id."sortkey"]!="")) ? "../../../../library/images/arrow_down_high.png" : "../../../../library/images/arrow_down_reg.png"; $arr_down="<img src=\"".$arr_down."\" border=\"0\" style=\"display:inline;\" />";
            if($column[$sc[$tc]+3]==$sortkey) $t_key=true;
            $t_view.= utf8_encode("<th id=\"v$".$tc."\" style=\"background-color:#ffffff;\">");
            if(strtoupper(substr($column[$sc[$tc]+3],0,7))!="%%HTML:") {
               $descr = (strlen($column[$sc[$tc]+3]) > 1) ? utf8_encode($column[$sc[$tc]+3]) : "";
               $t_view.= utf8_encode("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"".$align."\"><tr>");
               $t_ef = $_REQUEST[$m_id."ef_".str_replace("=", "", base64_encode($descr))];


               $f_img = ($_REQUEST[$m_id."ef_".str_replace("=", "", base64_encode($descr))] == "") ? "filter.gif" : "filter.png";

               if($descr!="" && !isset($dsp_filter[$descr])) $t_view.= utf8_encode("<td><a href=\"javascript:embed_filter('".$t_ef."', '".$module_id."', '".base64_encode($descr)."');\"><img src=\"../../../../library/images/icons/".$f_img."\" style=\"border:none;margin-right:2px;\" /></a><br /><a name=\"".$m_id."ext_filter\" r_id=\"".$m_id.base64_encode($descr)."\" style=\"visibility:hidden;position:absolute;\" id=\"".$m_id."url_".str_replace("=", "", base64_encode($descr))."\">".str_replace("&".$m_id."ef_".str_replace("=", "", base64_encode($descr))."=".$_REQUEST[$m_id."ef_".str_replace("=", "", base64_encode($descr))], "", $_SERVER["QUERY_STRING"])."</a><div id=\"".$m_id.base64_encode($descr)."\" class=\"extended_filter\"></div></td>");
               if($descr!="") $t_view.= utf8_encode("<td><a href=\"?".str_replace("&".$m_id."f9=".$_REQUEST[$m_id."f9"],"",str_replace("&".$m_id."start=".$_REQUEST[$m_id."start"],"&".$m_id."start=1",str_replace("&".$m_id."sortkey=".$_REQUEST[$m_id."sortkey"],"",str_replace("&".$m_id."sortdir=".$_REQUEST[$m_id."sortdir"],"",$query))))."&".$m_id."sortkey=".urlencode($column[$sc[$tc]+3])."&".$m_id."sortdir=asc\">".$arr_up."</a></td>");
               if($descr!="") $t_view.= utf8_encode("<td><a href=\"?".str_replace("&".$m_id."f9=".$_REQUEST[$m_id."f9"],"",str_replace("&".$m_id."start=".$_REQUEST[$m_id."start"],"&".$m_id."start=1",str_replace("&".$m_id."sortkey=".$_REQUEST[$m_id."sortkey"],"",str_replace("&".$m_id."sortdir=".$_REQUEST[$m_id."sortdir"],"",$query))))."&".$m_id."sortkey=".urlencode($column[$sc[$tc]+3])."&".$m_id."sortdir=desc\">".$arr_down."</a></td>");
               $t_view.= utf8_encode("<td><div style=\"white-space:nowrap;\">".$descr."</div></td></tr></table>");
            }
            $t_view.= utf8_encode("</th>\r\n");
         }
         else $isfilter[]=$c;
      }
      $t_view.= "</tr>";

      // View-Selector erstellen

      for($c=1;$c<count($column);$c++) {
         $instr=strpos(strtolower(".".$column[$c]),"filter");
         if($instr!=1) $colname[]=utf8_encode($column[$c]);
      }

      if($hideline!=false) for($i=0;$i<count($hideline);$i++) $hidecolumn[]=$column[$hideline[$i]+3];

      $border="border-bottom:solid 2px rgb(90,90,90);";
      $t_view.= "<tr id=\"view_selector\" style=\"position:absolute;visibility:hidden;\">\r\n";
      if($number==true) $t_view.= "<td></td>";
      if($icon==true) $t_view.= "<td style=\"background-color:#ffffff;filter:Alpha(opacity=94);$border\">&nbsp;</td>";
      $tmp_hl=($hideline==false) ? 0 : count($hideline);
      for($c1=0;$c1<$disp_columns-3;$c1++) {
         $t_view.= "<td style=\"font:normal 12px; vertical-align:top;white-space:nowrap;padding-top:10px;padding-left:10px;background-color:#ffffff;filter:Alpha(opacity=94);$border\">";
         for($c2=0;$c2<count($colname);$c2++) {
            //if(!in_array ($colname[$c2],$hidecolumn)) {
            $t_view.= "<a style=\"";
            if(($sc[$c1+1])==($c2+1)) $t_view.= "color:rgb(222,135,3);";
            $t_view.= "\" name=\"view_column_link_".($c1+1)."\" href=\"javascript:void(0);\" onclick=\"for(i=0;i<document.getElementsByName('view_column_link_".($c1+1)."').length;i++) document.getElementsByName('view_column_link_".($c1+1)."')[i].style.color='';this.style.color='rgb(222,135,3)';document.getElementById('view_column_".($c1+1)."').innerText='".($c2+1)."';\">";
            $t_view.= "  ".$colname[$c2]."</a><br />";
         }
         $t_col=($sort_column==false) ? ($c1+1) : $sort_column[$c1];
         $t_view.= "<a id=\"view_column_".($c1+1)."\" style=\"display:none;\">".$t_col."</a>";
         if($c1==0) {
            $query=str_replace("&".$m_id."sort=".$_REQUEST[$m_id."sort"],"",urldecode($_SERVER["QUERY_STRING"]));
            $t_view.= "<div style=\"width:100%;text-align:left;padding:7px 0px 7px 0px;\"><span class=\"phpbutton\">";
            $t_view.= "<a href=\"javascript:void(0)\" onclick=\"";
            $t_view.= "v='';";
            $t_view.= "for(i=1;i<=".count($colname).";i++) if(document.getElementById('view_column_'+String(i))) v+=document.getElementById('view_column_'+String(i)).innerText+',';";
            $t_view.= "location.href=('?$query&".$m_id."sort='+(v.substr(0,v.length-1)));\" style=\"color:#ffffff;\">Submit</a></span></div>";
         }
         $t_view.= "</td>\r\n";
      }
      $t_view.= "</tr>\r\n";

      // Link-Selector erstellen
      $border="border-bottom:solid 2px rgb(90,90,90);";
      $t_view.= "<tr id=\"link_selector\" style=\"position:absolute;visibility:hidden;\">\r\n";
      if($number==true) $t_view.= "<td></td>";
      if($icon==true) $t_view.= "<td style=\"background-color:#ffffff;filter:Alpha(opacity=94);$border\">&nbsp;</td>";
      $t_view.= "<td colspan=\"".($disp_columns-3)."\" style=\"font:normal 12px;vertical-align:top;white-space:nowrap;overflow:hidden;padding-top:10px;background-color:#ffffff;filter:Alpha(opacity=94);$border\">";
      for($c2=0;$c2<count($colname);$c2++) {
         if(!in_array (($c2+1),$hideline)) {
            $t_view.= "<a style=\"";
            if($colname[$c2]==$link) $t_view.= "color:rgb(222,135,3);";
            $t_view.= "\" name=\"link_column_".($c1+1)."\" href=\"javascript:void(0);\" onclick=\"for(i=0;i<document.getElementsByName('link_column_".($c1+1)."').length;i++) document.getElementsByName('link_column_".($c1+1)."')[i].style.color='';this.style.color='rgb(222,135,3)';document.getElementById('t_link_column').innerText=this.innerText;\">".$colname[$c2]."</a><br />";
         }
      }
      $query=str_replace("&".$m_id."link=".$_REQUEST[$m_id."link"],"",urldecode($_SERVER["QUERY_STRING"]));
      $t_view.= "<a id=\"t_link_column\" style=\"display:none;\">".$link."</a>";
      $t_view.= "<div style=\"width:100%;text-align:left;padding:7px 0px 7px 0px;\"><span class=\"phpbutton\">";
      $t_view.= "<a href=\"javascript:void(0)\" onclick=\"v=document.getElementById('t_link_column').innerText;";
      $t_view.= "location.href='?$query&".$m_id."link='+v;\" style=\"color:#ffffff;\">Submit</a></span></div>";

      $t_view.= "</td></tr>\r\n";


      // HTML-Tabelle erstellen
      if(icon==true) $disp_columns++;
      $countline=0; $r=$start-1; while($r<count($f_data) && $countline<$count) {
         $countline++;
         $t_view.= "<tr id=\"v$".$countline."\">\r\n";   
         $num=($sortdir==SORT_ASC) ? $countline+$start-1 : count($f_data)-$start-$countline+2;
         if($number==true) $t_view.= "<td id=\"v$".$countline."$0\" class=\"vnum\" align=\"center\" width=\"1%\">".number_format($num,0,",",".")."</td>\r\n";  
         if($icon==true) {
            if($uselink[0]=="use:ajax") $t_img="<a href=\"javascript:void(0)\" onclick=\"dsp=document.getElementById('v_addline_".$m_id.$countline."').parentNode.parentNode.style; if(dsp.display=='none') run_ajax('v_addline_".$m_id.$countline."','".str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1])."&id=v_addline_".$m_id.$countline."','dsp.display=\'block\''); else dsp.display='none';\"><img src=\"../../../../library/images/ico_doc.gif\" style=\"border:none;filter:Gray();\" /></a>";
            else {
               $t_link = str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1]);
               $t_link = str_replace("%%PART NUMBER%%", $f_data[$r]["Part number"], $t_link);
               $t_link = str_replace("%%TOOL NUMBER%%", $f_data[$r]["Tool number"], $t_link);
               $t_img="<a href=\"".$t_link."\"><img src=\"../../../../library/images/ico_doc.gif\" style=\"border:none;filter:Gray();margin-left:3px;\" /></a>";
            }
            $t_view.= "<td class=\"vreg\" align=\"center\" style=\"width:1%;border-right:solid 1px rgb(99,99,99);\">".$t_img."</td>\r\n";
         }
         $tc=1; $nolink=false; for($c=4+$_c;$c<$disp_columns+3;$c++) {
            $class=($column[$sc[$tc]+3]==$sortkey) ? "vhigh" : "vreg";
            $txt=(strpos(strtolower(".".urldecode($f_data[$r][$column[$sc[$tc]+3]])),"%%display:")) ? substr($f_data[$r][$column[$sc[$tc]+3]],strpos(strtolower(".".urldecode($f_data[$r][$column[$sc[$tc]+3]])),"%%display:")+9,strlen(urldecode($f_data[$r][$column[$sc[$tc]+3]]))) : urldecode($f_data[$r][$column[$sc[$tc]+3]]);
            $align="left";
            if($sort[$c]>0) {
               if(intval($txt)>1000) $txt=number_format($txt,$n,",",".");
               $align="right";
            } 
            if(!in_array($c,$isfilter)) {
               $tc++;
               if($column[$sc[$c]-3]==$link) {
                  $nolink=true;
                  if($uselink[0]=="use:ajax") $txt="<a href=\"javascript:void(0)\" onclick=\"dsp=document.getElementById('v_addline_".$m_id.$countline."').parentNode.parentNode.style; if(dsp.display=='none') run_ajax('v_addline_".$m_id.$countline."','".str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1])."&id=v_addline_".$m_id.$countline."','dsp.display=\'block\''); else dsp.display='none';\">".$txt."</a>";
                  else {
                     $t_link = str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1]);
                     $t_link = str_replace("%%PART NUMBER%%", $f_data[$r]["Part number"], $t_link);
                     $t_link = str_replace("%%TOOL NUMBER%%", $f_data[$r]["Tool number"], $t_link);

                     // ************************
                     // START: Datum formatieren
                     // ************************
                     if(strtotime($txt) > 0) $txt = date($_application["preferences"]["settings"]["date_format"], strtotime($txt));	


                     $txt="<a href=\"".$t_link."\">".trim($txt)."</a>";
                  }
               }
               if($q!=false) $txt=getString($txt,$q,0);
               // ************************
               // START: Datum formatieren
               // ************************
               if(strtotime($txt) > 0) $txt = date($_application["preferences"]["settings"]["date_format"], strtotime($txt));	
               // ************************
               // ENDE: Datum formatieren
               // ************************

               if(!in_array (($c-3),$hideline)) $t_view.= utf8_encode("<td id=\"v$".$countline."$".$tc."\" class=\"".$class."\" align=\"".$align."\">".str_replace('&#x0022;','"',$txt)."&nbsp;</td>\r\n");
            }
         }
         $t_view.= "</tr>\r\n";
         if($addline==true) $t_view.= "<tr style=\"display:none;\"><td style=\"border-right:solid 1px rgb(99,99,99);border-bottom:solid 1px rgb(200,200,200);\">&nbsp;</td><td colspan=\"".($disp_columns)."\" style=\"border-bottom:solid 1px rgb(200,200,200);\"><span id=\"v_addline_".$m_id.$countline."\">&nbsp;</span></td></tr>\r\n";
         $r++;
      }
      $t_view.= "<tr><td align=\"right\" colspan=\"".($disp_columns+1-$_c)."\" style=\"background-color:#ffffff;\"><br /><a href=\"#top\" class=\"text\"><img src=\"../../../../library/images/icons/top.png\" border=\"0\" /></a></td></tr></table>\r\n<div style=\"background-color:#ffffff;\">&nbsp;</div>";
   }

   $t_view .= "\r\n<div id=\"".$m_id."path\" style=\"display:none;\">".base64_encode($path)."</div>\r\n";

   return $t_view;

}






// Neue Einstellungen f�r Supplier DB, wenn keine Werte in der URL stehen
if($link=="") $link=$column[$sc[1]];
if($sortkey=="") $sortkey=$column[$sc[1]];


















// Ab hier: generatesearch alte Funktionen...

function searchFirstLine($key,$sortkey,$f_data) {
   $ret=1;
   $exit=false;
   $i=0; while($i<count($f_data) && $exit==false) {
      $instr=strpos(strtolower(".".$f_data[$i][$sortkey]),$key);
      if($instr==1) {
         $exit=true;
         $ret=($i+1);
      }
      $i++;
   }
   return $ret;
}


function doViewSearch($searchstring,$data,$column,$hideline) {

   $searchfor=explode(" ",removeSpecialChar($searchstring));
   $globalfound=true;


   for($sf=0;$sf<count($searchfor);$sf++) {
      $notfound=true;
      for($d=3;$d<count($column);$d++) {
         if(!in_array($d-2,$hideline)) if(strpos(strtolower(" " . str_replace("&nbsp;","",removeSpecialChar($data[$column[$d]]))),strtolower($searchfor[$sf]))==true) $notfound=false;
      }
   if($notfound==true) $globalfound=false;
   }
   return $globalfound;
}


function doSearch($searchstring,$row,$field) {
   global $data;
   global $columns;
   if($_REQUEST['rtf']=="true") $rtf=0;
   else $rtf=-1;
   $searchfor=explode(" ",removeSpecialChar($searchstring));
   $globalfound=true;
   for($sf=0;$sf<count($searchfor);$sf++) {
      $found=false;
      if($field<0) { 
         for($d=0;$d<($field*-1)+$rtf;$d++) if(strpos(strtolower(" " . removeSpecialChar($data[$row][$d])),strtolower($searchfor[$sf]))==true) $found=true;
      }
      else {
         if(strpos(strtolower(" " . removeSpecialChar($data[$row][$field])),strtolower($searchfor[$sf]))==true) $found=true;
      }
      if($found==false) $globalfound=false;
   }
   return $globalfound;
}


function doAdvancedSearch($row) {
   $searchfor1=trim(str_replace("  "," ",$_REQUEST['q1']));
   $searchfor2=trim(str_replace("  "," ",$_REQUEST['q2']));
   $searchfor3=trim(str_replace("  "," ",$_REQUEST['q3']));
   $searchfor4=trim(str_replace("  "," ",$_REQUEST['q4']));
   $searchfield1=$_REQUEST['s1'];
   $searchfield2=$_REQUEST['s2'];
   $searchfield3=$_REQUEST['s3'];
   $searchfield4=$_REQUEST['s4'];

   $found=true;
   if($searchfor1!="") if(doSearch($searchfor1,$row,$searchfield1)==false) $found=false;
   if($searchfor2!="") if(doSearch($searchfor2,$row,$searchfield2)==false) $found=false;
   if($searchfor3!="") if(doSearch($searchfor3,$row,$searchfield3)==false) $found=false;
   if($searchfor4!="") if(doSearch($searchfor4,$row,$searchfield4)==false) $found=false;
   
   return $found;
   
}


function getString($string,$searchstring,$num) {
   $string=str_replace("&nbsp;"," ",$string);
   $searchfor=explode(" ",removeSpecialChar($searchstring));
   for($sf=0;$sf<count($searchfor);$sf++) {   
      $p=strpos(" ".strtolower($string),strtolower($searchfor[$sf]));
      if($p>0) {
         $tstr=substr($string,$p-1,strlen($searchfor[$sf]));
         $string=substr_replace($string,"<b>".$tstr."</b>",$p-1,strlen($searchfor[$sf]));
      }
   }
   if($num==1) {
      $nstring=substr($string,0,200);
      $nstring=substr($nstring,0,strrpos($nstring," "));
      if($string!=$nstring) $nstring.=" ...";
      return $nstring;
   } 
   else return $string;
}


function removeSpecialChar($strng) {
$rep_from = array ('�','�','�','�','�','�','�','"','-');
   $rep_to = array ('ae','ue','oe','Ue','Ue','Oe','ss','','');
   for($x=0;$x<count($rep_from);$x++) $strng=str_replace($rep_from[$x],$rep_to[$x],$strng);
   return $strng;
}


function addUnicode($strng) {
$rep_from = array ('�','�','�','�','�','�','�','"','-');
   $rep_to = array ('&auml;','&uuml','&ouml','&Auml','&Uuml;','&Ouml;','&szlig;','','-');
   for($x=0;$x<count($rep_from);$x++) $strng=str_replace($rep_from[$x],$rep_to[$x],$strng);
   return $strng;
}


function addContent($arr,$tag,$attr) { 
   global $_content;
   if($attr!="") $attr=" ".$attr;
   $txt=str_replace("%%br%%","</".$tag.">\n<".$tag.$attr.">",$_content[$arr]);
   $res="<".$tag.$attr.">".$txt."</".$tag.">";
   if($_SESSION["userroles"]) {
      if(decode($_SESSION['userroles'],0)=="supervisor") $res="<div style=\"position:absolute;\"><span style=\"position:relative;top:-5px;left:5px;adding-bottom:10px;\"><span style=\"-moz-opacity:0.8;padding:2px;border:solid 1px #666666;background-color:rgb(255,102,102);font:normal 11px verdana;\">".$arr."</span></span></div>".$res;
   }
   return $res;
}


function addStringAsContent($strng,$tag,$attr) { 
   if($attr!="") $attr=" ".$attr;
   $txt=str_replace("%%br%%","</".$tag.">\n<".$tag.$attr.">",$strng);
   $res="<".$tag.$attr.">".$txt."</".$tag.">";
   if($_SESSION["userroles"]) {
      if(decode($_SESSION['userroles'],0)=="supervisor") $res="<div style=\"position:absolute;\"><span style=\"position:relative;top:-5px;left:5px;adding-bottom:10px;\"><span style=\"-moz-opacity:0.8;padding:2px;border:solid 1px #666666;background-color:rgb(255,102,102);font:normal 11px verdana;\">".$arr."</span></span></div>".$res;
   }
   return $res;
}



print "  </Table>\r\n";

print "  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\r\n";
print "   <PageSetup>\r\n";
print "    <Header x:Margin=\"0.4921259845\"/>\r\n";
print "    <Footer x:Margin=\"0.4921259845\"/>\r\n";
print "    <PageMargins x:Bottom=\"0.984251969\" x:Left=\"0.78740157499999996\"\r\n";
print "     x:Right=\"0.78740157499999996\" x:Top=\"0.984251969\"/>\r\n";
print "   </PageSetup>\r\n";
print "   <Selected/>\r\n";
print "   <Panes>\r\n";
print "    <Pane>\r\n";
print "     <Number>3</Number>\r\n";
print "     <ActiveRow>0</ActiveRow>\r\n";
print "     <ActiveCol>0</ActiveCol>\r\n";
print "    </Pane>\r\n";
print "   </Panes>\r\n";
print "   <ProtectObjects>False</ProtectObjects>\r\n";
print "   <ProtectScenarios>False</ProtectScenarios>\r\n";
print "  </WorksheetOptions>\r\n";
print " </Worksheet>\r\n";
print "</Workbook>\r\n";

?>