<style>
.t_high {
   border:solid 2px rgb(77,77,77);
   font-weight:bold;
}

.t_reg {
   border:solid 2px rgb(77,77,77);
   font-weight:normal;
}
</style>
<?php

error_reporting(0);


$today=getdate();
$t_month=($_REQUEST["ga_1_month"]=="") ? $today["mon"]-1 : $_REQUEST["ga_1_month"]-1;
$t_year=($_REQUEST["ga_1_year"]=="") ? $today["year"] : $_REQUEST["ga_1_year"];
$t_date=strtotime($t_year."-".($t_month+1)."-01");
$t_firstday = (date("w", $t_date)); if($t_firstday==0) $t_firstday=7;
$t_lastday = date("t", $t_date)+1;


$year=array($t_year-2, $t_year-1, $t_year, $t_year+1, $t_year+2);
$month=array("J","F","M","A","M","J","J","A","S","O","N","D");
$day=array("Mo","Tu","We","Th","Fr","Sa","Su");
$p_id=$_REQUEST["p_id"];

$t_url_m=str_replace("&ga_1_month=".$_REQUEST["ga_1_month"],"",$_SERVER["QUERY_STRING"]);

print "<table id=\"small_calendar\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"opacity:0.92;border:solid 1px rgb(169,169,169);box-shadow: 6px 6px 5px -3px #a1a1a1;\">\r\n";

print "<tr>\r\n";
print "<td colspan=\"13\" style=\"background:rgba(88,88,88,0.0); text-align:right; border-bottom:solid 1px rgba(169,169,169,0.95);\"><a style=\"color:rgb(14,14,14);\" href=\"javascript:$('#calendar2').fadeOut(200);\">[x]</a>&nbsp;&nbsp;</td>";
print "</tr>";

print "<tr>\r\n";

print "<td colspan=\"13\" style=\"position:relative;top:8px; left:11px; border:none;\">";

for($i=0;$i<12;$i++) {
   $style = ($t_month == $i) ? "font:bold 13px Open Sans;background:rgba(169,169,169,0.5);border-radius:1px;" : "font:normal 13px Open Sans";
   $onclick=($t_month==$i) ? "" : "run_ajax('calendar2','addin/".basename(__FILE__)."?".$t_url_m."&ga_1_month=".($i+1)."','');";
   print "<span style=\"".$style."\" >&nbsp;<a href=\"javascript:void(0);\" onclick=\"".$onclick."\">".$month[$i]."</a>&nbsp;</span>\r\n";
}

print "</td></tr>\r\n<tr>\r\n<td colspan=\"13\" class=\"body\">\r\n";

print "<table id=\"small_calendar_body\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n";
print "<tr>";
print "<th style=\"background-color:rgb(255,255,255);\">&nbsp;</th>";
foreach($day as $value) print "<th style=\"background-color:rgb(97,97,97);border:solid 1px rgb(255,255,255);color:rgb(255,255,255);\">".$value."</th>\r\n";
//print "<tr><td colspan=\"8\" class=\"line\"></td></tr>\r\n";

$t_day=($t_firstday*-1)+2;

$function = ($_REQUEST["p_function"] != "") ? $_REQUEST["p_function"] : "sc";

$exit=false; while($exit==false) {
   print "<tr>\r\n";
   for($i=0;$i<7;$i++) {
      $t_class=($today["mday"]==$t_day && $today["mon"]==($t_month+1) && $today["year"]==$t_year) ? "_today" : "";
      $value = ($t_day>0 && $exit==false) ? "<td class=\"d".($i+1).$t_class."\"><a style=\"font:normal 13px Open Sans;\" href=\"javascript:".$function."('$p_id','".$t_year."','".substr(("0".($t_month+1)),-2)."','".substr(("0".$t_day),-2)."','".$_REQUEST["p_use_rule"]."','".$_REQUEST["p_data_type"]."')\">".$t_day."</a></td>" : "<td></td>";
      if($i == 0) {
         $t_cw = mktime(0, 0, 0, ($t_month + 1), ($t_day + 1), $t_year);
         $value = "<td style=\"background-color:rgb(230,230,242);border:solid 1px rgb(255,255,255);text-align:center;\">".date("W", $t_cw)."</td>" .$value;
      }
      print $value;
      $t_day++;
      if($t_day==$t_lastday) $exit=true;
   }
   print "</tr>\r\n";
}

print "</table></td>\r\n</tr>\r\n";

print "<tr>\r\n";

$t_url_y=str_replace("&ga_1_year=".$_REQUEST["ga_1_year"],"",$_SERVER["QUERY_STRING"]);

print "<td colspan=\"4\" style=\"position:relative;top:-5px;left:9px;\">";

for($i=0;$i<count($year);$i++) {
   $style = ($t_year == $year[$i]) ? "font:bold 13px Open Sans;background:rgba(169,169,169,0.5);border-radius:2px;" : "font:normal 13px Open Sans";
   $onclick=($t_year==$year[$i]) ? "" : "javascript:run_ajax('calendar2','addin/".basename(__FILE__)."?".$t_url_y."&ga_1_year=".$year[$i]."','');";
   print "<span style=\"".$style."\">&nbsp;<a href=\"javascript:void(0);\" onclick=\"".$onclick."\">".$year[$i]."</a>&nbsp;</span>";
}
print "</td>";
print "<td class=\"reg_y\" colspan=\"3\">&nbsp;</td>\r\n</tr>\r\n</table>";


?>