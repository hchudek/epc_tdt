<?php


// TEMPLATE KONFIGURATION

$template["js"][] = "jquery";
$template["js"][] = "jquery-ui";
$template["js"][] = "jquery.gridster";
$template["js"][] = "grid.add";
$template["size"] = 30;
$template["margin"] = 6;
$js_exception = false;


// --------- START: LOAD MODULES -----------------------------------------------------------------------------
$_application["page"][$_application["page_id"]]["template"]["basic"]["header"] = "te_header";
$_application["page"][$_application["page_id"]]["template"]["basic"]["footer"] = "te_footer";
$_application["page"][$_application["page_id"]]["template"]["basic"]["breadcrumbing"] = "te_breadcrumbing";


foreach($_application["page"][$_application["page_id"]]["template"]["add"] as $val) {
   $load = substr($val, strrpos($val, ",") + 1, strlen($val));
   $key = rawurldecode($_application["module"]["description"][$load][0]);
   $_application["page"][$_application["page_id"]]["template"]["basic"][$key] = $load;
}


foreach ($_application["page"][$_application["page_id"]]["template"]["basic"] as $key => $block) {
   if($block != "") $include[$block] = include_once("modules/".$block.".php");   
}


// --------- END: LOAD MODULES -------------------------------------------------------------------------------

   
// ---------- START: GENERATE HTML ---------------------------------------------------------------------------
$_html =
"<div id=\"t_header\">".str_replace("\r\n", "\r\n   ", get_module("te_header", $http_header))."</div>\r\n".
"<div id=\"t_breadcrumbing\">".str_replace("\r\n", "\r\n   ", get_module("te_breadcrumbing", $http_header))."</div>\r\n".
"<div id=\"tdt4grid\" page_id=\"".$_application["page_id"]."\" perspective=\"".$_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]."\">\r\n".
"   <block>block2</block>\r\n".
"   <size>".$template["size"]."</size>\r\n".
"   <margin>".$template["margin"]."</margin>\r\n".
"   <div class=\"gridster\">\r\n".
"      <ul>\r\n";

$add = explode("/", $_SESSION["perpage"]["tab"][$_application["page_id"]][$_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]]);
unset($_SESSION["included"]);



foreach($add as $key => $val) {
   if(!strpos($val, ",")) {
      $val = "01,01,02,02,".$val;
   }
   $param = explode(",", $val);
   $param[4] = str_replace("__", ".", $param[4]);
   $grid_remove = ($_application["module"]["grid_remove"][$param[4]][0] == "false") ? "false" : "true";
   $grid_drag = ($_application["module"]["grid_drag"][$param[4]][0] == "false") ? "false" : "true";
   $grid_resize = ($_application["module"]["grid_resize"][$param[4]][0] == "false") ? "false" : "true";  

   if(isset($_application["module"]["userroles"][$param[4]])) {
      $moduleroles = explode(":", $_application["module"]["userroles"][$param[4]]);
   }
   else {
      $moduleroles = array();
   }
   $content = (check_module_access($moduleroles)) ? get_module($param[4], $http_header) : array($param[4], "Access denied.");
   $block = (max($col) == intval($param[0])) ? "resize" : "block";
   if($param[4] == "m_a4.linklist") $js_exception = true;
   $cursor = ($_application["grid"]["enabled"]) ? "move" : "default";
   $iconfolder = (substr($_application["module"]["grid_body"][$param[4]][0], 0, 6) == "grid_5") ? "": "white/";
   $class_inner_box = ($_application["module"]["scroll_x"][$param[4]][0] == "auto") ? "inner_box_x" : "inner_box";
   $_html .= 
   "\r\n<!-- START: ".$param[4]." -->\r\n\r\n".
   "         <li style=\"visibility:hidden;\" id=\"".str_replace(".", "__", $param[4])."\" class=\"".$_application["module"]["grid_body"][$param[4]][0]."\" grid_remove=\"".$grid_remove."\" grid_drag=\"".$grid_drag."\" grid_resize=\"".$grid_resize."\" data-row=\"".intval($param[1])."\" data-col=\"".intval($param[0])."\" data-sizex=\"".intval($param[2])."\" data-sizey=\"".intval($param[3])."\">\r\n".
   "            <block>".$block."</block>\r\n".
   "            <header class=\"".$_application["module"]["grid_header"][$param[4]][0]."\" style=\"cursor:".$cursor.";\"><img src=\"../../../../library/images/16x16/".$iconfolder.$_application["module"]["grid_image"][$param[4]][0]."\" id=\"del_".str_replace(".", "__", $param[4])."\" style=\"width:13px;cursor:default;position:absolute;margin:3px;\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$content[0]."</header>\r\n".
   "            <div class=\"".$class_inner_box."\">".$content[1]."</div>\r\n".
   "         </li>\r\n";
   "\r\n<!-- END: ".$module." -->\r\n\r\n";

   $_SESSION["included"][] = $param[4];

}


$_html .= 
"      </ul>\r\n".
"   </div>\r\n".
"</div>\r\n".
"<div id=\"t_footer\">".str_replace("\r\n", "\r\n   ", get_module("te_footer", $http_header))."</div>\r\n";


// ---------- END: GENERATE HTML --------------------------------------------------------------------------


// EXCEPTION
if($js_exception) {
   $template["js"][] = "jquery-ui.min";
}

// FUNCTIONS START HERE ....
function get_module($value, $http_header) {

   global $_application;
   global $_userpage;
   global $_hide_modules;
   global $include;
   $_html="";
   $module = ($include[$value] == false) ? array("", "\r\n<br /><div class=\"error\"><nobr>ERROR: ".$value." not included</nobr></div>") : call_user_func(str_replace(".", "__", $value), $_application);
  
   return $module;
}

function check_module_access($moduleroles) {
   if(count($moduleroles) > 0) {
      $return = false;
      foreach($moduleroles as $val) {
         if(in_array($val, $_SESSION["remote_userroles"])) {
            $return = true;
            break;
         } 
      }
   }
   else {
      $return = true;
   }
   return $return;
}

?>