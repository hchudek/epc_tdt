<?php

session_start();
include_once("../../../../library/tools/addin_xml.php");								// XML Library laden
$count = 100;
$start = 1;
$load = true;
do {
   $eval = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.late_reasons?open&start=".$start."&count=".$count."&function=plain");
   $start += $count;
   if(strpos(strtolower($eval), "no documents found")) $load = false;   
   else eval($eval);
} while($load);

$c = count($key);

if(!isset($_POST[$key[0]])) {
   // do early, lates, intime
   $html = "<form method=\"post\" action=\"addin/reasons_report.php?do=lates\" name=\"form_m_d2__causes\">\r\n".
   "<table border\"0\" cellpadding=\"0\" cellspacing=\"2\" id=\"tbl_m_d1__causes\">\r\n";

   $today = mktime();
   $option = "<select name=\"d_from\">\r\n<option></option>\r\n";
   for($i = 0; $i < 365; $i++) {
      $o = date("Y-m-d", $today - ($i * 86400));
      $is_selected = ($o == $_POST["d_from"]) ? " selected" : "";
      $option .= "<option".$is_selected.">".$o."</option>\r\n";
   } 
   $option .= "</select>\r\n";
   $html .= "<tr>\r\n".
   "<td>Date actual from</td>\r\n".
   "<td>".$option."</td>\r\n".
   "</tr>\r\n";

   $option = "<select name=\"d_to\">\r\n<option></option>\r\n";
   for($i = 0; $i < 100; $i++) {
      $o = date("Y-m-d", $today - ($i * 86400));
      $is_selected = ($o == $_POST["d_to"]) ? " selected" : "";
      $option .= "<option".$is_selected.">".$o."</option>\r\n";
   } 
   $option .= "</select>\r\n";
   $html .= "<tr>\r\n".
   "<td>Date actual to</td>\r\n".
   "<td>".$option."</td>\r\n".
   "</tr>\r\n";

   for($i = 0; $i < $c; $i++) {
      if($key[$i] != "date_actual" && $key[$i] != "edited_date" && $key[$i] != "date_planned"  && $key[$i] != "g_phase" && $key[$i] != "prv_process" && $key[$i] != "first_planned") {
         $sel = array();
         foreach($late as $val) {
            if(!in_array(rawurldecode($val[$key[$i]]), $sel)) $sel[] = rawurldecode($val[$key[$i]]);
         }
         sort($sel);
         $option = "<select name=\"".$key[$i]."\">\r\n<option></option>\r\n";
         foreach($sel as $o) {
            $is_selected = ($o == $_POST[$key[$i]]) ? " selected" : "";
            $option .= "<option".$is_selected.">".$o."</option>\r\n";
         }
         $option .= "</select>\r\n";
          $html .= 
         "<tr>\r\n".
         "<td>".strtoupper(substr($key[$i], 0, 1)).str_replace("_", " ", substr($key[$i], 1, strlen($key[$i])))."</td>\r\n".
         "<td>".$option."</td>\r\n".
         "</tr>\r\n";
      }
   }
   $html .= "</table>\r\n".   
   "</form>\r\n".
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font:normal 13px Open Sans; margin-left:11px; margin-top:10px;\">\r\n".
   "<tr>\r\n".
   "<td>".
   "<img name=\"late_causes_do\" onclick=\"m_d2__causes.do(this);\" src=\"../../../../library/images/16x16/status-11-1.png\" style=\"cursor:pointer; margin-right:5px;vertical-align:top;\"><span value=\"early\">Ahead&nbsp;schedule&nbsp;&nbsp;&nbsp;</span><br>\r\n".
   "<img name=\"late_causes_do\" onclick=\"m_d2__causes.do(this);\" src=\"../../../../library/images/16x16/status-11.png\" style=\"cursor:pointer; margin-right:5px;vertical-align:top;\"><span value=\"lates\">Behind&nbsp;schedule</span><br>\r\n".
   "<img name=\"late_causes_do\" onclick=\"m_d2__causes.do(this);\" src=\"../../../../library/images/16x16/status-11-1.png\" style=\"cursor:pointer; margin-right:5px;vertical-align:top;\"><span value=\"intime\">On&nbsp;schedule</span>\r\n".
   "</select>\r\n".
   "</td>\r\n".
   "<td style=\"vertical-align:top;\">\r\n".
   "<span class=\"phpbutton\" style=\"position:relative; left:7px;\"><a href=\"javascript:void(0);\" onclick=\"do_fade(true); document.getElementsByName('form_m_d2__causes')[0].submit();\">Get report</a></span>\r\n".
   "</td>\r\n".
   "</tr>\r\n".
   "</table>\r\n";
}
else {

   $d_from = (isset($_POST["d_from"]) && $_POST["d_from"] != "") ? strtotime($_POST["d_from"]) : strtotime("1970-01-01");
   $d_to = (isset($_POST["d_to"]) && $_POST["d_to"] != "") ? strtotime($_POST["d_to"]) : strtotime(date("Y-m-d", mktime()));

   $html .= "<table border\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font:normal 13px segoe ui, century gothic\">\r\n<tr>\r\n";
   $line = 0;
   for($i = 0; $i < $c; $i++) {
      $output[$line][$i] = strtoupper(substr($key[$i], 0, 1)).str_replace("_", " ", substr($key[$i], 1, strlen($key[$i])));
      $html .= "<td style=\"background-color:rgb(214,214,214);border:solid 1px rgb(255,255,255);\"><b>".strtoupper(substr($key[$i], 0, 1)).str_replace("_", " ", substr($key[$i], 1, strlen($key[$i])))."</b></td>\r\n";
   }
   $html .= "</tr>\r\n";
   $_do = explode(",", $_REQUEST["do"]);

   foreach($late as $val) {
      $val["delta"] = rawurlencode(str_replace(" ", "", rawurldecode($val["delta"])));
      $dsp = true; for($i = 0; $i < $c; $i++) {
         $d_actual = strtotime($val["date_actual"]);
         if($d_actual < $d_from) $dsp = false;
         if($d_actual > $d_to) $dsp = false;
         if(isset($_POST[$key[$i]]) && $_POST[$key[$i]] != "" && strtolower($_POST[$key[$i]]) != strtolower(rawurldecode($val[$key[$i]]))) {
            $dsp = false;
         }

         if(!in_array("lates", $_do) && strtolower(rawurldecode($val["delta"])) > 0) $dsp = false;
         if(!in_array("early", $_do) && strtolower(rawurldecode($val["delta"])) < 0) $dsp = false;
         if(!in_array("intime", $_do) && strtolower(rawurldecode($val["delta"])) == 0) $dsp = false;
 
         if(!$dsp) break;
      }
      if($dsp) {
         $line++;
         $html .= "<tr>\r\n";
         for($i = 0; $i < $c; $i++) {
           $output[$line][$i] = $val[$key[$i]];
           $html .= "<td>".str_replace("Discharge", "-", rawurldecode($val[$key[$i]]))."</td>\r\n";
         }
         $html .= "</tr>\r\n";  
      }
   }

   $html .= "</table>";
   $create_output = "GET";
   $type = "rawurldecode";
   $is_basic = true;			
   require_once("create_te_excel.php");
   print "<a id=\"_dwn\" href=\"".$create_output."\" download></a>\r\n";
   print "<script type=\"text/javascript\">\r\n";
   print "document.getElementById('_dwn').click();\r\n";
   print "window.setTimeout(\"history.back();\", 999);\r\n";
   print "</script>\r\n";
}

?>