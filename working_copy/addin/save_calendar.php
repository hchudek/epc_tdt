<?php
/**
 * Gleiche Funktionalitšt wie "save_data.php"
 * Nur wird das CalenderProfil aus der Session gekillt, um reload zu erzwingen
 * 
 */
session_start();
require("../../../../library/tools/addin_xml.php");
require("basic_php_functions.php");	
if($_REQUEST["target"] != "") {
   foreach($_POST as $key => $val) {
   		
      generate_uniqueid(8);
      $path = strtolower($_SERVER["SCRIPT_FILENAME"]);
      $file = substr($path, 0, strpos($path, "addin"))."static\\temp\\".generate_uniqueid(8).".".$key.".data";
      file_put_contents($file, $val);
      $agent = $_REQUEST["target"]."/a.save?open&type=".$_REQUEST["type"]."&unid=".$_REQUEST["unid"]."&field=".$key."&fget=".rawurlencode($file)."&nocache=".microtime(true);
      $do = file_get_authentificated_contents($agent);
    
      print $do;
   }
}
else {
   $url = $_REQUEST["db"]."/a.setfield?open&unid=".rawurlencode($_REQUEST["unid"])."&field=".rawurlencode($_REQUEST["f"])."&value=".rawurlencode($_REQUEST["v"]);

   $do = file_get_authentificated_contents($url);
/*
   if(trim(strip_tags($do)) == "done") {
      header("Location: ".$_REQUEST["redirect"]);
   }*/

   print($do);
}
unset( $_SESSION["calendarConfig"] );
foreach( $_REQUEST as $key => $value ){
	if( substr( $key , 0 , strlen("m_c8_")) == "m_c8_" ){
		$_SESSION["calendarConfig"][$key] = $value;
	}
}

?>