<?php

$_navigation=0; function generate_navigation($docs,$start,$count,$m_id) {
   global $_navigation; $_navigation++;
   $query=str_replace("&".$m_id."f9=".$_REQUEST["f9"],"",urldecode($_SERVER["QUERY_STRING"]));
   $page=(intval($start/$count)==$start/$count) ? $start/$count : intval($start/$count)+1;
   $pages=(intval($docs/$count)==$docs/$count) ? $docs/$count : intval($docs/$count)+1;

   $ret="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"view_nav\" class=\"view_nav\"><tr><td><a id=\"top_nav\" class=\"top_nav\" href=\"javascript:void(0);\" onclick=\"handleNavigationBar('t_navigator_".$_navigation."');\">Doc: ".$start." / ".$docs." | Page: ".$page." / ".$pages."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>";
   $ret.="<tr><td style=\"position:absolute;\"><div id=\"t_navigator_".$_navigation."\" class=\"navigator\" style=\"display:none;\"><div style=\"height:294px;overflow:auto;position:relative;top:12px;left:-2px;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"t_navigator\" style=\"width:197px;\"><tr>"; 
  
   $l=(intval($pages/7)==$pages/7) ? $pages : (intval($pages/7)+1)*7;
   for($i=0;$i<$l;$i++) {
      if(intval($i/7)==$i/7) $ret.="</tr><tr>";
      if($i<$pages) {
         $q="?".str_replace("&".$m_id."key=".$_REQUEST[$m_id."key"],"",str_replace("&".$m_id."start=".$_REQUEST[$m_id."start"],"&".$m_id."start=".($i*$count+1),$query));
         $txt=(($i+1)==$page) ? "<b>".($i+1)."</b>" : ($i+1);
         if($_REQUEST[$m_id."start"]=="") $q=$q."&".$m_id."start=".($i*$count+1);
         $ret.="<td width=\"21px;\"><a href=\"".$q."\" style=\"display:block;text-align:center;vertical-align:bottom;display:block;\" onmouseover=\"this.style.backgroundColor='rgb(99,177,229)';\" onmouseout=\"this.style.backgroundColor='rgb(255,255,255)';\">".$txt."</a></td>";
      }
      else $ret.="<td>&nbsp;</td>";
   }
   $ret.="</tr></table></div></td></tr></table></div>\r\n";
   return $ret;

}


$_filter=0; function generate_filter($data,$filter,$filter_descr,$m_id) {
   global $_filter; $_filter++;
   $f=$m_id.str_replace("ilter","",$filter);
   $query=str_replace("&".$m_id."f9=".$_REQUEST[$m_id."f9"],"",urldecode($_SERVER["QUERY_STRING"]));
   for($i=0;$i<count($data);$i++) {
      $key[]=$data[$i][$filter];
   }
   $key=array_unique($key);
   asort($key);
  
   if($_REQUEST[$m_id."filter"] != "") $filter_descr = base64_decode($_REQUEST[$m_id."filter"]);

   $filter_descr=($filter_descr=="") ? "No filter" : $filter_descr;
   $d_key = array_keys($data[0]);

   $d_filter = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"".$m_id."d_filter\" style=\"filter:alpha(opacity=92);position:absolute;width:220px;border:solid 1px rgb(99,99,99);background-color:rgb(255,255,255);display:none;\">";
   for($i = 4; $i < count($d_key); $i++) {
      $bgcolor = ($d_key[$i] == base64_decode($_REQUEST[$m_id."filter"])) ? "253,191,62" : "220,220,220";
      $d_filter .= "<tr><td style=\"padding:2px;background-color:rgb(".$bgcolor.")\"><a href=\"?".str_replace("&".$m_id."f0=".$_REQUEST[$m_id."f0"],"",str_replace("&".$m_id."filter=".$_REQUEST[$m_id."filter"], "", $_SERVER["QUERY_STRING"]))."&".$m_id."filter=".base64_encode($d_key[$i])."\">".$d_key[$i]."</a></td></tr>";
   }
   $d_filter .= "</table>";

   $txt=($_REQUEST[$f]=="") ? $filter_descr : $_REQUEST[$f];
   $ret="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"view_filter\"><tr><td><img onclick=\"document.getElementById('filter_1').style.display='none'; handle_fade_in_out('".$m_id."d_filter');\" src=\"../../../../library/images/icons/filter2.png\" style=\"cursor:pointer;border:none;filter:Gray();margin-right:6px;\" />".$d_filter."</td><td><a id=\"top_filter\" href=\"javascript:handleNavigationBar('filter_".$_filter."');\">Filter: ".$txt."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>";
   $ret.="<tr><td style=\"position:absolute;\"><div id=\"filter_".$_filter."\" style=\"position:relative;top:12px;display:none;width:220px;filter:alpha(opacity=90);\"><div style=\"overflow:auto;border-top:solid 1px rgb(99,99,99);background-color:rgb(240,240,255);\" class=\"t_navigator\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:212px;filter:alpha(opacity=94);\">"; 
   $q="?".str_replace("&".$f."=".$_REQUEST[$f],"",str_replace("&key=".$_REQUEST["key"],"",str_replace("&start=".$_REQUEST["start"],"&start=1",$query)))."&".$f."=";
   $ret.=($_REQUEST[$f]=="") ? "<tr><td id=\"no_filter\"><a href=\"".$q."\" style=\"height:17px;padding-top:2px;padding-left:4px;\"><b>No filter</b></a></td></tr>" : "<tr><td id=\"no_filter\"><a href=\"".$q."\" style=\"height:22px;padding-top:4px;padding-left:4px;\">No filter</a></td></tr>";

   foreach ($key as $value) {
      $t_rand=""; for($i=0;$i<6;$i++) $t_rand.=chr(rand(1,20)+96);
      $q="?".str_replace("&".$f."=".$_REQUEST[$f],"",str_replace("&key=".$_REQUEST["key"],"",str_replace("&start=".$_REQUEST["start"],"&start=1",$query)))."&".$f."=".urlencode($value);
      $value=($txt==$value) ? "<b>".$value."</b>" : $value;
      $ret.="<tr><td style=\"padding:0px 0px 0px 0px;\"><a href=\"".$q."\" name=\"t_".$t_rand."\" onmouseover=\"document.getElementsByName(this.name)[0].style.backgroundColor='rgb(99,177,229)'\" onmouseout=\"document.getElementsByName(this.name)[0].style.backgroundColor='rgb(240,240,240)'\" style=\"padding:4px;\">".$value."</a></td></tr>";
   }
   $ret.="</table></div></td></tr></table>";
   return $ret;
}

$_simple_columnselector=0; function generate_simple_columnselector($count, $t_count, $m_id) {
   global $_simple_columnselector; $_simple_columnselector++;
   $query=str_replace("&".$m_id."sort=".$_REQUEST[$m_id."sort"],"",str_replace("&".$m_id."f9=".$_REQUEST[$m_id."f9"],"",urldecode($_SERVER["QUERY_STRING"])));
   $d_columns=($_REQUEST[$m_id."columns"]=="") ? $count : $_REQUEST[$m_id."columns"];
   $d_rows=($_REQUEST[$m_id."count"]=="") ? $t_count : $_REQUEST[$m_id."count"];
   $ret="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"view_columnselector\" class=\"view_columnselector\"><tr><td><a id=\"top_columnselector\" class=\"top_columnselector\" href=\"javascript:void(0);\" onclick=\"handleNavigationBar('t_columnselector_".$_simple_columnselector."');\">Table:&nbsp;".$d_rows." rows ".$d_columns." columns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>";
   $ret.="<tr><td><div class=\"columnselector\" id=\"t_columnselector_".$_simple_columnselector."\" style=\"margin-top:6px;display:none;position:absolute;width:220px;height:165px;filter:alpha(opacity=88);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">"; 

   $docs=array("20","40","60","80","100","120","140");

   for($c=1;$c<=count($docs);$c++) {
      $ret.="<tr>";
      for($i=1;$i<=$count;$i++) {
         $t_border=($i<=$d_columns && $docs[$c-1]<=$d_rows) ? "0,102,158" : "0,0,0";
         $q="?".str_replace("&".$m_id."count=".$_REQUEST[$m_id."count"],"",str_replace("&".$m_id."start=".$_REQUEST[$m_id."start"],"&".$m_id."start=1",str_replace("&".$m_id."key=".$_REQUEST[$m_id."key"],"",str_replace("&".$m_id."columns=".$_REQUEST[$m_id."columns"],"",$query))))."&".$m_id."columns=".urlencode($i)."&".$m_id."count=".$docs[$c-1];
         $ret.="<td><a href=\"".$q."\" style=\"border:solid 1px rgb(".$t_border.");margin:1px;\"><img src=\"../../../../library/images/blank.gif\" alt=\"Rows: ".$docs[$c-1]." Columns: ".$i."\" style=\"width:23px;height:18px;border:0px;\" /></a><td>";
      }
      $ret.="</tr>";
   }
   $ret.="</table></div></td></tr></table>";
   return $ret;
}

function generate_dselector($count) {
   $query=str_replace("&f9=".$_REQUEST["f9"],"",urldecode($_SERVER["QUERY_STRING"]));
   $txt=($_REQUEST["count"]=="") ? "40" : $_REQUEST["count"];
   $ret="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"view_dselector\"><tr><td><a id=\"top_dselector\" href=\"javascript:handleNavigationBar('dselector');\">Documents: ".$txt."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>";
   $ret.="<tr><td><div id=\"dselector\" style=\"display:none;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"; 

   $docs=array("20","40","100","200");
 
   foreach ($docs as $value) {
      $q="?".str_replace("&start=".$_REQUEST["start"],"&start=1",str_replace("&key=".$_REQUEST["key"],"",str_replace("&count=".$_REQUEST["count"],"",$query)))."&count=".urlencode($value);
      $value=($txt==$value) ? "<b>".$value." documents per page</b>" : $value." documents per page";
      $ret.="<tr><td><a href=\"".$q."\">".$value."</a></td></tr>";
   }
   $ret.="</table></td></tr></table>";
   return $ret;
}

function generate_lookup($column, $m_id) {
   $ret="";
   for($i=1;$i<count($column);$i++) {
      if(!strpos(strtolower(".".$column[$i]),"filter")) $ret.="<a name=\"disp_".$m_id."lookup\">".$column[$i]."</a>";
   }
  return "<div style=\"display:none;\">".$ret."</div>";
}

/* 
2010/06/14 CA

PFLICHT		app=[Name der Anwendung]			L�dt den Inhalt der Ansicht
ERG�NZEND	number=true || false				Nummeriert die Ansicht
ERG�NZEND	sortkey=[Sortierte Spalte]			�berschrift der Spalte, nicht Spaltennummer
ERG�NZEND	sortdir=asc || desc				Aufsteigende oder absteigende Sortierung
ERG�NZEND	count=[Anzahl der Dokumente]
PFLICHT		start=[Erstes angezeigtes Dokument]
ERG�NZEND	f0=[Filter]					Funktioniert �hnlich wie Kategorien!	
ERG�NZEND	f1=[Filter]					Funktioniert �hnlich wie Kategorien!	
ERG�NZEND	f2=[Filter]					Funktioniert �hnlich wie Kategorien!	
ERG�NZEND	f9=[Filter]					Readerfelder!	
ERG�NZEND	key=[lookup]					Funktioniert wie Domino-startkey	
ERG�NZEND	link=[Verlinkte Spalte]				�berschrift der Spalte, nicht Spaltennummer	
ERG�NZEND	hideline=Spalte 1, Spalte 2, ...		Versteckt Spalten (ben�tigt JS-Funktion in Notes)
ERG�NZEND	icon=true || false				Icon in Ansicht anzeigen	
ERG�NZEND	q=Searchstring
ERG�NZEND	view=Name der Ansicht					
*/

function generate_view($column, $data, $_w, $module_id, $icon, $number, $filter, $columnselector, $navigator, $addline, $basecount, $uselink, $filter_descr, $kill_sort_view, $tbl_style) {

   global $_application;
   $_c=($filter==true) ? 3 : 0;
   $m_id=$module_id."_";

   $query=urldecode($_SERVER["QUERY_STRING"]);
   if($_REQUEST[$m_id."sortkey"]!="") $sortkey=$_REQUEST[$m_id."sortkey"]; else $sortkey=$column[1+$_c];
   if($_REQUEST[$m_id."sortdir"]=="asc") $sortdir=SORT_ASC; else $sortdir=SORT_DESC;
   if($_REQUEST[$m_id."count"]!="") $count=intval($_REQUEST[$m_id."count"]); else $count=$basecount;
   if($_REQUEST[$m_id."start"]!="") $start=intval($_REQUEST[$m_id."start"]); else $start=1;
   if($_REQUEST[$m_id."f0"]!="") $f0=strtolower($_REQUEST[$m_id."f0"]); else $f0="";
   if($_REQUEST[$m_id."f1"]!="") $f1=strtolower($_REQUEST[$m_id."f1"]); else $f1="";
   if($_REQUEST[$m_id."f2"]!="") $f2=strtolower(base64_decode($_REQUEST[$m_id."f2"])); else $f2="";
   if($_REQUEST[$m_id."f9"]!="") $f9=base64_decode($_REQUEST[$m_id."f9"]); else $f9="";
   if($_REQUEST[$m_id."key"]!="") $key=strtolower($_REQUEST[$m_id."key"]); else $key="";
   if($_REQUEST[$m_id."link"]!="") $link=$_REQUEST[$m_id."link"]; else $link=$column[1+$_c];
   if($_REQUEST[$m_id."hideline"]!="") $hideline=explode(",",$_REQUEST[$m_id."hideline"]); else $hideline=false;
   if($_REQUEST[$m_id."q"]!="") $q=$_REQUEST[$m_id."q"]; else $q=false;
   if($_REQUEST[$m_id."view"]!="") $view=$_REQUEST[$m_id."view"]; else $view=false;
   if($_REQUEST[$m_id."sort"]!="") $sort_column=explode(",",$_REQUEST[$m_id."sort"]); else $sort_column=false;
   $t_template=($_REQUEST[$m_id."template"]!="") ? "&template=".$_REQUEST[$m_id."template"] : ""; 
   $t_header=($_REQUEST[$m_id."header"]!="") ? "&header=".$_REQUEST[$m_id."header"] : ""; 

   unset($sc); for($i=0;$i<999;$i++) $sc[]=$i;

   // Dynamischer Filter aktivieren
   if($_REQUEST[$m_id."filter"] != "") $d_filter = base64_decode($_REQUEST[$m_id."filter"]); else $d_filter = false;
   if(!$d_filter == false) {
      for($i=0; $i < count($data); $i++) {
         $data[$i]["filter0"] = $data[$i][$d_filter];
      }
   }

   if($sort_column!=false) for($i=0;$i<=count($sort_column);$i++) $sc[$i+1]=$sort_column[$i]; 

   // Filter anwenden, wenn aktiviert
   $i=0; while($i<count($data)) {
      $res=true;
      if($f0!="") if(strtolower($data[$i]["filter0"])!=$f0) $res=false;
      if($f1!="") if(strtolower($data[$i]["filter1"])!=$f1) $res=false;
      if($f2!="") if(strtolower($data[$i]["filter2"])!=$f2) $res=false;
      if($f9!="") if(strtolower($data[$i]["filter9"])!=$f9) $res=false;
      if($data[$i]["unid"]=="%del%") $res=false;
      if($res==true) $f_data[]=$data[$i];
      $i++;
   }

   // Suche durchf�hren, wenn Suchanfrage vorhanden
   if($q!=false) {
      $tmp_data=$f_data;
      $f_data=array();
      for($r=0;$r<=count($tmp_data);$r++) {
         $searchres=doViewSearch($q,$tmp_data[$r],$column,$hideline);
         if($searchres) $f_data[]=$tmp_data[$r];
      }
   }
   // Ansicht sortieren
   if($kill_sort_view != true || $_REQUEST[$m_id."sortkey"]!="") {
      $exit=false; $c=0; while($c<count($column) && $exit==false) {
         if($column[$c]==$sortkey) {
            $sorttype=($sort[$c]==0) ? SORT_STRING : SORT_NUMERIC;
            $exit=true;
         }
         $c++; 
      }
      foreach ($f_data as $sortcriteria) $sort_array[] = $sortcriteria[$sortkey];
      array_multisort($sort_array,$sortdir,$sorttype,$f_data);
   }

   // Lookup anwenden, wenn Lookup ausgef�hrt
   if($key!="") $start=searchFirstLine($key,$sortkey,$f_data);

   $t_view= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"navigation_bar_".$module_id."\" class=\"navigation_bar\">\r\n";
   $t_view.= "<tr>\r\n";
   //$t_view.= "<td width=\"1%\"><a href=\"addin/get_xls.php?".str_replace("&count=".$_REQUEST["count"],"",$_SERVER["QUERY_STRING"])."&count=99999\"><img src=\"../../../../library/images/icons/xls.png\" border=\"0\" style=\"border:none;margin-right:7px;margin-bottom:2px;\" /></td>";

   // Filter-Selector erstellen
   if($filter==true) {
      $t_view.= "<td width=\"18%\" style=\"background-color:#ffffff;\">";
      $t_view.= utf8_encode(generate_filter($data, "filter0", $filter_descr, $m_id));
      $t_view.= "</td>\r\n";
   }

   // Column-Selector erstellen
   if($columnselector==true) {
      $t_view.= "<td width=\"30%\" style=\"background-color:#ffffff;\">";
      $t_view.= utf8_encode(generate_simple_columnselector(count($column)-1-$_c, $count,$m_id));
      $t_view.= "</td>\r\n";
   }

   // Navigator erstellen
   if($navigator==true) {
      $t_view.= "<td align=\"right\" width=\"25%\" style=\"background-color:#ffffff;\">";
      $t_view.= utf8_encode(generate_navigation(count($f_data),$start,$count,$m_id));
      $t_view.= "</td>\r\n";
   }

   $t_view.= "</tr>\r\n";
   $t_view.= "</table>\r\n";


   // Keine Suchtreffer
   if(count($f_data)==0) {
      $t_view.= "%%ADDLINE%%\r\n<div style=\"font:normal 12px trebuchet ms, verdana;\">no documents found<br /><br /></div>";
   }

   else {

      // Lookup erstellen
      $t_view.= utf8_encode(generate_lookup($column,$m_id));
      $t_view.= "%%ADDLINE%%\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl\" style=\"".$tbl_style."\">";
      $t_view.= "<tr>";

      // HTML-Kopzeile erstellen
      $isfilter=array();
      $tc=0;
      $disp_columns=($_REQUEST[$m_id."columns"]!="") ? $_REQUEST[$m_id."columns"]+$_c : count($column)-1;

      if($number==true) $t_view.= "<th id=\"v$0\" style=\"background-color:#ffffff;width:50px;text-align:center;border-right:solid 1px rgb(99,99,99);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>Num</td></tr></table></th>\r\n";
      if($icon==true) {
         $t_view.= "<th style=\"background-color:#ffffff;width:41px;vertical-align:top;\">";
         $t_view.= "<nobr><a href=\"javascript:void(0);\" onclick=\"dsp=(document.getElementById('view_selector').style.visibility=='visible') ? 'hidden':'visible';document.getElementById('view_selector').style.visibility=dsp;document.getElementById('link_selector').style.visibility='hidden';\"><img src=\"../../../../library/images/icons/filter.png\" style=\"filter:Gray();border:none;\" /></a>";
         
        // $t_view.= "<a style=\"display:none;\" href=\"javascript:void(0);\" onclick=\"dsp=(document.getElementById('link_selector').style.visibility=='visible') ? 'hidden':'visible';document.getElementById('link_selector').style.visibility=dsp;document.getElementById('view_selector').style.visibility='hidden';\"><img src=\"../../../../library/images/icons/link.png\" style=\"filter:Gray();border:none;\" /></a></nobr></th>"; 
      }
      $t_key=false;
      for($c=4+$_c;$c<=$disp_columns+3;$c++) {
         $instr=strpos(strtolower(".".$column[$c]),"filter");
         if($instr!=1) {
            $tc++;
            $align=($sort[$sc[$tc]+3]>0) ? "right" : "left";
            $arr_up=($column[$sc[$tc]+3]==$sortkey && $sortdir==SORT_ASC && ($kill_sort_view!=true || $_REQUEST[$m_id."sortkey"]!="")) ? "../../../../library/images/arrow_up_high.png" : "../../../../library/images/arrow_up_reg.png"; $arr_up="<img src=\"".$arr_up."\" border=\"0\" style=\"display:inline;\" />";
            $arr_down=($column[$sc[$tc]+3]==$sortkey && $sortdir==SORT_DESC && ($kill_sort_view!=true || $_REQUEST[$m_id."sortkey"]!="")) ? "../../../../library/images/arrow_down_high.png" : "../../../../library/images/arrow_down_reg.png"; $arr_down="<img src=\"".$arr_down."\" border=\"0\" style=\"display:inline;\" />";
            if($column[$sc[$tc]+3]==$sortkey) $t_key=true;
            $t_view.= utf8_encode("<th id=\"v$".$tc."\" style=\"background-color:#ffffff;\">");
            if(strtoupper(substr($column[$sc[$tc]+3],0,7))!="%%HTML:") {
               $descr = (strlen($column[$sc[$tc]+3]) > 1) ? utf8_encode($column[$sc[$tc]+3]) : "";
               $t_view.= utf8_encode("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"".$align."\"><tr>");
               if($descr!="") $t_view.= utf8_encode("<td><a href=\"?".str_replace("&".$m_id."f9=".$_REQUEST[$m_id."f9"],"",str_replace("&".$m_id."start=".$_REQUEST[$m_id."start"],"&".$m_id."start=1",str_replace("&".$m_id."sortkey=".$_REQUEST[$m_id."sortkey"],"",str_replace("&".$m_id."sortdir=".$_REQUEST[$m_id."sortdir"],"",$query))))."&".$m_id."sortkey=".urlencode($column[$sc[$tc]+3])."&".$m_id."sortdir=asc\">".$arr_up."</a></td>");
               if($descr!="") $t_view.= utf8_encode("<td><a href=\"?".str_replace("&".$m_id."f9=".$_REQUEST[$m_id."f9"],"",str_replace("&".$m_id."start=".$_REQUEST[$m_id."start"],"&".$m_id."start=1",str_replace("&".$m_id."sortkey=".$_REQUEST[$m_id."sortkey"],"",str_replace("&".$m_id."sortdir=".$_REQUEST[$m_id."sortdir"],"",$query))))."&".$m_id."sortkey=".urlencode($column[$sc[$tc]+3])."&".$m_id."sortdir=desc\">".$arr_down."</a></td>");
               $t_view.= utf8_encode("<td><div style=\"white-space:nowrap;\">".$descr."</div></td></tr></table>");
            }
            $t_view.= utf8_encode("</th>\r\n");
         }
         else $isfilter[]=$c;
      }
      $t_view.= "</tr>";

      // View-Selector erstellen

      for($c=1;$c<count($column);$c++) {
         $instr=strpos(strtolower(".".$column[$c]),"filter");
         if($instr!=1) $colname[]=utf8_encode($column[$c]);
      }

      if($hideline!=false) for($i=0;$i<count($hideline);$i++) $hidecolumn[]=$column[$hideline[$i]+3];

      $border="border-bottom:solid 2px rgb(90,90,90);";
      $t_view.= "<tr id=\"view_selector\" style=\"position:absolute;visibility:hidden;\">\r\n";
      if($number==true) $t_view.= "<td></td>";
      if($icon==true) $t_view.= "<td style=\"background-color:#ffffff;filter:Alpha(opacity=94);$border\">&nbsp;</td>";
      $tmp_hl=($hideline==false) ? 0 : count($hideline);
      for($c1=0;$c1<$disp_columns-3;$c1++) {
         $t_view.= "<td style=\"font:normal 12px; vertical-align:top;white-space:nowrap;padding-top:10px;padding-left:10px;background-color:#ffffff;filter:Alpha(opacity=94);$border\">";
         for($c2=0;$c2<count($colname);$c2++) {
            //if(!in_array ($colname[$c2],$hidecolumn)) {
            $t_view.= "<a style=\"";
            if(($sc[$c1+1])==($c2+1)) $t_view.= "color:rgb(222,135,3);";
            $t_view.= "\" name=\"view_column_link_".($c1+1)."\" href=\"javascript:void(0);\" onclick=\"for(i=0;i<document.getElementsByName('view_column_link_".($c1+1)."').length;i++) document.getElementsByName('view_column_link_".($c1+1)."')[i].style.color='';this.style.color='rgb(222,135,3)';document.getElementById('view_column_".($c1+1)."').innerText='".($c2+1)."';\">";
            $t_view.= "  ".$colname[$c2]."</a><br />";
         }
         $t_col=($sort_column==false) ? ($c1+1) : $sort_column[$c1];
         $t_view.= "<a id=\"view_column_".($c1+1)."\" style=\"display:none;\">".$t_col."</a>";
         if($c1==0) {
            $query=str_replace("&".$m_id."sort=".$_REQUEST[$m_id."sort"],"",urldecode($_SERVER["QUERY_STRING"]));
            $t_view.= "<div style=\"width:100%;text-align:left;padding:7px 0px 7px 0px;\"><span class=\"phpbutton\">";
            $t_view.= "<a href=\"javascript:void(0)\" onclick=\"";
            $t_view.= "v='';";
            $t_view.= "for(i=1;i<=".count($colname).";i++) if(document.getElementById('view_column_'+String(i))) v+=document.getElementById('view_column_'+String(i)).innerText+',';";
            $t_view.= "location.href=('?$query&".$m_id."sort='+(v.substr(0,v.length-1)));\" style=\"color:#ffffff;\">Submit</a></span></div>";
         }
         $t_view.= "</td>\r\n";
      }
      $t_view.= "</tr>\r\n";

      // Link-Selector erstellen
      $border="border-bottom:solid 2px rgb(90,90,90);";
      $t_view.= "<tr id=\"link_selector\" style=\"position:absolute;visibility:hidden;\">\r\n";
      if($number==true) $t_view.= "<td></td>";
      if($icon==true) $t_view.= "<td style=\"background-color:#ffffff;filter:Alpha(opacity=94);$border\">&nbsp;</td>";
      $t_view.= "<td colspan=\"".($disp_columns-3)."\" style=\"font:normal 12px;vertical-align:top;white-space:nowrap;overflow:hidden;padding-top:10px;background-color:#ffffff;filter:Alpha(opacity=94);$border\">";
      for($c2=0;$c2<count($colname);$c2++) {
         if(!in_array (($c2+1),$hideline)) {
            $t_view.= "<a style=\"";
            if($colname[$c2]==$link) $t_view.= "color:rgb(222,135,3);";
            $t_view.= "\" name=\"link_column_".($c1+1)."\" href=\"javascript:void(0);\" onclick=\"for(i=0;i<document.getElementsByName('link_column_".($c1+1)."').length;i++) document.getElementsByName('link_column_".($c1+1)."')[i].style.color='';this.style.color='rgb(222,135,3)';document.getElementById('t_link_column').innerText=this.innerText;\">".$colname[$c2]."</a><br />";
         }
      }
      $query=str_replace("&".$m_id."link=".$_REQUEST[$m_id."link"],"",urldecode($_SERVER["QUERY_STRING"]));
      $t_view.= "<a id=\"t_link_column\" style=\"display:none;\">".$link."</a>";
      $t_view.= "<div style=\"width:100%;text-align:left;padding:7px 0px 7px 0px;\"><span class=\"phpbutton\">";
      $t_view.= "<a href=\"javascript:void(0)\" onclick=\"v=document.getElementById('t_link_column').innerText;";
      $t_view.= "location.href='?$query&".$m_id."link='+v;\" style=\"color:#ffffff;\">Submit</a></span></div>";

      $t_view.= "</td></tr>\r\n";


      // HTML-Tabelle erstellen
      if(icon==true) $disp_columns++;
      $countline=0; $r=$start-1; while($r<count($f_data) && $countline<$count) {
         $countline++;
         $t_view.= "<tr id=\"v$".$countline."\">\r\n";   
         $num=($sortdir==SORT_ASC) ? $countline+$start-1 : count($f_data)-$start-$countline+2;
         if($number==true) $t_view.= "<td id=\"v$".$countline."$0\" class=\"vnum\" align=\"center\" width=\"1%\">".number_format($num,0,",",".")."</td>\r\n";  
         if($icon==true) {
            if($uselink[0]=="use:ajax") $t_img="<a href=\"javascript:void(0)\" onclick=\"dsp=document.getElementById('v_addline_".$m_id.$countline."').parentNode.parentNode.style; if(dsp.display=='none') run_ajax('v_addline_".$m_id.$countline."','".str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1])."&id=v_addline_".$m_id.$countline."','dsp.display=\'block\''); else dsp.display='none';\"><img src=\"../../../../library/images/ico_doc.gif\" style=\"border:none;filter:Gray();\" /></a>";
            else {
               $t_link = str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1]);
               $t_link = str_replace("%%PART NUMBER%%", $f_data[$r]["Part number"], $t_link);
               $t_link = str_replace("%%TOOL NUMBER%%", $f_data[$r]["Tool number"], $t_link);
               $t_img="<a href=\"".$t_link."\"><img src=\"../../../../library/images/ico_doc.gif\" style=\"border:none;filter:Gray();\" /></a>";
            }
            $t_view.= "<td class=\"vreg\" align=\"center\" width=\"1%\">".$t_img."</td>\r\n";
         }
         $tc=1; $nolink=false; for($c=4+$_c;$c<$disp_columns+3;$c++) {
            $class=($column[$sc[$tc]+3]==$sortkey) ? "vhigh" : "vreg";
            $txt=(strpos(strtolower(".".urldecode($f_data[$r][$column[$sc[$tc]+3]])),"%%display:")) ? substr($f_data[$r][$column[$sc[$tc]+3]],strpos(strtolower(".".urldecode($f_data[$r][$column[$sc[$tc]+3]])),"%%display:")+9,strlen(urldecode($f_data[$r][$column[$sc[$tc]+3]]))) : urldecode($f_data[$r][$column[$sc[$tc]+3]]);
            $align="left";
            if($sort[$c]>0) {
               if(intval($txt)>1000) $txt=number_format($txt,$n,",",".");
               $align="right";
            } 
            if(!in_array($c,$isfilter)) {
               $tc++;
               if($column[$sc[$c]-3]==$link) {
                  $nolink=true;
                  if($uselink[0]=="use:ajax") $txt="<a href=\"javascript:void(0)\" onclick=\"dsp=document.getElementById('v_addline_".$m_id.$countline."').parentNode.parentNode.style; if(dsp.display=='none') run_ajax('v_addline_".$m_id.$countline."','".str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1])."&id=v_addline_".$m_id.$countline."','dsp.display=\'block\''); else dsp.display='none';\">".$txt."</a>";
                  else {
                     $t_link = str_replace("%%UNID%%", $f_data[$r]["unid"], $uselink[1]);
                     $t_link = str_replace("%%PART NUMBER%%", $f_data[$r]["Part number"], $t_link);
                     $t_link = str_replace("%%TOOL NUMBER%%", $f_data[$r]["Tool number"], $t_link);

                     // ************************
                     // START: Datum formatieren
                     // ************************
                     if(strtotime($txt) > 0 && !isset($_SESSION["use_date_format"])) $txt = date($_application["preferences"]["settings"]["date_format"], strtotime($txt));	
                     $t_link = strip_tags($t_link);
                     $txt="<a href=\"".$t_link."\">".$txt."</a>";
                  }
               }
               if($q!=false) $txt=getString($txt,$q,0);
               // ************************
               // START: Datum formatieren
               // ************************
//          '     if(strtotime($txt) > 0 && !isset($_SESSION["use_date_format"])) $txt = date($_application["preferences"]["settings"]["date_format"], strtotime($txt));	
               // ************************
               // ENDE: Datum formatieren
               // ************************

               if(!in_array (($c-3),$hideline)) $t_view.= utf8_encode("<td id=\"v$".$countline."$".$tc."\" class=\"".$class."\" align=\"".$align."\">".str_replace('&#x0022;','"',$txt)."&nbsp;</td>\r\n");
            }
         }
         $t_view.= "</tr>\r\n";
         if($addline==true) $t_view.= "<tr style=\"display:none;\"><td style=\"border-right:solid 1px rgb(99,99,99);border-bottom:solid 1px rgb(200,200,200);\">&nbsp;</td><td colspan=\"".($disp_columns)."\" style=\"border-bottom:solid 1px rgb(200,200,200);\"><span id=\"v_addline_".$m_id.$countline."\">&nbsp;</span></td></tr>\r\n";
         $r++;
      }
      $t_view.= "<tr><td align=\"right\" colspan=\"".($disp_columns+1-$_c)."\" style=\"background-color:#ffffff;\"><br /><a href=\"#top\" class=\"text\"><img src=\"../../../../library/images/icons/top.png\" border=\"0\" /></a></td></tr></table>\r\n<div style=\"background-color:#ffffff;\">&nbsp;</div>";
   }

   return $t_view;

}






// Neue Einstellungen f�r Supplier DB, wenn keine Werte in der URL stehen
if($link=="") $link=$column[$sc[1]];
if($sortkey=="") $sortkey=$column[$sc[1]];


















// Ab hier: generatesearch alte Funktionen...

function searchFirstLine($key,$sortkey,$f_data) {
   $ret=1;
   $exit=false;
   $i=0; while($i<count($f_data) && $exit==false) {
      $instr=strpos(strtolower(".".$f_data[$i][$sortkey]),$key);
      if($instr==1) {
         $exit=true;
         $ret=($i+1);
      }
      $i++;
   }
   return $ret;
}


function doViewSearch($searchstring,$data,$column,$hideline) {

   $searchfor=explode(" ",removeSpecialChar($searchstring));
   $globalfound=true;


   for($sf=0;$sf<count($searchfor);$sf++) {
      $notfound=true;
      for($d=3;$d<count($column);$d++) {
         if(!in_array($d-2,$hideline)) if(strpos(strtolower(" " . str_replace("&nbsp;","",removeSpecialChar($data[$column[$d]]))),strtolower($searchfor[$sf]))==true) $notfound=false;
      }
   if($notfound==true) $globalfound=false;
   }
   return $globalfound;
}


function doSearch($searchstring,$row,$field) {
   global $data;
   global $columns;
   if($_REQUEST['rtf']=="true") $rtf=0;
   else $rtf=-1;
   $searchfor=explode(" ",removeSpecialChar($searchstring));
   $globalfound=true;
   for($sf=0;$sf<count($searchfor);$sf++) {
      $found=false;
      if($field<0) { 
         for($d=0;$d<($field*-1)+$rtf;$d++) if(strpos(strtolower(" " . removeSpecialChar($data[$row][$d])),strtolower($searchfor[$sf]))==true) $found=true;
      }
      else {
         if(strpos(strtolower(" " . removeSpecialChar($data[$row][$field])),strtolower($searchfor[$sf]))==true) $found=true;
      }
      if($found==false) $globalfound=false;
   }
   return $globalfound;
}


function doAdvancedSearch($row) {
   $searchfor1=trim(str_replace("  "," ",$_REQUEST['q1']));
   $searchfor2=trim(str_replace("  "," ",$_REQUEST['q2']));
   $searchfor3=trim(str_replace("  "," ",$_REQUEST['q3']));
   $searchfor4=trim(str_replace("  "," ",$_REQUEST['q4']));
   $searchfield1=$_REQUEST['s1'];
   $searchfield2=$_REQUEST['s2'];
   $searchfield3=$_REQUEST['s3'];
   $searchfield4=$_REQUEST['s4'];

   $found=true;
   if($searchfor1!="") if(doSearch($searchfor1,$row,$searchfield1)==false) $found=false;
   if($searchfor2!="") if(doSearch($searchfor2,$row,$searchfield2)==false) $found=false;
   if($searchfor3!="") if(doSearch($searchfor3,$row,$searchfield3)==false) $found=false;
   if($searchfor4!="") if(doSearch($searchfor4,$row,$searchfield4)==false) $found=false;
   
   return $found;
   
}


function getString($string,$searchstring,$num) {
   $string=str_replace("&nbsp;"," ",$string);
   $searchfor=explode(" ",removeSpecialChar($searchstring));
   for($sf=0;$sf<count($searchfor);$sf++) {   
      $p=strpos(" ".strtolower($string),strtolower($searchfor[$sf]));
      if($p>0) {
         $tstr=substr($string,$p-1,strlen($searchfor[$sf]));
         $string=substr_replace($string,"<b>".$tstr."</b>",$p-1,strlen($searchfor[$sf]));
      }
   }
   if($num==1) {
      $nstring=substr($string,0,200);
      $nstring=substr($nstring,0,strrpos($nstring," "));
      if($string!=$nstring) $nstring.=" ...";
      return $nstring;
   } 
   else return $string;
}


function removeSpecialChar($strng) {
$rep_from = array ('�','�','�','�','�','�','�','"','-');
   $rep_to = array ('ae','ue','oe','Ue','Ue','Oe','ss','','');
   for($x=0;$x<count($rep_from);$x++) $strng=str_replace($rep_from[$x],$rep_to[$x],$strng);
   return $strng;
}


function addUnicode($strng) {
$rep_from = array ('�','�','�','�','�','�','�','"','-');
   $rep_to = array ('&auml;','&uuml','&ouml','&Auml','&Uuml;','&Ouml;','&szlig;','','-');
   for($x=0;$x<count($rep_from);$x++) $strng=str_replace($rep_from[$x],$rep_to[$x],$strng);
   return $strng;
}


function addContent($arr,$tag,$attr) { 
   global $_content;
   if($attr!="") $attr=" ".$attr;
   $txt=str_replace("%%br%%","</".$tag.">\n<".$tag.$attr.">",$_content[$arr]);
   $res="<".$tag.$attr.">".$txt."</".$tag.">";
   if($_SESSION["userroles"]) {
      if(decode($_SESSION['userroles'],0)=="supervisor") $res="<div style=\"position:absolute;\"><span style=\"position:relative;top:-5px;left:5px;adding-bottom:10px;\"><span style=\"-moz-opacity:0.8;padding:2px;border:solid 1px #666666;background-color:rgb(255,102,102);font:normal 11px verdana;\">".$arr."</span></span></div>".$res;
   }
   return $res;
}


function addStringAsContent($strng,$tag,$attr) { 
   if($attr!="") $attr=" ".$attr;
   $txt=str_replace("%%br%%","</".$tag.">\n<".$tag.$attr.">",$strng);
   $res="<".$tag.$attr.">".$txt."</".$tag.">";
   if($_SESSION["userroles"]) {
      if(decode($_SESSION['userroles'],0)=="supervisor") $res="<div style=\"position:absolute;\"><span style=\"position:relative;top:-5px;left:5px;adding-bottom:10px;\"><span style=\"-moz-opacity:0.8;padding:2px;border:solid 1px #666666;background-color:rgb(255,102,102);font:normal 11px verdana;\">".$arr."</span></span></div>".$res;
   }
   return $res;
}


?>