<?php

function create_tc_data($load) {

   include_once("../../../../library/tools/addin_xml.php");

   if(in_array("uqs", $load)) {
      $tc_data["uqs"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:uqs/".$_SESSION["pot"]."?open");
   }

   if(in_array("tracker", $load)) {
      $tc_data["tracker"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:tracker/".$_SESSION["tracker"]."?open");
   }

   if(in_array("project", $load)) {
      $tc_data["project"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:project/".$_SESSION["tracker"]."?open");
   }

   if(in_array("project:ext", $load)) {
      $tc_data["project"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:project/".$_SESSION["tracker"]."?open&add=true");
   }

   if(in_array("case", $load)) {
      $tc_data["project"]["case"] = process_xml($_SESSION["remote_domino_path_epcmain"]."/v.get_dis_by_project?open&count=9999&restricttocategory=".$_SESSION["project"]."&function=xml:data");
   }

   if(in_array("meta", $load)) {
      $tc_data["meta"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:meta/".$_SESSION["pot"]."@".$_SESSION["tracker"]."?open");
   }

   if(in_array("pot", $load)) {
      $tc_data["pot"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:pot/".$_SESSION["pot"]."?open");
   }

   if(in_array("part", $load)) {
      $tc_data["part"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.xml:part/".$tc_data["pot"]["ref_part"]."?open");
   }

   if(in_array("tool", $load)) {
      $tc_data["tool"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.xml:tool/".$tc_data["pot"]["ref_tool"]."?open");
   }

   if(in_array("basetool", $load)) {
      $tc_data["basetool"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.xml:basetool/".$tc_data["pot"]["ref_tool"]."?open");
   }

   if(in_array("prj", $load)) {
      $tc_data["prj"] = generate_xml($_SESSION["remote_domino_path_leanpd"]."/v.xml:prj/".strtolower($tc_data["tracker"]["project_number"])."?open");
   }

   if(in_array("prj_parts", $load)) {
      $tc_data["prj_parts"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.xml:prj_parts/".strtolower($tc_data["tracker"]["project_number"])."?open");
   }
   
  if(in_array("prj_tools", $load)) {
      $tc_data["prj_tools"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.xml:prj_tools/".strtolower($tc_data["tracker"]["project_number"])."?open");
   }
   
  if(in_array("cr", $load)) {
    $tc_data["cr"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.xml:cr/".$_SESSION["cr"]."?open");
  }

   return $tc_data;

}

?>