<html>
</head>
<style type="text/css">

.view_tbl td div {				display:inline;
}

.view_tbl b {					background-color:rgb(255,200,200);
}

.view_tbl .th_basic {				width:93px;
}


.hide_content {					position:absolute;
						visibility:hidden;
}


.view_tbl th {					
						vertical-align:top;
						padding-left:3px;
						padding-right:5px;
						font:normal 12px trebuchet ms,verdana;
						margin-top:0px;
}

.view_tbl a {					color:rgb(0,92,132);
						text-decoration:none;
}


.view_tbl a:hover {				
						text-decoration:underline;
}

.view_tbl {					width:100%;
}

.view_tbl th td {				font:normal 12px trebuchet ms, verdana;
						text-align:left;
						vertical-align:top;
						line-height:13px;
						padding-bottom:8px;
}

.view_tbl th img {				margin-top:3px;
}

.view_tbl td img {				
						border:none;
						margin-right:7px;
}

.view_tbl th div {				margin-left:3px;
}

.view_tbl .vnum {				font:normal 12px trebuchet ms, verdana;
						padding:2px 3px 2px 3px;
						border-bottom:solid 1px rgb(200,200,200);
						border-right:solid 1px rgb(99,99,99);
						vertical-align:top;
						line-height:14px;
}

.view_tbl .vreg {				font:normal 12px trebuchet ms, verdana;
						padding:2px 3px 2px 3px;
						border-bottom:solid 1px rgb(200,200,200);
						vertical-align:top;
						line-height:14px;
						overflow:hidden;
						white-space:nowrap;
}

.view_tbl .vhigh {				font:normal 12px trebuchet ms, verdana;
						background-color:rgb(255,241,210);
						padding:2px 3px 2px 3px;
						border-bottom:solid 1px rgb(200,200,200);
						vertical-align:top;
						line-height:14px;
						overflow:hidden;
						white-space:nowrap;
}

.view_nav #navigator, #view_filter #filter, #view_filter1 #filter1, #view_filter2 #filter2, #view_filter3 #filter3, #view_dselector #dselector, #view_columnselector #columnselector, #view_colorizer #colorizer {
						position:relative; 
						top:30px;
}

.view_nav a, #view_filter a,  #view_filter1 a, #view_filter2 a, #view_filter3 a, #view_dselector a, #view_columnselector a, #view_colorizer a {
						font:normal 12px verdana;
						text-decoration:none;
						color:rgb(66,66,66);
						white-space:nowrap;
						display:block;
}

.view_nav a:hover, #view_filter a:hover,  #view_filter1 a:hover,  #view_filter2 a:hover, #view_filter3 a:hover, #view_dselector a:hover, #view_columnselector a:hover, #view_colorizer a:hover {
						text-decoration:none;
}

.view_nav .top_nav, #view_filter #top_filter, #view_filter1 #top_filter1, #view_filter2 #top_filter2, #view_filter3 #top_filter3, #view_dselector #top_dselector, #view_columnselector #top_columnselector, #view_colorizer #top_colorizer {
						background-image:url(../../../../../library/images/arrow-list270.gif);
						background-repeat:no-repeat;
						background-position:right center;
}

.columnselector, .t_navigator {
						overflow:auto;
						background-color:rgb(255,255,255);	
						border-left:solid 1px rgb(177,177,177);	
						border-right:solid 1px rgb(99,99,99);	
						border-bottom:solid 1px rgb(99,99,99);		
						padding:3px;	
						z-index:100;	
						width:100%;
						margin-top:6px;
						filter:alpha(opacity=50);
		
}

.view_nav #navigator, #view_filter #filter, #view_filter1 #filter1, #view_filter2 #filter2,  #view_filter3 #filter3, #view_dselector #dselector, #view_colorizer #colorizer {
						height:182px;
						overflow:auto;
						background-color:rgb(255,255,255);	
						border-left:solid 1px rgb(177,177,177);	
						border-right:solid 1px rgb(99,99,99);	
						border-bottom:solid 1px rgb(99,99,99);		
						z-index:100;				
}


.view_nav #navigator {
						width:100%;
}

#view_filter #filter, #view_filter1 #filter1, #view_filter2 #filter2, #view_filter3 #filter3, #view_dselector #dselector, #view_colorizer #colorizer {
						width:300px;
}

.view_nav #navigator table, #view_filter #filter table, #view_filter1 #filter1 table, #view_filter2 #filter2 table, #view_filter3 #filter3 table, #view_dselector #dselector table, #view_columnselector #columnselector table, #view_colorizer #colorizer table {
						width:100%;
}

.view_nav #navigator a, #view_filter #filter a, #view_filter1 #filter1 a, #view_filter2 #filter2 a, #view_filter3 #filter3 a, #view_dselector #dselector a, #view_columnselector #columnselector a, #view_colorizer #colorizer a {	
						font:normal 12px verdana;
						width:100%;					
}

.view_nav #navigator a {
						padding:3px 0px 3px 0px;
						text-align:center;
}

#view_filter #filter a, #view_filter1 #filter1 a, #view_filter2 #filter2 a, #view_filter3 #filter3 a, #view_dselector #dselector a, #view_columnselector #columnselector a, #view_colorizer #colorizer a {		
						padding:3px 0px 3px 3px;
}

.view_nav #navigator a:hover, #view_filter #filter a:hover, #view_filter1 #filter1 a:hover, #view_filter2 #filter2 a:hover, #view_filter3 #filter3 a:hover, #view_dselector #dselector a:hover, #view_columnselector #columnselector a:hover, #view_colorizer #colorizer a:hover  {
						background-color:rgb(99,177,229);	
						color:rgb(255,255,255);					
}

.navigation_bar {					
						padding-bottom:3px;
						padding-top:10px;
						margin-bottom:9px;	
						width:100%;		
}

#apply_columnselector, #no_filter, #apply_colorizer, #no_filter {
				 		background-color:rgb(255,182,18);
						border-bottom:solid 2px rgb(255,255,255);		
}

.drag {
	position:absolute;
	cursor:default;
}

.hline {
	font:normal 12px trebuchet ms,verdana;
}

</style>

<script type="text/javascript">

<!-- gueltig fuer Netscape ab Version 6, Mozilla, Internet Explorer ab Version 4

//Das Objekt, das gerade bewegt wird.
var dragobjekt = null;

// Position, an der das Objekt angeklickt wurde.
var dragx = 0;
var dragy = 0;

// Mausposition
var posx = 0;
var posy = 0;

window.onerror=function(err, url, line) {
   
   return true;
}


function draginit() {
  document.onmousemove = drag;
  document.onmouseup = dragstop;
}


function dragstart(element) {
  dragobjekt = element;
  oposx = dragobjekt.offsetLeft;
  oposy = dragobjekt.offsetTop;
  dragx = posx - dragobjekt.offsetLeft;
  dragy = posy - dragobjekt.offsetTop;
  dragobjekt.style.backgroundColor="rgb(255,241,210)";
}


function dragstop() {
   x1=Number(document.getElementById("sel_x").innerText);
   x2=x1+200;
   y1=Number(document.getElementById("sel_y").innerText);
   y2=y1+300;

   if(posx>x1 && posx<x2 && posy>y1 && posy<y2) {
     add_to_list(dragobjekt.id);
   }
   else {
      dragobjekt.style.backgroundColor="rgb(255,255,255)";
   }
   dragobjekt.style.left = oposx + "px";
   dragobjekt.style.top = oposy + "px";
   dragobjekt=null;
}

function add_selected_supplier() {
   supplier_id = (document.getElementById("t_s_id").innerText);
   supplier_name = (document.getElementById("t_s_name").innerText);
   dsp = supplier_name + " [" + supplier_id + "]";
   parent.document.getElementsByName("Supplier_old")[0].value = parent.document.getElementsByName("Supplier")[0].value + "//" + parent.document.getElementsByName("Supplier_old")[0].value;
   parent.document.getElementsByName("Supplier")[0].value = dsp;
   hide_iframe();
   if(typeof parent.query_save_selected_supplier == "function") parent.query_save_selected_supplier();
}

function hide_iframe() {
   parent.document.getElementsByName("production_location")[0].selectedIndex = 0;
   parent.document.getElementsByName("ifrm")[0].style.visibility = "hidden";
}


function drag(ereignis) {
  posx = document.all ? window.event.clientX : ereignis.pageX;
  posy = document.all ? window.event.clientY : ereignis.pageY;
  if(dragobjekt != null) {
    dragobjekt.style.left = (posx - dragx) + "px";
    dragobjekt.style.top = (posy - dragy) + "px";
  }
}

function add_to_list(id) {
   if(document.getElementById("sel_to_list").innerHTML == "") {
      if(id!="") {
         if(typeof arguments[1]=="undefined") {
            for(i=0;i<document.getElementsByName("sel_to_list").length;i++) {
               if(document.getElementsByName("sel_to_list")[i].innerText==document.getElementById("content_"+id+"_1").innerText) return true;
            }
            content=document.getElementById("content_"+id+"_1").innerText;
         }
         else {
            content=arguments[1];
            if(document.getElementById(id)) document.getElementById(id).style.backgroundColor="rgb(255,241,210)";
         }

         supplier_name=(typeof s["id_"+content]=="string") ? s["id_"+content] : "unknown";
         dsp_content='<td style="width:70px;overflow:hidden;"><a id=\"t_s_id\">'+content+'</a></td><td style="white-space:nowrap;overflow:hidden;"><a id=\"t_s_name\">'+supplier_name+'</a></td></tr></table>';
         cnt='<SPAN id=sel_to_list_'+id+'><div style="margin-top:4px;margin-left:2px;font:normal 12px trebuchet ms,verdana;">';
         cnt+='<table border="0" cellspacing="0" cellpadding="0" style="font:normal 12px trebuchet ms,verdana;width:99%;table-layout:fixed;"><tr><td style="width:20px;"><a href="javascript:delete_from_list(\''+id+'\');"><img src="../../../../../library/images/icons/trash.png" style="border:none;" /></a></td>'+dsp_content+'</div><a style="display:none;" name="sel_to_list">'+content+'+unid='+id+'</a>';
         cnt+='</div></span>';
         t_html=document.getElementById("sel_to_list").innerHTML;
         document.getElementById("sel_to_list").innerHTML=t_html+cnt;
         //set_selected();
      }
   }
}

function delete_from_list(id) {
   t_html=document.getElementById("sel_to_list").innerHTML;
   t_html=t_html.substr(t_html.indexOf('id=sel_to_list_'+id)-6,t_html.length);
   t_html=t_html.substr(0,t_html.indexOf('</SPAN>')+7);
   document.getElementById("sel_to_list").innerHTML=document.getElementById("sel_to_list").innerHTML.replace(t_html,"");
   document.getElementById(id).style.backgroundColor="transparent";
   //set_selected();
}

function set_selected() {
   val="";
   for(i=1;i<document.getElementsByName("sel_to_list").length;i++) {
      val+=document.getElementsByName("sel_to_list")[i].innerText+"§§";
   }
   document.selector.t_selected.value=(val.substr(0,val.length-2));
}

function load_page(url) {
   set_selected();
   document.selector.action=url;
   document.selector.submit();
}

function refresh_selected() {
   sel=arguments[0].split("§§");
   for(i=0;i<sel.length;i++) {
      txt=sel[i].substr(0,sel[i].indexOf("+unid="));
      id=sel[i].substr(sel[i].indexOf("+unid=")+6,sel[i].length);
      add_to_list(id,txt);
   }
}

function handleNavigationBar(id) {
   dsp=(document.getElementById(id).style.display=="none") ? "block" : "none";
   if(dsp=="block") {
      lid=new Array("filter","dselector","columnselector","navigator","colorizer");
      for(i=0;i<lid.length;i++) if(document.getElementById(lid[i])) document.getElementById(lid[i]).style.display="none";
   }
   document.getElementById(id).style.display=dsp;
}

function do_lookup() {
   val=document.getElementsByName("lookup")[0].value;
   load_page(arguments[0]+"&key="+val);
}

function base64_encode(input) {  
   var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";  
   //input = escape(input);  
   var output = "";  
   var chr1, chr2, chr3 = "";  
   var enc1, enc2, enc3, enc4 = "";  
   var i = 0;  
   do {  
      chr1 = input.charCodeAt(i++);  
      chr2 = input.charCodeAt(i++);  
      chr3 = input.charCodeAt(i++);  
      enc1 = chr1 >> 2;  
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);  
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);  
      enc4 = chr3 & 63;  
      if (isNaN(chr2)) enc3 = enc4 = 64;  
      else if (isNaN(chr3)) enc4 = 64;  
      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);  
      chr1 = chr2 = chr3 = "";  
      enc1 = enc2 = enc3 = enc4 = "";  

   } while (i < input.length);  
   return output;  
} 

function do_click() {
   t_do = (parent.location.href.indexOf("creport") > 0) ? arguments[0] : true;
   return t_do;
}

</script>
<?php
print "<script type=\"text/javascript\" src=\"../javascript/get_supplier_name.js\"></script>\r\n";
?>
</head>

<?php
// check einlesen
$supplier_db = $_REQUEST["supplier_db"];
$eval=file_get_contents($supplier_db."/v.has_evaluation_report_receiver?open&count=99999");
eval($eval);




error_reporting(0);	

$_navigation=0; function generate_navigation($docs,$start,$count,$m_id) {
   global $_navigation; $_navigation++;
   $query=str_replace("&".$m_id."f9=".$_REQUEST["f9"],"",urldecode($_SERVER["QUERY_STRING"]));
   $page=(intval($start/$count)==$start/$count) ? $start/$count : intval($start/$count)+1;
   $pages=(intval($docs/$count)==$docs/$count) ? $docs/$count : intval($docs/$count)+1;

   $ret="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"view_nav\" class=\"view_nav\"><tr><td><a id=\"top_nav\" class=\"top_nav\" href=\"javascript:void(0);\" onclick=\"handleNavigationBar('t_navigator_".$_navigation."');\">Document: ".$start." / ".$docs." | Page: ".$page." / ".$pages."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>";
   $ret.="<tr><td><div id=\"t_navigator_".$_navigation."\" class=\"navigator\" style=\"display:none;position:absolute;\"><div style=\"height:194px;overflow:auto;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"t_navigator\"><tr>"; 
  
   $l=(intval($pages/7)==$pages/7) ? $pages : (intval($pages/7)+1)*7;
   for($i=0;$i<$l;$i++) {
      if(intval($i/7)==$i/7) $ret.="</tr><tr>";
      if($i<$pages) {
         $q="?".str_replace("&".$m_id."key=".$_REQUEST[$m_id."key"],"",str_replace("&".$m_id."start=".$_REQUEST[$m_id."start"],"&".$m_id."start=".($i*$count+1),$query));
         $txt=(($i+1)==$page) ? "<b>".($i+1)."</b>" : ($i+1);
         if($_REQUEST[$m_id."start"]=="") $q=$q."&".$m_id."start=".($i*$count+1);
         $ret.="<td width=\"14%\"><a href=\"javascript:load_page('".$q."');\" style=\"width:100%;height:21px;display:block;text-align:center;padding-top:3px;\" onmouseover=\"this.style.backgroundColor='rgb(99,177,229)';\" onmouseout=\"this.style.backgroundColor='rgb(255,255,255)';\">".$txt."</a></td>";
      }
      else $ret.="<td>&nbsp;</td>";
   }
   $ret.="</tr></table></div></td></tr></table></div>\r\n";
   return $ret;
}

function search_first_line($key,$sortkey,$f_data) {
   $ret=1;
   $exit=false;
   $i=0; while($i<count($f_data) && $exit==false) {
      $instr=strpos(strtolower(".".$f_data[$i][$sortkey]),$key);
      if($instr==1) {
         $exit=true;
         $ret=($i+1);
      }
      $i++;
   }
   return $ret;
}


$_start=($_REQUEST["start"]=="") ? 1 : $_REQUEST["start"];
$_count=($_REQUEST["count"]=="") ? 40 : $_REQUEST["count"];
$_key=($_REQUEST["key"]=="") ? "" : $_REQUEST["key"];


print "<body onselectstart=\"return false;\" style=\"margin:0px;\">\r\n";


// Daten einlesen
if(isset($f_data)) unset($f_data);
$app="emeasupp/epc_supplier";
$path="file:///d:/Lotus/phptemp/".$app;
$handle=opendir($path);
while($file=readdir($handle)) if(strpos($file,".php")>0) {include_once($path."/".$file);}
closedir ($handle);

//$cm=$_REQUEST["cm"]; 
//if(strtolower($_REQUEST["cf"])=="cs") {
//   for($i=0;$i<count($data);$i++) {
//      if(in_array($cm,split(",",$data[$i]["CS"]))) $f_data[]=$data[$i];
//   }
//}
//else for($i=0;$i<count($data);$i++) if($cm==$data[$i]["CM"]) $f_data[]=$data[$i];
$f_data = $data;

$show_data[]="Supplier ID";
$show_data[]="Supplier Name";
$show_data[]="Supplier Classification";
$show_data[]="Approval Status";
//$show_data[]="Supplier Status";
$show_data[]="CM";
$show_data[]="CS";


$size_col[]="85px";
$size_col[]="200px";
$size_col[]="160px";
$size_col[]="105px";
//$size_col[]="105px";
$size_col[]="40px";
$size_col[]="40px";

for($i=0;$i<count($size_col);$i++) $x=$x+str_replace("px","",$size_col[$i]); $x=$x+20;

$gadget="<div style=\"display:none;\"><div id=\"sel_x\">".($x+25)."</div><div id=\"sel_y\">51</div></div>";

// Daten sortieren
$use_sortkey=($_REQUEST["use_sortkey"]=="") ? 1 : $_REQUEST["use_sortkey"]; $sortkey=$show_data[$use_sortkey-1];
$sortdir=(strtolower($_REQUEST["use_sortdir"])=="asc") ? SORT_DESC : SORT_ASC;


$exit=false; $c=0; while($c<count($column) && $exit==false) {
   if($column[$c]==$sortkey) {
      $sorttype=($sort[$c]==0) ? SORT_STRING : SORT_NUMERIC;
      $exit=true;
   }
   $c++; 
}
foreach ($f_data as $sortcriteria) $sort_array[] = $sortcriteria[$sortkey];
array_multisort($sort_array,$sortdir,$sorttype,$f_data);

for($i=0;$i<count($f_data);$i++) {
   if(strtolower($f_data[$i]["Supplier Status"]) != "deleted") $f_tmp[] = $f_data[$i];
}
unset($f_data); $f_data = $f_tmp;

// Lookup anwenden, wenn Lookup ausgeführt
//$t_start=($_key=="") ? 1 : search_first_line($_key,$sortkey,$f_data);
$t_start = 1;
// Suche anwenden
if($_key != "") {
   unset($tmp);
   for($i = 0; $i < count($f_data); $i++) {
      $is_inarray = false;
      foreach($show_data as $val) {
         if(strpos(strtolower($f_data[$i][$val]), strtolower($_key)) > -1 && $is_inarray == false) {
            $is_inarray = true;
            $tmp[] = $f_data[$i];
         }
      }
   }
   unset($f_data); $f_data = $tmp;
}


// Supplier classifications auslesen
$t_supplier_classification = base64_decode($_REQUEST["supplier_classification"]);
$selected = (strlen($t_supplier_classification) > 1 || $t_supplier_classification == "") ? "" : " selected"; 
$supplier_classification = "<option style=\"background-color:rgb(255,255,255);\">Select classification</option><option".$selected.">All</option>\r\n";
for($i = 0; $i < count($f_data); $i++) if(!in_array($f_data[$i]["filter2"], $tmp_classification) && $f_data[$i]["filter2"]!= "") $tmp_classification[] = $f_data[$i]["filter2"];
for($i = 0; $i < count($tmp_classification); $i++) {
   $selected = ($t_supplier_classification == $tmp_classification[$i]) ? " selected" : "";
   $supplier_classification .= "<option style=\"font:normal 11px verdana;\"".$selected.">".$tmp_classification[$i]."</option>\r\n";
}
// Filter anwenden
if(strlen($t_supplier_classification) > 1) {
   for($i = 0; $i < count($f_data); $i++) if($f_data[$i]["filter2"] == $t_supplier_classification) $tmp[] = $f_data[$i];
   unset($f_data); $f_data = $tmp;
}

$img1=(strtolower($_REQUEST["cf"])=="cs") ? "select_reg.png" : "select_high.png";
$img2=(strtolower($_REQUEST["cf"])=="cs") ? "select_high.png" : "select_reg.png";



$gadget.=
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;background-color:rgb(220,220,220);border-bottom:solid 2px #ffffff;padding-left:4px;padding-top:2px;background-color:rgb(220,220,220);\">\r\n".
"<tr><td colspan=\"3\" style=\"text-align:right;background-color:rgb(0,139,184);border-bottom:solid 2px rgb(0,139,184);\"><a href=\"javascript:hide_iframe();\" style=\"font:bold 11px verdana;vertical-align:top;text-align:center;width:10px;color:rgb(255,255,255);background-color:rgb(222,135,3);border:solid 2px rgb(255,182,8);text-decoration:none;margin-right:5px;\">x</a></td></tr>\r\n".
"<tr><td style=\"font:normal 12px verdana;color:rgb(66,66,66);position:relative;top:-2px;\"></td>".
"<td>".generate_navigation(count($f_data),$_start+$t_start-1,$_count,"")."</td><td style=\"width:99%\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" value=\"".$_key."\" name=\"lookup\" onkeydown=\"if(window.event.keyCode==13) do_lookup('?use_sortkey=".$_REQUEST["use_sortkey"]."&use_sortdir=".$_REQUEST["use_sortdir"]."&cm=".$_REQUEST["cm"]."&supplier_db=".$_REQUEST["supplier_db"]."');\" style=\"font:normal 12px trebuchet ms,verdana;height:19px;width:110px;border:solid 1px rgb(99,99,99);margin-bottom:3px;margin-right:7px;\" /><a href=\"javascript:void(0);\" style=\"position:relative;top:-3px;font:normal 13px verdana;text-decoration:none;color:rgb(0,0,0);\" onclick=\"do_lookup('?use_sortkey=".$_REQUEST["use_sortkey"]."&use_sortdir=".$_REQUEST["use_sortdir"]."&cf=".$_REQUEST["cf"]."&cm=".$_REQUEST["cm"]."&supplier_db=".$_REQUEST["supplier_db"]."')\"><img src=\"../../../../../library/images/icons/search2.gif\" style=\"position:relative;top:2px;border:none;\"></a>".
"<img src=\"../../../../../library/images/icons/clear.png\" onclick=\"document.getElementsByName('lookup')[0].value='';do_lookup('?use_sortkey=".$_REQUEST["use_sortkey"]."&use_sortdir=".$_REQUEST["use_sortdir"]."&cf=".$_REQUEST["cf"]."&cm=".$_REQUEST["cm"]."&supplier_db=".$_REQUEST["supplier_db"]."');\" style=\"filter:Gray();margin-left:5px;border:none;cursor:pointer;margin-right:20px;\">".
"<select name=\"supplier_classification\" onchange=\"val = (this.selectedIndex < 2) ? '' : this[this.selectedIndex].text ;load_page('?".str_replace("&supplier_classfication=".$_REQUEST["supplier_classification"], "", $_SERVER["QUERY_STRING"])."&supplier_classification='+base64_encode(val));\" style=\"font:normal 12px verdana;border:solid 1px rgb(99,99,99);position:relative;top:-2px;width:200px;height:21px;\">".$supplier_classification."</select>".
"</td>".
"</tr></table>\r\n".
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:10px;width:".$x."px;\">\r\n".
"<tr>%%TBLHEAD%%</tr></table>\r\n".
"<div style=\"width:".$x."px;height:300px;overflow-y:scroll;float:left;margin-right:20px;padding:2px;border-bottom:solid 2px rgb(220,220,220);border-right:solid 1px rgb(220,220,220);border-left:solid 1px rgb(220,220,220);border-top:solid 2px rgb(99,99,99);\">".
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding:2px;width:".$x."px;\">\r\n".
"<tr>\r\n".
"<td style=\"vertical-align:top;\">\r\n".
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n";



$d=$t_start-1; $tblhead=""; while($d<count($f_data) && $d<$t_start+$_count) {
   if(isset($_has_evaluation_report_receiver[strtolower($f_data[$d]["Supplier ID"])])) {
      $_has_evaluation_report_receiver["img"]="../../../../../../library/images/icons/haken.png";
      $_has_evaluation_report_receiver["txt"]="Evaluation report receiver";
      $drag = "if(do_click(true) == true) dragstart(this, this.id);";
      $dblclick = "if(do_click(true) == true) {this.style.backgroundColor='rgb(255,255,255)'; add_to_list(this.id); add_selected_supplier();} else alert('Selection not possible');";
   }
   else {
      $_has_evaluation_report_receiver["img"]="../../../../../../library/images/icons/warning.gif";
      $_has_evaluation_report_receiver["txt"]="No evaluation report receiver";
      $drag = "if(do_click(false) == true) dragstart(this, this.id);";
      $dblclick = "if(do_click(false) == true) {this.style.backgroundColor='rgb(255,255,255)'; add_to_list(this.id); add_selected_supplier();} else alert('Selection not possible');";
   }



   $_has_evaluation_report_receiver_txt=(isset($_has_evaluation_report_receiver[strtolower($f_data[$d]["Supplier ID"])])) ? "../../../../../../library/images/icons/haken.png" : "../../../../../../library/images/icons/warning.gif";

   $gadget.="<tr><td style=\"width:16px;\"><img src=\"".$_has_evaluation_report_receiver["img"]."\" alt=\"".$_has_evaluation_report_receiver["txt"]."\" style=\"width:12px;height:12px;margin-bottom:8px;\" /></td><td colspan=\"".count($show_data)."\"><div class=\"drag\" ondblclick=\"".$dblclick."\" onmousedown=\"parent.set_supplier('".$f_data[$d+$_start-1]["Supplier Name"]." [".$f_data[$d+$_start-1]["Supplier ID"]."]')\" id=\"obj_".$f_data[$d]["unid"]."\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"overflow:hidden;width:".$size_table.";table-layout:fixed;font:normal 12px trebuchet ms,verdana;\"><tr>\r\n";
   $cnt=0; foreach ($show_data as $v) {
      $cnt++;
      $gadget.="<td id=\"content_obj_".$f_data[$d]["unid"]."_".($cnt)."\" style=\"width:".$size_col[$cnt-1].";\"><nobr>".$f_data[$d+$_start-1][$v]."</nobr></td>\r\n";
      if($d==$t_start) {
         $img[0]=($use_sortkey==$cnt && $sortdir==SORT_DESC) ? "arrow_up_high.png" : "arrow_up_reg.png";
         $img[1]=($use_sortkey==$cnt && $sortdir==SORT_ASC) ? "arrow_down_high.png" : "arrow_down_reg.png";
         $tblhead.=
         "<td style=\"width:".$size_col[$cnt-1].";\">".
         "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"hline\"><tr><td>".
         "<a href=\"javascript:void(0);\" onclick=\"load_page('?use_sortkey=".$cnt."&use_sortdir=asc&key=".$_REQUEST["key"]."&cm=".$_REQUEST["cm"]."&cf=".$_REQUEST["cf"]."&supplier_db=".$_REQUEST["supplier_db"]."');\"><img src=\"../../../../../library/images/".$img[0]."\" style=\"border:none;margin-right:1px;\" /></a>".
         "<a href=\"javascript:void(0);\" onclick=\"load_page('?use_sortkey=".$cnt."&use_sortdir=desc&key=".$_REQUEST["key"]."&cm=".$_REQUEST["cm"]."&cf=".$_REQUEST["cf"]."&supplier_db=".$_REQUEST["supplier_db"]."');\"><img src=\"../../../../../library/images/".$img[1]."\" style=\"border:none;margin-right:5px;\" /></a>".
         "</td><td>".$v."</td></tr></table>".
         "</td>";
      }
   }
   $d++;
   $gadget.="</tr></table></div><div style=\"height:25px;\">&nbsp;</div></td></tr>\r\n";
}
$gadget.=
"</table>\r\n".
"</div></td></tr></table></div>".
"".
"<form method=\"post\" action=\"\" name=\"selector\" style=\"display:none;\"><input style=\"width:800px\" value=\"".$_POST["t_selected"]."\" name=\"t_selected\" /></form>";


$gadget=str_replace("%%TBLHEAD%%","<td style=\"width:20px;\">&nbsp;</td>".$tblhead,$gadget);

print $gadget;


?>

</body>
</html>