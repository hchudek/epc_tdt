<?php

require_once("../../../../library/tools/addin_xml.php");
session_start();

$get = ($_REQUEST["get"] == "") ? date("Y", mktime()) : $_REQUEST["get"];
$data = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.dig_report?open&restricttocategory=".$get."&count=99999&function=plain");

if(trim($data) == "<h2>No documents found</h2>") { print trim(strip_tags($data)); die; }

$table = json_decode("[".substr($data, 0, strrpos($data, ","))."]");
foreach($table as $k => $v) {
   foreach($v as $a => $c) $table[$k][$a] = rawurldecode($c);
}

$xls = "dig_report.xlsx";
require_once("../../../../zip/create.php");


?>