<?php

error_reporting(0);

session_start();
include_once("../../../../library/tools/addin_xml.php");								// XML Library laden
include_once("../modules/Elements/PROCESS/subsprocesses.php");

$date = generate_xml($_SESSION["remote_domino_path_main"]."/a.get_date_pot_tracker?open&tracker=".base64_decode($_REQUEST["tracker"])."&pot=".base64_decode($_REQUEST["pot"])."&date=".base64_decode($_REQUEST["date"]));


if(isset($date["date"]["date_new"])) {
   $tmp = $date["date"];
   unset($date);
   $date["date"][0] = $tmp;
}
$format = base64_decode($_REQUEST["date_format"]);
$c_code = $_application["process"]["reason_codes"][base64_decode($_REQUEST["date"])];


print "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tbl_date_pot_tracker\">\r\n";
print "   <tr>\r\n";
print "      <th style=\"text-align:center;\">Rev.</th>\r\n";
print "      <th>Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>\r\n";
print "      <th>Changed date&nbsp;&nbsp;&nbsp;&nbsp;</th>\r\n";
print "      <th>Changed by</th>\r\n";
print "      <th style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"for(i=99; i>=0; i--) {d=document.getElementById('date_".strtolower(base64_decode($_REQUEST["date"]))."_' + String(i)); if(d) d.style.display='none';}\">[Close]</span>&nbsp;</th>\r\n";
print "   </tr>\r\n";


$count = 0; foreach($date["date"] as $val) {
   $rev = ($count == 0) ? "P" : "R".$count;
   $changed_by = (substr($_REQUEST["gate"],-2) == "99") ? "&nbsp;" : $val["changed_by"];
   print "   <tr>\r\n";
   print "      <td style=\"text-align:center;\">".$rev."</td>\r\n";
   print "      <td>".date($format, strtotime($val["date_new"]))."</td>\r\n";
   print "      <td>".date($format, strtotime($val["changed_date"]))."</td>\r\n";
   print "      <td>".$changed_by."&nbsp;</td>\r\n";
   if($c_code != "") {
      if($count == 0) print "      <td>&nbsp;</td>\r\n";
      else {
         $rc = ($val["reason_code"] == "none") ? create_reason_codes($c_code, strtolower($rev)) : $val["reason_code"]."&nbsp;";
         print "      <td>".$rc."</td>\r\n";
      }
   }
   else print "      <td>&nbsp;</td>\r\n";
   print "   </tr>\r\n";
   $count++;

}

print "</table>\r\n";



function create_reason_codes($c_code, $rev) {
   $unid = base64_decode($_REQUEST["unid"]);
   $date = strtolower(base64_decode($_REQUEST["date"]));
   $reason_codes = explode(":", $c_code);
   $html = "<select unid=\"".$unid."\" onchange=\"handle_save_single_field(this.unid, 'rc_".$rev."', this.value, '')\" style=\"font:normal 12px century gothic, verdana;height:21px;\">\r\n";
   foreach($reason_codes as $val) {
      $html .= "<option value=\"".$val."\">".$val."</option>\r\n";
   }
   $html .= "</select>\r\n";
   return $html;
}


?>