<?php


$include[] = "subsprocesses";
$include[] = "rules";
$include[] = "types";
foreach($include as $val) include_once("../modules/Elements/PROCESS/".$val.".php");

$colspan = (count(array_keys($_application["process"]["rules"])) + 1);

$html = 
"<div style=\"background-color:rgb(244,244,244);width:100%;padding:2px;text-align:center;vertical-align:center;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tbl_select_rules\" style=\"border:dotted 1px rgb(99,99,99);\">".
"<tr><td colspan=\"".$colspan."\" class=\"top\"><span onclick=\"close_window();\" style=\"cursor:pointer;\">[Close&nbsp;Window]</span></td></tr>".
"\r\n   <tr>\r\n%%TH%%</tr>\r\n";

switch ($_REQUEST["page"]) {
   case "rules":
      $do = false;
      $th = "      <th style=\"border:solid 1px rgb(255,255,255);\">Subprocess</th>\r\n";
      foreach(array_keys($_application["process"]["subprocess"]) as $subprocess) {
         $html .= "   <tr>\r\n";
         $style = (substr($subprocess, -2) == "99") ? "background-color:rgb(236,101,43);color:rgb(255,255,255);" : "";
         if(substr($prvsubprocess, -2) == "99") $html .= "<td style=\"border:none;\" colspan=\"".$colspan."\"><img src=\"../../../../library/images/te_orange.png\" style=\"width:100%;height:2px;\" /></td></tr><tr>";
         $html .= "      <td style=\"".$style."\">".$subprocess." (".$_application["process"]["subprocess"][$subprocess].")</td>\r\n";
         foreach(array_keys($_application["process"]["rules"]) as $key) {
            if($do == false) {
               if(strtolower($_REQUEST["selected"]) == strtolower($key)) $th .= "      <th>".$key."</th>\r\n";
               else $th .= "      <th><a href=\"javascript:void(0);\" onclick=\"document.getElementById('te_top_navigation').style.zIndex = '999999';handle_save_single_field('".$_REQUEST["meta_unid"]."', 'process_rules', '".$key."', 'location.reload()');\" style=\"font-size:12px;\">".$key."</a></th>\r\n";
            }
            $from_id = $_application["process"]["rules"][$key][$subprocess]["from_id"];
            $operation = $_application["process"]["rules"][$key][$subprocess]["operation"];
            $class = (strtolower($_REQUEST["selected"]) == strtolower($key)) ? "data_high" : "data_reg";
            $html .= "      <td class=\"".$class."\">".$from_id.str_replace("+", "<span style=\"padding:0px 7px 0px 7px;\">+</span>", $operation)."</td>\r\n";
         }
         $do = true;
         $html .= "   </tr>\r\n";
         $prvsubprocess = $subprocess;
      }
      break;

   case "type":
      $do = false;
      $th = "      <th style=\"border:solid 1px rgb(255,255,255);\">Subprocess</th>\r\n";
      foreach(array_keys($_application["process"]["subprocess"]) as $subprocess) {
         $html .= "   <tr>\r\n";
         $style = (substr($subprocess, -2) == "99") ? "background-color:rgb(236,101,43);color:rgb(255,255,255);" : "";
         if(substr($prvsubprocess, -2) == "99") $html .= "<td style=\"border:none;\" colspan=\"".$colspan."\"><img src=\"../../../../library/images/te_orange.png\" style=\"width:100%;height:2px;\" /></td></tr><tr>";
         $html .= "      <td style=\"".$style."\">".$subprocess." (".$_application["process"]["subprocess"][$subprocess].")</td>\r\n";
         foreach(array_keys($_application["process"]["type"]) as $key) {
            if($do == false) {
               if(strtolower($_REQUEST["selected"]) == strtolower($key)) $th .= "      <th><div>".$key."</div></th>\r\n";
               else $th .= "      <th><div><a href=\"javascript:void(0);\" onclick=\"document.getElementById('te_top_navigation').style.zIndex = '999999';handle_save_single_field('".$_REQUEST["meta_unid"]."', 'process_type', '".$key."', 'location.reload()');\" style=\"font-size:12px;\">".$key."</a></div></th>\r\n";
            }
            $class = (strtolower($_REQUEST["selected"]) == strtolower($key)) ? "data_high" : "data_reg";
            $bgcol = (!in_array($subprocess, $_application["process"]["type"][$key])) ? "rgb(236,101,43);" : "rgb(163,217,108);"; 
            $border = (strtolower($_REQUEST["selected"]) == strtolower($key)) ? "solid 1px  rgb(255,255,255);" : "solid 1px rgb(99,99,99);";
            $html .= "      <td class=\"".$class."\"><div><span style=\"background-color:".$bgcol."border:".$border."\">&nbsp;&nbsp;&nbsp;&nbsp;</span></div></td>\r\n";
         }
         $do = true;
         $html .= "   </tr>\r\n";
      }
      break;
}

$html .= "</table></div>";

print str_replace("%%TH%%", $th."   ", $html);

?>