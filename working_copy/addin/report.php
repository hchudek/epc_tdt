<?php

session_start();
         

// START APPLICATION -------------------------------------------------------------------------------------------------------
ob_start("ob_gzhandler");											// Komprimierung aktivieren
header("Content-Type: text/html; charset=UTF-8"); 


if(!in_array("[loop_report]", $_SESSION["remote_userroles"])) {
   print "<span style=\"font:normal 13px verdana; background:rgb(205,32,44); padding:2px 10px 2px 10px; color:rgb(255,255,255);\">Access denied</span>";
   die;
}

if(isset($_COOKIE["app"])) {
   // BASIC INCLUDES -------------------------------------------------------------------------------------------------------
   require_once("basic_php_functions.php");									// Basisfunktionen laden
   require_once("../../../../library/tools/addin_xml.php");							// XML Library laden
   // BASIC INCLUDES -------------------------------------------------------------------------------------------------------

   set_time_limit(180);												// KOMPLEXE ABFRAGEN MAX 180 SEKUNDEN
   error_reporting(0);												// KEINE FEHLER :-)
   set_error_handler("error_handler");										// ERROR HANDLER
   require_once("../session.php");										// BASIC SESSION DATA
 
}
eval(file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.report.dates?open&count=99999&function=plain"));
eval(file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.report.meta?open&count=99999&function=plain"));

foreach($date as $key => $v) {
   $phase = array_keys($v);
   foreach($v[$phase[1]] as $num => $d) {
      if(trim($d) != "") {
         $type = ($phase[0] == strtolower($phase[0])) ? "2" : "1"; 
         $line[] = array($meta[$key][0], $meta[$key][1], $meta[$key][2], $meta[$key][3], $meta[$key][4], $type, $num, ($num + 1), $d, $v[$phase[0]][$num + 1]);
      }
   }
}

/** Include PHPExcel */
$path = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "apps")));
require_once($path."library/tools/phpexcel.php");
require_once("basic_php_functions.php");


$is_basic = false;

if($is_basic) {
   $objPHPExcel = new PHPExcel();
}
else {
   $path = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "addin")));
   $objReader = PHPExcel_IOFactory::createReader("Excel2007");
   $objPHPExcel = $objReader -> load($path."templates/report.xlsx");
}

// Set document properties
$objPHPExcel->getProperties()
   -> setCreator(substr($_SESSION["domino_user"], 3, -3 + strpos($_SESSION["domino_user"], "/")))
   -> setLastModifiedBy(substr($_SESSION["domino_user"], 3, -3 + strpos($_SESSION["domino_user"], "/")))
   -> setTitle("")
   -> setSubject("")
   -> setDescription("")
   -> setKeywords("")
   -> setCategory("");


// Font
$objPHPExcel
   -> getDefaultStyle()
      -> getFont()
      -> setName("Century Gothic")
      -> setSize(10.5);

// DATA
$row = 1;
$add = ($is_basic) ? 0 : 0;

unset($filter);

$output = $line;

foreach($output as $o) {
   if($o[7] != "" && $o[8] != "") {
      $row++;
      $col = -1;
      foreach($o as $val) {
         $col++;
         $objPHPExcel -> setActiveSheetIndex(0) -> setCellValue(get_cell($row + $add, $col), $val);
         //$objPHPExcel -> getActiveSheet() -> getStyle(get_cell($row + $add, $col)) -> getAlignment() -> setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
      }
   }
}



// Autofilter
//$objPHPExcel -> getActiveSheet() -> setAutoFilter("A".($add + 1).":".get_cell($row + $add, $col));
 

// SAVE
$path = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "addin")));
$objPHPExcel -> getActiveSheet() -> setSelectedCells("A".($add + 1));
$file = date("YmdHms", mktime()).generate_uniqueid(4).".xlsx";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter -> save($path."static/temp/".$file);
print "<a id=\"_download\" href=\"".$_SESSION["remote_database_path"]."addin/download.php?file=".rawurlencode($_SESSION["remote_database_path"]."static/temp/".$file)."&filename=".rawurlencode($file)."\"></a>";
print "<script>document.getElementById(\"_download\").click(); window.setTimeout('history.back()', 999);</script>\r\n";



function get_cell($row, $n) {
    return number_to_alpha($n).$row;
}

function number_to_alpha($n) {
    $r = "";
    for($i = 1; $n >= 0 && $i < 10; $i++) {
        $r = chr(0x41 + ($n % pow(26, $i) / pow(26, $i - 1))) . $r;
        $n -= pow(26, $i);
    }
    return $r;
}

?>