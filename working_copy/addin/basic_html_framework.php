<?php

session_start();

// PHP7 COMPATIBILITY ---------------------------------------------------------------------------------------------------------
function split($n, $h) { return explode($n, $h); }
$debug = false;

// UPLOAD FILE ----------------------------------------------------------------------------------------------------------------
$_SESSION["img_root"] = "c:/inetpub/wwwroot/apps/epc/tdtracking/images/tracker_images/";
$_SESSION["allowed_types"] = "(jpg|jpeg|gif|png)";
$_SESSION["authentification"] = "1.0";										// TEST
ini_set('memory_limit','3072M');

if(!empty($_FILES)) {
   if(preg_match("/\." . $_SESSION["allowed_types"]."$/i", $_FILES["image"]["name"])) {
      $destination = $_SESSION["img_root"].$_REQUEST["unique"].substr($_FILES["image"]["name"], strrpos($_FILES["image"]["name"], ".") ,5);
      if(!move_uploaded_file($_FILES["image"]["tmp_name"], $destination)) $content = "UPLOAD FAILED";
   }
   print "<meta http-equiv=\"refresh\" content=\"0; url=".$_SESSION["php_server"].$_SERVER["REQUEST_URI"]."\">";
   die;
}
                    
// BROWSERCHECK -------------------------------------------------
if($_REQUEST["bc"] == "skip") $_SESSION["bc"] = "skip";
if($_SESSION["bc"] != "skip") {
   require_once("../../../library/addin/browserinfo.php");
   $browser = get_browserinfo();
   if($browser["shortname"] != "Chrome") {print browserinfo_kill_app($browser); die;}
}


// START APPLICATION -------------------------------------------------------------------------------------------------------
ob_start("ob_gzhandler");											// Komprimierung aktivieren
header("Content-Type: text/html; charset=UTF-8"); 

if(isset($_COOKIE["app"])) {
   // BASIC INCLUDES -------------------------------------------------------------------------------------------------------
   require_once("basic_php_functions.php");									// Basisfunktionen laden
   require_once("../../../library/tools/addin_xml.php");							// XML Library laden
   // BASIC INCLUDES -------------------------------------------------------------------------------------------------------

   set_time_limit(180);												// KOMPLEXE ABFRAGEN MAX 180 SEKUNDEN
   error_reporting(0);												// KEINE FEHLER :-)
   set_error_handler("my_error_handler");										// ERROR HANDLER
   require_once("session.php");											// BASIC SESSION DATA
   $_application = array_merge($_application, get_application());
}




if(!$_application || !isset($_COOKIE["app"]) || strtolower($_application["domino_user"]) == "anonymous") {
   include_once("session.php");
   header("Location: ".$_SESSION["remote_domino_path_perportal"]."/login?create=td&r=".rawurlencode("http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]));
   die;
}

if($_application["preferred_language"] == "") $_application["preferred_language"] = "English";
$_application["grid"]["enabled"] = ($_application["grid"]["enabled"] == "true") ? true : false;
$_application["page_id"] = trim(substr(basename($_SERVER["URL"]), 0, strlen(basename($_SERVER["URL"])) - 4));
$_SESSION["page_id"] = $_application["page_id"];
require_once("application.ini");

// EMBED SESSION -------------------------------------------------------------------------------------------------------
unset($_SESSION["redirect"]);
$_SESSION["unique_page"] = generate_uniqueid(32);
$_SESSION["remote_perportal_unid"] = $_application["remote_perportal_unid"];
$_SESSION["application_id"] = $_application["app"];
$_SESSION["domino_user"] = $_application["domino_user"];
$_SESSION["domino_user_cn"] = $_application["domino_user_cn"];
$_SESSION["remote_shortname"] = $_application["shortname"];
$_SESSION["file_path_on_server"] = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], $_application["page_id"].".php")));


if(1 == 1 || !isset($_SESSION["remote_userroles"]) || !isset($_SESSION["remote_database_name"])) {
   $get_session = generate_xml($_SESSION["remote_domino_path_main"]."/p.session.xml?open");
   $_SESSION["remote_userroles"] = explode(":", $get_session["remote_userroles"]);
   $_SESSION["remote_database_name"] = $get_session["remote_database_name"];
   unset($get_session);
}

$plugged=(!function_exists("get_userpages")) ? "" : "<img name=\"img_plugged\" src=\"../../../../library/images/16x16/communication-39.png\" alt=\"Connected (no keep alive)\" style=\"border:none;position:relative;left:-24px;\" />";
$timestamp = time();
$now = date("d.m.Y",$timestamp);


// ---------- PERSPECTIVES LADEN ---------------------------------------------------------------------------------------
unset($_SESSION["perpage"]);
$perpage_file = strtolower(str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], $_application["page_id"])))."static/perpage/".$_application["page_id"].$_REQUEST["unid"]."/".$_SESSION["remote_perportal_unid"].".php");
$include = include_once($perpage_file);

if(isset($_SESSION["edit_perpage"])) {
   $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"] = $_SESSION["edit_perpage"];
   unset($_SESSION["edit_perpage"]);
}
if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]]["active"])) $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"] = "SYSTEM_1";
if(!$include) {
   $count = 0;
   foreach($_application["page"][$_application["page_id"]]["template"]["bp"] as $key => $val) {
      $count++;
      $_SESSION["perpage"]["tab"][$_application["page_id"]]["SYSTEM_".$count] = str_replace(".", "__", implode($val, "/"));
   }
}
$per_perspectives = file_get_contents($_SESSION["file_path_on_server"]."static/perspective/".$_application["page_id"]."/".strtolower($_SESSION["remote_perportal_unid"]).".txt");
if($per_perspectives != "") {
   $per_perspectives = explode("\r\n", trim($per_perspectives));
   $count = 0;
   unset($_SESSION["perspectives"][$_application["page_id"]]);
   foreach($per_perspectives as $val) {
      $count++;
      $v = explode(";;;;", $val);
      $_SESSION["perspectives"][$_application["page_id"]][rawurldecode($v[0])] = $v[1];
      $_SESSION["perpage"]["tab"][$_application["page_id"]]["CUSTOM_".$count] = str_replace(array(";", "."), array("/", "__"), $v[1]);

   }
}
else {
   unset($_SESSION["perspectives"][$_application["page_id"]]);
}
// ---------------------------------------------------------------
unset($include);


include_once("application_exceptions.php");									// Applikationsspezifika laden

// HTML erzeugen
$_SESSION["page_id"] = $_application["page_id"];
$use_template = (isset($_SESSION["header"]["template"]) && $_SESSION["header"]["template"] != "0") ? "template_".substr("0".$_SESSION["header"]["template"], -2) : $_application["page"][$_application["page_id"]]["template"]["name"];
require_once("addin/template.php");	


include_once("../../../library/addin/html_framework.php");					// HTML TEMPLATE




// PERPAGE -------------------------------------------------------
$count = 0;
foreach($_SESSION["perpage"]["tab"][$_application["page_id"]] as $key => $val) {
   $perpage .= "<input type=\"text\" name=\"".$key."\" id=\"_".$key."\" value=\"".$val."\" />\r\n";
   if(substr($key, 0, 6) == "CUSTOM") {
      $perpage .= "<input type=\"text\" name=\"".$key."_dsp\" id=\"_".$key."_dsp\" value=\"".rawurldecode(substr($per_perspectives[$count], 0, strpos($per_perspectives[$count], ";")))."\" />\r\n";
      $count++;
   }
}
$_html = 
"<div id=\"calendar\" style=\"font:normal 13px Open Sans;position:absolute;top:0px;left:0px;display:none;z-index:999999;opacity:0.85;\"></div>\r\n".
"<form name=\"perpage\" method=\"post\" style=\"display:none;\" action=\"".base64_encode($perpage_file)."\">\r\n".$perpage."</form>\r\n".$_html;						
$html_template = create_html5_framework($_html, implode(":", $template["js"]));



// Title festlegen
$title = strtoupper($_SESSION["remote_database_name"])." | ".$_application["page"][$_application["page_id"]]["page_descr"]. " | ".date("l, d m Y");


// CSS und Javascript hinzufügen
$path_js = $_SESSION["remote_database_path"]."javascript";
$path_css = $_SESSION["php_server"]."/library/css5";



foreach(array_keys($_application["module"]["css"]) as $module) {
   if(in_array($module, $_SESSION["included"]) || $module = "te_global") {
      foreach($_application["module"]["css"][$module] as $t_css) {
         if(!in_array($t_css, $include_css)) $include_css[] = $t_css;
      }
   }
}
$css = base64_encode(implode($include_css,":"));



foreach(array_keys($_application["module"]["javascript"]) as $module) {
   if(in_array($module, $_SESSION["included"]) || $module = "te_global") {
      foreach($_application["module"]["javascript"][$module] as $t_javascript) {
         if(!in_array($t_javascript, $include_javascript)) $include_javascript[] = $t_javascript;
      }
   }
}

$session = session_id();
$javascript = "sid=".session_id()."&include=".base64_encode(implode($include_javascript,":"));


// Body Onload erstellen
$_application["module"]["body_onload"]["te_global"][] = "init_application(".(1000 * $_application["logout"]).");";
foreach(array_keys($_application["module"]["body_onload"]) as $module) {
   if(in_array($module, $_SESSION["included"]) || $module = "te_global") {
      foreach($_application["module"]["body_onload"][$module] as $t_body_onload) {
         if(!in_array($t_body_onload, $include_body_onload)) $include_body_onload[] = $t_body_onload;
      }
   }
}


$body_onload = "";
// HTML Head erstellen
foreach(array_keys($_application["module"]["html_head"]) as $module) {
   if(in_array($module, $included_modules) || $module = "te_global") {
      foreach($_application["module"]["html_head"][$module] as $t_html_head) {
         if(!in_array($t_html_head, $include_html_head)) $include_html_head[] = urldecode(str_replace("%00", "\r\n", $t_html_head));
      }
   }
}


$onload = 
"head = document.getElementsByTagName(\"head\")[0];\r\n".
"script = document.createElement(\"script\");\r\n".
"script.setAttribute(\"src\", \"".$_SESSION["php_server"]."/library/tools/javascript/global/jload.js\");\r\n".
"script.setAttribute(\"type\", \"text/javascript\");\r\n".
"head.appendChild(script);\r\n".
"window.onload = function() {\r\n".
"   ".str_replace(";;", ";", implode($include_body_onload,";\r\n   "))."\r\n".
"   if(session.page_id == \"tracker\") {\r\n".
"      script = document.createElement(\"script\");\r\n".
"      script.setAttribute(\"src\", \"modules/te_header.php?get_project_number\");\r\n".
"      script.setAttribute(\"async\", \"async\");\r\n".
"      script.setAttribute(\"onload\", \"d = document.getElementsByTagName('title')[0]; d.textContent = tdproject.number + ' | ' + d.textContent\");\r\n".
"      head.appendChild(script);\r\n".
"   }\r\n".
"   ftsearch.init();\r\n".
"}\r\n";

$html_head = (!$debug) ? "<script type=\"text/javascript\" src=\"data:text/javascript;base64,".base64_encode($onload)."\"></script>\r\n" : "LOL";
$html_head .= implode($include_html_head,"\r\n")."\r\n";


// Embedded Window erstellen - ALT
$embedded_window="";


// HTML ausgeben
$output = str_replace(explode("$!","%%PLUGGED%%$!$!%%HTML_HEAD%%$!%%PATHJS%%$!%%PATHCSS%%$!%%TITLE%%$!%%CSS%%$!%%JAVASCRIPT%%$!%%BODY_ONLOAD%%"), explode("$!","$!$plugged$!$html_head$!$path_js$!$path_css$!$title$!$css$!$javascript$!$body_onload"), $html_template);

// SKIN HUINZUFÜGEN
$_application["skin"] = (empty($_application["skin"])) ? "" : $_application["skin"];
$output = str_replace("include.css", "include".$_application["skin"].".css", $output);

// DATUM FORMATIEREN
set_date_format($output);

function set_date_format($strng) {
   global $_application;
   if(strpos($strng, "<date>") && strpos($strng, "</date>")) {
      $tmp = substr($strng, strpos($strng, "<date>") + 6, strlen($strng));
      $tmp = substr($tmp, 0, strpos($tmp, "</date>"));
      $time = strtotime($tmp) + 0;
      $date = ($time > 0) ? date($_application["preferences"]["settings"]["date_format"], $time) : $tmp;
      $res = str_replace("<date>".$tmp."</date>", $date, $strng);
      set_date_format($res);
   }
   else {
      //if(!$debug) $strng = str_replace(array("\r\n", "   "), array("", ""), $strng);
      print $strng;
   }
}


?>