<?php

session_start();
ini_set('memory_limit','3072M');
ob_start("ob_gzhandler");											// Komprimierung aktivieren
header("Content-Type: text/html; charset=UTF-8"); 

require_once("../../../../library/tools/addin_xml.php");	

$adjust = ($_REQUEST["adjust"] != "") ? $_REQUEST["adjust"] : 0;
$today = strtotime(date("Y-m-d", mktime())) + ($adjust * 7 * 86400);

$type = ($_REQUEST["type"] == "") ? "planned " : strtolower($_REQUEST["type"])." ";
$history = ($_REQUEST["history"] == "") ? -1 : -1 * $_REQUEST["history"];

$d = - 1 *(date("N", $today) - 1);


for($h = $history + 1; $h <= 0; $h++) {
   for($i = $d; $i < ($d + 7); $i++) {
      $load = date("Y-m-d", $today + ($i * 86400) + ($h * 7 * 86400));
      $get = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.date.week?open&count=99999&restricttocategory=".rawurlencode($type.$load)."&function=xml:data");
      if(!strpos($get, "No documents found")) {
         $xml[$load] = process_xml_from_string($get);
      }
   }
}


// HEADER
foreach($xml[array_keys($xml)[0]]["head"]["h"] as $val) $table[0][] = $val;

// TABLE
$count = 1;
foreach($xml as $key => $val) {
   foreach($val["task"] as $v) {
      foreach($v as $out) $table[$count][] = rawurldecode($out);
      $count++;
   }
}

if(count($table) == 0) { print "Nothing found"; die; }

// CREATE EXCEL
require_once("../../../../zip/create.php");
print "<div style=\"font:normal 14px verdana;\">";
print "<br><br>";
print "Report created for calendar week ".date("W", strtotime(array_keys($xml)[0]));
print "</div>";

?>