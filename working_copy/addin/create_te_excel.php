<?php

session_start();
ini_set('memory_limit','3072M');
error_reporting(E_ALL);
ini_set('display_errors', 1); 
/** Include PHPExcel */
$path = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "apps")));
require_once($path."library/tools/phpexcel.php");
require_once("basic_php_functions.php");

if($is_basic == "") {
   $is_basic = ($_REQUEST["xls"] == "basic") ? true : false;
}

if($is_basic) {
   $objPHPExcel = new PHPExcel();
}
else {
   $path = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "addin")));
   $objReader = PHPExcel_IOFactory::createReader("Excel2007");
   $objPHPExcel = $objReader -> load($path."templates/te_basic.xlsx");
}


// Set document properties
$objPHPExcel->getProperties()
   -> setCreator(substr($_SESSION["domino_user"], 3, -3 + strpos($_SESSION["domino_user"], "/")))
   -> setLastModifiedBy(substr($_SESSION["domino_user"], 3, -3 + strpos($_SESSION["domino_user"], "/")))
   -> setTitle("")
   -> setSubject("")
   -> setDescription("")
   -> setKeywords("")
   -> setCategory("");


// Font
$objPHPExcel
   -> getDefaultStyle()
      -> getFont()
      -> setName("Century Gothic")
      -> setSize(10.5);

// DATA
$row = 0;
$add = ($is_basic) ? 0 : 5;

unset($filter);
if($output == "") {
   eval($_POST["data"]);
   if(isset($line)) {
      $output = $line;
   }
}

if($_SESSION["page_id"] == "ee_transfer_view") {
   $type = "rawurldecode";
}



foreach($output as $o) {
   $row++;
   $col = -1;
   foreach($o as $val) {
      $val = ($type == "rawurldecode") ? rawurldecode($val) : rawurldecode(base64_decode($val));
      $col++;
      $objPHPExcel -> setActiveSheetIndex(0) -> setCellValue(get_cell($row + $add, $col), $val);
      $objPHPExcel -> getActiveSheet() -> getColumnDimension(number_to_alpha($row)) -> setAutoSize(true);
      if(!$is_basic) {
         if($sortkey == $output[0][$col]) $objPHPExcel -> getActiveSheet() -> getStyle(get_cell($row + $add, $col)) -> applyFromArray(array("fill" => array("type" => PHPExcel_Style_Fill::FILL_SOLID, "color" => array("rgb" => "FFF2D6"))));
      }
      $objPHPExcel -> getActiveSheet() -> getStyle(get_cell($row + $add, $col)) -> getAlignment() -> setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
      if($row == 1) {
         if(!$is_basic) $objPHPExcel -> getActiveSheet() -> getStyle(get_cell($row + $add, $col)) -> getFont() -> setBold(false) -> getColor()->setRGB('FFFFFF');
         if(!$is_basic) $objPHPExcel -> getActiveSheet() -> getStyle(get_cell($row + $add, $col)) -> applyFromArray(array("fill" => array("type" => PHPExcel_Style_Fill::FILL_SOLID, "color" => array("rgb" => "FFB612"))));
         if(!$is_basic) $objPHPExcel -> getActiveSheet() -> getStyle(get_cell($row + $add, $col)) -> applyFromArray(array("borders" => array("outline" => array("style" => PHPExcel_Style_Border::BORDER_THICK, "color" => array("rgb" => "FFFFFF"),),),));
         if(isset($title["comment"][$val]) && !$is_basic) {
            $objPHPExcel -> getActiveSheet() -> getComment(get_cell($row + $add, $col)) -> getText() -> createTextRun($title["comment"][$val]);
            $objPHPExcel -> getActiveSheet() -> getComment(get_cell($row + $add, $col)) -> setHeight(count($title["comment"][$val]) * 21);
            $objPHPExcel -> getActiveSheet() -> getComment(get_cell($row + $add, $col)) -> setWidth(200);
            $filter[] = $val;
         }
      }
   }
}



// Autofilter
$objPHPExcel -> getActiveSheet() -> setAutoFilter("A".($add + 1).":".get_cell($row + $add, $col));

if(!$is_basic) {
   // Grid entfernen
   $objPHPExcel -> getActiveSheet() -> setShowGridlines(false);

   // META DATA
   $dsp_filter = (isset($filter)) ? implode(", ", $filter) : "None";
   $objPHPExcel -> getActiveSheet() -> setCellValue(get_cell(2, 3), $title["meta"]["head"]);
   $objPHPExcel -> getActiveSheet() -> getStyle(get_cell(2, 3)) -> getFont() -> setSize(14);
   $objPHPExcel -> getActiveSheet() -> setCellValue(get_cell(2, 6), "Application:");
   $objPHPExcel -> getActiveSheet() -> setCellValue(get_cell(2, 7), "TD Tracking");
   $objPHPExcel -> getActiveSheet() -> setCellValue(get_cell(3, 6), "Created date:");
   $objPHPExcel -> getActiveSheet() -> setCellValue(get_cell(3, 7), date("Y-m-d H:m:s", mktime()));
   $objPHPExcel -> getActiveSheet() -> setCellValue(get_cell(4, 6), "Created by:");
   $objPHPExcel -> getActiveSheet() -> setCellValue(get_cell(4, 7), substr($_SESSION["domino_user"], 3, -3 + strpos($_SESSION["domino_user"], "/")));
}



   
// SAVE
$path = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "addin")));


$objPHPExcel -> getActiveSheet() -> setSelectedCells("A".($add + 1));
$file = date("YmdHms", mktime()).generate_uniqueid(4).".xlsx";

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter -> save($path."static/temp/".$file);


if($create_output == "GET") $create_output = $_SESSION["remote_database_path"]."addin/download.php?file=".rawurlencode($_SESSION["remote_database_path"]."static/temp/".$file)."&filename=".rawurlencode($file);
else print $_SESSION["remote_database_path"]."addin/download.php?file=".rawurlencode($_SESSION["remote_database_path"]."static/temp/".$file)."&filename=".rawurlencode($file);



function get_cell($row, $n) {
    return number_to_alpha($n).$row;
}

function number_to_alpha($n) {
    $r = "";
    for($i = 1; $n >= 0 && $i < 10; $i++) {
        $r = chr(0x41 + ($n % pow(26, $i) / pow(26, $i - 1))) . $r;
        $n -= pow(26, $i);
    }
    return $r;
}



?>
