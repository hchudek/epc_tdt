<?php


session_start();

require_once("basic_php_functions.php");

$folder[] = "static";
$folder[] = "perspective";
$folder[] = $_POST["page_id"];

$file = $_SESSION["file_path_on_server"].implode($folder, "/")."/".strtolower($_SESSION["remote_perportal_unid"]).".txt";

$p = substr($_SESSION["file_path_on_server"], 0, strlen($_SESSION["file_path_on_server"]) - 1);
foreach($folder as $val) {
   $p .= "/".$val;
   if(!file_exists($p)) mkdir($p);
}


if($_POST["perspective"] != "") {
   $data = file_get_contents($file);
   if($_POST["do"] == "true") {
      $data .= generate_uniqueid(16).":".$_POST["perspective"].";;;;".$_POST["modules"]."\r\n";
   }
   else {
      $lines = explode("\r\n", $data);
      unset($data);
      foreach($lines as $line) {
         if(trim($line) != "") {
            if($_POST["tab"] == substr($line, 0, strlen($_POST["tab"]))) {
               if($_POST["delete"] == "") $data .= $_POST["tab"].":".$_POST["perspective"].";;;;".$_POST["modules"]."\r\n";
            }
            else {
               $data .= $line."\r\n";
            }
         }
      }
   }
}

file_put_contents($file, $data);

// SET CUSTOM
unset($folder);
$folder[] = "static";
$folder[] = "perpage";
$folder[] = $_POST["page_id"];

$p = substr($_SESSION["file_path_on_server"], 0, strlen($_SESSION["file_path_on_server"]) - 1);
foreach($folder as $val) {
   $p .= "/".$val;
   if(!file_exists($p)) mkdir($p);
}

$file = $_SESSION["file_path_on_server"].implode($folder, "/")."/".strtolower($_SESSION["remote_perportal_unid"]).".php";
$_SESSION["perpage"]["tab"][$_POST["page_id"]]["active"] = $_POST["custom_id"];

$data = "<?php";
foreach($_SESSION["perpage"]["tab"][$_POST["page_id"]] as $key => $val) {
   $data .= "\r\n\$_SESSION[\"perpage\"][\"tab\"][\"".$_POST["page_id"]."\"][\"".$key."\"] = \"".$val."\";";
}
$data .= "\r\n?>";
file_put_contents($file, $data);
header ("Location: ".$_POST["r"]);

?>