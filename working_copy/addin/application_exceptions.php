<?php

$_SESSION["page_id"] = $_application["page_id"];
$_SESSION["tracker"] = $_REQUEST["unique"];
$_SESSION["pot"] = $_REQUEST["pot"];

// *********************************************
// START: NICHT GESETZTE APPLICATION PREFERENCES
// *********************************************
//$_application["module"]["body_onload"]["te_global"][] = "init_post_it()";
$include_javascript[] = "handle_form";
//$include_html_head[] = "<script type=\"text/javascript\" src=\"javascript/post_it.js\"></script>";

$_application["module"]["css"]["te_global"][] = "te.woff";
$_application["module"]["css"]["te_global"][] = "html5.te.basic.xhtml";
$_application["module"]["css"]["te_global"][] = "jquery.gridster";
$_application["module"]["css"]["te_global"][] = "tdt4.grid";
$_application["module"]["css"]["te_global"][] = "te.perspective";
$_application["module"]["css"]["te_global"][] = "te.perspective_header";
$_application["module"]["css"]["te_global"][] = "te.tools/confirm_box";
$_application["module"]["css"]["te_global"][] = "scroll";

$_application["module"]["javascript"]["te_global"][] = "init_application";
$_application["module"]["javascript"]["te_global"][] = "handle_perspective";

$include_html_head[] = "<script type=\"text/javascript\" src=\"data:text/javascript;base64,".base64_encode(file_get_contents($_SESSION["php_server"]."/apps/wiki/javascript/embed.js"))."\"></script>";
if($_application["page_id"] == "tracker") $include_html_head[] = "<script type=\"text/javascript\" src=\"../ssl/javascript/jload/json.supplier.js\"></script>";


if($_application["page_id"] == "edit_tracker") {
   $include_html_head[] = "<script type=\"text/javascript\" src=\"dd/js/ajax.js\"></script>";
   $include_html_head[] = "<script type=\"text/javascript\" src=\"dd/js/context-menu.js\"></script>";
   $include_html_head[] = "<script type=\"text/javascript\" src=\"dd/js/drag-drop-folder-tree.js\"></script>";
}



if(!isset($_application["preferences"]["settings"]["date_format"])) $_application["preferences"]["settings"]["date_format"] = "Y-m-d";
if(isset($_application["preferences"]["header"]["type"]) && !isset($_SESSION["header"]["type"])) $_SESSION["header"]["type"] = $_application["preferences"]["header"]["type"];
// ********************************************
// ENDE: NICHT GESETZTE APPLICATION PREFERENCES
// ********************************************

// ****************************
// START: TRACKERDATEN EINLESEN
// ****************************

/*

if($_application["page_id"] == "tracker" || $_application["page_id"] == "add_pot") {
   $tc_data = generate_xml($_SESSION["remote_domino_path_main"]."/v.get_xml_data/".$_REQUEST["unique"]."?open&tree=".$_REQUEST["tree"]."&pot=".$_REQUEST["pot"]."&project=".$_REQUEST["project"]);
   if(isset($tc_data["tracker"]["pot_dsp_name_tmp"])) {
      $tc_data["tracker"]["pot_dsp_name"][0] = $tc_data["tracker"]["pot_dsp_name_tmp"];
      unset($tc_data["tracker"]["pot_dsp_name_tmp"]);
   }

   $tc_data["tracker"]["tracker_descr"]["value"] = rawurldecode($tc_data["tracker"]["tracker_descr"]["value"]);
   $tc_data["pot"]["pot_comment"]["value"] = rawurldecode($tc_data["pot"]["pot_comment"]["value"]);



   if(isset($tc_data["tracker"]["ref_pot_tmp"])) {
      $tc_data["tracker"]["ref_pot"][0] = $tc_data["tracker"]["ref_pot_tmp"];
      unset($tc_data["tracker"]["ref_pot_tmp"]);
   }
   if(isset($tc_data["tracker"]["pot_tool_supplier_tmp"])) {
      $tc_data["tracker"]["pot_tool_supplier"][0] = $tc_data["tracker"]["pot_tool_supplier_tmp"];
      unset($tc_data["tracker"]["pot_tool_supplier_tmp"]);

   }

   if(count($tc_data["case"]["number"]) == 1) {
      $tmp = $tc_data["case"]["number"];
      unset($tc_data["case"]["number"]);
      $tc_data["case"]["number"][0] = $tmp;
   }

   if(count($tc_data["case"]["name"]) == 1) {
      $tmp = $tc_data["case"]["name"];
      unset($tc_data["case"]["name"]);
      $tc_data["case"]["name"][0] = $tmp;
   }

   if(count($tc_data["case"]["issued"]) == 1) {
      $tmp = $tc_data["case"]["issued"];
      unset($tc_data["case"]["issued"]);
      $tc_data["case"]["issued"][0] = $tmp;
   }

   if(count($tc_data["case"]["status"]) == 1) {
      $tmp = $tc_data["case"]["status"];
      unset($tc_data["case"]["status"]);
      $tc_data["case"]["status"][0] = $tmp;
   }

   if(count($tc_data["case"]["initiator"]) == 1) {
      $tmp = $tc_data["case"]["initiator"];
      unset($tc_data["case"]["initiator"]);
      $tc_data["case"]["initiator"][0] = $tmp;
   }

   if(count($tc_data["case"]["customer"]) == 1) {
      $tmp = $tc_data["case"]["customer"];
      unset($tc_data["case"]["customer"]);
      $tc_data["case"]["customer"][0] = $tmp;
   }

   if(count($tc_data["case"]["tpm"]) == 1) {
      $tmp = $tc_data["case"]["tpm"];
      unset($tc_data["case"]["tpm"]);
      $tc_data["case"]["tpm"][0] = $tmp;
   }

   if(count($tc_data["case"]["pe"]) == 1) {
      $tmp = $tc_data["case"]["pe"];
      unset($tc_data["case"]["pe"]);
      $tc_data["case"]["pe"][0] = $tmp;
   }
   if(count($tc_data["tracker"]["hide_pot"]) == 1) {
      $tmp = $tc_data["tracker"]["hide_pot"];
      unset($tc_data["tracker"]["hide_pot"]);
      $tc_data["tracker"]["hide_pot"][0] = $tmp;
   }
   if(count($tc_data["tracker"]["pot_status"]) == 1) {
      $tmp = $tc_data["tracker"]["pot_status"];
      unset($tc_data["tracker"]["pot_status"]);
      $tc_data["tracker"]["pot_status"][0] = $tmp;
   }
   if(count($tc_data["tracker"]["pot_status_dsp"]) == 1) {
      $tmp = $tc_data["tracker"]["pot_status_dsp"];
      unset($tc_data["tracker"]["pot_status_dsp"]);
      $tc_data["tracker"]["pot_status_dsp"][0] = $tmp;
   }

}

*/

// TRACKER NEU
if($_application["page_id"] == "tracker") {
   require_once ("load_tracker_data.php");
};




if($_REQUEST["status_filter"] == "disable") $tc_data["tracker"]["status_filter"] = "Active,Inactive";


$tc_data["base_tool"] = process_xml($_SESSION["remote_domino_path_base_tool"]."/f.base_tool?readform&tool=".$tc_data["tool"]["number"]);
if($_REQUEST["project"] != "") {
   $tc_data["project"]["case"] = process_xml($_SESSION["remote_domino_path_epcmain"]."/v.get_dis_by_project?open&count=9999&restricttocategory=".strtolower($tc_data["project"]["number"])."&function=xml:data");
}


// ***************************
// ENDE: TRACKERDATEN EINLESEN
// ***************************


// ******************************
// START: BASETOOL DATEN EINLESEN
// ******************************
if($_application["page_id"] == "edit_base_tool") {
   $bt_data = generate_xml($_SESSION["remote_domino_path_base_tool"]."/v.get_xml/".$_REQUEST["unid"]."?open");
   array_walk_recursive($bt_data, "rawurlencode_array");
}
// ******************************
// ENDE: BASETOOL DATEN EINLESEN
// ******************************



// HANDLE VIEW CACHE -------------------------------------------------------------------------------------------------------
$modules = $_application["page"][$_application["page_id"]]["template"]["add"];
foreach($modules as $module) {
   $name = explode(",", $module);
   if(strpos($name[count($name) - 1], "_view")) {
      $function = str_replace(".", "__", $name[count($name) - 1]);
      foreach($_SESSION["cache"] as $key => $val) {
         if($key != $function) unset($_SESSION["cache"][$key]);
      }
      $q = $_REQUEST[substr($function, 0, strpos($function, "__"))."_q"];
      if($q != $_SESSION["cache"][$function]["q"] || $q == "") unset($_SESSION["cache"]);
   }
}


// -------------------------------------------------------------------------------------------------------------------------

// *********************************
// START: CONFIG F�R CAPACITY REPORT
// *********************************
if($_application["page_id"] == "confcapacity" || $_application["page_id"] == "capacity") {
   $capacity_report_data = generate_xml($_SESSION["remote_domino_path_main"]."/_/".$_SESSION["report_setup"]."?open");
}

// ********************************
// ENDE: CONFIG F�R CAPACITY REPORT
// ********************************


// REPORT DATEN
if($_application["page_id"] != $_SESSION["r_page"] || ($_application["page_id"] != "ee_transfer_view" && $_application["page_id"] != "ee_report" && $_application["page_id"] != "ee_edit_pot") || $_REQUEST["recalc"] == "1") {
   unset($_SESSION["r_data"]);
   unset($_SESSION["r_column"]);
   unset($_SESSION["r_recalc"]);
}


if($_application["page_id"] != "ee_transfer_view" || $_application["page_id"] == "ee_transfer_view" && $_SERVER["QUERY_STRING"] == "" || $_application["page_id"] == "ee_report" && $_SERVER["QUERY_STRING"] == "") {
   unset($_SESSION["m_e1__ee_report_embed"]);
}


if($_application["page_id"] == "capacity") {
   $report_data = generate_xml($_SESSION["remote_domino_path_main"]."/p.get_tpm?open");
   array_walk_recursive($report_data, "rawurlencode_array");
}


if($_application["page_id"]) {
   if($handle = opendir("modules/Elements/PROCESS/")) {
      while (false !== ($file = readdir($handle))) {
        if(!in_array($file, array(".", "..")) && strpos(strtolower($file), ".php")) $include[] = $file;
      }
      closedir($handle);
   }
   foreach($include as $val) include_once("modules/Elements/PROCESS/".$val);
   foreach(array_keys($_application["process"]["type"]) As $key) {
      if($_application["process"]["prv_process"][$key] != "") {
       $new_type = array_merge($_application["process"]["type"][$_application["process"]["prv_process"][$key]], $_application["process"]["type"][$key]);
       $_application["process"]["type"][$key] = $new_type;
      }
   }
   include_once("modules/Elements/PLANTS/plants.php");
}

// NOT NEEDED ANYMORE
include_once("application_corrections.php");





              

?>