<?php

function m_g2__basetool_small($_application) {

   global $bt_data;

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $content=
   "<table style=\"width:218px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tracker_data_small\">".
   "<tr><td colspan=\"2\" style=\"vertical-align:top;padding:4px;border-bottom:solid 2px #ffffff;background-color:rgb(255,194,54);\"><strong>Tool data</strong></td></tr>".
   "<tr><td style=\"vertical-align:top;padding:6px;border-bottom:solid 2px #ffffff;\">Tool&nbsp;Number&nbsp;&nbsp;</td><td style=\"padding:6px;border-bottom:solid 2px #ffffff;\">".$bt_data["tool_base_number"]."</td></tr>".
   "<tr><td style=\"vertical-align:top;padding:6px;border-bottom:solid 2px #ffffff;\">Tool&nbsp;Type&nbsp;&nbsp;</td><td style=\"padding:6px;border-bottom:solid 2px #ffffff;\">".$bt_data["tool_type"]."&nbsp;</td></tr>".
   "<tr><td style=\"vertical-align:top;padding:6px;border-bottom:solid 2px #ffffff;\">Tool&nbsp;Technologie&nbsp;&nbsp;</td><td style=\"padding:6px;border-bottom:solid 2px #ffffff;\">".$bt_data["tool_tech"]."&nbsp;</td></tr>".
   "<tr><td style=\"vertical-align:top;padding:6px;border-bottom:solid 2px #ffffff;\">Tool&nbsp;specification&nbsp;&nbsp;</td><td style=\"padding:6px;border-bottom:solid 2px #ffffff;\">".$bt_data["tool_spec"]."&nbsp;</td></tr>".
   "<tr><td style=\"vertical-align:top;padding:6px;border-bottom:solid 2px #ffffff;\">MDP</td><td style=\"text-align:center; padding:6px;border-bottom:solid 2px #ffffff;\"><img src=\"../../../../library/images/16x16/status-11-1.png\"></td></tr>";



   $content.=
   "<tr><td style=\"vertical-align:top;padding:6px;border-bottom:solid 2px #ffffff;\">Corresponding</br>Part(s)</td><td style=\"text-align:right; padding:6px;padding-left:0px;border-bottom:solid 2px #ffffff;\">".str_replace("://:","<br>",$bt_data["part_number"]);




   $content.=
   "</td></tr></table>";


   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:220px;table-layout:fixed;margin-bottom:22px;\">\r\n".

   "   <tr>\r\n".
   "      <td class=\"module_2_content\" colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";


// print_r ($bt_data);die;
   return array($headline, $module);

}


?>