<?php


function m_v1_select_pot($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;


   // Überschrift und Modulsonderzubehör erstellen
   $img1=($_REQUEST[$_module_id."_f2"]=="") ? "status-5-1.png" : "status-5.png";
   $img2=($_REQUEST[$_module_id."_f2"]=="") ? "status-5.png" : "status-5-1.png";
   $tabs=
   "<table border=\"0\" cellspacing=\"0\"0 cellpadding=\"0\" style=\"width:100%;font:normal 12px verdana;text-transform:uppercase;\"><tr><td><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-add_POT-Add POT headline');\"> &nbsp;&nbsp;Add POT</span></td><td style=\"text-align:right;\">".
   "<a href=\"?".str_replace("&".$_module_id."_f2=".$_REQUEST[$_module_id."_f2"],"",$_SERVER["QUERY_STRING"])."&".$_module_id."_f2=".$_REQUEST["project"]."\"><img src=\"../../../../library/images/16x16/".$img1."\" style=\"position:relative;top:1px;border:none;margin-right:5px;\" /></a><span style=\"position:relative;top:-2px;\">Project POT&nbsp;&nbsp;&nbsp;</span>".
   "<a href=\"?".str_replace("&".$_module_id."_f2=".$_REQUEST[$_module_id."_f2"],"",$_SERVER["QUERY_STRING"])."\"><img src=\"../../../../library/images/16x16/".$img2."\" style=\"border:none;position:relative;top:1px;margin-right:5px;\" /></a><span style=\"position:relative;top:-2px;\">All POT&nbsp;</span>".
   "</td></table>";


   // Ansicht einbinden
   if(isset($data)) unset($data);
   $app="epc/tdtracking/pot";
   $icon="true";
   $number="true";
   $path="file:///d:/Lotus/phptemp/".$app;
   $handle=opendir($path);
   while($file=readdir($handle)) if(strpos($file,".php")>0) {include($path."/".$file);}
   closedir ($handle);
   include_once("addin/generate_view.php");


   // View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
   $_SESSION["use_date_format"] = false;
   $t_view=generate_view($column, $data, $_w, $_SESSION["module"][$_module_name], true, true, true, true, true, false, 40, array("use:html","javascript:add_pot('".$_REQUEST["unique"]."','%%UNID%%','%%PART NUMBER%%','%%TOOL NUMBER%%')"));
   unset($_SESSION["use_date_format"]);
   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_4_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_4_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr><td colspan=\"2\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
   "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
   "</form>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));  

   return $module;

}

?>