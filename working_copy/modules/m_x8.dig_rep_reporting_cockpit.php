<?php


function m_x8__dig_rep_reporting_cockpit($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------

   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_average"] == "") {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_average"] = "1";
   }

   $average[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_average"] == "1") ? "status-11" : "status-11-1";
   $average[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_average"] == "1") ? "status-11-1" : "status-11";

   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_min"] == "") {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_min"] = "0";
   }

   $min[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_min"] == "1") ? "status-11" : "status-11-1";
   $min[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_min"] == "1") ? "status-11-1" : "status-11";

   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_max"] == "") {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_max"] = "0";
   }

   $max[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_max"] == "1") ? "status-11" : "status-11-1";
   $max[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_max"] == "1") ? "status-11-1" : "status-11";

   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_target"] == "") {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_target"] = "0";
   }

   for($i = 0; $i < 20; $i++) {
      $txt = ($i == 0) ? "" : $i. " days";
      $selected = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_target"] == $i) ? " selected" : "";
      $target .= "<option value=\"".$i."\"".$selected.">".$txt."</option>\r\n";
   }


   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_adjust"] == "") {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_adjust"] = "0";
   }

   $adjust[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_adjust"] == "1") ? "status-11" : "status-11-1";
   $adjust[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_adjust"] == "1") ? "status-11-1" : "status-11";
   
   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_mdp"] == "") {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_mdp"] = "0";
   }

   $mdp[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_mdp"] == "1") ? "status-11" : "status-11-1";
   $mdp[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_mdp"] == "1") ? "status-11-1" : "status-11";

   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_type"] == "") {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_type"] = "0";
   }

   $type[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_type"] == "0") ? "status-11" : "status-11-1";
   $type[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_type"] == "0") ? "status-11-1" : "status-11";



   $module = 
   "<table>\r\n".

   "<tr>\r\n".
   "<td valign=\"top\">Chart type</td>\r\n".
   "<td colspan=\"2\">".
   "<img perpage=\"m_x8__dig_rep_reporting_cockpit_type\" src=\"../../../../library/images/16x16/".$type[0].".png\" onclick=\"m_x8.kill=true; m_x8.switch(this); m_x8.perpage(this, 0);\">RDO/Month<br>\r\n".
   "<img perpage=\"m_x8__dig_rep_reporting_cockpit_type\" src=\"../../../../library/images/16x16/".$type[1].".png\" onclick=\"m_x8.kill=true; m_x8.switch(this); m_x8.perpage(this, 1);\">1 RDO/Year</td>\r\n".
   "</tr>\r\n".

   "<tr>\r\n".
   "<td>Include average</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_average\" src=\"../../../../library/images/16x16/".$average[0].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 1);\">Yes</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_average\" src=\"../../../../library/images/16x16/".$average[1].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 0);\">No</td>\r\n".
   "</tr>\r\n".

   "<tr>\r\n".
   "<td>Include min</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_min\" src=\"../../../../library/images/16x16/".$min[0].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 1);\">Yes</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_min\" src=\"../../../../library/images/16x16/".$min[1].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 0);\">No</td>\r\n".
   "</tr>\r\n".

   "<tr>\r\n".
   "<td>Include max</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_max\" src=\"../../../../library/images/16x16/".$max[0].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 1);\">Yes</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_max\" src=\"../../../../library/images/16x16/".$max[1].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 0);\">No</td>\r\n".
   "</tr>\r\n".

   "<tr>\r\n".
   "<td>Use target</td>\r\n".
   "<td colspan=\"2\"><select perpage=\"m_x8__dig_rep_reporting_cockpit_target\" onclick=\"m_x8.perpage(this, this[this.selectedIndex].value);\">".$target."</select></td>\r\n".
   "</tr>\r\n".

   "<tr>\r\n".
   "<td>Adjust</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_adjust\" src=\"../../../../library/images/16x16/".$adjust[0].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 1);\">Yes</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_adjust\" src=\"../../../../library/images/16x16/".$adjust[1].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 0);\">No</td>\r\n".
   "</tr>\r\n".

      "<tr>\r\n".
   "<td>Selection</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_mdp\" src=\"../../../../library/images/16x16/".$mdp[0].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 1); m_x8.chkmdp();\">mdp</td>\r\n".
   "<td><img perpage=\"m_x8__dig_rep_reporting_cockpit_mdp\" src=\"../../../../library/images/16x16/".$mdp[1].".png\" onclick=\"m_x8.switch(this); m_x8.perpage(this, 0); m_x8.chkmdp()\">&ne;mdp</td>\r\n".
   "</tr>\r\n".

   "</table>\r\n".
  
   "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"location.href = (m_x8.kill) ? location.pathname : location.pathname;\">Refresh chart</a></span>\r\n";

   return array($headline, $module);
}


?>