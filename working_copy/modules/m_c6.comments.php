<?php

function m_c6__comments($_application) {
   
// Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;


   // Überschrift und Modulsonderzubehör erstellen
   $tabs="<img src=\"../../../../library/images/icons/icn_comment.gif\" style=\"border:none;position:relative;top:4px;filter:Gray();\" />&nbsp;&nbsp;Tracker comments";

   $content = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/a.show_comments?open&type=embed&ref=ref\$unique&key=".$_REQUEST["unique"]);
   $content = htmlentities_html($content);


   $module.=
   "<div style=\"background-color:rgb(255,194,54);font:bold 12px verdana;color:rgb(0,0,0);height:23px;width:1200px;padding-left:10px;margin-top:8px;margin-bottom:2px; \"><span style=\"position:relative; top:4px; cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-comment-comment headline');\">Comment Section</span></div>".
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:1200px;table-layout:fixed;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\" style=\"width:90%;\">%%TABS%%</td>\r\n".
   "      <td class=\"module_2_headline\" style=\"text-align:right;\"><img onclick=\"window.open('".$_SESSION["remote_domino_path_main"]."/f.comment?open&ref\$unique=".$_REQUEST["unique"]."&category=POT&user=".urlencode($_SESSION["domino_user"])."','comment','width=1200,height=700,resizable=no,toolbar=no,top=20,left=20,location=no,menubar=no');\" src=\"../../../../library/images/icons/add_comment.png\" style=\"border:none;position:relative;top:3px;cursor:pointer;\" />&nbsp;&nbsp;<img src=\"../../../../library/images/icons/refresh.gif\" style=\"border:none;position:relative;top:2px;cursor:pointer;\" onclick=\"window.location.reload();\" /></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\" class=\"module_2_content\" colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";


   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));


   return $module;

}
?>