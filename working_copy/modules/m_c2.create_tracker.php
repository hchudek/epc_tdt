<?php

$t_file = str_replace(array(".php", "."), array("", "__"), substr(__FILE__, 1 + strrpos(__FILE__, "\\"), strlen(__FILE__)));

if($_REQUEST[$t_file] == "get_projects") {
   session_start();
  // require_once("../../../../library/tools/addin_xml.php");							
   $data = file_get_authentificated_contents($_SESSION["remote_domino_path_leanpd"]."/v.get_projects_for_jqueryui?open&count=99999");

   print $data;
   die;
}

function m_c2__create_tracker($_application) {

   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);   

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   $function = __FUNCTION__;
   $mid = substr(__FUNCTION__, 0, strpos(__FUNCTION__, "__"));




   $module = 
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\" id=\"tbl_".$function."\">\r\n".
   "<tr>\r\n".
   "<td class=\"td1\">Project&nbsp;number</td>\r\n".
   "<td class=\"td2\"><span id=\"".__FUNCTION__."_loading\">Loading projects ....</span><input onkeyup=\"m_c2__create_tracker.clear();\" style=\"display:none;\" readonly=\"readonly\" value=\"\" id=\"".__FUNCTION__."_prj\" onsave=\"m_c2__create_tracker.get_project('%%ID%%');\"></td>\r\n".
   "</tr>".

   "<tr>\r\n".
   "<td class=\"td1\">Project&nbsp;description</td>\r\n".
   "<td class=\"td2\"><span id=\"".$function."_description\"></span></td>\r\n".
   "</tr>".
   "<tr>\r\n".
   "<td class=\"td1\">Current&nbsp;phase</td>\r\n".
   "<td class=\"td2\"><span id=\"".$function."_phase\"></span></td>\r\n".
   "</tr>".
   "<tr>\r\n".
   "<td class=\"td1\">Manufacturing&nbsp;city</td>\r\n".
   "<td class=\"td2\"><span id=\"".$function."_manufacturing_city\"></span></td>\r\n".
   "</tr>".
   "<tr>\r\n".
   "<td class=\"td1\">Date&nbsp;PLMC</td>\r\n".
   "<td class=\"td2\"><span id=\"".$function."_date_plmc\"></span></td>\r\n".
   "</tr>".
   "<tr>\r\n".
   "<td colspan=\"2\" id=\"".$function."_create_tracker\" style=\"display:none;\"><span class=\"phpbutton\" style=\"position:relative; top:16px;\"><a href=\"javascript:void(0);\">Create tracker</a></span></td>\r\n".
   "</tr>".
   "</table>\r\n";

   return array(strtoupper($headline), $module);

}


?>