<?php

if($_REQUEST["css"] != "") {
   header("Content-Type: text/css");
   $id = $_REQUEST["css"];
   print "#un".$id.", #".$id." {\r\n";
   print "	margin: 0;\r\n";
   print "    	padding: 2px 0px 0px 0px;\r\n";
   print "    	float: left;\r\n";
   print "    	margin-right: 1px;\r\n";
   print "	height:280px;\r\n";
   print "	width:140px;\r\n";
   print "	overflow-y:auto;\r\n";
   print "	overflow-x:hidden;\r\n";
   print "}\r\n";

   print "#un".$id." div, #".$id." div {\r\n";
   print "	margin:1px;\r\n";
   print "    	padding:2px;\r\n";
   print "    	font:normal 13px Open Sans;\r\n";
   print "    	width:130px;\r\n";
   print "	height:15px;\r\n";
   print "}\r\n";
}


if($_REQUEST["m_s2__my_tasks_generate_html_filter"] != "") {
   session_start();
   $id = str_replace("%", "_", rawurlencode(str_replace("@", "_", $_REQUEST["m_s2__my_tasks_generate_html_filter"])));
   require_once("../../../../library/tools/addin_xml.php");							// XML Library laden
   require_once("../../../../library/tools/view/generate_view.php");

   // GET DATA
   $substi = file_get_authentificated_contents($_SESSION["remote_domino_path_substi"]."/v.is_substi?open&restricttocategory=".rawurlencode($_SESSION["domino_user_cn"])."&count=9999&function=xml:data");
   if(!strpos($substi, "No documents found")) {
      $substi = process_xml_from_string($substi);
   }  else  {
      unset ($substi);
   }
   array_unshift($substi["owner"], "My tasks");


   $domino_user = (in_array($_SESSION["perpage"]["tab"]["mytasks"]["m_s2__my_tasks_tasks"], $substi["owner"]) && $_SESSION["perpage"]["tab"]["mytasks"]["m_s2__my_tasks_tasks"] != "My tasks") ? $_SESSION["perpage"]["tab"]["mytasks"]["m_s2__my_tasks_tasks"] : $_SESSION["domino_user_cn"];

   $load_data = ($_SESSION["application:page_id"] == "tracker") ? $domino_user.":".trim($_SESSION["tracker"]) : $domino_user;
   $process_status = $_SESSION["perpage"]["tab"][$_SESSION["application:page_id"]]["m_s2__my_tasks_generate_html_status"];
   $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.get_dates_by_responsible?open&restricttocategory=".rawurlencode($load_data.":".$process_status)."&function=plain&count=99999");

   $explode = explode(":", $file);
   for($i = 1; $i <= 1 + substr_count($explode[0], ";"); $i++) $in_view[] = $i;


   $data = get_table($file, $in_view);
   $count = 0;

   foreach($data[0] as $val) {
      if(strtolower($val) == strtolower($_REQUEST["m_s2__my_tasks_generate_html_filter"])) {
         break;
      }
      $count++;
   }
   $arr = array();
   $counter = array();

   foreach($data[1] as $v) {
      $val = $v[$count];
      //$val = substr($v[$count], 0, strrpos($v[$count], "=>"));
      if(!in_array($val, $arr) && trim($val) != "") $arr[] = $val;
      $counter[$val]++;
   }
   sort($arr);
   print "<span id=\"selected_".$id."\">\r\n";
    foreach($arr as $val) {
      print "<div>".$val." [".$counter[$val]."]</div>\r\n";
   }
   print "</span>\r\n";

}

function m_s2__my_tasks($_application) {


   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"])) {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"] = "1,2";
   }
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_tasks"])) {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_tasks"] = "My tasks";
   }

   $_SESSION["application:page_id"] = $_application["page_id"];
   $_SESSION["application:tracker"] = $_REQUEST["unique"];

   $load = array("tracker");
   require_once("addin/load_tracker_data.php");

   $tc_data = create_tc_data($load);
   // Module Headline ----------------------------------------------------------------------------------------------
   // GET DEPUTY



   $substi = file_get_authentificated_contents($_SESSION["remote_domino_path_substi"]."/v.is_substi?open&restricttocategory=".rawurlencode($_SESSION["domino_user_cn"])."&count=9999&function=xml:data");


   if(!strpos($substi, "No documents found")) {
      $substi = process_xml_from_string($substi);
   }  else  {
      unset ($substi);
   }

   array_unshift($substi["owner"], "My tasks");

   $headline = "<select onchange=\"d=document.perpage.m_s2__my_tasks_tasks;d.value=this[this.selectedIndex].text;save_perpage('', true);\">\r\n";

   foreach($substi["owner"] as $text) {
      $selected = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_tasks"] == $text) ? " selected" : "";
      if($text != "%%USER%%") $headline .= "<option".$selected.">".$text."</option>\r\n";
   }
   $headline .= "</select>\r\n";

   if($_application["page_id"] == "tracker") $headline .= " for tracker ".$tc_data["tracker"]["number"];

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";

   $content = m_s2__my_tasks_generate_html($_application, $substi["owner"]);

   return array($headline."&nbsp;&nbsp;".$content[0], $content[1]);

}


function m_s2__my_tasks_generate_html($_application, $substi) {

   require_once("../../../library/tools/view/generate_view.php");
   // CONFIG ------------------------------------------------------------------------------------------------------------------------------
   $no_filter = array("S");


   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   $function = __FUNCTION__;
   $mid = substr(__FUNCTION__, 0, strpos(__FUNCTION__, "__"));

   // Überschrift und Modulsonderzubehör erstellen
   $img1=($_REQUEST[$_module_id."_f2"]=="") ? "select_reg.png" : "select_high.png";
   $img2=($_REQUEST[$_module_id."_f2"]=="") ? "select_high.png" : "select_reg.png";
   $tabs=
   "<table border=\"0\" cellspacing=\"0\"0 cellpadding=\"0\" style=\"width:100%;font:normal 12px verdana;text-transform:uppercase;\"><tr><td>&nbsp;&nbsp;Browse tracker</td><td style=\"text-align:right;\">".
   "</td></table>";


   // Ansicht einbinden -------------------------------------------------------------------------------------------------------------------
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"] = "2,3,1,4,5,9,10";
   $in_view = explode(",", $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"]);
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"] = "1";
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] = "2";
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_status"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_status"] = "99";

   $sort_column = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"];
   $sort_dir = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? SORT_DESC : SORT_ASC;

   // HEADLINE
   $pstatus[] = array(value => "10", name => "Projected");
   $pstatus[] = array(value => "99", name => "Ongoing / Overdue");
   $pstatus[] = array(value => "40", name => "Completed");
   $pstatus[] = array(value => "all", name => "All");

   foreach($pstatus as $val) {
      $img = ($val["value"] == $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_status"]) ? "status-5" : "status-5-1";
      $headline .= "<span onclick=\"m_s2__my_tasks.filter('".$val["value"]."');\" style=\"padding-right:10px;\"><img src=\"../../../../library/images/16x16/white/".$img.".png\" style=\"cursor:pointer; width:14px;position:relative;top:2px;vertical-align:top;\">&nbsp;".$val["name"]."</span>";
   }

   // LOAD DATA --------------------------------------------------
   $domino_user = (in_array($_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_tasks"], $substi) && $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_tasks"] != "My tasks") ? $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_tasks"] : $_SESSION["domino_user_cn"];
   $load_data = ($_SESSION["application:page_id"] == "tracker") ? $domino_user.":".trim($_SESSION["tracker"]) : $domino_user;
   $process_status = $_SESSION["perpage"]["tab"][$_SESSION["application:page_id"]]["m_s2__my_tasks_generate_html_status"];
   $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.get_dates_by_responsible?open&restricttocategory=".rawurlencode($load_data.":".$process_status)."&function=plain&count=99999");
   $data = get_table($file, $in_view);


//print $_SESSION["remote_domino_path_main"]."/v.get_dates_by_responsible?open&restricttocategory=".rawurlencode($load_data.":".$_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_status"])."&function=plain&count=99999";
//die;

   foreach($data[0] as $val) {
      $key = str_replace("%", "", rawurlencode(substr($val."@", 0, strpos($val."@", "@"))));
      if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key] = "";
   }
   $count = 0;
   foreach($data[0] as $val) {
      $count++;
      $saved_filter = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];

      // EXCEPTIONS
      if(strpos("!".strtolower($val), "tracker") && $_REQUEST["tracker"] != "") $saved_filter = $_REQUEST["tracker"]."=>";
      if(strpos("!".strtolower($val), "id") && $_REQUEST["id"] != "") $saved_filter = "[".$_REQUEST["id"]."]=>";

     if($saved_filter != "") $filter[$count] = explode("</>", strtolower($saved_filter));
   }


   $data[1] = filter_view($data[1], $filter);
   $data[1] = sort_view($data[1], $data[2][$sort_column], $sort_dir);

   // ADD PAGE COL SWITCH ----------------------------------------
   $t_width = 70;
   if(implode($in_view) != "") {
      for($i = 0; $i < count($in_view); $i++) {
        $width = substr($data[3][$in_view[$i] - 1], strpos($data[3][$in_view[$i] - 1], "@") + 1, strlen($data[3][$in_view[$i] - 1]));
        $t_width += intval($width);
        $css .= "#smartsearch a:nth-child(".($i + 1).") {\r\n".
                "	width:".$width."px;\r\n".
                "	position:relative;\r\n".
                "	top:-3px;\r\n".
                "}\r\n";
        $txt = substr($data[3][$in_view[$i] - 1], 0, strpos($data[3][$in_view[$i] - 1], "@"));
        $class = ($txt != "S") ? "class=\"ui-state-default-sort\"" : "style=\"border:solid 1px rgb(220,220,220);background:rgb(247,247,247);\"";
        $added .= "<div ".$class." row=\"".$in_view[$i]."\">".$txt."</div>\r\n";
      }
   }
   else {
     $data[1] = "";
   }
   for($i = 0; $i < count($data[3]); $i++) {
      if(!in_array(($i + 1), $in_view)) {
         $not_added .= "<div class=\"ui-state-default-sort\" row=\"".($i + 1)."\">".substr($data[3][$i], 0, strpos($data[3][$i], "@"))."</div>\r\n";
      }
   }

   $module = "\r\n<style type=\"text/css\">\r\n".$css."</style>\r\n";


   // HEADLINE --------------------------------------------------
   $module .= "<span id=\"smartsearch\">\r\n".
   "<input class=\"readonly\" readonly=\"readonly\" type=\"text\" id=\"smartsearch_search\" name=\"search\" value=\"".$_POST["search"]."\" onkeydown=\"this.setAttribute('clock', Date.now());\" onkeyup=\"window.setTimeout('smartsearch.check_do_filter(\'smartsearch.body\')', 800);\" />\r\n".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/interface-32.png\" onclick=\"smartsearch.configure('".__FUNCTION__."', 'smartsearch.".__FUNCTION__.".fieldlist');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px;margin-left:4px;margin-right:7px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/text-6.png\" onclick=\"smartsearch.kill_filter('".__FUNCTION__."');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/table.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', false);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/excel-icon.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', true);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<span style=\"font:normal 13px Open Sans;\">Results:&nbsp;<a id=\"smartsearch_results\" count=\"".count($data[1])."\">".count($data[1])."&nbsp;/&nbsp;".count($data[1])."</a></span>\r\n";

   $count = 0;
   $module .= "<div style=\"white-space:no-wrap;width:".$t_width."px;overflow:hidden;\"><c id=\"smartsearch.head\" clear=\"true\" style=\"display:none; white-space:nowrap;\">\r\n";
   foreach($data[0] as $val) {
      if($val != "") {
         $id[$val] = generate_uniqueid(8);
         $img = "chevron-reg";
         if($in_view[$count] == $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"]) {
            $img = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? "chevron-desc" : "chevron-asc";
         }
         $tooltip = $_SESSION["perpage"]["tab"]["tracker_view"][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];
         $usecolor = (is_array($filter[$count + 1])) ? "rgb(205,32,44)" : "rgb(0,0,0)";
         $usedir = (is_array($filter[$count + 1])) ? "_red" : "";
         $img_filter = (in_array(substr($val, 0, strpos($val, "@")), $no_filter)) ? "" : "img:filter=\"hardware-46\" ";
         $module.= "<a smartsearch.tooltip=\"".str_replace(" ", "&nbsp;", $tooltip)."\" style=\"color:".$usecolor.";\" name=\"smartsearch.sort.usemap\" img:usedir=\"".$usedir."\" img:sortdir=\"".$img."\" smartsearch.usemap:id=\"".$id[$val]."\" module:id=\"".str_replace("_get_html", "", __FUNCTION__)."\" area:link=\"use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 2);use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 1)\" area:coords=\"2,2,9,7;11,2,18,7\" ".$img_filter."img:link=\"smartsearch.configure('".__FUNCTION__."', '".$id[$val]."'); smartsearch.embed_filter(this, '".$id[$val]."', '".$val."');\">".substr($val, 0, strpos($val, "@"))."</a>\r\n";
         $count++;
      }
   }
   $module .= "</c></div>\r\n";

   // BODY -----------------------------------------------------
   $module .= "<div id=\"smartsearch.body\" clear=\"true\" php:function=\"".__FUNCTION__."\" style=\"display:none;\">\r\n";
   $e = 0;

   foreach($data[1] as $td) {
      foreach($td as $sk => $sv) {
          if(strpos($sv,"=>") && strpos($sv,"[") && strpos($sv,"]")) {
             $use_key = $sk;
          }
      }
      $key = explode("~", str_replace(array("[", "]"), array("", ""), trim(substr($td[$use_key], strrpos($td[$use_key], "=>") + 2, strlen($td[$use_key])))));
      $drag = (in_array($key[0], array("20", "30"))) ? "draggable=\"true\" ondragstart=\"m_s2__my_tasks.drag(event);\" " : "";
      $module .= "<p ondblclick=\"m_s2__my_tasks.drop(this.id);\" oncontextmenu=\"m_s2__my_tasks.open('unique=".$key[4]."&pot=".$key[5]."')\" attr=\"".implode($key, "~")."\" ".$drag."do=\"void(0);\">";
      $e++;
      foreach($td as $val) {
         $val = explode("=>", $val);
         $module .= "<a>".$val[0]."</a>";
      }
      $module .= "</p>\r\n";
   }

   $module .= "</div>\r\n".
   "</span>\r\n";

   // FIELDLIST -----------------------------------------------
   $module .=
   "<div id=\"smartsearch.".__FUNCTION__.".fieldlist\" style=\"margin-top:7px;display:none;\"\">\r\n".
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<td style=\"position:relative;top:27px;left:-6px;\"><span class=\"phpbutton\" style=\"position:relative;top:-4px;left:4px;\"><a href=\"javascript:void(0);\" onclick=\"do_save_filter('".__FUNCTION__."');\">Apply selection</a></span></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Included</b></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Not included</b></td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_selected\" class=\"connected_sortable\">".$added."</span></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_unselected\" class=\"connected_sortable\">".$not_added."</span></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n".
   "</div>\r\n";


   // FILTER ---------------------------------------------------
   foreach($data[0] as $val) {
      if($val != "") {
         $module .= "<div id=\"".$id[$val]."\"></div>\r\n";
      }
   }

   // DROPBOX --------------------------------------------------
   $dropbox =
   "<div style=\"height:30px; vertical-align:top;\" id=\"m_s2__my_tasks_calculate\">\r\n".
   "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"m_s2__my_tasks.run_agent.do(true);\">Complete</a></span>\r\n".
   "<span class=\"phpbutton\" style=\"margin-left:4px;\"><a href=\"javascript:void(0);\" onclick=\"m_s2__my_tasks.add_all();\">Add all</a></span>\r\n".
   "</div>\r\n".
   "<div ondrop=\"m_s2__my_tasks.drop(event);\" ondragover=\"m_s2__my_tasks.allow_drop(event);\" id=\"".str_replace("_generate_html", "", __FUNCTION__)."_dropbox\"></div>\r\n".
   "<div style=\"display:none;\">\r\n";

   $rc = array_keys($_application["process"]["reason_codes"]);
   foreach($rc as $key) {
      if($_application["process"]["reason_codes"][$key] != "") {
         $dropbox .= "<a id=\"rc:".$key."\">".$_application["process"]["reason_codes"][$key]."</a>\r\n";
      }
   }
   $dropbox .= "</div>\r\n";

   $module .= "<p style=\"display:none;\" id=\"smartsearch.configure\" nest_init_view=\"".implode($id, ":")."\">smartsearch.".__FUNCTION__.".fieldlist:".implode($id, ":")."</p>";

   // EMBED MODULE ---------------------------------------------
   $module =
   "<table id=\"tbl_".str_replace("_generate_html", "", __FUNCTION__)."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n".
   "      <tr>\r\n".
   "         <td>".$dropbox."</td>\r\n".
   "         <td>".$module."</td>\r\n".
   "      </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));
   return array("<span>".$headline."<span>", $module);

}



?>