<?php

function m_x4__rdo_report($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------
   $module = m_x4__rdo_report_generate_html($_application);
 
   return array($headline, $module);

}


function m_x4__rdo_report_generate_html($_application) {

   for($m = 1; $m <= 12; $m++) {
      $use_date = date("Y-m", strtotime("-".$m." months"));
      $xml = process_xml($_SESSION["remote_domino_path_main"]."/v.rdo?open&count=9999&restricttocategory=".$use_date."&function=xml:data");
      if(isset($xml["h2"])) {
         break;
      }
      else {
         if(!isset($xml["dataset"][0])) $dataset[$use_date][0] = $xml["dataset"];
         else $dataset[$use_date] = $xml["dataset"];
      }
   }

   
   $dataset = array_reverse($dataset);
   foreach($dataset as $k => $v) {
      $labels[] = $k;
      foreach($dataset[$k] as $v) {
         if($v["delta"] > 0) {
            $data[$k][$v["rdo"]][$v["phase"]] += $v["delta"];
            if(!in_array($v["phase"], array_keys($phase))) $phase[$v["phase"]] = substr($_application["process"]["subprocess"][$v["phase"]], 0, strrpos($_application["process"]["subprocess"][$v["phase"]], ":"));
            $rdo[$v["rdo"]][] = $k;
            $reason_code  = "id_".preg_replace("/\W+/", "", rawurldecode($v["reason_code"]));
            $rc[$v["rdo"]][$k][] = rawurldecode($v["reason_code"]);
            if(!in_array(rawurldecode($v["reason_code"]), $rc_all)) $rc_all[] = rawurldecode($v["reason_code"]);
         }
      }
   }

   $reason_codes = $rc;
   sort($rc_all);


   unset($phase["id"]);
   ksort($phase);
   foreach($rdo as $k => $v) $rdo[$k] = array_values(array_unique($v));
   ksort($rdo);
   foreach($rc as $key => $val) {
      foreach($rc[$key] as $k => $v) $rc[$key][$k] = array_values(array_unique($v));
   }


   $av_datasets = array();
   foreach($phase as $key => $v) {
      $av_datasets[] = "   <av_datasets>\r\n".
      "      <name>Time series: ".$v."</name>\r\n".
      "      <id>".$key."</id>\r\n";

      foreach(array_keys($rdo) as $rd) {
         $_label = array();
         $_data = array();
         $cnt = 0;
         foreach(array_keys($dataset) as $p) {
            $_label[] = "\"".$p."\"";
            $_data[$cnt] = 0;
            foreach($dataset[$p] as $d) {
               if($d["rdo"] == $rd && $d["phase"] == $key) $_data[$cnt] = $d["delta"];
            }
            $cnt++;
         }
         $av_datasets[count($av_datasets) - 1] .="      <dataset>".base64_encode("{name: \"RDO ".$rd."\", labels: [".implode($_label, ", ") ."], data: [".implode($_data, ", ") ."]}")."</dataset>\r\n";
      }
      $av_datasets[count($av_datasets) - 1] .= "   </av_datasets>\r\n";
   }



   foreach(array_keys($rdo) as $rd) {
      $av_datasets[] = "   <av_datasets>\r\n".
      "      <name>Reason codes: RDO ".$rd."</name>\r\n".
      "      <id>rdo_".preg_replace("/\W+/", "", $rd)."</id>\r\n";

      $_label = array();
      foreach(array_keys($dataset) as $dt) {
        $_label = array();
        $_data = array();
         foreach($rc_all as $p) {
            $_label[] = "\"".$p."\"";
            $_data[] = 0;
            foreach($p as $pp) $_label[] = "\"".$pp."\"";
            foreach($reason_codes[$rd][$dt] as $rc_in) if(strtolower($rc_in) == strtolower($p)) $_data[count($_data) - 1]++;
         }
         $av_datasets[count($av_datasets) - 1] .="      <dataset>".base64_encode("{name: \"RDO ".$rd." [".$dt."]\", labels: [".implode($_label, ", ") ."], data: [".implode($_data, ", ") ."]}")."</dataset>\r\n";
      }
      $av_datasets[count($av_datasets) - 1] .= "   </av_datasets>\r\n";
   }



   $module = "\r\n".
   "<div form=\"m_x4.rdo_report:data\" style=\"display:none;\">\r\n".
   implode("\r\n", $av_datasets).
   "</div>\r\n".
   "<div anchor=\"charts\" style=\"display:none;\"></div>\r\n".
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"4\" anchor=\"menu\">\r\n".
   "   <thead>\r\n".
   "      <tr>\r\n".
   "         <td>Dataset:</td>\r\n".
   "         <td><select name=\"use_dataset\"><option></option></select></td>\r\n".
   "      </tr>\r\n".
   "   </thead>\r\n".
   "   <tbody style=\"display:none;\">\r\n".
   "      <tr>\r\n".
   "         <td>Style:</td>\r\n".
   "         <td>\r\n".
   "            <select name=\"use_style\">\r\n".
   "               <option value=\"material\">Material</option>\r\n".
   "               <option value=\"pantone\" selected>Pantone</option>\r\n".
   "            </select>\r\n".
   "         </td>\r\n".
   "      </tr>\r\n".
   "      <tr>\r\n".
   "         <td>Size:</td>\r\n".
   "         <td>\r\n".
   "            <select name=\"use_size\">\r\n".  
   "               <option value=\"300\">Extra small</option>\r\n".
   "               <option value=\"500\">Small</option>\r\n".
   "               <option value=\"700\" selected>Standard</option>\r\n".
   "               <option value=\"900\">Large</option>\r\n".
   "               <option value=\"1100\">Extra large</option>\r\n".
   "            </select>\r\n".
   "         </td>\r\n".
   "      </tr>\r\n".
   "      <tr>\r\n".
   "         <td colspan=\"2\"><br><span class=\"phpbutton\"><a style=\"color:rgb(255,255,255);\" href=\"javascript:void(0);\" onclick=\"prepare.selector.execute();\">Execute</a></span><br><br></td>\r\n".
   "      </tr>\r\n".
   "      <tr>\r\n".
   "         <td anchor=\"dragbox\" style=\"vertical-align:top;\"></td>\r\n".
   "         <td anchor=\"dropbox\" style=\"vertical-align:top;\"></td>\r\n".
   "      </tr>\r\n".
   "   </tbody>\r\n".
   "</table>\r\n";

   return $module;
}

?>
