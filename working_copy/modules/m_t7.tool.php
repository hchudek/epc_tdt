<?php

function m_t7__tool($_application) {

   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"])) {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"] = "1,2";
   }
   $_SESSION["application:page_id"] = $_application["page_id"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   //$module = m_t7__tool_generate_html($_application);
   return array($headline, $module);

}



function m_t7__tool_generate_html($_application) {

   global $tc_data;
   require_once("elements/plants/plants.php");
   //$load = array("tracker", "pot", "tool", "meta");
   //require_once($path."/addin/load_tracker_data.php");
   //$tc_data = create_tc_data($load);


   
   // EDIT / READ?
   $attr["t"] = (trim($tc_data["tool"]["transferred_location"]) == "" && (in_array("[t_location]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"]))) ? "" : " readonly=\"readonly\"";
   $attr["p"] = (trim($tc_data["tool"]["production_location"]) == "" && (in_array("[location_g3]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"]))) ? "" : " readonly=\"readonly\"";


   $t_html = "<input check=\"t_location\" type=\"\" name=\"transferred_location\" value=\"".trim(rawurldecode($tc_data["tool"]["transferred_location"]))."\"".$attr["t"].">";
   $p_html = "<input check=\"location_g3\" type=\"\" name=\"production_location\" value=\"".trim(rawurldecode($tc_data["tool"]["production_location"]))."\"".$attr["p"].">";


   $_application["page_id"] = $_SESSION["application:page_id"];
   $container = explode(",", $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"]);

   if (trim(implode($tc_data["tool"]), ",") == "") {
     $module = "Tool data not available";
      if($tc_data["pot"]["is_proposal"] == "1") $module .= "&nbsp;[PROPOSAL]";
   } 
   else {
      $dsp_calendar = (check_editable(array("[superuser]", "[procurement]"))) ? "<a href=\"javascript:load_calendar('".$unique_calendar_cevt."', '".$_SESSION["remote_database_path"]."addin/calendar2.php?&p_function=sc2&p_use_rule=0&p_id=calendar_".$unique_calendar_cevt."','');\"><img src=\"../../../../library/images/16x16/time-3.png\" style=\"border:none;margin-right:7px;vertical-align:top;position:relative;top:2px;\" /></a>" : "";
      $saplink = ($tc_data["tool"]["pono"] == "-") ? "<nobr>" : "<img src=\"../../../../library/images/16x16/white/Interface-6.png\" style=\"border:none;cursor:pointer;width:16px;position:absolute;margin-top:1px;\" onclick=\"location.href='addin/generate_sap_link.php?order=".$tc_data["tool"]["pono"]."&transaction=ME2M&field=S_EBELN-LOW';\"\" /><nobr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      $sapprlink = ($tc_data["tool"]["prno"] == "-") ? "<nobr>" : "<img src=\"../../../../library/images/16x16/white/Interface-6.png\" style=\"border:none;cursor:pointer;width:16px;position:absolute;margin-top:1px;\" onclick=\"location.href='addin/generate_sap_link.php?order=".$tc_data["tool"]["prno"]."&transaction=ME53&field=EBAN-BANFN';\"\" /><nobr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

      $ctsn = ($tc_data["tool"]["ctsn"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
      $dsp_ctsn = (check_editable(array("[superuser]", "[t_d]"))) ? "<a id=\"tool:ctsn\" href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"ctsn\">".$ctsn."</a>" : $ctsn;

      $a2p = ($tc_data["tool"]["a2p"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
      $dsp_a2p = (check_editable(array("[superuser]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"a2p\">".$a2p."</a>" : $a2p;
	  
	  $mbd = ($tc_data["tool"]["mbd"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
      $dsp_mbd = (check_editable(array("[superuser]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"mbd\">".$mbd."</a>" : $mbd;
      
	  $dsource = ($tc_data["tool"]["dualsource"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
      $dsp_dsource = (check_editable(array("[superuser]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"dualsource\">".$dsource."</a>" : $dsource;

	  $exttool = ($tc_data["tool"]["exttool"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
	  
      $dsp_exttool = (check_editable(array("[t_location]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"exttool\">".$exttool."</a>" : $exttool;
      $fotsampling = ($tc_data["tool"]["fotsampling"] == "true" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
      $dsp_fotsampling = (in_array("[sampling_editor]", $_SESSION["remote_userroles"])) ? "<a href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this, ['True', 'False']);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"fotsampling\">".$fotsampling."</a>" : $fotsampling;


      $followupsampling = ($tc_data["tool"]["followupsampling"] == "true" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
      $dsp_followupsampling = (in_array("[sampling_editor]", $_SESSION["remote_userroles"])) ? "<a href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this, ['True', 'False']);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"followupsampling\">".$followupsampling."</a>" : $followupsampling;

      $prodlocconf = ($tc_data["tool"]["prodlocconf"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\" style=\"position:relative; top:2px;\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\" style=\"position:relative; top:2px;\">";
      $dsp_prodlocconf = (in_array("[procurement]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"])) ? "<a href=\"javascript:void(0);\" onclick=\"m_t7__tool.set_checkbox(this, ['True', 'False']);\" unid=\"".$tc_data["tool"]["unid"]."\" field=\"prodlocconf\">".$prodlocconf."</a>" : $prodlocconf;



      if(check_editable(array("[superuser]", "[procurement]"))) {
        $dsp_calendar = "<a href=\"javascript:calendar.embed_big_pick_date('".$tc_data["tool"]["unid"]."', 'datesuppevt', session.remote_domino_path_epcmain, '".rawurlencode("Set confirmed EVT date")."');\">";
        if(is_array($tc_data["tool"]["datesuppevt"])) $dsp_calendar .= "<img src=\"../../../../library/images/16x16/time-3.png\" style=\"border:none;margin-right:7px;vertical-align:top;position:relative;top:2px;\" />";
        else $dsp_calendar .= "<date>".$tc_data["tool"]["datesuppevt"]."</date>";
        $dsp_calendar .= "</a>";
      }
      else {
         $dsp_calenadr = "<date>".$tc_data["tool"]["datesuppevt"]."</date>";
      }

      $img[0] = (in_array("1", $container)) ? "status-11" : "status-11-1";
      $img[1] = (in_array("2", $container)) ? "status-11" : "status-11-1";

      $module = $_application["plant"]["name"].
      "<table tpm=\"".$tc_data["tracker"]["responsible"]."\" id=\"tbl_m_t7__tool\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\" unid=\"".$tc_data["tool"]["unid"]."\">\r\n".
      "<tr>\r\n".
      "<th colspan=\"4\">".
      "<div style=\"font-weight:bold; margin:0px 0px 5px 3px; white-space:nowrap; overflow:hidden;\">".
      "<img onclick=\"m_t7__tool.handle_container(this);\" name=\"m_t7__tool_container\" src=\"../../../../library/images/16x16/".$img[0].".png\" style=\"vertical-align:bottom; margin-bottom:1px; margin-right:6px; cursor:pointer;\">Base tool data".
      "<img onclick=\"m_t7__tool.handle_container(this);\" name=\"m_t7__tool_container\" src=\"../../../../library/images/16x16/".$img[1].".png\" style=\"vertical-align:bottom; margin-bottom:1px; margin-right:6px; cursor:pointer; margin-left:20px;\">Additional tool data".
       "</div>\r\n".
      "</th>\r\n".
      "</tr>\r\n";

      if($tc_data["pot"]["is_proposal"] == "0") {
         if(in_array("1", $container)) {
            $module .=
            "<tr>\r\n".
            "<td>Base Number</td>\r\n".
            "<td colspan=\"3\" field=\"base_number\">".substr(urldecode($tc_data["tool"]["number"]),0,10)."</td>\r\n".
            "</tr>\r\n".
         //   "<tr>\r\n".
         //   "<td>Tool Supplier</td>\r\n".
          //  "<td colspan=\"3\">".rawurldecode($tc_data["base_tool"]["supplier"])."&nbsp;</td>\r\n".
        //    "</tr>\r\n".
            "<td><history field=\"a2p\" oncontextmenu=\"protocol.embed(this, {'0': 'unchecked', '1': 'checked'}); return false;\">MDP</history></td>\r\n".
            "<td>".$dsp_a2p."</td>\r\n".
            "<td><history field=\"ctsn\" oncontextmenu=\"protocol.embed(this, {'0': 'unchecked', '1': 'checked'}); return false;\">CT scan</history></td>\r\n".
            "<td>".$dsp_ctsn."</td>\r\n".
            "</tr>\r\n".
            "<tr>\r\n".
			"<td><history field=\"mbd\" oncontextmenu=\"protocol.embed(this, {'0': 'unchecked', '1': 'checked'}); return false;\">MBD</history></td>\r\n".
            "<td>".$dsp_mbd."</td>\r\n";

			if ($tc_data["tool"]["suppliernotool"] == "1"){
			$module .=
            "<td><history field=\"maintplant\" oncontextmenu=\"protocol.embed(this); return false;\">Ordering Plant</history></td>".
            "<td>".embed_input(array(), $tc_data["tool"]["maintplant"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "maintplant", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);")).
			"</tr>\r\n";
			}
			$module .=
            "<tr>\r\n".
            "<td><history field=\"oldtool\" oncontextmenu=\"protocol.embed(this); return false;\">Old tool no.</history></td>\r\n".
            "<td colspan=\"3\">".embed_input(array(), $tc_data["tool"]["oldtool"], array("check" => "oldtool", "id" => generate_uniqueid(8), "type" => "text", "name" => "oldtool", "style" => "width:306px", "onkeydown" => "validate.nest(this, 'te_part_num');", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>".
            "</tr>\r\n".
            "<tr>\r\n".
            "<td><history field=\"cavity\" oncontextmenu=\"protocol.embed(this); return false;\">No cavities</history></td>\r\n".
            "<td>".embed_input(array(), $tc_data["tool"]["cavity"], array("check" => "cavity", "id" => generate_uniqueid(8), "type" => "text", "name" => "cavity", "style" => "width:65px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>\r\n".
            "<td><history field=\"convkitno\" oncontextmenu=\"protocol.embed(this); return false;\">Total no. of Conv. Kits</history></td>\r\n".
            "<td>".embed_input(array(), $tc_data["tool"]["convkitno"], array("check" => "convkitno", "id" => generate_uniqueid(8), "type" => "text", "name" => "convkitno", "style" => "width:68px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>\r\n".
            "</tr>\r\n".
            "<tr>\r\n".
            "<td><history field=\"toolspec\" oncontextmenu=\"protocol.embed(this); return false;\">Tool specification</history></td>\r\n".
            "<td colspan=\"3\">".embed_input(array(), urldecode($tc_data["tool"]["spec"]), array("check" => "toolspec", "id" => generate_uniqueid(8), "type" => "text", "name" => "toolspec", "select" => "Full Specification;Small Series;Minimum Requirement", "style" => "width:306px", "onblur" => "if(in_array(this.value, this.getAttribute('select').split(';'))) post.save_field(session.remote_domino_path_epcmain, '".urldecode($tc_data["tool"]["unid"])."', this.name, this.value);"))."</td>\r\n".
            "</tr>\r\n";


            $t_html_del = (in_array("[t_location]", $_SESSION["remote_userroles"]) && trim($tc_data["tool"]["transferred_location"]) != "") ? "<span style=\"position:absolute;\"><img src=\"../../../../library/images/16x16/red/status-22.png\" style=\"position:relative; left:-2px; margin-top:1px; cursor:pointer;\" onclick=\"do_fade(true); post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', 'transferred_location', '', 'location.reload()')\"></span>&nbsp;&nbsp;&nbsp;&nbsp;" : "";
            $p_html_del = (in_array("[location_g3]", $_SESSION["remote_userroles"]) && trim($tc_data["tool"]["production_location"]) != "") ? "<span style=\"position:absolute;\"><img src=\"../../../../library/images/16x16/red/status-22.png\" style=\"position:relative; left:-2px; margin-top:1px; cursor:pointer;\" onclick=\"do_fade(true); post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', 'production_location', '', 'location.reload()')\"></span>&nbsp;&nbsp;&nbsp;&nbsp;" : "";

            $dsp_img = (in_array("[t_location]", $_SESSION["remote_userroles"])) ? "visible" : "hidden";

            if (rawurldecode($tc_data["pot"]["production_location"]) == "Not set" || rawurldecode($tc_data["pot"]["production_location"]) == "") {
               $module .=
               "<tr>\r\n".
               "<td>".$p_html_del."<history field=\"production_location\" oncontextmenu=\"protocol.embed(this); return false;\">Prod. location G3</history></td>".
               "<td colspan=\"3\">".$p_html."</td>\r\n".
               "</tr>\r\n";
            }
            else {
               $module .=
              "<tr>\r\n".
              "<td><history field=\"production_location\" oncontextmenu=\"protocol.embed(this); return false;\">Prod. location G3</history></td>\r\n".
              "<td colspan=\"3\">".rawurldecode($tc_data["pot"]["production_location"])."<div style=\"display:none;\">".$p_html."</div></td>\r\n".
              "</tr>\r\n";
            }

            $module .=
            "<tr>".
            "<td><history field=\"prodlocconf\" oncontextmenu=\"protocol.embed(this); return false;\">Prod. loc. conf.</td>".
            "<td colspan=\"3\">".$dsp_prodlocconf."</td>\r\n".
            "</tr>";

            if (rawurldecode($tc_data["pot"]["transferred_location"]) == "Not set"  || rawurldecode($tc_data["pot"]["transferred_location"]) == "") {
               $module .=
               "<tr>\r\n".
               "<td>".$t_html_del."<history field=\"transferred_location\" oncontextmenu=\"protocol.embed(this); return false;\">Transf. location</history></td>\r\n".
               "<td colspan=\"3\">".$t_html."</td>\r\n".
               "</tr>\r\n";
            }
            else {
	      $module .=
	      "<tr>\r\n".
   	      "<td><history field=\"transferred_location\" oncontextmenu=\"protocol.embed(this); return false;\">Transf. location</history></td>".
              "<td colspan=\"3\">".rawurldecode($tc_data["pot"]["transferred_location"])."</td>\r\n".
              "</tr>\r\n";
            }


            $tc_data["base_tool"]["scan_location"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["scan_location"]);
            $tc_data["base_tool"]["digital_report"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["digital_report"]);
            $tc_data["base_tool"]["tabulated_report"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["tabulated_report"]);

            $dsp_selected_scan_location = $tc_data["base_tool"]["scan_location"];
            if($tc_data["base_tool"]["scan_location"] == "TE" && $tc_data["base_tool"]["selected_scan_location"] != "") {
               $dsp_selected_scan_location .= " [".rawurldecode($tc_data["base_tool"]["selected_scan_location"])."]";
            }

            $dsp_selected_digital_report = $tc_data["base_tool"]["digital_report"];
            if($tc_data["base_tool"]["digital_report"] == "TE" && $tc_data["base_tool"]["selected_digital_report"] != "") {
               $dsp_selected_digital_report .= " [".rawurldecode($tc_data["base_tool"]["selected_digital_report"])."]";
            }

            $dsp_selected_tabulated_report = $tc_data["base_tool"]["tabulated_report"];
            if($tc_data["base_tool"]["tabulated_report"] == "TE" && $tc_data["base_tool"]["selected_tabulated_report"] != "") {
              $dsp_selected_tabulated_report .= " [".rawurldecode($tc_data["base_tool"]["selected_tabulated_report"])."]";
            }

            $module .=
            "<tr>\r\n".
            "<td>Scan location</td>\r\n".
            "<td colspan=\"3\">".$dsp_selected_scan_location."</td>\r\n".
            "</tr>\r\n".
            "<tr>\r\n".
            "<td>Digital report</td>\r\n".
            "<td colspan=\"3\">".$dsp_selected_digital_report."</td>\r\n".
            "</tr>\r\n".
            "<tr>\r\n".
            "<td>Tabulated report</td>\r\n".
            "<td colspan=\"3\">".$dsp_selected_tabulated_report."</td>\r\n".
            "</tr>\r\n";
         }
  
         // Additional tool data
         if(in_array("2", $container)) {
           $module .= 
            "<tr>\r\n".
            "<td>Number</td>\r\n".
            "<td colspan=\"3\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/0/".$tc_data["tool"]["unid"]."?open\" target=\"_blank\">".rawurldecode($tc_data["tool"]["number"])."</a>".$proposal_tool."<div style=\"position:absolute;margin-top:10px;display:none;\" id=\"tool_history\"></div></td>\r\n".
            //   "<td><a href=\"javascript:void(0);\" onclick=\"load_history('".$tc_data["tool"]["unid"]."', 'tool');\"><img src=\"../../../../library/images/16x16/objects-18.png\" style=\"border:none; margin-right:233px; position:relative;top:2px; left:5px;\" /></a></td>\r\n".
            "</tr>".
            "<tr>\r\n".
            "<td>Proposal</td>\r\n".
            "<td colspan=\"3\">".rawurldecode($tc_data["meta"]["proposal_pot_tool"])."</td>\r\n".
            "</tr>".
            "<tr>".
            "<td>Tool Supplier</td>".
            "<td colspan=\"3\"><input name=\"tool_supplier\" value=\"".rawurldecode($tc_data["tool"]["supplier"])."\" style=\"border:none;\" readonly=\"readonly\"></td>".
            "</tr>".
            "<tr>".
            "<td>Name</td>".
            "<td colspan=\"3\">".rawurldecode($tc_data["tool"]["name"])."</td>".
            "</tr>".
            "<tr>".
            "<td>CuCo</td>".
            "<td>".rawurldecode($tc_data["tool"]["cuco"])."</td>".
            "<td>Confirmed EVT</td>".
            "<td>".$dsp_calendar."</td>".
            "</tr>".
            "<tr>".
            "<td><history field=\"purch_req\" oncontextmenu=\"protocol.embed(this); return false;\">".$sapprlink."PR no</history></td>".
            "<td colspan=\"3\">".embed_input(array(), $tc_data["tool"]["prno"], array("check" => "purch_req", "id" => generate_uniqueid(8), "type" => "text", "name" => "purch_req", "style" => "width:306px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>".
            "</tr>".
            "<tr>".
            "<td><history field=\"pono\" oncontextmenu=\"protocol.embed(this); return false;\">".$saplink."PO num</history></td>".
            "<td colspan=\"3\" style=\"white-space:nowrap;\">".embed_input(array(), $tc_data["tool"]["pono"], array("check" => "pono", "id" => generate_uniqueid(8), "type" => "text", "name" => "pono", "style" => "width:224px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);")).
            "<span style=\"position:relative;left:-2px;\"><history field=\"position\" oncontextmenu=\"protocol.embed(this); return false;\">/</history></span>".
            embed_input(array(), $tc_data["tool"]["position"], array("check" => "position", "id" => generate_uniqueid(8), "type" => "text", "name" => "position", "style" => "width:68px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);")).
            "</td>".
            "</tr>".
            "<tr>".
            "<td><history field=\"totalcost\" oncontextmenu=\"protocol.embed(this); return false;\">Total tool costs</history></td>".
            "<td>".embed_input(array(), $tc_data["tool"]["totalcost"], array("check" => "totalcost", "id" => generate_uniqueid(8), "type" => "text", "name" => "totalcost", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>\r\n".
            "<td><history field=\"dualsource\" oncontextmenu=\"protocol.embed(this, {'0': 'unchecked', '1': 'checked'}); return false;\">Dual Source</history></td>".
            "<td>".$dsp_dsource."</td>";
			
			if	(strpos(trim(rawurldecode($tc_data["tool"]["transferred_location"])),'TE00')== true)	{	
			$module .=
			"</tr>".
            "<td><history field=\"exttool\" oncontextmenu=\"protocol.embed(this); return false;\">Tool location - External</history></td>".
           "<td>".embed_input(array(), $tc_data["tool"]["exttool"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "exttool", "value"=> "No","readonly"=>"readonly", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"));
            "</tr>";
			}
			else if	(substr(rawurldecode($tc_data["tool"]["transferred_location"]),0,3)== 'TE ')	{	
			$module .=
			"</tr>".
            "<td><history field=\"exttool\" oncontextmenu=\"protocol.embed(this); return false;\">Tool location - External</history></td>".
           "<td>".embed_input(array(), $tc_data["tool"]["exttool"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "exttool", "value"=> "No","readonly"=>"readonly", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"));
            "</tr>";
			}
			else if	(strpos(trim(rawurldecode($tc_data["tool"]["transferred_location"])),'TE00')!= true && trim(rawurldecode($tc_data["tool"]["transferred_location"]))!="")	{	
			$module .=
			"</tr>".
            "<td><history field=\"exttool\" oncontextmenu=\"protocol.embed(this); return false;\">Tool location - External</history></td>".
           "<td>".embed_input(array(), $tc_data["tool"]["exttool"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "exttool", "value"=> "Yes","readonly"=>"readonly", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"));
            "</tr>";
			}
					else if	(substr(rawurldecode($tc_data["tool"]["transferred_location"]),0,3)!= 'TE ' && trim(rawurldecode($tc_data["tool"]["transferred_location"]))!="")	{	
			$module .=
			"</tr>".
            "<td><history field=\"exttool\" oncontextmenu=\"protocol.embed(this); return false;\">Tool location - External</history></td>".
           "<td>".embed_input(array(), $tc_data["tool"]["exttool"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "exttool", "value"=> "No","readonly"=>"readonly", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"));
            "</tr>";
			}
			
			else if	(strpos(trim(rawurldecode($tc_data["tool"]["supplier"])),'TE00')== true)
			{
			$module .=
			"</tr>".
            "<td><history field=\"exttool\" oncontextmenu=\"protocol.embed(this); return false;\">Tool location - External</history></td>".
           "<td>".embed_input(array(), $tc_data["tool"]["exttool"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "exttool", "value"=> "No","readonly"=>"readonly", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"));
            "</tr>";
		
			  }
			  
			
			  else {
			  			$module .=
            "</tr>".
            "<td><history field=\"exttool\" oncontextmenu=\"protocol.embed(this); return false;\">Tool location - External</history></td>".
           "<td>".embed_input(array(), $tc_data["tool"]["exttool"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "exttool", "value"=> "Yes","readonly"=>"readonly", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"));
            "</tr>";
		
			  }
			  
			
			  $module .=
            "<tr>".
            "<td><history field=\"resin_primary\" oncontextmenu=\"protocol.embed(this); return false;\">Primary Resin</history></td>".
            "<td>".embed_input(array(), $tc_data["tool"]["resin_primary"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "resin_primary", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"));
 
 
           if ($tc_data["tool"]["dualsource"] == "1" ){
             $module .=
              "<td><history field=\"resin_secondary\" oncontextmenu=\"protocol.embed(this); return false;\">Secondary Resin</history></td>".
              "<td>".embed_input(array(), $tc_data["tool"]["resin_secondary"], array("id" => generate_uniqueid(8), "type" => "text", "name" => "resin_secondary", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);")).
              "</td>".
              "</tr>";
           } else {
             $module .=
              "<td colspan=\"2\">&nbsp;</td>".
              "</td>".
              "</tr>";
           }

           $module .=
            "<tr>".
            "<td><history field=\"guarantoutput\" oncontextmenu=\"protocol.embed(this); return false;\">Guarant. output [k]</history></td>".
            "<td>".embed_input(array(), $tc_data["tool"]["guarantoutput"], array("check" => "guarantoutput", "id" => generate_uniqueid(8), "type" => "text", "name" => "guarantoutput", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>\r\n".
            "<td><history field=\"currentoutput\" oncontextmenu=\"protocol.embed(this); return false;\">Curr output</history></td>\r\n".
            "<td>".embed_input(array(), $tc_data["tool"]["currentoutput"], array("check" => "currentoutput", "id" => generate_uniqueid(8), "type" => "text", "name" => "currentoutput", "style" => "width:85px;", "onblur" => "post.save_field(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>\r\n".
            "</tr>".
            "<tr>".
            "<td><history field=\"fotsampling\" oncontextmenu=\"protocol.embed(this); return false;\">FOT sampling</td>".
            "<td>".$dsp_fotsampling."</td>\r\n".
            "<td><history field=\"followupsampling\" oncontextmenu=\"protocol.embed(this); return false;\">Follow up sampling</history></td>\r\n".
            "<td>".$dsp_followupsampling."</td>\r\n".
            "</tr>";
         }
     }


     $module .= "</table>\r\n";

   }

   return str_replace("Array","",$module);

}




?>