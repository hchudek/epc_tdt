<?php


function m_v6_mgmt_report_view($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Ansicht einbinden
   if(isset($data)) unset($data);
   $app="epc/projects";
   $icon="true";
   $number="true";

   if($_REQUEST[$_module_id."_add"] != "") {
      $path = str_replace(" ", "%20", $_SESSION["remote_injectdb_path"]."/v.unique/".$_REQUEST[$_module_id."_add"]."?open");
      $handle = print_r(@fopen($path, "r"), true);
      $load = (strpos($handle, "Resource id") > -1) ? true : false;
      if($load) {
         $eval = file_get_authentificated_contents($path);
         eval($eval);
      }
   }

   include_once("addin/generate_extended_view.php");


   // ************************
   // START: CREATED BY FILTER
   //*************************
   foreach($data as $val) if(!in_array($val["filter2"], $created_by)) $created_by[] = $val["filter2"];
   sort($created_by);

   $c_html = "T&PM:&nbsp;&nbsp;<select style=\"height:20px;font:normal 11px century gothic,verdana;\" onchange=\"location.href='?".str_replace("&".$_module_id."_f2=".$_REQUEST[$_module_id."_f2"], "", $_SERVER["QUERY_STRING"])."&".$_module_id."_f2=' + this.value;\">\r\n".
   "<option value=\"\">Anybody</option>\r\n";
   foreach($created_by as $val) {
      $selected = ($_REQUEST[$_module_id."_f2"] == base64_encode($val)) ? " selected" : "";
      $c_html .= "<option value=\"".base64_encode($val)."\"".$selected.">".urldecode($val)."</option>\r\n";

   }
   $c_html .= "</select>\r\n";
   // ***********************
   // ENDE: CREATED BY FILTER
   // ***********************


   // Überschrift und Modulsonderzubehör erstellen
   $tabs="&nbsp;&nbsp;Parts with Tools & Project %%INFO%%";

   // Export Links
   $exp = "<img onclick=\"location.href='?".$_module_id."_download=xls&".$_SERVER["QUERY_STRING"]."';\" src=\"../../../../library/images/icons/ms-excel-icon.gif\" style=\"margin-right:7px;cursor:pointer;filter:Gray();\" /><img onclick=\"location.href='?".$_module_id."_download=doc&".$_SERVER["QUERY_STRING"]."';\" src=\"../../../../library/images/icons/ms-word-icon.gif\" style=\"margin-right:7px;cursor:pointer;filter:Gray();\" />";


   // View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
   if($_REQUEST[$_module_id."_fields"] == "") $t_view = "<div style=\"font:normal 13px century gothic;\" id=\"".$_module_id."_report_info\"><div style=\"padding-top:10px;\">No fields selected</div></div>";
   else {
      $dsp_filter["Project-No"] = false;
      $dsp_filter["Part No"] = false;
      $dsp_filter["Part Rev."] = false;
      $dsp_filter["Part Desc."] = false;
      $dsp_filter["Part Desc."] = false;
      $t_view = "<div id=\"".$_module_id."_report_info\">".
      generate_extended_view($dsp_filter, $path, $column, $data, $_w, $_SESSION["module"][$_module_name], true, true, true, true, true, false, 40, array("use:html","edit_tracker.php?unique=%%UNID%%")).
      "</div>";
   }
   if(!$load && $_REQUEST[$_module_id."_add"] != "") $t_view = "<div style=\"font:normal 13px century gothic;\" id=\"".$_module_id."_report_info\"><div style=\"padding-top:10px;\">Report expiered</div></div>";

   $strng = substr($_REQUEST[$_module_id."_add"], strrpos($_REQUEST[$_module_id."_add"], "@") + 1, 14);
   $date = substr($strng,0, 4)."-".substr($strng, 4, 2)."-".substr($strng, 6,2);
   $time = substr($strng, 8, 2).":".substr($strng, 10, 2).":".substr($strng, 12, 2);
  
   if($_REQUEST[$_module_id."_add"] == "") $tabs = str_replace("%%INFO%%", "<p id=\"report_loaded\" style=\"display:none;\"></p>", $tabs);
   else $tabs = str_replace("%%INFO%%", "&nbsp;<span style=\"color:rgb(0,102,158);\">[report date:&nbsp;<date>".$date."</date>&nbsp;".$time."]</span>", $tabs);

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tbl_header_pot\" style=\"width:100%;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_4_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_4_headline\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\"><tr><td style=\"width:98%;\">%%TABS%%</td><td style=\"text-align:right;width:1%;\"><nobr>".$c_html."&nbsp;</nobr></td><td style=\"text-align:right;width:1%;\"><nobr>&nbsp;".$exp."</nobr></td></tr></table></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<div id=\"tbl_main_pot\" style=\"width:1px;height:500px;overflow:auto;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"overflow-y:hidden;\">\r\n".
   "   <tr><td colspan=\"2\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
   "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
   "</form></div>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));

   return $module;

}

?>