<?php

function m_s1_report_setup ($_application) {
   $html = (in_array("[mgmt_report]", $_SESSION[remote_userroles])) ? m_s1_report_setup_add($_application) : "ACCESS DENIED";
   return $html;
}

function m_s1_report_setup_add($_application) {
// Modulnamen festlegen
$_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
$_module_id=substr(basename(__FILE__),0,4);
$_SESSION["module"][$_module_name]=$_module_id;
global $capacity_report_data;

$cr = $capacity_report_data;

// Überschrift und Modulsonderzubehör erstellen
$tabs="&nbsp;&nbsp;Report Setup";


   $t_view="<form name=\"create_setup\" style=\"display:inline;\">\r\n".
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:2px;\">\r\n".

// Setup Project complexity
   "   <tr>\r\n".
   "      <td colspan=\"7\" class=\"td_head\">Project complexity</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_reg\">A</td>\r\n".
   "      <td class=\"td_reg\">B</td>\r\n".
   "      <td class=\"td_reg\">C</td>\r\n".
   "      <td class=\"td_reg\">D</td>\r\n".
   "      <td class=\"td_reg\">E</td>\r\n".
   "      <td class=\"td_reg\">F</td>\r\n".
   "      <td class=\"td_reg\">G</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"complexity_a\" value=\"".$cr["project"]["complexity_a"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"complexity_b\" value=\"".$cr["project"]["complexity_b"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"complexity_c\" value=\"".$cr["project"]["complexity_c"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"complexity_d\" value=\"".$cr["project"]["complexity_d"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"complexity_e\" value=\"".$cr["project"]["complexity_e"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"complexity_f\" value=\"".$cr["project"]["complexity_f"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"complexity_g\" value=\"".$cr["project"]["complexity_g"]."\" /></td>\r\n".
   "   </tr>\r\n".

// Setup Type of Tool
   "   <tr>\r\n".
   "      <td colspan=\"7\" class=\"td_head\">Type of Tool</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_reg\">Conv. Kit</td>\r\n".
   "      <td class=\"td_reg\">Dupl. / Repl.</td>\r\n".
   "      <td class=\"td_reg\">Modification</td>\r\n".
   "      <td class=\"td_reg\">New Project</td>\r\n".
   "      <td class=\"td_reg\">Pilot</td>\r\n".
   "      <td class=\"td_reg\">Purchase Part</td>\r\n".
   "      <td class=\"td_reg\">Packaging</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"conkit_type_of_tool\" value=\"".$cr["process_type"]["conv_kit"]["type_of_tool"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"repl_type_of_tool\" value=\"".$cr["process_type"]["dupl_repl"]["type_of_tool"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"modif_type_of_tool\" value=\"".$cr["process_type"]["modification"]["type_of_tool"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"newprj_type_of_tool\" value=\"".$cr["process_type"]["new_project"]["type_of_tool"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"pilot_type_of_tool\" value=\"".$cr["process_type"]["pilot"]["type_of_tool"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"purch_type_of_tool\" value=\"".$cr["process_type"]["purchase_part"]["type_of_tool"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"packaging_type_of_tool\" value=\"".$cr["process_type"]["packaging"]["type_of_tool"]."\" /></td>\r\n".
   "   </tr>\r\n".

// Setup Kind of Project
   "   <tr>\r\n".
   "      <td colspan=\"7\" class=\"td_head\">Kind of Project</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_reg\">Assy easy</td>\r\n".
   "      <td class=\"td_reg\">Assy middle</td>\r\n".
   "      <td class=\"td_reg\">Assy diff.</td>\r\n".
   "      <td class=\"td_reg\">Mold easy</td>\r\n".
   "      <td class=\"td_reg\">Mold middle</td>\r\n".
   "      <td class=\"td_reg\">Mold diff.</td>\r\n".
   "      <td class=\"td_reg\">Packaging</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"assyeasy_kind_of_project\" value=\"".$cr["process_rules"]["assembly_easy"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"assymid_kind_of_project\" value=\"".$cr["process_rules"]["assembly_mid"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"assydiff_kind_of_project\" value=\"".$cr["process_rules"]["assembly_difficult"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"moldeasy_kind_of_project\" value=\"".$cr["process_rules"]["molding_easy"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"moldmid_kind_of_project\" value=\"".$cr["process_rules"]["molding_mid"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"molddiff_kind_of_project\" value=\"".$cr["process_rules"]["molding_difficult"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"packaging_kind_of_project\" value=\"".$cr["process_rules"]["packaging"]["kind_of_project"]."\" /></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_reg\">Stamping easy</td>\r\n".
   "      <td class=\"td_reg\">Stamping middle</td>\r\n".
   "      <td class=\"td_reg\">Stamping diff.</td>\r\n".
   "      <td class=\"td_reg\">Modification</td>\r\n".
   "      <td class=\"td_reg\">Plating</td>\r\n".
   "      <td colspan=\"2\" class=\"td_reg\">Purchase part</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"stampeasy_kind_of_project\" value=\"".$cr["process_rules"]["stamping_easy"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"stampmid_kind_of_project\" value=\"".$cr["process_rules"]["stamping_mid"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"stampdiff_kind_of_project\" value=\"".$cr["process_rules"]["stamping_difficult"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"modif_kind_of_project\" value=\"".$cr["process_rules"]["modifikation"]["kind_of_project"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"plating_kind_of_project\" value=\"".$cr["process_rules"]["plating_mid"]["kind_of_project"]."\" /></td>\r\n".
   "      <td colspan=\"2\" class=\"td_alt\"><input type=\"text\" name=\"purch_kind_of_project\" value=\"".$cr["process_rules"]["purchase_part"]["kind_of_project"]."\" /></td>\r\n".
   "   </tr>\r\n".

// Setup Process Type
   "   <tbody style=\"display:none;\">".
   "   <tr>\r\n".
   "      <td colspan=\"7\" class=\"td_head\">Process Type</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_reg\">Conv. Kit hour</td>\r\n".
   "      <td class=\"td_reg\">Dupl. / Repl. hour</td>\r\n".
   "      <td class=\"td_reg\">Modification hour</td>\r\n".
   "      <td class=\"td_reg\">New Project hour</td>\r\n".
   "      <td class=\"td_reg\">Pilot hour</td>\r\n".
   "      <td class=\"td_reg\">Purchase Part hour</td>\r\n".
   "      <td class=\"td_reg\">Packaging hour</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"convkit_hour\" value=\"".$cr["process_type"]["conv_kit"]["hour"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"repl_hour\" value=\"".$cr["process_type"]["dupl_repl"]["hour"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"modif_hour\" value=\"".$cr["process_type"]["modification"]["hour"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"newprj_hour\" value=\"".$cr["process_type"]["new_project"]["hour"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"pilot_hour\" value=\"".$cr["process_type"]["pilot"]["hour"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"purch_hour\" value=\"".$cr["process_type"]["purchase_part"]["hour"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"packaging_hour\" value=\"".$cr["process_type"]["packaging"]["hour"]."\" /></td>\r\n".

   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_reg\">Conv. Kit week</td>\r\n".
   "      <td class=\"td_reg\">Dupl. / Repl. week</td>\r\n".
   "      <td class=\"td_reg\">Modification week</td>\r\n".
   "      <td class=\"td_reg\">New Project week</td>\r\n".
   "      <td class=\"td_reg\">Pilot week</td>\r\n".
   "      <td class=\"td_reg\">Purchase Part week</td>\r\n".
   "      <td class=\"td_reg\">Packaging week</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"convkit_week\" value=\"".$cr["process_type"]["conv_kit"]["week"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"repl_week\" value=\"".$cr["process_type"]["dupl_repl"]["week"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"modif_week\" value=\"".$cr["process_type"]["modification"]["week"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"newprj_week\" value=\"".$cr["process_type"]["new_project"]["week"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"pilot_week\" value=\"".$cr["process_type"]["pilot"]["week"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"purch_week\" value=\"".$cr["process_type"]["purchase_part"]["week"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"packaging_week\" value=\"".$cr["process_type"]["packaging"]["week"]."\" /></td>\r\n".
   "   </tr>\r\n".
   "   </tbody>".
// Setup Sourcing
   "   <tr>\r\n".
   "      <td colspan=\"7\" class=\"td_head\">Sourcing</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_reg\">EMEA</td>\r\n".
   "      <td class=\"td_reg\">Global</td>\r\n".
   "      <td class=\"td_reg\">Local</td>\r\n".
   "      <td colspan=\"4\" class=\"td_reg\">Dual source</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"source_emea\" value=\"".$cr["sourcing"]["emea"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"source_global\" value=\"".$cr["sourcing"]["global"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"source_local\" value=\"".$cr["sourcing"]["local"]."\" /></td>\r\n".
   "      <td colspan=\"4\" class=\"td_alt\"><input type=\"text\" name=\"source_dual\" value=\"".$cr["sourcing"]["dual_source"]."\" /></td>\r\n".
   "   </tr>\r\n".
   
// Setup Slider
   "   <tr>\r\n".
   "      <td colspan=\"7\" class=\"td_head\">Slider configuration</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_bold\">Fixed</td>\r\n".
   "      <td class=\"td_reg\">Start point</td>\r\n".
   "      <td class=\"td_reg\">End point</td>\r\n".
   "      <td class=\"td_reg\">Default value</td>\r\n".
   "      <td colspan=\"3\" class=\"td_reg\">Grid width</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\">&nbsp;</td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"fix_start\" value=\"".$cr["slider"]["fixed"]["start"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"fix_end\" value=\"".$cr["slider"]["fixed"]["end"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"fix_default\" value=\"".$cr["slider"]["fixed"]["default"]."\" /></td>\r\n".
   "      <td colspan=\"3\" class=\"td_alt\"><input type=\"text\" name=\"fix_grid\" value=\"".$cr["slider"]["fixed"]["grid"]."\" /></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_bold\">Variable</td>\r\n".
   "      <td class=\"td_reg\">Start point</td>\r\n".
   "      <td class=\"td_reg\">End point</td>\r\n".
   "      <td class=\"td_reg\">Default value</td>\r\n".
   "      <td colspan=\"3\" class=\"td_reg\">Grid width</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"td_alt\">&nbsp;</td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"var_start\" value=\"".$cr["slider"]["variable"]["start"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"var_end\" value=\"".$cr["slider"]["variable"]["end"]."\" /></td>\r\n".
   "      <td class=\"td_alt\"><input type=\"text\" name=\"var_default\" value=\"".$cr["slider"]["variable"]["default"]."\" /></td>\r\n".
   "      <td colspan=\"3\" class=\"td_alt\"><input type=\"text\" name=\"var_grid\" value=\"".$cr["slider"]["variable"]["grid"]."\" /></td>\r\n".
   "   </tr>\r\n".
   "</table></form>\r\n".

   "<span class=\"phpbutton\"><a href=\"javascript:save_config();\">Save</a></span>";



$module=
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"create_setup\" style=\"width:920px;margin-top:39px\">\r\n".
"   <tr>\r\n".
"      <td colspan=\"2\" class=\"module_5_content\"><div style=\"border:solid 1px rgb(200,200,200);position:relative;top:-11px;padding-top:4px;padding-bottom:4px;\" class=\"content\">".$t_view."</div></td>\r\n".
"   </tr>\r\n".
"</table>\r\n";


$module=str_replace("Array", "", str_replace("%%TABS%%", $tabs, $module));

return $module;
}

?>