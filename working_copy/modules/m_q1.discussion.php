<?php


function m_q1__discussion($_application) {

   $_SESSION["m_q1__discussion:request"] = $_REQUEST;
   $_SESSION["m_q1__discussion:page_id"] = str_replace(".php", "", strtolower(substr($_SERVER["PATH_TRANSLATED"], strrpos($_SERVER["PATH_TRANSLATED"], "\\") + 1, strlen($_SERVER["PATH_TRANSLATED"]))));
   $_SESSION["application:page_id"] = $_application["page_id"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   //$module = ($_REQUEST["unique"] == "") ? "No tracker selected" : "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   $module = m_q1__discussion_generate_html($_application);


   return array($headline, $module);

}


function m_q1__discussion_generate_html($_application) {

   $_application["page_id"] = $_SESSION["application:page_id"];
   $_REQUEST = $_SESSION["m_q1__discussion:request"];

   global $tc_data;

   $_SESSION["remote_domino_path_resource"] = substr($_SESSION["remote_domino_path_perportal"], 0, strrpos($_SESSION["remote_domino_path_perportal"], "/"))."/resource.nsf";
   $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_folder"] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_folder"] == "") ? "" : $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_folder"];
   $folder = $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_folder"];

   switch(strtolower($folder)) {
      case "project":
         $id = $_SESSION["project"];
         break;
      case "tracker":
         $id = $_REQUEST["unique"];
         break;
      case "pot":
         $id = $_REQUEST["pot"]."@".$_REQUEST["unique"];
         break;
   }

   $option[] = array("", "");
   $option["Project"] = array("Project", "");
   $option["Tracker"] = array("Tracker", "");
   $option["POT"] = array("POT", "");


   if($_REQUEST["pot"] == "") {
      $option["POT"][1] = " disabled";
      $option["Part"][1] = " disabled";
      $option["Tool"][1] = " disabled";
   }

   $select = "<select style=\"width:300px;\" target=\"".str_replace("_generate_html", "", __FUNCTION__)."_folder\" onchange=\"".str_replace("_generate_html", "", __FUNCTION__).".select_folder(this);\">\r\n";
   foreach($option as $key => $val) { 
      $selected = ($folder == $key) ? " selected" : "";
      $select .= "<option value=\"".$key."\"".$selected.$val[1].">".$val[0]."</option>\r\n";
   }
   $select .= "</select>\r\n";

   $resources = file_get_authentificated_contents($_SESSION["remote_domino_path_resource"]."/v.get_html?open&count=9999&restricttocategory=td,".$id."&headline=false");


   $html = 
   "<table id=\"tbl_m_q1__discussion\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\" resource=\"".$id."\" style=\"width:100%;\">\r\n".
   "<tr>\r\n".
   "<td class=\"nth1\" width=\"1%;\"><b>Folder&nbsp;&nbsp;&nbsp;</b></td>\r\n".
   "<td style=\"width:99%;\">".$select."</td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td colspan=\"2\">".str_replace("embed_resource", "m_q1__discussion.embed_resource", $resources)."</td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n";

   if(trim($folder) != "" && trim($folder != "0")) $html .= "<td colspan=\"2\"><span class=\"phpbutton\"><a href=\"javascript:m_q1__discussion.add('".$id."');\">Add new resource</a></span></th>\r\n";

   $html .= "</tr>\r\n".
   "<tr>\r\n<td style=\"height:4px;\" colspan=\"5\"></td>\r\n</tr>\r\n".
   "</table>\r\n";

   return str_replace("No documents found", "", $html);

}



?>