<?php


function m_t2__meta_data($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   
   return array($headline, $module);

}

function m_t2__meta_data_generate_html($_application) {

   global $tc_data;

   if (trim(implode($tc_data["meta"]), ",") == "") {
     $module = "&nbsp;";
   } 
   else {


$tc_data["meta"]["process_type"] = (is_array($tc_data["meta"]["process_type"])) ? "&nbsp;" : rawurldecode($tc_data["meta"]["process_type"]);
$tc_data["meta"]["process_rules"] = (is_array($tc_data["meta"]["process_rules"])) ? "&nbsp;" : rawurldecode($tc_data["meta"]["process_rules"]);


   $module =
   "<table id=\"tbl_t2__meta\">".
   "<tr>".
      "<td class=\"label\" style=\"width:1%;\"><nobr>Process type:</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$tc_data["meta"]["process_type"]."</td>".
   "</tr>".
   "<tr>".
      "<td class=\"label\" style=\"width:1%;\"><nobr>Process rules:</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$tc_data["meta"]["process_rules"]."</td>".
   "</tr>";

//   if($tc_data["pot"]["is_proposal"] == "1") {
//   }
//   else {
      $module .=
   "<tr>".
      "<td class=\"label\" style=\"width:1%;\">Project Case:</td>".
      "<td class=\"data\" colspan=\"3\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcasesbynumber/".$tc_data["meta"]["disno"]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["meta"]["disno"]."</a></td>".
   "</tr>";
  /*
      if(strlen($tc_data["disno"])<=12){
         $module .=
         "<td class=\"data\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcasesbynumber/".$tc_data["pot"]["disno"]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["disno"]."</a></td>";
      }else{
         $module .=
         "<td class=\"data\"><b>!!!</b> ".substr($tc_data["disno"],0,"12")." <b>!!!</b>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcasesbynumber/".substr($tc_data["disno"],(-12))."?open\" class=\"linkdata\" target=\"_blank\">".substr($tc_data["pot"]["disno"],(-12))."</a></td>";
      }
	  
	  if(in_array("[supervisor]", $_SESSION["remote_userroles"])){
	     $module .=
//	      "<td class=\"data\" colspan=\"2\"><a href=\"ee_edit_pot.php?&load=".strtolower($tc_data["case_unid"])."\"><img src=\"".$_SESSION["php_sever"]."/library/images/16x16/setting-5.png\" style=\"broder:none;margin-left:3px;\" /></a><font style=\"color: red;\">&nbsp;&nbsp;&nbsp;nur f&uuml;r Claudia: <b>".$tc_data["pot"]["disno_new"]."</b></font></td>".
      "</tr>";		 
	  } else {
	     $module .=
//	      "<td class=\"data\" colspan=\"2\"><a href=\"ee_edit_pot.php?&load=".strtolower($tc_data["case_unid"])."\"><img src=\"".$_SESSION["php_sever"]."/library/images/16x16/setting-5.png\" style=\"broder:none;margin-left:3px;\" /></a></td>".
      "</tr>";
	  }
   }
*/

      }
   "</table>";

   return str_replace("Array","",$module);

}

?>