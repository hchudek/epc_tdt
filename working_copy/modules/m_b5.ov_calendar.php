<?php


function m_b5__ov_calendar($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   
   $headline="Sample Calendar";


   // START: Werktagedefinition einbinden
   $t_year =($_REQUEST[$_module_id."_date"] != "") ? date("Y", $_REQUEST[$_module_id."_date"]) : date("Y", time());

   $go_past = 0;
   $go_future = 0;

   $start_date = mktime(0, 0, 0, 1, 1, intval($t_year) - $go_past);
   $end_date = mktime(0, 0, 0, 12, 31, intval($t_year) + $go_future);

   $this_date = $start_date;

   $i = 86400; while($this_date <= $end_date) {
      $is_weekday = (in_array(date("w", $this_date), explode(":", "0:6")) == 0) ? 1 : 0;
      $wd[date("Y/m/d", $this_date)] = $is_weekday;
      $this_date += $i;
   }

   $xml_wd = generate_xml($_SESSION["remote_domino_path_perportal"]."/v.get_workingdays?open&restricttocategory=".$_SESSION["remote_perportal_unid"]."&function=xml:data");

   foreach(array_keys($xml_wd) as $key) {
      $count = 0;
      $y = substr($key,-4);
      for($d = 1; $d < 32; $d++) {
         for($m = 1; $m < 13; $m++) {
            if(checkdate($m, $d, intval($y)) == true) {
               $count++;
               $c_date =  mktime(0, 0, 0, $m, $d, intval($y));
               $weekday[date("Y-m-d", $c_date)] = substr($xml_wd[$key], $count - 1, 1);
            }
         }
      } 
   }
   // ENDE: Werktagedefinition einbinden
   // Kategory Selector erstellen
   $xml = generate_xml($_SESSION["remote_domino_path_main"]."/v.get_calendar_ov?open&count=99999&function=xml:category");
   $t_category = (isset($_REQUEST[$_module_id."_category"])) ? base64_decode($_REQUEST[$_module_id."_category"]) : "";
   foreach($xml["value"] as $val) {
      $val = urldecode($val);
      if(!in_array($val, $category)) $category[] = $val;
   }
   sort($category);
 
   $dsp_category = "<select style=\"font:normal 11px century gothic,verdana;width:290px;height:21px;position:relative;top:1px;\" onchange=\"location.href='?".str_replace("&".$_module_id."_category=".$_REQUEST[$_module_id."_category"], "", $_SERVER["QUERY_STRING"])."&".$_module_id."_category=' + this.value\">".
   "<option value=\"\"></option><option value=\"".base64_encode("all")."\">All samples</option>";
   foreach($category as $val) {
      $selected = ($t_category == $val) ? " selected" : "";
      $dsp_category .= "<option value=\"".base64_encode($val)."\"$selected>".$val."</option>";
   }    
   $dsp_category .= "</select>";  

   // Überschrift und Modulsonderzubehör erstellen
   $tabs=
   "<table border=\"0\" cellspacing=\"0\"0 cellpadding=\"0\" style=\"width:100%;font:normal 12px verdana;text-transform:uppercase;\"><tr><td width=\"99%\"></td><td style=\"text-align:right;\"><nobr>Sample calendar&nbsp;&nbsp;</nobr></td><td style=\"text-align:right;\">".
   $dsp_category."</td></table>";

   
 
   $t_date = time();
   $t_month = ($_REQUEST["m_b4_date"] == "") ? date("m", $t_date) : date("m", $_REQUEST["m_b4_date"]);
   $t_year = ($_REQUEST["m_b4_date"] == "") ? date("Y", $t_date) : date("Y", $_REQUEST["m_b4_date"]);
   $wd_1 = date("N", mktime(0, 0, 0, $t_month, 1, $t_year));

  
   $dsp_date = ($t_category == "") ? "" : generate_xml($_SESSION["remote_domino_path_main"]."/v.get_calendar_ov?open&count=9999&restricttocategory=".urlencode(date("Y-m-", mktime(0, 0, 0, $t_month, 1, $t_year)).$t_category)."&function=xml:data");

   $content = "<div style=\"border-bottom:solid 2px rgb(99,99,99);width:100%;text-align:right;font:normal 22px century gothic, verdana;margin-top:10px;margin-bottom:10px;padding-bottom:5px;\">".
   "<a style=\"text-decoration:none; color:rgb(99,99,99);\" href=\"?".str_replace("&m_b4_date=".$_REQUEST["m_b4_date"], "", $_SERVER["QUERY_STRING"])."&m_b4_date=".mktime(0, 0, 0, ($t_month - 1), 1,  $t_year)."\">-</a>".
   "<span style=\"padding-left:8px;padding-right:8px;\">".date("F", mktime(0, 0, 0, $t_month, 1, $t_year))." ".$t_year."</span>".
   "<a style=\"text-decoration:none; color:rgb(99,99,99);\" href=\"?".str_replace("&m_b4_date=".$_REQUEST["m_b4_date"], "", $_SERVER["QUERY_STRING"])."&m_b4_date=".mktime(0, 0, 0, ($t_month + 1), 1,  $t_year)."\">+</a></div>".

   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;font:normal 13px century gothic, verdana;\" id=\"tbl_my_calendar\">";
   $wd = 0; $d = 2; while(checkdate($t_month, $d - $wd_1, $t_year) || $d <= $wd_1) {
      if($wd == 0) $content .= "<tr style=\"height:100px;\">\r\n";
      $final_date = date("d (D)", mktime(0, 0, 0, $t_month, ($d - $wd_1), $t_year));
      $txt = ($d > $wd_1) ? $final_date : "&nbsp;";
      $date_ident = ($d > $wd_1) ? date("d", mktime(0, 0, 0, $t_month, ($d - $wd_1), $t_year)) : "&nbsp;"; 
      if(isset($dsp_date["calendar_dsp_".$date_ident])) {
         $c_t = "";
         if(is_array($dsp_date["calendar_dsp_".$date_ident])) {
            foreach($dsp_date["calendar_dsp_".$date_ident] as $val) {
               $c_t .= "<div>".urldecode($val)."</div>";
            }
         }
         else $c_t .= "<div>".urldecode($dsp_date["calendar_dsp_".$date_ident])."</div>";
      }
      else $c_t = "&nbsp;";

      $t_date = date("Y-m-d", mktime(0, 0, 0, $t_month, ($d - $wd_1), $t_year));
      $bg = ($wd < 5) ? "255,222,102" : "220,220,220";
      $bgcol = ($weekday[$t_date] == "1") ? "255,255,255" : "255,241,210";
      $today = ($t_date == date("Y-m-d", time())) ? "bold" : "normal";

      $content .= "<td style=\"width:14%; vertical-align:top;border:solid 2px rgb(255,255,255);\"><div style=\"width:100%;background-color:rgb(".$bg.");padding:2px;font-weight:".$today."\">".$txt."</div><div style=\"margin-top:2px;margin-right:2px;padding:2px;color:rgb(0,102,158);border:dotted 1px rgb(99,99,99); background-color:rgb(".$bgcol.");height:130px;overflow:auto;\">".$c_t."</div></td>\r\n";
      if($wd == 6) {
         $content .= "</tr>\r\n";
         $wd = -1;
      }
      $wd++;
      $d++;
   }
   $content .= "</table>\r\n";

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"table-layout:fixed;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_4_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_4_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr><td colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
   "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
   "</form>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));  

   return array($headline, $module);

}

?>