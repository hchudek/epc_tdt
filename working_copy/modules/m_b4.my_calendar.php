<?php


function m_b4__my_calendar($_application) {


   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------


   $tpm = process_xml($_SESSION["remote_domino_path_main"]."/p.get_tpm?open");

   $headline .= "<select onchange=\"document.cookie = 'tdt:calendar=' + this[this.selectedIndex].value; location.reload();\">".
   "<option value=\"".substr($_SESSION["domino_user"],3,strpos($_SESSION["domino_user"],"OU")-4)."\">My calendar</option>";

   if(in_array("[insp_calendar]", $_SESSION["remote_userroles"])) {
      foreach($tpm["tpm"] as $v) {
         $select = ($_COOKIE["tdt:calendar"] == rawurldecode($v) ? " selected" : "");
         $headline .= "<option".$select." value=\"".rawurldecode($v)."\">".rawurldecode($v)."</option>\r\n";
      }
   }
   $headline .= "</select>";

   // START: Werktagedefinition einbinden
   $t_year =($_REQUEST[$_module_id."_date"] != "") ? date("Y", $_REQUEST[$_module_id."_date"]) : date("Y", time());

   $go_past = 0;
   $go_future = 0;

   $start_date = mktime(0, 0, 0, 1, 1, intval($t_year) - $go_past);
   $end_date = mktime(0, 0, 0, 12, 31, intval($t_year) + $go_future);

   $this_date = $start_date;

   $i = 86400; while($this_date <= $end_date) {
      $is_weekday = (in_array(date("w", $this_date), explode(":", "0:6")) == 0) ? 1 : 0;
      $wd[date("Y/m/d", $this_date)] = $is_weekday;
      $this_date += $i;
   }

   $xml_wd = generate_xml($_SESSION["remote_domino_path_perportal"]."/v.get_workingdays?open&restricttocategory=".$_SESSION["remote_perportal_unid"]."&function=xml:data");

   foreach(array_keys($xml_wd) as $key) {
      $count = 0;
      $y = substr($key,-4);
      for($d = 1; $d < 32; $d++) {
         for($m = 1; $m < 13; $m++) {
            if(checkdate($m, $d, intval($y)) == true) {
               $count++;
               $c_date =  mktime(0, 0, 0, $m, $d, intval($y));
               $weekday[date("Y-m-d", $c_date)] = substr($xml_wd[$key], $count - 1, 1);
            }
         }
      } 
   }
   // ENDE: Werktagedefinition einbinden

   // Überschrift und Modulsonderzubehör erstellen
   $tabs= "";


   $t_date = time();
   $t_month = ($_REQUEST["m_b4_date"] == "") ? date("m", $t_date) : date("m", $_REQUEST["m_b4_date"]);
   $t_year = ($_REQUEST["m_b4_date"] == "") ? date("Y", $t_date) : date("Y", $_REQUEST["m_b4_date"]);
   $wd_1 = date("N", mktime(0, 0, 0, $t_month, 1, $t_year));

   $uname = ($_COOKIE["tdt:calendar"] != "") ? $_COOKIE["tdt:calendar"] : substr($_SESSION["domino_user"],3,strpos($_SESSION["domino_user"],"OU")-4);


   $dsp_date = generate_xml($_SESSION["remote_domino_path_main"]."/v.get_calendar_my?open&count=9999&restricttocategory=".urlencode(date("Y-m-", mktime(0, 0, 0, $t_month, 1, $t_year)).$uname)."&function=xml:data");
   $content = "<div style=\"border-bottom:solid 2px rgb(99,99,99);width:100%;text-align:right;font:normal 22px century gothic, verdana;margin-top:10px;margin-bottom:10px;padding-bottom:5px;\">".
   "<a style=\"text-decoration:none; color:rgb(99,99,99);\" href=\"?".str_replace("&m_b4_date=".$_REQUEST["m_b4_date"], "", $_SERVER["QUERY_STRING"])."&m_b4_date=".mktime(0, 0, 0, ($t_month - 1), 1,  $t_year)."\">-</a>".
   "<span style=\"padding-left:8px;padding-right:8px;\">".date("F", mktime(0, 0, 0, $t_month, 1, $t_year))." ".$t_year."</span>".
   "<a style=\"text-decoration:none; color:rgb(99,99,99);\" href=\"?".str_replace("&m_b4_date=".$_REQUEST["m_b4_date"], "", $_SERVER["QUERY_STRING"])."&m_b4_date=".mktime(0, 0, 0, ($t_month + 1), 1,  $t_year)."\">+</a></div>".  
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;font:normal 13px century gothic, verdana;\" id=\"tbl_my_calendar\">";

   $wd = 0; $d = 2; while(checkdate($t_month, $d - $wd_1, $t_year) || $d <= $wd_1) {
      if($wd == 0) $content .= "<tr style=\"height:100px;\">\r\n";
      $final_date = date("d (D)", mktime(0, 0, 0, $t_month, ($d - $wd_1), $t_year));
      $txt = ($d > $wd_1) ? $final_date : "&nbsp;";
      $date_ident = ($d > $wd_1) ? date("d", mktime(0, 0, 0, $t_month, ($d - $wd_1), $t_year)) : "&nbsp;"; 
      if(isset($dsp_date["calendar_dsp_".$date_ident])) {
         $c_t = "";
         if(is_array($dsp_date["calendar_dsp_".$date_ident])) {
            foreach($dsp_date["calendar_dsp_".$date_ident] as $val) {
               $c_t .= "<div>".str_replace(array("taget=", "blank", "edit_tracker.php"), array("target=", "_blank", "tracker.php"), urldecode($val))."</div>";
            }
         }
         else $c_t .= "<div>".str_replace(array("taget=", "blank", "edit_tracker.php"), array("target=", "_blank", "tracker.php"), urldecode($dsp_date["calendar_dsp_".$date_ident]))."</div>";
      }
      else $c_t = "&nbsp;";

      $t_date = date("Y-m-d", mktime(0, 0, 0, $t_month, ($d - $wd_1), $t_year));
      $bg = ($wd < 5) ? "255,222,102" : "220,220,220";
      $bgcol = ($weekday[$t_date] == "1") ? "255,255,255" : "255,241,210";
      $today = ($t_date == date("Y-m-d", time())) ? "bold" : "normal";

      $content .= "<td style=\"width:14%; vertical-align:top;border:solid 2px rgb(255,255,255);\"><div style=\"width:100%;background-color:rgb(".$bg.");padding:2px;font-weight:".$today."\">".$txt."</div><div style=\"margin-top:2px;margin-right:2px;padding:2px;color:rgb(0,102,158);border:dotted 1px rgb(99,99,99); background-color:rgb(".$bgcol.");height:130px;overflow:auto;\">".$c_t."</div></td>\r\n";
      if($wd == 6) {
         $content .= "</tr>\r\n";
         $wd = -1;
      }
      $wd++;
      $d++;
   }
   $content .= "</table>\r\n";

   $module = $content;

   return array($headline, $module);

}

?>