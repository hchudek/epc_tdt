<?php


function m_c1_epc_projects_view($_application, $do) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;


   // Überschrift und Modulsonderzubehör erstellen
   $tabs="&nbsp;&nbsp;Select EP & C Project";


   // Ansicht einbinden
   if(isset($data)) unset($data);
   $app="epc/projects";
   $icon="true";
   $number="true";
   $path="file:///d:/Lotus/phptemp/".$app;
   $handle=opendir($path);
   while($file=readdir($handle)) if(strpos($file,".php")>0) {include_once($path."/".$file);}
   closedir ($handle);
   include_once("addin/generate_extended_view.php");

   // View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
   $dsp_filter["Project No."] = false;
   $dsp_filter["Description"] = false;
   $dsp_filter["Created"] = false;
   $t_view=generate_extended_view($dsp_filter, $path, $column, $data, $_w, $_SESSION["module"][$_module_name], true, true, true, true, true, false, 40, array("use:html",$do));

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_4_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_4_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr><td colspan=\"2\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
   "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
   "</form>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));

   return $module;

}

?>