<?php

include_once("area.php");

function m_a5__my_area($_application){

   global $area;

   // HEADLINE -----------------------------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // BODY ---------------------------------------------------------------------------------------------------------------------
   $myarea = explode(":", $_application["myarea"]);
   foreach($area as $key => $val) {
      if(in_array($val["userrole"], $_SESSION["remote_userroles"])) {
         $img = (in_array($key, $myarea)) ? "status-5" : "status-5-1";
         $module .= 
         "<div style=\"padding:1px;\">".
         "<a href=\"javascript:void(0);\" onclick=\"handle_area(this, true);\" name=\"m_a5__my_area\" key=\"".$key."\">".
         "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/".$img.".png\" style=\"vertical-align:top;margin-right:7px;\"></a>".rawurldecode($val["description"]).
         "</div>\r\n";
      }
   }

   return array($headline, $module);

}


?>