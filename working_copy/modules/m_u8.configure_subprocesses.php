<?php


function m_u8__configure_subprocesses($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = m_u8__configure_subprocesses_generate_html($_application);
   return array($headline, $module);

}

function m_u8__configure_subprocesses_generate_html($_application) {

   $file = process_xml($_SESSION["remote_domino_path_main"]."/v.subprocesses?open&count=99999&function=xml:data");
   $module = 
   "<table id=\"tbl_m_u8__configure_subprocesses\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n".
   "<tr>\r\n".
   "<td><span id=\"tbl_m_u8__configure_subprocesses_activate\" class=\"phpbutton\" style=\"visibility:xhidden; \"><a href=\"javascript:m_u8__configure_subprocesses.activate(false, true);\">Activate changes</a></span><br><br></td>\r\n".
   "<tr>\r\n".
   "<tr>\r\n".
   "<td>\r\n".
   "<input type=\"text\" value=\"\" onkeyup=\"m_u8__configure_subprocesses.search(this)\">".
   "<div id=\"m_u8__subprocesslist\">\r\n".
   "<table id=\"tbl_m_u8__subprocesses\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n";
   foreach($file["subprocess"] as $subprocess) {
      $module .= 
      "<tbody style=\"cursor:pointer;\" onclick=\"m_u8__configure_subprocesses.edit('".$subprocess["@attributes"]["unid"]."');\" key=\"".$subprocess["id"].$subprocess["name"]."\">\r\n".
      "<tr>\r\n".
      "<td><span style=\"display:inline-block; width:70px;\" id=\"".$subprocess["@attributes"]["unid"].":id\">".rawurldecode($subprocess["id"])."</span><span id=\"".$subprocess["@attributes"]["unid"].":name\">".rawurldecode($subprocess["name"])."</span></td>\r\n".
      "</tr>\r\n".
      "</tbody>\r\n";
   }
   $module .= 
   "</table>\r\n".
   "</div>\r\n".
   "</td>\r\n".
   "<td style=\"border-left:solid 30px rgb(255,255,255);\">\r\n";

   $module .= 
   "<span id=\"tbl_m_u8__configure_subprocesses_form\"></span></td>".
   "<td style=\"vertical-align:top; border-left:solid 20px transparent;\"><iframe style=\"display:none;\" id=\"tbl_m_u8__configure_subprocesses_save\"></iframe></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n";

   return str_replace("Array", "", $module);
   

}




?>