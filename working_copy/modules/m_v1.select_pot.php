<?php

session_start();
if($_REQUEST["m_v1_get__project"] != "") print m_v1_get__project($_REQUEST["m_v1_get__project"]);


function m_v1_get__project($prj) {

   if($prj !="") require_once("../../../../library/tools/addin_xml.php");							
   $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.export_pot?open&count=99999&function=plain&restricttocategory=".$prj);

   $lines = explode(":", $file);
   for($i = 0; $i < count($lines); $i++) {
      $v = explode(";", $lines[$i]);
      for($e = 0; $e < count($v); $e++) {
         if($e == 1 && $i > 0) {
            $exp = explode("@", rawurldecode($v[$e]));
            $exp = trim(str_replace("STATUS:", "", $exp[0]));

            $status[0] = "Ungrabbed";
            $status[100] = "Grabbed";
            $status[200] = "Unrealized";
            $status[300] = "Reconditioned";
            $status[999] = "Released";

            $v[$e] = $status[intval($exp)];
         }
         else $v[$e] = rawurldecode($v[$e]);
      }
      if($i == 0) {
         $column = $v;
      }
      else {
         if(trim($v[0]) != "") $data[] = $v;
      }
   }

  

   $arr = array();
   $module = "\r\n".
   "<div id=\"m_v1__select_pot_head\">\r\n".
   "<input type=\"text\" value=\"\" onkeyup=\"m_v1__select_pot.basicsearch(this.value.toLowerCase());\" />\r\n".
   "<p>\r\n".
   "<a>".
   "<img src=\"../../../../library/images/16x16/interface-81.png\" style=\"cursor:pointer; border:none; position:relative; top:3px; left:-4px;\" onclick=\"m_v1__select_pot.do_select_all(true);\" />".
   "<img src=\"../../../../library/images/16x16/interface-82.png\" style=\"cursor:pointer; border:none; position:relative; top:3px; left:-9px;\" onclick=\"m_v1__select_pot.do_select_all(false);\" />".   "</a>";

   for($i = 1; $i < count($column); $i++) $module .= "<a>".$column[$i]."</a>"; 
   $module .= 
   "</p>\r\n".
   "</div>\r\n".
   "<div id=\"m_v1__select_pot_body\">\r\n";

   foreach($data as $val) {
      $module .= "<p><a><img src=\"../../../../library/images/16x16/status-5-1.png\" style=\"border:none; margin-left:2px;\" unique=\"".$val[0]."\" /></a>";
      for($i = 1; $i < count($column); $i++) $module .= "<a>".$val[$i]."</a>"; 
      $module .= "</p>\r\n";   
   }
   $module .= "</div>\r\n";

   return $module;
}


function m_v1__select_pot($_application) {

   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);   
   $module = "";
   return array(strtoupper($headline), $module);

}


?>