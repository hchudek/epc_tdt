<?php

if($_REQUEST["callback"] != "") {
   session_start();
   $jsonp = m_x7__dig_rep_info_jsonp();
   if(strpos($jsonp, "<h2>No documents found<\/h2>")) $jsonp = "";
   print $_REQUEST["callback"]."(".$jsonp.");";
}


function m_x7__dig_rep_info($_application) {

   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   $_SESSION["tracker"] = $_REQUEST["unique"];
   $_SESSION["pot"] = $_REQUEST["pot"];

   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   $module = ($_REQUEST["pot"] == "") ? "<div>No POT selected.</div>" : "<div>Loading ...</div>";

   return array($headline, $module);

}

function m_x7__dig_rep_info_jsonp() {

   require_once("../addin/load_tracker_data.php");

   $load = array("meta");
   $tc_data = create_tc_data($load);
   $key_cr = trim(substr($tc_data["meta"]["id"], strpos($tc_data["meta"]["id"], "+") + 1, strlen($tc_data["meta"]["id"])));

   $file = file_get_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_cr_rms_dig_rep_for_pot?open&count=9999&restricttocategory=".rawurlencode($key_cr)."&function=plain");
   $file = explode(":", $file);

   if(isset($file[0])) {
      $t = explode(";", $file[0]);
      foreach($t as $v) {
         $v = explode("@", $v);
         $th[] = array("value" => $v[0], "width" => $v[1]);
      }
   }
   for($i = 1; $i < count($file); $i++) {
      if(trim($file[$i]) != "") {
         $cnt = 0;
         $collect = array();
         $t = explode(";", $file[$i]);
         foreach($t as $v) {
            $collect[$th[$cnt]["value"]] = $v;
            $cnt++;
         }
         $td[] = $collect;
      }
   }

   $output = json_encode(array("th" => $th, "td" => $td));
   return $output;

}

?>