<?php

function m_a4__linklist($_application){

   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;


   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   $file = file_get_contents($_SESSION["remote_database_path"]."static/linklist/".$_SESSION["remote_shortname"].".php");
   $module = "<section id=\"linklist\">\r\n".utf8_encode($file)."</section>\r\n";


   if(!$_application["grid"]["enabled"] && $file != "") {
      $module =      
      "<span class=\"phpbutton\" style=\"display:none;\" id=\"edit_linklist\"><a href=\"javascript:void(0);\" onclick=\"do_enable_linklist(document.getElementById('linklist').getElementsByTagName('div'));\" style=\"width:100px;text-align:center;\"></a></span>\r\n".
      "<span class=\"phpbutton\" style=\"margin-left:5px;display:none;\" id=\"save_linklist\"><a href=\"javascript:void(0);\" onclick=\"save_linklist('linklist');\">Save linklist</a></span>\r\n".
      "<br />\r\n".
      "<br />\r\n".
      $module;
   }

   return array($headline, "<span id=\"frm_m_a4__linklist\">\r\n".$module."</span>\r\n");

}


?>