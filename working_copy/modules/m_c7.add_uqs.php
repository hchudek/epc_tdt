<?php


include_once("../../../library/tools/addin_xml.php");


function m_c7_add_uqs($_application) {
   $module .= m_c7_get_view_search($_application).m_c7_get_view($_application, "javascript:add_uqs('%%UNID%%', '".$_REQUEST["unique"]."', '".$_REQUEST["pot"]."');");
   return($module);
}

function m_c7_get_view($_application, $do) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;


   // Überschrift und Modulsonderzubehör erstellen
   $tabs="&nbsp;&nbsp;Select UQS";


   // Ansicht einbinden
   if(isset($data)) unset($data);
   $app="epc/uqs";
   $icon="true";
   $number="true";
   $path="file:///d:/Lotus/phptemp/".$app;
   $handle=opendir($path);
   while($file=readdir($handle)) if(strpos($file,".php")>0) {include_once($path."/".$file);}
   closedir ($handle);
   include_once("addin/generate_extended_view.php");

   // View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
   $dsp_filter["Quote num"] = false;
   $dsp_filter["Part num"] = false;
   $dsp_filter["Name"] = false;
   $dsp_filter["Author"] = false;
   $dsp_filter["Tool num"] = false;
   $t_view=generate_extended_view($dsp_filter, $path, $column, $data, $_w, $_SESSION["module"][$_module_name], true, true, true, true, true, false, 40, array("use:html",$do));

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_4_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_4_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr><td colspan=\"2\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
   "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
   "</form>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));

   return $module;

}


function m_c7_get_view_search($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $sel_tab=($_REQUEST[$_SESSION["module"][$_module_name]."_tab"]=="") ? "1" : $_REQUEST[$_SESSION["module"][$_module_name]."_tab"]; 
   $_t_tab_names[]="Search UQS";
   $_t_tab_names[]="Lookup UQS";


   switch($sel_tab) {

      case "1":
         $t_view=
         "<form name=\"form_search_".$_SESSION["module"][$_module_name]."\" action=\"javascript:t_val=document.getElementsByName('search_".$_SESSION["module"][$_module_name]."')[0].value;if(t_val!='') location.href=('?".str_replace("&".$_SESSION["module"][$_module_name]."_q=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_q"]),"",$_SERVER["QUERY_STRING"])."&".$_SESSION["module"][$_module_name]."_q='+escape(t_val.replace(/ /g,'+'))); else void(0);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\">".
         "<tr>".
         "<td><input onkeydown=\"if(event.keyCode==13) document.form_search_".$_SESSION["module"][$_module_name].".submit();\" type=\"text\" value=\"".$_REQUEST[$_SESSION["module"][$_module_name]."_q"]."\" name=\"search_".$_SESSION["module"][$_module_name]."\" style=\"border:solid 1px rgb(99,99,99);width:280px;height:20px;font:normal 12px verdana;\" /></td>".
         "<td>&nbsp;</td>".
         "<td><span class=\"phpbutton\"><a id=\"do_search\" href=\"javascript:document.form_search_".$_SESSION["module"][$_module_name].".submit();\">Search</a></td>".
         "<td>&nbsp;</td>".
         "<td><span class=\"phpbutton\"><a href=\"?".str_replace("&m_c7_q=".$_REQUEST["m_c7_q"], "", str_replace("+"," ",$_SERVER["QUERY_STRING"]))."\">Clear</a></td>".
         "<tr>".
         "</table></form>";
         break;

      case "2":
          $t_view=
         "<form name=\"form_lookup_".$_SESSION["module"][$_module_name]."\" action=\"javascript:t_val=document.getElementsByName('lookup_".$_SESSION["module"][$_module_name]."')[0].value;if(t_val!='') location.href=('?".str_replace("&".$_SESSION["module"][$_module_name]."_sortkey=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_sortkey"]),"",str_replace("&".$_SESSION["module"][$_module_name]."_key=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_key"]),"",$_SERVER["QUERY_STRING"]))."&".$_SESSION["module"][$_module_name]."_key='+escape(t_val.replace(/ /g,'+'))+'&".$_SESSION["module"][$_module_name]."_sortkey='+escape(document.getElementById('".$_SESSION["module"][$_module_name]."_selected_value').innerText.replace(/ /g,'+'))); else void(0);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\">".
         "<tr>".
         "<td><input onkeydown=\"if(event.keyCode==13) document.form_lookup_".$_SESSION["module"][$_module_name].".submit();\" type=\"text\" value=\"".$_REQUEST[$_SESSION["module"][$_module_name]."_key"]."\" name=\"lookup_".$_SESSION["module"][$_module_name]."\" style=\"border:solid 1px rgb(99,99,99);width:280px;height:20px;font:normal 12px verdana;\" /></td>".
         "<td>&nbsp;</td>".
         "<td><span id=\"selector_lookup_".$_SESSION["module"][$_module_name]."\">&nbsp;</span></td>".
         "<td>&nbsp;</td>".
         "<td><span class=\"phpbutton\"><a id=\"do_lookup\" href=\"javascript:document.form_lookup_".$_SESSION["module"][$_module_name].".submit();\">Lookup</a></td>".
         "<tr>".
         "</table></form>";
         break;

   }

   $tabs=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tabs\">".
   "<tr><td><img src=\"../../../../library/images/icons/search.gif\" style=\"margin-right:7px;filter:Gray();\" /></td>";

   for($i=0; $i<count($_t_tab_names);$i++) {
      $class=($sel_tab==$i+1) ? "high" : "reg";
      $style=($sel_tab==$i+1) ? " style=\"background-color:#822433;\"" : "";
      $tabs.="<td class=\"".$class."\"".$style."><a href=\"?&unique=".$_REQUEST["unique"]."&pot=".$_REQUEST["pot"]."&m_c7_tab=".($i+1)."\">".$_t_tab_names[$i]."</a></td>";
   }

   $tabs.=
   "</tr>".
   "</table>";

   $module=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;margin-bottom:2px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_5_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_5_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\" class=\"module_5_content\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table><br />\r\n";

   $module=str_replace("%%TABS%%", $tabs, $module);
   return $module;

}


?>