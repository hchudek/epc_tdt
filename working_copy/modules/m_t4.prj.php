<?php


function m_t4__prj($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   return array($headline, $module);

}


function m_t4__prj_generate_html($_application) {
   global $tc_data;



$ref_project = ($tc_data["project"]["ref_number"]) ? strtoupper(" [".$tc_data["project"]["ref_number"]."]") : "";

$arr= array("A","B","C","D","E","F","G"); 
$selectbox = "<select onchange=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_leanpd"]."', '".$tc_data["project"]["unid"]."', 'prj_complex', this[this.selectedIndex].value, '');\">;".
"<option></option>";

foreach ($arr as $val) {
  $select=($tc_data["project"]["complex"]==$val) ? " selected": "";
  $selectbox.="<option value=\"".$val."\"".$select.">".$val."</option>";

}

   $module =
   "<table id=\"tbl_t4__prj\">".
   "<tr>".
   "<td class=\"label\">Project number</td>".
   "<td class=\"data\" colspan=\"3\"><span id=\"project_number\">".$tc_data["tracker"]["project_number"].$ref_project."</span></td>".
   "</tr>".
   "<tr>".
   "<td class=\"label\">Description</td>".
   "<td class=\"data\" colspan=\"3\">".$tc_data["prj"]["description"]."</td>".
   "</tr>".
   "<tr>".
   "<td class=\"label\">Current Phase</td>".
   "<td class=\"data\">".$tc_data["prj"]["phasedesc"]."</td>".
   "<td class=\"label\">Manufacturing City</td>".
   "<td class=\"data\">".$tc_data["prj"]["city"]."</td>".
   "</tr>".
   
   "<tr>".
   "<td class=\"label\">Project complexity</td>".
   "<td class=\"data\" colspan=\"3\">".$selectbox."</td>".
   "</tr>".
   "<tr>".
   "<td class=\"headline\" colspan=\"4\">Responsibles</td>".
   "</tr>".
   "<tr>".
   "<td class=\"label\">Production Manager</td>".
   "<td class=\"data\" colspan=\"3\">".$tc_data["prj"]["prodmngr"]."</td>".
   "</tr>".
   "<tr>".
   "<td class=\"label\">Project Manager</td>".
   "<td class=\"data\" colspan=\"3\">".$tc_data["prj"]["projmngr"]."</td>".
   "</tr>".

   "</table>";
   return str_replace("Array","",$module);

}

/*


   $ref_project = ($tc_data["project"]["ref_number"]) ? strtoupper(" [".$tc_data["project"]["ref_number"]."]") : "";

$arr= array("A","B","C","D","E","F","G"); 
$selectbox = "<select onchange=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_leanpd"]."', '".$tc_data["project"]["unid"]."', 'prj_complex', this[this.selectedIndex].value, '');\">;".
"<option></option>";

foreach ($arr as $val) {
  $select=($tc_data["project"]["complex"]==$val) ? " selected": "";
  $selectbox.="<option value=\"".$val."\"".$select.">".$val."</option>";

}
$selectbox .= "</select>";
   $html = 

   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px; table-layout:fixed;\">".
   "<thead>".
   "<tr>".
   "<th colspan=\"12\"><img id=\"m_b1_projectinfo_section_1_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_1')\" /><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Project data headline');\">Project data</span></th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_1\" style=\"display:table-row-group;\">".
   "<tr>".
   "<td class=\"td_reg\" colspan=\"2\">Project number</td>".
   "<td class=\"td_reg\" colspan=\"4\"><span id=\"project_number\">".$tc_data["project"]["number"].$ref_project."</span></td>".
   "<td class=\"td_reg\" colspan=\"2\">Description</td>".
   "<td class=\"td_reg\" colspan=\"4\">".urldecode($tc_data["project"]["description"])."</td>".
   "</tr>".
   "<tr>".
   "<td class=\"td_alt\" colspan=\"2\">Current Phase</td>".
   "<td class=\"td_alt\" colspan=\"4\">".$tc_data["project"]["phasedesc"]."</td>".
   "<td class=\"td_alt\" colspan=\"2\">Manufacturing City</td>".
   "<td class=\"td_alt\" colspan=\"4\">".$tc_data["project"]["city"]."</td>".
   "</tr>".
   
   "<tr>".
   "<td class=\"td_reg\" colspan=\"2\">Project complexity</td>".
   "<td class=\"td_reg\" colspan=\"10\">".$selectbox."</td>".

   
   "</tr>".



   "</tbody>".
   "</table>".

   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px;\">".
   "<thead>".
   "<tr>".
   "<th colspan=\"12\"><img id=\"m_b1_projectinfo_section_3_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_3')\" /><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Responsibles headline');\">Responsibles</span></th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_3\" style=\"display:table-row-group;\">".
   "<tr>".
   "<td class=\"td_reg\" colspan=\"2\">Production Manager</td>".
   "<td class=\"td_reg\" colspan=\"4\">".urldecode($tc_data["project"]["prodmngr"])."</td>".
   "<td class=\"td_reg\" colspan=\"2\">Project Manager</td>".
   "<td class=\"td_reg\" colspan=\"4\">".urldecode($tc_data["project"]["projmngr"])."</td>".
   "</tr>".

   "</tbody>".
   "</table>".


  "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px;\">".
   "<thead>".
   "<tr>".
   "<th colspan=\"12\"><img id=\"m_b1_projectinfo_section_5_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_5')\" /><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Parts regarding headline');\">Parts regarding to this Tracking case</span></th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_5\" style=\"margin-bottom:6px;\" class=\"m_b2_case\" style=\"display:table-row-group;\">".
	"<tr>".
	   "<th colspan=\"2\">Part Info</th>".
	   "<th colspan=\"1\">Part No.</th>".
	   "<th colspan=\"4\">Part Desc.</th>".
	   "<th colspan=\"1\">Loop</th>".
	   "<th colspan=\"1\">Process Type</th>".
	   "<th colspan=\"3\">Supplier</th>".
	"</tr>";

     if(count($tc_data["project"]["selparts"]["number"])<=1){
       $html .="<tr>".
		"<td colspan=\"2\" class=\"td_reg\">".str_replace('href="http://partinquiry.us.tycoelectronics.com', 'target="_blank" href="http://partinquiry.us.tycoelectronics.com', urldecode($tc_data["project"]["selparts"]["pic"]))."</td>".		
		"<td colspan=\"1\" class=\"td_reg\"><a class=\"linkdata\" href=\"edit_tracker.php?&unique=".$tc_data["tracker"]["unique"]."&pot=".$tc_data["project"]["selparts"]["partunique"]."\" >".$tc_data["project"]["selparts"]["number"]."<a></td>".
		"<td colspan=\"4\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["name"])."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["loop"])."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["ptype"])."</td>".
		"<td colspan=\"3\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["supplier"])."</td>".
	     "</tr>";
     }else{ 
       for($i = 0; $i < count($tc_data["project"]["selparts"]["number"]); $i++) {
          $dsp = explode("/", str_replace("TN ", "", str_replace("PN ", "", $tc_data["tracker"]["pot_dsp_name"][$i])));

       $html .="<tr>".
		"<td colspan=\"2\" class=\"td_reg\">".str_replace('href="http://partinquiry.us.tycoelectronics.com', 'target="_blank" href="http://partinquiry.us.tycoelectronics.com', urldecode($tc_data["project"]["selparts"]["pic"][$i]))."</td>".		
		"<td colspan=\"1\" class=\"td_reg\"><a class=\"linkdata\" href=\"edit_tracker.php?&unique=".$tc_data["tracker"]["unique"]."&pot=".$tc_data["project"]["selparts"]["partunique"][$i]."\" >".$tc_data["project"]["selparts"]["number"][$i]."<a></td>".
		"<td colspan=\"4\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["name"][$i])."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["loop"][$i])."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["ptype"][$i])."</td>".
		"<td colspan=\"3\" class=\"td_reg\">".urldecode($tc_data["project"]["selparts"]["supplier"][$i])."</td>".
	     "</tr>";
     }
   }

   $html .= "</tbody>".

   "</table>".


   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px;\">".
   "<thead>".
   "<tr>".
     "<th colspan=\"12\"><img id=\"m_b1_projectinfo_section_4_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_4')\" /><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Tools regarding headline');\">Tools regarding to this Tracking case</span></th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_4\" style=\"margin-bottom:6px;\" class=\"m_b2_case\" style=\"display:table-row-group;\">".
	"<tr>".
	   "<th colspan=\"2\">Tool No.</th>".
	   "<th colspan=\"4\">Tool Desc.</th>".
	   "<th colspan=\"4\">Supplier</th>".
	   "<th colspan=\"1\">PO</th>".
	   "<th colspan=\"1\"  style=\"text-align:center\" >Position</th>".
	"</tr>";

 
    if(count($tc_data["project"]["seltools"]["number"])<=1){
       $html .="<tr>".		
		"<td colspan=\"2\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["number"])."</td>".
		"<td colspan=\"4\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["name"])."</td>".
		"<td colspan=\"4\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["supplier"])."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["saplink"])."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$tc_data["project"]["seltools"]["pono"]."</td>".
		"<td colspan=\"1\" class=\"td_reg\" style=\"text-align:center\" >".$tc_data["project"]["seltools"]["position"]."</td>".
	     "</tr>";
    }else{
        for($i = 0; $i < count($tc_data["project"]["seltools"]["number"]); $i++) {
           $dsp = explode("/", str_replace("TN ", "", str_replace("PN ", "", $tc_data["tracker"]["pot_dsp_name"][$i])));
         $html .="<tr>".		
		"<td colspan=\"2\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["number"][$i])."</td>".
		"<td colspan=\"4\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["name"][$i])."</td>".
		"<td colspan=\"4\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["supplier"][$i])."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["seltools"]["saplink"][$i])."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$tc_data["project"]["seltools"]["pono"][$i]."</td>".
		"<td colspan=\"1\" class=\"td_reg\" style=\"text-align:center\" >".$tc_data["project"]["seltools"]["position"][$i]."</td>".
	     "</tr>";
        }
   }

   $html .= "</tbody>".

   "</table>".

   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px;\">".
   "<thead>".
   "<tr>".
   "<th colspan=\"12\"><img id=\"m_b1_projectinfo_section_2_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_2')\" /><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Project Case data headline');\">Project Case data</span></th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_2\" style=\"margin-bottom:6px;\" class=\"m_b2_case\" style=\"display:table-row-group;\">".
	"<tr>".
	   "<th colspan=\"1\">Number</th>".
	   "<th colspan=\"2\">Name</th>".
	   "<th colspan=\"1\">Date issued</th>".
	   "<th colspan=\"1\">Status</th>".
	   "<th colspan=\"2\">Initiator</th>".
	   "<th colspan=\"1\">Customer</th>".
	   "<th colspan=\"2\">T&PM</th>".
	   "<th colspan=\"2\">PE Manager</th>".
	"</tr>";

  if(count($tc_data["project"]["case"]["number"])=="1"){
     $html .="<tr>".
		"<td colspan=\"1\" class=\"td_reg\"><a href=\"".$_SESSION["remote_domino_path_epcdis"]."/v.byno/".$tc_data["project"]["case"]["number"]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["project"]["case"]["number"]."</a></td>".
		"<td colspan=\"2\" class=\"td_reg\">".urldecode($tc_data["project"]["case"]["name"])."</td>";

		if($tc_data["project"]["case"]["issued"]=="01.01.1900"){
		   $html .= "<td colspan=\"1\" class=\"td_reg\">no Date</td>";
		}else{
		   $html .= "<td colspan=\"1\" class=\"td_reg\">".date($_application["preferences"]["settings"]["date_format"], strtotime($tc_data["project"]["case"]["issued"]))."</td>";
		}
		$html .= "<td colspan=\"1\" class=\"td_reg\">".$tc_data["project"]["case"]["status"]."</td>".
		"<td colspan=\"2\" class=\"td_reg\">".get_person(urldecode($tc_data["project"]["case"]["initiator"]))."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["case"]["customer"])."</td>".
		"<td colspan=\"2\" class=\"td_reg\">".get_person(urldecode($tc_data["project"]["case"]["tpm"]))."</td>".
		"<td colspan=\"2\" class=\"td_reg\">".get_person(urldecode($tc_data["project"]["case"]["pe"]))."</td>".
	     "</tr>";
  }else{
  for($i = 0; $i < count($tc_data["project"]["case"]["number"]); $i++) {
     $html .="<tr>".
		"<td colspan=\"1\" class=\"td_reg\"><a href=\"".$_SESSION["remote_domino_path_epcdis"]."/v.byno/".$tc_data["project"]["case"]["number"][$i]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["project"]["case"]["number"][$i]."</a></td>".
		"<td colspan=\"2\" class=\"td_reg\">".urldecode($tc_data["project"]["case"]["name"][$i])."</td>";

		if($tc_data["project"]["case"]["issued"][$i]=="01.01.1900"){
		   $html .= "<td colspan=\"1\" class=\"td_reg\">no Date</td>";
		}else{
		   $html .= "<td colspan=\"1\" class=\"td_reg\">".date($_application["preferences"]["settings"]["date_format"], strtotime($tc_data["project"]["case"]["issued"][$i]))."</td>";
		}
		$html .= "<td colspan=\"1\" class=\"td_reg\">".$tc_data["project"]["case"]["status"][$i]."</td>".
		"<td colspan=\"2\" class=\"td_reg\">".get_person(urldecode($tc_data["project"]["case"]["initiator"][$i]))."</td>".
		"<td colspan=\"1\" class=\"td_reg\">".urldecode($tc_data["project"]["case"]["customer"][$i])."</td>".
		"<td colspan=\"2\" class=\"td_reg\">".get_person(urldecode($tc_data["project"]["case"]["tpm"][$i]))."</td>".
		"<td colspan=\"2\" class=\"td_reg\">".get_person(urldecode($tc_data["project"]["case"]["pe"][$i]))."</td>".
	     "</tr>";
   };
   }

   $html .= "</tbody>".

   "</table>";

   /* SOURCING ALT

   $html .= 

   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px;\">".
   "<thead>".
   "<tr>".
    "<th colspan=\"12\"><img id=\"m_b1_projectinfo_section_6_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_6')\" /><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Sourcing Data headline');\">Sourcing Data</span></th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_6\" style=\"margin-bottom:6px;\" class=\"m_b2_case\" style=\"display:table-row-group;\">".
	"<tr>".
	   "<th colspan=\"2\">UQS Number</th>".
	   "<th colspan=\"8\">Desc</th>".
	   "<th colspan=\"2\">ToolNo</th>".
	"</tr>";

  if(count($tc_data["project"]["uqs"]["number"])=="1"){
      $html .="<tr>".
		  "<td colspan=\"2\" class=\"td_reg\"><a href=\"javascript:fade_in_out('uqs_info_1')\"><img src=\"../../../../library/images/16x16/business-5.png\" style=\"position:absolute;\" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".create_uqs_data($tc_data["project"]["uqs"]["unid"], $tc_data["project"]["uqs"]["toolno"], 1)."<a href=\"".$_SESSION["remote_domino_path_epcquoting"]."/0/".$tc_data["project"]["uqs"]["unid"]."?open&key=rfq\" class=\"linkdata\" target=\"_blank\">".$tc_data["project"]["uqs"]["number"]." / ".$tc_data["project"]["uqs"]["snumber"]."</a></td>".
		  "<td colspan=\"8\" class=\"td_reg\">".urldecode($tc_data["project"]["uqs"]["desc"])."</td>".
		  "<td colspan=\"2\" class=\"td_reg\">".urldecode($tc_data["project"]["uqs"]["toolno"])."</td>".
	       "</tr>";
  }else{
    for($i = 0; $i < count($tc_data["project"]["uqs"]["number"]); $i++) {
       $html .="<tr>".
		  "<td colspan=\"2\" class=\"td_reg\"><nobr><a href=\"javascript:fade_in_out('uqs_info_".($i + 1)."')\"><img src=\"../../../../library/images/16x16/business-5.png\" style=\"position:absolute;\" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".create_uqs_data($tc_data["project"]["uqs"]["unid"][$i], $tc_data["project"]["uqs"]["toolno"][$i], ($i + 1))."<a href=\"".$_SESSION["remote_domino_path_epcquoting"]."/0/".$tc_data["project"]["uqs"]["unid"][$i]."?open&key=rfq\" class=\"linkdata\" target=\"_blank\">".$tc_data["project"]["uqs"]["number"][$i]." / ".$tc_data["project"]["uqs"]["snumber"][$i]."</a></nobr></td>".
		  "<td colspan=\"8\" class=\"td_reg\">".urldecode($tc_data["project"]["uqs"]["desc"][$i])."</td>".
		  "<td colspan=\"2\" class=\"td_reg\">".urldecode($tc_data["project"]["uqs"]["toolno"][$i])."</td>".
	       "</tr>";
    };
   }


   $html .= "</tbody>".
   "</table>";

   SOURCING ALT */

/*

function get_person($name) {
   global $tc_data;
   $sn = "";
   foreach($tc_data["project"]["nab"]["name"] as $val) {
      if(rawurldecode($val["cn"]) == $name) {
         $sn = strtolower($val["sn"]);
         break;
      }
   }

   $r = 
   "<table border=\"0\"style=\"font-size: 1.0em;\">".
   "<tr>".
   "<td><img border=\"0\" align=\"left\" style= \"margin-right: 20px;\" src=\"http://phone.tycoelectronics.com/imgupload/pics/".$sn."_JpegPhoto.jpeg\" onError=\"nopic(this)\" style=\"width: 26px; height: 26px\" /></td>".
   "<td><a href=\"http://phone.tycoelectronics.com/search.asp?s=".$sn."\" class=\"linkdata\" target=\"_blank\">".$name."</a></td>".
   "</tr>".
   "</table>";
			
   return $r;
}


function create_uqs_data($tunid, $tid, $id) {

   global $tc_data;

   $settings_data = generate_xml($_SESSION["remote_domino_path_epcquoting"]."/v.get_settings_data/".$tunid."?open");


   if(count($settings_data) == 0) {
      return false;
   }
   else {

      $tva = $settings_data["variants"];
      $tse = $settingsdata["selected_variant"];
      $tool = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\" id=\"uqs_info_".$id."\" style=\"z-index:99999;border:dotted 1px rgb(99,99,99);background-color:rgb(255,255,255);position:absolute;display:none;font:normal 12px century gothic,verdana;\">";

      $headline = true;
      $keys = array_keys($settings_data["settings-block"]);
      foreach($keys as $key) {
         $dsp_key = $key;
         $tool .= "<tr><td style=\"text-align:center;background-color:rgb(255,194,54);\" colspan=\"".(1 + $tva)."\">".$dsp_key."</td></tr>";
         if($headline == true) {
            $tool .= "<tr><td>&nbsp;</td>";
            for($i = 0; $i < $tva; $i++) {
               $tool .= "<td style=\"text-align:right;\">".$settings_data["settings-block"]["base.tool"]["titles"]["value"][$i]."</td>";
            }
            $headline = false;
         }
         $tool .= "</tr>";
         unset($sum); $tmp_count = 1;
         foreach($settings_data["settings-block"][$key]["rows"]["row"] as $val) {
            if($key == "conv.kits") $val["label"] = "Converion kit ".$tmp_count;
            $tmp_count++;
            $tool .= "<tr>".
            "<td style=\"padding-right:30px;\">".$val["label"]."</td>";
            for($i = 0; $i < $tva; $i++) {
               if(isset($val["value"][$i])) {
                  if($tva == 1) $v = ($i == ($tse + 1)) ? "<b>".str_replace(",", ".", $val["value"])."</b>" : str_replace(",", ".", $val["value"]);
                  else $v = ($i == ($tse + 1)) ? "<b>".str_replace(",", ".", $val["value"][$i])."</b>" : str_replace(",", ".", $val["value"][$i]);
               }
               else $v = "&nbsp;";

               $tool .= "<td style=\"text-align:right;background-color:rgb(220,220,220);\">&nbsp;".str_replace("Array", "", strval($v))."</td>";
               if(isset($val["value"][$i]) && $val["summary"] == "1") {
                  if($tva == "1") $sum[$i] += floatval(str_replace(",", ".", $val["value"]));
                  else $sum[$i] += floatval(str_replace(",", ".", $val["value"][$i]));
               }
            }
            $tool .= "</tr>";
         }
         $tool .= "<tr><td>Sum</td>";
         for($i = 0; $i < $tva; $i++) {
            $v = ($i == ($tse + 1)) ? "<b>".$sum[$i]."</b>" : $sum[$i];
            $tool .= "<td style=\"text-align:right;\">".$v."</td>";
         }  
         $tool .= "</tr>";
      }
      $tool .= "<tr><td>&nbsp;</td></tr></table>";
   }

   return $tool;


}

*/

?>

