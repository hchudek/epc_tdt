<?php
function m_u1_edit_userpages($_application) {

   global $_userpage;

   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   $t_userpage=base64_decode($_REQUEST["page"]);

   $img1=($_application["page"][$t_userpage]["status"]=="active") ? "select_high" : "select_reg";
   $img2=($_application["page"][$t_userpage]["status"]=="active") ? "select_reg" : "select_high";
   $val=($_application["page"][$t_userpage]["status"]=="active") ? "active" : "inactive";


   $button["save_userpage"]=
   "<span class=\"phpbutton\"><a href=\"javascript:void(0)\" onclick=\"save_userpage('".$_SESSION[remote_domino_path_perportal]."','".$_SESSION[remote_perportal_unid]."','".base64_decode($_REQUEST["page"])."');\">Save</a></span>\r\n";

   $button["cancel"]=
   "<span class=\"phpbutton\"><a href=\"index.php\">Cancel</a></span>\r\n";

   $form = 
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"text\" style=\"margin-top:9px;padding:2px;width:99%;\"><tr>".
   "<td style=\"width:176px;\" class=\"text\">Userpage status:</td>".
   "<td style=\"vertical-align:top;width:1%;\">".
   "<a href=\"javascript:void(0);\" onclick=\"handle_userpage('isactive','active')\"><img name=\"userpage_t_isactive\" src=\"../../../../library/images/icons/".$img1.".png\" style=\"width:14px;border:none;margin-right:5px;position:relative;top:3px;\" /></a>".
   "Active&nbsp;</td>".
   "<td style=\"vertical-align:top;\">".
   "<a href=\"javascript:handle_userpage('isactive','inactive')\"><img name=\"userpage_t_isactive\" src=\"../../../../library/images/icons/".$img2.".png\" style=\"width:14px;border:none;margin-right:5px;position:relative;top:3px;\" /></a>".
   "Inactive</td>".
   "</tr></table><div id=\"userpage_isactive\" style=\"display:none;\">".$val."</div>";


   $form.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:23px;margin-bottom:23px;\"><tr>".
   "<td style=\"width:180px;\" class=\"text\">Userpage name:</td>".
   "<td><input type=\"text\" style=\"border:solid 1px rgb(99,99,99);width:250px;\" maxlength=\"15\" name=\"userpage_name\" value=\"".$_application["page"][$t_userpage]["page_descr"]."\" />".
   "</tr></table>".
   "<input type=\"hidden\" id=\"userpage\" name=\"td$".str_replace(" ","",$t_userpage)."\" value=\"\" />".
   "<input type=\"hidden\" id=\"userpage_blocks\" name=\"td$".str_replace(" ","",$t_userpage)."_blocks\" value=\"\" />";


   // TEMPLATES HINZUFÜGEN
   $form.="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
   $val="0";
   for($i=1;$i<=4;$i++) {
      if(substr($_application["page"][$t_userpage]["template"]["name"],-1)==$i) {
         $img="select_high";
         $val=$i;
      }
      else $img="select_reg";
      $form.=
      "<td style=\"vertical-align:top;\"><a href=\"javascript:void(0);\" onclick=\"handle_userpage('style','".$i."')\"><img name=\"userpage_t_style\" src=\"../../../../library/images/icons/".$img.".png\" style=\"width:14px;margin-top:12px;border:none;\" /></a></td>".
      "<td><img src=\"../../../../library/images/t1_v".$i."_big.png\" style=\"margin-right:8px;\"/></td>";
   }
   $form.=
   "</tr></table><div id=\"userpage_style\" style=\"display:none;\">".$val."</div>";


   // KONFIGURATOR HINZUFÜGEN
   $configurator="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"userpage_modules\"><tr><th>Module name</th><th>Description</th><th>Best use</th></tr><tr>";

   $count=0; foreach($_userpage["module"] as $value) {
      $count++;
      $css=(strpos(" ".$_application["page"][$t_userpage]["blocks"], $_userpage[$value]["id"])==false) ? "reg" : "out";
      $configurator.=
      "<tr id=\"module_".$count."\" class=\"".$css."\" onclick=\"select_module(".$count.")\">".
      "<td id=\"module_name_".$value."\" style=\"width:1%;\"><nobr>".$_userpage[$value]["name"]."&nbsp;&nbsp;&nbsp;</nobr><div id=\"module_id_".$value."\" style=\"display:none;\">".$_userpage[$value]["id"]."</div><div id=\"module_name_".$count."\" style=\"display:none;\">".$_userpage[$value]["name"]."</div><div id=\"module_t_name_".$count."\" style=\"display:none;\">".$value."</div></td>".
      "<td>".$_userpage[$value]["description"]."</td>".
      "<td>".implode(", ", $_userpage[$value]["fit"])."&nbsp;</td>".
      "</tr>";
   }

   $configurator.="</table>";
   $configurator.="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"userpage_configuration\"><tr>";
   $block=explode(":", $_application["page"][$t_userpage]["blocks"]);

   switch (substr($_application["page"][$t_userpage]["template"]["name"],-1)) {
      case 1:
         $embed_block=array("1","2","3");
         break;
      case 2:
         $embed_block=array("1","2");
         break;
      case 3:
         $embed_block=array("2","3");
         break;
      case 4:
         $embed_block=array("2");
         break;
   }


   for($d=0;$d<3;$d++) {
      $css=(in_array(($d+1),$embed_block)) ? "reg" : "out";
      $isactive=(in_array(($d+1),$embed_block)) ? "true" : "false";
      $bgcolor=(in_array(($d+1),$embed_block)) ? "#ffffff" : "#eeeeee";
      $dsp=(in_array(($d+1),$embed_block)) ? "visible" : "hidden";
      $width=($d==1)? "50%" : "25%";
      $configurator.="<td id=\"configurator_block_".($d+1)."\" style=\"background-color:".$bgcolor.";width:".$width.";\"><div style=\"display:none;\">".$isactive."</div><div style=\"padding:2px;border-bottom:solid 2px rgb(99,99,99);width:100%;\"><span class=\"text\">Block ".($d+1)."&nbsp;&nbsp;</span><span id=\"move_modules_".($d+1)."\" style=\"visibility:".$dsp.";\"><a href=\"javascript:add_to_module(".($d+1).")\"><img src=\"../../../../library/images/icons/arrow_down.png\" border=\"0\" /></a><a href=\"javascript:delete_from_module(".($d+1).")\"><img src=\"../../../../library/images/icons/arrow_up.png\" border=\"0\" /></a></span></div>";
      $modules=explode(",", $block[$d]);
      for($i=0;$i<10;$i++) {
         $m=(count($modules)>$i) ? $modules[$i] : "";
         $m=generate_block_text($m, $_userpage);
         $configurator.="<div><input readonly=\"readonly\" onclick=\"select_active_module(".($d+1).",".($i+1).")\" class=\"".$css."\" style=\"width:100%;\" type=\"text\" name=\"configurator_".($d+1)."\" value=\"".$m."\" /></div>"; 
      }
      $configurator.="</td>";
   }

   $configurator.="</td></tr></table>";


   $form.=$configurator."<div id=\"select_module\" style=\"display:none;\"></div><div id=\"select_module_number\" style=\"display:none;\"></div>";

   $module="<div style=\"border-bottom:solid 2px rgb(99,99,99);width:100%;padding-top:5px;padding-bottom:5px;\">".$button["save_userpage"].$button["cancel"]."</div><br />Basic data for ".ucfirst(substr($t_userpage,0,strlen($t_userpage)-1)." ".substr($t_userpage,strlen($t_userpage)-1,1)).
   "<form method=\"post\" action=\"".$_SESSION["remote_domino_path_perportal"]."/0/".$_SESSION["remote_perportal_unid"]."?savedocument&r=".urlencode($_SESSION["level"]["link"][1])."\" name=\"_DominoForm\">\r\n".
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"domino_form\" style=\"width:100%;\">".
   "<tr>".
   "<td class=\"module_2_content\">".$form."</td></tr></table>\r\n".
   "</form>\r\n";


   $module=str_replace("%%TABS%%", $tabs, $module);

   if($_REQUEST["page"] == "") return "";
   else return $module;

}


function generate_block_text($m, $_userpage) {
   if($m=="<empty>") $return="";
   else {
      foreach ($_userpage["module"] as $value) {
         if($_userpage[$value]["id"]==$m) $return=$_userpage[$value]["name"]." [".$value."]";
      }
   }
   return $return;
}


?>