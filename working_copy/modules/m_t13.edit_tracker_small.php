<?php

if($_REQUEST["m_t13__edit_tracker_small"] == "get_editors") {
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "modules"));
   $load = array("tracker");
   require_once($path."addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   foreach($tc_data["tracker"]["editors"]["name"] as $editor) print "<p value=\"".rawurldecode($editor)."\"><a>".rawurldecode($editor)."</a></p>\r\n";
   die;
}


function m_t13__edit_tracker_small($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   //$module = m_t13__edit_tracker_small_generate_html($_application);
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";

   return array($headline, $module);

}


function m_t13__edit_tracker_small_generate_html($_application) {

//   $load = array("tracker", "project");
//   require_once("addin/load_tracker_data.php");
//   $tc_data = create_tc_data($load);

   global $tc_data;


   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "modules"));
   require_once($path."addin/basic_php_functions.php");


   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   
   $module =
   "<table border=\"0\" cellspacing=\"2\" cellpadding=\"0\" id=\"tbl_m_t13__edit_tracker_data_small\" unid=\"".$tc_data["tracker"]["unid"]."\">".
   "<tr>".
   "<td colspan=\"2\" class=\"divider\" style=\"border:none; background:rgb(255,255,255); font-weight:bold; position:relative; top:-2px;\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/Edition-35.png\" style=\"vertical-align:bottom; margin-bottom:1px; margin-right:6px;\">Basic tracker data</td>\r\n".
   "</tr>\r\n".
   "<tr>".
   "<td>Number&nbsp;&nbsp;</td>".
   "<td>".$tc_data["tracker"]["number"]."</td>".
   "</tr>".
   "<tr>".
   "<td><history field=\"responsible\" msg=\"TPM change log\" oncontextmenu=\"protocol.embed(this); return false;\">T&PM</history></td>".
   "<td><input check=\"tpm\" style=\"border:solid 1px rgb(169,169,169); height:19px; padding-left:3px;\" unid=\"".$tc_data["tracker"]["unid"]."\" onkeyup=\"m_t13.save(this, false);\" name=\"m_t13__edit_tracker_small\" class=\"smartcombo_combobox\" sizex=\"149\" sizey=\"76\" value=\"".$tc_data["tracker"]["responsible"]."\"></td>\r\n".
   "</tr>".
   "<tr>".
   "<td>Created</td>".
   "<td>".rawurldecode($tc_data["tracker"]["created_by"])."</td>".
   "</tr>\r\n".
   "<tr>".
   "<td>Date</td>".
   "<td>".format_date($tc_data["tracker"]["created_date"])."</td>".
   "</tr>\r\n".
   "<tr>".
   "<td>Archived</td>".
   "<td>".rawurldecode($tc_data["tracker"]["archived"])."</td>".
   "</tr>\r\n".
   "<tr>".
   "<td>Project&nbsp;&nbsp;</td>".
   "<td>".$tc_data["project"]["number"]."</td>".
   "</tr>".
   "<tr>".
   "<td colspan=\"2\" class=\"divider\" style=\"border:none; background:rgb(255,255,255); font-weight:bold; padding-top:7px;\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/Edition-35.png\" style=\"vertical-align:bottom; margin-bottom:1px; margin-right:6px;\"><history field=\"tracker_descr\" oncontextmenu=\"protocol.embed(this); return false;\">Description</history></td>".
   "</tr>\r\n".
   "<tr>".
   "<td colspan=\"2\" style=\"border:none; background:rgb(255,255,255);\"><div check=\"description\" id=\"tracker_description\" class=\"div_editable\" contenteditable=\"true\" onkeyup=\"window.setTimeout('edit_tracker_small.save_descr(\\'' + this.getAttribute('id') + '\\', \\'".$tc_data["tracker"]["unid"]."\\')', 2000);\" onkeydown=\"edit_tracker_small.set_clock();\">".utf8_encode(rawurldecode($tc_data["tracker"]["description"]))."</div></td>".   "</tr>\r\n".
   "</table>";


   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   return str_replace("Array", "", $module);

}


function get_responsible($tc_data, $responsible) {

   $html = "<select onchange=\"handle_save_single_field('".$tc_data["tracker"]["unid"]."', 'responsible', this.value, 'location.href=location.href');\" style=\"width:212px;font:normal 11px century gothic;height:19px;background-color:rgb(255,255,255);z-index:999999;filter:alpha(opacity=92);\">\r\n";
   foreach($tc_data["tracker"]["editors"]["name"] as $editor) {
      $selected = ($responsible == $editor) ? " selected" : ""; 
      $html .= "<option value=\"".$editor."\"".$selected.">".$editor."</option>\r\n";
   }   
   $html .= "</select>\r\n";

   return "<div id=\"change_responsible\" style=\"display:none;position:absolute;left:11px;\">".$html."</div>";
}



?>