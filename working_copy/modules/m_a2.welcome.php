<?php

function m_a2__welcome($_application){

   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $eval=file_get_contents($_SESSION["remote_domino_path_main"]."/a.get_last_login?open&me=".urlencode($_SESSION["domino_user"]));
   eval($eval);

   $lang = ($_application["preferred_language"] == "") ? "english" : strtolower($_application["preferred_language"]);
   $content = file_get_contents($_SESSION["remote_database_path"]."static/".__FUNCTION__."/welcome_english.html");

   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   $module=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"welcome\" style=\"width:100%;\">".
   "<tr><td class=\"td1\" style=\"border-bottom:solid 2px rgb(99,99,99);\"><div class=\"welcome\" style=\"padding:0px;margin:1px 1px 1px 7px;position:relative;top:-2px;\">Welcome, ".substr($_SESSION["domino_user"],3,strpos($_SESSION["domino_user"],"/")-3)."</td>".
   "<td align=\"right\" class=\"td1\" style=\"border-bottom:solid 2px rgb(99,99,99);\"><span class=\"text\" style=\"\">&nbsp;&nbsp;[last login ".$_SESSION["last_login"]."]&nbsp;</span></div></td></tr>".
   "<tr><td colspan=\"2\"><div class=\"message\" style=\"margin-left:7px;\">".$content."</div></td>".
   "</tr></table>\r\n<br />\r\n";


   return array($headline, $module);

}


?>