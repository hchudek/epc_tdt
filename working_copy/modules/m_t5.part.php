<?php

function m_t5__part($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
 
   return array($headline, $module);

}


function m_t5__part_generate_html($_application) {

   global $tc_data;
   if (trim(implode($tc_data["part"]), ",") == "") {
     $module = "&nbsp;";
   }
   else {
      $module =
      "<table id=\"tbl_m_t5__part\" unid=\"".$tc_data["part"]["unid"]."\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">";

      if($tc_data["pot"]["is_proposal"] == "1") {
         $module.=
         "<tr>".
         "<td>Number</td>".
         "<td>".$tc_data["pot"]["part_proposal"]." [PROPOSAL]</td>".
         "</tr>";
      }
      else {
         $proposal_part = (count($tc_data["part_proposal"]) > 0 ) ? "<span>[PROPOSAL&nbsp;".$tc_data["pot"]["part_proposal"]."]</span>" : "";
         $module .=
         "<tr>".
         "<td>Number</td>".
         "<td>".
         "<div style=\"width:100%;\">".
         "<span style=\"display:inline-block; width:60%;\">".
         "<a target=\"_blank\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/0/".$tc_data["part"]["unid"]."\">".rawurldecode($tc_data["part"]["number"])."</a>".$proposal_part.
         "<div style=\"position:absolute;margin-top:10px;display:none;\" id=\"part_history\"></div>".
         "</span>".
         "<span style=\"display:inline-block; width:40%; text-align:right; white-space:nowrap;\">".
         "<img src=\"../../../../library/images/16x16/communication-99.png\" onclick=\"window.open('http://partinquiry.tycoelectronics.com/tpmweb/views/PartInquiryView.faces?partNumber=".rawurldecode($tc_data["part"]["number"])."')\" style=\"cursor:pointer; border:none; margin-right:4px; margin-top:2px;\" />".
         "<img src=\"../../../../library/images/16x16/Content-39.png\" onclick=\"window.open('http://bekworld1.be.tycoelectronics.com/Automotive/EvalReport/default.cfm?Part_Nr=".rawurldecode($tc_data["part"]["number"])."')\" style=\"cursor:pointer; border:none; margin-right:4px; margin-top:2px;\" />".
         "<img src=\"../../../../library/images/16x16/objects-18.png\" onclick=\"load_history('".$tc_data["part"]["unid"]."', 'part');\" style=\"cursor:pointer; border:none; margin-right:4px; margin-top:2px;\" />".
         "</span>".
         "</div>".
         "</td>\r\n".
         "</tr>".
         "<tr>".
         "<td>Proposal</td>".
         "<td>".rawurldecode($tc_data["meta"]["proposal_pot_part"])."</td>".
         "</tr>".
         "<tr>".
         "<td>Name</td>".
         "<td>".rawurldecode($tc_data["part"]["name"])."</td>".
         "</tr>".
         "<tr>".
         "<td>Part Rev.</td>".
         "<td>".rawurldecode($tc_data["part"]["rev"])."</td>".
         "</tr>".
         "<tr>".
         "<td><history field=\"num_drwrevision\" oncontextmenu=\"protocol.embed(this); return false;\">Drw. Rev.</history></td>\r\n".
         "<td>".embed_input(array(), $tc_data["part"]["drawing"], array("check" => "num_drwrevision", "id" => generate_uniqueid(8), "type" => "text", "name" => "num_drwrevision", "style" => "width:65px;", "onblur" => "this.value = this.value.toUpperCase(); post.save_field(session.remote_domino_path_epcmain, '".$tc_data["part"]["unid"]."', this.name, this.value);"))."</td>\r\n".         
         "</tr>".
         "<tr>".
         "<td>PCN</td>".
                  "<td>".embed_input(array(), $tc_data["part"]["pcn"], array("check" => "pcn", "id" => generate_uniqueid(8), "type" => "text", "name" => "pcn", "style" => "width:155px;", "onblur" => "this.value = this.value.toUpperCase(); post.save_field(session.remote_domino_path_epcmain, '".$tc_data["part"]["unid"]."', this.name, this.value);"))."</td>\r\n".
         "</tr>".
         "<tr>";
  //       "<td>Supplier</td>".
  //       "<td>".rawurldecode($tc_data["part"]["supplier"])."</td>".
  //       "</tr>";
      }
      $module .= "</table>\r\n";
   }
   
   return str_replace("Array","",$module);
}

?>
