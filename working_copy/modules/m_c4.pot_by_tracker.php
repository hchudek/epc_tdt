<?php

function m_c4_pot_by_tracker($_application) {

   global $tc_data;

  // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Überschrift und Modulsonderzubehör erstellen
   $tabs="<img src=\"../../../../library/images/icons/plugged.png\" style=\"border:none;position:relative;top:4px;filter:Gray();\" />&nbsp;&nbsp;Included POT";

   
   // POT Daten laden
   if(isset($data)) unset($data);
   $app="epc/tdtracking/pot";
   $path="file:///d:/Lotus/phptemp/".$app;
   $handle=opendir($path);
   while($file=readdir($handle)) if(strpos($file,".php")>0) {include($path."/".$file);}
   closedir ($handle);


   foreach($tc_data["tracker"]["ref_pot"] as $pot) {
      $i = 0; $exit = false; while($exit == false && count($data) > $i) {
         if($data[$i]["unid"] == $pot) {
            $c[$pot][0] = "<a href=\"edit_tracker.php?unique=".$tc_data["tracker"]["unique"]."&pot=".$pot."\">".$pot."</a>";
            $c[$pot][1] = $data[$i]["Project number"];
            $c[$pot][2] = $data[$i]["Case number"];
            $c[$pot][3] = $data[$i]["Part number"];
            $c[$pot][4] = $data[$i]["Part description"];
            $c[$pot][5] = $data[$i]["Tool number"];
            $c[$pot]["is_propsal"] = ($data[$i]["Project number"] == "None") ? 1 : 0;
            $exit = true;
         }
         $i++;
      }
      if($exit == false) {
         $c[$pot][0] = "<a href=\"edit_tracker.php?unique=".$tc_data["tracker"]["unique"]."&pot=".$pot."\">".$pot."</a>";
         $c[$pot][1] = "None";
         $c[$pot][2] = "None";
         $c[$pot][3] = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.get_data?open&unique=".$pot."&field=part");
         $c[$pot][4] = "None";
         $c[$pot][5] = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.get_data?open&unique=".$pot."&field=tool");
         $c[$pot]["is_propsal"] = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.get_data?open&unique=".$pot."&field=is_proposal");
      }

   }

   $l_count= count($data); 
   $t_project = strtolower(base64_decode($_REQUEST["project"]));

   $pot_sel = 
   "<p style=\"display:none;\"><input name=\"selected_tracker\" value=\"".$tc_data["tracker"]["unique"]."\" />".
   "<input name=\"selected_proposal_pot\" value=\"\" /></p>".
   "<div id=\"pot_selector\" style=\"display:none;\">".
   "<div style=\"position:relative;width:910px;height:506px;overflow:auto;\">".
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:893px;border:dotted 1px rgb(99,99,99);filter:alpha(opacity=92);padding:2px;\">".
   "<tr>".
   "<td style=\"background-color:rgb(255,194,54);border-bottom:solid 2px #ffffff;text-align:left;\">Case number</td>".
   "<td style=\"background-color:rgb(255,194,54);border-bottom:solid 2px #ffffff;text-align:left;\">Part number</td>".
   "<td style=\"background-color:rgb(255,194,54);border-bottom:solid 2px #ffffff;text-align:left;\">Part description</td>".
   "<td style=\"background-color:rgb(255,194,54);border-bottom:solid 2px #ffffff;text-align:left;\">Tool number</td>".
   "</tr>";
   for($i = 0; $i < $l_count; $i++) {
      if(strtolower($data[$i]["Project number"]) == $t_project && !in_array($data[$i]["unid"], $tc_data["tracker"]["ref_pot"])) {
         $pot_sel .= "<tr>".
         "<td style=\"background-color:#f1f1f1;text-align:left;\"><a href=\"javascript:replace_proposal_pot('".$data[$i]["unid"]."');\">".$data[$i]["Case number"]."</a>&nbsp;</td>".
         "<td style=\"background-color:#f1f1f1;text-align:left;\">".$data[$i]["Part number"]."&nbsp;</td>".
         "<td style=\"background-color:#f1f1f1;text-align:left;\">".$data[$i]["Part description"]."&nbsp;</td>".
         "<td style=\"background-color:#f1f1f1;text-align:left;\">".$data[$i]["Tool number"]."&nbsp;</td>".
         "</tr>";
      }
   }

   $pot_sel .= "</table></div></div>\r\n";

   $content = $pot_sel."<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tbl_pot_by_tracker\"><tr>".
   "<th>&nbsp;</th>".
   "<th>&nbsp;&nbsp;</th>".
   "<th>POT ID</th>".
   "<th>Project number</th>".
   "<th>Case number</th>".
   "<th>Part number</th>".
   "<th>Part description</th>".
   "<th>Tool number</th>".
   "</tr>";

   $has_pot = false; foreach($tc_data["tracker"]["ref_pot"] as $pot) {
      if(is_array($c[$pot])) {
         $has_pot = true;
         $num++;
         $plug = ($c[$pot]["is_propsal"] == 1) ? "<img onclick=\"plug_pot(this);\" src=\"../../../../library/images/icons/plug.png\" style=\"cursor:pointer;\" /><div id=\"plug_".$pot."\" style=\"border:outset 2px;position:absolute;display:none;z-index:99999;\"></div>" : "&nbsp;";
         $content .= "<tr>".
         "<td style=\"text-align:center;width:20px;\">".$plug."</td>". 
         "<td style=\"text-align:center;width:20px;\">".$num."</td>";
         for($i = 0; $i < count($c[$pot]) - 1; $i++ ) $content .= "<td id=\"id_".$pot."_".$i."\">".$c[$pot][$i]."&nbsp;</td>";
         $content .= "</tr>\r\n";
      }
   }

   if(!$has_pot) $content .= "<tr><td style=\"width:20px;\">&nbsp;</td><td colspan=\"5\">Nothing included</td></tr>\r\n";
   $content .= "</table>\r\n";

   
   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;margin-bottom:28px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_content\" colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   return $module;

}




?>