<?php

if($_REQUEST["css"] != "") {
   header("Content-Type: text/css");
   $id = $_REQUEST["css"];
   print "#un".$id.", #".$id." {\r\n";
   print "	margin: 0;\r\n";
   print "    	padding: 2px 0px 0px 0px;\r\n";
   print "    	float: left;\r\n";
   print "    	margin-right: 1px;\r\n";
   print "	height:280px;\r\n";
   print "	width:140px;\r\n";
   print "	overflow-y:auto;\r\n";
   print "	overflow-x:hidden;\r\n";
   print "}\r\n";

   print "#un".$id." div, #".$id." div {\r\n";
   print "	margin:1px;\r\n";
   print "    	padding:2px;\r\n";
   print "    	font:normal 13px Open Sans;\r\n";
   print "    	width:130px;\r\n";
   print "	height:15px;\r\n";
   print "}\r\n";
}


if($_REQUEST["m_x5__dig_rep_resp"] != "") {
   session_start();
   $id = str_replace("%", "_", rawurlencode(str_replace("@", "_", $_REQUEST["m_x5__dig_rep_resp"])));
   require_once("../../../../library/tools/addin_xml.php");							// XML Library laden
   require_once("../../../../library/tools/view/generate_view.php");

   // GET DATA
   $file = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_cr_rms_dig_rep?open&restricttocategory=".rawurlencode("unset:1")."&count=9999&function=plain");

   $explode = explode(":", $file);
   for($i = 1; $i <= 1 + substr_count($explode[0], ";"); $i++) $in_view[] = $i;

   $data = get_table($file, $in_view);
   $count = 0;


   foreach($data[0] as $val) {
      if(strtolower($val) == strtolower($_REQUEST["m_x5__dig_rep_resp"])) {
         break;
      }
      $count++;
   }
   $arr = array();
   $counter = array();

   foreach($data[1] as $v) {
      $val = (strrpos($v[$count], "=>")) ? substr($v[$count], 0, strrpos($v[$count], "=>")) : $v[$count];
      if(!in_array($val, $arr) && trim($val) != "") $arr[] = $val;
      $counter[$val]++;
   }
   sort($arr);
   print "<span id=\"selected_".$id."\">\r\n";
    foreach($arr as $val) {
      print "<div>".$val." [".$counter[$val]."]</div>\r\n";
   }
   print "</span>\r\n";
}


function m_x5__dig_rep_resp($_application) {
   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_view"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_view"] = "1";
   $img[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_view"] == "1") ? "status-11" : "status-11-1";
   $img[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_view"] == "2") ? "status-11" : "status-11-1";
   $headline .= "&nbsp;|&nbsp;".
   "<img onclick=\"document.getElementsByName('m_x5__dig_rep_resp_view')[0].value = '1'; save_perpage('', true);\" src=\"../../../../library/images/16x16/".$img[0].".png\" style=\"cursor:pointer; position:absolute; width:14px; border-top:solid 2px rgba(0,0,0,0.0);\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Set date&nbsp;&nbsp;&nbsp;&nbsp;".
   "<img onclick=\"document.getElementsByName('m_x5__dig_rep_resp_view')[0].value = '2'; save_perpage('', true);\" src=\"../../../../library/images/16x16/".$img[1].".png\" style=\"cursor:pointer; position:absolute; width:14px; border-top:solid 2px rgba(0,0,0,0.0);\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Show edited";
   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_view"] == "1") {
      $module = utf8_encode(m_x5__dig_rep_resp_get_html($_application));
   }
   else {
       $module = m_x5__dig_rep_resp_get_show_selected($_application);
   }
   return array($headline, $module);
}


function m_x5__dig_rep_resp_get_html($_application) {

   require_once("../../../library/tools/view/generate_view.php");   
   // CONFIG ------------------------------------------------------------------------------------------------------------------------------
   $no_filter = array("Number", "Originator", "Type", "Part", "Rev", "Tool", "Date Issued", "Date Samples", "Created");

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   $function = __FUNCTION__;
   $mid = substr(__FUNCTION__, 0, strpos(__FUNCTION__, "__"));

   // Überschrift und Modulsonderzubehör erstellen
   $img1=($_REQUEST[$_module_id."_f2"]=="") ? "select_reg.png" : "select_high.png";
   $img2=($_REQUEST[$_module_id."_f2"]=="") ? "select_high.png" : "select_reg.png";
   $tabs=
   "<table border=\"0\" cellspacing=\"0\"0 cellpadding=\"0\" style=\"width:100%;font:normal 12px verdana;text-transform:uppercase;\"><tr><td>&nbsp;&nbsp;Browse tracker</td><td style=\"text-align:right;\">".
   "</td></table>";


   // Ansicht einbinden -------------------------------------------------------------------------------------------------------------------
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"] = "1,2,3,4,5,6,7,8,9";
   $in_view = explode(",", $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"]);
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"] = "1";
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] = "2";

   $sort_column = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"];
   $sort_dir = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? SORT_DESC : SORT_ASC;
   $file = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_cr_rms_dig_rep?open&restricttocategory=".rawurlencode("unset:1")."&count=9999&function=plain");
   $data = get_table($file, $in_view);


   foreach($data[0] as $val) {
      $key = str_replace("%", "", rawurlencode(substr($val."@", 0, strpos($val."@", "@"))));
      if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key] = "";
   }
   $count = 0;
   foreach($data[0] as $val) {
      $count++;
      $saved_filter = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];
      if($saved_filter != "") $filter[$count] = explode("</>", strtolower($saved_filter));
   }

   $data[1] = filter_view($data[1], $filter);
   $data[1] = sort_view($data[1], $data[2][$sort_column], $sort_dir);

   // ADD PAGE COL SWITCH ----------------------------------------
   $t_width = 70;
   if(implode($in_view) != "") {
      for($i = 0; $i < count($in_view); $i++) {
        $width = substr($data[3][$in_view[$i] - 1], strpos($data[3][$in_view[$i] - 1], "@") + 1, strlen($data[3][$in_view[$i] - 1]));
        $t_width += intval($width);
        $css .= "#smartsearch a:nth-child(".($i + 1).") {".
                "width:".$width."px;".
                "}";

        $added .= "<div class=\"ui-state-default-sort\" row=\"".$in_view[$i]."\">".substr($data[3][$in_view[$i] - 1], 0, strpos($data[3][$in_view[$i] - 1], "@"))."</div>\r\n";
      }
   }
   else {
     $data[1] = ""; 
   }
   for($i = 0; $i < count($data[3]); $i++) {
      if(!in_array(($i + 1), $in_view)) {
         $not_added .= "<div class=\"ui-state-default-sort\" row=\"".($i + 1)."\">".substr($data[3][$i], 0, strpos($data[3][$i], "@"))."</div>\r\n";
      }
   }
  
   $module = "\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"data:text/css;base64,".base64_encode($css)."\">\r\n";   


   // HEADLINE --------------------------------------------------
   $now = date("Y", mktime());
   for($i = 0; $i < 5; $i++) $filter .= "<option>".($now - $i)."</option>";
   $module .= "<span id=\"smartsearch\">\r\n".
   "<input class=\"readonly\" readonly=\"readonly\" type=\"text\" id=\"smartsearch_search\" name=\"search\" value=\"".$_POST["search"]."\" onkeydown=\"this.setAttribute('clock', Date.now());\" onkeyup=\"window.setTimeout('smartsearch.check_do_filter(\'smartsearch.body\')', 800);\" />\r\n".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/interface-32.png\" onclick=\"smartsearch.configure('".__FUNCTION__."', 'smartsearch.".__FUNCTION__.".fieldlist');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px;margin-left:4px;margin-right:7px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/text-6.png\" onclick=\"smartsearch.kill_filter('".__FUNCTION__."');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/table.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', false);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/excel-icon.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', true);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<span>|&nbsp;<span style=\"color:rgb(0,102,158); cursor:pointer;\" onclick=\"m_x5.download();\">Digital report</span><select name=\"dig_report\" style=\"border:none;font:normal 13px Open Sans; color:rgb(0,102,158);\">".$filter."</select>&nbsp;|&nbsp;</span>\r\n".
   "<span style=\"font:normal 13px Open Sans;\">Results:&nbsp;<a id=\"smartsearch_results\" count=\"".count($data[1])."\">".count($data[1])."&nbsp;/&nbsp;".count($data[1])."</a></span>\r\n";


   $count = 0;
   $module .= "<div style=\"white-space:no-wrap;width:".$t_width."px;overflow:hidden;\"><c id=\"smartsearch.head\" clear=\"true\" style=\"display:none;\">\r\n";

   foreach($data[0] as $val) {
      if($val != "") {
         $id[$val] = generate_uniqueid(8);
         $img = "chevron-reg";
         if($in_view[$count] == $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"]) {
            $img = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? "chevron-desc" : "chevron-asc";
         }
         $tooltip = $_SESSION["perpage"]["tab"]["tracker_view"][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];
         $usecolor = (is_array($filter[$count + 1])) ? "rgb(205,32,44)" : "rgb(0,0,0)";
         $usedir = (is_array($filter[$count + 1])) ? "_red" : "";
         $img_filter = (in_array(substr($val, 0, strpos($val, "@")), $no_filter)) ? "" : "img:filter=\"hardware-46\" ";
         $module.= "<a smartsearch.tooltip=\"".str_replace(" ", "&nbsp;", $tooltip)."\" style=\"color:".$usecolor.";\" name=\"smartsearch.sort.usemap\" img:usedir=\"".$usedir."\" img:sortdir=\"".$img."\" smartsearch.usemap:id=\"".$id[$val]."\" module:id=\"".str_replace("_get_html", "", __FUNCTION__)."\" area:link=\"use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 2);use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 1)\" area:coords=\"2,2,9,7;11,2,18,7\" ".$img_filter."img:link=\"smartsearch.configure('".__FUNCTION__."', '".$id[$val]."'); smartsearch.embed_filter(this, '".$id[$val]."', '".$val."');\">".substr($val, 0, strpos($val, "@"))."</a>\r\n";
         $count++;
      }
   }
   $module .= "</c></div>\r\n";
   // BODY -----------------------------------------------------
   $module .= "<div id=\"smartsearch.body\" clear=\"true\" php:function=\"".__FUNCTION__."\" style=\"display:none;\">\r\n";
   $e = 0;
   
   foreach($data[1][0] as $key => $val) {
      if(strpos($val, "=>")) {
         $pointer = $key;
         break;
      }
   }

   foreach($data[1] as $td) {

      $module .= "<p do=\"".trim(substr($td[$pointer], strrpos($td[$pointer], "=>") + 2, strlen($td[$pointer])))."\">";
      $e++;
      foreach($td as $val) {
         $val = explode("=>", $val);
         $module .= "<a>".str_replace("error", "", $val[0])."</a>";
      }
      $module .= "</p>\r\n";
   }

   $module .= "</div>\r\n".
   "</span>\r\n";

   // FIELDLIST -----------------------------------------------
   $module .= 
   "<div id=\"smartsearch.".__FUNCTION__.".fieldlist\" style=\"margin-top:7px;display:none;\"\">\r\n".
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<td style=\"position:relative;top:27px;left:-6px;\"><span class=\"phpbutton\" style=\"position:relative;top:-4px;left:4px;\"><a href=\"javascript:void(0);\" onclick=\"do_save_filter('".__FUNCTION__."');\">Apply selection</a></span></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Included</b></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Not included</b></td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_selected\" class=\"connected_sortable\">".$added."</span></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_unselected\" class=\"connected_sortable\">".$not_added."</span></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n".
   "</div>\r\n";


   // FILTER ---------------------------------------------------
   foreach($data[0] as $val) {
      if($val != "") {
         $module .= "<div id=\"".$id[$val]."\"></div>\r\n";
      }
   }

   $module .= "<p style=\"display:none;\" id=\"smartsearch.configure\" nest_init_view=\"".implode($id, ":")."\">smartsearch.".__FUNCTION__.".fieldlist:".implode($id, ":")."</p>";

   // EMBED MODULE ---------------------------------------------      
   $module = 
   "<table id=\"tbl_".__FUNCTION__."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n".
   "      <tr>\r\n".
   "         <td>".$module."</td>\r\n".
   "      </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));  
   return $module;


}



function m_x5__dig_rep_resp_get_show_selected($_application) {

   require_once("../../../library/tools/view/generate_view.php");   
   // CONFIG ------------------------------------------------------------------------------------------------------------------------------
   $no_filter = array("Number", "Originator", "Type", "Part", "Rev", "Tool", "Date Issued", "Date Samples", "Created", "Date dig rep");

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   $function = __FUNCTION__;
   $mid = substr(__FUNCTION__, 0, strpos(__FUNCTION__, "__"));

   // Überschrift und Modulsonderzubehör erstellen
   $img1=($_REQUEST[$_module_id."_f2"]=="") ? "select_reg.png" : "select_high.png";
   $img2=($_REQUEST[$_module_id."_f2"]=="") ? "select_high.png" : "select_reg.png";
   $tabs=
   "<table border=\"0\" cellspacing=\"0\"0 cellpadding=\"0\" style=\"width:100%;font:normal 12px verdana;text-transform:uppercase;\"><tr><td>&nbsp;&nbsp;Browse tracker</td><td style=\"text-align:right;\">".
   "</td></table>";


   // Ansicht einbinden -------------------------------------------------------------------------------------------------------------------
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"] = "1,2,3,4,5,6,7,8,9";
   $in_view = explode(",", $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"]);
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"] = "1";
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] = "2";

   $sort_column = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"];
   $sort_dir = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? SORT_DESC : SORT_ASC;
   $file = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_cr_rms_dig_rep_selected?open&count=99999&function=plain");
   $data = get_table($file, $in_view);


   foreach($data[0] as $val) {
      $key = str_replace("%", "", rawurlencode(substr($val."@", 0, strpos($val."@", "@"))));
      if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key] = "";
   }
   $count = 0;
   foreach($data[0] as $val) {
      $count++;
      $saved_filter = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];
      if($saved_filter != "") $filter[$count] = explode("</>", strtolower($saved_filter));
   }

   $data[1] = filter_view($data[1], $filter);
   $data[1] = sort_view($data[1], $data[2][$sort_column], $sort_dir);

   // ADD PAGE COL SWITCH ----------------------------------------
   $t_width = 70;
   if(implode($in_view) != "") {
      for($i = 0; $i < count($in_view); $i++) {
        $width = substr($data[3][$in_view[$i] - 1], strpos($data[3][$in_view[$i] - 1], "@") + 1, strlen($data[3][$in_view[$i] - 1]));
        $t_width += intval($width);
        $css .= "#smartsearch a:nth-child(".($i + 1).") {".
                "width:".$width."px;".
                "}";

        $added .= "<div class=\"ui-state-default-sort\" row=\"".$in_view[$i]."\">".substr($data[3][$in_view[$i] - 1], 0, strpos($data[3][$in_view[$i] - 1], "@"))."</div>\r\n";
      }
   }
   else {
     $data[1] = ""; 
   }
   for($i = 0; $i < count($data[3]); $i++) {
      if(!in_array(($i + 1), $in_view)) {
         $not_added .= "<div class=\"ui-state-default-sort\" row=\"".($i + 1)."\">".substr($data[3][$i], 0, strpos($data[3][$i], "@"))."</div>\r\n";
      }
   }
  
   $module = "\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"data:text/css;base64,".base64_encode($css)."\">\r\n";   


   // HEADLINE --------------------------------------------------
   $now = date("Y", mktime());
   for($i = 0; $i < 5; $i++) $filter .= "<option>".($now - $i)."</option>";
   $module .= "<span id=\"smartsearch\">\r\n".
   "<input class=\"readonly\" readonly=\"readonly\" type=\"text\" id=\"smartsearch_search\" name=\"search\" value=\"".$_POST["search"]."\" onkeydown=\"this.setAttribute('clock', Date.now());\" onkeyup=\"window.setTimeout('smartsearch.check_do_filter(\'smartsearch.body\')', 800);\" />\r\n".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/interface-32.png\" onclick=\"smartsearch.configure('".__FUNCTION__."', 'smartsearch.".__FUNCTION__.".fieldlist');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px;margin-left:4px;margin-right:7px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/text-6.png\" onclick=\"smartsearch.kill_filter('".__FUNCTION__."');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/table.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', false);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/excel-icon.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', true);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<span style=\"font:normal 13px Open Sans;\">Results:&nbsp;<a id=\"smartsearch_results\" count=\"".count($data[1])."\">".count($data[1])."&nbsp;/&nbsp;".count($data[1])."</a></span>\r\n";


   $count = 0;
   $module .= "<div style=\"white-space:no-wrap;width:".$t_width."px;overflow:hidden;\"><c id=\"smartsearch.head\" clear=\"true\" style=\"display:none;\">\r\n";

   foreach($data[0] as $val) {
      if($val != "") {
         $id[$val] = generate_uniqueid(8);
         $img = "chevron-reg";
         if($in_view[$count] == $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"]) {
            $img = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? "chevron-desc" : "chevron-asc";
         }
         $tooltip = $_SESSION["perpage"]["tab"]["tracker_view"][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];
         $usecolor = (is_array($filter[$count + 1])) ? "rgb(205,32,44)" : "rgb(0,0,0)";
         $usedir = (is_array($filter[$count + 1])) ? "_red" : "";
         $img_filter = (in_array(substr($val, 0, strpos($val, "@")), $no_filter)) ? "" : "img:filter=\"hardware-46\" ";
         $module.= "<a smartsearch.tooltip=\"".str_replace(" ", "&nbsp;", $tooltip)."\" style=\"color:".$usecolor.";\" name=\"smartsearch.sort.usemap\" img:usedir=\"".$usedir."\" img:sortdir=\"".$img."\" smartsearch.usemap:id=\"".$id[$val]."\" module:id=\"".str_replace("_get_html", "", __FUNCTION__)."\" area:link=\"use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 2);use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 1)\" area:coords=\"2,2,9,7;11,2,18,7\" ".$img_filter."img:link=\"smartsearch.configure('".__FUNCTION__."', '".$id[$val]."'); smartsearch.embed_filter(this, '".$id[$val]."', '".$val."');\">".substr($val, 0, strpos($val, "@"))."</a>\r\n";
         $count++;
      }
   }
   $module .= "</c></div>\r\n";
   // BODY -----------------------------------------------------
   $module .= "<div id=\"smartsearch.body\" clear=\"true\" php:function=\"".__FUNCTION__."\" style=\"display:none;\">\r\n";
   $e = 0;
   
   foreach($data[1][0] as $key => $val) {
      if(strpos($val, "=>")) {
         $pointer = $key;
         break;
      }
   }

   foreach($data[1] as $td) {

      $module .= "<p do=\"".trim(substr($td[$pointer], strrpos($td[$pointer], "=>") + 2, strlen($td[$pointer])))."\">";
      $e++;
      foreach($td as $val) {
         $val = explode("=>", $val);
         $out = str_replace("error", "", $val[0]);
         $anchor = "";
         if(substr($out, 0, 4) == ">>>>") {
            $anchor = " anchor=\"".substr($out, 4, strlen($out))."\"";
            $out = substr($out, 4, 10);
         }
         $module .= "<a".$anchor.">".$out."</a>";
      }
      $module .= "</p>\r\n";
   }

   $module .= "</div>\r\n".
   "</span>\r\n";

   // FIELDLIST -----------------------------------------------
   $module .= 
   "<div id=\"smartsearch.".__FUNCTION__.".fieldlist\" style=\"margin-top:7px;display:none;\"\">\r\n".
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<td style=\"position:relative;top:27px;left:-6px;\"><span class=\"phpbutton\" style=\"position:relative;top:-4px;left:4px;\"><a href=\"javascript:void(0);\" onclick=\"do_save_filter('".__FUNCTION__."');\">Apply selection</a></span></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Included</b></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Not included</b></td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_selected\" class=\"connected_sortable\">".$added."</span></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_unselected\" class=\"connected_sortable\">".$not_added."</span></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n".
   "</div>\r\n";


   // FILTER ---------------------------------------------------
   foreach($data[0] as $val) {
      if($val != "") {
         $module .= "<div id=\"".$id[$val]."\"></div>\r\n";
      }
   }

   $module .= "<p style=\"display:none;\" id=\"smartsearch.configure\" nest_init_view=\"".implode($id, ":")."\">smartsearch.".__FUNCTION__.".fieldlist:".implode($id, ":")."</p>";

   // EMBED MODULE ---------------------------------------------      
   $module = 
   "<table id=\"tbl_".__FUNCTION__."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n".
   "      <tr>\r\n".
   "         <td>".$module."</td>\r\n".
   "      </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));  
   return $module;


}


?>