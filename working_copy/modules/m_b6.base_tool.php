<?php


function m_b6__base_tool($_application) {

   global $bt_data;
   $xml = process_xml($_SESSION["remote_domino_path_digital_report"]."/v.get_fielddefinition?open&count=9999&restricttocategory=f.base_tool&function=xml:data");



   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);


   // Überschrift und Modulsonderzubehör erstellen
   if($bt_data["wf_status"] == "") $bt_data["wf_status"] = 0;


   $dsp_status["0"] = "Procurement input"; 
   $dsp_status["5"] = "Procurement update / Reason: ".$bt_data["change_reason_procurement"]; 
   $dsp_status["10"] = "Specification input"; 
   $dsp_status["80"] = "Specification update / Reason: ".$bt_data["change_reason_specification"]; 
   $dsp_status["90"] = "Finalized"; 

   $tabs="<img src=\"../../../../library/images/16x16/Business-30.png\" style=\"border:none;position:relative;top:2px;\" />&nbsp;&nbsp;Base tool [".$dsp_status[$bt_data["wf_status"]]."]</span>";

   $cursor = ($bt_data["wf_status"] != "10" && in_array("[base_tool_r1]", $_SESSION[remote_userroles])) ? "pointer" : "text";
   $img[0] = ($bt_data["scan_location"] == "1") ? "status-5" : "status-5-1";
   $img[1] = ($bt_data["scan_location"] == "2") ? "status-5" : "status-5-1";
   $dsp_scan_location = 
   "<img src=\"../../../../library/images/16x16/".$img[0].".png\" style=\"cursor:".$cursor.";width:16px;vertical-align:top;margin-right:6px;border:none;\" name=\"scan_location\" onclick=\"save_selected(this, '".$bt_data["unid"]."', '1', 'procurement');\" />TE".
   "<img src=\"../../../../library/images/16x16/".$img[1].".png\" style=\"cursor:".$cursor.";width:16px;vertical-align:top;margin-right:6px;margin-left:10px;\" name=\"scan_location\"  onclick=\"save_selected(this, '".$bt_data["unid"]."', '2', 'procurement');\" />Supplier";

   $img[0] = ($bt_data["digital_report"] == "1") ? "status-5" : "status-5-1";
   $img[1] = ($bt_data["digital_report"] == "2") ? "status-5" : "status-5-1";
   $dsp_digital_report = 
   "<img src=\"../../../../library/images/16x16/".$img[0].".png\" style=\"cursor:".$cursor.";width:16px;vertical-align:top;margin-right:6px;border:none;\" name=\"digital_report\" onclick=\"save_selected(this, '".$bt_data["unid"]."', '1', 'procurement');\" />TE".
   "<img src=\"../../../../library/images/16x16/".$img[1].".png\" style=\"cursor:".$cursor.";width:16px;vertical-align:top;margin-right:6px;margin-left:10px;\" name=\"digital_report\"  onclick=\"save_selected(this, '".$bt_data["unid"]."', '2', 'procurement');\" />Supplier";


   $img[0] = ($bt_data["tabulated_report"] == "1") ? "status-5" : "status-5-1";
   $img[1] = ($bt_data["tabulated_report"] == "2") ? "status-5" : "status-5-1";
   $dsp_tabulated_report = 
   "<img src=\"../../../../library/images/16x16/".$img[0].".png\" style=\"cursor:".$cursor.";width:16px;vertical-align:top;margin-right:6px;border:none;\" name=\"tabulated_report\" onclick=\"save_selected(this, '".$bt_data["unid"]."', '1', 'procurement');\" />TE".
   "<img src=\"../../../../library/images/16x16/".$img[1].".png\" style=\"cursor:".$cursor.";width:16px;vertical-align:top;margin-right:6px;margin-left:10px;\" name=\"tabulated_report\"  onclick=\"save_selected(this, '".$bt_data["unid"]."', '2', 'procurement');\" />Supplier";


   $visibility = ($bt_data["supplier"] != "" && ($bt_data["scan_location"] == "1" || $bt_data["digital_report"] == "1" || $bt_data["tabulated_report"] == "1")) ? "visible" : "hidden";
   $edit_procurement_data = (!in_array("[base_tool_r1]", $_SESSION[remote_userroles])) ? "0" : $bt_data["edit_procurement_data"];
   if(in_array("[base_tool_r1]", $_SESSION[remote_userroles])) {

      $dsp_change_reason = "<select style=\"display:none;position:absolute;left:3px;\" name=\"change_reason_procurement\" onchange=\"save_change_status(this, '".$bt_data["unid"]."', '5');\">\r\n<option>Select reason</option>\r\n";
      foreach($xml["change_reason_procurement"]["value"] as $val) {
         $val = rawurldecode($val)."|".rawurldecode($val);
         $dsp_change_reason .= "<option value=\"".substr($val, strpos($val, "|") + 1, strlen($val))."\"$selected>".substr($val, 0, strpos($val, "|"))."</option>\r\n";
      }
      $dsp_change_reason .= "</select>\r\n";
     

      $button_send = ($bt_data["wf_status"] != "10" && $bt_data["wf_status"] != "80") ? "<span id=\"button_send\" class=\"phpbutton\" style=\"visibility:".$visibility."\"><a href=\"javascript:void(0);\" onclick=\"change_status('".$bt_data["unid"]."', 10);\">Request specification</a></span>" : "";
      $button_change = ($bt_data["wf_status"] != "10" && $bt_data["wf_status"] != "80") ? "" : $dsp_change_reason."<span id=\"button_send\" class=\"phpbutton\" style=\"visibility:".$visibility."\"><a href=\"javascript:void(0);\" onclick=\"dsp_select('change_reason_procurement');\">Change data</a></span>";
   }

   $content = 
   "<p style=\"display:none;\">".
   "<a name=\"handle_button_send\">scan_location,digital_report,tabulated_report</a>\r\n".
   "<a name=\"edit_procurement_data\">".$edit_procurement_data."</a>\r\n".
   "<a name=\"wf_status\">".$bt_data["wf_status"]."</a>\r\n".
   "</p>".
   "<div style=\"padding:2px;background-color:rgb(255,194,54);width:100%;height:18px;font:bold 13px century gothic,verdana;\">Procurement data</div>".
   "<div style=\"margin-top:2px;background-color:rgb(220,220,220);width:100%;\">".
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"procurement_data\">".
   "<tr>".
   "<td style=\"width:1%;\">Supplier</td>".
   "<td style=\"width:99%;\">".
   "<img src=\"../../../../library/images/16x16/business-5.png\" style=\"border:none;vertical-align:top;\" onclick=\"select_supplier('".$bt_data["unid"]."', 'procurement');\" style=\"cursor:".$cursor."\" />".
   "&nbsp;&nbsp<span id=\"dsp_supplier\">".rawurldecode($bt_data["supplier"])."</span>".
   "<iframe scrolling=\"no\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\" framespacing=\"0\" name=\"ifrm\" src=\"\" style=\"visibility:hidden;z-index:99999;position:absolute;width:674px;height:380px;filter:alpha(opacity=92);border:dotted 1px rgb(99,99,99);\"></iframe>".
   "</td>".
   "</tr>".
   "<tr>".
   "<td>Scan location</td>".
   "<td>".$dsp_scan_location."</td>".
   "</tr>".
   "<tr>".
   "<td>Digital report</td>".
   "<td>".$dsp_digital_report."</td>".
   "</tr>".
   "<tr>".
   "<td>Tabulated report&nbsp;&nbsp;&nbsp;</td>".
   "<td>".$dsp_tabulated_report."</td>".
   "</tr>".
   "<tr>".
   "<td colspan=\"2\" style=\"vertical-align:center;height:29px;\">".$button_send.$button_change."&nbsp;</td>".
   "</tr>".
   "</table>".
   "</div>";



   if(in_array($bt_data["wf_status"], array("10", "80", "90"))) {
      $disabled = ($bt_data["wf_status"] == "90" || !in_array("[base_tool_r2]", $_SESSION[remote_userroles])) ? " disabled" : "";
      if($bt_data["scan_location"] == "1") {
         $dsp_scan_location = "<select name=\"selected_scan_location\" onchange=\"check_finished();save_select(this, '".$bt_data["unid"]."');\">\r\n<option$disabled>Select scan location</option>\r\n";
         foreach($xml["selected_scan_location"]["value"] as $val) {
            $val = rawurldecode($val)."|".rawurldecode($val);
            $selected = ($bt_data["selected_scan_location"] == substr($val, strpos($val, "|") + 1, strlen($val))) ? "selected" : "";
            $dsp_scan_location .= "<option value=\"".substr($val, strpos($val, "|") + 1, strlen($val))."\"$selected$disabled>".substr($val, 0, strpos($val, "|"))."</option>\r\n";
         }
         $dsp_scan_location .= "</select>\r\n";
      }
      else {
         $dsp_scan_location = "&nbsp;";
      }

      if($bt_data["digital_report"] == "1") {
         $dsp_digital_report = "<select name=\"selected_digital_report\" onchange=\"check_finished();save_select(this, '".$bt_data["unid"]."');\">\r\n<option$disabled>Select digital report</option>\r\n";
         foreach($xml["selected_digital_report"]["value"] as $val) {
            $val = rawurldecode($val)."|".rawurldecode($val);
            $selected = ($bt_data["selected_digital_report"] == substr($val, strpos($val, "|") + 1, strlen($val))) ? "selected" : "";
            $dsp_digital_report .= "<option value=\"".substr($val, strpos($val, "|") + 1, strlen($val))."\"$selected$disabled>".substr($val, 0, strpos($val, "|"))."</option>\r\n";
         }
         $dsp_digital_report .= "</select>\r\n";
      }
      else {
         $dsp_digital_report = "&nbsp;";
      }

      if($bt_data["tabulated_report"] == "1") {
         $dsp_tabulated_report = "<select name=\"selected_tabulated_report\" onchange=\"check_finished();save_select(this, '".$bt_data["unid"]."');\">\r\n<option$disabled>Select tabulated report</option>\r\n";
         foreach($xml["selected_tabulated_report"]["value"] as $val) {
            $val = rawurldecode($val)."|".rawurldecode($val);
            $selected = ($bt_data["selected_tabulated_report"] == substr($val, strpos($val, "|") + 1, strlen($val))) ? "selected" : "";
            $dsp_tabulated_report .= "<option value=\"".substr($val, strpos($val, "|") + 1, strlen($val))."\"$selected$disabled>".substr($val, 0, strpos($val, "|"))."</option>\r\n";
         }
         $dsp_tabulated_report .= "</select>\r\n";
      }
      else {
         $dsp_tabulated_report = "&nbsp;";
      }

      $display = ($bt_data["wf_status"] == "90") ? "block" : "none";
      if(in_array("[base_tool_r2]", $_SESSION[remote_userroles])) {

         $dsp_change_specification = "<select style=\"display:none;position:absolute;left:3px;\" name=\"change_reason_specification\" onchange=\"save_change_status(this, '".$bt_data["unid"]."', '80');\">\r\n<option>Select reason</option>\r\n";
         foreach($xml["change_reason_specification"]["value"] as $val) {
            $val = rawurldecode($val)."|".rawurldecode($val);
            $dsp_change_specification .= "<option value=\"".substr($val, strpos($val, "|") + 1, strlen($val))."\"$selected>".substr($val, 0, strpos($val, "|"))."</option>\r\n";
         }
         $dsp_change_specification .= "</select>\r\n";


         $button_grab = $dsp_change_specification."<span id=\"button_grab\" class=\"phpbutton\" style=\"display:".$display.";\"><a href=\"javascript:void(0);\" onclick=\"dsp_select('change_reason_specification');\">Grab</a></span>\r\n";
         $button_finish = "<span id=\"button_finished\" class=\"phpbutton\" style=\"visibility:hidden;\"><a href=\"javascript:void(0);\" onclick=\"change_status('".$bt_data["unid"]."', 90);\">Finalize</a></span>\r\n";
      }

      $content .= 
      "<div style=\"padding:2px;background-color:rgb(255,194,54);width:100%;height:18px;font:bold 13px century gothic,verdana;\">Specific data</div>".
      "<div style=\"margin-top:2px;background-color:rgb(220,220,220);width:100%;\">".
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"india_data\">".
      "<tr>".
      "<td style=\"width:1%;\">Scan location</td>".
      "<td style=\"width:99%;\">".$dsp_scan_location."</td>".
      "</tr>".
      "<tr>".
      "<td>Digital report</td>".
      "<td>".$dsp_digital_report."</td>".
      "</tr>".
      "<tr>".
      "<td>Tabulated report&nbsp;&nbsp;&nbsp;</td>".
      "<td>".$dsp_tabulated_report."</td>".
      "</tr>".
      "<tr>".
      "<td colspan=\"2\" style=\"vertical-align:center;height:29px;\">".$button_grab.$button_finish."</td>".
      "</tr>".
      "</table>".
      "</div>";


   }


   // START: HISTORY
   // -----------------------------------------------------------------------------------------------------------------
   if(in_array("[history]", $_SESSION[remote_userroles])) {


      $get_php = file_get_authentificated_contents($_SESSION["remote_domino_path_digital_report"]."/v.translate_value?open&count=9999&restricttocategory=f.base_tool&function=plain");
      if(!strpos($get_php, "No documents")) eval($get_php);

      $e = 0;
      $exit = ($_REQUEST["history"] != "") ? $_REQUEST["history"] : 15;

      $content .= 
      "<div style=\"padding:2px;background-color:rgb(255,194,54);width:100%;height:18px;font:bold 13px century gothic,verdana;\">History [show last ".$exit." changes]</div>".
      "<div style=\"margin-top:2px;background-color:rgb(220,220,220);width:100%;\">".
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"history\">".
      "<tr>".
      "<th style=\"width:20%;\">Date</th>".
      "<th style=\"width:20%;\">Changed by</th>".
      "<th style=\"width:20%;\">Field</th>".
      "<th style=\"width:20%;\">From</th>".
      "<th style=\"width:20%;\">To</th>".
      "</tr>";

      $bt_data["history"] = explode("://:", $bt_data["history"]);
   
      foreach($bt_data["history"] as $val) {
         $e++;
         if($exit < $e) break;
         if($val != "") {
            $dsp = explode("|", $val);
            $dsp[3] = ($translate[$dsp[2]][$dsp[3]] != "") ? rawurldecode($translate[$dsp[2]][$dsp[3]]) : $dsp[3]; 
            $dsp[4] = ($translate[$dsp[2]][$dsp[4]] != "") ? rawurldecode($translate[$dsp[2]][$dsp[4]]) : $dsp[4]; 
            $content .=
            "<tr>".
            "<td>".substr($dsp[0], 0, 4)."-".substr($dsp[0], 4, 2)."-".substr($dsp[0], 6, 2)." ".substr($dsp[0], 8, 2).":".substr($dsp[0], 10, 2).":".substr($dsp[0], 12, 2)."</td>".
            "<td>".$dsp[1]."<img src=\"../../../../library/images/blank.gif\" style=\"width:1px;\" /></td>".
            "<td>".$dsp[2]."<img src=\"../../../../library/images/blank.gif\" style=\"width:1px;\" /></td>".
            "<td>".$dsp[3]."<img src=\"../../../../library/images/blank.gif\" style=\"width:1px;\" /></td>".
            "<td>".$dsp[4]."<img src=\"../../../../library/images/blank.gif\" style=\"width:1px;\" /></td>".
            "</tr>";
         }
      }

      $content .= 
      "</table>".
      "</div>";
   }
   // END: HISTORY
   // -----------------------------------------------------------------------------------------------------------------

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;table-layout:fixed;margin-bottom:22px;position:relative;top:-1px;z-index:9998;\">\r\n".

   "      <tr>\r\n".
   "         <td class=\"module_2_content\" style=\"background-color:rgb(255,255,255);\">".$content."</td>\r\n".
   "      </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   return array($headline, $module);
}

?>



   