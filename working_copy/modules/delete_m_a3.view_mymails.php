<?php

function m_a3__view_mymails ($_application){

// Modulnamen festlegen
$_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
$_module_id=substr(basename(__FILE__),0,4);
$_SESSION["module"][$_module_name]=$_module_id;

$headline="My E-Mail";

// Überschrift und Modulsonderzubehör erstellen
$category=($_REQUEST[$_SESSION["module"][$_module_name]."_category"]=="") ? "2" : $_REQUEST[$_SESSION["module"][$_module_name]."_category"];
$s_img[0]=($category=="0") ? "select_high" : "select_reg";
$s_img[1]=($category=="1") ? "select_high" : "select_reg";
$s_img[2]=($category=="2") ? "select_high" : "select_reg";


$use_view = base64_decode($_REQUEST[$_module_id."_type"]); if($use_view == "") $use_view = "v.get_mail_sent";


$img1=($use_view=="v.get_mail_received") ? "select_reg.png" : "select_high.png";
$img2=($use_view=="v.get_mail_received") ? "select_high.png" : "select_reg.png";
$tabs=
"<table border=\"0\" cellspacing=\"0\"0 cellpadding=\"0\" style=\"width:100%;font:normal 12px verdana;text-transform:uppercase;\"><tr><td>&nbsp;&nbsp;".get_text("headline_my_notifications")."</td><td style=\"text-align:right;\">".
"<a href=\"?".str_replace("&".$_module_id."_type=".$_REQUEST[$_module_id."_type"],"",$_SERVER["QUERY_STRING"])."&".$_module_id."_type=".base64_encode("v.get_mail_sent")."\"><img src=\"../../../../library/images/icons/".$img1."\" style=\"position:relative;top:1px;border:none;width:12px;width:11px;filter:chroma(color=#ffffff);margin-right:5px;\" /></a>Sent&nbsp;&nbsp;&nbsp;".
"<a href=\"?".str_replace("&".$_module_id."_type=".$_REQUEST[$_module_id."_type"],"",$_SERVER["QUERY_STRING"])."&".$_module_id."_type=".base64_encode("v.get_mail_received")."\"><img src=\"../../../../library/images/icons/".$img2."\" style=\"border:none;width:11px;filter:chroma(color=#ffffff);position:relative;top:1px;margin-right:5px;\" /></a>Received&nbsp;".
"</td></table>";

$q = str_replace("&".$_module_id."_category=".$_REQUEST[$_module_id."_category"], "", $_SERVER["QUERY_STRING"]);
$addedline=
"<div style=\"font:normal 12px verdana; color:rgb(66,66,66);margin-top:0px;margin-bottom:19px;\"><span style=\"position:relative;top:-1px;z-index:-1;\">Use filter:&nbsp;</span>".
"<a href=\"?".$q."&".$_SESSION["module"][$_module_name]."_category=0\"><img src=\"../../../../library/images/icons/".$s_img[0].".png\" style=\"border:none;margin-right:3px;width:12px;\" /></a><span style=\"position:relative;top:-1px;z-index:-1;\">Not read&nbsp;&nbsp;&nbsp;</span>".
"<a href=\"?".$q."&".$_SESSION["module"][$_module_name]."_category=1\"><img src=\"../../../../library/images/icons/".$s_img[1].".png\" style=\"border:none;margin-right:3px;width:12px;\" /></a><span style=\"position:relative;top:-1px;z-index:-1;\">Read&nbsp;&nbsp;&nbsp;</span>".
"<a href=\"?".$q."&".$_SESSION["module"][$_module_name]."_category=2\"><img src=\"../../../../library/images/icons/".$s_img[2].".png\" style=\"border:none;margin-right:3px;width:12px;\" /></a><span style=\"position:relative;top:-1px;z-index:-1;\">All mails&nbsp;&nbsp;&nbsp;</span>".
"</div>";

// Ansicht einbinden
if(isset($data)) unset($data); if(isset($column)) unset($column);
unset($get_data);
$s[0] = substr($_SESSION["remote_email"], 0 , strpos($_SESSION["remote_email"], "@"));
$s[1] = strtolower(substr($_SESSION["domino_user"], 3, strpos($_SESSION["domino_user"], "/") - 3));

foreach($s as $t_cat) {
   $tmp = file_get_authentificated_contents($_SESSION["remote_domino_path_mail"]."/".$use_view."?open=php&count=99999&restricttocategory=".urlencode($t_cat)."/".$category);
   if(!strpos($tmp, "No documents found")) {
      $col = substr($tmp, 0, strpos($tmp, "data[]") - 1);
      $get_data .= substr($tmp, strpos($tmp, "data[]") - 1, strlen($tmp));
   }
}

$get_data = $col.$get_data; 
$eval=str_replace("_c[]","column[]",$get_data);

if(substr(strtolower($eval),4,18)!="no documents found") eval($eval);
include_once("addin/generate_view.php"); include_once("generate_search.php");

// View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
$t_view = generate_view($column, $data, $_w, $_SESSION["module"][$_module_name], true, true, true, true, true, true, 40, array("use:ajax",$_SESSION["remote_domino_path_mail"]."/v.get_php/%%UNID%%?open"));

$module.=
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;\">\r\n".
"   <tr>\r\n".
"      <td class=\"module_4_spacer\">&nbsp;</td>\r\n".
"      <td class=\"module_4_headline\">%%TABS%%</td>\r\n".
"   </tr>\r\n".
"   <tr>\r\n".
"      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
"   </tr>\r\n".
"   <tr><td colspan=\"2\">".$t_view."</td>\r\n".
"   </tr>\r\n".
"</table>\r\n";


$module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));


Return array($headline, $module);

}

?>