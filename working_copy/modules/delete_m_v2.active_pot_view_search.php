<?php


function m_v2_active_pot_view_search($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $sel_tab=($_REQUEST[$_SESSION["module"][$_module_name]."_tab"]=="") ? "1" : $_REQUEST[$_SESSION["module"][$_module_name]."_tab"]; 
   $_t_tab_names[]="Select fields";
   $_t_tab_names[]="Search";
   $_t_tab_names[]="Lookup";

   switch($sel_tab) {

      case "1":

         unset($field);

         $field[] = "Project-No";
         $field[] = "Part No";
         $field[] = "Part Rev.";
         $field[] = "Part Desc.";
         $field[] = "New Tool No";
         $field[] = "Supplier (Tool)";
         $field[] = "DIS No";
         $field[] = "EVT Date revised";
         $field[] = "EVT Date actual";
         $field[] = "DIM Report revised";
         $field[] = "DIM Report actual";
         $field[] = "Status IR";	
         $field[] = "Purchase Order";
         $field[] = "DM TEC actual";
         $field[] = "53P Date revised";
         $field[] = "Active";	
         $field[] = "Engineer";
         $field[] = "Customer";
         $field[] = "Production location G3";


         $selected_fields = explode(",", $_REQUEST[$_module_id."_fields"]);
         $t_view = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\" style=\"width:800px;table-layout:fixed;\"><tr>";
         $count = count($field); for($i = 0; $i < $count; $i++) {
            $img = (in_array(($i + 1), $selected_fields)) ? "select_high" : "select_reg";
            if(intval($i / 5) == ($i / 5)) $t_view .= "</tr><tr>";
            $t_view .= "<td style=\"overflow:hidden;padding:0px 15px 7px 0px;\"><nobr><img name=\"selected_field\" onclick=\"select_field(this);\" src=\"../../../../library/images/icons/".$img.".png\" style=\"width:12px;vertical-align:top;margin-right:4px;position:relative;top:1px;cursor:pointer;\">".$field[$i]."</nobr></td>";
         }
         $t_view .= "</tr>".
         "<td colspan=\"5\" style=\"padding:12px 0px 10px 0px;\">".
         "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"select_all('selected_field', true);\">Select all</a></span>".
         "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"select_all('selected_field', false);\">Deselect all</a></span>".
         "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"submit_selected_fields('".$_module_id."');\">Submit</a></span>".
         "</td></tr>".
         "</table><p style=\"display:none;\" id=\"".$_module_id."_query_string\">".str_replace("&".$_module_id."_fields=".$_REQUEST[$_module_id."_fields"], "", $_SERVER["QUERY_STRING"])."</p>";
         break;


      case "2":
         $t_view=
         "<form name=\"form_search_".$_SESSION["module"][$_module_name]."\" action=\"javascript:t_val=document.getElementsByName('search_".$_SESSION["module"][$_module_name]."')[0].value;if(t_val!='') location.href=('?".str_replace("&".$_SESSION["module"][$_module_name]."_q=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_q"]),"",$_SERVER["QUERY_STRING"])."&".$_SESSION["module"][$_module_name]."_q='+escape(t_val.replace(/ /g,'+'))); else void(0);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\">".
         "<tr>".
         "<td style=\"padding-top:10px;\"><input onkeydown=\"if(event.keyCode==13) document.form_search_".$_SESSION["module"][$_module_name].".submit();\" type=\"text\" value=\"".$_REQUEST[$_SESSION["module"][$_module_name]."_q"]."\" name=\"search_".$_SESSION["module"][$_module_name]."\" style=\"border:solid 1px rgb(99,99,99);width:280px;height:20px;font:normal 12px verdana;\" /></td>".
         "<td style=\"padding-top:10px;\">&nbsp;</td>".
         "<td style=\"padding-top:10px;\"><span class=\"phpbutton\"><a id=\"do_search\" href=\"javascript:if(document.getElementById('report_loaded')) alert('No data loaded'); else document.form_search_".$_SESSION["module"][$_module_name].".submit();\">Search</a></td>".
         "<td style=\"padding-top:10px;\">&nbsp;</td>".
         "<td style=\"padding-top:10px;\"><span class=\"phpbutton\"><a href=\"?".str_replace("&".$_module_id."_q=".$_REQUEST[$_module_id."_q"], "", str_replace("+"," ",$_SERVER["QUERY_STRING"]))."\">Clear</a></td>".
         "<tr>".
         "</table></form>";
         break;


      case "3":
         $t_view=
         "<form name=\"form_lookup_".$_SESSION["module"][$_module_name]."\" action=\"javascript:t_val=document.getElementsByName('lookup_".$_SESSION["module"][$_module_name]."')[0].value;if(t_val!='') location.href=('?".str_replace("&".$_SESSION["module"][$_module_name]."_sortkey=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_sortkey"]),"",str_replace("&".$_SESSION["module"][$_module_name]."_key=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_key"]),"",$_SERVER["QUERY_STRING"]))."&".$_SESSION["module"][$_module_name]."_key='+escape(t_val.replace(/ /g,'+'))+'&".$_SESSION["module"][$_module_name]."_sortkey='+escape(document.getElementById('".$_SESSION["module"][$_module_name]."_selected_value').innerText.replace(/ /g,'+'))); else void(0);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\">".
         "<tr>".
         "<td><input onkeydown=\"if(event.keyCode==13) document.form_lookup_".$_SESSION["module"][$_module_name].".submit();\" type=\"text\" value=\"".$_REQUEST[$_SESSION["module"][$_module_name]."_key"]."\" name=\"lookup_".$_SESSION["module"][$_module_name]."\" style=\"border:solid 1px rgb(99,99,99);width:280px;height:20px;font:normal 12px verdana;\" /></td>".
         "<td>&nbsp;</td>".
         "<td><span id=\"selector_lookup_".$_SESSION["module"][$_module_name]."\">&nbsp;</span></td>".
         "<td>&nbsp;</td>".
         "<td><span class=\"phpbutton\"><a id=\"do_lookup\" href=\"javascript:if(document.getElementById('report_loaded')) alert('No data loaded'); else document.form_lookup_".$_SESSION["module"][$_module_name].".submit();\">Lookup</a></td>".
         "<tr>".
         "</table></form>";
         break;
   }

   $tabs=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tabs\">".
   "<tr><td><img src=\"../../../../library/images/icons/search.gif\" style=\"margin-right:7px;filter:Gray();\" /></td>";

   for($i=0; $i<count($_t_tab_names);$i++) {
      $class=($sel_tab==$i+1) ? "high" : "reg";
      $style=($sel_tab==$i+1) ? " style=\"background-color:#822433;\"" : "";
      $tabs.="<td class=\"".$class."\"".$style."><a href=\"?&".$_SESSION["module"][$_module_name]."_tab=".($i+1)."&".$_SESSION["module"][$_module_name]."_fields=".$_REQUEST[$_SESSION["module"][$_module_name]."_fields"]."&".$_SESSION["module"][$_module_name]."_add=".$_REQUEST[$_SESSION["module"][$_module_name]."_add"]."&".$_SESSION["module"][$_module_name]."_sort=".$_REQUEST[$_SESSION["module"][$_module_name]."_sort"]."&".$_SESSION["module"][$_module_name]."_f2=".$_REQUEST["".$_SESSION["module"][$_module_name]."_f2"]."\">".$_t_tab_names[$i]."</a></td>";
   }

   $tabs.=
   "</tr>".
   "</table>";

   $module=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;margin-bottom:2px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_5_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_5_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\" class=\"module_5_content\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table><br />\r\n";

   $module=str_replace("%%TABS%%", $tabs, $module);
   return $module;

}

?>