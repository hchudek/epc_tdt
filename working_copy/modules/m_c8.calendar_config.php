<?php



$filterNames = array();

require_once("elements/m_c8.calendar_config/m_c19.phase_selector.php");
//require_once("elements/m_c8.calendar_config/m_c18.toolsupplier_selector.php");
//require_once("elements/m_c8.calendar_config/m_c17.category_selector.php");
require_once("elements/m_c8.calendar_config/m_c16.locationfilter_selector.php");
require_once("elements/m_c8.calendar_config/m_c15.subprocessphaseresponsible_selector.php");
require_once("elements/m_c8.calendar_config/m_c14.location_selector.php");
require_once("elements/m_c8.calendar_config/m_c13.body_selector.php");
require_once("elements/m_c8.calendar_config/m_c12.subject_selector.php");
require_once("elements/m_c8.calendar_config/m_c10.tpm_selector.php");
require_once("elements/m_c8.calendar_config/m_c9.calendar_filter_selector.php");
require_once("elements/m_c8.calendar_config/m_c11.field_selector.php");




/**
 * laedt Konfiguration des Kalenders aus PerPortal
 */
function loadCalendarConfig(){

	unset( 	$_SESSION["calendarProfile"] );
	
	// init predefined fields
	$_SESSION["calendarProfile"]['m_c8_options_reminderenabled_value'] = "";
	$_SESSION["calendarProfile"]['m_c8_options_showasrange_value'] = "";
	$_SESSION["calendarProfile"]['m_c8_options_reminder_value'] = "";
	
	$url = $_SESSION["remote_domino_path_perportal"];
	$url.= '/get_value?OpenPage&unid=';
	$url.= $_SESSION["remote_perportal_unid"];
	$url.= '&field=td$calendar';
	$url.= '&noache='.microtime(true);
	
	require_once("../../../library/tools/addin_xml.php");								// XML Library laden
	$html = file_get_authentificated_contents($url);



	if( trim( $html ) === "" ){
		$_SESSION["calendarProfile"]["isInitilialized"] = false;
	}else{
		$_SESSION["calendarProfile"]["isInitilialized"] = true;
	}

	$data = explode("&", $html);
	$data = preg_replace( "/\r|\n/", "", $data );
	foreach ( $data as $entry ){
		$hlp = explode( "=", $entry );
		if( $hlp[1] === "NaN" )
			$hlp[1] = "";
		
		if(  $hlp[0] != "" )
			$_SESSION["calendarProfile"][$hlp[0]] = $hlp[1];
	}

}

function m_c8__calendar_config($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0])."&nbsp;&nbsp;".
   "<a style=\"color:rgb(255,255,255);text-decoration:none;\" href=\"javascript:document.getElementById('t_1').click();\">Options</a>&nbsp;&nbsp;|&nbsp;&nbsp;".
   "<a style=\"color:rgb(255,255,255);text-decoration:none;\" href=\"javascript:document.getElementById('t_2').click();\">Filter</a>&nbsp;&nbsp;|&nbsp;&nbsp;".
   "<a style=\"color:rgb(255,255,255);text-decoration:none;\" href=\"javascript:document.getElementById('t_3').click();\">Content</a>";
 
   // Module Body --------------------------------------------------------------------------------------------------
   $module = m_c8_calendar_config($_application);

   return array($headline, $module);

}

function m_c8_calendar_config($_application) { 
	global $p_data;

	m_c8_loadSessionData( $_application);
	
	$_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
	$_module_id=substr(basename(__FILE__),0,4);
	$_SESSION["module"][$_module_name]=$_module_id;
	
	$headline = "Calendar Configuration";

	 
	$content =
	"<script language=\"javascript\">\r\nvar nsfPath='".$_SESSION ["remote_domino_path_main"]."';\r\n</script>\r\n".
	
	"<script language=\"javascript\" src=\"".$_SESSION["remote_database_path"]."javascript/jqueryui.js\"></script>\r\n".
	"<div style=\"width:100%; border-top:solid 1px rgb(200,200,200);position:relative;top:-3px;\"  >".m_c8__get_content($_application)."</div>\r\n";
	
	
	$module = "<div style=\"padding-left:4px;\" id=\"m_c8_container\">\r\n".$content."</div>\r\n";
    $module.= "<div id=\"calendar\" style=\"font:normal 13px Open Sans;position:absolute;top:0px;left:0px;display:none;z-index:999999;opacity:0.85;background-color:rgb(50;50;50)\"></div>\r\n";
	
	//return array($headline, $module);
	return $module;

}


function m_c8__get_content($_application) {

   global $p_data;
   global $filterNames;



   $calendarSaveLink ="<span class=\"phpbutton\" style=\"display:none;\"><a href=\"javascript:void(0);\" onclick=\"save_calendarToNotes();\">Save Calendar</a></span>";
   $calendarLink ="<a href=\"#\" style=\"visibility:hidden;\" id=\"calendar_saveresult_link\">Path to calendar</a>";
   $dropAttributes = " ondrop=\"handle_dropCalendar(event);\" ondragover=\"allow_drop(event);\" allow_type=\"create_filter,resource,tpm,spprp,locationfilter,toolsupplier,phase\" ";

      
   $is_creator = ($p_data["created_by"] == $_SESSION["domino_user"]) ? true : false;

   $folder = strtolower(str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], $_application["page_id"])))."static/images/".$_application["page_id"].$_REQUEST["unid"]."/".$_SESSION["remote_perportal_unid"]);
   $path = $_SESSION["remote_database_path"]."static/images/".$_application["page_id"].$_REQUEST["unid"]."/".$_SESSION["remote_perportal_unid"];
   $_SESSION["nojs"]["folder"] = $folder;
   $_SESSION["nojs"]["path"] = $path;

   $r .= "<form name=\"calendarprofile\" method=\"post\" action=\"addin/save_data.php\" style=\"background-color:rgb(255,255,255);\">\r\n".
   		"<input type=\"hidden\" name=\"redirect\" value=\"\" />\r\n".
   		"<input type=\"hidden\" name=\"f\" value=\"".$_SESSION["application_id"]."\$calendar\" />\r\n".
   		"<input type=\"hidden\" name=\"v\" value=\"\" />\r\n".
   		"<input type=\"hidden\" name=\"unid\" value=\"".$_SESSION["remote_perportal_unid"]."\" />\r\n".
   		"<input type=\"hidden\" name=\"db\" value=\"".$_SESSION["remote_domino_path_perportal"]."\" />\r\n";


   $r .= "<div id=\"tabs\" style=\"background-color:rgb(255,255,255);\">";
   $r .= "<ul>";
   $r .= "<li><a id=\"t_1\" href=\"#tab3-options\"></a></li>";
   $r .= "<li><a id=\"t_2\" href=\"#tab1-filters\"></a></li>";
   $r .= "<li><a id=\"t_3\" href=\"#tab2-content\"></a></li>";
   $r .="</ul>";
   
   


   foreach( $_SESSION["calendarProfile"] as $key => $value ){
	   	if( $key === "isInitilialized" )
	   		continue;
	   		
	   	if( $_SESSION["calendarProfile"]["isInitilialized"] == true ){
	  		if( substr($key,0, strlen("m_c8_filter")) == "m_c8_filter" )
	   			continue;
	   		if( substr($key,0, strlen("m_c8_subject")) == "m_c8_subject" )
	   			continue;
	   		if( substr($key,0, strlen("m_c8_body")) == "m_c8_body" )
	   			continue;
	   		if( substr($key,0, strlen("m_c8_location")) == "m_c8_location" )
	   			continue;

	   		
	   	}
		$r .= "<input type=\"hidden\" name=\"".$key."\" id=\"".$key."\" value=\"".$value."\" />\r\n";
		
   }
 

   /** FILTERS **/
   $filters = m_c8_get_filters($p_data, $_application);
   foreach($filters as $filter) {
  	if(!in_array($filter["id"], $done)) $r .= "<input type=\"hidden\" id=\"".$filter["id"] ."_value\" name=\"".$filter["id"]. "_value\" value=\"".$filter["value"]."\" />\r\n";
        $done[] = $filter["id"]; 
   }
    
   //$options = m_c8_create_filter($p_data, $_application);
   
   
   /** SUBJECT **/
   $subjects = m_c8_get_subjects($p_data, $_application);
   foreach($subjects as $subject) {
  	if(!in_array($subject["id"], $done)) $r .= "<input type=\"hidden\" id=\"".$subject["id"] ."\" name=\"".$subject["id"]. "\" value=\"".$subject["value"]."\" />\r\n";
        $done[] = $subject["id"]; 
        
   }
 

   /** BODY **/
   $bodies = m_c8_get_bodies($p_data, $_application);
   foreach($bodies as $body) {
  	if(!in_array($body["id"], $done)) $r .= "<input type=\"hidden\" id=\"".$body["id"] ."\" name=\"".$body["id"]. "\" value=\"".$body["value"]."\" />\r\n";
        $done[] = $body["id"]; 
   }
   

   /** LOCATIONS **/
   $locations = m_c8_get_locations($p_data, $_application);
   foreach($locations as $location) {
  	if(!in_array($location["id"], $done)) $r .= "<input type=\"hidden\" id=\"".$location["id"] ."\" name=\"".$location["id"]. "\" value=\"".$location["value"]."\" />\r\n";
        $done[] = $location["id"]; 
        
   }
   
   /** CATEGORIES **/
   $categories = m_c8_get_categories($p_data, $_application);
   foreach($categories as $category) {
  	if(!in_array($category["id"], $done)) $r .= "<input type=\"hidden\" id=\"".$category["id"] ."\" name=\"".$category["id"]. "\" value=\"".$category["value"]."\" />\r\n";
        $done[] = $category["id"]; 
        
   }
   
   		
   	
   		

   		$add["tpm_selector"] = m_c10__tpm_selector($_application, $filters);
   		$add["spprp_selector"] = m_c15__subprocessphaseresponsible_selector($_application, $filters);
   		//$add["filter_selector"] = m_c9__calendar_filter_selector($_application, $filters);
		$add["subject_selector"] = m_c12__subject_selector($_application, $subjects);
		$add["body_selector"] = m_c13__body_selector($_application, $bodies);
		$add["location_selector"] = m_c14__location_selector($_application, $locations);
		$add["locationfilter_selector"] = m_c16__locationfilter_selector($_application, $filters);
	//	$add["category_selector"] = m_c17__category_selector($_application, $filters, $categories);
		//$add["toolsupplier_selector"] = m_c18__toolsupplier_selector($_application, $filters);
		$add["phase_selector"] = m_c19__phase_selector($_application, $filters);


 
   		
   		$r .= "<div id=\"tab1-filters\"\">".
   		"<table border=\"0\" style=\"background-color:rgb(255,255,255); position:relative; top:-8px; left:-4px; border:none;\"><tr><td style=\"width:290px;\">".
   		"<div id=\"accordion_filters\">".
   		
	/*		"<div class=\"inner_portlet1\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/programing-27.png\" style=\"vertical-align:top;position:relative;top:1px;left:1px;margin-right:6px;\" />".$add["filter_selector"][0]."</div>\r\n".
   		"<div id=\"m_c8_period\">\r\n".   		
   		$add["filter_selector"][1].
   		"</div>\r\n".*/
   	

   	
		"<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0);position:relative;top:-8px;\">".$add["tpm_selector"][0]."</span></div>\r\n".
   		"<div id=\"m_c8_tpm\">\r\n".
   		$add["tpm_selector"][1].
   		"</div>\r\n".

   		

   		"<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0); position:relative;top:-8px;\">".$add["spprp_selector"][0]."</span></div>\r\n".
   		"<div id=\"m_c8_spprp\">\r\n".
   		$add["spprp_selector"][1].
   		"</div>\r\n".


		"<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0); position:relative;top:-8px;\">".$add["locationfilter_selector"][0]."</div>\r\n".
   		"<div id=\"m_c8_locationfilter\">\r\n".
   		$add["locationfilter_selector"][1].
   		"</div>\r\n".

   		
   		/*
   		"<span id=\"toolsupplier_list\" style=\"color:rgb(0,0,0);\">\r\n".
   		"<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0); position:relative;top:-8px;\">".$add["toolsupplier_selector"][0]."</div>\r\n".
   		"<div id=\"m_c8_toolsupplier\">\r\n".
   		$add["toolsupplier_selector"][1].
   		"</div>\r\n".
   		"</span>\r\n".*/
   		
  
        	"<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0); position:relative;top:-8px;\">".$add["phase_selector"][0]."</div>\r\n".
   		"<div id=\"m_c8_phase\">\r\n".
   		$add["phase_selector"][1].
   		"</div>\r\n".

   	"</div>\r\n". // END OF ACCORDEON
   	"</td><td style=\"vertical-align: top\">\r\n". // END OF FILTER AREA
   	"<div id=\selectedFilter\" style=\"width:240px;\">".m_8_getFilterData( $filters, $dropAttributes ).
   	"<div style=\"margin-left:15px;margin-top:10px;border:dotted 1px rgb(204,227,238);background-color:rgb(204,227,238);height:50px;padding:2px;border-left: dashed 2px rgb(92,92,92);border-bottom: dashed 2px rgb(92,92,92);border-right: dashed 2px rgb(130,130,130);border-top: dashed 2px rgb(130,130,130);border-radius: 15px; text-align: center;\" ondrop=\"handle_dropCalendar(event);\" ondragover=\"allow_drop(event);\" allow_type=\"create_filter,resource,field,task,tpm,spprp,locationfilter,toolsupplier,phase\">Drop filters here to track the corresponding task also within your Outlook calendar</div>\r\n".
   	"</div>\r\n".
   	"</td></tr></table>".
	"</div>"; // END OF TAB
   		
   	
		$r.= "<div id=\"tab2-content\">".m_8_getContent($_application, $filterNames, $add, $subjects, $bodies, $locations, $categories )."</div>\r\n";
   		$r .= "<div id=\"tab3-options\">".m_8_getOptions($add)."</div>\r\n";
   		 
   		$r .= $calendarSaveLink;
   		$r .= $calendarLink;

   			


$r.="</tabs>";
  

  // if(file_exists($_SESSION["file_path_on_server"]."\calendar\\".$_GET['u']."\activity_tracking.ics") == true) {
	//	$r .= "<span class=\"phpbutton\"><a href=\"calendar/".$_GET['u']."/activity_tracking.ics\">Embed calendar</a></span>";
   // }else{
     // $r .= "<span class=\"phpbutton\"><a href=\"".$_SESSION["remote_domino_path_main"]."/a.generate_calendar?OpenAgent&unid=".$_SESSION["remote_perportal_unid"]."\">Generate calendar</a></span>";          	
   //}
  	$r .= "</form>\r\n";


   return str_replace("Array", "&nbsp;", $r);
}

function m_c8_loadSessionData( $_application ){

	//if( !isset($_SESSION["calendarProfile"])){
		loadCalendarConfig();
	//}
	
/*
		if( isset($_application["calendar"] )){
				
			if( isset( $_application["calendar"][$_SESSION["application_id"]]) ){
	
				$hlpArr = array();
				$tmp = explode( "&",  $_application["calendar"][$_SESSION["application_id"]] );

				foreach( $tmp as $entry ){
					$tmp2 = explode( "=", $entry );
					$hlpArr[$tmp2[0]] = $tmp2[1];
				}
				$_SESSION["calendarProfile"] = $hlpArr;
	
			}
		}

*/
}
function m_c8_get_categories($p_data, $_application ) {
	$data = array();
	$pStr = "m_c8_category_";	
	$strLen = strlen( $pStr );

	m_c8_loadSessionData($_application);
		
	foreach( $_SESSION["calendarProfile"] as $key => $value ){
		
		$pos = strpos($key, $pStr); 
	
		if( $pos !== false ){
			$tmp = substr($key, $pos + $strLen );
			$tmp = explode( "_" , $tmp );
			$entry = array( 
				"no" => $tmp[0],
				"type" => $tmp[1],
				"value" =>  rawurldecode(utf8_encode($value)),
				"id" => $pStr.$tmp[0]."_".$tmp[1],
			);
			$data[] = $entry;
		}
		
		
	}
	
	//die( var_dump( $data ));
	
	return $data;
}

function m_c8_get_subjects($p_data, $_application ) {
	return m_c8_get ( $p_data, $_application, "m_c8_subject_" );
}

function m_c8_get_bodies($p_data, $_application ) {
	return m_c8_get ( $p_data, $_application, "m_c8_body_" );
}
function m_c8_get_locations($p_data, $_application ) {
	return m_c8_get ( $p_data, $_application, "m_c8_location_" );
}

function m_c8_get_filters($p_data, $_application ) {
	$data = array();
	$pStr = "m_c8_filter_";	
	$strLen = strlen( $pStr );

	m_c8_loadSessionData($_application);
		
	foreach( $_SESSION["calendarProfile"] as $key => $value ){
		
		$pos = strpos($key, $pStr); 
	
		if( $pos !== false ){
			$tmp = substr($key, $pos + $strLen );
			$tmp = explode( "_" , $tmp );
			$entry = array( 
				"no" => $tmp[0],
				"type" => $tmp[1],
				"value" =>  rawurldecode(utf8_encode($value)),
				"id" => $pStr.$tmp[0]."_".$tmp[1],
			);
			$data[] = $entry;
		}
		
		
	}
	
	
	return $data;
}

function m_c8_get( $p_data, $_application, $pStr ){
	$data = array();
	$strLen = strlen( $pStr );

	m_c8_loadSessionData($_application);
		
	foreach( $_SESSION["calendarProfile"] as $key => $value ){
		
		$pos = strpos($key, $pStr); 
	
		if( $pos !== false ){
			$tmp = substr($key, $pos + $strLen );
			$tmp = explode( "_" , $tmp );
			$entry = array( 
				"no" => $tmp[0],
				"type" => $tmp[0],
				"value" => "",
				"id" => $pStr.$tmp[0]."_".$tmp[1],
			);
			$data[] = $entry;
		}
		
		
	}
	
	
	return $data;
}

function m_c8_get_options($p_data, $_application ){

	$data = array();
	$strLen = strlen( "m_c8_options_"  );

	m_c8_loadSessionData($_application);

		
	foreach( $_SESSION["calendarProfile"] as $key => $value ){
		
		$pos = strpos($key, $pStr); 
	
		if( $pos !== false ){
			$tmp = substr($key, $pos + $strLen );
			$tmp = explode( "_" , $tmp );
			$entry = array( 
				"no" => $tmp[0],
				"type" => $tmp[1],
				"value" => rawurldecode(utf8_encode($value)),
				"id" => $pStr.$tmp[0]."_".$tmp[1],
			);
			$data[] = $entry;
		}
		
		
	}
		
	if( !isset($_SESSION["calendarProfile"]["m_c8_options_reminderenabled_value"] )){
		$entry = array(
				"no" => "",
				"type" => "reminderEnabled",
				"value" => "0",
				"id" => "m_c8_options_reminderenabled",
		);
		$data[] = $entry;
	
	}
	if( !isset($_SESSION["calendarProfile"]["m_c8_options_reminder_value"] )){
		$entry = array(
				"no" => "",
				"type" => "reminder",
				"value" => "0",
				"id" => "m_c8_options_reminder",
		);
		$data[] = $entry;
	
	}
/*	if( !isset($_SESSION["calendarProfile"]["m_c8_options_showasrange_value"] )){
		$entry = array(
				"no" => "",
				"type" => "range",
				"value" => "0",
				"id" => "m_c8_options_showasrange",
		);
		$data[] = $entry;
	
	}*/
	if( !isset($_SESSION["calendarProfile"]["m_c8_options_dump_value"] )){
		$entry = array(
				"no" => "",
				"type" => "dump",
				"value" => "",
				"id" => "m_c8_options_dump",
		);
		$data[] = $entry;
	
	}
	
	return $data;
	
}

function m_c8_create_filter($arr) {
   $type = strtolower($arr["name"]);
   $dsp = ($_SESSION["perpage"]["m_c8__show_tasks_filter"] == "1") ? "table-cell" : "none";
   $r = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n".
   "<tr>".
   "<th style=\"text-align:left;font-weight:normal;width:120px;overflow:hidden;padding-bottom:4px;\">".$arr["name"]."</th>\r\n".
   "</tr>\r\n";
   foreach($arr["filter"] as $val) {
      $get_image = ($_SESSION["perpage"]["m_c8__show_tasks_".$type] == "all" || in_array($val["value"], explode(";", $_SESSION["perpage"]["m_c8__show_tasks_".$type]))) ? "status-5.png" : "status-5-1.png";
      $r .= 
      "<tr>\r\n".
      "<td style=\"display:".$dsp.";\"><a name=\"td_filter\"></a>".
      "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/".$get_image."\" name=\"img_m_c8__show_tasks_".$type."\" value=\"".$val["value"]."\" onclick=\"set_filter(this, '".$type."');\" style=\"vertical-align:top;margin-right:3px;position:relative;top:-1px;cursor:pointer;\" />".
      $val["dsp"].
      "</td>\r\n".
      "</tr>\r\n";
   }

   $r .= "</table>\r\n";


   return $r;
}


function m_8_getFilterData($filters, $dropAttributes ){
	if(isset($filters)) {
	
   			$content = "<table id=\"m_c8_filter\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign=\"top\"  style=\"width:500px;margin-left:15px;table-layout: fixed;\">\r\n".
   					"<thead id=\"m_c8_tasks_head\">\r\n";
   			$content .= "<tr>
   			<th class=\"calconf\" style=\"height:22px;max-width:270px;width:270px;text-align:left;background-color:rgb(255,194,54);\"".$dropAttributes.">&nbsp;Type</th>
   			<th class=\"calconf\" style=\"width:32px;background-color:rgb(255,194,54);\"".$dropAttributes.">&nbsp;</th>
   			<th class=\"calconf\" style=\"text-align:left;background-color:rgb(255,194,54);\" ".$dropAttributes.">&nbsp;Value</th>
   			</tr>";
   			
   			$content .= "</thead>\r\n";
   		}

   		foreach($filters as $filter) {
   	
			if($filter["type"] == "resource" || $filter["type"] == "tpm" || $filter["type"] == "spprp" || $filter["type"] == "locationfilter" || $filter["type"] == "phase" ) {
			
				switch($filter["type"]){
					case "tpm":
						$fName = "T&PM";
					break;
					case "spprp":
						$fName = "Sub process phase responsible person";
					break;
					case "locationfilter":
						$fName = "Product Location G3";
					break;
					case "toolsupplier":
						$fName = "Tool Supplier";
					break;
					case "phase":
						$fName = "Reportable Date";
					break;
					default:
						$fName = $filter["type"];
					break;
				}				
				
				$content .=
   				"<thead id=\"thead_".$filter["id"]."\" filterformula=\"".$filter["value"]."\" filtertype=\"".$filter["type"]."\">\r\n".
   				"<tr>\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);padding:2px 0px 0px 0px;width:120px;\"".$dropAttributes." >".$fName."</td>\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:center;padding:2px 0px 0px 0px;width:32px;\"".$dropAttributes.">"."<img src=\"/library/images/16x16/edition-43.png\" onclick=\"deleteCalendarEntry('".$filter["id"]."');\">"."</td>\r\n";
   		
   				
   				switch( $filter["type"] ){
   					case "startdate":
   					case "enddate":
   					case "date":
   						
   				/*		$content .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(255,246,234);\">";
   						$content .= "<img src=\"/library/images/16x16/time-3.png\" onclick=\"s = $(document.getElementById('m_c8_container').parentNode).scrollTop();load_calendar(this, '" .$filter["id"] . "', 'm_c8_set_calendar_date', 'Select start date', 79, (255 - s));\" ";
   						$content .= "style=\"cursor:pointer;margin:3px;vertical-align:top;\" />";
   						$content .= "<div class=\"textcontent\" id=\"".$filter["id"]."\"".$dropAttributes.">".$filter["value"]."</div>";
   						$content .= "</td>";*/
   					break;
   					
   					case "tpm":
   					case "spprp":
   					case "locationfilter":
   					case "toolsupplier":
   					case "phase":
   						$content .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);\">";
    					$content .= "<div class=\"textcontent\" id=\"".$filter["id"]."\"".$dropAttributes.">".$filter["value"]."</div>";
   						$content .= "</td>";
   					break;
   					default:
					break;
   				}
   		
   		
   				$content .= "</td>\r\n";
   				$content .= "</tr>\r\n";
   				$content .= "</tbody>\r\n";
			}
   			 
   		}
   		
   		$content .= "</table>\r\n";
   		$content = str_replace("Array", "", $content);
    		return $content;
}
function m_8_getOptions($add){
	
			$selValue = "0";
   			if( isset( $_SESSION["calendarProfile"]["m_c8_options_reminder_value"]) ){
   				$selValue = $_SESSION["calendarProfile"]["m_c8_options_reminder_value"];
   			}
   			
   			$optReminder = array();
   			
   		//	$optReminder[0] = 'immediately';
   		//	$optReminder[5] = '5 Minutes';
   		//	$optReminder[10] = '10 Minutes';
   			$optReminder[15] = '15 Minutes';
   			$optReminder[30] = '30 Minutes';
   			$optReminder[60] = '1 Hour';
   		//	$optReminder[120] = '2 Hours';
   		//	$optReminder[180] = '3 Hours';
   		//	$optReminder[240] = '4 Hours';
   		//	$optReminder[300] = '5 Hours';
   		//	$optReminder[360] = '6 Hours';
   		//	$optReminder[420] = '7 Hours';
   		//	$optReminder[480] = '8 Hours';
   		//	$optReminder[540] = '9 Hours';
   		//	$optReminder[600] = '10 Hours';
   		//	$optReminder[660] = '11 Hours';
   		//	$optReminder[720] = '0,5 Days';
   		//	$optReminder[1080] = '18 Hours';
   			$optReminder[1440] = '1 Day';
   			$optReminder[2880] = '2 Days';
   			$optReminder[4320] = '3 Days';
   		//	$optReminder[5760] = '4 Days';
   			$optReminder[10080] = '1 Week';
   			$optReminder[20160] = '2 Weeks';


			$options .= "<div class=\"inner_portlet3\" id=\"inner_portlet3\" style=\"padding-left:2px;\">";
   			$options .= "<span id=\"options_list\" >";
   			
   			$options .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
   			$options .= "<tr>";
   			$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;width:140px;\">";
   			$options .= "Reminder";
   			$options .= "</td>";
   			$options .= "<td colspan=\"2\" style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;\">";
   			if( $_SESSION["calendarProfile"]["m_c8_options_reminderenabled_value"] == "1" ){
   				$options .= "<img src=\"../../../library/images/16x16/status-5.png\" onclick=\"change_option_reminderEnabled(this);\" style=\"cursor:pointer;\">";
   			}else{
   				$options .= "<img src=\"../../../library/images/16x16/status-5-1.png\" onclick=\"change_option_reminderEnabled(this);\" style=\"cursor:pointer;\">";
   			}
   			$options .= "</td>";
   			$options .= "</tr>";
   			$options .= "<tr>";
   			$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;;\">";
   			$options .="Remind before";
   			$options .= "</td>";
   			$options .= "<td colspan=\"2\" style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;\">";
   			
   			$options .= "<select name=\"m_c8_options_reminderselected\" onchange=\"change_option_reminder(this)\">";
   			foreach( $optReminder as $value=>$key){
   				
   				$options .= "<option value=\"".$value."\"";
   				if( $value == $selValue ){
   					$options .= " selected ";
   				}
   				$options .= ">".$key."</option>\r\n";
   				
   			}
   			$options .= "</select>";
   			$options .= "</td>";
   			$options .= "</tr>";
   			/*$options .= "<tr>";
   			$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(255,246,234);text-align:left;padding:2px 0px 0px 0px;\">";
   			$options .= "Show as range?";
   			$options .= "</td>";
   			$options .= "<td colspan=\"2\" style=\"border:solid 1px rgb(255,255,255);background-color:rgb(255,246,234);text-align:left;padding:2px 0px 0px 0px;\">";
   			if( $_SESSION["calendarProfile"]["m_c8_options_showasrange_value"] === "1" ){
   				$options .= "<img src=\"../../../library/images/16x16/status-5.png\" onclick=\"change_option_showAsRangeEnabled(this);\" style=\"cursor:pointer;\">";
   			}else{
   				$options .= "<img src=\"../../../library/images/16x16/status-5-1.png\" onclick=\"change_option_showAsRangeEnabled(this);\" style=\"cursor:pointer;\">";
   			}
   			$options .= "</td>";
   			$options .= "</tr>";*/
   			$options .= "<tr>\r\n";
			$options .= "<td  style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;\">Start date:</td>\r\n";
         	$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);\"><div class=\"textcontent\" id=\"m_c8_startdate\">".$_SESSION["calendarProfile"]["m_c8_filter_".strtolower($_SESSION["remote_perportal_unid"])."_startdate_value"]."</div></td>\r\n";
         	$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/time-3.png\" onclick=\"s = $(document.getElementById('m_c8_container').parentNode).scrollTop();load_calendar(this, 'm_c8_startdate', 'm_c8_set_calendar_date', 'Select start date', 70, 0);\" style=\"cursor:pointer;margin:3px;vertical-align:top;\" /></td>\r\n";
         	$options .= "</tr>\r\n";
			$options .= "<tr>\r\n";
			$options .= "<td  style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;\">End date:</td>\r\n";
			$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);\"><div class=\"textcontent\" id=\"m_c8_enddate\" style=\"width:100px;\">".$_SESSION["calendarProfile"]["m_c8_filter_".strtolower($_SESSION["remote_perportal_unid"])."_enddate_value"]."</div></td>\r\n";
			$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/time-3.png\" onclick=\"s = $(document.getElementById('m_c8_container').parentNode).scrollTop();load_calendar(this, 'm_c8_enddate', 'm_c8_set_calendar_date', 'Select end date', 70, 0);\" style=\"cursor:pointer;margin:3px;vertical-align:top;\" /></td>\r\n";
			$options .= "</tr>\r\n";
                        $options .= "<tr><td>&nbsp;</td></tr>\r\n";
			$options .= "</table>";
                        $options .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">";
                        $options .= "<tr>\r\n";
                        $options .= "<td><strong>1:&nbsp;</strong></td><td colspan=\"2\">\r\n";
            	        $options .= "<span class=\"phpbutton\"><a href=\"addin/generate_calendar.php\" onclick=\"do_fade(true);\">Generate calendar</a></span><br /><br />";
            	        $options .= "</td>\r\n";
            	        $options .= "</tr>\r\n";
                        $options .= "<tr>\r\n";
                        $options .= "<td><strong>2:&nbsp;</strong></td><td colspan=\"2\">\r\n";
		   	$options .= "<span class=\"phpbutton\"><a href=\"calendar/".$_SESSION["remote_shortname"]."/td_tracking.ics\">Embed calendar</a></span>";
                        $options .= "</td>\r\n";
            	        $options .= "</tr>\r\n";
   			$options .= "</table>";

   			return $options;

}
function m_8_getContent($_application, $filterNames, $add, $subjects, $bodies, $locations, $categories ){
		
		   $dropAttributesSubject = " ondrop=\"handle_dropSubject(event);\" ondragover=\"allow_drop(event);\" allow_type=\"subject\" ";
   			$dropAttributesBody = " ondrop=\"handle_dropBody(event);\" ondragover=\"allow_drop(event);\" allow_type=\"body\" ";
   			$dropAttributesLocation = " ondrop=\"handle_dropLocation(event);\" ondragover=\"allow_drop(event);\" allow_type=\"location\" ";
  			$dropAttributesCategory = " ondrop=\"handle_dropCategory(event);\" ondragover=\"allow_drop(event);\" allow_type=\"category\" ";
   			
  			$add["field_selector"] = m_c11__field_selector($_application);
  			
   			$options = "<div id=\"accordion_content\">";
   			
   			$options .= "<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0);position:relative;top:-8px;\">".$add["subject_selector"][0]."</span></div>\r\n";
   			$options .= "<div><table><tr><td>";
   			$options .= "<div id=\"m_c8_subjectselector\" style=\"width:240px;\">\r\n".$add["subject_selector"][1]."</div>\r\n";;
			$options .= "</td><td style=\"vertical-align:top; margin-left:15px;\">";
			/**
   			 * SUBJECT CONTAINER
   			 */
			$options .= "<div id=\selectedSubject\">";
			$options .= "<div id=\"container_subject\"  allow_type=\"subject\" ondrop=\"handle_dropSubject(event);\" ondragover=\"allow_drop(event);\" class=\"dragbox2\">";
   			$options .= "<table id=\"m_c8_subject\" cellpadding=\"0\" cellspacing=\"0\" valign=\"top\"  style=\"width:400px;table-layout: fixed;\">\r\n";
   			$options .=	"<thead id=\"m_c8_tasks_head\">\r\n";
   			$options .= "<tr>
   			<th class=\"calconf\" style=\"height:22px;text-align:left;background-color:rgb(255,194,54);max-width:80px;width:80px;\"".$dropAttributesSubject.">&nbsp;Type</th>
   			<th class=\"calconf\" style=\"text-align:left;background-color:rgb(255,194,54);width:32px;\"".$dropAttributesSubject.">&nbsp;</th>
   			</tr>";
      		$options .=	"</thead>";
      		$options .=	"<tbody>";
      	
   			foreach($subjects as $subject) {
   				
   				$name = $filterNames["subject-".$subject["type"]];
   				$rowId = str_replace("_value", "", $subject["id"]);
   				$rowId = $rowId."_row";
   				$options .=
   				"<tr id=\"".$rowId."\" filtertype=\"".$subject["type"]."\" filterformula=\"".$subject["type"]."\">\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);padding:2px 0px 0px 0px;width:120px;\"".$dropAttributesSubject." >".$name."</td>\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:center;padding:2px 0px 0px 0px;width:32px;\"".$dropAttributesSubject.">"."<img src=\"/library/images/16x16/edition-43.png\" onclick=\"deleteEntry('".$subject["id"]."');\">";
   				$options .= "<div id=\"".$subject["id"]."\" />";
   				$options .= "</td>\r\n";
   				$options .= "</tr>\r\n";

			}
			$options .="</tbody>";
			$options .= "</table>";
   			$options .= "</div>";
			$options .= "<div style=\"margin-top:10px;border:dotted 1px rgb(204,227,238);background-color:rgb(204,227,238);height:35px;padding-top:12px;padding-left:5px;padding-right:5px;border-left: dashed 2px rgb(92,92,92);border-bottom: dashed 2px rgb(92,92,92);border-right: dashed 2px rgb(130,130,130);border-top: dashed 2px rgb(130,130,130);border-radius: 15px; text-align: center;\" ondrop=\"handle_dropSubject(event);\" ondragover=\"allow_drop(event);\" allow_type=\"subject\">Drop subject here to track the corresponding task also within your Outlook calendar</div>\r\n";
			$options .= "</div>\r\n";
			$options .= "</td></tr></table></div>";
   			
			
			/**
			 * BODY 
			 */
   			$options .= "<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0);position:relative;top:-8px;\">".$add["body_selector"][0]."</span></div>\r\n";
   			$options .= "<div><table><tr><td>";
   			$options .= "<div id=\"m_c8_bodyselector\" style=\"width:240px;\">\r\n".$add["body_selector"][1]."</div>\r\n";
   			$options .= "</td><td style=\"vertical-align:top; margin-left:15px;\">";
   	
			
			$options .= "<div id=\selectedBody\">";
			$options .= "<div id=\"container_body\"  allow_type=\"body\" ondrop=\"handle_dropBody(event);\" ondragover=\"allow_drop(event);\" class=\"dragbox2\">";
   			$options .= "<table id=\"m_c8_body\" cellpadding=\"0\" cellspacing=\"0\" valign=\"top\"  style=\"width:400px;table-layout: fixed;\">\r\n";
   			$options .=	"<thead id=\"m_c8_tasks_head\">\r\n";
   			$options .= "<tr>
   			<th class=\"calconf\" style=\"height:22px;text-align:left;background-color:rgb(255,194,54);max-width:80px;width:80px;\"".$dropAttributesBody.">&nbsp;Type</th>
   			<th class=\"calconf\" style=\"text-align:left;background-color:rgb(255,194,54);width:32px;\"".$dropAttributesBody.">&nbsp;</th>
   			</tr>";
      		$options .=	"</thead>";
      		$options .=	"<tbody>";
      	
			foreach($bodies as $body) {
   				
   				$name = $filterNames["body-".$body["type"]];
   				$rowId = str_replace("_value", "", $body["id"]);
   				$rowId = $rowId."_row";
   				$options .=
   				 "<tr id=\"".$rowId."\" filtertype=\"".$body["type"]."\" filterformula=\"".$body["type"]."\">\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);padding:2px 0px 0px 0px;width:120px;\"".$dropAttributesBody." >".$name."</td>\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:center;padding:2px 0px 0px 0px;width:32px;\"".$dropAttributesBody.">"."<img src=\"/library/images/16x16/edition-43.png\" onclick=\"deleteEntry('".$body["id"]."');\">";
   				$options .= "<div id=\"".$body["id"]."\" />";
   				$options .= "</td>\r\n";
   				$options .= "</tr>\r\n";

			}
			$options .="</tbody>";
			$options .= "</table>";
   			$options .= "</div>";
			$options .= "<div style=\"margin-top:10px;border:dotted 1px rgb(204,227,238);background-color:rgb(204,227,238);height:36px;padding-top:12px;padding-left:5px;padding-right:5px;border-left: dashed 2px rgb(92,92,92);border-bottom: dashed 2px rgb(92,92,92);border-right: dashed 2px rgb(130,130,130);border-top: dashed 2px rgb(130,130,130);border-radius: 15px; text-align: center;\" ondrop=\"handle_dropBody(event);\" ondragover=\"allow_drop(event);\" allow_type=\"body\">Drop Body here to track the corresponding task also within your Outlook calendar</div>\r\n";
			$options .= "</div>\r\n";
			$options .= "</td></tr></table></div>";
			
			
			
			
			/**
			 * LOCATION
			 */
   			$options .= "<div class=\"inner_portlet1\" style=\"background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);\"><span style=\"color:rgb(0,0,0);position:relative;top:-8px;\">".$add["location_selector"][0]."</span></div>\r\n";
   			$options .= "<div><table style=\"margin-left;15px\"><tr><td>";
   			$options .= "<div id=\"m_c8_location_selector\" style=\"width:240px;\">\r\n".$add["location_selector"][1]."</div>\r\n";
			$options .= "</td><td style=\"vertical-align:top; margin-left:15px;\">";
			$options .= "<div id=\selectedLocation\">";
			$options .= "<div id=\"container_location\"  allow_type=\"body\" ondrop=\"handle_dropLocation(event);\" ondragover=\"allow_drop(event);\" class=\"dragbox2\">";
   			$options .= "<table id=\"m_c8_location\" cellpadding=\"0\" cellspacing=\"0\" valign=\"top\"  style=\"width:400px;table-layout: fixed;\">\r\n";
   			$options .=	"<thead id=\"m_c8_tasks_head\">\r\n";
   			$options .= "<tr>
   			<th class=\"calconf\" style=\"height:22px;text-align:left;background-color:rgb(255,194,54);max-width:80px;width:80px;\"".$dropAttributesLocation.">&nbsp;Type</th>
   			<th class=\"calconf\" style=\"text-align:left;background-color:rgb(255,194,54);width:32px;\"".$dropAttributesLocation.">&nbsp;</th>
   			</tr>";
      		$options .=	"</thead>";
      		$options .=	"<tbody>";
      	
   			foreach($locations as $location) {
   				
   				$name = $filterNames["location-".$location["type"]];

   				$rowId = str_replace("_value", "", $location["id"]);
   				$rowId = $rowId."_row";
   				$options .=
   				"<tr id=\"".$rowId."\" filtertype=\"".$location["type"]."\" filterformula=\"".$location["type"]."\">\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);padding:2px 0px 0px 0px;width:120px;\"".$dropAttributesLocation." >".$name."</td>\r\n".
   				"<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:center;padding:2px 0px 0px 0px;width:32px;\"".$dropAttributesLocation.">"."<img src=\"/library/images/16x16/edition-43.png\" onclick=\"deleteEntry('".$location["id"]."');\">";
   				$options .= "<div id=\"".$location["id"]."\" />";
   				$options .= "</td>\r\n";
   				$options .= "</tr>\r\n";

			}
			$options .="</tbody>";
			$options .= "</table>";
   			$options .= "</div>";
			$options .= "<div style=\"margin-top:10px;border:dotted 1px rgb(204,227,238);background-color:rgb(204,227,238);height:35px;padding-top:12px;padding-left:5px;padding-right:5px;border-left: dashed 2px rgb(92,92,92);border-bottom: dashed 2px rgb(92,92,92);border-right: dashed 2px rgb(130,130,130);border-top: dashed 2px rgb(130,130,130);border-radius: 15px; text-align: center;\" ondrop=\"handle_dropLocation(event);\" ondragover=\"allow_drop(event);\" allow_type=\"location\">Drop Location here to track the corresponding task also within your Outlook calendar</div>\r\n";
			$options .= "</div>\r\n";
			$options .= "</td></tr></table></div>";
			
			/**
			 * CATEGORY
			 */
   			/*$options .= "<div class=\"inner_portlet1\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/programing-27.png\" style=\"vertical-align:top;position:relative;top:1px;left:1px;margin-right:6px;\" />".$add["category_selector"][0]."</div>\r\n";
   			$options .= "<div><table><tr><td>";
   			$options .= "<div id=\"m_c8_category_selector\"  style=\"width:240px;\">\r\n".$add["category_selector"][1]."</div>\r\n";
   			$options .= "</td><td style=\"vertical-align:top; margin-left:15px;\">";
 			*/
 			/**
   			 * CATEGORY CONTAINER
   			 */
			/*
			$options .= "<div id=\selectedCategory\>";
			$options .= "<div id=\"container_category\"  allow_type=\"category\" ondrop=\"handle_dropLocation(event);\" ondragover=\"allow_drop(event);\" class=\"dragbox2\">";
   			$options .= "<table id=\"m_c8_category\" cellpadding=\"0\" cellspacing=\"0\" valign=\"top\"  style=\"width:400px;table-layout: fixed;\">\r\n";
   			$options .=	"<thead id=\"m_c8_tasks_head\">\r\n";
   			$options .= "<tr>
   			<th class=\"calconf\" style=\"max-width:80px;width:80px;\"".$dropAttributesCategory.">&nbsp;Type</th>
   			<th class=\"calconf\" style=\"\"".$dropAttributesCategory.">Color</th>
   			<th class=\"calconf\" style=\"width:32px;\"".$dropAttributesCategory.">&nbsp;</th>
   			</tr>";
      		$options .=	"</thead>";
      		$options .=	"<tbody>";
      	
      		$colors = array( "Red" => "red", "Yellow" => "yellow", "Blue" => "blue", "Green" => "green", "Orange" => "orange");
      		foreach($categories as $category) {

   				  				
   				$name = $filterNames["category-".$category["type"]];
   				$name = $category["no"];
   				
   				$rowId = str_replace("_value", "", $category["id"]);
   				$rowId = $rowId."_row";
   				$options .=	"<tr id=\"".$rowId."\" filtertype=\"".$category["type"]."\" filterformula=\"".$category["type"]."\">\r\n";
   				$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(255,246,234);padding:2px 0px 0px 0px;width:120px;\"".$dropAttributesCategory." >".$name."</td>\r\n";
   				$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(255,246,234);padding:2px 0px 0px 0px\"".$dropAttributesCategory." >";
   				
   				$idValue = "m_c8_category_".$category["no"]."_value";
   				$options .= "<select id=\"m_c8_category_select\" idvalue=\"".$idValue."\">";
   				
   				foreach ( $colors as $colorName => $colorValue ){
   					$options .= "<option value=\"".$colorValue."\"";
   					if( $category["value"] == $colorValue )
   						$options .= " selected ";
   						
   					$options .= ">".$colorName."</option>";
   				}
   				$options .= "</select>";
   				$options .= "<script>\r\n";
   				$options .= "var sel = $('[idValue=".$idValue."]');\r\n";
   				$options .= "sel.change( function() {   	 value = $(this).val();	        	id = $(this).attr('idValue');	        	$('#' + id ).val( value );	        	save_calendarToNotes();	         });\r\n";
	         
   				$options .= "</script>\r\n";
   				$options .= "</td>\r\n";
   				
   				$options .= "<td style=\"border:solid 1px rgb(255,255,255);background-color:rgb(255,246,234);text-align:center;padding:2px 0px 0px 0px;width:32px;\"".$dropAttributesCategory.">"."<img src=\"/library/images/16x16/edition-43.png\" onclick=\"deleteEntry('".$category["id"]."');\">";
   				
   		
   				$options .= "<div id=\"".$category["id"]."\" />";
   				$options .= "</td>\r\n";
   				$options .= "</tr>\r\n";

			}
			$options .="</tbody>";
			$options .= "</table>";
   			$options .= "</div>";
			$options .= "<div style=\"margin-left:15px;border:dotted 1px rgb(204,227,238);background-color:rgb(204,227,238);height:40px;padding:2px;border-left: dashed 2px rgb(92,92,92);border-bottom: dashed 2px rgb(92,92,92);border-right: dashed 2px rgb(130,130,130);border-top: dashed 2px rgb(130,130,130);border-radius: 15px; text-align: center;\" ondrop=\"handle_dropCategory(event);\" ondragover=\"allow_drop(event);\" allow_type=\"category\">Drop Category here to track the corresponding task also within your Outlook calendar</div>\r\n";
			$options .= "</div>\r\n";
			$options .= "</td></tr></table></div>";
   			*/
   			$options .= "</div>"; // END OF ACCORDEON
   	
   			
		return $options;
			

}
?>