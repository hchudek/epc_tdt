<?php


function m_r3__accounting_report_generate($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);


   // BODY ---------------------------------------------------------------------------------------------------------

   unset($field);

   $field[] = "Project-No";
   $field[] = "Part No";
   $field[] = "Part Desc.";
   $field[] = "Part Rev.";
   $field[] = "Tool No";
   $field[] = "Tool Desc.";
   $field[] = "Type of Tool";
   $field[] = "DIS No";
   $field[] = "CER No";
   $field[] = "Customer";
   $field[] = "COT";
   $field[] = "SOP";
   $field[] = "Resp. Marketing";
   $field[] = "Resp. PE";
   $field[] = "Resp. T&PM";
   $field[] = "Resp. T&D Purchasing";
   $field[] = "Export Date (DIS)";

   $selected_fields = explode(",", $_REQUEST[$_module_id."_fields"]);
   $t_view = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td><table border=\"0\" cellspacing=\"2\" cellpadding=\"0\" class=\"view_tbl_small\" style=\"width:800px;table-layout:fixed;\"><tr>";
   $count = count($field); for($i = 0; $i < $count; $i++) {
      $img = (in_array(($i + 1), $selected_fields)) ? "status-5" : "status-5-1";
      if(intval($i / 5) == ($i / 5)) $t_view .= "</tr><tr>";
      $t_view .= "<td style=\"overflow:hidden;border:solid 1px rgb(222,222,232); background-color:rgb(243,243,247); padding:2px;\"><nobr><img name=\"selected_field\" onclick=\"select_field(this);\" src=\"../../../../library/images/16x16/".$img.".png\" style=\"width:12px;vertical-align:top;margin-right:4px;position:relative;top:1px;cursor:pointer;\">".$field[$i]."</nobr></td>";
   }
   $t_view .= "</tr>".
   "<td colspan=\"5\" style=\"padding:12px 0px 10px 0px;\">".
   "<span class=\"phpbutton\" style=\"margin-right:4px;\"><a href=\"javascript:void(0);\" onclick=\"select_all('selected_field', true);\">Select all</a></span>".
   "<span class=\"phpbutton\" style=\"margin-right:4px;\"><a href=\"javascript:void(0);\" onclick=\"select_all('selected_field', false);\">Deselect all</a></span>".
   "<span class=\"phpbutton\" style=\"margin-right:4px;\"><a href=\"javascript:void(0);\" onclick=\"submit_selected_fields('".$_module_id."', 'accounting_view');\">Submit</a></span>".
   "</td></tr>".
   "</table>".
   "</td>".
   "<td style=\"vertical-align:top;\">".
   "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\" style=\"margin-left:30px; margin-top:2px;\">".
   "<tr><td style=\"overflow:hidden;border:solid 1px rgb(222,222,232); background-color:rgb(243,243,247); width:160px; padding:2px;\">DATE FROM</td><td style=\"overflow:hidden; padding:2px;\"><img onclick=\"calendar_big_pick_date('', 'g_df', '".rawurlencode("Set start date")."');\" src=\"../../../../library/images/16x16/content-47.png\" style=\"cursor:pointer;vertical-align:top;\" /><td><div style=\"width:80px;margin-left:6px;\" id=\"g_df\">".$_REQUEST["m_r3_df"]."</div></td></tr>".
   "<tr><td style=\"overflow:hidden;border:solid 1px rgb(222,222,232); background-color:rgb(243,243,247); padding:2px;\">DATE TO</td><td style=\"overflow:hidden; padding:2px;\"><img onclick=\"calendar_big_pick_date('', 'g_dt', '".rawurlencode("Set start date")."');\" src=\"../../../../library/images/16x16/content-47.png\" style=\"cursor:pointer;vertivcal-align:top;\" /><td><div style=\"width:80px;margin-left:6px;\" id=\"g_dt\">".$_REQUEST["m_r3_dt"]."</div></td></tr>";
  
   if($_REQUEST["m_r3_add"] != "") {
      $t_view .= "<tr><td colspan=\"3\" style=\"padding-top:10px;\"><span class=\"phpbutton\"><a href=\"javascript:do_refresh('".str_replace("&m_r3_dt=".$_REQUEST["m_r3_dt"], "", str_replace("&m_r3_df=".$_REQUEST["m_r3_df"], "", $_SERVER["QUERY_STRING"]))."')\">Refresh</a></span></td></tr>";
   }

   $t_view .=
   "</table>".
   "</td></tr></table>".
   "<p style=\"display:none;\" id=\"".$_module_id."_query_string\">".str_replace("&".$_module_id."_fields=".$_REQUEST[$_module_id."_fields"], "", $_SERVER["QUERY_STRING"])."</p>";
       

   $tabs=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tabs\">".
   "<tr><td><img src=\"../../../../library/images/icons/search.gif\" style=\"margin-right:7px;filter:Gray();\" /></td>";

   for($i=0; $i<count($_t_tab_names);$i++) {
      $class=($sel_tab==$i+1) ? "high" : "reg";
      $style=($sel_tab==$i+1) ? " style=\"background-color:#822433;\"" : "";
      $tabs.="<td class=\"".$class."\"".$style."><a href=\"?&".$_SESSION["module"][$_module_name]."_tab=".($i+1)."&".$_SESSION["module"][$_module_name]."_fields=".$_REQUEST[$_SESSION["module"][$_module_name]."_fields"]."&".$_SESSION["module"][$_module_name]."_add=".$_REQUEST[$_SESSION["module"][$_module_name]."_add"]."&".$_SESSION["module"][$_module_name]."_sort=".$_REQUEST[$_SESSION["module"][$_module_name]."_sort"]."&".$_SESSION["module"][$_module_name]."_f2=".$_REQUEST["".$_SESSION["module"][$_module_name]."_f2"]."\">".$_t_tab_names[$i]."</a></td>";
   }

   $tabs.=
   "</tr>".
   "</table>";



   $module = $t_view;
   $module = str_replace("%%TABS%%", $tabs, $module);
   return array($headline, $module);

}

?>