<?php

//include_once("addin/post_it.php");
include_once("area.php");
session_start();

// HANDLE MY AREA -----------------------------------------------------------------------------------------------------
if($_application["myarea"] == "") {
   foreach($area as $key => $val) if(in_array($val["userrole"], $_SESSION["remote_userroles"])) $tmp_myarea[] = $key;
   $_application["myarea"] = implode($tmp_myarea, ":");
}

function get_myarea($_application, $page_id) {

   $areas = explode(";", $_application["page"][$page_id]["area"]);
   $myarea = explode(":", $_application["myarea"]);
   $return = false;

   foreach($myarea as $val) {
      if(in_array($val, $areas)) {
         $return = true;
         break; 
      }
   }
   return $return;
}

if($_SERVER["QUERY_STRING"] == "get_project_number") te_header_get_project_number();
function te_header_get_project_number() {
   $load = array("project", "tracker");
   require_once("../addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   print "tdproject = {\r\n";
   print "   number: \"".rawurlencode($tc_data["project"]["number"])."\",\r\n"; 
   print "}\r\n";
   print "tdtracker = {\r\n";
   print "   unid: \"".rawurlencode($tc_data["tracker"]["unid"])."\",\r\n"; 
   print "   basic_ref_pot: \"".implode(",", $tc_data["tracker"]["basic"]["ref_pot"])."\",\r\n"; 
   print "   basic_ref_pot_tmp: \"".$tc_data["tracker"]["basic"]["ref_pot_tmp"]."\",\r\n"; 
   print "};\r\n";
   print "if(tdtracker.basic_ref_pot == \"\") tdtracker.basic_ref_pot = \"".$tc_data["tracker"]["basic"]["ref_pot_tmp"]."\";\r\n";

}

function te_header($_application) {

   global $area;
   $bgcol = ($_application["grid"]["enabled"]) ? "rgb(0,102,158)" : "rgb(0,102,158)"; 
   $setgrid = ($_application["grid"]["enabled"]) ? "0" : "1"; 
   $setimg = ($_application["grid"]["enabled"]) ? "<img src=\"../../../../library/images/16x16/white/edition-5.gif\" style=\"width:16px;border:none;margin-left:5px;margin-top:7px;\" />" : "<img src=\"../../../../library/images/16x16/white/edition-5.png\" style=\"width:16px;border:none;margin-left:5px;margin-top:7px;\" />";
   $setbread = ($_application["breadcrumbing"] == "1") ? "0" : "1";
   $breadimg = ($_application["breadcrumbing"] == "1") ? "<img src=\"../../../../library/images/16x16/white/triangle-big-1-01.png\" style=\"width:16px;border:none;margin-left:5px;margin-top:7px;\" />" : "<img src=\"../../../../library/images/16x16/white/triangle-big-2-01.png\" style=\"width:16px;border:none;margin-left:5px;margin-top:7px;\" />";
   $skin = ($_application["skin"] == "2") ? "leisure-15" : "leisure-16";
   $setskin = ($_application["skin"] == "2") ? "1" : "2";


   $welcome = ($_application["domino_user_cn"] != "") ? "Welcome,&nbsp;".$_application["domino_user_cn"] : "Please authentify yourself";


   $line1 = "\r\n".
   "         <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"te_top_navigation\" style=\"position:fixed; z-index:99999; background-color:rgb(0,102,158);border-bottom:solid 2px rgb(255,255,255);\">\r\n".
   "            <tr>\r\n".
   "               <td class=\"text\" style=\"width:1%;\"><nobr>".$welcome."</nobr></td>\r\n".
   "               <td>\r\n".
   "                  <div id=\"navcontainer\">\r\n".
   "                     <ul id=\"navi\">\r\n".                
   "                        <li id=\"m0\"><a href=\"javascript:void(0);\"><img src=\"../../../../library/images/16x16/white/multimedia-4.png\" />My area</a>\r\n".
   "                           <ul>\r\n".
   "                              <li style=\"height:1px;\"><a href=\"javascript:void(0);\" style=\"background-color:rgb(99,177,229);\">&nbsp;</a></li>\r\n";


   // ------- HANDLE + INCLUDE AREAS -------------------------------------------------------------------------------------------
   foreach($area as $key => $val) {
      if(in_array($val["userrole"], $_SESSION["remote_userroles"])) {
         $myarea = explode(":", $_application["myarea"]);
         $img = (in_array($key, $myarea)) ? "status-5" : "status-5-1"; 
         $line1 .= "                              <li><a href=\"javascript:void(0);\" onclick=\"handle_area(this);\" name=\"myarea\" key=\"".$key."\" onmouseover=\"t=this.firstChild;t.src=t.src.replace('16x16/','16x16/white/');\" onmouseout=\"t=this.firstChild;t.src=t.src.replace('white/','')\"><img src=\"../../../../library/images/16x16/".$img.".png\" style=\"position:relative;left:-2px;width:13px;\" />".rawurldecode($val["description"])."</a></li>\r\n";
      }
   }
   unset($img);

   $line1 .=
   "                           </ul>\r\n".
   "                        </li>\r\n".
   "                        <li id=\"m1\"><a href=\"javascript:void(0);\"><img src=\"../../../../library/images/navigate.gif\" style=\"border:none;vertical-align:top;margin-right:2px; filter:chroma(color=#ff0000);\" />Navigate</a>\r\n".
   "                           <ul>\r\n";

   // START: NAVIGATION EINBINDEN
   foreach($_application["page"] as $value) {
      if(is_string($value)) {
         if(get_myarea($_application, $value)) {
            if(check_userrole($_application["page"][$value]["access_user_role"]) || !isset($_application["page"][$value]["access_user_role"])) {
               if($_application["page"][$value]["navigator"]["level"]) {
                  $t_attributes=(isset($_application["page"]["link"][$value])) ? $_application["page"]["link"][$value] : "";
                  $t_attributes = str_replace("DOLLAR", "$", $t_attributes);
                  $txt = ($t_attributes != "") ? "*" : "";
                  $line1 .= "                              <li><a href=\"".$value.".php".$t_attributes."\"><img src=\"../../../../library/images/icons/bookmark.gif\" style=\"margin-right:7px;margin-top:3px;border:none;vertical-align:top;width:12px;\">".$_application["page"][$value]["page_descr"].$txt."</a></li>\r\n";
               }
            }
         }
      }
   }



   foreach($_application["page"] as $value) {
      if(is_string($value) && $_application["page"][$value]["type"]=="userpage" && $_application["page"][$value]["status"]=="active") {
         if(check_userrole($_application["page"][$value]["access_user_role"]) || !isset($_application["page"][$value]["access_user_role"])) {
           $userpage = true;
           $t_attributes=(isset($_application["page"][$value]["navigator"]["attributes"])) ? "?&".$_application["page"][$value]["navigator"]["attributes"] : "";
           $t_attributes = str_replace("DOLLAR", "$", $t_attributes);
           $txt = ($t_attributes != "") ? "*" : "";
           $line1 .= "                              <li><a href=\"".$value.".php".$t_attributes."\"><img src=\"../../../../library/images/icons/bookmark.gif\" style=\"margin-right:7px;margin-top:3px;border:none;vertical-align:top;width:12px;\">".$_application["page"][$value]["page_descr"].$txxt."</a></li>\r\n";
         }
      }
   }
   if($userpage) $line1 = str_replace("%%SEP%%", "<li style=\"height:1px;\"><a href=\"javascript:void(0);\" style=\"background-color:rgb(99,177,229);\">&nbsp;</a></li>", $line1);
   else str_replace("%%SEP%%", "", $line1);
   // ENDE: NAVIGATION EINBINDEN


   $img[0] = ($_SESSION["_header"]["type"] == "1") ? "high" : "reg";
   $img[1] = ($_SESSION["_header"]["type"] == "1") ? "reg" : "high";



   $img[99] = (isset($_SESSION["header"]["template"]) && $_SESSION["header"]["template"] != "0") ? "high" : "reg";

   if($img[99] == "high") {
      $img[11] = ($_SESSION["header"]["template"] == "1") ? "high" : "reg";
      $img[12] = ($_SESSION["header"]["template"] == "2") ? "high" : "reg";
      $img[13] = ($_SESSION["header"]["template"] == "3") ? "high" : "reg";
      $img[14] = ($_SESSION["header"]["template"] == "4") ? "high" : "reg";
   }
   else {
      $img[11] = ($_application["page"][$_application["page_id"]]["template"]["name"] == "template_01") ? "high" : "reg";
      $img[12] = ($_application["page"][$_application["page_id"]]["template"]["name"] == "template_02") ? "high" : "reg";
      $img[13] = ($_application["page"][$_application["page_id"]]["template"]["name"] == "template_03") ? "high" : "reg";
      $img[14] = ($_application["page"][$_application["page_id"]]["template"]["name"] == "template_04") ? "high" : "reg";
   }

   $line1 .=
   "                           </ul>\r\n".
   "                        </li>\r\n".
   "                        <li id=\"m2_1\"><a href=\"javascript:void(0);\"><img src=\"../../../../library/images/16x16/white/edition-16.png\" style=\"border:none;vertical-align:top;width:14px;margin-right:4px;\" />Perspective</a>\r\n".
   "                           <ul>\r\n".
   "                              <li style=\"height:1px;\"><a href=\"javascript:void(0);\" style=\"background-color:rgb(99,177,229);\">&nbsp;</a></li>\r\n";
//   "                              <li><a href=\"javascript:void(0);\" onclick=\"d=document.perpage.index_tab;d.value='".$bp."';save_perpage('', true);\">Standard</a></li>\r\n";


   $bp = 0;
   foreach($_application["page"][$_application["page_id"]]["template"]["bp"] as $key => $val) {
      $bp++;
      $line1 .= "                              <li><a href=\"javascript:void(0);\" onclick=\"d=document.perpage.active;d.value='SYSTEM_".$bp."';save_perpage('', true);\">".$key."</a></li>\r\n";
   }

   $bp = 0;
   foreach($_SESSION["perspectives"][$_application["page_id"]] as $key => $val) {
      $bp++;
      $line1 .= "                              <li><a href=\"javascript:void(0);\" onclick=\"d=document.perpage.active;d.value='CUSTOM_".$bp."';save_perpage('', true);\">".substr($key, strpos($key, ":") + 1, strlen($key))."</a></li>\r\n";

   }


   $line1 .=
   "                           </ul>\r\n".
   "                        </li>\r\n".

   "                        <li id=\"m2\"><a href=\"javascript:void(0);\"><img src=\"../../../../library/images/configure.gif\" style=\"border:none;vertical-align:top;filter:chroma(color=#ff0000);\" />Configure</a>\r\n".
   "                           <ul>\r\n".
   "                              <li style=\"height:1px;\"><a href=\"javascript:void(0);\" style=\"background-color:rgb(99,177,229);\">&nbsp;</a></li>\r\n".
   // "                              <li><a href=\"configuration.php?&m_x1_visibility=false&m_x2_visibility=true\">Application settings</a></li>\r\n".
   "                              <li><a href=\"calendar_config.php\">Calendar configuration</a></li>\r\n".
   "                              <li><a href=\"conf_process.php\">Customize process</a></li>\r\n".
   "                           </ul>\r\n".
   "                        </li>\r\n".
   "                        <li id=\"m3\"><a href=\"javascript:void(0);\"><img src=\"../../../../library/images/create.gif\" style=\"border:none;vertical-align:top;margin-right:1px;filter:chroma(color=#ff0000);\" />Create</a>\r\n".
   "                           <ul>\r\n".
   "                              <li style=\"height:1px;\"><a href=\"javascript:void(0);\" style=\"background-color:rgb(99,177,229);\">&nbsp;</a></li>\r\n".
   "                              <li><a href=\"create_tracker.php\">Tracker</a></li>\r\n";
   if($_application["page_id"] == "create_tracker") $line1 .=  "                              <li><a href=\"javascript:infobox('Create standlone project', '<iframe frameborder=0 src=modules/z_create_project.php width=850 height=570></iframe>', 900, 600);\">Standalone project</a></li>\r\n";

   if($_application["page_id"] == "tracker") {

      //$load = array("tracker");
      //require_once("addin/load_tracker_data.php");
      $tc_data = create_tc_data($load);
      $line1 .= "                              <li><a href=\"javascript:include_add_pot('project', tdtracker.unid);\">Add / Connect POT to tracker</a></li>\r\n";
      $line1 .= "                              <li><a href=\"javascript:include_add_pot('proposal', tdtracker.unid);\">Add Proposal POT to tracker</a></li>\r\n";
      $line1 .= "                              <li><a href=\"javascript:if(typeof tdtracker.basic_ref_pot != 'undefined') include_add_pot('inspection', tdtracker.unid, tdtracker.basic_ref_pot);\" id=\"header:add_inspection_pot\">Add inspection POT to tracker</a></li>\r\n";
   }

   $line1 .=
   "                           </ul>\r\n".
   "                        </li>\r\n".
   "                        <li id=\"m5\"><a href=\"".$_SESSION["php_server"]."/logout.php\"><img src=\"../../../../library/images/log_off.gif\" style=\"border:none;vertical-align:top;filter:chroma(color=#ff0000);\" />Log off</a></li>\r\n".
   "                     </ul>\r\n".
   "                     <div style=\"width:40px;border-right:dotted 1px rgb(255,255,255);height:32px;float:left;\">".
   "                        <a href=\"addin/set_header.php?set=".base64_encode("type:1")."&redirect=".base64_encode($_application["page_id"].".php?".$_SERVER["QUERY_STRING"])."\"><img alt=\"Presentation mode\" src=\"../../../../library/images/up_".$img[0].".gif\" style=\"border:none;margin-left:5px;margin-top:7px;filter:chroma(color=#ff0000);\" /></a>".
   "                        <a href=\"addin/set_header.php?set=".base64_encode("type:0")."&redirect=".base64_encode($_application["page_id"].".php?".$_SERVER["QUERY_STRING"])."\"><img alt=\"Basic mode\" src=\"../../../../library/images/down_".$img[1].".gif\" style=\"border:none;margin-top:7px;filter:chroma(color=#ff0000);\" /></a>".
   "                     </div>\r\n".
//   "                     <div style=\"width:28px;border-right:dotted 1px rgb(255,255,255);height:32px;float:left;\">".
//   "                        <a href=\"javascript:void(0);\" onclick=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_perportal"]."', '".$_SESSION["remote_perportal_unid"]."', '".$_SESSION["application_id"]."\$breadcrumbing', '".$setbread."', 'location.reload();');\">".$breadimg."</a>".
//   "                     </div>\r\n".
   "                     <div id=\"grid_enabled\" grid_enabled=\"".(($setgrid - 1) * -1)."\" style=\"width:28px;border-right:dotted 1px rgb(255,255,255);height:32px;float:left;background-color:".$bgcol.";\">".
   "                        <a href=\"javascript:void(0);\" onclick=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_perportal"]."', '".$_SESSION["remote_perportal_unid"]."', '".$_SESSION["application_id"]."\$grid', '".$setgrid."', 'location.reload();');\">".$setimg."</a>".
   "                     </div>\r\n".
//   "                     <div style=\"width:26px;border-right:dotted 1px rgb(255,255,255);height:32px;float:left;\">".
//   "                        <a href=\"javascript:void(0);\"><img onclick=\"d=document.add_to_linklist;d.add.value=location.href;d.dsp.value=document.getElementsByTagName('title')[0].textContent;d.submit();\" src=\"../../../../library/images/16x16/white/vote-6.png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px;\" /></a>".
//   "                     </div>\r\n".
//   "                     <div style=\"width:26px;border-right:dotted 1px rgb(255,255,255);height:32px;float:left;\">".
//   "                        <a href=\"javascript:void(0);\"><img onclick=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_perportal"]."', '".$_SESSION["remote_perportal_unid"]."', '".$_SESSION["application_id"]."\$skin', '".$setskin."', 'location.reload();');\" src=\"".$_SESSION["php_server"]."/library/images/16x16/white/".$skin.".png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px;\" /></a>".
//   "                     </div>\r\n".

//   "                     <div style=\"width:26px;border-right:dotted 1px rgb(255,255,255);height:32px;float:left;\">".
//   "                        <a href=\"javascript:void(0);\"><img onclick=\"do_trouble_mail();\" src=\"".$_SESSION["php_server"]."/library/images/16x16/white/Business-48.png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px;\" /></a>".
//   "                     </div>\r\n".

//   "                     <div style=\"width:26px;border-right:dotted 1px rgb(255,255,255);height:32px;float:left;\">".
//   "                        <a href=\"javascript:void(0);\"><img onclick=\"wiki.embed('tdt');\" src=\"".$_SESSION["php_server"]."/library/images/16x16/white/edition-25.png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px;\" /></a>".
//   "                     </div>\r\n".

   "                  </div>\r\n".
   "                  </div>\r\n".
   "               </td>\r\n".
   "               <td class=\"server_name\">".$_SERVER["HTTP_HOST"]."<span id=\"indicate_save\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/hardware-20.png\" style=\"vertical-align:top;position:relative;left:11px;\"></span></td>\r\n".
   "            </tr>\r\n".
   "         </table>\r\n";



//   $usemargin = ($_SESSION["_header"]["type"] != "1") ? "27" : "0";


   $line3 = "\r\n         <div class=\"bottom_orange\" id=\"header_buttons\" style=\"margin-top:10px; width:100%;z-index:99998; clear:both;\">".create_button($_application)."</div>\r\n";

   $module =
   "<form name=\"bestpractise\" method=\"post\" action=\"addin/save_data.php\" style=\"display:none;\">\r\n".
   "<input type=\"text\" name=\"redirect\" value=\"\" />\r\n".
   "<input type=\"text\" name=\"f\" value=\"td\$grid_".$_application["page_id"]."\" />\r\n".
   "<input type=\"text\" name=\"v\" value=\"\" />\r\n".
   "<input type=\"text\" name=\"unid\" value=\"".$_SESSION["remote_perportal_unid"]."\" />\r\n".
   "<input type=\"text\" name=\"db\" value=\"".$_SESSION["remote_domino_path_perportal"]."\" />\r\n".
   "</form>\r\n".

   "<form name=\"add_to_linklist\" method=\"post\" action=\"addin/edit_linklist.php\" style=\"display:none;\">\r\n".
   "<input type=\"text\" name=\"add\" value=\"http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]\" />\r\n".
   "<input type=\"text\" name=\"dsp\" value=\"\" />\r\n".
   "<input type=\"text\" name=\"linklist\" value=\"\" />\r\n".
   "</form>\r\n";


   $module .=
   "<!--begin header-->".
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; white-space:nowrap;\">\r\n".
   "   <thead>\r\n".
   "      <tr>\r\n".
   "         <td >".
    "   <div id=\"suiteBar\" class=\"ms-dialogHidden noindex\" >".
    "      <div id=\"suiteBarLeft\">".
    "         <div class=\"ms-table ms-fullWidth\">".
    "               <div class=\"ms-tableRow\">".
    "                  <div class=\"ms-tableCell ms-verticalAlignMiddle\" >".
    "                     <div style=\"position: relative; top:14px;\" class=\"ms-core-brandingText\">".$_SESSION["remote_database_name"].
    "                     <div style=\"position:absolute; z-index:99999;\"><input onkeyup=\"ftsearch.do(this);\" id=\"ftsearch\" style=\"position:relative; left:310px; top:-10px; width:200px; border:none; color: rgb(129,129,129); font: normal 13px open sans; padding-left:2px; visibility: hidden;\"></div>".
    "                     </div>".
    "                  </div>".
    "                     <div id=\"DeltaSuiteLinks\" class=\"ms-core-deltaSuiteLinks\">".
    "                        <div id=\"suiteLinksBox\">".
    "                        </div>".
    "                     </div>".
    "                  </div>".
    "               </div>".
    "                  <div style=\"text-align: right; padding-right:25px; position: relative; bottom:6px;\" >".
    "                        <a href=\"javascript:void(0);\" onclick=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_perportal"]."', '".$_SESSION["remote_perportal_unid"]."', '".$_SESSION["application_id"]."\$breadcrumbing', '".$setbread."', 'location.reload();');\">".$breadimg."</a>".
    "                        <a id=\"grid_enabled\" grid_enabled=\"".(($setgrid - 1) * -1)."\" href=\"javascript:void(0);\" onclick=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_perportal"]."', '".$_SESSION["remote_perportal_unid"]."', '".$_SESSION["application_id"]."\$grid', '".$setgrid."', 'location.reload();');\">".$setimg."</a>".
    "                        <a href=\"javascript:void(0);\"><img onclick=\"d=document.add_to_linklist;d.add.value=location.href;d.dsp.value=document.getElementsByTagName('title')[0].textContent;d.submit();\" src=\"../../../../library/images/16x16/white/vote-6.png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px;\" /></a>".
    "                        <a href=\"javascript:void(0);\"><img onclick=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_perportal"]."', '".$_SESSION["remote_perportal_unid"]."', '".$_SESSION["application_id"]."\$skin', '".$setskin."', 'location.reload();');\" src=\"".$_SESSION["php_server"]."/library/images/16x16/white/".$skin.".png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px;\" /></a>".
    "                        <a href=\"javascript:void(0);\"><img onclick=\"do_trouble_mail();\" src=\"".$_SESSION["php_server"]."/library/images/16x16/white/Business-48.png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px;\" /></a>".
    "                        <a href=\"javascript:void(0);\"><img onclick=\"wiki.embed('tdt');\" src=\"".$_SESSION["php_server"]."/library/images/16x16/white/edition-25.png\" style=\"width:16px;border:none;margin-top:7px;margin-left:5px; \" /></a>".
    "                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$_SERVER["HTTP_HOST"]."<span id=\"indicate_save\"><img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/hardware-20.png\" style=\"vertical-align:top;position:relative;left:11px; top:8px;\"></span>".
    "                  </div>".
    "               </div>".
    "               <div id=\"suiteBarRight\">".
    "                     <div id=\"DeltaSuiteBarRight\" class=\"ms-core-deltaSuiteBarRight\">".
    "                     <div id=\"welcomeMenuBox\"><span class=\"ms-menu-althov ms-welcome-root\">".$_application["domino_user_cn"]."</span>".
    "                        </div>".
    "                        <div id=\"suiteBarButtons\">".
    "                        </div>".
    "                     </div>".
    "                  </div>".
    "               </div>".
    "               <div style=\"position:absolute; left:20px; top:33px; display:none; width:1px; height: 860px; z-index: 999999;\" id=\"ftsearch-res\"></div>".
    "               <div class=\"ms-cui-topBar2\" style=\"background-color:#f2f2f2;\"></div>".
    "               <table width=\"100%\" border=\"0\" style=\"border-top: 1px solid #cfcfcf; background-color: #e2e2e2; padding: 0px; margin: 0px; border-collapse: collapse;\">".
    "                  <tbody><tr><td style=\"margin: 0px; padding: 0px; border-collapse: collapse; border-spacing: 0px;\">".
    "                     <div style=\"width: 1245px; margin-left: 99px;\">".
    "                        <table style=\"width: 1245px; margin: 0px; padding: 0px; border-collapse: collapse;\">".
    "                           <tbody ><tr><td style=\"line-height: 0; margin: 0px; padding: 0px; border-collapse: collapse;\">".
    "                              <a href=\"https://portal.connect.te.com/sites/myTE/\"><img style=\"border: none;\" src=\"http://eva1.de.tycoelectronics.com/library/images/te/myTE_Logo_50.png\"></a></td><td>".
    "                              <div style=\"position: relative; display:block; z-index:99999; \">".
    "                                 <ul id=\"menu\">";



    // ------- HANDLE + INCLUDE AREAS -------------------------------------------------------------------------------------------
    $module .=
    "                                     <li anchor=\"img:head\"><a href=\"#\" class=\"drop\"><img src=\"../../../../library/images/multimedia-4_b.gif\" style=\"border:none;vertical-align:top;margin-right:2px; \" />&nbsp;Areas</a>".
    "                                        <div class=\"dropdown_1column\"><div class=\"col_1\"><ul><h3><a href=\"#\">".$_application["domino_user_cn"]."'s Area</a></h3>";
    foreach($area as $key => $val) {
       if(in_array($val["userrole"], $_SESSION["remote_userroles"])) {
          $myarea = explode(":", $_application["myarea"]);
          $img = (in_array($key, $myarea)) ? "status-5" : "status-5-1";
          $module .= "                                        <li><a href=\"javascript:void(0);\" onclick=\"handle_area(this);\" name=\"myarea\" key=\"".$key."\" onmouseover=\"t=this.firstChild;t.src=t.src.replace('16x16/','16x16/white/');\" onmouseout=\"t=this.firstChild;t.src=t.src.replace('white/','')\"><img src=\"../../../../library/images/16x16/".$img.".png\" style=\"position:relative;left:-2px;width:13px;\" />&nbsp;&nbsp;&nbsp;".rawurldecode($val["description"])."</a></li>\r\n";
       }
    }
    unset($img);
    $module .= "                                        </ul></div></li>";

    // START: NAVIGATION
    $module .=
    "                                     <li anchor=\"img:head\"><a href=\"#\" class=\"drop\"><img src=\"../../../../library/images/navigate_b.gif\" style=\"border:none;vertical-align:top;margin-right:2px; \" />&nbsp;Navigate</a>".
    "                                        <div class=\"dropdown_1column\"><div class=\"col_1\"><ul><h3><a href=\"#\">Navigate on Tracking</a></h3>";
    foreach($_application["page"] as $value) {
       if(is_string($value)) {
          if(get_myarea($_application, $value)) {
             if(check_userrole($_application["page"][$value]["access_user_role"]) || !isset($_application["page"][$value]["access_user_role"])) {
                if($_application["page"][$value]["navigator"]["level"]) {
                   $t_attributes=(isset($_application["page"]["link"][$value])) ? $_application["page"]["link"][$value] : "";
                   $t_attributes = str_replace("DOLLAR", "$", $t_attributes);
                   $txt = ($t_attributes != "") ? "*" : "";
                   $module .= "                                        <li><a href=\"".$value.".php".$t_attributes."\"><img src=\"../../../../library/images/icons/bookmark.gif\" style=\"margin-right:7px;margin-top:3px;border:none;vertical-align:top;width:12px;\">&nbsp;&nbsp;".$_application["page"][$value]["page_descr"].$txt."</a></li>\r\n";
                }
             }
          }
       }
    }
    $module .= "                                        </ul></div></li>";

    // START: Perspective
    $module .=
    "                                                      <li anchor=\"img:head\"><a class=\"drop\" href=\"javascript:void(0);\"><img src=\"../../../../library/images/edition-16_b.gif\" style=\"border:none;vertical-align:top;width:14px;margin-right:4px;\" />Perspective</a>\r\n".
    "                                                         <div class=\"dropdown_1column\"><div class=\"col_1\"><ul><h3><a href=\"#\">Coose Perspective</a></h3>";
    $bp = 0;
    foreach($_application["page"][$_application["page_id"]]["template"]["bp"] as $key => $val) {
       $bp++;
       $module .= "                              <li><a href=\"javascript:void(0);\" onclick=\"d=document.perpage.active;d.value='SYSTEM_".$bp."';save_perpage('', true);\">".$key."</a></li>\r\n";
    }

    $bp = 0;
    foreach($_SESSION["perspectives"][$_application["page_id"]] as $key => $val) {
       $bp++;
       $module .= "                              <li><a href=\"javascript:void(0);\" onclick=\"d=document.perpage.active;d.value='CUSTOM_".$bp."';save_perpage('', true);\">".substr($key, strpos($key, ":") + 1, strlen($key))."</a></li>\r\n";
    }
    $module .= "                                        </ul></div></li>";

    // START: Create
    $module .=
    "                                                      <li anchor=\"img:head\"><a class=\"drop\" href=\"javascript:void(0);\"><img src=\"../../../../library/images/create_b.gif\" style=\"border:none;vertical-align:top;margin-right:1px;\" />Create</a>\r\n".
    "                                                         <div class=\"dropdown_1column\"><div class=\"col_1\"><ul><h3><a href=\"#\">Create</a></h3>".
    "                                                            <li><a href=\"create_tracker.php\">Tracker</a></li>\r\n";
    if($_application["page_id"] == "create_tracker") $module .=  "                                                            <li><a href=\"javascript:infobox('Create standlone project', '<iframe frameborder=0 src=modules/z_create_project.php width=850 height=570></iframe>', 900, 600);\">Standalone project</a></li>\r\n";
    if($_application["page_id"] == "tracker") {
      $tc_data = create_tc_data($load);
      $module .= "                              <li><a href=\"javascript:include_add_pot('project', tdtracker.unid);\">Add / Connect POT</a></li>\r\n";
      $module .= "                              <li><a href=\"javascript:include_add_pot('proposal', tdtracker.unid);\">Add Proposal POT</a></li>\r\n";
      $module .= "                              <li><a href=\"javascript:if(typeof tdtracker.basic_ref_pot != 'undefined') include_add_pot('inspection', tdtracker.unid, tdtracker.basic_ref_pot);\" id=\"header:add_inspection_pot\">Add inspection POT</a></li>\r\n";
    }
    $module .= "                                        </ul></div></li>\r\n";

    // START: log off
    $module .=
    "                                                      <li anchor=\"img:head\"><a class=\"drop\" href=\"".$_SESSION["php_server"]."/logout.php\"><img src=\"../../../../library/images/log_off_b.gif\" style=\"border:none;vertical-align:top;\" />&nbsp;Log off</a></li>\r\n";


    $module .=


    "                                 </ul></div></td>\r\n".
    "                     </div></tr></tbody></table>\r\n".
    "                     </td></tr></tbody></table></div>\r\n".$line3.
    "                  </td>\r\n".
    "               </tr>\r\n".
    "            <thead>\r\n".
    "         </table>\r\n".
    "<!--end header-->\r\n";



   return $ttitle.$module;
}


function generate_secondary_nav($t_module, $_hide_modules,$_application) {

   $t_module=str_replace(".php", "", basename(__FILE__));

   if(!in_array($t_module, $_hide_modules)) {
      $n_modules=implode(",",$_hide_modules);
      $n_modules=($n_modules=="") ? $t_module : $n_modules.",".$t_module; 
      $t_replace[0]="action_portlet_collapse.gif";
      $tmp=str_replace("hide_module=".$_REQUEST["hide_module"], "", $_SERVER["QUERY_STRING"]);
      $t_replace[1]=($tmp=="") ? "?hide_module=".$n_modules : "?".$tmp."&hide_module=".$n_modules;
      $t_replace[2]="<div class=\"module_content\">\r\n%%NAVIGATION%%<div><img src=\"../../../../library/images/blank.gif\" style=\"width:4px;\" /></div></div>\r\n";

      $navigation=""; $line=false;

      foreach($_application["page"] as $value) {
         if(is_string($value)  && ($_application["page"][$value]["type"]=="userpage" && $_application["page"][$value]["status"]=="active" || $_application["page"][$value]["navigator"]["level"]!="")) {
            if($_application["page"][$value]["type"]=="userpage" && $line==false) {
               $navigation.="<div style=\"background-color:rgb(255,255,255);margin-top:4px;\" /><img src=\"../../../../library/images/blank.gif\" style=\"width:170px;height:2px;\"></div>";
               $line=true;
            }
            $t_class=($value==$_application["page_id"]) ? "secondary_navigator_high" : "secondary_navigator_reg";
            $t_attributes=(isset($_application["page"][$value]["navigator"]["attributes"])) ? "?&".$_application["page"][$value]["navigator"]["attributes"] : "";
            $navigation.="<div class=\"".$t_class."\"><a href=\"".$value.".php".$t_attributes."\">".$_application["page"][$value]["page_descr"]."</a></div>";
         }
      }
      $t_replace[2]=str_replace("%%NAVIGATION%%", $navigation, $t_replace[2]);
   }
   else {
      $n_hide_module=str_replace($t_module, "", str_replace(",".$t_module, "", $_REQUEST["hide_module"]));
      $t_replace[0]="action_portlet_expand.gif";
      $t_replace[1]="?".str_replace("hide_module=".$_REQUEST["hide_module"], "hide_module=".$n_hide_module, $_SERVER["QUERY_STRING"]);
      $t_replace[2]="<div style=\"font:normal 5px Open Sans;\">&nbsp;</div>\r\n";
   }

   $replace=split(",", "%%IMAGE%%,%%HREF%%,%%CONTENT%%");
   return "<div style=\"margin-bottom:2px;\">".$navigation."</div>";
}

function create_button($_application) {
   $_html="<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\"><tr><td><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"height:25px; display:none;\" id=\"tbl_nav_bottom\"><tr>".
   "<td id=\"add_usernavigation\">!!!</td>";
   $c=count($_application["topbutton"]["name"]);
   for($i=0;$i<$c;$i++) {
      $class=($i<$c-1) ? "topbutton_orange" : "topbutton_orange";
      $_html.="<td style=\"border-right:dotted 1px rgb(255,255,255);\"><a class=\"".$class."\" href=\"".$_application["topbutton"]["link"][$i]."\">".$_application["topbutton"]["name"][$i]."</a></td>";
   }
   $_html .= "</tr></table></td><td style=\"width:98%;\">".add_breadcrumbing($_application)."</td><td style=\"text-align:right;\">".add_perspective($_application)."</td></tr></table>";

  return $_html;

}



function add_perspective($_application) {
   foreach($_application["page"][$_application["page_id"]]["template"]["bp"] as $key => $val) {
      $tab[] = array("SYSTEM", $key);
   }
   foreach($_SESSION["perspectives"][$_application["page_id"]] as $key => $val) {
      $tab[] = array("CUSTOM", $key, $val);
   }
   $_SESSION["perpage"][$_application["page_id"]."_tab"] = (isset($_SESSION["perpage"][$_application["page_id"]."_tab"])) ? $_SESSION["perpage"][$_application["page_id"]."_tab"] : "1";

   foreach($tab as $val) $count[$val[0]] = 0;
   $prv_tab = "SYSTEM_1";

   foreach($tab as $val) {
      $count[$val[0]]++;
      $class = ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "tab_high_header_".$val[0] : "tab_reg_header_".$val[0];
      $class .= "_orange";
      $onmouseover = ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "" : "this.className='tab_high_header_".$val[0]."_orange'";
      $onmouseout = ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "" : "this.className='tab_reg_header_".$val[0]."_orange'";
      $onclick= ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "" : "d=document.perpage.active;d.value='".$val[0]."_".$count[$val[0]]."';save_perpage('', true);"; 
      $content .= 
      "<td class=\"".$class."\" onclick=\"".$onclick."\" onmouseover=\"".$onmouseover."\" onmouseout=\"".$onmouseout."\">";
      if($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"] && $_application["grid"]["enabled"] && $val[0] == "SYSTEM") {
         $reset = str_replace(".", "__", implode($_application["page"][$_application["page_id"]]["template"]["bp"][$val[1]], "/"));
         $content .= "&nbsp;&nbsp;<img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/design-21.png\" style=\"position:absolute;cursor:pointer;\" onclick=\"d=document.perpage.".$val[0]."_".$count[$val[0]].";d.value='".$reset."';save_perpage('SYSTEM', true);\" />&nbsp;&nbsp;&nbsp;";
      }
      if($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"] && $_application["grid"]["enabled"] && $val[0] == "CUSTOM") {
         $exp = explode(",", $val[2]);
         if(count($exp) > 1) {
            foreach(explode("/", $val[2]) as $gm) {
               $added_modules[] = substr($gm, strrpos($gm, ",") + 1, strlen($gm));
            }
         }
         else $added_modules[] = $val[2];
         $content .= "&nbsp;&nbsp;<img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/edition-16.png\" style=\"position:absolute;cursor:pointer;\" onclick=\"add_perspective(false, '".$_application["page_id"]."', '".substr($val[1], 0, strpos($val[1], ":"))."', '".substr($val[1], strpos($val[1], ":") + 1, strlen($val[1]))."', '".str_replace(array("__", "/"), array(".", ";"), implode(";", $added_modules))."', 'CUSTOM_".$count["CUSTOM"]."');\" />&nbsp;&nbsp;&nbsp;";

      }
      $txt = $val[1];
      $val[1] = ($_application["grid"]["enabled"] && $val[0] == "CUSTOM" && $val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "<span ondblclick=\"edit_field('Edit perspective', this.textContent, 20, 'document.perpage.".$val[0]."_".$count[$val[0]]."_dsp.value = v; save_perpage(\\'CUSTOM\\', true);');\">".$txt."</span>" : $txt;
      $val[1] = ":".$val[1];
      $content .= "&nbsp;&nbsp;&nbsp;&nbsp;".substr($val[1], strrpos($val[1], ":") + 1, strlen($val[1]))."&nbsp;&nbsp;&nbsp;&nbsp;</td>\r\n";
      $prv_tab = $val[0]."_".$count[$val[0]];

   }


   $dsp_add = ($_application["grid"]["enabled"]) ? "" : "display:none;";

   $r = 
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding-left:6px;position:relative;top:0px;\" id=\"te_perspective\">\r\n".
   "<tr>\r\n".
   "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>\r\n".$content."<td style=\"border-left:dotted 1px rgb(255,255,255);\">&nbsp;</td>\r\n".
   "<td><img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/interface-78.png\" style=\"position:relative;top:2px;cursor:pointer;".$dsp_add."\" onclick=\"add_perspective(true, '".$_application["page_id"]."', 'CUSTOM_".(1 + $count["CUSTOM"])."');\" />&nbsp;&nbsp;</td>\r\n".
   "</tr>\r\n".
   "</table>\r\n";

   return $r;

}


function add_breadcrumbing($_application) {


   $dsp_breadcrumbing = ($_application["breadcrumbing"] == "0") ? "none;" : "inline-block";

   $tmp=(function_exists("get_userpages")) ? " style=\"padding-left:20px;\"" : "";
   $padding = ($_SESSION["header"]["type"] != "1") ? "padding-top:0px;padding-bottom:0px;" : "padding-top:12px;padding-bottom:0px;";
   $module="<div".$tmp."><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"".$padding."\"><tr><td>%%PLUGGED%%</td><td style=\"font:normal 16px Open Sans;\"><a href=\"".$_SESSION["remote_index"]."\"><img src=\"../../../../library/images/16x16/places-1.png\" style=\"border:none;float:left;position:relative;top:-1px;left:-16px;margin-left:2px;\"></a></td><td>";
   $level = $_application["page"][$_application["page_id"]]["breadcrumbing"]["level"];

   $_SESSION["level"]["name"][0] = "Welcome";
   $_SESSION["level"]["link"][0] = $_SESSION["remote_database_path"];
   $_SESSION["level"]["name"][$level] = $_application["page"][$_application["page_id"]]["page_descr"];
   $_SESSION["level"]["link"][$level] = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

   if($_application["page_id"]=="index") $level = 0;

   $module = "<div style=\"display:inline-block;padding-left:4px;margin:0px;\">\r\n".
   "</div>\r\n".
   "<div style=\"display:".$dsp_breadcrumbing.";padding-left:4px;\" class=\"basefont\">\r\n";

   for($i = 0; $i <= $level; $i++) {
      if(isset($_SESSION["level"]["name"][$i])) {
         $txt = ($level == $i) ? "<b>".$_SESSION["level"]["name"][$i]."</b>" : $_SESSION["level"]["name"][$i];
         $module.="<a href=\"".$_SESSION["level"]["link"][$i]."\" style=\"font:normal 12px Open Sans;color:rgb(255,255,255);text-decoration:none;\" onmouseover=\"this.style.textDecoration='underline';\" onmouseout=\"this.style.textDecoration='none';\">".$txt."</a>\r\n";
         if($level>$i && $_SESSION["level"]["name"][1]!=$_SESSION["level"]["name"][0]) {
            $module.="<img src=\"".$_SESSION["php_server"]."/library/images/16x16/white/chevron-medium-big-2-01.png\" style=\"position:relative;top:3px;left:-1px;width:15px;\" />";
         }
      }
   }

   $module .= "</div>\r\n";

   return $module;

}


?>