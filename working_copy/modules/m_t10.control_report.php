<?php


function m_t10__control_report($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   return array($headline, $module);

}


function m_t10__control_report_generate_html($_application) {

   global $tc_data;  
   
   $uqs["number"] = (is_array($tc_data["uqs"]["unid"])) ? "&nbsp;" : "<a href=\"".$_SESSION["remote_domino_path_epcquoting"]."/_/".$tc_data["uqs"]["unid"]."?open\" target=\"_blank\">".rawurldecode($tc_data["uqs"]["number"])."</a>";
   $uqs["name"] = (is_array($tc_data["uqs"]["unid"])) ? "&nbsp;" : rawurldecode($tc_data["uqs"]["name"]);
   
   $module = 
   "<table id=\"tbl_t10__control_report\">".
   "<tr>".
   "<td class=\"label\" style=\"width:1%;\"><nobr><img src=\"../../../../library/images/16x16/white/edition-50.png\" onclick=\"connect_uqs('".$tc_data["uqs"]["pot_unid"]."');\" style=\"margin-right:4px;vertical-align:top;cursor:pointer;\" />Number:&nbsp;&nbsp;&nbsp;</nobr></td>".
   "<td class=\"data\" colspan=\"3\">".$uqs["number"]."&nbsp;</td>".

   "</tr>".
   "<tr>".
   "<td class=\"label\" style=\"width:1%;padding-left:22px;\"><nobr>Name:&nbsp;&nbsp;&nbsp;</nobr></td>".
   "<td class=\"data\" colspan=\"3\">".$uqs["name"]."&nbsp;</td>".
   "</tr>".
   "</table>";
   return $module;

}


?>
