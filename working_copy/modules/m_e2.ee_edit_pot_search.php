<?php


function m_e2_ee_edit_pot_search($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $sel_tab=($_REQUEST[$_SESSION["module"][$_module_name]."_tab"]=="") ? "1" : $_REQUEST[$_SESSION["module"][$_module_name]."_tab"]; 
   $_t_tab_names[]="Search";
   $_t_tab_names[]="Lookup";

   switch($sel_tab) {

     
      case "1":
         $t_view=
         "<form name=\"form_search_".$_SESSION["module"][$_module_name]."\" action=\"javascript:t_val=document.getElementsByName('search_".$_SESSION["module"][$_module_name]."')[0].value;if(t_val!='') location.href=('?".str_replace("&".$_SESSION["module"][$_module_name]."_q=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_q"]),"",$_SERVER["QUERY_STRING"])."&".$_SESSION["module"][$_module_name]."_q='+escape(t_val.replace(/ /g,'+'))); else void(0);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\">".
         "<tr>".
         "<td style=\"padding-top:0px;\"><input onkeydown=\"if(event.keyCode==13) document.form_search_".$_SESSION["module"][$_module_name].".submit();\" type=\"text\" value=\"".$_REQUEST[$_SESSION["module"][$_module_name]."_q"]."\" name=\"search_".$_SESSION["module"][$_module_name]."\" style=\"border:solid 1px rgb(99,99,99);width:280px;height:20px;font:normal 12px verdana;\" /></td>".
         "<td style=\"padding-top:0px;\">&nbsp;</td>".
         "<td style=\"padding-top:0px;\"><span class=\"phpbutton\"><a id=\"do_search\" href=\"javascript:if(document.getElementById('report_loaded')) alert('No data loaded'); else document.form_search_".$_SESSION["module"][$_module_name].".submit();\">Search</a></td>".
         "<td style=\"padding-top:0px;\">&nbsp;</td>".
         "<td style=\"padding-top:0px;\"><span class=\"phpbutton\"><a href=\"?".str_replace("&".$_module_id."_q=".$_REQUEST[$_module_id."_q"], "", str_replace("+"," ",$_SERVER["QUERY_STRING"]))."\">Clear</a></td>".
         "<tr>".
         "</table></form>";
         break;


      case "2":
         $t_view=
         "<form name=\"form_lookup_".$_SESSION["module"][$_module_name]."\" action=\"javascript:t_val=document.getElementsByName('lookup_".$_SESSION["module"][$_module_name]."')[0].value;if(t_val!='') location.href=('?".str_replace("&".$_SESSION["module"][$_module_name]."_sortkey=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_sortkey"]),"",str_replace("&".$_SESSION["module"][$_module_name]."_key=".str_replace(" ","+",$_REQUEST[$_SESSION["module"][$_module_name]."_key"]),"",$_SERVER["QUERY_STRING"]))."&".$_SESSION["module"][$_module_name]."_key='+escape(t_val.replace(/ /g,'+'))+'&".$_SESSION["module"][$_module_name]."_sortkey='+escape(document.getElementById('".$_SESSION["module"][$_module_name]."_selected_value').innerText.replace(/ /g,'+'))); else void(0);\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\">".
         "<tr>".
         "<td><input onkeydown=\"if(event.keyCode==13) document.form_lookup_".$_SESSION["module"][$_module_name].".submit();\" type=\"text\" value=\"".$_REQUEST[$_SESSION["module"][$_module_name]."_key"]."\" name=\"lookup_".$_SESSION["module"][$_module_name]."\" style=\"border:solid 1px rgb(99,99,99);width:280px;height:20px;font:normal 12px verdana;\" /></td>".
         "<td>&nbsp;</td>".
         "<td><span id=\"selector_lookup_".$_SESSION["module"][$_module_name]."\">&nbsp;</span></td>".
         "<td>&nbsp;</td>".
         "<td><span class=\"phpbutton\"><a id=\"do_lookup\" href=\"javascript:if(document.getElementById('report_loaded')) alert('No data loaded'); else document.form_lookup_".$_SESSION["module"][$_module_name].".submit();\">Lookup</a></td>".
         "<tr>".
         "</table></form>";
         break;
   }

   $tabs=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tabs\">".
   "<tr><td><img src=\"../../../../library/images/icons/search.gif\" style=\"margin-right:7px;filter:Gray();\" /></td>";

   for($i=0; $i<count($_t_tab_names);$i++) {
      $class=($sel_tab==$i+1) ? "high" : "reg";
      $style=($sel_tab==$i+1) ? " style=\"background-color:#822433;\"" : "";
      $tabs.="<td class=\"".$class."\"".$style."><a href=\"?&".$_SESSION["module"][$_module_name]."_tab=".($i+1)."&".$_SESSION["module"][$_module_name]."_dsp=".$_REQUEST[$_SESSION["module"][$_module_name]."_dsp"]."&".$_SESSION["module"][$_module_name]."_add=".$_REQUEST[$_SESSION["module"][$_module_name]."_add"]."&".$_SESSION["module"][$_module_name]."_sort=".$_REQUEST[$_SESSION["module"][$_module_name]."_sort"]."&".$_SESSION["module"][$_module_name]."_f2=".$_REQUEST["".$_SESSION["module"][$_module_name]."_f2"]."\">".$_t_tab_names[$i]."</a></td>";
   }

   $tabs.=
   "</tr>".
   "</table>";

   $module=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;margin-bottom:2px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_5_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_5_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\" class=\"module_5_content\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table><br />\r\n";

   $module=str_replace("%%TABS%%", $tabs, $module);
   return $module;

}

?>