<?php

if($_REQUEST["css"] != "") {
   header("Content-Type: text/css");
   $id = $_REQUEST["css"];
   print "#un".$id.", #".$id." {\r\n";
   print "	margin: 0;\r\n";
   print "    	padding: 2px 0px 0px 0px;\r\n";
   print "    	float: left;\r\n";
   print "    	margin-right: 1px;\r\n";
   print "	height:280px;\r\n";
   print "	width:140px;\r\n";
   print "	overflow-y:auto;\r\n";
   print "	overflow-x:hidden;\r\n";
   print "}\r\n";

   print "#un".$id." div, #".$id." div {\r\n";
   print "	margin:1px;\r\n";
   print "    	padding:2px;\r\n";
   print "    	font:normal 13px Open Sans;\r\n";
   print "    	width:130px;\r\n";
   print "	height:15px;\r\n";
   print "}\r\n";
}


if($_REQUEST["m_e1__smartsearch_filter"] != "") {
   session_start();
   $id = str_replace("%", "_", rawurlencode(str_replace("@", "_", $_REQUEST["m_e1__smartsearch_filter"])));
   require_once("../../../../library/tools/addin_xml.php");							// XML Library laden
   require_once("../../../../library/tools/view/generate_view.php");

   // GET DATA
   $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.get_tracker?open&count=99999&function=plain");

   $explode = explode(":", $file);
   for($i = 1; $i <= 1 + substr_count($explode[0], ";"); $i++) $in_view[] = $i;

   $data = get_table($file, $in_view);
   $count = 0;


   foreach($data[0] as $val) {
      if(strtolower($val) == strtolower($_REQUEST["m_e1__smartsearch_filter"])) {
         break;
      }
      $count++;
   }
   $arr = array();
   $counter = array();

   foreach($data[1] as $v) {
      $val = (strrpos($v[$count], "=>")) ? substr($v[$count], 0, strrpos($v[$count], "=>")) : $v[$count];
      if(!in_array($val, $arr) && trim($val) != "") $arr[] = $val;
      $counter[$val]++;
   }
   sort($arr);
   print "<span id=\"selected_".$id."\">\r\n";
    foreach($arr as $val) {
      print "<div>".$val." [".$counter[$val]."]</div>\r\n";
   }
   print "</span>\r\n";
}



function m_e1__smartsearch_get_html($_application, $l_data, $fieldlist, $keys) {
   require_once("../../../library/tools/view/generate_view.php");   
   // CONFIG ------------------------------------------------------------------------------------------------------------------------------
   

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   $function = __FUNCTION__;
   $mid = substr(__FUNCTION__, 0, strpos(__FUNCTION__, "__"));

   // Überschrift und Modulsonderzubehör erstellen
   $img1=($_REQUEST[$_module_id."_f2"]=="") ? "select_reg.png" : "select_high.png";
   $img2=($_REQUEST[$_module_id."_f2"]=="") ? "select_high.png" : "select_reg.png";
   $tabs=
   "<table border=\"0\" cellspacing=\"0\"0 cellpadding=\"0\" style=\"width:100%;font:normal 12px verdana;text-transform:uppercase;\"><tr><td>&nbsp;&nbsp;Browse tracker</td><td style=\"text-align:right;\">".
   "</td></table>";


   // Ansicht einbinden -------------------------------------------------------------------------------------------------------------------
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"] = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15";
   $in_view = explode(",", $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_in_view"]);
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"] = "1";
   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] = "2";

   $sort_column = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"];
   $sort_dir = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? SORT_DESC : SORT_ASC;

   $responsible = ($_REQUEST["m_e1__responsible"] != "") ?  $_REQUEST["m_e1__responsible"] : "";

   foreach($fieldlist as $fl) { 
      foreach($keys as $v) if($v[0] == $fl) { $res = $v[1]; break; }
      $f[] = $res."@140"; 
      $no_filter[] = $fl;
   }
   $file = implode(";", $f).":";

   foreach($l_data as $key => $val) {
      if($responsible == "" || $responsible == strtolower(rawurldecode($val["tr_responsible"]))) {
         $f = array();
         foreach($fieldlist as $k) $f[] = $val[$k];  
         $f[0] .= "=>".$key;
         $file .= implode(";", $f).":";
         $fdone = true;
      }
   }

   $data = get_table($file, $in_view);

   foreach($data[0] as $val) {
      $key = str_replace("%", "", rawurlencode(substr($val."@", 0, strpos($val."@", "@"))));
      if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_".$key] = "";
   }
   $count = 0;
   foreach($data[0] as $val) {
      $count++;
      $saved_filter = $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];
      if($saved_filter != "") $filter[$count] = explode("</>", strtolower($saved_filter));
   }

   $data[1] = filter_view($data[1], $filter);
   $data[1] = sort_view($data[1], $data[2][$sort_column], $sort_dir);

   // ADD PAGE COL SWITCH ----------------------------------------
   $t_width = 70;
   if(implode($in_view) != "") {
      for($i = 0; $i < count($in_view); $i++) {
        $width = substr($data[3][$in_view[$i] - 1], strpos($data[3][$in_view[$i] - 1], "@") + 1, strlen($data[3][$in_view[$i] - 1]));
        $t_width += intval($width);
        $css .= "#smartsearch a:nth-child(".($i + 1).") {\r\n".
                "	width:".$width."px;\r\n".
                "}\r\n";

        $added .= "<div class=\"ui-state-default-sort\" row=\"".$in_view[$i]."\">".substr($data[3][$in_view[$i] - 1], 0, strpos($data[3][$in_view[$i] - 1], "@"))."</div>\r\n";
      }
   }
   else {
     $data[1] = ""; 
   }
   for($i = 0; $i < count($data[3]); $i++) {
      if(!in_array(($i + 1), $in_view)) {
         $not_added .= "<div class=\"ui-state-default-sort\" row=\"".($i + 1)."\">".substr($data[3][$i], 0, strpos($data[3][$i], "@"))."</div>\r\n";
      }
   }
  
   $module = "\r\n<style type=\"text/css\">\r\n".$css."</style>\r\n";   


   // HEADLINE --------------------------------------------------
   $module .= "<span id=\"smartsearch\">\r\n".
   "<input class=\"readonly\" readonly=\"readonly\" type=\"text\" id=\"smartsearch_search\" name=\"search\" value=\"".$_POST["search"]."\" onkeydown=\"this.setAttribute('clock', Date.now());\" onkeyup=\"window.setTimeout('smartsearch.check_do_filter(\'smartsearch.body\')', 800);\" />\r\n".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/interface-32.png\" onclick=\"smartsearch.configure('".__FUNCTION__."', 'smartsearch.".__FUNCTION__.".fieldlist');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px;margin-left:4px;margin-right:7px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/text-6.png\" onclick=\"smartsearch.kill_filter('".__FUNCTION__."');\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/table.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', false);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<img src=\"".$_SESSION["php_server"]."/library/images/excel-icon.png\" onclick=\"smartsearch.excel.infobox('smartsearch.body', true);\" style=\"cursor:pointer;vertical-align:top;position:relative;top:6px; margin-left:2px;margin-right:5px;\" />".
   "<span style=\"font:normal 13px Open Sans;\">Results:&nbsp;<a id=\"smartsearch_results\" count=\"".count($data[1])."\">".count($data[1])."&nbsp;/&nbsp;".count($data[1])."</a></span>\r\n";

   $count = 0;
   $module .= "<div style=\"white-space:no-wrap;width:".$t_width."px;overflow:hidden;\"><c id=\"smartsearch.head\" clear=\"true\" style=\"display:none;\">\r\n";

   foreach($data[0] as $val) {

      if($val != "") {
         $id[$val] = generate_uniqueid(8);
         $img = "chevron-reg";
         if($in_view[$count] == $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_column"]) {
            $img = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sort_dir"] == "2") ? "chevron-desc" : "chevron-asc";
         }
         $tooltip = $_SESSION["perpage"]["tab"]["tracker_view"][__FUNCTION__.str_replace("%", "_", rawurlencode("_filter_".str_replace("@", "_", $val)))];
         $usecolor = (is_array($filter[$count + 1])) ? "rgb(205,32,44)" : "rgb(0,0,0)";
         $usedir = (is_array($filter[$count + 1])) ? "_red" : "";
         $img_filter = "";
         $module.= "<a smartsearch.tooltip=\"".str_replace(" ", "&nbsp;", $tooltip)."\" style=\"color:".$usecolor.";\" name=\"smartsearch.sort.usemap\" img:usedir=\"".$usedir."\" img:sortdir=\"".$img."\" smartsearch.usemap:id=\"".$id[$val]."\" module:id=\"".str_replace("_get_html", "", __FUNCTION__)."\" area:link=\"use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 2);use_addin_sort(".$in_view[$count].", '".__FUNCTION__."', 1)\" area:coords=\"2,2,9,7;11,2,18,7\" ".$img_filter."img:link=\"smartsearch.configure('".__FUNCTION__."', '".$id[$val]."'); smartsearch.embed_filter(this, '".$id[$val]."', '".$val."');\">".substr($val, 0, strpos($val, "@"))."</a>\r\n";
         $count++;
      }
   }
   $module .= "</c></div>\r\n";
   // BODY -----------------------------------------------------
   $module .= "<div id=\"smartsearch.body\" clear=\"true\" php:function=\"".__FUNCTION__."\" style=\"display:none;\">\r\n";
   $e = 0;
   
   foreach($data[1][0] as $key => $val) {
      if(strpos($val, "=>")) {
         $pointer = $key;
         break;
      }
   }

   foreach($data[1] as $td) {

      $module .= "<p do=\"location.href='tracker.php?&pot=".str_replace("@", "&unique=", trim(substr($td[$pointer], strrpos($td[$pointer], "=>") + 2, strlen($td[$pointer]))))."';\">";
      $e++;
      foreach($td as $val) {
         $val = explode("=>", $val);
         $module .= "<a>".utf8_encode($val[0])."</a>";
      }
      $module .= "</p>\r\n";
   }

   $module .= "</div>\r\n".
   "</span>\r\n";

   // FIELDLIST -----------------------------------------------
   $module .= 
   "<div id=\"smartsearch.".__FUNCTION__.".fieldlist\" style=\"margin-top:7px;display:none;\"\">\r\n".
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<td style=\"position:relative;top:27px;left:-6px;\"><span class=\"phpbutton\" style=\"position:relative;top:-4px;left:4px;\"><a href=\"javascript:void(0);\" onclick=\"do_save_filter('".__FUNCTION__."');\">Apply selection</a></span></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Included</b></td>\r\n".
   "<td style=\"padding-left:2px;\"><b>Not included</b></td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_selected\" class=\"connected_sortable\">".$added."</span></td>\r\n".
   "<td style=\"background-color:rgb(243,243,247);border:solid 3px rgb(243,243,247);\"><span id=\"filter_unselected\" class=\"connected_sortable\">".$not_added."</span></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n".
   "</div>\r\n";


   // FILTER ---------------------------------------------------
   foreach($data[0] as $val) {
      if($val != "") {
         $module .= "<div id=\"".$id[$val]."\"></div>\r\n";
      }
   }

   $module .= "<p style=\"display:none;\" id=\"smartsearch.configure\" nest_init_view=\"".implode($id, ":")."\">smartsearch.".__FUNCTION__.".fieldlist:".implode($id, ":")."</p>";

   // EMBED MODULE ---------------------------------------------      
   $module = 
   "<table id=\"tbl_".__FUNCTION__."\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n".
   "      <tr>\r\n".
   "         <td>".$module."</td>\r\n".
   "      </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));  
   return $module;


}


?>