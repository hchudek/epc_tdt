<?php

function m_t3__uqs($_application) {

   if($_REQUEST["pot"] != "") $_SESSION["pot"] = $_REQUEST["pot"];
   if($_REQUEST["unique"] != "") $_SESSION["tracker"] = $_REQUEST["unique"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
//$module =       m_t3__uqs_generate_html($_application);
   return array($headline, $module);

}


function m_t3__uqs_generate_html($_application) {

   global $tc_data;

   // IS INSPECTION POT ? ---------------------------------------------------------------------------------------------
   $pot_status = explode("@", $tc_data["pot"]["status"]);
   $inspection_pot = ($pot_status[1] != $_SESSION["tracker"]) ? $pot_status[1] : false;

   if(!$inspection_pot == false) {
      $tc_data["meta"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:meta/".$_SESSION["pot"]."@".$inspection_pot."?open");
   }


   if (trim(implode($tc_data["uqs"]), ",") == "") {
     $module = "&nbsp;";
   }
   else {

      $pot_status["000"] = "Ungrabbed";
      $pot_status["100"] = "Grabbed";
      $pot_status["200"] = "Unrealized";
      $pot_status["300"] = "Reconditioned";
      $pot_status["400"] = "Finalized";
      $pot_status["999"] = "Released";
      $pot["status"] = explode("@", $tc_data["pot"]["status"]);

      if(!$inspection_pot == false) {
         foreach($pot_status as $key => $val) $pot_status[$key] = "<a href=\"?unique=".$tc_data["meta"]["ref_tracker"]."&pot=".$tc_data["meta"]["ref_pot"]."\">".$pot_status[$key]."@".substr($tc_data["meta"]["id"], 0, strpos($tc_data["meta"]["id"], "+"))."</a>";
      }

      $dsp_type = (is_array($tc_data["meta"]["process_type"])) ? "" : $_application["process"]["displayed_name"][$tc_data["meta"]["process_type"]]." [".str_replace("v->2", " Version 2", $tc_data["meta"]["process_type"])."]";
      $dsp_rule = (is_array($tc_data["meta"]["process_rule"])) ? "" : $_application["process"]["rules"][$tc_data["meta"]["process_rule"]]["displayed_name"]." [".$tc_data["meta"]["process_rule"]."]";


      $uqs["number"] = (is_array($tc_data["uqs"]["unid"])) ? "&nbsp;" : "<a href=\"".$_SESSION["remote_domino_path_epcquoting"]."/0/".$tc_data["uqs"]["unid"]."?open\" target=\"_blank\">".rawurldecode($tc_data["uqs"]["number"])."</a>";

      
      $uqs["name"] = (is_array($tc_data["uqs"]["unid"])) ? "&nbsp;" : rawurldecode($tc_data["uqs"]["name"]);

//      if(!$inspection_pot == false) {
//         $module = "<div style=\"margin-bottom:12px; padding:2px; margin-left:6px;\"><img src=\"../../../../library/images/16x16/edition-29.png\" style=\"position:relative;left:-5px; vertical-align:top;\">Inspection only</div>\r\n";
//      }

      $module .= 
      "<table id=\"tbl_m_t3__uqs\" style='table-layout:fixed;width:100%' border=\"0\" cellpadding=\"0\" cellspacing=\"2\"><tr>\r\n";

      if($tc_data["tracker"]["responsible"] == $_SESSION["domino_user_cn"] && !in_array($pot["status"][0], array("200", "400"))) {
         $module .= "<td><a href=\"javascript:m_t3.change_status('".$tc_data["pot"]["unid"]."', ".str_replace("\"", "'", json_encode($pot_status)).", [200, 400], '".$tc_data["pot"]["unique"]."');\">POT status</a></td>\r\n";
      }
      else {
         $module .= "<td>POT status</td>\r\n";
      }
      $module .=
//    "<td><a href=\"?unique=".$pot["status"][1]."&pot=".$tc_data["pot"]["unique"]."\">".$pot_status[$pot["status"][0]]."</a></td>\r\n".
      "<td>".$pot_status[$pot["status"][0]]."</td>\r\n";


      if(!$inspection_pot == false) {
         $module .= "<td><img src=\"../../../../library/images/16x16/edition-29.png\" style=\"position:relative;left:5px; vertical-align:middle;\">&nbsp;</td><td>Inspection only</td>\r\n";
      } else {
         $module .=       "<td>Edit Parts/ Tools</td><td><a href=\"javascript:void(0);\" onclick=\"m_e3__ee_edit_pot.embed('".$tc_data["pot"]["ref_case"]."');\"><img src=\"".$_SESSION["php_sever"]."/library/images/16x16/setting-5.png\" style=\"broder:none;margin-left:3px;\" /></a></td>\r\n";
      }

      $module .=
      "</tr>\r\n".
      

      "<tr>\r\n".
      "<td>ID<img src=\"../../../../library/images/16x16/edition-43.png\" onclick=\"m_t3__tracking_case.delete(false, '".$inspection_pot."');\" style=\"vertical-align:top; position:relative; top:2px; left:69px; cursor:pointer;\" /></td>\r\n".
      "<td style=\"white-space:nowrap; overflow:hidden;\">".str_replace("+", "", $tc_data["meta"]["id"])."</td>\r\n".
      "<td>Project case</td>\r\n";
      
      if($tc_data["meta"]["disno"] == "no Project case found") {
         $module .= "<td>".$tc_data["meta"]["disno"]."</td>\r\n";
      } else {
         $query_string = (strtoupper(substr($tc_data["meta"]["disno"], 0, 3)) == "DIS") ? "?open=dis" : "";
         $module .= "<td><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/p.dis?open=".$tc_data["meta"]["disno"]."\" target=\"_blank\">".$tc_data["meta"]["disno"]."</a></td>\r\n";
      }

      $module .=
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>Type</td>\r\n".
      "<td>".$dsp_type."</td>\r\n".
      "<td>CER number</td>\r\n";
      if($tc_data["meta"]["cernumber"] == "no Project case found") {
         $module .= "<td>".$tc_data["meta"]["cernumber"]."</td>\r\n";
      } else {
         $module .= "<td><a href=\"".$_SESSION["remote_domino_path_cer"]."/ControllerAllCER/".$tc_data["meta"]["cernumber"]."\" target=\"_blank\">".$tc_data["meta"]["cernumber"]."</a></td>\r\n";
      }
      $module .=
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>Rule</td>\r\n".
      "<td>".$dsp_rule."</td>\r\n".
      "<td><nobr>UQS Number";
      if($inspection_pot == false) $module .= "<img src=\"../../../../library/images/16x16/interface-78.png\" onclick=\"uqs.connect('".$tc_data["uqs"]["pot_unid"]."');\" style=\"vertical-align:top; position:relative; top:2px; left:05px; cursor:pointer;\" /></td>\r\n";
      $module .=
      "<td>".$uqs["number"]."&nbsp;</td>\r\n".
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>iSource No.</td>\r\n".
      "<td>".
      embed_input(array(), $tc_data["meta"]["isource"], array("check" => "isource", "id" => generate_uniqueid(8), "type" => "text", "name" => "isource", "style" => "width:236px", "onblur" => "post.save_field(session.remote_domino_path_main, '".$tc_data["meta"]["unid"]."', this.name, this.value);")).
      "</td>\r\n".
      "<td>UQS name</td>\r\n".
      "<td>".$uqs["name"]."</td>\r\n".
      "</tr>\r\n".
      "</table>";
   }
   return str_replace("Array","",$module);

}

?>