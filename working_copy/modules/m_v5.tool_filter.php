<?php



function m_v5_tool_filter($_application) {

   global $tc_data;

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Überschrift und Modulsonderzubehör erstellen
   $tabs="<img src=\"../../../../library/images/16x16/edition-13.png\" style=\"border:none;position:relative;top:2px;filter:Gray();cursor:pointer;\" />&nbsp;&nbsp;<span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Embedded Tools headline');\">Embedded Tools</span>";

   $content = "<table>";
   foreach($tc_data["tracker"]["pot_dsp_name"] as $val) {
      $tmp = trim(substr($val, strpos($val, " / ") + 5,strlen($val)));  
      if(!in_array($tmp, $tool)) $tool[] = $tmp;
   }
   sort($tool);
   foreach($tool as $val) $content .= "<tr><td><img src=\"../../../../library/images/16x16/vote-2.png\" style=\"margin-right:7px;\" /></td><td><a style=\"font:normal 13px century gothic;text-decoration:none;color:rgb(0,102,158);\" href=\"javascript:do_select_dsp_pot(false);select_dsp_pot('".$val."','tool_base');save_select_dsp_pot('".$tc_data["tracker"]["unid"]."');\">".$val."</a></td></tr>";
   $content .= "</table>";

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:220px;table-layout:fixed;margin-bottom:22px;margin-top:22px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_content\" colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   return $module;

}


?>