<?php


function m_t1__process_indicator($_application) {

  // return array("", "");

   $load = array("tracker", "project", "pot", "tool", "meta");
   require_once("addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   $_SESSION["tool_base"] = substr($tc_data["tool"]["number"], 0, 2);

   if(trim($_SESSION["tool_base"]) == "") $_SESSION["tool_base"] = substr($tc_data["pot"]["tool"]["number"], 0, 2);
   $_SESSION["meta"]["unid"] = $tc_data["meta"]["unid"];
   $_SESSION["process_type"] = $tc_data["meta"]["process_type"];

   $_SESSION["pot:unid"] = $tc_data["pot"]["unid"];
   $_SESSION["project_number"] = $tc_data["project"]["number"];

   $_SESSION["pot"] = $_REQUEST["pot"];
   $_SESSION["tracker"] = $_REQUEST["unique"];

   $_SESSION["request:unique"] = $_REQUEST["unique"];
   $_SESSION["request:pot"] = $_REQUEST["pot"];

   $_SESSION["PATH_TDT"] = str_replace(array($_application["page_id"].".php", "\\"), array("", "/"), $_SERVER["PATH_TRANSLATED"]);

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]).
   "<span form=\"".__FUNCTION__."\" style=\"display:none;\">".json_encode($tc_data["meta"])."</span>";

   if(strtolower($tc_data["tracker"]["archived"]) != "no") {
      $headline .= "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;Tracker is archived";
   }


   if($tc_data["tool"]["dualsource"] == "1" && !$_application["grid"]["enabled"]) {
      if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_dualsource_filter"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_dualsource_filter"] = "1,2";

      $e = explode("," ,$_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_dualsource_filter"]);
      $img[0] = (in_array("1", $e)) ? "status-5" : "status-5-1";
      $img[1] = (in_array("2", $e)) ? "status-5" : "status-5-1";

       $headline .=
      "<span><span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n".
      "<img onclick=\"m_t1__process_indicator.dualsource.filter(this);\" src=\"../../../../library/images/16x16/white/".$img[0].".png\" style=\"cursor:pointer; margin:0px 5px 0px 0px; width:12px; height:12px; position:relative; top:1px;\">First source\r\n".
      "<img onclick=\"m_t1__process_indicator.dualsource.filter(this);\" src=\"../../../../library/images/16x16/white/".$img[1].".png\" style=\"cursor:pointer; margin:0px 5px 0px 5px; width:12px; height:12px; position:relative; top:1px;\">Second source\r\n".
      "</span>";

   }

   if(!$_application["grid"]["enabled"] && !strpos($tc_data["meta"]["process_type"], "v->2")) {
      if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_loop_filter"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_loop_filter"] = "1";
      $e = explode("," ,$_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_loop_filter"]);

      $img[2] = (in_array("1", $e)) ? "status-11" : "status-11-1";
      $img[3] = (in_array("2", $e)) ? "status-11" : "status-11-1";

      $headline .=
      "<span><span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n".
      "<img onclick=\"m_t1__process_indicator.loop.filter(this);\" src=\"../../../../library/images/16x16/white/".$img[2].".png\" style=\"cursor:pointer; margin:0px 5px 0px 0px; width:12px; height:12px; position:relative; top:1px;\">Current loop\r\n".
      "<img onclick=\"m_t1__process_indicator.loop.filter(this);\" src=\"../../../../library/images/16x16/white/".$img[3].".png\" style=\"cursor:pointer; margin:0px 5px 0px 5px; width:12px; height:12px; position:relative; top:1px;\">All loops\r\n".
      "</span>";
   }

   if($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_tooltipp"] == "") $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_tooltipp"] = "1";
   $save = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_tooltipp"] == "1") ? "0" : "1";
   $img[4] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_tooltipp"] == "1") ? "status-5" : "status-5-1";
   $headline .= 
   "<span><span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n".
   "<img onclick=\"document.perpage.m_t1__process_indicator_tooltipp.value='".$save."'; save_perpage('', true);\" src=\"../../../../library/images/16x16/white/".$img[4].".png\" style=\"cursor:pointer; margin:0px 5px 0px 0px; width:12px; height:12px; position:relative; top:1px;\">Tooltipps\r\n".
   "</span>";

   // Module Body --------------------------------------------------------------------------------------------------

//   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";

   $module = m_t1__process_indicator_generate_html($_application, $tc_data);

   return array($headline, $module);
}



function m_t1__process_indicator_generate_html($_application, $tc_data) {

   //$_REQUEST["unique"] = $_SESSION["request:unique"];
   //$_REQUEST["pot"] = $_SESSION["request:pot"];


   // GET SUBSTI
   $substi = file_get_authentificated_contents($_SESSION["remote_domino_path_substi"]."/v.is_substi?open&restricttocategory=".rawurlencode($_SESSION["domino_user_cn"])."&count=9999&function=xml:data");
   if(!strpos($substi, "No documents found")) {
      $substi = process_xml_from_string($substi);
   }


   $e_dualsource = explode("," ,$_SESSION["perpage"]["tab"][$_application["page_id"]]["m_t1__process_indicator_dualsource_filter"]);
   $e_loop = $_SESSION["perpage"]["tab"][$_application["page_id"]]["m_t1__process_indicator_loop_filter"];

   // IS INSPECTION POT ? ---------------------------------------------------------------------------------------------
   $pot_status = explode("@", $tc_data["pot"]["status"]); 


   $inspection_pot = ($pot_status[1] != $_REQUEST["unique"]) ? $pot_status[1] : false;
   if(!$inspection_pot == false) {
      $tc_data["meta"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:meta/".$_SESSION["pot"]."@".$inspection_pot."?open");
   }

   $ptype = ($tc_data["pot"]["meta"]["process_type"] != "") ? $tc_data["pot"]["meta"]["process_type"] : $tc_data["meta"]["process_type"];

   $plist = $_application["process"]["type"][$ptype];


   if($ptype == "") return "Meta unavailable";

   $is_tpm = (strpos($_SESSION["domino_user"], $tc_data["tracker"]["responsible"])) ? true : false; 
   $is_v2 = (strpos($ptype, "v->2")) ? true : false;
   $_SESSION["is_v2"] = $is_v2;

   // START: POT-Termine laden -----------------------------------------------------------------------------------------
   $pot = $_REQUEST["pot"];

   if($pot != "") {
      $pot_date[$pot] = tracker_get_pot_dates($pot, $inspection_pot, $tc_data);
   }



   // END: POT-Termine laden -------------------------------------------------------------------------------------------
   // TRACKER NICHT AKTIVIERT


   if(trim(implode($pot_date[$pot])) == "") {


      if($_REQUEST["unique"] != "" && $_REQUEST["pot"] != "") {

         if(!$inspection_pot && strtolower($tc_data["tracker"]["responsible"]) == strtolower($_SESSION["domino_user_cn"])) {

            $module = "<div id=\"process_indicator\">\r\n".
            "<span class=\"phpbutton\">\r\n".
            "<a href=\"javascript:include_activate_pot('".$_SESSION["pot"]."');\">Activate Tracking case</a>\r\n".
            "</span>\r\n".
            "</div>\r\n";
         }
         else {
            if(strtolower($tc_data["tracker"]["responsible"]) == strtolower($_SESSION["domino_user_cn"])) $module .= "POT already in use. You may <span class=\"f1\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById('header:add_inspection_pot').click();\">add</a></span> an inspection POT.";
         }
      }
      else $module = "Select POT";
   }
   else {
      $th = array("Subprocess", "Responsible", "Planned", "Actual", "&Delta;");
      if(!$is_v2) {
         $th[1] = "Planned";
         $th[2] = "Revised";
      }


      $module = "<div id=\"process_indicator\">\r\n".
      "<div style=\"width:100%;\">\r\n";
      if(!$inspection_pot == false) {
         $module .= "<div style=\"position:relative; left:-5px; margin-bottom:12px; padding:2px;\"><img src=\"../../../../library/images/16x16/edition-29.png\" style=\"position:relative;left:-5px; vertical-align:top;\">Inspection only</div>\r\n";
         $dsp_inspection = " style=\"display:none;\"";
      }
      else {
         $dsp_inspection = "";
      }
      $activate_next = "";
      $module .=
      "<input type=\"text\" name=\"search_string\" value=\"\" onkeyup=\"do_search_process_indicator(this.value.toLowerCase(), this.parentNode.parentNode.id);\" />\r\n".
      "<input type=\"hidden\" name=\"search_type\" value=\"0\" />\r\n".
      "<img name=\"img_process_indicator\" onclick=\"m_t1__process_indicator.change_search(this);\" src=\"".$_SESSION["php_server"]."/library/images/16x16/status-11.png\" style=\"margin-top:1px;\">Include\r\n".
      "<img name=\"img_process_indicator\" onclick=\"m_t1__process_indicator.change_search(this);\" src=\"".$_SESSION["php_server"]."/library/images/16x16/status-11-1.png\" style=\"margin-top:1px;\">Exclude\r\n".
      "%%ACTIVATE_NEXT%%". 
      "</div>\r\n".
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"tbl_m_t1__process_indicator\">\r\n".
      "<tr onclick=\"void(0);\">\r\n".
      "<th".$dsp_inspection.">&nbsp;</th>\r\n".
      "<th>".$th[0]."</th>\r\n".
      "<th><img src=\"../../../../library/images/blank.gif\" style=\"width:16px; height:16px;\">".$th[1]."</th>\r\n".
      "<th>".$th[2]."</th>\r\n".
      "<th>".$th[3]."</th>\r\n".
      "<th>&Delta;</th>\r\n".
      "</tr>\r\n";

      $c_gate = 0;


//      if(!is_array($substi["substi"][0])) $substi["substi"] = array($substi["substi"]);
      $usenav = true;

      foreach($pot_date[$pot] as $key => $gate) {

         if($tc_data["pot"]["status"] == "200") $usenav = false;
        
         $module .= "<div class=\"date_process\" unid=\"".$gate[$loop]["@attributes"]["unid"]."\">\r\n";

         if(in_array($key, $plist)) {
            $pk = 0; foreach($plist as $pk => $pv) if($pv == $key) break;
            $c_gate++;

//            $loop = count($gate); while($loop > 0) {
//            $loop--;

            $loop = -1; while(($loop + 1) < count($gate)) {
            $loop++;

            $current_loop[1] = ((intval($gate[0]["@attributes"]["loop_s1"]) - 1) == $loop) ? true : false;
            $current_loop[2] = ((intval($gate[0]["@attributes"]["loop_s2"]) - 1) == $loop) ? true : false;

            $date["actual"] = $gate[$loop]["S1"]["actual_value"];
            if(!$is_v2) {
               $date["planned"] = $gate[$loop]["S1"]["planned_value"];
               $date["revised"] = $gate[$loop]["S1"]["revised_value"];
               $date["revised:next"] = $pot_date[$pot][$plist[$pk + 1]][0]["S1"]["revised_value"];
            }
            else {
               $date["revised"] = (implode($gate[$loop]["S1"]["revised_value"]) == "") ? $gate[$loop]["S1"]["planned_value"] : $gate[$loop]["S1"]["revised_value"];
               $date["revised:next"] = (implode($pot_date[$pot][$plist[$pk + 1]][0]["S1"]["revised_value"]) == "") ? $pot_date[$pot][$plist[$pk + 1]][0]["S1"]["planned_value"] : $pot_date[$pot][$plist[$pk + 1]][0]["S1"]["revised_value"];
            }
            $date["revision"] = (is_array($gate[$loop]["S1"]["revised_value"])) ? "R0" : str_replace("P", "R0", $gate[$loop]["S1"]["revision"]);
            $date["plmc"] = (substr($key, -3) == "990") ? "_plmc" : "";
            $date["text"] = substr($_application["process"]["subprocess"][$key], 0, strpos($_application["process"]["subprocess"][$key], ":"));
            if(count($gate) > 1) $date["text"] .= "&nbsp;Loop&nbsp".$loop;

            $date["has_loop"] = substr($_application["process"]["subprocess"][$key], strpos($_application["process"]["subprocess"][$key], ":") + 1, strlen($_application["process"]["subprocess"][$key]));
            $date["has_loop"] = ($date["has_loop"]  == "0") ? false : explode("/", $date["has_loop"]);

            $date["rc"] = (is_array($_application["process"]["reason_codes"][$key])) ? "" : rawurlencode($_application["process"]["reason_codes"][$key]); 
            $indicator = date_get_indicator($date, $pot_date[$pot][$plist[$c_gate]][0][$source], $is_v2);
            $date["delta"] = "";


            // BUTTON NEXT

           if(strtolower("date_actual_".$key) == $_application["process"]["active_next"][$tc_data["meta"]["process_type"]]) {
               if(!is_array($date["actual"])) {
                  $activate_next = 
                  "<span id=\"activate_next_process\" chain_pt=\"".$tc_data["meta"]["chain_process_type"]."\" chain_pr=\"".$tc_data["meta"]["chain_process_rules"]."\" activate_next=\"".$_application["process"]["active_next"][$tc_data["meta"]["process_type"]]."\" activate_date=\"".$_application["process"]["active_date"][$tc_data["meta"]["process_type"]]."\" class=\"phpbutton\" style=\"position:absolute; top:32px; left:451px;\">".
                  "<a href=\"javascript:void(0);\" onclick=\"include_activate_pot();\">".$_application["process"]["active_button_text"][$tc_data["meta"]["process_type"]]."</a>".
                  "</span>\r\n";
                  $_SESSION["process_type"] = $tc_data["meta"]["process_type"];
               }
               else unset($_SESSION["process_type"]);
            }


            if(!is_array($date["revised"])  && !is_array($date["actual"])) {
               if(strtotime($date["revised"]) > 0 && strtotime($date["actual"]) > 0) {
                  $v = (strtotime($date["revised"]) - strtotime($date["actual"])) / 86400;
                  $v = calc_delta($date);
                  if($v > 0) $v = "<span style=\"color:rgb(0,155,0);\">".$v."</span>\r\n";
                  if($v < 0) $v = "<span style=\"color:rgb(155,0,0);\">".($v * -1)."</span>\r\n";
                  $date["delta"] = $v;
               }
            }


            // START: DUAL SOURCE DATES ---------------------------------------------------------------------------------+
            if($_application["process"]["dualsource"][$key] == "1" && $tc_data["tool"]["dualsource"] == "1" && in_array("2", $e_dualsource)) {
               $date["text_s2"] = $date["text"]." [2nd source]";
               $date["actual_s2"] = $gate[$loop]["S2"]["actual_value"];
               if(!$is_v2) {
                  $date["planned_s2"] = $gate[$loop]["S1"]["planned_value"];
                  $date["revised_s2"] = $gate[$loop]["S2"]["revised_value"];
                  $date["revised_s2:next"] = $pot_date[$pot][$plist[$pk + 1]][0]["S2"]["revised_value"];
               }
               else {
                  $date["revised_s2"] = (implode($gate[$loop]["S2"]["revised_value"]) == "") ? $gate[$loop]["S2"]["planned_value"] : $gate[$loop]["S2"]["revised_value"];
                  $date["revised_s2:next"] = (implode($pot_date[$pot][$plist[$pk + 1]][0]["S2"]["revised_value"]) == "") ? $pot_date[$pot][$plist[$pk + 1]][0]["S2"]["planned_value"] : $pot_date[$pot][$plist[$pk + 1]][0]["S2"]["revised_value"];
               }
               $date["revision_s2"] = (is_array($gate[$loop]["S2"]["revised_value"])) ? "R0" : str_replace("P", "R0", $gate[$loop]["S2"]["revision"]);
               $date["plmc_S2"] = (substr($key, -3) == "990") ? "_plmc" : "";
               $date["rc_s2"] = (is_array($_application["process"]["reason_codes"][$key])) ? "" : rawurlencode($_application["process"]["reason_codes"][$key]); 
            
            }
            // END: DUAL SOURCE DATES ----------------------------------------------------------------------------

            $isactive = array("true", "true");
            if($_application["process"]["deactivatable"][$key] == "yes") $isactive = ($gate[$loop]["wf_is_active"] == "true") ? array("Deactivate", "false") : array("Activate", "true");

            // MENU V2 PROCESS
            if($is_v2) {  
               $status = $gate[$loop]["@attributes"]["process_status"]; if($status == "") $status = "60";
               if($_application["process"]["deactivatable"][$key] == "yes" && $isactive[1] == "true") $status = "99";
               $date["menu"] = 
               "<menu class=\"menuwrapper\">\r\n".
               "<menu>\r\n".
                "<level1><a name=\"editable_date\" type=\"".str_replace("_", "", $date["plmc"])."\" descr=\"".str_replace("G0", "G", $key)." ".$date["text"]."\" revision=\"".rawurlencode($date["revision"])."\" value_revised=\"".$date["revised"]."\" value_actual=\"".$date["actual"]."\">".
                "<div class=\"indicator".$status."\"></div></a>\r\n";

                if($indicator[0] != 1  && !strpos($key, "99") && strtolower($tc_data["tracker"][archived]) == "no") {
                   if(!$is_v2) {
                      $click["actual_date"] = "calendar_big_pick_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_actual', '".rawurlencode("Set actual date for ".$date["text"])."');";
                   }
                   else {
                      $set_status = (isset($_application["process"]["status:".$key][$ptype])) ? $_application["process"]["status:".$key][$ptype] : "";
                      $click["actual_date"] = "m_t1__process_indicator.set_status = '".$set_status."'; m_t1__process_indicator.set_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_actual', true, '".$key."', '".$status."', '".$date["text"]."')";
                      $click["actual_date_superuser"] = "m_t1__process_indicator.set_status = '".$set_status."'; m_t1__process_indicator.set_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_actual', true, '".$key."', '99', '".$date["text"]."')";

                   }
                   if($usenav) {
                      $date["menu"] .= "<menu>\r\n";
                      $date["menu"] .=  "<level2 style=\"border-top:solid 1px rgba(222,222,232,0.98); border-bottom:solid 1px rgba(222,222,232,0.98); background:rgba(255,255,255, 0.98); color:rgba(0,0,0,0.98); height:20px; cursor:default; position:relative; left:-22px; width:330px;\"><img src=\"../../../../library/images/navigate_b.gif\" style=\"position:relative; left:-6px; top:1px;\"><span style=\"position:relative; left:8px; top:1px;\"><strong>".$date["text"]."</strong></span></level2>\r\n";


                      if(!$is_v2 && $isactive[1] != "true") $date["menu"] .=  "<level2><a onclick=\"calendar_big_pick_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_revised', '".rawurlencode("Set revised date for ".$date["text"])."', '".$date["rc"]."', '[".str_replace("G0", "G", $key)."] ".$date["text"]."');\">Set new revised date</a></level2>\r\n";
                      if($isactive[0] != "true" && $status != "40") $date["menu"] .=  "<level2><a href=\"javascript:void(0);\" onclick=\"m_t1__process_indicator.activate('".$gate[$loop]["@attributes"]["unid"]."', ".$isactive[1].");\">".$isactive[0]."</a></level2>\r\n";
                      if(($_application["process"]["deactivatable"][$key] != "yes" || $isactive[1] != "true") && $_SESSION["domino_user_cn"] == rawurldecode($gate[$loop]["@attributes"]["responsible"]) && in_array($status, array("20", "30"))) $date["menu"] .= "<level2><a href=\"javascript:".$click["actual_date"]."\">Set actual date</a></level2>\r\n";


                      // Superuser
                      if(($_application["process"]["deactivatable"][$key] != "yes" || $isactive[1] != "true") && in_array("[superuser]", $_SESSION["remote_userroles"]) && in_array($status, array("20", "30"))) $date["menu"] .= "<level2><a do=\"".$click["actual_date_superuser"]."\" href=\"javascript:void(0);\" onclick=\"m_t1__process_indicator.planned_date = '".str_replace("-", "", $date["revised"])."'; m_t1__process_indicator.container = this; calendar_big_pick_date('', 'superuser:date_actual', '".rawurlencode("Set actual date for ".$date["text"])."', '', '');\">Select actual date (superuser)</a></level2>\r\n";

                      // Substitution
                      if(in_array(rawurldecode($gate[$loop]["@attributes"]["responsible"]), $substi["owner"]) && ($_application["process"]["deactivatable"][$key] != "yes" || $isactive[1] != "true") && in_array($status, array("20", "30"))) $date["menu"] .= "<level2><a href=\"javascript:".$click["actual_date"]."\">finish this sub process on behalf of ".rawurldecode($gate[$loop]["@attributes"]["responsible"])."</a></level2>\r\n";

                      if(1 ==2) $date["menu"] .= "<level2><a href=\"#\">Add new loop</a></level2>\r\n";
                      if(($_application["process"]["deactivatable"][$key] != "yes" || $isactive[1] != "true") && $is_tpm && is_string($gate[$loop]["@attributes"]["responsible"]) && in_array($status, array("10", "20", "30"))) $date["menu"] .= "<level2><a href=\"javascript:void(0);\" onclick=\"m_t1__process_indicator.change_responsible(this, '".$key." ".$tc_data["meta"]["process_type"]."');\" gate=\"".$date["text"]."\" responsible=\"".rawurldecode($gate[$loop]["@attributes"]["responsible"])."\">Change responsible</a></level2>\r\n";
                      $date["menu"] .= "</menu>\r\n";
                   }
             }
             $date["menu"] .= 
             "</level1>\r\n".
             "</menu>\r\n".
             "</menu>\r\n";
          }

          else {
             $now = mktime();
             // STANDARD ----------------------------------------------------------------------------------------------------------
             $class = "process_white";
             $usenav = true;
             $status = 0;
             // REVISED DATE WENIGER ALS 2 WOCHEN IN DER ZUKUNFT ------------------------------------------------------------------
             if(strtotime($date["revised"]) > 0 &&  strtotime($date["revised"]) - $now - (86400 * 13) <= 0) $class= "process_yellow";
             // REVISED DATE IN DER VERGANGENHEIT ---------------------------------------------------------------------------------		
             if(strtotime($date["revised"]) > 0  && strtotime($date["revised"]) - $now < -86400) $class = "process_red";
             // R Reihenfolge nicht auf dem Zeitstrahl ----------------------------------------------------------------------------								
             if(strtotime($date["revised:next"]) > 0  && (strtotime($date["revised:next"]) - strtotime($date["revised"])) < 0) $class = "process_orange";				
             // ACTUAL DATE GESETZT -----------------------------------------------------------------------------------------------
             if(strtotime($date["actual"]) > 0) {
                $class = "process_green";
                $status = 40;
             }	

             if(strtolower($tc_data["tracker"][archived]) != "no") $usenav = false;

             $write_access = explode(":", $_application["process"]["write_planned"][$key]);
             $write_access  = array_merge($write_access, explode(":", $_application["process"]["write_revised"][$key]));
             $write_access  = array_merge($write_access, explode(":", $_application["process"]["write_actual"][$key]));
             $write_access = array_unique($write_access);
             $role_access = false;
             foreach($write_access as $access) {
                if(in_array($access, $_SESSION["remote_userroles"]) && $access != "") $role_access = true;
             }


             if(strpos($key, "990") || 
                (!in_array("[superuser]", $_SESSION["remote_userroles"]) && 
                !in_array("[assistant]", $_SESSION["remote_userroles"]) && 
                strtolower($tc_data["tracker"]["responsible"]) != strtolower($_SESSION["domino_user_cn"]) &&
                !$role_access)) {$usenav = false;}	
             if(!$use_nav) $use_nav = false;

             if(in_array("[superuser]", $_SESSION["remote_userroles"])) $role_access = false;
             if(in_array("[assistant]", $_SESSION["remote_userroles"])) $role_access = false;
             if(strtolower($tc_data["tracker"]["responsible"]) == strtolower($_SESSION["domino_user_cn"])) $role_access = false;


             if($role_access) {
                $write_access = explode(":", $_application["process"]["write_planned"][$key]);
                $write_planned = false;
                foreach($write_access as $access) if(in_array($access, $_SESSION["remote_userroles"]) && $access != "") $write_planned = true;
                $write_access = explode(":", $_application["process"]["write_revised"][$key]);
                $write_revised = false;
                foreach($write_access as $access) if(in_array($access, $_SESSION["remote_userroles"]) && $access != "") $write_revised = true;
                $write_access = explode(":", $_application["process"]["write_actual"][$key]);
                $write_actual = false;
                foreach($write_access as $access) if(in_array($access, $_SESSION["remote_userroles"]) && $access != "") $write_actual = true;
             }


             $date["menu"] = 
             "<menu class=\"menuwrapper\">\r\n".
             "<menu>\r\n".
             "<level1><a name=\"editable_date\" type=\"".str_replace("_", "", $date["plmc"])."\" descr=\"".str_replace("G0", "G", $key)." ".$date["text"]."\" revision=\"".rawurlencode($date["revision"])."\" value_revised=\"".$date["revised"]."\" value_actual=\"".$date["actual"]."\">".
             "<div class=\"".$class."\"></div></a>\r\n";
             if($usenav) {
                $date["menu"] .= "<menu>\r\n";
                $date["menu"] .=  "<level2 style=\"border-top:solid 1px rgba(222,222,232,0.98); border-bottom:solid 1px rgba(222,222,232,0.98); background:rgba(255,255,255, 0.98); color:rgba(0,0,0,0.98); height:20px; cursor:default; position:relative; left:-22px; width:330px;\"><img src=\"../../../../library/images/navigate_b.gif\" style=\"position:relative; left:-6px; top:1px;\"><span style=\"position:relative; left:8px; top:1px;\"><strong>".$date["text"]."</strong></span></level2>\r\n";
                 // SET REVISED DATE -----------------------------------------------------------------------------------------------
                if($status != "40") if(!$role_access || $write_revised) $date["menu"] .=  "<level2><a onclick=\"pick_date.execute='set_v1_date'; calendar_big_pick_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_revised', '".rawurlencode("Set revised date for ".$date["text"])."', '".$date["rc"]."', '[".str_replace("G0", "G", $key)."] ".$date["text"]."');\">Set new revised date</a></level2>\r\n";
                // SET REVISED DATE WITH RULE -------------------------------------------------------------------------------------
                if($status != "40") if($tc_data["tracker"]["responsible"] == $_SESSION["domino_user_cn"]) {    
                   if($loop == (count($gate) - 1) && !strpos($date["text"], "2nd")) $date["menu"] .=  "<level2><a onclick=\"pick_date.loop='".$loop."'; pick_date.execute='set_v1_date_rule'; calendar_big_pick_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_revised', '".rawurlencode("Set revised date for ".$date["text"])." and use rule', '".$date["rc"]."', '[".str_replace("G0", "G", $key)."] ".$date["text"]."');\">Set revised date and use rule</a></level2>\r\n";
                }

                // SET ACTUAL DATE ------------------------------------------------------------------------------------------------
                if($status != "40") if(!$role_access || $write_actual) $date["menu"] .=  "<level2><a onclick=\"pick_date.execute='set_v1_date'; calendar_big_pick_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_actual', '".rawurlencode("Set actual date for ".$date["text"])."', '".$date["rc"]."', '[".str_replace("G0", "G", $key)."] ".$date["text"]."');\">Set actual date</a></level2>\r\n";
                // CREATE NEW LOOP ------------------------------------------------------------------------------------------------
                if(in_array($key, $date["has_loop"]) && $current_loop[1]) {
                   if(!$role_access) $date["menu"] .=  "<level2><a onclick=\"create_loop.init('".implode($date["has_loop"], "/")."', '".$_SESSION["tracker"]."', '".$_SESSION["pot"]."', 0, ".($gate[0]["@attributes"]["loop_s1"] + 1).", 1);\">Create new loop</a></level2>\r\n";
                   if(intval($loop) > 0) $date["menu"] .=  "<level2><a onclick=\"create_loop.delete('".implode($date["has_loop"], "/")."', '".$_SESSION["tracker"]."', '".$_SESSION["pot"]."', 0, ".($gate[0]["@attributes"]["loop_s1"] + 1).", 1);\">Delete loop</a></level2>\r\n";
                }
                $date["menu"] .= "</menu>\r\n";
             }

             $date["menu"] .= 
             "</level1>\r\n".
             "</menu>\r\n".
             "</menu>\r\n";
             // START DUAL SOURCE NAV --------------------------------------------------------------
             if($_application["process"]["dualsource"][$key] == "1" && $tc_data["tool"]["dualsource"] == "1" && in_array("2", $e_dualsource)) {
                $class = "process_white";
                $status = 0;
                // REVISED DATE WENIGER ALS 2 WOCHEN IN DER ZUKUNFT ------------------------------------------------------------------
                if(strtotime($date["revised_s2"]) > 0 &&  strtotime($date["revised_s2"]) - $now - (86400 * 13) <= 0) $class= "process_yellow";
                // REVISED DATE IN DER VERGANGENHEIT ---------------------------------------------------------------------------------		
                if(strtotime($date["revised_s2"]) > 0  && strtotime($date["revised_s2"]) - $now < -86400) $class = "process_red";
                // R Reihenfolge nicht auf dem Zeitstrahl ----------------------------------------------------------------------------								
                if(strtotime($date["revised_S2:next"]) > 0  && (strtotime($date["revised_s2:next"]) - strtotime($date["revised_s2"])) < 0) $class = "process_orange";				
                // ACTUAL DATE GESETZT -----------------------------------------------------------------------------------------------
                if(strtotime($date["actual_s2"]) > 0) {
                   $class = "process_green";
                   $status = 40;
                }	
                $date["menu_s2"] = 
                "<menu class=\"menuwrapper\">\r\n".
                "<menu>\r\n".
                "<level1><a name=\"editable_date\" type=\"".str_replace("_", "", $date["plmc"])."\" descr=\"".str_replace("G0", "G", $key)." ".$date["text"]."\" revision=\"".rawurlencode($date["revision"])."\" value_revised=\"".$date["revised"]."\" value_actual=\"".$date["actual"]."\">".
                "<div class=\"".$class."\"></div></a>\r\n";

                if($usenav) {
                   $date["menu_s2"] .= "<menu>\r\n";
                   if($status != "40" || (in_array($key, $date["has_loop"]) && $status == "40")) $date["menu_s2"] .=  "<level2 style=\"border-top:solid 1px rgba(222,222,232,0.98); border-bottom:solid 1px rgba(222,222,232,0.98); background:rgba(255,255,255, 0.98); color:rgba(0,0,0,0.98); height:20px; cursor:default; position:relative; left:-22px; width:330px;\"><img src=\"../../../../library/images/navigate_b.gif\" style=\"position:relative; left:-6px; top:1px;\"><span style=\"position:relative; left:8px; top:1px;\"><strong>".$date["text"]." 2nd source</strong></span></level2>\r\n";
                    // SET REVISED DATE ----------------------------------------------------------------------------------------------
                   if($status != "40") $date["menu_s2"] .=  "<level2><a onclick=\"pick_date.execute='set_v1_date'; calendar_big_pick_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_revised_s2', '".rawurlencode("Set revised date for ".$date["text"]." 2nd source")."', '".$date["rc"]."', '[".str_replace("G0", "G", $key)."] ".$date["text"]."');\">Set new revised date</a></level2>\r\n";
                   // SET ACTUAL DATE ------------------------------------------------------------------------------------------------
                   if($status != "40") $date["menu_s2"] .=  "<level2><a onclick=\"pick_date.execute='set_v1_date'; calendar_big_pick_date('".$gate[$loop]["@attributes"]["unid"]."', 'date_actual_s2', '".rawurlencode("Set actual date for ".$date["text"]." 2nd source")."', '".$date["rc"]."', '[".str_replace("G0", "G", $key)."] ".$date["text"]."');\">Set actual date</a></level2>\r\n";
                  // CREATE NEW LOOP ------------------------------------------------------------------------------------------------
                  if(in_array($key, $date["has_loop"]) && $current_loop[2]) {
                     $date["menu_s2"] .=  "<level2><a onclick=\"create_loop.init('".implode($date["has_loop"], "/")."', '".$_SESSION["tracker"]."', '".$_SESSION["pot"]."', 0, ".($gate[0]["@attributes"]["loop_s2"] + 1).", 2);\">Create new loop</a></level2>\r\n";
                      if(intval($loop) > 0) $date["menu_s2"] .=  "<level2><a onclick=\"create_loop.delete('".implode($date["has_loop"], "/")."', '".$_SESSION["tracker"]."', '".$_SESSION["pot"]."', 0, ".($gate[0]["@attributes"]["loop_s2"] + 1).", 2);\">Delete loop</a></level2>\r\n";
                  }
                  $date["menu_s2"] .= "</menu>\r\n";

                }

                $date["menu_s2"] .= 
                "</level1>\r\n".
                "</menu>\r\n".
                "</menu>\r\n";
             }
             // END: DUAL SOURCE NAV --------------------------------------------------------------


          }
          $protocol["revision"] = (is_array($date["revised"])) ? "&nbsp;" : "<a href=\"javascript:void(0);\" onclick=\"embed_history('date_revised', '".$gate[$loop]["@attributes"]["unid"]."');\">".$date["revision"]."</a>";
          $protocol["actual"] = (is_array($date["actual"])) ? "&nbsp;" : "<a href=\"javascript:void(0);\" onclick=\"embed_history('date_actual', '".$gate[$loop]["@attributes"]["unid"]."');\">A</a>";

          $class = (substr($key, -3) == "990") ? "plmc" : "reg";
          $dsp_status = array("10" => "Projected", "20" => "Ongoing", "30" => "Overdue", "40" => "Completed");
          unset($k);
          $k[] = $dsp_status[$status];
          $k[] = $key;
          if(strpos($key, "99")) $k[] = "plmc";
          $k[] = $date["text"];
          $k[] = rawurldecode($gate[$loop]["@attributes"]["responsible"]);
          $k[] = $date["revised"];
          $k[] = $date["actual"];
          $k[] = strip_tags($date["delta"]);



          $substi_img = "<img src=\"../../../../library/images/blank.gif\" style=\"width:16px; height:16px; cursor:default;\">";
          if(intval($status) < 40) {
             $substi_img = (in_array(rawurldecode($gate[$loop]["@attributes"]["responsible"]), $substi["owner"])) ? "<img src=\"../../../../library/images/16x16orange/users-21.png\" style=\"position:relative; left:-3px; cursor:default;\">" : "<img src=\"../../../../library/images/blank.gif\" style=\"width:16px; height:16px; cursor:default;\">";
          }
          else {
             if(!strpos($key, "990") && trim($gate[0]["@attributes"]["actual_edited"]) !="" && $gate[0]["@attributes"]["actual_edited"] != $gate[0]["@attributes"]["responsible"]) $substi_img = "<img src=\"../../../../library/images/16x16/Users-21.png\" title=\"finished by ".rawurldecode($gate[0]["@attributes"]["actual_edited"])."\" style=\"position:relative; left:-3px; cursor:default;\">";
          }

          $color = ($gate[$loop]["@attributes"]["locked"] == "1") ? "rgb(189,51,50)" : "rgb(0,0,0)";

          $td[0] = $substi_img."<history field=\"responsible\" oncontextmenu=\"protocol.embed('".$gate[$loop]["@attributes"]["unid"]."', 'responsible', 'Responsible'); return false;\">".rawurldecode($gate[$loop]["@attributes"]["responsible"])."</history>";
          $td[1] = "<history field=\"date_planned\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Planned date [".strtolower($date["text"])."]")."'); return false;\" style=\"color:".$color."\">".$date["revised"]."</history>";
          $td[2] = "<history field=\"date_actual\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Actual date [".strtolower($date["text"])."]")."'); return false;\" id=\"date_actual_".strtolower($key)."\" anchor=\"".strtolower($key)."_loop".$loop."\" value=\"".$date["actual"]."\">".$date["actual"]."</history>";
          $td[3] = "<history field=\"reason_code_actual\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Reason code")."'); return false;\">".$date["delta"]."</history>";

          if(!$is_v2) {
             $td[0] = "<history field=\"date_planned\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Planned date [".strtolower($date["text"])."]")."'); return false;\">".$date["planned"]."</history>";
             $revision = ($gate[$loop]["S1"]["revision"] != "0") ? "&nbsp;[R".$gate[$loop]["S1"]["revision"]."]" : "";
             $td[1] = "<history field=\"date_revised\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Revised date [".strtolower($date["text"])."]")."'); return false;\">".$date["revised"].$revision."</history>";
          }

          $date["rule"] = (implode($date["revised"]) == "") ? $date["planned"] : $date["revised"];
          $date["from_rid"] = $_application["process"]["rules"][$tc_data["meta"]["process_rule"]][$key]["from_id"];
          $date["operation"] = $_application["process"]["rules"][$tc_data["meta"]["process_rule"]][$key]["operation"];
          $date["unid"] = $gate[$loop]["@attributes"]["unid"];

          if(!$inspection_pot == false) $date["menu"] = ""; 


          $show_firstsource = true;
          if($_application["process"]["dualsource"][$key] == "1" && $tc_data["tool"]["dualsource"] == "1" && !in_array("1", $e_dualsource)) $show_firstsource = false;
             if($show_firstsource) {
                  if(($e_loop == "2" && intval($gate[0]["@attributes"]["loop_s1"]) > $loop) || count($gate) == 1 ||($e_loop == "1" && $current_loop[1])) {

                      $remark = (rawurlencode(rawurldecode($gate[$loop]["@attributes"]["remark"])) == $gate[$loop]["@attributes"]["remark"]) ? rawurldecode($gate[$loop]["@attributes"]["remark"]) : $gate[$loop]["@attributes"]["remark"];

                     $_module[] = 
                     "<tbody unid=\"".$gate[$loop]["@attributes"]["unid"]."\" key=\"".str_replace("array", "", strtolower(implode($k, " ")))."\">\r\n".
                     "<tr>\r\n".
                     "<td".$dsp_inspection." style=\"width:10px;\">".$date["menu"]."<rule loop=\"".$loop."\" status=\"".$status."\" rid=\"".$key."\" from_rid=\"".$date["from_rid"]."\" operation=\"".$date["operation"]."\" documentunid=\"".$date["unid"]."\">".$date["rule"]."</rule></td>\r\n".
                     "<td class=\"".$class."\">".$date["text"]."<tooltipp form=\"remark\"><div style=\"margin-left:40px;\">&nbsp;Gate:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[".$key."]".
                     "<br>&nbsp;Remark:<br>".
                     "<textarea responsible=\"".$gate[$loop]["@attributes"]["responsible"]."\" unid=\"".$gate[$loop]["@attributes"]["unid"]."\">".$remark."</textarea>\r\n".
                     "</div></tooltipp></td>\r\n".
                     "<td class=\"".$class."\">".$td[0]."</td>\r\n".
                     "<td class=\"".$class."\">".$td[1]."</td>\r\n".
                     "<td class=\"".$class."\">".$td[2]."</td>\r\n".
                     "<td class=\"".$class."\">".$td[3]."</td>\r\n".
                     "</tr>\r\n".
                     "</tbody>\r\n";
                  }
               }

               if($_application["process"]["dualsource"][$key] == "1" && $tc_data["tool"]["dualsource"] == "1" && in_array("2", $e_dualsource)) {
           
                   $td[0] = "<history field=\"responsible_s2\" oncontextmenu=\"protocol.embed('".$gate[$loop]["@attributes"]["unid"]."', 'responsible_s2', 'Responsible'); return false;\">".rawurldecode($gate[$loop]["@attributes"]["responsible"])."</history>";
                   $td[1] = "<history field=\"date_planned_s2\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Planned date [".strtolower($date["text"])."]")."'); return false;\">".$date["revised"]."</history>";
                   $td[2] = "<history field=\"date_actual_s2\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Actual date [".strtolower($date["text"])."]")."'); return false;\">".$date["actual"]."</history>";
                   if(!$is_v2) {
                      $td[0] = "<history field=\"date_planned_s2\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Planned date [".strtolower($date["text_s2"])."]")."'); return false;\">".$date["planned_s2"]."</history>";
                      $revision = ($gate[$loop]["S2"]["revision"] != "0") ? " [R".$gate[$loop]["S2"]["revision"]."]" : "";
                      if(trim($revision) == "[R]") $revision = "";
                      if(trim($date["revised_s2"]) == "") $revision = "";
                      $td[1] = "<history field=\"date_revised_s2\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Revised date [".strtolower($date["text_s2"])."]")."'); return false;\">".$date["revised_s2"].$revision."</history>";
                      $td[2] = "<history field=\"date_actual\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Actual date [".strtolower($date["text_2"])."]")."'); return false;\" id=\"date_actual_".strtolower($key)."\" value=\"".$date["actual"]."\">".$date["actual_s2"]."</history>";
                      $date["delta"] = "";
                
                      $date["revised_s2"] = ($date["planned_s2"] != "") ? $date["planned_s2"] : $date["revised_s2"];

                      if(!is_array($date["revised_s2"])  && !is_array($date["actual_s2"])) {
                         if(strtotime($date["revised_s2"]) > 0 && strtotime($date["actual_s2"]) > 0) {
                            $v = (strtotime($date["revised_s2"]) - strtotime($date["actual_s2"])) / 86400;
                            $v = calc_delta(array("revised" => $date["revised_s2"], "actual" => $date["actual_s2"]));
                            if($v > 0) $v = "<span style=\"color:rgb(0,155,0);\">".$v."</span>\r\n";
                            if($v < 0) $v = "<span style=\"color:rgb(155,0,0);\">".($v * -1)."</span>\r\n";
                            $date["delta"] = $v;
                         }
                      }

                      $td[3] = "<history field=\"reason_code_actual_s2\" oncontextmenu=\"protocol.embed_indicator(this, '".rawurlencode("Reason code")."'); return false;\">".$date["delta"]."</history>";
                  }

                   if(($e_loop == "2" && intval($gate[0]["@attributes"]["loop_s2"]) > $loop) || count($gate) == 1 ||($e_loop == "1" && $current_loop[2])) {
                      $_module[] = 
                      "<tbody unid=\"".$gate[$loop]["@attributes"]["unid"]."\" key=\"dual source ".str_replace("array", "", strtolower(implode($k, " ")))."\">\r\n".
                      "<tr>\r\n".
                      "<td style=\"width:10px;\">".$date["menu_s2"]."</td>\r\n".
                      "<td class=\"".$class."\">".$date["text_s2"]."<tooltipp><div style=\"margin-left:40px;\">&nbsp;Gate:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[".$key."]</div></tooltipp></td>\r\n".
                      "<td class=\"".$class."\">".$td[0]."</td>\r\n".
                      "<td class=\"".$class."\">".$td[1]."</td>\r\n".
                      "<td class=\"".$class."\">".$td[2]."</td>\r\n".
                      "<td class=\"".$class."\">".$td[3]."</td>\r\n".
                      "</tr>\r\n".
                      "</tbody>\r\n";
                   }
                }
            }
         }
      }
 
      $module .= implode($_module)."</table>\r\n";
      $module = str_replace(array("%%ACTIVATE_NEXT%%", "undefined", "[]"), array($activate_next, "", ""), str_replace("Array", "", $module));
   }

   return $module."<script>tooltipp('tooltipp');</script>\r\n";


}


function date_get_indicator($date, $n_date, $is_v2) {
   $nr_date = (is_array($n_date["revised_value"])) ? $n_date["planned_value"] : $n_date["revised_value"];
   $status = 0;
   $now = date("Y-m-d", mktime());
   if(!is_array($date["actual"])) $status = 1;
   else {
      if(!is_array($date["revised"])) {
         $rdate = strtotime($date["revised"]);
         if(strtotime($now) - $rdate < 0) $status = 2;
         if(!is_array($nr_date) && $rdate > strtotime($nr_date)) $status = 5;
         if(strtotime($now) - $rdate > 0) $status = 4;
      }
   }


   // STATUS BERECHNEN ALTER PROZESS ------------------------------


   $html = "<div class=\"indicator".$status."\">";

   $html .= "</div>\r\n";
   return array($status, $html);

}





?>