<?php


session_start();
header("Content-Type: text/html; charset=UTF-8"); 
require_once("../../../addin/basic_php_functions.php");							// Basisfunktionen laden
require_once("../../../../../../library/tools/addin_xml.php");						// XML Library laden
require_once("../../../addin/load_tracker_data.php");
set_time_limit(180);											// KOMPLEXE ABFRAGEN MAX 180 SEKUNDEN
error_reporting(0);											// KEINE FEHLER :-)
set_error_handler("error_handler");									// ERROR HANDLER


print include_pot();



function include_pot() {

   if($_REQUEST["get"] == "") {
     $load = array("project", "tracker");
     $tc_data = create_tc_data($load);
     $get = $tc_data["project"]["number"]; 
     $c_tag = "c";
     $p_tag = "p";
   }
   else {
     $get = $_REQUEST["get"];
     $c_tag = "cc";
     $p_tag = "pp";
   }

   $html = "";
   $year = Date("Y", mktime());
   $i = 0;
   while($i < 6) {
      if($get == "_") $add = "_all";
      $load = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.export_pot".$add."?open&start=".(($i * 5000) + 1)."&count=5000&restricttocategory=".rawurlencode($get)."&function=plain");
      if(strpos(strtolower($load), "no documents found")) break;
      if($i == 0) {
            $file = explode(":", $load);
      }
      else {
         $load = explode(":", substr($load, strpos($load, ":"),strlen($load)));
         foreach($load as $val) array_push($file, $val);
      }
      $i++;
   }

   $line = 0;
   $dsp_status = array("000" => "Ungrabbed", "100" => "Grabbed", "200" => "Unrealized", "300" => "Reconditioned", "999" => "Released");
   if($tc_data["tracker"]["unid"] != "") $html = "<div id=\"trackerunid\" style=\"display:none;\">".$tc_data["tracker"]["unid"]."</div>";

   foreach($file as $val) {
      $row = explode(";", $val);
      $count = count($row);
      if(trim($row[0]) != "") {
         if($line == 0) { 
            $add_html .= "<".$c_tag."><a style=\"font-weight:bold;position:relative;top:2px;\">Added</a>";
            for($c = 1; $c < $count; $c++) {
               $td = $row[$c];
               $add_html .= "<a style=\"font-weight:bold;position:relative;top:2px;\">".$td."</a>";  
            }
            $add_html .= "</".$c_tag.">\r\n"; 
         }
         else {
            $status = substr(rawurldecode($row[1]), 7, 3);
            $row[1] = $dsp_status[$status]; 
            $addpot = ($_REQUEST["type"] == "add" && in_array($status, array("100", "200", "300", "999"))) ? false : true;
            if($addpot) {
               $add_html = "<".$p_tag." do=\"\" draggable=\"true\" ondragstart=\"add_pot.drag(event);\" pid=\"".$row[4]."+".$row[5]."+".$row[6]."+".$row[0]."+".strtolower($row[1])."\"><a>A</a>";           
               $key = array();
               for($c = 1; $c < $count; $c++) {
                  $td = $row[$c];
                  $key[] = $td;
                  $add_html .= "<a>".rawurldecode($td)."&nbsp;</a>";  
               } 
               $add_html .= "</".$p_tag.">\r\n";
            }
            else $html = "";
         }
         $html .= str_replace("%%KEY%%", implode($key, " "), $add_html);
         $line++;
      }
   }
   return $html;
}

?>

<style type="text/css">
#smartsearch_potsearch p a:nth-child(1) {
	width:40px;
}
#smartsearch_potsearch p a:nth-child(2) {
	width:90px;
}
#smartsearch_potsearch p a:nth-child(3) {
	width:135px;
}
#smartsearch_potsearch p a:nth-child(4) {
	width:120px;
}
#smartsearch_potsearch p a:nth-child(5) {
	width:120px;
}
#smartsearch_potsearch p a:nth-child(6) {
	width:30px;
}
#smartsearch_potsearch p a:nth-child(7) {
	width:140px;
}

#smartsearch_potsearch c a:nth-child(1) {
	width:44px;
}
#smartsearch_potsearch c a:nth-child(2) {
	width:94px;
}
#smartsearch_potsearch c a:nth-child(3) {
	width:139px;
}
#smartsearch_potsearch c a:nth-child(4) {
	width:124px;
}
#smartsearch_potsearch c a:nth-child(5) {
	width:124px;
}
#smartsearch_potsearch c a:nth-child(6) {
	width:34px;
}
#smartsearch_potsearch c a:nth-child(7) {
	width:144px;
}
#smartsearch_potsearch b {
	background-color:rgb(252,212,80);
	font-weight:normal;
}

#smartsearch_potsearch c {
	display:block;
	width:100%;
	padding-top:20px;
	padding-left:2px;
	border-bottom:solid 1px rgb(99,99,99);
}
#smartsearch_potsearch .p_reg {
	padding:2px;
	margin:0px;
	white-space:nowrap;
	background-color:rgb(255,255,255);
	border:solid 1px rgb(255,255,255);
	height:16px;
	padding:2px;
	cursor:default;
	display:block;
}

#smartsearch_potsearch .p_high {
	padding:2px;
	margin:0px;
	white-space:nowrap;
	background-color:rgb(243,243,247);
	border:solid 1px rgb(222,222,232);
	height:16px;
	padding:2px;
	cursor:default;
	display:block;
}

#smartsearch_potsearch a, #smartsearch_potsearch a:hover {
	display:inline-block;
	overflow:hidden;
	margin-right:4px;
	border:none;
	position:relative;
	top:-3px;
}
#smartsearch_potsearch div {
	font:normal 13px Open Sans;
	overflow-x:auto;
	overflow:hidden;
}

#smartsearch_potsearch #smartsearch_results {
	position:relative;
	top:14px;
	font:normal 13px Open Sans;
}

#smartsearch_potsearch input {
	font:normal 13px Open Sans;
	width:240px;
	height:15px;
	border:solid 1px rgb(99,99,99);
	padding:2px;
}

</style>
