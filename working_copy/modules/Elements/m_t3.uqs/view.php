<?php


session_start();
header("Content-Type: text/html; charset=UTF-8"); 
require_once("../../../addin/basic_php_functions.php");							// Basisfunktionen laden
require_once("../../../../../../library/tools/addin_xml.php");						// XML Library laden
set_error_handler("error_handler");									// ERROR HANDLER
ob_start("ob_gzhandler");

$filename = "output.php";
$file = file_get_contents($filename);
if(!file_exists($filename) || date("Ymd", filemtime($filename)) != date("Ymd", mktime())) {
   $output = m_t3__uqs_view();
   if(!strpos($output, "names.nsf")) file_put_contents($filename, $output);
   $file = $output;
}
else {
   $file = file_get_contents($filename);
}

print $file;

?>

<style type="text/css">
#smartsearch a:nth-child(1) {
	width:110px;
}
#smartsearch a:nth-child(2) {
	width:420px;
}
#smartsearch a:nth-child(3) {
	width:290px;
}
#smartsearch a:nth-child(4) {
	width:110px;
}
#smartsearch a:nth-child(5) {
	width:110px;
}
</style>

<?php

function m_t3__uqs_view() {

   $html = "";
   $year = Date("Y", mktime());
   for($i = 0; $i <= 1; $i++) {
     $load = file_get_authentificated_contents($_SESSION["remote_domino_path_epcquoting"]."/v.uqs_tdtracking?open&count=99999&restricttocategory=".($year - $i)."&function=plain");
      if($i == 0) {
            $file = explode(":", $load);
      }
      else {
         $load = explode(":", substr($load, strpos($load, ":"),strlen($load)));
         foreach($load as $val) array_push($file, $val);
      }
   }

  $line = 0;

   foreach($file as $val) {
      $row = explode(";", $val);
      $count = count($row);
      if(trim($row[0]) != "") {
         if($line == 0) { 
            $add_html = "<c id=\"smartsearch_head\" clear=\"true\">";
            for($c = 1; $c < $count; $c++) {
               $td = $row[$c];
               $add_html .= "<a>".$td."</a>";  
            }
            $add_html .= "</c>"; 
         }
         else {
            $add_html = "<p do=\"uqs.save('".$row[0]."');\">";           
            $key = array();
            for($c = 1; $c < $count; $c++) {
               $td = $row[$c];
               $key[] = $td;
               $add_html .= "<a>".rawurldecode($td)."&nbsp;</a>";  
            } 
            $add_html .= "</p>";
         }
         $html .= str_replace("%%KEY%%", implode($key, " "), $add_html);
         $line++;
      }
   }
   $file = str_replace("\\", "/", substr(__FILE__, 0, strrpos(__FILE__, "\\") + 1)."output.php");

   file_put_contents($file, str_replace("\n", "", $html));

   return $html;
}




?>