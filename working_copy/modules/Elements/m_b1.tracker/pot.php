<?php


$process_type_apply = ($tc_data["pot"]["meta"]["process_type"] == "Not selected") ? "" : "<a href=\"javascript:set_tracker('".base64_encode("process_type")."', '".base64_encode($tc_data["pot"]["meta"]["process_type"])."', '".base64_encode("process_type=\"\" & form=\"f.meta\" & ref\$tracker=\"".$tc_data["tracker"]["unique"]."\"")."');\"><img src=\"../../../../library/images/icons/change_all.gif\" alt=\"Apply to all\" style=\"position:absolute;border:none;filter:gray();\" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$process_rules_apply = ($tc_data["pot"]["meta"]["process_rules"] == "Not selected") ? "" : "<a href=\"javascript:set_tracker('".base64_encode("process_rules")."', '".base64_encode($tc_data["pot"]["meta"]["process_rules"])."', '".base64_encode("process_rules=\"\" & form=\"f.meta\" & ref\$tracker=\"".$tc_data["tracker"]["unique"]."\"")."');\"><img src=\"../../../../library/images/icons/change_all.gif\" alt=\"Apply to all\" style=\"position:absolute;border:none;filter:gray();\" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

// *************************************************
// START: PRODUCTION LOCATION / TRANSFERRED LOCATION
//**************************************************
for($i = 0; $i < count($_application["plant"]["name"]); $i++) $production_location[] = $_application["plant"]["name"][$i].":".$_application["plant"]["mail"][$i];   
$p_html = "<select name=\"production_location\" style=\"background-color:rgb(220,220,220);width:100%;height:20px;color:rgb(0,108,152);font:normal 11px century gothic,verdana;\" onchange=\"if(this.selectedIndex == 0) location.reload(); else save_production_location(this, '".$tc_data["pot"]["unid"]."', this.value, escape('".date("d.m.Y", time())."'), escape('".$tc_data["tool"]["number"]."'), escape('".$tc_data["tool"]["type"]."'), escape('".$tc_data["tool"]["old"]."'), escape('".$tc_data["part"]["number"]."'), escape('".urldecode($tc_data["part"]["name"])."'), escape('".urldecode($tc_data["tool"]["supplier"])."'), location.href, escape('".$tc_data["project"]["number"]."'));\">\r\n"."<option value=\"\"></option>%%OV%%\r\n";
$t_html = "<select name=\"transferred_location\" style=\"background-color:rgb(220,220,220);width:100%;height:20px;color:rgb(0,108,152);font:normal 11px century gothic,verdana;\" onchange=\"save_transferred_location(this, '".$tc_data["pot"]["unid"]."', this.value, escape('".date("d.m.Y", time())."'), '".$tc_data["pot"]["unid"]."');\">\r\n"."<option value=\"\"></option>%%OV%%\r\n";
$is_selected = false;
foreach($production_location as $val) {
   if($tc_data["pot"]["production_location"] == substr($val, 0 , strpos($val, ":"))) {
      $is_selected = true;
      $selected = " selected";
      $sel_p_loc = substr($val, 0 , strpos($val, ":"));
   }
   else $selected = "";
   if($tc_data["pot"]["transferred_location"] == substr($val, 0 , strpos($val, ":"))) {
      $selected_tl = " selected";
      $sel_t_loc = substr($val, 0 , strpos($val, ":"));
   }
   else $selected_tl = "";
   $style = (substr($val, 0 , strpos($val, ":")) == "Select outside vendor") ? " style=\"color:rgb(255,255,255);background-color:rgb(222,135,3);\"" : "";
   $p_html .= "<option value=\"".$val."\"".$selected.$style.">".substr($val, 0 , strpos($val, ":"))."</option>\r\n";
   $t_html .= "<option value=\"".$val."\"".$selected_tl.$style.">".substr($val, 0 , strpos($val, ":"))."</option>\r\n";
}
if(!$is_selected) $p_html = str_replace("%%OV%%", "<option value=\"\" selected>".urldecode($tc_data["pot"]["production_location"])."</option>", $p_html);
else $p_html = str_replace("%%OV%%", "", $p_html);
if(!$is_selected) $t_html = str_replace("%%OV%%", "<option value=\"\" selected>".urldecode($tc_data["pot"]["transferred_location"])."</option>", $t_html);
else $t_html = str_replace("%%OV%%", "", $t_html);
$p_html .= "</select>\r\n";
$t_html .= "</select>\r\n";

// EDIT / READ?
$p_html = (in_array("[location_g3]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"])) ? $p_html : $sel_p_loc."&nbsp;";
$t_html = (in_array("[t_location]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"])) ? $t_html : $sel_t_loc."&nbsp;";


// ************************************************
// ENDE: PRODUCTION LOCATION / TRANSFERRED LOCATION
// ************************************************

      $saplink = ($tc_data["tool"]["pono"] == "-") ? "<nobr>" : "<img src=\"../../../../library/images/sap_link.gif\" style=\"border:none;cursor:pointer;width:16px;position:absolute;margin-top:1px;\" onclick=\"location.href='addin/generate_sap_link.php?order=".$tc_data["tool"]["pono"]."&transaction=ME2M&field=S_EBELN-LOW';\"\" /><nobr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      $sapprlink = ($tc_data["tool"]["prno"] == "-") ? "<nobr>" : "<img src=\"../../../../library/images/sap_link.gif\" style=\"border:none;cursor:pointer;width:16px;position:absolute;margin-top:1px;\" onclick=\"location.href='addin/generate_sap_link.php?order=".$tc_data["tool"]["prno"]."&transaction=ME53&field=EBAN-BANFN';\"\" /><nobr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                                                  

      $embed_process_type = (!is_array($pot_date[$_REQUEST["pot"]])) ? "<a href=\"javascript:load_page('type','".$tc_data["pot"]["meta"]["unid"]."','".$tc_data["pot"]["meta"]["process_type"]."');\"><img src=\"../../../../library/images/icons/select.png\" style=\"width:20px;position:absolute;vertical-align:middle;border:none;\" /></a>" : "";
      $embed_process_rules = (!is_array($pot_date[$_REQUEST["pot"]]) &&  (in_array("[t_d]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"]))) ? "<a href=\"javascript:load_page('rules','".$tc_data["pot"]["meta"]["unid"]."','".$tc_data["pot"]["meta"]["process_rules"]."');\"><nobr><img src=\"../../../../library/images/icons/select.png\" style=\"width:20px;position:absolute;vertical-align:middle;border:none\" /></a>" : "";

      $d_select = "(@lowercase(@text(@documentuniqueid))=\"".strtolower($tc_data["pot"]["unid"])."\") | (ref\$pot=\"".$tc_data["pot"]["unique"]."\" & ref\$tracker=\"".$tc_data["tracker"]["unique"]."\")";

      $img_1 = ($tc_data["pot"]["status"] != "0") ? "select_high" : "select_reg";
      $img_2 = ($tc_data["pot"]["status"] == "0") ? "select_high" : "select_reg";

      $dsp_pot_part_tool = "<iframe scrolling=\"no\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\" framespacing=\"0\" name=\"ifrm\" src=\"\" style=\"visibility:hidden;z-index:99999;position:absolute;top:40px;left:20px;width:674px;height:380px;filter:alpha(opacity=92);border:dotted 1px rgb(99,99,99);\"></iframe>".
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:500px;\" id=\"tbl_pot_pot\">".
      "<tr>".
      "<th colspan=\"4\"><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Tracker & POT data headline');\">Tracker & POT data</span></strong></th>".
      "</tr>".
      "<tr>".   
      "<td class=\"descr\" width=\"25%\"><a href=\"javascript:void(0);\" onclick=\"change_status('".rawurlencode("doc\$status")."', '".rawurlencode("1")."', '".rawurlencode($d_select)."');\"><img src=\"../../../../library/images/icons/".$img_1.".png\" style=\"position:relative;top:5px;left:8px;vertical-align:top;filter:chroma(color=#ffffff);width:12px;border:none;\" /></a>&nbsp;&nbsp;&nbsp&nbsp;&nbsp;Active POT</td>".
      "<td class=\"descr\" width=\"25%\"><a href=\"javascript:void(0);\" onclick=\"change_status('".rawurlencode("doc\$status")."', '".rawurlencode("0")."', '".rawurlencode($d_select)."');\"><img src=\"../../../../library/images/icons/".$img_2.".png\" style=\"position:relative;top:5px;left:8px;vertical-align:top;filter:chroma(color=#ffffff);width:12px;border:none;\" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inactive POT</td>".
      "<td class=\"descr\" colspan=\"2\">&nbsp;</td>\r\n".
      "</tr>".
      "<tr>".   
      "<td class=\"descr\" width=\"25%\">".$embed_process_type."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process type:</td>".
      "<td class=\"data\" width=\"25%\">".$process_type_apply.$tc_data["pot"]["meta"]["process_type"]."</td>".
      "<td class=\"descr\" width=\"25%\">".$embed_process_rules."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process rules:</nobr></td>".
      "<td class=\"data\" width=\"25%\">".$process_rules_apply.$tc_data["pot"]["meta"]["process_rules"]."</td>".
      "</tr>";
   if($tc_data["pot"]["is_proposal"] == "1") {
   }
   else {
      $dsp_pot_part_tool .= 
      "<tr>".      
      "<td class=\"descr\" colspan=\"1\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project Case:</nobr></td>";

      if(strlen($tc_data["pot"]["disno"])<=12){
         $dsp_pot_part_tool .= 
         "<td class=\"data\" colspan=\"1\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcasesbynumber/".$tc_data["pot"]["disno"]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["pot"]["disno"]."</a></td>";
      }else{
         $dsp_pot_part_tool .= 
         "<td class=\"data\" colspan=\"1\"><b>!!!</b> ".substr($tc_data["pot"]["disno"],0,"12")." <b>!!!</b>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcasesbynumber/".substr($tc_data["pot"]["disno"],(-12))."?open\" class=\"linkdata\" target=\"_blank\">".substr($tc_data["pot"]["disno"],(-12))."</a></td>";
      }
      $dsp_pot_part_tool .= 

      "<td class=\"data\" colspan=\"2\"><a href=\"ee_edit_pot.php?&load=".strtolower($tc_data["pot"]["case_unid"])."\"><img src=\"".$_SESSION["php_sever"]."/library/images/16x16/setting-5.png\" style=\"broder:none;margin-left:3px;\" /></a></td>".
      "</tr>";
   }

   $uqs["number"] = (is_array($tc_data["pot"]["uqs"]["unid"])) ? "&nbsp;" : "<span class=\"\basic_link\"><a style=\"font:normal 13px century gothic,verdana;color:rgb(0,102,158);\" href=\"".$_SESSION["remote_domino_path_epcquoting"]."/_/".$tc_data["pot"]["uqs"]["unid"]."?open\" target=\"_blank\">".rawurldecode($tc_data["pot"]["uqs"]["number"])."</a></span>";
   $uqs["name"] = (is_array($tc_data["pot"]["uqs"]["unid"])) ? "&nbsp;" : rawurldecode($tc_data["pot"]["uqs"]["name"]);
   $dsp_pot_part_tool .= 
   "<tr>".
   "<th colspan=\"4\"><strong>UQS</strong></th>".
   "</tr>".
   "<tr>".
   "<td class=\"descr\" style=\"width:1%;\"><nobr><img src=\"../../../../library/images/16x16/edition-50.png\" onclick=\"location.href=location.href.replace(/edit_tracker/, 'add_uqs');\" style=\"margin-right:4px;vertical-align:top;cursor:pointer;\" />Number:&nbsp;&nbsp;&nbsp;</nobr></td>".
   "<td class=\"data\" colspan=\"3\">".$uqs["number"]."&nbsp;</td>".

   "</tr>".
   "<tr>".
   "<td class=\"descr\" style=\"width:1%;padding-left:22px;\"><nobr>Name:&nbsp;&nbsp;&nbsp;</nobr></td>".
   "<td class=\"data\" colspan=\"3\">".$uqs["name"]."&nbsp;</td>".
   "</tr>".
   "<tr>".
   "<th colspan=\"4\"><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-part data headline');\">Part data</span></strong>".
   "<div style=\"position:absolute;\"><img src=\"../../../../library/images/16x16/Content-39.png\" onclick=\"javascript:window.open('http://bekworld1.be.tycoelectronics.com/Automotive/EvalReport/default.cfm?Part_Nr=".$tc_data["part"]["number"]."')\" style=\"cursor:pointer;position:relative; left:455px;\" /></div></th>".
    "</tr>";

   if($tc_data["pot"]["is_proposal"] == "1") {
      $dsp_pot_part_tool .= 
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Number:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$tc_data["pot"]["part_proposal"]." [PROPOSAL]</td>".
      "</tr>";
   }
   else {
      $proposal_part = (count($tc_data["pot"]["part_proposal"]) > 0 ) ? "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[PROPOSAL&nbsp;".$tc_data["pot"]["part_proposal"]."]</span>" : "";
      $dsp_pot_part_tool .= 
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Number:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"2\" >".
      "<div style=\"position:absolute;\"><img src=\"../../../../library/images/16x16/communication-99.png\" onclick=\"javascript:window.open('http://partinquiry.tycoelectronics.com/tpmweb/views/PartInquiryView.faces?partNumber=".$tc_data["part"]["number"]."')\" style=\"cursor:pointer;position:relative;left:-22px;\" /></div>".
      "<a class=\"linkdata\" target=\"_blank\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/0/".$tc_data["part"]["unid"]."\" style=\"font-weight:bold;\" >".$tc_data["part"]["number"]."</a>".$proposal_part."<div style=\"position:absolute;margin-top:10px;display:none;\" id=\"part_history\"></div></td>".
      "<td class=\"data\" style=\"text-align:right;\"><a href=\"javascript:load_history('".$tc_data["part"]["unid"]."', 'part');\"><img src=\"../../../../library/images/16x16/objects-18.png\" style=\"border:none;margin-right:8px;\" /></a></td>\r\n".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Name:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".urldecode($tc_data["part"]["name"])."<img src=\"../../../../library/images/blank.gif\" /></td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Part Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".$tc_data["part"]["rev"]."</td>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Drw. Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".$tc_data["part"]["drawing"]."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Supplier:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".urldecode($tc_data["part"]["supplier"])."</td>".
      "</tr>";


   }
      $dsp_pot_part_tool .=
      "<tr>".
      "<th colspan=\"4\"><a name=\"tool_unid\" style=\"display:none;\">".$tc_data["tool"]["unid"]."</a><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-tool data headline');\">Base Tool</span></strong></th>".
      "</tr>";
   if($tc_data["pot"]["is_proposal"] == "1") {
      $dsp_pot_part_tool .= 
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Number:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$tc_data["pot"]["tool_proposal"]." [PROPOSAL]</td>".
      "</tr>";
   }
   else {
      $proposal_tool = (count($tc_data["pot"]["tool_proposal"]) > 0 ) ? "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[PROPOSAL&nbsp;".$tc_data["pot"]["tool_proposal"]."]</span>" : "";
      $dsp_pot_part_tool .= 
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Base Number:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".substr(urldecode($tc_data["tool"]["number"]),0,10)."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Tool Type:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_selectbox(array("[superuser]", "[t_d]"), true, array("pilot", "production"), array("Pilot", "Production"), $tc_data["tool"]["type"], array("name" => "tooltype", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:110px;font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onchange" => "handle_save_single_field_extdb(session.remote_domino_path_epctlib, '".$tc_data["tool"]["source_unid"]."', 'tooltype', this.value, '');"))."</td>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Technology:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_selectbox(array("[superuser]", "[t_d]"), true, array("die", "mold", "assembly", "soldering", "packaging", "plating", "coating", "metalworking", "other"), array("Die", "Mold", "Assembly", "Soldering", "Packaging", "Plating", "Coating", "Metalworking", "Other"), $tc_data["tool"]["techno"], array("name" => "technology", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:110px;font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onchange" => "handle_save_single_field_extdb(session.remote_domino_path_epctlib, '".$tc_data["tool"]["source_unid"]."', 'technology', this.value, 'document.getElementById(&#x22;get_technology&#x22;).innerText=&#x22;' + this.value + '&#x22;');"))."<span id=\"get_technology\" style=\"display:none;\">".$tc_data["tool"]["techno"]."</span></td>".
      "</tr>".
      "<tr>";


      if ($tc_data["tool"]["dualsource"] == "1" ) {
         $dsource = "<img src=\"../../../../library/images/16x16/status-11.png\">";
      } else {
         $dsource = "<img src=\"../../../../library/images/16x16/status-11-1.png\">";  
      }
 
      if ($tc_data["tool"]["a2p"] == "1" ) {
         $a2p = "<img src=\"../../../../library/images/16x16/status-11.png\">";
      } else {
         $a2p = "<img src=\"../../../../library/images/16x16/status-11-1.png\">";  
      } 

      $dsp_dsource = (check_editable(array("[superuser]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"set_checkbox(this, '".$tc_data["tool"]["unid"]."', 'dualsource');\">".$dsource."</a>" : $dsource;
      $dsp_a2p = (check_editable(array("[superuser]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"set_checkbox(this, '".$tc_data["tool"]["unid"]."', 'a2p');\">".$a2p."</a>" : $a2p;

      // ZUFALLIDS F�R FELDER
      $arr = array("pono", "prno", "pos", "calendar_cevt", "old", "cavity", "convkitno", "spec", "costs", "guarantoutput", "coutput");
      foreach($arr as $val) {
         eval("\$unique_".$val." = generate_uniqueid(8);");
      }

      $dsp_calendar = (check_editable(array("[superuser]", "[procurement]"))) ? "<a href=\"javascript:load_calendar('".$unique_calendar_cevt."', '".$_SESSION["remote_database_path"]."addin/calendar2.php?&p_function=sc2&p_use_rule=0&p_id=calendar_".$unique_calendar_cevt."','');\"><img src=\"../../../../library/images/16x16/time-3.png\" style=\"border:none;margin-right:7px;vertical-align:top;position:relative;top:2px;\" /></a>" : "";

      $dsp_pot_part_tool .= 
      "<td class=\"descr\" style=\"width:1%;\"><nobr>A2P:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$dsp_a2p."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Old Tool No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".embed_input(array("[superuser]","[t_d]"), $tc_data["tool"]["old"], array("id" => $unique_old, "type" => "text", "name" => "oldtool", "style" => "background-color:rgb(220,220,220);width:130px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_old."&#x22;)');"))."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>No. of cavities:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_input(array("[superuser]","[t_d]"), $tc_data["tool"]["cav"], array("id" => $unique_cavity, "type" => "text", "name" => "cavity", "style" => "background-color:rgb(220,220,220);width:30px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_cavity."&#x22;)');"))."</td>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Conversion Kit No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_input(array("[superuser]","[t_d]"), $tc_data["tool"]["convkit"], array("id" => $unique_convkitno, "type" => "text", "name" => "convkitno", "style" => "background-color:rgb(220,220,220);width:30px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_convkitno."&#x22;)');"))."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Tool specification:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".embed_input(array("[superuser]", "[t_d]"), $tc_data["tool"]["spec"], array("id" => $unique_spec, "type" => "text", "name" => "toolspec", "style" => "background-color:rgb(220,220,220);width:324px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_spec."&#x22;)');"))."</td>".
      "</tr>";


      $dsp_img = (in_array("[t_location]", $_SESSION["remote_userroles"])) ? "visible" : "hidden";
      if (urldecode($tc_data["pot"]["transferred_location"]) == 'Not set') {
	$dsp_pot_part_tool .= 
	"<tr>".
	"<td class=\"descr\" style=\"width:1%;\"><nobr>Transferred location:&nbsp;<img src=\"../../../../library/images/16x16/files-28.png\" onclick=\"get_value('select', 'production_location', 'transferred_location');handle_save_single_field('".$tc_data["pot"]["unid"]."', 't_location', document.getElementsByName('production_location')[0][document.getElementsByName('production_location')[0].selectedIndex].text, 'location.href=location.href');\" style=\"cursor:pointer;vertical-align:top;position:relative;left:4px;top:1px;visibility:".$dsp_img.";\" /></a></nobr></td>".
        "<td class=\"data\" colspan=\"3\">".$t_html."</td>".
        "</tr>";
      } else {
	$dsp_pot_part_tool .= 
	"<tr>".
	"<td class=\"descr\" style=\"width:1%;\"><nobr>Transferred location:&nbsp;</nobr></td>".
        "<td class=\"data\" colspan=\"3\">".urldecode($tc_data["pot"]["transferred_location"])."</td>".
        "</tr>";

      }
      if (urldecode($tc_data["pot"]["production_location"]) == 'Not set') {
      $dsp_pot_part_tool .= 
         "<tr>".
         "<td class=\"descr\" style=\"width:1%;\">Production&nbsp;location&nbsp;G3:</td>".
         "<td class=\"data\" colspan=\"3\">".$p_html."</td>".
         "</tr>";
      } else {
         $dsp_pot_part_tool .= 
        "<tr>".
         "<td class=\"descr\" style=\"width:1%;\">Production&nbsp;location&nbsp;G3:</td>".
         "<td class=\"data\" colspan=\"3\">".urldecode($tc_data["pot"]["production_location"])."<div style=\"display:none;\">".$p_html."</div></td>".
         "</tr>";
      }

      $tc_data["base_tool"]["scan_location"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["scan_location"]);
      $tc_data["base_tool"]["digital_report"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["digital_report"]);
      $tc_data["base_tool"]["tabulated_report"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["tabulated_report"]);

      $dsp_selected_scan_location = $tc_data["base_tool"]["scan_location"];
      if($tc_data["base_tool"]["scan_location"] == "TE" && $tc_data["base_tool"]["selected_scan_location"] != "") {
         $dsp_selected_scan_location .= " [".rawurldecode($tc_data["base_tool"]["selected_scan_location"])."]";
      }

      $dsp_selected_digital_report = $tc_data["base_tool"]["digital_report"];
      if($tc_data["base_tool"]["digital_report"] == "TE" && $tc_data["base_tool"]["selected_digital_report"] != "") {
         $dsp_selected_digital_report .= " [".rawurldecode($tc_data["base_tool"]["selected_digital_report"])."]";
      }

      $dsp_selected_tabulated_report = $tc_data["base_tool"]["tabulated_report"];
      if($tc_data["base_tool"]["tabulated_report"] == "TE" && $tc_data["base_tool"]["selected_tabulated_report"] != "") {
         $dsp_selected_tabulated_report .= " [".rawurldecode($tc_data["base_tool"]["selected_tabulated_report"])."]";
      }


      $dsp_pot_part_tool .=
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Supplier:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".rawurldecode($tc_data["base_tool"]["supplier"])."&nbsp;</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Scan location:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$dsp_selected_scan_location."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Digital report:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$dsp_selected_digital_report."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Tabulated report:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".$dsp_selected_tabulated_report."</td>".
      "</tr>";

   }
   
      
      // Additional Tool Info
      $dsp_pot_part_tool .=
      "<tr>".
      "<th colspan=\"4\"><a name=\"additional_unid\" style=\"display:none;\">".$tc_data["additional"]["unid"]."</a><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-additional tool info headline');\">Additional Tool Info</span></strong></th>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Number:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"2\"><a class=\"linkdata\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/0/".$tc_data["tool"]["unid"]."?open\" target=\"_blank\" style=\"font-weight:bold;\">".urldecode($tc_data["tool"]["number"])."</a>".$proposal_tool."<div style=\"position:absolute;margin-top:10px;display:none;\" id=\"tool_history\"></div></td>".
      "<td class=\"data\" style=\"text-align:right;\"><a href=\"javascript:void(0);\" onclick=\"load_history('".$tc_data["tool"]["unid"]."', 'tool');\"><img src=\"../../../../library/images/16x16/objects-18.png\" style=\"border:none;margin-right:8px;\" /></a></td>\r\n".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Name:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".urldecode($tc_data["tool"]["name"])."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>CuCo:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".$tc_data["tool"]["cuco"]."</td>".
      "<td class=\"descr\" style=\"width:1%;\">".$saplink."PO No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_input(array("[superuser]","[procurement]", "[t_d]"), $tc_data["tool"]["pono"], array("id" => $unique_pono, "type" => "text", "name" => "pono", "style" => "background-color:rgb(220,220,220);width:70px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_pono."&#x22;)');")).
      "&nbsp;/&nbsp;&nbsp;".
      embed_input(array("[superuser]"), $tc_data["tool"]["pos"], array("id" => $unique_pos, "type" => "text", "name" => "position", "style" => "background-color:rgb(220,220,220);width:19px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_pos."&#x22;)');"))."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Supplier confirmed EVT:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".$dsp_calendar."<div id=\"calendar_".$unique_calendar_cevt."\" style=\"position:absolute;background-color:rgb(255,255,255);\"></div><span id=\"".$unique_calendar_cevt."\"><date>".$tc_data["tool"]["confevt"]."</date></span></td>".
      "<td class=\"descr\" style=\"width:1%;\">".$sapprlink."PR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_input(array("[superuser]","[procurement]", "[t_d]"), $tc_data["tool"]["prno"], array("id" => $unique_prno, "type" => "text", "name" => "purch_req", "style" => "background-color:rgb(220,220,220);width:70px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_prno."&#x22;)');"))."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Supplier:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".urldecode($tc_data["tool"]["supplier"])."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Total tooling costs:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_input(array("[superuser]", "[t_d]"), $tc_data["tool"]["costs"], array("id" => $unique_costs, "type" => "text", "name" => "totalcost", "style" => "background-color:rgb(220,220,220);width:103px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_costs."&#x22;)');"))."</td>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Dual Source:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".$dsp_dsource."</td>".
      "</tr>".
      "<tr>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Guaranteed output:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\">".embed_input(array("[t_d]"), $tc_data["tool"]["goutput"], array("id" => $unique_guarantoutput, "type" => "text", "name" => "guarantoutput", "style" => "background-color:rgb(220,220,220);width:50px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_guarantoutput."&#x22;)');"))."</td>".
      "<td class=\"descr\" style=\"width:1%;\"><nobr>Current output:&nbsp;&nbsp;&nbsp;</nobr></td>".
      "<td class=\"data\" colspan=\"3\">".embed_input(array("[t_d]"), $tc_data["tool"]["coutput"], array("id" => $unique_coutput, "type" => "text", "name" => "currentoutput", "style" => "background-color:rgb(220,220,220);width:50px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_coutput."&#x22;)');"))."</td>".
      "</tr>";


   if($tc_data["pot"]["is_proposal"] == "1") {
   }
   else {
      $num = ($_REQUEST["loop"] != "") ? $_REQUEST["loop"] : $maxloop - 1;
      $dsp_pot_part_tool .=
      "<tr>".
      "<th colspan=\"4\"><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Control Report headline');\">Control Report<a name=\"control_report\">&nbsp;</a>Source 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loop:</span></strong>";
      if($maxloop != "" && $maxloop > 1) {
      //   $dsp_loops = array_unique($tc_data["cr"]["0"]["load"]["loop"]);
         for($z = 0; $z < $maxloop; $z++) $dsp_loops[] = $z;
         $dsp_pot_part_tool .= embed_selectbox(false, true, $dsp_loops, $dsp_loops, $num, array("name" => "select_creport", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:110px;font:normal 11px century gothic,verdana;color:rgb(0,0,0);height:20px;padding:0px;margin-left:10px;", "onchange" => "loop = (this.value != '') ?(Number(this.value)) : ''; location.href='?".str_replace("&loop=".$_REQUEST["loop"], "", $_SERVER["QUERY_STRING"])."&loop=' + loop + '#control_report';"));
      }
      // STATUS anzeigen
      if($tc_data["cr"]["0"]["status"] == "0") $dsp_status = "in preparing";
      if($tc_data["cr"]["0"]["status"] == "1") $dsp_status = "to process";
      if($tc_data["cr"]["0"]["status"] == "2") $dsp_status = "in process";
      if($tc_data["cr"]["0"]["status"] == "3") $dsp_status = "prod. eval.";
      if($tc_data["cr"]["0"]["status"] == "4") $dsp_status = "withdrawn";
      if($tc_data["cr"]["0"]["status"] == "5") $dsp_status = "process eval.";
      if($tc_data["cr"]["0"]["status"] == "5" && $tc_data["cr"]["0"]["DateIRA"] <> "false") $dsp_status = "forwarded to PE";
      if($tc_data["cr"]["0"]["status"] == "6") $dsp_status = "prod. & proc. eval.";

      // STATUS Source 2 anzeigen
      if($tc_data["cr"]["1"]["status"] == "0") $s2dsp_status = "in preparing";
      if($tc_data["cr"]["1"]["status"] == "1") $ds2sp_status = "to process";
      if($tc_data["cr"]["1"]["status"] == "2") $s2dsp_status = "in process";
      if($tc_data["cr"]["1"]["status"] == "3") $s2dsp_status = "prod. eval.";
      if($tc_data["cr"]["1"]["status"] == "4") $s2dsp_status = "withdrawn";
      if($tc_data["cr"]["1"]["status"] == "5") $s2dsp_status = "process eval.";
      if($tc_data["cr"]["1"]["status"] == "5" && $tc_data["cr"]["1"]["DateIRA"] <> "false") $s2dsp_status = "forwarded to PE";
      if($tc_data["cr"]["1"]["status"] == "6") $s2dsp_status = "prod. & proc. eval.";

// print_r ($tc_data); die;



      $ptr = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_last_process_trial_run?open&restricttocategory=".$tc_data["cr"][0]["number"]."&count=1&function=xml:data");
      if(!strpos(strtolower($ptr), "no documents found")) {
         $tmp = process_xml_from_string($ptr);
         $tc_data["cr"][0]["status_ira_process"][0] = $tmp["cr"];
         unset($tmp);
      }

      $ptr = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_last_process_trial_run?open&restricttocategory=".$tc_data["cr"][1]["number"]."&count=1&function=xml:data");
      if(!strpos(strtolower($ptr), "no documents found")) {
         $tmp = process_xml_from_string($ptr);
         $tc_data["cr"][1]["status_ira_process"][0] = $tmp["cr"];
         unset($tmp);
      }



      $dsp_pot_part_tool .=
      "</th>".
      "</tr>";
      if(isset($tc_data["cr"]["0"]["number"])) {
         $dsp_pot_part_tool .=
         "<tr>".
         "<td class=\"descr\" style=\"width:1%;\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\"><a id=\"get_cr_number\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"]["0"]["number"]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"]["0"]["number"]."</a></td>".
         "<td class=\"descr\" style=\"width:1%;\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\">".format_date($tc_data["cr"]["0"]["issued"])."</td>".
         "</tr>".
         "<tr>".
         "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRT:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\">".$tc_data["cr"]["0"]["status_ir"]."</td>".
         "<td class=\"descr\" style=\"width:1%;\"><nobr>Drw. Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\">".$tc_data["cr"]["0"]["drwrev"]."</td>".
         "</tr>".
         "<tr>".
         "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRP:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\">".$tc_data["cr"]["0"]["status_irp"]."</td>".
         "<td class=\"descr\" style=\"width:1%;\"><nobr>Part Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\">".$tc_data["cr"]["0"]["rev"]."</td>".
         "</tr>".
         "<tr>".
         "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRS:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\">".$tc_data["cr"]["0"]["status_irs"]."</td>".
         "<td class=\"data\" colspan=\"2\">&nbsp;</td>".
         "</tr>".
         "<tr>".
         "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\" style=\"border-bottom:none;\">".$tc_data["cr"]["0"]["status_ira"]."<img src=\"../../../../library/images/blank.gif\" /></td>".
         "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\" style=\"border-bottom:none;\">".$dsp_status."</td>".
         "</tr>";

         $cnt = count($tc_data["cr"][0]["status_ira_process"]);
         if($cnt > 0) {
            $dsp_pot_part_tool .=
            "<tr>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);border-top:solid 1px rgb(255,255,255);\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"][0]["status_ira_process"][$cnt - 1]["number"]."?open\" style=\"color:rgb(255,255,255);\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"][0]["status_ira_process"][$cnt - 1]["number"]."</a><img src=\"../../../../library/images/blank.gif\" /></td>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);\">".format_date($tc_data["cr"][0]["status_ira_process"][$cnt - 1]["date"])."</td>".
            "</tr>".
            "<tr>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".$tc_data["cr"][0]["status_ira_process"][$cnt - 1]["status_ira"]."</td>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".rawurldecode($tc_data["cr"][0]["status_ira_process"][$cnt - 1]["status"])."</td>".
            "</tr>";
         }

 
         $do_trial_run = (in_array("[composeproz]", $_SESSION["remote_userroles"])) ? true : false;			// Rolle
         if(!in_array(substr($tc_data["tool"]["number"],0,2), Array(21, 23, 28))) $do_trial_run = false;		// Tool Prefix
         if(strtoupper($tc_data["pot"]["meta"]["process_type"]) == "pilot") $do_trial_run = false;			// Prozesstyp
         if($cnt > 0 && is_array($tc_data["cr"][0]["status_ira_process"][$cnt - 1]["status_ira"])) $do_trial_run = false;							// Ein unbewerterter PTR exisitiert
	 if($num != ($maxloop - 1)) $do_trial_run = false;

         if($do_trial_run) {
            $dsp_pot_part_tool .=
            "<tr>".
            "<td class=\"descr\" colspan=\"4\"><span class=\"phpbutton\"><a href=\"javascript:run_ajax('run_ajax','".$_SESSION["remote_domino_path_epcmain"]."/a.copy_cr?open&cr_reason=process%20approval&cr_type=2&unid=".$tc_data["cr"][0]["unid"]."&user=".$_SESSION["domino_user"]."','open_cr_copy()');\">Create process trial run</a></span></td>".
            "</tr>";
         }



      }
      else {
         if($maxloop != "" && (in_array("[cr]", $_SESSION["remote_userroles"]) || in_array("[t_d]", $_SESSION["remote_userroles"]))) {
            $url_loop = ($_REQUEST["loop"] != "") ? $_REQUEST["loop"] : strip_tags($tc_data["cr"]["0"]["loop"]-1);
            $dsp_pot_part_tool .=
            "<tr>".
            "<td class=\"data\" colspan=\"4\" style=\"padding:10px 10px 10px 2px;\"><span class=\"phpbutton\">". 
            "<a href=\"javascript:void(0);\" onclick=\"create_cr('".$tc_data["pot"]["unique"]."', '".strtolower($tc_data["pot"]["unid"])."', '".strip_tags($tc_data["pot"]["source_link_unid"])."' , '".strip_tags($tc_data["pot"]["source_part_unid"])."', '".$_REQUEST["unique"]."', '".strip_tags($tc_data["pot"]["source_tool_unid"])."' , '1', '".rawurlencode("correction loop")."', '".($maxloop - 1)."');\">Create NEW CR</a>".
            "</span></td>".
            "</tr>";
         }
      }

      // START: DUALSOURCE
      if($tc_data["tool"]["dualsource"] == "1") {
         $num = ($_REQUEST["loop"] != "") ? $tc_data["cr"]["1"]["load"]["loop"][$_REQUEST["loop"]] : $maxloop;
         $dsp_pot_part_tool .=
         "<tr>".
         "<th colspan=\"4\"><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Control Report headline');\">Control Report Source 2</span></strong>";
   //      if($maxloop != "" && $maxloop > 1) $dsp_pot_part_tool .= embed_selectbox(false, true, $tc_data["cr"]["1"]["load"]["loop"], $tc_data["cr"]["1"]["load"]["number"], $num, array("name" => "select_creport", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:110px;font:normal 11px century gothic,verdana;color:rgb(0,0,0);height:20px;padding:0px;margin-left:10px;", "onchange" => "location.href='?".str_replace("&loop=".$_REQUEST["loop"], "", $_SERVER["QUERY_STRING"])."&loop=' + loop;"));
         $dsp_pot_part_tool .=
         "</th>".
         "</tr>";
         if(isset($tc_data["cr"]["1"]["number"])) {
            $num = ($_REQUEST["loop"] != "") ? $_REQUEST["loop"] : $maxloop - 1;
            $cnt = count($tc_data["cr"][1]["status_ira_process"]);
            $dsp_pot_part_tool .=
            "<tr>".
            "<td class=\"descr\" style=\"width:1%;\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\"><a id=\"get_cr_number\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"]["1"]["number"]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"]["1"]["number"]."</a></td>".
            "<td class=\"descr\" style=\"width:1%;\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\">".format_date($tc_data["cr"]["1"]["issued"])."</td>".
            "</tr>".
            "<tr>".
            "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRT:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\">".$tc_data["cr"]["1"]["status_ir"]."</td>".
            "<td class=\"descr\" style=\"width:1%;\"><nobr>Drw. Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\">".$tc_data["cr"]["1"]["drwrev"]."</td>".
            "</tr>".
            "<tr>".
            "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRP:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\">".$tc_data["cr"]["1"]["status_irp"]."</td>".
            "<td class=\"descr\" style=\"width:1%;\"><nobr>Part Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\">".$tc_data["cr"]["1"]["rev"]."</td>".
            "</tr>".
            "<tr>".
            "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRS:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" colspan=\"3\">".$tc_data["cr"]["1"]["status_irs"]."&nbsp;</td>".
            "</tr>".
            "<tr>".
            "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"border-bottom:none;\">".$tc_data["cr"]["1"]["status_ira"]."<img src=\"../../../../library/images/blank.gif\" /></td>".
            "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"border-bottom:none;\">".$s2dsp_status."&nbsp;</td>".
            "</tr>";


            if($cnt > 0) {
               $dsp_pot_part_tool .=
               "<tr>".
               "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
               "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"][1]["status_ira_process"][$cnt - 1]["number"]."?open\" style=\"color:rgb(255,255,255);\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"][1]["status_ira_process"][$cnt - 1]["number"]."</a><img src=\"../../../../library/images/blank.gif\" /></td>".
               "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
               "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);\">".format_date($tc_data["cr"][1]["status_ira_process"][$cnt - 1]["date"])."</td>".
               "</tr>".
               "<tr>".
               "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
               "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".$tc_data["cr"][1]["status_ira_process"][$cnt - 1]["status_ira"]."</td>".
               "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
               "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".rawurldecode($tc_data["cr"][1]["status_ira_process"][$cnt - 1]["status"])."</td>".
               "</tr>";
            }

   
            $do_trial_run = (in_array("[composeproz]", $_SESSION["remote_userroles"])) ? true : false;			// Rolle
            if(!in_array(substr($tc_data["tool"]["number"],0,2), Array(21, 23, 28))) $do_trial_run = false;		// Tool Prefix
            if(strtoupper($tc_data["pot"]["meta"]["process_type"]) == "pilot") $do_trial_run = false;			// Prozesstyp
            if($cnt > 0 && is_array($tc_data["cr"][1]["status_ira_process"][$cnt - 1]["status_ira"])) $do_trial_run = false;							// Ein unbewerterter PTR exisitiert
	    if($num != ($maxloop - 1)) $do_trial_run = false;

            if($do_trial_run) {
               $dsp_pot_part_tool .=
               "<tr>".
               "<td class=\"descr\" colspan=\"4\"><span class=\"phpbutton\"><a href=\"javascript:run_ajax('run_ajax','".$_SESSION["remote_domino_path_epcmain"]."/a.copy_cr?open&cr_reason=process%20approval&cr_type=2&unid=".$tc_data["cr"][1]["unid"]."&user=".$_SESSION["domino_user"]."','open_cr_copy()');\">Create process trial run</a></span></td>".
               "</tr>";
            }


         }
         else {
            if($maxloop != "" && (in_array("[cr]", $_SESSION["remote_userroles"]) || in_array("[t_d]", $_SESSION["remote_userroles"]))) {
               $url_loop = ($_REQUEST["loop"] != "") ? $_REQUEST["loop"] : strip_tags($tc_data["cr"]["0"]["loop"]);
               $style = ($maxloop - 1 > $url_loop ) ? "none" : "block";
               $dsp_pot_part_tool .=
               "<tr>".
               "<td class=\"data\" colspan=\"4\" style=\"padding:10px 10px 10px 2px;\"><span class=\"phpbutton\" style=\"display:$style;\"><a href=\"javascript:void(0);\" onclick=\"create_cr('".$tc_data["pot"]["unique"]."', '".strtolower($tc_data["pot"]["unid"])."', '".strip_tags($tc_data["pot"]["source_link_unid"])."', '".strip_tags($tc_data["pot"]["source_part_unid"])."', '".$_REQUEST["unique"]."', '".strip_tags($tc_data["pot"]["source_tool_unid"])."', '2', '".rawurlencode("correction loop")."', '".($maxloop - 1)."');\">Create NEW CR</a></span></td>".
               "</tr>";
            }
         }
      }
     // ENDE: DUALSOURCE
   }


   $dsp_pot_part_tool = str_replace("Array", "<img src=\"../../../../library/images/blank.gif\" />", $dsp_pot_part_tool);


   $dsp_pot_part_tool .= 
   "<tr>".
   "<th colspan=\"4\"><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-POT comment headline');\">POT comment</span></strong></th>".
   "</tr>";
   $dsp_pot_part_tool .=
   "<tr>".
   "<td class=\"descr\" style=\"width:100%;\" colspan=\"4\">".
   "<div class=\"save_info\" id=\"info_pot_comment\">DATA SAVED</div>".
   create_field_pot("pot_comment", rawurldecode($tc_data["pot"]["pot_comment"]["value"]), $tc_data["pot"]["pot_comment"]["type"], "handle_save_single_field('".$tc_data["pot"]["unid"]."', this.name, escape(this.value), escape('handle_field(\''+this.name+'\')'))").
   "</td>".
   "</tr>".
   "</table>";

?>