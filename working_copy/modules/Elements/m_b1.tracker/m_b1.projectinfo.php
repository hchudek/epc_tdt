<?php

function m_b1_projectinfo($_application, $tc_data) {


   $app="epc/tdtracking/pot";
   $path="file:///d:/Lotus/phptemp/".$app;
   $handle=opendir($path);
   while($file=readdir($handle)) if(strpos($file,".php")>0) {include($path."/".$file);}
   closedir ($handle);

   for($i = 0; $i < count($data); $i++) {
      if(strtolower($tc_data["project"]["number"]) == strtolower($data[$i]["filter2"])) $f_data[] = $data[$i]; 
   }

   $html = 
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px; table-layout:fixed;\">".
   "<thead>".
   "<tr>".
   "<th colspan=\"3\"><img id=\"m_b1_projectinfo_section_1_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_1')\" />Project data</th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_1\" style=\"display:table-row-group;\">".
   "<tr>".
   "<td class=\"td_reg\">Project number</td>".
   "<td class=\"td_reg\" colspan=\"2\"><span id=\"project_number\">".$tc_data["project"]["number"]."</span></td>".
   "</tr>".
   "<tr>".
   "<td class=\"td_alt\">Description</td>".
   "<td class=\"td_alt\" colspan=\"2\"><span id=\"project_descr\">".urldecode($tc_data["project"]["description"])."</span></td>".
   "</tr>".
   "<tr>".
   "<td class=\"td_reg\">Current Phase</td>".
   "<td class=\"td_reg\" colspan=\"2\">".$tc_data["project"]["phasedesc"]."</td>".
   "</tr>".
   "<tr>".
   "<td class=\"td_alt\">Manufacturing City</td>".
   "<td class=\"td_alt\" colspan=\"2\">".rawurldecode($tc_data["project"]["city"])."</td>".
   "</tr>".
   "<tr>".
   "<td class=\"td_reg\">Date PLMC</td>".
   "<td class=\"td_reg\" colspan=\"2\">".$tc_data["project"]["date_plmc"]."</td>".
   "</tr>".
   "</tbody>".
   "</table>\r\n".
 
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m_b1_projectinfo\" style=\"margin-bottom:6px;\">".
   "<thead>".
   "<tr>".
   "<th><img id=\"m_b1_projectinfo_section_2_img\" src=\"../../../../library/images/minus.jpg\" style=\"margin-right:7px;\" onclick=\"handle_section('m_b1_projectinfo_section_2')\" />Project number</th><th>Case number</th><th>Part number</th><th>Tool number</th>".
   "</tr>".
   "</thead>".
   "<tbody id=\"m_b1_projectinfo_section_2\" style=\"display:table-row-group;\">";

   foreach($f_data as $val) {
      $class = ($class == "td_reg") ? "td_alt" : "td_reg"; 
      $html .= 
      "<tr>".
      "<td class=\"".$class."\">".$val["Project number"]."</td>".
      "<td class=\"".$class."\">".$val["Case number"]."</td>".
      "<td class=\"".$class."\">".$val["Part number"]."</td>".
      "<td class=\"".$class."\">".$val["Tool number"]."</td>".
      "</tr>";
   }
   $html .=
   "</tbody>".
   "</table>\r\n";




   return str_replace("Array", "&nbsp;", $html);
}


?>