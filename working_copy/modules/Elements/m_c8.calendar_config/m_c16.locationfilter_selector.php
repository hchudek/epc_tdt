<?php

function m_c16__locationfilter_selector($_application, $filters) {


   foreach($filters as $filter) {
       if( $filter["type"] == "locationfilter" ){
	      $member[] = $filter["value"];
	    }
   }

   $_module_name = str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id = substr(basename(__FILE__),0,4);
   if(!isset($_SESSION["perpage"]["m_c8__show_resources"])) $_SESSION["perpage"]["m_c8__show_resources"] = "all";
   if(!isset($_SESSION["perpage"]["m_c8__show_resources_filter"])) $_SESSION["perpage"]["m_c8__show_resources_filter"] = 1;
   $img[0] = ($_SESSION["perpage"]["m_c8__show_resources"] == "all") ? "status-11.png" : "status-11-1.png";
   $img[1] = ($_SESSION["perpage"]["m_c8__show_resources"] != "all") ? "status-11.png" : "status-11-1.png";
 
   $get_img = ($_SESSION["perpage"]["m_c8__show_resources"] == "all") ? "hardware-46.png" : "red/hardware-46.png";
   $dsp = ($_SESSION["perpage"]["m_c8__show_resources_filter"] == "1") ? "block" : "none";
  
   
   $resource [] = array (
			"name" => "TE Dinkelsbuehl",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Dinkelsbuehl"
			 
	);
    $resource [] = array (
			"name" => "TE Esztergom",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Esztergom"
			 
	);
	$resource [] = array (
			"name" => "TE India",
			"filtertype" => "locationfilter",
			"filterformula" => "TE India"
			 
	);
	$resource [] = array (
			"name" => "TE Kurim",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Kurim"
			 
	);
    $resource [] = array (
			"name" => "TE Montcada",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Montcada"
			 
	);
	$resource [] = array (
			"name" => "TE Oostkamp",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Oostkampl"
			 
	);
	$resource [] = array (
			"name" => "TE San Salvo",
			"filtertype" => "locationfilter",
			"filterformula" => "TE San Salvo"
			 
	);	
	$resource [] = array (
			"name" => "TE Speyer",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Speyer"
			 
	);
    $resource [] = array (
			"name" => "TE Steinach",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Steinach"
			 
	);
	$resource [] = array (
			"name" => "TE Ukraine",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Ukraine"
			 
	);
	$resource [] = array (
			"name" => "TE Woert",
			"filtertype" => "locationfilter",
			"filterformula" => "TE Woert"
			 
	);		
   
   $headline = "Location";

   $module =  "<span id=\"location_list\" style=\"position:relative;\" allow_type=\"location_added\" ondrop=\"handle_drop(event, this.getAttribute('id'));\" ondragover=\"allow_drop(event);\">\r\n";
 
   $i = 0; 
   foreach($resource as $val) {
      $dsp = (!in_array($val["filtertype"], $member) ) ? "block" : "none";
      $i++;
      $module .= 
      "<div id=\"calendar_resource_location_".$i."\" draggable=\"true\" ondragstart=\"drag(event)\" type=\"locationfilter\" style=\"display:".$dsp.";\" class=\"resource\" filtertype=\"locationfilter\" filterformula=\"" . $val["filterformula"] . "\">".
 	  "<span style=\"position:relative;top:2px;\">".$val["filterformula"]."</span>\r\n".
      "</div>\r\n";
   }
   $module .= "</span>\r\n";

  return array($headline, $module);
  //return $module;

}


?>