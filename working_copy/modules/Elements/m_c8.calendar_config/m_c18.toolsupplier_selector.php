<?php

function m_c18__toolsupplier_selector($_application, $filters) {


   foreach($filters as $filter) {
       if( $filter["type"] == "toolsupplier" ){
	      $member[] = $filter["value"];
	    }
   }

   $_module_name = str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id = substr(basename(__FILE__),0,4);
   if(!isset($_SESSION["perpage"]["m_c8__show_resources"])) $_SESSION["perpage"]["m_c8__show_resources"] = "all";
   if(!isset($_SESSION["perpage"]["m_c8__show_resources_filter"])) $_SESSION["perpage"]["m_c8__show_resources_filter"] = 1;
   $img[0] = ($_SESSION["perpage"]["m_c8__show_resources"] == "all") ? "status-11.png" : "status-11-1.png";
   $img[1] = ($_SESSION["perpage"]["m_c8__show_resources"] != "all") ? "status-11.png" : "status-11-1.png";
 
   $get_img = ($_SESSION["perpage"]["m_c8__show_resources"] == "all") ? "hardware-46.png" : "red/hardware-46.png";
   $dsp = ($_SESSION["perpage"]["m_c8__show_resources_filter"] == "1") ? "block" : "none";
  
   
   $resource [] = array (
			"name" => "TE Dinkelsbuehl",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Dinkelsbuehl"
			 
	);
    $resource [] = array (
			"name" => "TE Esztergom",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Esztergom"
			 
	);
	$resource [] = array (
			"name" => "TE India",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE India"
			 
	);
	$resource [] = array (
			"name" => "TE Kurim",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Kurim"
			 
	);
    $resource [] = array (
			"name" => "TE Montcada",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Montcada"
			 
	);
	$resource [] = array (
			"name" => "TE Oostkamp",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Oostkampl"
			 
	);
	$resource [] = array (
			"name" => "TE San Salvo",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE San Salvo"
			 
	);	
	$resource [] = array (
			"name" => "TE Speyer",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Speyer"
			 
	);
    $resource [] = array (
			"name" => "TE Steinach",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Steinach"
			 
	);
	$resource [] = array (
			"name" => "TE Ukraine",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Ukraine"
			 
	);
	$resource [] = array (
			"name" => "TE Woert",
			"filtertype" => "toolsupplier",
			"filterformula" => "TE Woert"
			 
	);		
   
   $headline = "Tool supplier";

   $module =  "<span id=\"toolsupplier_list\" style=\"position:relative;\" allow_type=\"toolsupplier_added\" ondrop=\"handle_drop(event, this.getAttribute('id'));\" ondragover=\"allow_drop(event);\">\r\n";
 
   $i = 0; 
   foreach($resource as $val) {
      $dsp = (!in_array($val["filtertype"], $member) ) ? "block" : "none";
      $i++;
      $module .= 
      "<div id=\"calendar_resource_toolsupplier_".$i."\" draggable=\"true\" ondragstart=\"drag(event)\" type=\"toolsupplier\" style=\"display:".$dsp.";\" class=\"resource\" filtertype=\"toolsupplier\" filterformula=\"" . $val["filterformula"] . "\">".
 	  "<span style=\"position:relative;top:2px;\">".$val["filterformula"]."</span>\r\n".
      "</div>\r\n";
   }
   $module .= "</span>\r\n";

  return array($headline, $module);
  //return $module;

}


?>