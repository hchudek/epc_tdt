<?php

function m_c19__phase_selector($_application, $filters) {


   foreach($filters as $filter) {
       if( $filter["type"] == "phase" ){
	      $member[] = $filter["value"];
	    }
   }

   $_module_name = str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id = substr(basename(__FILE__),0,4);
   if(!isset($_SESSION["perpage"]["m_c8__show_resources"])) $_SESSION["perpage"]["m_c8__show_resources"] = "all";
   if(!isset($_SESSION["perpage"]["m_c8__show_resources_filter"])) $_SESSION["perpage"]["m_c8__show_resources_filter"] = 1;
   $img[0] = ($_SESSION["perpage"]["m_c8__show_resources"] == "all") ? "status-11.png" : "status-11-1.png";
   $img[1] = ($_SESSION["perpage"]["m_c8__show_resources"] != "all") ? "status-11.png" : "status-11-1.png";
 
   $get_img = ($_SESSION["perpage"]["m_c8__show_resources"] == "all") ? "hardware-46.png" : "red/hardware-46.png";
   $dsp = ($_SESSION["perpage"]["m_c8__show_resources_filter"] == "1") ? "block" : "none";
	$resource = process_xml($_SESSION["remote_domino_path_main"]."/v.get_phase?open&count=99999&function=xml:data");

   $headline = "Reportable Date";

   $module =  "<span id=\"phase_list\" style=\"position:relative;\" allow_type=\"phase_added\" ondrop=\"handle_drop(event, this.getAttribute('id'));\" ondragover=\"allow_drop(event);\">\r\n";

   $i = 0;
   foreach($resource["phase"] as $val) {
      $dsp = (!in_array(urldecode($val["name"]), $member) ) ? "block" : "none";
      $i++;
      $module .= 
      "<div id=\"calendar_resource_phase_".$i."\" draggable=\"true\" ondragstart=\"drag(event)\" type=\"phase\" style=\"display:".$dsp.";\" class=\"resource\" filtertype=\"phase\" filterformula=\"" . urldecode($val["name"]) . "\">".
     // "<img  src=\"http://phone.tycoelectronics.com/imgupload/pics/".$val["teid"]."_JpegPhoto.jpeg\" onError=\"nopic(this)\"\" style=\"cursor:pointer;vertical-align:middle;margin:4px; width: 16px; height: 16px\" />".
      "&nbsp;<span style=\"position:relative;top:2px;\">".urldecode($val["name"])."</span>\r\n".
      "</div>\r\n";
   }
   $module .=
   "</span>\r\n";

  return array($headline, $module);
  //return $module;

}


?>