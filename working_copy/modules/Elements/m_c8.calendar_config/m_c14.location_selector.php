<?php

function m_c14__location_selector($_application, $locations) {
 
	global $filterNames;
	
	foreach($locations as $location) {
			$member[] = $location["type"];
	}
	
 	$_module_name = str_replace ( ".php", "", substr ( basename ( __FILE__ ), 5, strlen ( basename ( __FILE__ ) ) ) );
	$_module_id = substr ( basename ( __FILE__ ), 0, 4 );
	if (! isset ( $_SESSION ["perpage"] ["m_c8__show_resources"] ))
		$_SESSION ["perpage"] ["m_c8__show_resources"] = "all";
	$img [0] = ($_SESSION ["perpage"] ["m_c8__show_resources"] == "all") ? "status-11.png" : "status-11-1.png";
	$img [1] = ($_SESSION ["perpage"] ["m_c8__show_resources"] != "all") ? "status-11.png" : "status-11-1.png";
	// $resource = process_xml($_SESSION["remote_domino_path_main"]."/v.get_hr?open&restricttocategory=".$_SESSION["perpage"]["m_c3__show_resources"]."&count=99999&function=xml:data");
	$resource [] = array (
			"name" => "Tool Number",
			"filtertype" => "toolnumber",
			"filterformula" => "toolnumber"
			 
	);
	$resource [] = array (
			"name" => "Part Name",
			"filtertype" => "partname",
			"filterformula" => "partname"
	
	);
	$resource [] = array (
			"name" => "Project Number",
			"filtertype" => "projectnumber",
			"filterformula" => "projectnumber"
	
	);
	$resource [] = array (
			"name" => "Supplier",
			"filtertype" => "supplier",
			"filterformula" => "supplier"
	
	);
	$resource [] = array (
			"name" => "Product Location G3",
			"filtertype" => "productlocationg3",
			"filterformula" => "productlocationg3"
	
	);
	
	/*$resource [] = array (
			"name" => "Enddatum",
			"filtertype" => "enddate",
			"filterformula" => "@Text(Form)"
	
	);*/
/*	$resource [] = array (
			"name" => "MilestoneX",
			"form" => "f.milestone" 
	);*/
	
	$headline = "Location";
	
	$module = "<span id=\"filter_list\">\r\n";
	
	$i = 0;
	foreach ( $resource as $val ) {
		$filterNames["location-".$val["filterformula"]] = $val["name"];
		$i ++;
		$dsp = (!in_array($val["filtertype"], $member) ) ? "block" : "none";
		$module .= "<div id=\"location_" . $i . "\" draggable=\"true\" ondragstart=\"drag(event)\" type=\"location\" class=\"task\" style=\"display:".$dsp."\" filtertype=\"".$val["filtertype"]."\" filterformula=\"".$val["filterformula"]."\" filtername=\"".$val["name"]."\">" . "<img src=\"http://phone.tycoelectronics.com/imgupload/pics/" . $val ["teid"] . "_JpegPhoto.jpeg\" onError=\"nopic(this)\"\" style=\"vertical-align:middle;margin:4px; width: 16px; height: 16px\" />" . "<span style=\"position:relative;top:2px;\">" . $val ["name"] . "</span>\r\n" . "</div>\r\n";
	}
	$module .= "</span>\r\n";
	
	return array (
			$headline,
			$module 
	);

}

?>