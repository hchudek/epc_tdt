<?php
function m_c9__calendar_filter_selector($_application, $filters) {

	foreach($filters as $filter) {
		if( $filter["type"] == "enddate" ||  $filter["type"] == "startdate"  ){
			$member[] = $filter["type"];
		}
		
	}
		
	$_module_name = str_replace ( ".php", "", substr ( basename ( __FILE__ ), 5, strlen ( basename ( __FILE__ ) ) ) );
	$_module_id = substr ( basename ( __FILE__ ), 0, 4 );
	if (! isset ( $_SESSION ["perpage"] ["m_c8__show_resources"] ))
		$_SESSION ["perpage"] ["m_c8__show_resources"] = "all";
	$img [0] = ($_SESSION ["perpage"] ["m_c8__show_resources"] == "all") ? "status-11.png" : "status-11-1.png";
	$img [1] = ($_SESSION ["perpage"] ["m_c8__show_resources"] != "all") ? "status-11.png" : "status-11-1.png";
	// $resource = process_xml($_SESSION["remote_domino_path_main"]."/v.get_hr?open&restricttocategory=".$_SESSION["perpage"]["m_c3__show_resources"]."&count=99999&function=xml:data");
	/*$resource [] = array (
			"name" => "Textfeld",
			"filtertype" => "text",
			"filterformula" => "@Text(Form)"
			 
	);*/
	$resource [] = array (
			"name" => "Start date",
			"filtertype" => "startdate",
			"filterformula" => "@Text(Form)"
	
	);
	$resource [] = array (
			"name" => "End date",
			"filtertype" => "enddate",
			"filterformula" => "@Text(Form)"
	
	);

	$headline = "Dates";
	
	$module = "<span id=\"filter_list\">\r\n";
	
	$i = 0;
	foreach ( $resource as $val ) {
		$i ++;
		$dsp = (!in_array($val["filtertype"], $member) ) ? "block" : "none";
		$module .= "<div id=\"filter_" . $i . "\" draggable=\"true\" ondragstart=\"drag(event)\" type=\"create_filter\" style=\"display:".$dsp."\" class=\"task\" filtertype=\"".$val["filtertype"]."\" filterformula=\"".$val["filterformula"]."\">" . "<img src=\"http://phone.tycoelectronics.com/imgupload/pics/" . $val ["teid"] . "_JpegPhoto.jpeg\" onError=\"nopic(this)\"\" style=\"vertical-align:middle;margin:4px; width: 16px; height: 16px\" />" . "<span style=\"position:relative;top:2px;\">" . $val ["name"] . "</span>\r\n" . "</div>\r\n";
	}
	$module .= "</span>\r\n";
	
	return array (
			$headline,
			$module 
	);
}

?>