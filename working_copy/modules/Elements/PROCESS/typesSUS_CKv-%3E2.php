<?php
$_application["process"]["is_active"]["SUS_DC_Ev->2"] = "1";
$_application["process"]["displayed_name"]["SUS_DC_Ev->2"] = "Die Ext. conv-kit";
$_application["process"]["prv_process"]["SUS_DC_Ev->2"] = rawurldecode("SUS_CKv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_DC_Ev->2"] = "";
$_application["process"]["active_next"]["SUS_DC_Ev->2"] = "";
$_application["process"]["active_button_text"]["SUS_DC_Ev->2"] = "";
$_application["process"]["active_date"]["SUS_DC_Ev->2"] = "g03980";
$_application["process"]["type"]["SUS_DC_Ev->2"] = "G04005:G04064:G04066:G04100:G05100:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_DC_Iv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_DC_Iv->2"] = "Die Int. conv-kit";
$_application["process"]["prv_process"]["SUS_DC_Iv->2"] = rawurldecode("SUS_CKv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_DC_Iv->2"] = "";
$_application["process"]["active_next"]["SUS_DC_Iv->2"] = "";
$_application["process"]["active_button_text"]["SUS_DC_Iv->2"] = "";
$_application["process"]["active_date"]["SUS_DC_Iv->2"] = "g03980";
$_application["process"]["type"]["SUS_DC_Iv->2"] = "G03994:G03997:G04002:G04004:G04006:G04007:G04064:G04066:G04100:G05100:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_MC_Ev->2"] = "1";
$_application["process"]["displayed_name"]["SUS_MC_Ev->2"] = "Mold Ext. conv-kit";
$_application["process"]["prv_process"]["SUS_MC_Ev->2"] = rawurldecode("SUS_CKv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_MC_Ev->2"] = "";
$_application["process"]["active_next"]["SUS_MC_Ev->2"] = "";
$_application["process"]["active_button_text"]["SUS_MC_Ev->2"] = "";
$_application["process"]["active_date"]["SUS_MC_Ev->2"] = "g03980";
$_application["process"]["type"]["SUS_MC_Ev->2"] = "G04005:G04055:G04100:G04150:G04200:G04250:G05100:G05105:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_MC_Iv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_MC_Iv->2"] = "Mold Int. conv-kit";
$_application["process"]["prv_process"]["SUS_MC_Iv->2"] = rawurldecode("SUS_CKv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_MC_Iv->2"] = "";
$_application["process"]["active_next"]["SUS_MC_Iv->2"] = "";
$_application["process"]["active_button_text"]["SUS_MC_Iv->2"] = "";
$_application["process"]["active_date"]["SUS_MC_Iv->2"] = "g03980";
$_application["process"]["type"]["SUS_MC_Iv->2"] = "G04002:G04003:G04004:G04007:G04100:G04150:G04200:G04250:G05100:G05105:G05190:G05220:G05240:G05260:G05280:G05300";
foreach(array_keys($_application["process"]["type"]) As $key) {
   if(!is_array($_application["process"]["type"][$key])) {
      $_application["process"]["type"][$key] = explode(":", $_application["process"]["type"][$key]);
   }
}
?>
