<?php
$_application["process"]["is_active"]["SUS_D_ENv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_D_ENv->2"] = "Die Ext. / Ext. no transfer diff";
$_application["process"]["prv_process"]["SUS_D_ENv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_D_ENv->2"] = "";
$_application["process"]["active_next"]["SUS_D_ENv->2"] = "";
$_application["process"]["active_button_text"]["SUS_D_ENv->2"] = "";
$_application["process"]["active_date"]["SUS_D_ENv->2"] = "g03980";
$_application["process"]["type"]["SUS_D_ENv->2"] = "G04005:G04040:G04050:G04064:G04066:G04100:G05100:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_D_ETv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_D_ETv->2"] = "Die Ext. w. transfer";
$_application["process"]["prv_process"]["SUS_D_ETv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_D_ETv->2"] = "";
$_application["process"]["active_next"]["SUS_D_ETv->2"] = "";
$_application["process"]["active_button_text"]["SUS_D_ETv->2"] = "";
$_application["process"]["active_date"]["SUS_D_ETv->2"] = "g03980";
$_application["process"]["type"]["SUS_D_ETv->2"] = "G04005:G04040:G04050:G04064:G04066:G04100:G05030:G05035:G05040:G05050:G05055:G05060:G05100:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_D_INv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_D_INv->2"] = "Die Int. / Int. no transfer diff";
$_application["process"]["prv_process"]["SUS_D_INv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_D_INv->2"] = "";
$_application["process"]["active_next"]["SUS_D_INv->2"] = "";
$_application["process"]["active_button_text"]["SUS_D_INv->2"] = "";
$_application["process"]["active_date"]["SUS_D_INv->2"] = "g03980";
$_application["process"]["type"]["SUS_D_INv->2"] = "G03994:G03997:G04002:G04004:G04006:G04007:G04064:G04066:G04100:G05100:G05190:G05220:G05240:G05260:G05280:G05300:G04006 ";
$_application["process"]["is_active"]["SUS_D_ITv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_D_ITv->2"] = "Die Int. w. transfer";
$_application["process"]["prv_process"]["SUS_D_ITv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_D_ITv->2"] = "";
$_application["process"]["active_next"]["SUS_D_ITv->2"] = "";
$_application["process"]["active_button_text"]["SUS_D_ITv->2"] = "";
$_application["process"]["active_date"]["SUS_D_ITv->2"] = "g03980";
$_application["process"]["type"]["SUS_D_ITv->2"] = "G03994:G03997:G04002:G04004:G04006:G04007:G04064:G04066:G04100:G05030:G05035:G05040:G05050:G05055:G05060:G05100:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_M_ENv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_M_ENv->2"] = "Mold Ext. / Ext. no transfer";
$_application["process"]["prv_process"]["SUS_M_ENv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_M_ENv->2"] = "";
$_application["process"]["active_next"]["SUS_M_ENv->2"] = "";
$_application["process"]["active_button_text"]["SUS_M_ENv->2"] = "";
$_application["process"]["active_date"]["SUS_M_ENv->2"] = "g03980";
$_application["process"]["type"]["SUS_M_ENv->2"] = "G04005:G04020:G04030:G04040:G04050:G04055:G04100:G04150:G04200:G04250:G05005:G05007:G05010:G05020:G05100:G05105:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_M_ETv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_M_ETv->2"] = "Mold Ext. w. transfer";
$_application["process"]["prv_process"]["SUS_M_ETv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_M_ETv->2"] = "";
$_application["process"]["active_next"]["SUS_M_ETv->2"] = "";
$_application["process"]["active_button_text"]["SUS_M_ETv->2"] = "";
$_application["process"]["active_date"]["SUS_M_ETv->2"] = "g03980";
$_application["process"]["type"]["SUS_M_ETv->2"] = "G04005:G04020:G04030:G04040:G04050:G04055:G04100:G04150:G04200:G04250:G05005:G05007:G05010:G05020:G05060:G05080:G05100:G05105:G05120:G05180:G05190:G05220:G05240:G05260:G05280:G05300:G06990";
$_application["process"]["is_active"]["SUS_M_INv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_M_INv->2"] = "Mold Int. / Int. no transfer";
$_application["process"]["prv_process"]["SUS_M_INv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_M_INv->2"] = "";
$_application["process"]["active_next"]["SUS_M_INv->2"] = "";
$_application["process"]["active_button_text"]["SUS_M_INv->2"] = "";
$_application["process"]["active_date"]["SUS_M_INv->2"] = "g03980";
$_application["process"]["type"]["SUS_M_INv->2"] = "G04002:G04003:G04004:G04007:G04100:G04150:G04200:G04250:G05005:G05007:G05020:G05100:G05105:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_M_ITv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_M_ITv->2"] = "Mold Int. w. transfer";
$_application["process"]["prv_process"]["SUS_M_ITv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_M_ITv->2"] = "";
$_application["process"]["active_next"]["SUS_M_ITv->2"] = "";
$_application["process"]["active_button_text"]["SUS_M_ITv->2"] = "";
$_application["process"]["active_date"]["SUS_M_ITv->2"] = "g03980";
$_application["process"]["type"]["SUS_M_ITv->2"] = "G04002:G04003:G04004:G04007:G04100:G04150:G04200:G04250:G05005:G05007:G05020:G05060:G05080:G05100:G05105:G05120:G05180:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_S_ENv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_S_ENv->2"] = "Silicon Ext. / Ext. no transfer";
$_application["process"]["prv_process"]["SUS_S_ENv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_S_ENv->2"] = "";
$_application["process"]["active_next"]["SUS_S_ENv->2"] = "";
$_application["process"]["active_button_text"]["SUS_S_ENv->2"] = "";
$_application["process"]["active_date"]["SUS_S_ENv->2"] = "g03980";
$_application["process"]["type"]["SUS_S_ENv->2"] = "G04005:G04020:G04030:G04055:G04100:G04150:G04200:G04250:G05100:G05190:G05220:G05240:G05260:G05280:G05300";
$_application["process"]["is_active"]["SUS_S_ETv->2"] = "1";
$_application["process"]["displayed_name"]["SUS_S_ETv->2"] = "Silicon Ext. w. transfer";
$_application["process"]["prv_process"]["SUS_S_ETv->2"] = rawurldecode("SUSv-%3E2");
$_application["process"]["bind_to_rule"]["SUS_S_ETv->2"] = "";
$_application["process"]["active_next"]["SUS_S_ETv->2"] = "";
$_application["process"]["active_button_text"]["SUS_S_ETv->2"] = "";
$_application["process"]["active_date"]["SUS_S_ETv->2"] = "g03980";
$_application["process"]["type"]["SUS_S_ETv->2"] = "G04005:G04020:G04030:G04055:G04100:G04150:G04200:G04250:G05060:G05080:G05100:G05190:G05220:G05240:G05260:G05280:G05300";
foreach(array_keys($_application["process"]["type"]) As $key) {
   if(!is_array($_application["process"]["type"][$key])) {
      $_application["process"]["type"][$key] = explode(":", $_application["process"]["type"][$key]);
   }
}
?>
