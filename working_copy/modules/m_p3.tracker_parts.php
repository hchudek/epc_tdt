<?php


function m_p3__tracker_parts($_application) {

   $load = array("project");
   require_once("addin/load_tracker_data.php");
   $project = create_tc_data($load);
   $_SESSION["project"]  = $project["project"]["number"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = ($_REQUEST["unique"] == "") ? "No tracker selected" : "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";

   return array($headline, $module);

}


function m_p3__tracker_parts_generate_html($_application) {

   global $tc_data;

   $html = 
   "<table id=\"tbl_m_p3__tracker_parts\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<th>Part Info</th>\r\n".
   "<th>Part No.</th>\r\n".
   "<th>Part Desc.</th>\r\n".
   "<th>Loop</th>\r\n".
   "<th>Process Type</th>\r\n".
   "<th>Supplier</th>\r\n".
   "</tr>\r\n".
   "<tr>\r\n<td style=\"height:4px;\" colspan=\"6\"></td>\r\n</tr>\r\n";

   if(count($tc_data["project"]["selparts"]["number"]) <= 1) {
   $html .="<tr>\r\n".
      "<td>".str_replace('href="http://partinquiry.us.tycoelectronics.com', 'target="_blank" href="http://partinquiry.us.tycoelectronics.com', rawurldecode($tc_data["project"]["selparts"]["pic"]))."</td>\r\n".		
      "<td><a class=\"linkdata\" href=\"edit_tracker.php?&unique=".$tc_data["tracker"]["unique"]."&pot=".$tc_data["project"]["selparts"]["partunique"]."\" >".$tc_data["project"]["selparts"]["number"]."<a></td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["selparts"]["name"])."</td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["selparts"]["loop"])."</td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["selparts"]["ptype"])."</td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["selparts"]["supplier"])."</td>\r\n".
      "</tr>\r\n";
   } 
   else {
      for($i = 0; $i < count($tc_data["project"]["selparts"]["number"]); $i++) {
         $dsp = explode("/", str_replace("TN ", "", str_replace("PN ", "", $tc_data["tracker"]["pot_dsp_name"][$i])));
         $html .="<tr>\r\n".
         "<td>".str_replace('href="http://partinquiry.us.tycoelectronics.com', 'target="_blank" href="http://partinquiry.us.tycoelectronics.com', urldecode($tc_data["project"]["selparts"]["pic"][$i]))."</td>\r\n".
         "<td><a class=\"linkdata\" target=\"_blank\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/0/".$tc_data["project"]["selparts"]["part_unid"][$i]."?open\">".$tc_data["project"]["selparts"]["number"][$i]."<a></td>\r\n".
      //"<td><a class=\"linkdata\" href=\"edit_tracker.php?&unique=".$tc_data["tracker"]["unique"]."&pot=".$tc_data["project"]["selparts"]["partunique"][$i]."\" >".$tc_data["project"]["selparts"]["number"][$i]."<a></td>\r\n".
         "<td>".urldecode($tc_data["project"]["selparts"]["name"][$i])."</td>\r\n".
         "<td>".urldecode($tc_data["project"]["selparts"]["loop"][$i])."</td>\r\n".
         "<td>".urldecode($tc_data["project"]["selparts"]["ptype"][$i])."</td>\r\n".
         "<td>".urldecode($tc_data["project"]["selparts"]["supplier"][$i])."</td>\r\n".
         "</tr>\r\n";
      }
   }

   $html .= "</table>\r\n";

   return $html;
}



?>