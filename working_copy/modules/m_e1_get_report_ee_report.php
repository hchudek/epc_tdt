<?php

require_once("m_e1.ee_report.php");

function m_e1_get_report_ee_report($_application) {
   $app_id = $_application["page_id"];
   $function = strtolower(__FUNCTION__);
   $_application["page_id"] = str_replace("m_e1_get_report_", "", $function);
   $content = m_e1_ee_report($_application);
   $_application["page_id"] = $app_id;
   return $content;
}
