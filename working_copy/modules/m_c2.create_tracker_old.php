<?php

include_once("m_c1.epc_projects_view_search.php");
include_once("m_c1.epc_projects_view.php");
include_once("../../../library/tools/addin_xml.php");
include_once("Elements/m_b1.tracker/m_b1.projectinfo.php");

function m_c2_create_tracker($_application) {

   $module = "<form name=\"create_tracker\" method=\"post\" style=\"display:none;\"><input type=\"text\" name=\"ref_project\" value=\"".$_POST["ref_project"]."\" /><input type=\"\" name=\"project_number\" value=\"".$_POST["project_number"]."\" ></form>";

   if($_POST["ref_project"]) {
      $app="epc/projects";
      $path="file:///d:/Lotus/phptemp/".$app;
      $handle=opendir($path);
      while($file=readdir($handle)) if(strpos($file,".php")>0) {include_once($path."/".$file);}
      closedir ($handle);

      $i = 0; while($project_number == "" && $i < count($data)) {
         if($data[$i]["unid"] == $_POST["ref_project"]) $project_number = $data[$i]["Project No."];
         $i++;
      }


      // START: PROJEKTDATEN AUS LEANPD EINLESEN
      if(isset($_POST["project_number"]) && $_POST["project_number"] != "") $project_number = $_POST["project_number"];

      $xml = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/f.get_project_data_xml?readform&project=".urlencode($project_number));
      $xml_obj = simplexml_load_string($xml);
      $project_xml = convert_xml_into_array($xml_obj);
      // ENDE: PROJEKTDATEN AUS LEANPD EINLESEN


      if($project_xml["@attributes"]["in_leanpd_feed"] == "false") {
         $p_num = file_get_authentificated_contents($_SESSION["remote_domino_path_leanpd"]."/v.get_plmc_projects?open&count=99999");
         $module .= "<p style=\"display:none;\">".$p_num."</p>".

         "<span style=\"font:normal 18px century gothic,verdana;\"><b id=\"p_num_new\">".$project_number. "</b>&nbsp;not found in LEANPD FEED<br /><br /></span>".

         "<span style=\"font:normal 13px century gothic,verdana;\">You can either <a href=\"?\" style=\"text-decoration:none;color:rgb(0,102,158);\">start new search</a></span>".
         "<br /><br /><span style=\"font:normal 13px century gothic,verdana;\">create a <a style=\"text-decoration:none;color:rgb(0,102,158);\" href=\"javascript:void(0);\" onclick=\"document.getElementsByName('create_new_project')[0].style.display='block';document.getElementsByName('create_new_project')[0].style.height = '520px';\">standalone project</a> or</span><br /><br />".
         "<span style=\"font:normal 13px century gothic,verdana;\">create a new project and connect it to a LEAN PD project. Enter LEANPD Project number:</span>&nbsp;<span style=\"position:relative;top:1px;height:18px;font:normal 13px century gothic,verdana\"><input type=\"text\" name=\"p_num\" value=\"\" onkeyup=\"check_pnum();\" style=\"width:200px;border:solid 1px rgb(99,99,99);\" /><img onclick=\"void(0)\" name=\"p_num_img\" src=\"../../../library/images/16x16/status-11-1.png\" style=\"border:none;margin-left:7px;position:relative;top:2px;\" /></span><br /><br />".
         "<iframe name=\"create_new_project\" scrolling=\"no\" frameborder=\"0\" src=\"".$_SESSION["remote_domino_path_leanpd"]."/f.w.eng_project?open&EXTERNAL_PRJ_MGE_SYSTEM_PRJ_ID=".$project_number."\" style=\"width:100%;height:1px;position:relative;left:-8px;display:none;\"></iframe>";
         unset($_POST);
      }
      else {
         $module = 
         "<span class=\"phpbutton\"><a href=\"javascript:void(0)\" onclick=\"b=document.getElementsByTagName('body')[0];b.style.visibility='hidden';create_new_tracker();\">Create tracker</a></span>".
         "<span class=\"phpbutton\"><a href=\"?\">Start new search</a></span><br />".
         "<div>&nbsp;</div>".
         m_b1_projectinfo($_application, $project_xml);
      }
   }
   else {
      $module .= m_c1_epc_projects_view_search($_application).m_c1_epc_projects_view($_application, "javascript:preview_tracker('%%UNID%%');");
   }
   return($module);

}

?>