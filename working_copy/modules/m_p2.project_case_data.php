<?php


function m_p2__project_case_data($_application) {

   $load = array("project");
   require_once("addin/load_tracker_data.php");
   $project = create_tc_data($load);
   $_SESSION["project"]  = $project["project"]["number"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode("<span form=\"p2.prj\" style=\"display:none;\">".$_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]." for</form>");

   // Module Body --------------------------------------------------------------------------------------------------
   $module = ($_REQUEST["unique"] == "") ? "No tracker selected" : "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";

   return array($headline, $module);

}


function m_p2__project_case_data_generate_html($_application) {


   global $tc_data;

   $html = 
   "<table id=\"tbl_m_p2__project_case_data\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<th>Number</th>\r\n".
   "<th>Name</th>\r\n".
   "<th>Date issued</th>\r\n".
   "<th>Status</th>\r\n".
   "<th>Initiator</th>\r\n".
   "<th>Customer</th>\r\n".
   "<th>T&PM</th>\r\n".
   "<th>PE Manager</th>\r\n".
   "</tr>\r\n".
   "<tr>\r\n<td style=\"height:4px;\" colspan=\"8\"></td>\r\n</tr>\r\n";

   if(count($tc_data["project"]["case"]["number"]) == 1) {
      $html .="<tr>".
      "<td>\r\n<a href=\"".$_SESSION["remote_domino_path_epcdis"]."/v.byno/".$tc_data["project"]["case"]["number"]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["project"]["case"]["number"]."</a></td>\r\n".
      "<td >".rawurldecode($tc_data["project"]["case"]["name"])."</td>\r\n";
      if($tc_data["project"]["case"]["issued"] == "01.01.1900") {
         $html .= "<td>\r\nNo date</td>\r\n";
      }
      else {
         $html .= "<td>".$tc_data["project"]["case"]["issued"]."</td>\r\n";
      }
      $html .= "<td>".$tc_data["project"]["case"]["status"]."</td>\r\n".
      "<td>\r\n".m_p2__project_case_data_get_person(rawurldecode($tc_data["project"]["case"]["initiator"]))."</td>\r\n".
      "<td>\r\n".rawurldecode($tc_data["project"]["case"]["customer"])."</td>\r\n".
      "<td>\r\n".m_p2__project_case_data_get_person(rawurldecode($tc_data["project"]["case"]["tpm"]))."</td>\r\n".
      "<td>\r\n".m_p2__project_case_data_get_person(rawurldecode($tc_data["project"]["case"]["pe"]))."</td>\r\n".
      "</tr>";
    }
   else {
      for($i = 0; $i < count($tc_data["project"]["case"]["number"]); $i++) {
         $html .="<tr>".
         "<td>\r\n<a href=\"".$_SESSION["remote_domino_path_epcdis"]."/v.byno/".$tc_data["project"]["case"]["number"][$i]."?open\" class=\"linkdata\" target=\"_blank\">".$tc_data["project"]["case"]["number"][$i]."</a></td>\r\n".
         "<td>\r\n".rawurldecode($tc_data["project"]["case"]["name"][$i])."</td>\r\n";
         if($tc_data["project"]["case"]["issued"][$i]=="01.01.1900") {
            $html .= "<td>No date</td>\r\n";
         }
         else {
            $html .= "<td>".$tc_data["project"]["case"]["issued"]."</td>\r\n";
         }
         $html .= "<td>".$tc_data["project"]["case"]["status"][$i]."</td>\r\n".
         "<td>\r\n".m_p2__project_case_data_get_person(rawurldecode($tc_data["project"]["case"]["initiator"][$i]))."</td>\r\n".
         "<td>\r\n".rawurldecode($tc_data["project"]["case"]["customer"][$i])."</td>\r\n".
         "<td>\r\n".m_p2__project_case_data_get_person(rawurldecode($tc_data["project"]["case"]["tpm"][$i]))."</td>\r\n".
         "<td>\r\n".m_p2__project_case_data_get_person(rawurldecode($tc_data["project"]["case"]["pe"][$i]))."</td>\r\n".
         "</tr>";
      }
   }

   $html .= 
   "</table>\r\n";

   return $html;
}


function m_p2__project_case_data_get_person($name) {
   global $tc_data;
   $sn = "";
   foreach($tc_data["project"]["nab"]["name"] as $val) {
      if(rawurldecode($val["cn"]) == $name) {
         $sn = strtolower($val["sn"]);
         break;
      }
   }

   $r = 
   "<span style=\"position:absolute;\">".
   "<img src=\"http://phone.tycoelectronics.com/EmployeeProfiles/imgupload/pics/".$sn."_JpegPhoto.jpeg\" onError=\"nopic(this)\">".
   "</span>".
   "<a style=\"padding-left:32px;\" href=\"http://phone.tycoelectronics.com/Default.aspx?s=".$sn."\" target=\"_blank\">".$name."</a>\r\n";
 			
   return $r;
}








?>