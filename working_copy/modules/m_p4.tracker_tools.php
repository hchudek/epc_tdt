<?php


function m_p4__tracker_tools($_application) {

   $load = array("project");
   require_once("addin/load_tracker_data.php");
   $project = create_tc_data($load);
   $_SESSION["project"]  = $project["project"]["number"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = ($_REQUEST["unique"] == "") ? "No tracker selected" : "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";

   return array($headline, $module);

}


function m_p4__tracker_tools_generate_html($_application) {

   global $tc_data;

   $html = 
   "<table id=\"tbl_m_p4__tracker_tools\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<th>Tool No.</th>\r\n".
   "<th>Tool Desc.</th>\r\n".
   "<th>Supplier</th>\r\n".
   "<th>PO</th>\r\n".
   "<th>Position</th>\r\n".
   "</tr>\r\n".
   "<tr>\r\n<td style=\"height:4px;\" colspan=\"5\"></td>\r\n</tr>\r\n";

   if(count($tc_data["project"]["seltools"]["number"]) <= 1) {
      $html .="<tr>\r\n".		
      "<td>".rawurldecode($tc_data["project"]["seltools"]["number"])."</td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["seltools"]["name"])."</td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["seltools"]["supplier"])."</td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["seltools"]["saplink"])."&nbsp;".$tc_data["project"]["seltools"]["pono"]."</td>\r\n".
      "<td>".rawurldecode($tc_data["project"]["seltools"]["position"])."</td>\r\n".
      "</tr>\r\n";
   }
   else {
      for($i = 0; $i < count($tc_data["project"]["seltools"]["number"]); $i++) {
         $dsp = explode("/", str_replace("TN ", "", str_replace("PN ", "", $tc_data["tracker"]["pot_dsp_name"][$i])));
         $html .="<tr>\r\n".		
         "<td>".rawurldecode($tc_data["project"]["seltools"]["number"][$i])."</td>\r\n".
         "<td>".rawurldecode($tc_data["project"]["seltools"]["name"][$i])."</td>\r\n".
         "<td>".rawurldecode($tc_data["project"]["seltools"]["supplier"][$i])."</td>\r\n".
         "<td>".rawurldecode($tc_data["project"]["seltools"]["saplink"][$i])."<span style=\"padding-left:24px;\">".$tc_data["project"]["seltools"]["pono"][$i]."</span></td>\r\n".
         "<td>".rawurldecode($tc_data["project"]["seltools"]["position"][$i])."</td>\r\n".
	 "</tr>\r\n";
      }
   }

   $html .= "</table>\r\n";


   return $html;

}



?>