<?php

function m_r3__accounting_report_search($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------
   $module = m_r3__accounting_report_search_generate_html($_application);

   return array($headline, $module);

}

function m_r3__accounting_report_search_generate_html($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
    
   $value = ($_REQUEST[$_module_id."_q"] == "") ? $_REQUEST[$_module_id."_key"] : $_REQUEST[$_module_id."_q"];


   if($_REQUEST["m_r3_add"] == "") {
      $module = "";
   }

   else {
     
      $module = 
      "<table id =\"tbl_m_e1__ee_report_search\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n".
      "<tr>\r\n".
      "<td style=\"height:24px; vertical-align:top;\"><input type=\"text\" name=\"".$_module_id."_q\" value=\"".$value."\" onkeyup=\"search.checkevent(this, event);\"></td>\r\n".
      "<td style=\"height:24px; vertical-align:top;\"><span class=\"phpbutton\" style=\"width:67px; text-align:center;\"><a href=\"javascript:search.view('".$_module_id."_q');\">Search</a></span></td>\r\n".
      "</tr>\r\n".
      "<tr>\r\n".
      "<td id=\"".$_module_id."_select\" style=\"height:24px; vertical-align:top;\"></td>\r\n".
      "<td style=\"height:24px; vertical-align:top;\"><span class=\"phpbutton\" style=\"width:67px; text-align:center;\"><a href=\"javascript:search.lookup.execute('".$_module_id."');\">Lookup</a></span></td>\r\n".  
      "</tr>\r\n".
      "<tr>\r\n".
      "<td colspan=\"2\"><span class=\"phpbutton\" style=\"width:67px; text-align:center;\"><a href=\"javascript:search.clear('".$_module_id."_q');\">Clear</a></span></td>\r\n".
      "</tr>\r\n".
      "</table>\r\n";
   }

   $module = str_replace("%%TABS%%", $tabs, $module);

   return $module;

}

?>