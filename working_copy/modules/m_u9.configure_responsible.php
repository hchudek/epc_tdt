<?php


function m_u9__configure_responsible($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = m_u9__configure_responsible_generate_html($_application);
   return array($headline, $module);

}


function m_u9__configure_responsible_generate_html($_application) {
 
   $file = process_xml($_SESSION["remote_domino_path_main"]."/v.responsibilities?open&count=99999&function=xml:data");

   $module = 
   "<table id=\"tbl_m_u9__configure_responsible\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n".
   "<tr>\r\n".
   "<td><span class=\"phpbutton\" anchor=\"activator\" style=\"visibility:xhidden;\"><a href=\"javascript:m_u9__configure_responsible.activate(true);\">Activate changes</a></span><br><br></td>\r\n".
   "<tr>\r\n".
   "<td>\r\n".
   "<input type=\"text\" value=\"\" onkeyup=\"m_u9__configure_responsible.search(this)\">".
   "<div id=\"m_u9__userlist\">\r\n".
   "<table id=\"tbl_m_u9__userlist\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n";
   foreach($file["user"] as $user) {
      $module .= 
      "<tr>\r\n".
      "<td unid=\"".$user["@attributes"]["unid"]. "\" username=\"".rawurldecode($user["username"])."\" subprocess=\"".rawurldecode($user["subprocess"])."\" h_next=\"".rawurldecode($user["h_next"])."\" h_previous=\"".rawurldecode($user["h_previous"])."\" onclick=\"m_u9__configure_responsible.edit(this)\">".rawurldecode($user["@attributes"]["shortname"])."</td>\r\n".
      "</tr>\r\n";
   }
   $module .= 
   "</table>\r\n".
   "</div>\r\n".
   "</td>\r\n".
   "<td>\r\n";
   $arr = array();
//   foreach($file["user"] as $subprocess) if(trim($subprocess["subprocess"]) != "") {
//      $exp = explode(":", rawurldecode($subprocess["subprocess"]));
//      foreach($exp as $add) if(!in_array($add, $res)) $res[] = $add;  
//   }    

//   sort($res);

   $xml = process_xml($_SESSION["remote_domino_path_main"]."/v.rule?open&count=99999&function=xml:data");
   foreach($xml["responsible"] as $v) $res[] = rawurldecode($v);
   $res = array_unique($res);

   foreach($res as $val) $module .= "<subprocess>".$val."</subprocess>\r\n";  
   $module .= 
   "<span id=\"tbl_m_u9__configure_responsible_form\"></span></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n";

   return str_replace("Array", "", $module);

}



?>