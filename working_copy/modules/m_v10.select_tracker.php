<?php

function m_v10__select_tracker($_application) {

   global $tc_data;

   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sortkey"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sortkey"] = "2";

   $img[0] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sortkey"] == "2") ? "chevron2-desc" : "chevron2-reg";
   $img[1] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sortkey"] == "3") ? "chevron2-desc" : "chevron2-reg";
   $img[2] = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_sortkey"] == "4") ? "chevron2-desc" : "chevron2-reg";

   // Module Headline --------------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   $module .= 
   "<input style=\"font:normal 13px open sans; width:205px; height:19px; padding:1px; border:solid 1px rgb(153,153,153);\" type=\"text\" value=\"\" onkeyup=\"do_element_search(this.value.toLowerCase(), 'tbl_".__FUNCTION__."', 'tbody');\" />\r\n".
   "<table id=\"tbl_".__FUNCTION__."\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n".
   "<thead style=\"display:table-header-group;\">\r\n".
   "<tr>\r\n".
   "<th>Number</th>\r\n".
   "<th>Description</th>\r\n".
   "</tr>\r\n".
   "</thead>\r\n".
   "</table>\r\n".
   "<load>loading ...</load>\r\n";
   return array($headline, $module);

}

?>