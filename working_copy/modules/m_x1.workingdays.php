<?php

function m_x1_workingdays($_application) {

   $module_id = "m_x1";
   $t_year = (isset($_REQUEST[$module_id."_year"])) ? $_REQUEST[$module_id."_year"] : date("Y", time());
   $t_visibility = (isset($_REQUEST[$module_id."_visibility"])) ? $_REQUEST[$module_id."_visibility"] : "false";

   $go_past = 0;
   $go_future = 0;

   $start_date = mktime(0, 0, 0, 1, 1, intval($t_year) - $go_past);
   $end_date = mktime(0, 0, 0, 12, 31, intval($t_year) + $go_future);

   $this_date = $start_date;

   $i = 86400; while($this_date <= $end_date) {
      $is_weekday = (in_array(date("w", $this_date), explode(":", "0:6")) == 0) ? 1 : 0;
      $weekday[date("Y-m-d", $this_date)]["is_weekday"] = $is_weekday;
      $this_date += $i;
   }


   $c_start_date = mktime(0, 0, 0, 1, 1, intval($t_year));
   $c_end_date = mktime(0, 0, 0, 12, 31, intval($t_year));

   $c_this_date = $c_start_date;

   $txt = ($t_visibility == "false") ? "Show calendar" : "Hide calendar"; 
   $attr = ($t_visibility == "false") ? "true" : "false"; 

   $html = 
   "<div class=\"selector\">\r\n<img src=\"../../../../library/images/icons/calendar.gif\" style=\"position:relative;top:4px;margin-right:6px;filter:Gray();\" /><a name=\"selector\" href=\"?".str_replace("&".$module_id."_visibility=".$_REQUEST[$module_id."_visibility"], "", $_SERVER["QUERY_STRING"])."&".$module_id."_visibility=".$attr."\" style=\"text-decoration:none;color:rgb(0,102,158);\">".$txt."</a>\r\n</div>\r\n";

   if($t_visibility == "true") {


      $xml = file_get_contents($_SESSION["remote_domino_path_perportal"]."/v.get_workingdays?open&restricttocategory=".$_SESSION["remote_perportal_unid"]."&function=xml:data");
      $xml_obj = simplexml_load_string($xml);
      $xml_wd = convert_xml_into_array($xml_obj);


      $html .= 
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"tbl_big_calendar\"><tr>\r\n".
      "<tr><td colspan=\"6\"><span class=\"phpbutton\" id=\"save_changes\" style=\"display:none;\"><a href=\"javascript:save_changes();\">Save changes</a></span></td><td colspan=\"6\" class=\"year\">".
      "<a href=\"?".str_replace("&".$module_id."_year=".$t_year, "", $_SERVER["QUERY_STRING"])."&".$module_id."_year=".(intval($t_year) - 1)."\" style=\"margin-right:6px;\">-</a>".
      "<span id=\"calendar_year\">".$t_year."</span>".
      "<a href=\"?".str_replace("&".$module_id."_year=".$t_year, "", $_SERVER["QUERY_STRING"])."&".$module_id."_year=".(intval($t_year) + 1)."\" style=\"margin-left:6px;\">+</a>".
      "</td></tr>\r\n<tr>";

      for($m = 1; $m < 13; $m++) {
         $dsp_month = mktime(0, 0, 0, $m, 1, intval($t_year));
         $html .= "<th>".date("M", $dsp_month)."</th>\r\n";   
      }
      $html .= "</tr>\r\n";


      $value = "";
      $count = 0;
      for($d = 1; $d < 32; $d++) {
         $html .="<tr>";
         for($m = 1; $m < 13; $m++) {
            if(checkdate($m, $d, intval($t_year)) == true) {
               $count++;
               $calendar_date =  mktime(0, 0, 0, $m, $d, intval($t_year));
               $use_class = "";

               if(isset($xml_wd["td_wd_".$t_year])) {
                  if(substr($xml_wd["td_wd_".$t_year], $count - 1, 1) == "0" && $weekday[date("Y-m-d", $calendar_date)]["is_weekday"] == "1") $use_class = "no_workingday_individual";
                  $weekday[date("Y-m-d", $calendar_date)]["is_weekday"] = substr($xml_wd["td_wd_".$t_year], $count - 1, 1);
               } 
               if($use_class == "") $use_class = ($weekday[date("Y-m-d", $calendar_date)]["is_weekday"] == 1) ? "is_workingday" : "no_workingday";
               $cw = (date("w", $calendar_date) == 1) ? "<div style=\"position:absolute;font-size:32px;z-index:1;\">".date("W", $calendar_date)."</div>" : "";
               $html .= "<td class=\"bc_td_".$m."\"><div id=\"dte_".$m."_".$d."\" onclick=\"set_workingday('".$count."', this.id)\" class=\"".$use_class."\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding:0px;width:100%;margin:0px;\"><tr><td style=\"border:none;z-index:2;\">".date("d D", $calendar_date)."</td><td style=\"border:none;color:rgb(202,202,255);text-align:right;position:relative;top:-4px;left:-43px;\">".$cw."</td></tr></table></div></td>\r\n";
               $value .= $weekday[date("Y-m-d", $calendar_date)]["is_weekday"];
            }
            else $html .= "<td>&nbsp;</td>\r\n";
         }
         $html .= "</tr>\r\n";
      }

      $html .= 
      "</table>\r\n".
      "<a style=\"display:none;\"><form method=\"post\" name=\"big_calendar\"><input type=\"text\" name=\"workingdays\" value=\"".$value."\" /></form></a>\r\n";
   }
 
   return $html;

}


?>






