<?php

function m_c3__edit_tracker_small($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
//   $module = m_c3__edit_tracker_small_generate_html($_application);
//$module = "LOL";
   return array($headline, $module);

}

function m_c3__edit_tracker_small_generate_html($_application) {

   global $tc_data;

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Überschrift und Modulsonderzubehör erstellen
   $tabs="<img onclick=\"handle_fade_in_out('tbl_select_dsp_pot')\" src=\"../../../../library/images/icons/filter2.png\" style=\"border:none;position:relative;top:2px;filter:Gray();cursor:pointer;\" />&nbsp;&nbsp;<span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-basic data headline');\">Tracker data</span>";

   $img["tracker_type"][0] = ($tc_data["tracker"]["type"] == "0") ? "select_high" : "select_reg";
   $img["tracker_type"][1] = ($tc_data["tracker"]["type"] == "1") ? "select_high" : "select_reg";
   $img["tracker_type"][2] = ($tc_data["tracker"]["type"] == "2") ? "select_high" : "select_reg";

   $img["tracker_status"][0] = ($tc_data["tracker"]["status"] != "0") ? "select_high" : "select_reg";
   $img["tracker_status"][1] = ($tc_data["tracker"]["status"] == "0") ? "select_high" : "select_reg";


   $img[2] = ($tc_data["tracker"]["date_filter"] == "0") ? "select_high" : "select_reg";
   $img[3] = ($tc_data["tracker"]["date_filter"] == "1") ? "select_high" : "select_reg";

   $img[4] = ($tc_data["tracker"]["active"] == "1") ? "select_high" : "select_reg";
   $img[5] = ($tc_data["tracker"]["active"] == "0") ? "select_high" : "select_reg";

   foreach($tc_data["tracker"]["ref_pot"] as $pot) {
      $ref_pot[] = "\"".$pot."\"";
   }

   if(count($ref_pot) > 0) {
      $or = "(@ismember(td\$unique;@getdocfield(\"".$tc_data["tracker"]["unid"]."\";\"ref\$pot\"))) | ";
   }
   else {
      $or = "";
   }

   $responsible = (!is_array($tc_data["tracker"]["responsible"])) ? $tc_data["tracker"]["responsible"] : $tc_data["tracker"]["created_by_user"];
   $edit_resp = (in_array("[t_d]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"])) ? "<img src=\"../../../../library/images/16x16/edition-35.png\" style=\"position:absolute;left:96px;cursor:pointer;\" onclick=\"document.getElementById('change_responsible').style.display='block';\"/>" : "";
   $edit_tracker_type = (in_array("[t_d]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"])) ? "true" : "false";
   $edit_tracker_status = (in_array("[t_d]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"])) ? "true" : "false";


   $content=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tracker_data_small\">".
   "<tr><td colspan=\"2\" style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;background-color:rgb(255,194,54);\"><strong>Basic data</strong></td></tr>".
   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Tracker&nbsp;number&nbsp;&nbsp;</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">".$tc_data["tracker"]["number"]."</td></tr>".

"<!--".

   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Unique ID</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">".$tc_data["tracker"]["unique"]."&nbsp;</td></tr>".

"-->".

   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">T&PM".$edit_resp."</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\"><span id=\"tracker_responsible\">".$responsible."</span>".get_responsible($tc_data, $responsible)."&nbsp;</td></tr>".
   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Created by</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">".$tc_data["tracker"]["created_by_user"]."&nbsp;</td></tr>".
   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Created date</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">".format_date($tc_data["tracker"]["created_date"])."&nbsp;</td></tr>".
   "<tr><td colspan=\"2\"  style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;background-color:rgb(255,194,54);\"><strong>Tracking parameter</strong></td></tr>".

   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Tracker status</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">".
   "<a href=\"javascript:void(0);\" onclick=\"change_status('".rawurlencode("doc\$status")."','".rawurlencode("1")."', '".rawurlencode("(@lowercase(@text(@documentuniqueid))=\"".rawurlencode($tc_data["tracker"]["unid"])."\") | (ref\$tracker=\"".$tc_data["tracker"]["unique"]."\")")."&u=".$tc_data["tracker"]["unid"]."', ".$edit_tracker_status.");\" style=\"cursor:".str_replace("true", "pointer", str_replace("false", "default", $edit_tracker_status)).";\"><img name=\"tracker_status_img\" id=\"tracker_type_img_0\" src=\"../../../../library/images/icons/".$img["tracker_status"][0].".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /></a>Active<br />".
   "<a href=\"javascript:void(0);\" onclick=\"change_status('".rawurlencode("doc\$status")."','".rawurlencode("0")."', '".rawurlencode("(@lowercase(@text(@documentuniqueid))=\"".rawurlencode($tc_data["tracker"]["unid"])."\") | (ref\$tracker=\"".$tc_data["tracker"]["unique"]."\")")."&u=".$tc_data["tracker"]["unid"]."', ".$edit_tracker_status.");\" style=\"cursor:".str_replace("true", "pointer", str_replace("false", "default", $edit_tracker_status)).";\"><img name=\"tracker_status_img\" id=\"tracker_type_img_1\" src=\"../../../../library/images/icons/".$img["tracker_status"][1].".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /></a>Inactive<br />".
   "</td></tr>".



   "<tr><td colspan=\"2\" style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\"><div class=\"save_info\" id=\"info_tracker_descr\">DATA SAVED</div>Tracker description<br />".
   str_replace("type=\"text\"", "type=\"text\" style=\"width:206px;\"", create_field("tracker_descr", $tc_data["tracker"]["tracker_descr"]["value"], $tc_data["tracker"]["tracker_descr"]["type"], "handle_save_single_field('".$tc_data["tracker"]["unid"]."', this.name, this.value, escape('handle_field(\''+this.name+'\')'))")).
   "</td></tr>".



   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Tracker type</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">".
   "<a href=\"javascript:void(0);\" onclick=\"handle_save_single_field('".$tc_data["tracker"]["unid"]."','tracker_type','0', escape('handle_selectbox(\'tracker_type\', 0)'), $edit_tracker_type);\" style=\"cursor:".str_replace("true", "pointer", str_replace("false", "default", $edit_tracker_type)).";\"><img name=\"tracker_type_img\" id=\"tracker_type_img_0\" src=\"../../../../library/images/icons/".$img["tracker_type"][0].".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /></a>Pilot<br />".
   "<a href=\"javascript:void(0);\" onclick=\"handle_save_single_field('".$tc_data["tracker"]["unid"]."','tracker_type','1', escape('handle_selectbox(\'tracker_type\', 1)'), $edit_tracker_type);\" style=\"cursor:".str_replace("true", "pointer", str_replace("false", "default", $edit_tracker_type)).";\"><img name=\"tracker_type_img\" id=\"tracker_type_img_1\" src=\"../../../../library/images/icons/".$img["tracker_type"][1].".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /></a>Serial<br />".
   "<a href=\"javascript:void(0);\" onclick=\"handle_save_single_field('".$tc_data["tracker"]["unid"]."','tracker_type','2', escape('handle_selectbox(\'tracker_type\', 2)'), $edit_tracker_type);\" style=\"cursor:".str_replace("true", "pointer", str_replace("false", "default", $edit_tracker_type)).";\"><img name=\"tracker_type_img\" id=\"tracker_type_img_2\" src=\"../../../../library/images/icons/".$img["tracker_type"][2].".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /></a>Both".
   "</td></tr>".


   "<tr><td colspan=\"2\"  style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;background-color:rgb(255,194,54);\"><strong>Filter</strong></td></tr>".
   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Subprocesses</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">".
   "<a href=\"javascript:handle_save_single_field('".$tc_data["tracker"]["unid"]."','date_filter','0', escape('location.reload()'));\"><img name=\"date_filter_img\" id=\"date_filter_img_0\" src=\"../../../../library/images/icons/".$img[2].".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /></a>All<br />".
   "<a href=\"javascript:handle_save_single_field('".$tc_data["tracker"]["unid"]."','date_filter','1', escape('location.reload()'));\"><img name=\"date_filter_img\" id=\"date_filter_img_1\" src=\"../../../../library/images/icons/".$img[3].".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /></a>Gate specific".
   "</td></tr></table>";

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:220px;table-layout:fixed;margin-bottom:22px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_content\" colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   $module = "<b>kjllkjiohoubnknkjn</b>";

   return $module;

}


function get_responsible($tc_data, $responsible) {

   $html = "<select onchange=\"handle_save_single_field('".$tc_data["tracker"]["unid"]."', 'responsible', this.value, 'location.href=location.href');\" style=\"width:212px;font:normal 11px century gothic;height:19px;background-color:rgb(255,255,255);z-index:999999;filter:alpha(opacity=92);\">\r\n";
   foreach($tc_data["tracker"]["editors"]["name"] as $editor) {
      $selected = ($responsible == $editor) ? " selected" : ""; 
      $html .= "<option value=\"".$editor."\"".$selected.">".$editor."</option>\r\n";
   }   
   $html .= "</select>\r\n";

   return "<div id=\"change_responsible\" style=\"display:none;position:absolute;left:11px;\">".$html."</div>";
}



?>