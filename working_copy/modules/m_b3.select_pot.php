<?php

function m_b3_select_pot($_application) {

   global $tc_data;

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Überschrift und Modulsonderzubehör erstellen
   $tabs="<img onclick=\"handle_fade_in_out('tbl_select_dsp_pot')\" src=\"../../../../library/images/icons/filter2.png\" style=\"border:none;position:relative;top:2px;filter:Gray();cursor:pointer;\" />&nbsp;&nbsp;<span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Select active POT headline');\">Select active POT</span>";

  
   $content = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tbl_select_dsp_pot\" style=\"border:outset 2px;;width:700px;position:absolute;table-layout:fixed;z-index:9999;display:none;background-color:rgb(220,220,220)\">".  
   "<tr>".
   "<th style=\"width:20px;\">&nbsp;</th>".
   "<th style=\"width:80px;\"><strong>Part</strong></th>".
   "<th style=\"width:120px;\"><strong>Tool</strong></th>".
   "<th style=\"width:220px;\"><strong>Supplier</strong></th>".
   "</tr>\r\n";

   for($i = 0; $i < count($tc_data["tracker"]["pot_dsp_name"]); $i++) {
      $dsp = explode("/", str_replace("TN ", "", str_replace("PN ", "", $tc_data["tracker"]["pot_dsp_name"][$i])));
      if(trim($dsp[0]) == "") $dsp[0] = "&nbsp;";
      if(trim($dsp[1]) == "") $dsp[1] = "&nbsp;";

      if(in_array($tc_data["tracker"]["ref_pot"][$i], $tc_data["tracker"]["hide_pot"])) {
         $img = "select_reg";
         $sel = "0";
      }
      else {
         $img = "select_high";
         $sel = "1";
      }
      if($tc_data["tracker"]["pot_status"][$i] == "1") $content .= "<tr><td style=\"text-align:center;width:20px;\"><img onclick=\"do_select_dsp_pot(this);\" name=\"select_dsp_pot\" tool_supplier=\"".strtolower(htmlentities(urldecode($tc_data["tracker"]["pot_tool_supplier"][$i])))."\" tool_base=\"".substr(trim($dsp[1]),0,10)."\" is_selected=\"".$sel."\" value=\"".$tc_data["tracker"]["ref_pot"][$i]."\" src=\"../../../../library/images/icons/".$img.".png\" style=\"width:12px;\" /></td>".
      "<td style=\"width:80px;overflow:hidden;\"><nobr>".trim($dsp[0])."</nobr></td>".
      "<td style=\"width:120px;overflow:hidden;\"><nobr><a href=\"javascript:select_dsp_pot('".substr(trim($dsp[1]),0,10)."', 'tool_base');\" style=\"font:normal 12px century gothic, verdana;text-decoration:none;color:rgb(0,102,158);\">".substr(trim($dsp[1]),0,10)."</a>".substr(trim($dsp[1]),10,12)."</nobr></td>\r\n".
      "<td style=\"width:80px;overflow:hidden;\"><nobr><a href=\"javascript:select_dsp_pot('".strtolower(htmlentities(urldecode($tc_data["tracker"]["pot_tool_supplier"][$i])))."', 'tool_supplier');\" style=\"font:normal 12px century gothic, verdana;text-decoration:none;color:rgb(0,102,158);\">".htmlentities(urldecode($tc_data["tracker"]["pot_tool_supplier"][$i]))."</a></nobr></td></tr>";
//print htmlentities(urldecode($tc_data["tracker"]["pot_tool_supplier"][$i]))."qwerz"; 
  }



   $content .= "<tr><td colspan=\"3\" style=\"padding-left:4px;padding-top:5px;padding-bottom:5px;\">".
   "<span class=\"phpbutton\"><a href=\"javascript:do_select_dsp_pot(true);\">Select all</a></span>".
   "<span class=\"phpbutton\"><a href=\"javascript:do_select_dsp_pot(false);\">Deselect all</a></span>".
   "<span class=\"phpbutton\"><a href=\"javascript:save_select_dsp_pot('".$tc_data["tracker"]["unid"]."');\">Apply</a></span>".
   "</td></tr>";

   $content .= "</table>";


   $tc_data["tracker"]["pot_status_dsp"][] = "Active";
   $status = array_unique($tc_data["tracker"]["pot_status_dsp"]); sort($status);
   if(count($tc_data["tracker"]["status_filter"]) == 0) $tc_data["tracker"]["status_filter"] = "Active";
   $content2 .= "<table bordeRr\"0\" cellpdding=\"0\" cellspcing=\"0\">";
   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;\">Status</td><td style=\"padding:2px;border-bottom:solid 1px #ffffff;\">";
   $status_filter = explode(",", $tc_data["tracker"]["status_filter"]);
   foreach($status as $val) {
      $img = (in_array($val, $status_filter)) ? "select_high" : "select_reg";
      $content .= 
      "<div style=\"padding:2px;font:normal 12px century gothic,verdana;\"><img name=\"status_filter_img\" onclick=\"v = change_selected(this, 'get_selected_value'); handle_save_single_field('".$tc_data["tracker"]["unid"]."','status_filter', v, escape('location.href=(location.href.replace(/&status_filter=disable/,\'\')).replace(/pot=/,\'o=\')'));\" src=\"../../../../library/images/icons/".$img.".png\" style=\"position:relative;top:2px;width:12px;margin-right:5px;filter:chroma(color=#ffffff);border:none;\" /><span style=\"display:none;\">".$val."</span>Show all ".$val." POTs</div>";
   }

   $content2 .= "</td></tr></table>";

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:220px;table-layout:fixed;margin-bottom:22px;position:relative;top:-1px;z-index:9998;\">\r\n".
   "      <tr>\r\n".
   "         <td class=\"module_2_headline\" style=\"background-color:rgb(255,255,255);width:220px;\">%%TABS%%</td>\r\n".
   "      </tr>\r\n".
   "      <tr>\r\n".
   "         <td><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "      </tr>\r\n".
   "      <tr>\r\n".
   "         <td class=\"module_2_content\">".$content."</td>\r\n".
   "      </tr>\r\n".
   "      <tr>\r\n".
   "         <td class=\"module_2_content\">".$content2."</td>\r\n".
   "      </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   return $module;

}




?>