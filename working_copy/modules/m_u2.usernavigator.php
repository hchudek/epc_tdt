<?php


function m_u2__usernavigator($_application) {

   $t_module=str_replace(".php", "", basename(__FILE__));

   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);


   $module.=
   "%%CONTENT%%";

   if(!in_array($t_module, $_hide_modules)) {
      $n_modules=implode(",",$_hide_modules);
      $n_modules=($n_modules=="") ? $t_module : $n_modules.",".$t_module; 
      $t_replace[0]="action_portlet_collapse.gif";
      $tmp=str_replace("hide_module=".$_REQUEST["hide_module"], "", $_SERVER["QUERY_STRING"]);
      $t_replace[1]=($tmp=="") ? "?hide_module=".$n_modules : "?".$tmp."&hide_module=".$n_modules;
      $t_replace[2]="<div class=\"module_content\">\r\n%%NAVIGATION%%<div><img src=\"../../../../library/images/blank.gif\" style=\"width:4px;\" /></div></div>\r\n";

      $navigation="";

      foreach($_application["page"] as $value) {
         if(is_string($value) && $_application["page"][$value]["type"]=="userpage" && $_application["page"][$value]["status"]=="active") {
            $t_class=($value==$_application["page_id"]) ? "navigator_high" : "navigator_reg";
            $t_attributes=(isset($_application["page"][$value]["navigator"]["attributes"])) ? "?&".$_application["page"][$value]["navigator"]["attributes"] : "";
            $t_attributes = str_replace("DOLLAR", "$", $t_attributes);
            $txt = ($t_attributes != "") ? "*" : "";
            $navigation.="<div class=\"".$t_class."\"><a href=\"".$value.".php".$t_attributes."\" style=\"font:normal 13px century gothic,verdana;\">".$_application["page"][$value]["page_descr"].$txt."</a></div>";
         }
      }

      $t_replace[2]=str_replace("%%NAVIGATION%%", $navigation, $t_replace[2]);
   }
   else {
      $n_hide_module=str_replace($t_module, "", str_replace(",".$t_module, "", $_REQUEST["hide_module"]));
      $t_replace[0]="action_portlet_expand.gif";
      $t_replace[1]="?".str_replace("hide_module=".$_REQUEST["hide_module"], "hide_module=".$n_hide_module, $_SERVER["QUERY_STRING"]);
      $t_replace[2]="<div style=\"font:normal 5px verdana;\">&nbsp;</div>\r\n";
   }

   $replace=split(",", "%%IMAGE%%,%%HREF%%,%%CONTENT%%");
   $module="<div style=\"margin-bottom:18px;\">".str_replace($replace, $t_replace, $module)."</div>";


   return array($headline, $module);

}

?>