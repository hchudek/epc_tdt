<?php

function te_footer($_application) {
   $module = 
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:99%;\"><tr>".
   "<td style=\"text-align:left;99%;\"><div style=\"white-space:nowrap;background-color:rgb(99,99,99);color:rgb(255,255,255);font:normal 12px open Sans;padding:3px;margin-right:10px;\">".
   $_SERVER["SERVER_NAME"].
   "&nbsp;|&nbsp;".str_replace(".php", "", substr($_SERVER["PHP_SELF"], strrpos($_SERVER["PHP_SELF"], "/") + 1, strlen($_SERVER["PHP_SELF"]))).
   "&nbsp;|&nbsp;".phpversion().
   "&nbsp;|&nbsp;".ini_get("memory_limit").
   "&nbsp;|&nbsp;".Intval(memory_get_peak_usage()/1048576)."M".
   "&nbsp;|&nbsp;".intval(memory_get_usage()/1048576)."M".
   "&nbsp;|&nbsp;".session_id().
   "</div></td>".
   "<td style=\"text-align:right;width:1%;white-space:nowrap;\">".create_bottom_button($_application)."&nbsp;&nbsp;</td>".
   "</tr></table>";

   return $module;
}

function create_bottom_button($_application) {
   $_html="";
   $c=count($_application["topbutton"]["name"]);
   for($i=0;$i<$c;$i++) {
      $delim = ($i == $c - 1) ? "" : "&nbsp;|&nbsp;";
      $_html.="<a style=\"font:normal 12px Open Sans;color:rgb(99,99,99);text-decoration:none;\" href=\"".$_application["topbutton"]["link"][$i]."\">".$_application["topbutton"]["name"][$i]."</a>".$delim;
   }
   return $_html."<a id=\"te_footer\"></a>";
}



?>


