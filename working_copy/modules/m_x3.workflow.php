<?php

function m_x3__workflow($_application) {

   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_source"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_source"] = "1";


   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0])." ".
   "<select onchange=\"m_x3.change_source(this)\" style=\"background:transparent; font:normal 12px Open Sans; border:none;\">\r\n";

   for($i = 1; $i <= 2; $i++) {
      $selected = ($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_source"] == $i) ? " selected" : "";
      $headline .= "<option value=\"".$i."\"".$selected.">Source ".$i."</option>\r\n";
   }
   
   $headline .= "<select>\r\n";   


   // Module Body --------------------------------------------------------------------------------------------------
   $_SESSION["tracker"] = $_REQUEST["unique"];
   $_SESSION["pot"] = $_REQUEST["pot"];   

   $load = array("tracker", "pot", "meta", "tool");
   require_once("addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);


   $dualsource = ($tc_data["tool"]["dualsource"] == "1") ? "true" : "false";
   $module = "<div form=\"".__FUNCTION__."\" tpm=\"".$tc_data["tracker"]["responsible"]."\" dualsource=\"".$dualsource."\" process_type=\"".rawurlencode($tc_data["meta"]["process_type"])."\"></div>\r\n";
   return array($headline, $module);

}


?>