<?php

if($_REQUEST["session"] == "r_data") {
   session_start();
   unset($_SESSION["r_data"]);
   unset($_SESSION["r_column"]);
   unset($_SESSION["r_recalc"]);
   die;
}

if($_REQUEST["embed_epc"] == "true") {
   session_start();
   $_SESSION["remote_domino_path_main"] = "http://teg1.de.tycoelectronics.com:9220/tyco/web/tdtracking.nsf";
   $_SESSION["remote_domino_path_epcmain"] = "http://teg1.de.tycoelectronics.com:9220/tyco/web/main.nsf";
   include_once("../../../../library/tools/addin_xml.php");								
   print m_e2_ee_edit_pot("true");
   die;
}

function m_e2__ee_edit_pot($_application) {

  //global $r_data;

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $headline="Edit Part and Tools";

   // DOKUMENTTYPEN
   $doc_type[] = "Tracker"; 
   $doc_type[] = "POT";
   $doc_type[] = "Part"; 
   $doc_type[] = "Tool"; 
   $doc_type[] = "Creport";
   $doc_type[] = "Date"; 


   // FELDER + STANDARDFELDER
   $selected_fields = explode(":", trim(rawurldecode(base64_decode($_REQUEST[$_module_id."_dsp"]))));
   $selected_fields[] = "unid";
   $selected_fields[] = "filter0";
   $selected_fields[] = "filter1";
   $selected_fields[] = "filter2";

   //if($_REQUEST[$_module_id."_dsp"] == "") {
   //   $selected_fields[] = "Project-No";
   //}  


   // FELDDEFINITION
   //$field_def["unid"] = array("type"=> "pot", "value" => "unique_tracker&pot=unique_pot");
   //$field_def["filter0"] = array("type"=> "tracker", "value" => "get_r_data('unique_tracker', 1, false);");
   //$field_def["filter1"] = array("type"=> "pot", "value" => "get_r_data('unique_pot', 2, false);");
   //$field_def["filter2"] = array("type"=> "tracker", "value" => "rawurldecode(get_r_data('unique_tracker', 3, false));");
   //$field_def["Project-No"] = array("type"=> "tracker", "value" => "rawurldecode(get_r_data('unique_tracker', 1, false));");
   //$field_def["Production location G3"] = array("type"=> "pot", "value" => "rawurldecode(get_r_data('unique_pot', 1, false));");
 



   // WIRD IMMER GELDAN
   $load_type[] = "pot";
   if($_REQUEST["load"] != "") {
      $load_type[] = "part";
      $load_type[] = "tool";
   }


   foreach($field_def as $key => $val) {
      if(in_array($key, $selected_fields)) {
         $field[$key] = $val;
         if(!in_array($val["type"], $load_type)) $load_type[] = $val["type"];
      }
   }


   // Daten laden
   // ----------------------------------------------------------------------------------------------------------------------

   if(!isset($_SESSION["load_case"]) || $_SESSION["load_case"] != $_REQUEST["load"] || 1 == 1) {
      $load_case = ($_REQUEST["load"] == "") ? "_" : strtolower($_REQUEST["load"]);
      unset($_SESSION["r_data"]);
   }

   if($_SESSION["r_data"] == "" || 1 == 1) {
      // POT   
      if(in_array("pot", $load_type)) {
        $use_view = ($_REQUEST["load"] == "") ? "v.load.dis.link" : "v.load.pot.link";
        $pot_data = load_data_from_view($_SESSION["remote_domino_path_main"]."/".$use_view."?open&restricttocategory=".$load_case."&start=%%START%%&count=%%COUNT%%&function=plain");
        foreach($pot_data as $key => $val) {
           if(!in_array($pot_data[$key][1], $tmp_key) || $_REQUEST["load"] != "") {
              $r_data[$key] = $pot_data[$key];
              $r_data["key"][] = $key;
              $tmp_key[] = $pot_data[$key][1];
           }
        }
        $r_data["partid"] = array_unique($r_data["partid"]);

      }

//print "OK"; die;
//print $_SESSION["remote_domino_path_epcmain"]."/v.load.pot.tool?open&restricttocategory=".$load_case."&start=%%START%%&count=%%COUNT%%&function=plain";
//die;

      // Parts
      if(in_array("part", $load_type)) { 
         $load = load_data_from_view($_SESSION["remote_domino_path_epcmain"]."/v.load.pot.part?open&restricttocategory=".$load_case."&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($load as $key => $val) {
            $r_data[$key] = $val;
         }
      } 


      // Tools
      if(in_array("tool", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_epcmain"]."/v.load.pot.tool?open&restricttocategory=".$load_case."&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($load as $key => $val) {
            $r_data[$key] = $val;
         }
      } 

      unset($data);
      $column[] = "unid";
      $column[] = "filter0";
      $column[] = "filter1";
      $column[] = "filter2";
      if($_application == "true") {
         $column[] = "Part no";
         $column[] = "Part descr";
         $column[] = "Tool PO no";
         $column[] = "Tool supp";
      }
      else {
         if($_REQUEST["load"] == "") {
            $column[] = "Project num";
            $column[] = "Project name";
            $column[] = "Case num";   
            $column[] = "Case name";         
         }
         else {
            $column[] = "Project num";
            $column[] = "Case num";
            $column[] = "Part num";
            $column[] = "Drawing revision";
            $column[] = "Part supplier";
            $column[] = "Tool";
            $column[] = "Tool PO num";
            $column[] = "Position";
            $column[] = "Confirmed EVT";
            $column[] = "Tool supplier";
         }
      }

      $count = count($r_data["key"]);for($i = 0; $i < $count; $i++) {

         $unid["part"] = $r_data[$r_data["key"][$i]][3];
         $unid["tool"] = $r_data[$r_data["key"][$i]][5];

         if($i == 0 && $_REQUEST["load"] != "") {
            $headline .= "&nbsp;[<a href=\"".$_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$r_data["key"][$i]][7]."\" style=\"text-decoration:none;color:rgb(0,102,158);\" target=\"_blank\">".$r_data[$r_data["key"][$i]][2]."</a>&nbsp;/&nbsp;<a href=\"".$_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$r_data["key"][$i]][4]."\" style=\"text-decoration:none;color:rgb(0,102,158);\" target=\"_blank\">".$r_data[$r_data["key"][$i]][1]."</a>]";
         }

         foreach($column as $key => $val) {
            $dsp_filter[$val] = false;
            if($_REQUEST["load"] == "") {
               switch($val) {
                  case "unid" : $set_data = $r_data[$r_data["key"][$i]][4]; break;
                  case "filter0" : $set_data = "f0"; break;
                  case "filter1" : $set_data = "f1"; break;
                  case "filter2" : $set_data = "f2"; break;
                  case "Project num": $set_data = $r_data[$r_data["key"][$i]][2]; break;
                  case "Project name": $set_data = $r_data[$r_data["key"][$i]][0]; break;
                  case "Case num": $set_data = do_include($r_data[$r_data["key"][$i]][1], "?load=".$r_data[$r_data["key"][$i]][4]); break;
                  case "Case name": $set_data = $r_data[$r_data["key"][$i]][3]; break;
               }
            }
            else {
               switch($val) {
                  case "Part no": $set_data = "<a href=\"".$r_data[$unid["part"]][4]."\">".$r_data[$unid["part"]][0]."</a>"; break;
                  case "Part descr": $set_data = rawurldecode($r_data[$unid["part"]][1]); break;
                  case "Tool PO no": $set_data = "<a href=\"".$r_data[$unid["tool"]][7]."\">".$r_data[$unid["tool"]][0]."</a>"; break;
                  case "Tool descr": $set_data = rawurldecode($r_data[$unid["tool"]][5]); break;
                  case "Tool supp": $set_data = rawurldecode($r_data[$unid["tool"]][1]); break;

                  case "unid" : $set_data = $r_data[$r_data["key"][$i]][4]; break;
                  case "filter0" : $set_data = "f0"; break;
                  case "filter1" : $set_data = "f1"; break;
                  case "filter2" : $set_data = "f2"; break;
                  case "Project num": $set_data = do_include($r_data[$r_data["key"][$i]][2], $_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$r_data["key"][$i]][7]); break;
                  case "Project name": $set_data = do_include($r_data[$r_data["key"][$i]][0], $_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$r_data["key"][$i]][7]); break;
                  case "Case num": $set_data = do_include($r_data[$r_data["key"][$i]][1], $_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$r_data["key"][$i]][4]); break;
                  case "Case name": $set_data = do_include($r_data[$r_data["key"][$i]][3], $_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$r_data["key"][$i]][4]); break;
                  case "Part num": $set_data = do_include($r_data[$unid["part"]][0], $_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$unid["part"]][4]); break;
                  case "Drawing revision" : $set_data = ($r_data[$unid["part"]][0] != "" & in_array("[epc_part_edit]", $_SESSION["remote_userroles"])) ? do_include($r_data[$unid["part"]][2], $unid["part"], "tyco/web/main.nsf", "NUM_DrwRevision") : $r_data[$unid["part"]][2]."&nbsp;"; break;
                  case "Part supplier":
                     $part_supplier = utf8_decode(rawurldecode($r_data[$unid["part"]][3])); 
                     if(strpos($part_supplier, "[") && strpos($part_supplier, "]")) {
                        $alt = substr($part_supplier, 0, strpos($part_supplier, "["));
                        //$part_supplier = substr($part_supplier, strpos($part_supplier, "[") + 1, strpos($part_supplier, "]") - strpos($part_supplier, "[") - 1);
                     }
                     else {
                        $alt = $part_supplier;
                        $part_supplier = substr($part_supplier, 0, 18);
                        if(strlen($alt) > 18) $part_supplier .= "...";
                     }
                     $set_data = in_array("[epc_part_edit]", $_SESSION["remote_userroles"]) ? do_include(rawurlencode($part_supplier), $unid["part"], "tyco/web/main.nsf", "supplier", "supplier", $alt) : rawurlencode($part_supplier)."&nbsp;";
                     break;
                  case "Tool" :
                     $tool = ($r_data[$unid["tool"]][0] == "") ? "&nbsp;" : $r_data[$unid["tool"]][0];
                     $link = ($r_data[$unid["tool"]][0] == "") ? "" : $_SESSION["remote_domino_path_epcmain"]."/_/".$r_data[$unid["tool"]][7]; 
                     $set_data = do_include($tool, $link); 
                     break;
                  case "Tool PO num" : $set_data = ($r_data[$unid["tool"]][0] != "" & in_array("[epc_tool_edit]", $_SESSION["remote_userroles"])) ? do_include($r_data[$unid["tool"]][4], $unid["tool"], "tyco/web/main.nsf", "pono") : $r_data[$unid["tool"]][4]."&nbsp;"; break;
                  case "Position" : $set_data = ($r_data[$unid["tool"]][0] != "" & in_array("[epc_tool_edit]", $_SESSION["remote_userroles"])) ? do_include($r_data[$unid["tool"]][5], $unid["tool"], "tyco/web/main.nsf", "position") : $r_data[$unid["tool"]][5]."&nbsp;"; break;
                  case "Confirmed EVT" : $set_data = ($r_data[$unid["tool"]][0] != "" & in_array("[epc_tool_edit]", $_SESSION["remote_userroles"])) ? do_include($r_data[$unid["tool"]][6], $unid["tool"], "tyco/web/main.nsf", "datesuppevt", "calendar") : $r_data[$unid["tool"]][6]."&nbsp;"; break;
                  case "Tool supplier":
                     $tool_supplier = utf8_decode(rawurldecode($r_data[$unid["tool"]][1])); 
                     if(strpos($tool_supplier, "[") && strpos($tool_supplier, "]")) {
                        $alt = substr($tool_supplier, 0, strpos($tool_supplier, "["));
                        $tool_supplier = substr($tool_supplier, strpos($tool_supplier, "[") + 1, strpos($tool_supplier, "]") - strpos($tool_supplier, "[") - 1);
                     }
                     else {
                        $alt = $tool_supplier;
                        $tool_supplier = substr($tool_supplier, 0, 18);
                        if(strlen($alt) > 18) $tool_supplier .= "...";
                     }
                     $set_data = ($r_data[$unid["tool"]][0] != "" & in_array("[epc_tool_edit]", $_SESSION["remote_userroles"])) ? do_include(rawurlencode($tool_supplier), $unid["tool"], "tyco/web/main.nsf", "supplier", "supplier", $alt) : rawurlencode($tool_supplier)."&nbsp;";
                     break;
               }
            }
            $data[$i][$val] = $set_data;
         }
      }
      //$_SESSION["r_data"] = $data;
      //$_SESSION["load_case"] = $load_case;
      //$_SESSION["dsp_filter"] = $dsp_filter;
      //$_SESSION["r_column"] = $column;
      //$_SESSION["r_recalc"] = mktime();
   }
   else {
      $data = $_SESSION["r_data"];
      $column = $_SESSION["r_column"];
   }

   // NEW
   return $data;


   if($_REQUEST["m_e2_sortdir"] == "") $_REQUEST["m_e2_sortdir"] = "asc";

   include_once("addin/generate_extended_editable_view.php");


   // Export Links
   $exp = "<img onclick=\"location.href='?".$_module_id."_download=xls&".$_SERVER["QUERY_STRING"]."';\" src=\"../../../../library/images/icons/ms-excel-icon.gif\" style=\"margin-right:7px;cursor:pointer;filter:Gray();\" /><img onclick=\"location.href='?".$_module_id."_download=doc&".$_SERVER["QUERY_STRING"]."';\" src=\"../../../../library/images/icons/ms-word-icon.gif\" style=\"margin-right:7px;cursor:pointer;filter:Gray();\" />";




   // View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
   if($_application == "true") {
      $module = "<div id=\"add_part_list\">\r\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n<tr>%%HEAD%%</tr>\r\n";
      foreach($data as $f_data) {
         $module .= "<tr>";
         foreach($f_data as $key => $val) {
            if($key != "unid" && $key != "filter0" && $key != "filter1" && $key != "filter2") {
               if($add_head != true) $head .= "<th>".$key."</th>\r\n";
               $module .= "<td>".$val."&nbsp;</td>\r\n";
            }
         }
         $add_head = true;
         $module .= "</tr>\r\n";
      }
      $module .= "</table>\r\n</div>\r\n";
      return str_replace("%%HEAD%%", $head, $module);
   }
   else {
      $t_view = generate_extended_view($_SESSION["dsp_filter"], $path, $column, $data, $_w, $_SESSION["module"][$_module_name], false, true, false, true, true, false, 40, array("use:html",$_SESSION["remote_domino_path_epcmain"]."/_/%%UNID%%?open\" target=\"_blank"));
   }

   $module.=
   "<iframe name=\"ifram\" scrolling=\"no\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\" framespacing=\"0\" src=\"".$_SESSION["php_server"]."/apps/epc/main/addin/data_picker.php?use_sortkey=1&use_sortdir=asc&start=1&key=&cm=&supplier_db=\" style=\"width:978px;height:382px;position:absolute;left:0px;margin-left:172px;margin-top:24px;border:solid 2px rgb(0,139,184);visibility:hidden;filter:alpha(opacity=92);padding:0px;\"></iframe>\r\n".
   "<input type=\"hidden\" name=\"supplier\" value=\"\" />\r\n".
   "<input type=\"hidden\" name=\"supplier_old\" value=\"\" />\r\n".
   "<input type=\"hidden\" name=\"supplier_old_bak\" value=\"\" />\r\n".
   "<input type=\"hidden\" name=\"supplier_unid\" value=\"\" />\r\n".
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_4_spacer\">&nbsp;</td>\r\n".
   "      <td class=\"module_4_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr><td colspan=\"2\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
   "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
   "</form></div>\r\n";

   $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));

  // return array($headline, $module);

   return $data;
}



function do_include() {
   $args = func_get_args();
   return implode("DO_INCLUDE_ATTR", $args); 
}



?>