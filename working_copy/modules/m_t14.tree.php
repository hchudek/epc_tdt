<?php

function m_t14__tree($_application) {

	if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"])) {
		$_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"] = "1,2";
	}
 
	$_SESSION["application:page_id"] = $_application["page_id"];
	$_SESSION["tracker"] = $_REQUEST["unique"];
	$_SESSION["pot"] = $_REQUEST["pot"];
	 
	// Module Headline ----------------------------------------------------------------------------------------------
	$headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

	// Module Body --------------------------------------------------------------------------------------------------
	$module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
	$module .= "<script src=\"dd/js/drag-drop-folder-tree.js\"></script>\r\n";
	return array($headline, $module);

}

function generate_tree_module($_application, $pot_date, $unique, $pot) {

	global $tc_data;

	$tc_data = generate_xml($_SESSION["remote_domino_path_main"]."/v.get_xml_data/".$unique."?open&tree=true&pot=".$pot);

	if(count($tc_data["tracker"]["pot_dsp_name"]) > 0) {
		$node = (!is_array($tc_data["tracker"]["tree"])) ? $tc_data["tracker"]["tree"] : "1-null";
		$pot_name = $tc_data["tracker"]["pot_dsp_name"];
		if(!is_array($tc_data["tracker"]["additional_pot"])) {
			foreach(explode(":", substr($tc_data["tracker"]["additional_pot"], 6, strlen($tc_data["tracker"]["additional_pot"]))) as $val) {
				$pot_name[] = "---".$val."/---";
			}
		}

		$count = 0;
		foreach(explode(",", $node) as $val) {
			$partno = substr($val,0, strpos($val, "-"));
			$token = explode("-", $val);

			$key = "";
			$num = -1;
			for($i = 0 ; $i < count($tc_data["tracker"]["ref_pot"]); $i++) {
				if($tc_data["tracker"]["pot_dsp_name"][$i] == $pot_name[$partno - 2]) {
					$key = $tc_data["tracker"]["ref_pot"][$i];
					$num = $i;
				}
			}

			if($key !="") $status_ir[$key] = $tc_data["tracker"][status_ir][$num];

			$set_status_ir = ($key == "") ? "X" : $status_ir[$key];
			if($tc_data["tracker"]["process_type"][$count] == "Pilot") {
				$evt_date = ($pot_date[$key]["G0303"][count($pot_date[$key]["G0303"]) - 1]["revised_value"]);
			} else {
				$evt_date = ($pot_date[$key]["G0407"][count($pot_date[$key]["G0407"]) - 1]["revised_value"]);
			}

			$time_to_evt = (strtotime($evt_date) > 0) ? "[days to EVT: ".(intval((strtotime($evt_date) - time()) / 86400) + 1)."]" : "";

			$dsp_name = "<b>".substr($pot_name[$partno - 2],3, strpos($pot_name[$partno - 2], "/") - 3)."</b>&nbsp;<i>".rawurldecode($tc_data["tracker"]["part_name"][$partno - 2])."</i>&nbsp;".$time_to_evt;
			 
			$tree[$token[0]]["id"] = $token[0];
			$tree[$token[0]]["parent_id"] = $token[1];
			$tree[$token[0]]["status_ir"] = $set_status_ir;
			$tree[$token[0]]["pot"] = $tc_data["tracker"]["ref_pot"][$partno - 2];
			$tree[$token[0]]["name"] = (strpos($val, "null")) ? $tc_data["project"]["number"]."</a>" : $dsp_name."</a>";

			$tree[$token[0]]["href"] = (strpos($val, "null")) ? "?&unique=".$_REQUEST["unique"]."&project=".base64_encode($tc_data["project"]["number"]) : "?&unique=".$_REQUEST["unique"]."&pot=".$tree[$token[0]]["pot"];


			if(!strpos($val, "null")) $tree[$token[0]]["name"] .= "<img src=\"../../../../library/images/blank.gif\" onclick=\"return true;edit_comment(this, '', false);\" style=\"margin-left:4px;border:none;\" /><span style=\"display:none;\"><input type=\"text\" value=\"".rawurldecode($tc_data["tracker"]["pot_name"][$partno - 2])."\" style=\"width:300px;border:dotted 1px rgb(99,99,99);font:normal 12px century gothic,verdana;margin-left:4px;\" /><img src=\"../../../../library/images/16x16/status-17.png\" onclick=\"edit_comment(this, '".$tree[$token[0]]["pot"]."', true);\" style=\"margin-left:4px;display:none;\" /></span>";

			$count++;
		}
		$count = 1;


		for($i = 0 ; $i < count($pot_name); $i++) {
			$val = $pot_name[$i];

			$count++;
			$img = (in_array($count."-1", explode(",", $node)) ||
			in_array($count."-2", explode(",", $node)) ||
			in_array($count."-3", explode(",", $node)) ||
			in_array($count."-4", explode(",", $node)) ||
			in_array($count."-5", explode(",", $node)) ||
			in_array($count."-6", explode(",", $node)) ||
			in_array($count."-7", explode(",", $node)) ||
			in_array($count."-8", explode(",", $node)) ||
			in_array($count."-9", explode(",", $node)) ||
			in_array($count."-10", explode(",", $node))) ? "<a href=\"javascript:void(0);\" onclick=\"document.getElementsByName('add_pot')[0].value='".substr($val, 3, strpos($val, "/") - 3)."';m_t14.add_dummy_pot('".$tc_data["tracker"]["unid"]."');\"><img src=\"../../../../library/images/icons/copy.gif\" style=\"margin-left:8px;border:none;\" /></a>" : "<a href=\"javascript:m_t14.add_to_tree('".$tc_data["tracker"]["unid"]."','".$count."-1')\" ><img src=\"../../../../library/images/arrow_bullet.gif\" style=\"margin-left:8px;border:none;\" /></a>";
			$work_basket .= "<div style=\"width:120px;padding-bottom:6px;background-color:rgb(220,220,220);border:solid 1px rgb(255,255,255);padding-top:5px;padding-left:4px;padding-right:4px;\">".substr($val, 3, strpos($val, "/") - 3).$img."</div>\r\n";
		}

		$work_basket .= "<div style=\"width:120px;padding-bottom:6px;background-color:rgb(220,220,220);border:solid 1px rgb(255,255,255);padding-top:5px;padding-left:4px;padding-right:4px;\">";
		$work_basket .= "<input type=\"text\" name=\"add_pot\" value=\"\" style=\"width:77px;border:dotted 1px rgb(99,99,99);margin-right:4px;position:relative;top:1px;\" /><span class=\"phpbutton\" style=\"position:relative; top:-18px;right:-80px;\" ><a href=\"javascript:m_t14.add_dummy_pot('".$tc_data["tracker"]["unid"]."');\">Add</a></span>";
		$work_basket .= "</div>";
		
		$html = "<p id=\"added_pot\" style=\"display:none;\">".$tc_data["tracker"]["additional_pot"]."</p>";
		$html .= "<table><tr><td>".
	    "<span class=\"phpbutton\" style=\"margin-right:8px\"><a href=\"javascript:void(0);\" onclick=\"m_t14.save_tree('".$tc_data["tracker"]["unid"]."');\">Save</a></span>\r\n".
    	"<span class=\"phpbutton\" style=\"margin-right:8px\"><a href=\"javascript:void(0)\" onclick=\"treeObj.collapseAll()\">Collapse all</a></span>\r\n". 
        "<span class=\"phpbutton\" style=\"margin-right:8px\"><a href=\"javascript:void(0)\" onclick=\"treeObj.expandAll()\">Expand all</a></span>\r\n".
        "<span class=\"phpbutton\" ><a href=\"javascript:void(0)\" onclick=\"m_t14.reset_tree('".$tc_data["tracker"]["unid"]."');\">Reset tree</a></span>\r\n".
        "</td></tr><tr><td>";
		
		$html .=
        "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>".
        "<td style=\"vertical-align:top;padding-top:10px;padding-left:0px;\">".$work_basket."</td>";

		$html .= "<td style=\"vertical-align:top;\">".str_replace("<ul></ul>", "", generate_tree($tree))."</td>".
        "</tr></table>";
		$html .= "</td></tr></table>";
	}
	 
	return $html;
}
function generate_tree($tree, $parent_id = "null", $res) {
	$tracker = $_SESSION["tracker"];
	if($parent_id == "null") $res = "<ul id=\"dhtmlgoodies_tree2\" class=\"dhtmlgoodies_tree\" style=\"padding-top:8px\">";
	else $res .= "<ul style=\"padding-left:40px\">";
	foreach($tree as $key => $value) {
		if($value["parent_id"] === $parent_id) {
			$href = str_replace("unique=&", "unique=".$tracker."&", $value["href"]);
			$attr = ($parent_id == "null") ? " style=\"position:relative;\" status_ir=\"P\" nnoDrag=\"true\" noSiblings=\"true\" noDelete=\"true\" noRename=\"true\"" : " status_ir=\"".substr($value["status_ir"], 0 , 1)."\" noDelete=\"true\" noRename=\"true\"";
			$res .= "<li id=\"node".$value["id"]."\"".$attr.">".
         "<a href=\"".$href."\">".$value["name"];
			$res = generate_tree($tree, $value["id"], $res);
			$res .= "</li>";
		}
	}
	$res .= "</ul>";
	return $res;
}
function m_t14__tree_generate_html($_application) {
	$tracker = $_SESSION["tracker"];
	$pot = $_SESSION["pot"];
	$module = generate_tree_module( $_application,  $module, $tracker, $pot );
	return str_replace("Array","",$module);
}



?>