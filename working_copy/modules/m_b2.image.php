<?php


function m_b2__image($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";

   return array($headline, $module);
}


function m_b2__image_generate_html($_application) {

   global $tc_data;

   $img_root = $_SESSION["img_root"];
   $allowed_types = $_SESSION["allowed_types"];

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $edit_upload = (in_array("[t_d]", $_SESSION["remote_userroles"]) || in_array("[superuser]", $_SESSION["remote_userroles"])) ? true : false;
  
   $file = false; foreach(explode("|", substr($allowed_types, 1, strlen($allowed_types) -2)) as $type) {
      if($file == false && file_exists($img_root.$tc_data["tracker"]["unique"].".".$type)) $file = "images/tracker_images/".$tc_data["tracker"]["unique"].".".$type;
   }

   if($file) {
      $oncontextnemu = (strpos($_SESSION["domino_user"], $tc_data["tracker"]["responsible"])) ? " oncontextmenu=\"m_b2__image.kill('".trim(substr($file, strrpos($file, "/") + 1, strlen($file)))."'); return false;\"" : "";
      $content .= "<img".$oncontextnemu." src=\"".$file."\" style=\"max-width:100%;\">\r\n";
   }
   else {
      $content .= "<form style=\"padding:10px 4px 0px 4px;\"><div style=\"position:absolute;left:0px;\">";
      if($edit_upload) $content .=    "<input type=\"file\" onchange=\"m_b2__image.upload(this);\" name=\"image\" style=\"cursor:pointer;width:0px;position:absolute;z-index:2;filter:alpha(opacity=0);\"/></div><span class=\"phpbutton\"><a>Upload image</a></span>";
      $content .=    "</form><br />\r\n";
   }

   return $content;

}




?>