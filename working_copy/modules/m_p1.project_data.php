<?php


function m_p1__project_data($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = ($_REQUEST["unique"] == "") ? "No tracker selected" : "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   //$module = m_p1__project_data_generate_html($_application);

   return array($headline, $module);

}


function m_p1__project_data_generate_html($_application) {

   global $tc_data;

   $ref_project = ($tc_data["project"]["ref_number"]) ? strtoupper(" [".$tc_data["project"]["ref_number"]."]") : "";

   $arr= array("A","B","C","D","E","F","G"); 
   $selectbox = 
   "<select onchange=\"handle_save_single_field_extdb('".$_SESSION["remote_domino_path_leanpd"]."', '".$tc_data["project"]["unid"]."', 'prj_complex', this[this.selectedIndex].value, '');\">;".
   "<option></option>";

   foreach ($arr as $val) {
     $select=($tc_data["project"]["complex"]==$val) ? " selected": "";
     $selectbox.="<option value=\"".$val."\"".$select.">".$val."</option>";
   }
   $selectbox .= "</select>";

   $html = 

   "<table id=\"tbl_m_p1__project_data\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">".
   "<tr>".
   "<td>Project number</td>".
   "<td><span id=\"project_number\">".rawurldecode($tc_data["project"]["number"]).$ref_project."</span></td>".
   "</tr>".
   "<tr>".
   "<td>Description</td>".
   "<td>".rawurldecode($tc_data["project"]["description"])."</td>".
   "</tr>".
   "<tr>".
   "<td>Current Phase</td>".
   "<td>".rawurldecode($tc_data["project"]["phasedesc"])."</td>".
   "</tr>".
   "<tr>".
   "<td>Manufacturing City</td>".
   "<td>".rawurldecode($tc_data["project"]["city"])."</td>".
   "</tr>".
   "<tr>".
   "<td>Project complexity</td>".
   "<td>".$selectbox."</td>".
   "</tr>".
   "<tr>".
   "<td>Production Manager</td>".
   "<td>".rawurldecode($tc_data["project"]["prodmngr"])."</td>".
   "</tr>".
   "<tr>".
   "<td>Project Manager</td>".
   "<td>".rawurldecode($tc_data["project"]["projmngr"])."</td>".
   "</tr>".
   "</table>";

   return $html;
}







function create_uqs_data($tunid, $tid, $id) {

   global $tc_data;

   $settings_data = generate_xml($_SESSION["remote_domino_path_epcquoting"]."/v.get_settings_data/".$tunid."?open");


   if(count($settings_data) == 0) {
      return false;
   }
   else {

      $tva = $settings_data["variants"];
      $tse = $settingsdata["selected_variant"];
      $tool = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\" id=\"uqs_info_".$id."\" style=\"z-index:99999;border:dotted 1px rgb(99,99,99);background-color:rgb(255,255,255);position:absolute;display:none;font:normal 12px century gothic,verdana;\">";

      $headline = true;
      $keys = array_keys($settings_data["settings-block"]);
      foreach($keys as $key) {
         $dsp_key = $key;
         $tool .= "<tr><td style=\"text-align:center;background-color:rgb(255,194,54);\" colspan=\"".(1 + $tva)."\">".$dsp_key."</td></tr>";
         if($headline == true) {
            $tool .= "<tr><td>&nbsp;</td>";
            for($i = 0; $i < $tva; $i++) {
               $tool .= "<td style=\"text-align:right;\">".$settings_data["settings-block"]["base.tool"]["titles"]["value"][$i]."</td>";
            }
            $headline = false;
         }
         $tool .= "</tr>";
         unset($sum); $tmp_count = 1;
         foreach($settings_data["settings-block"][$key]["rows"]["row"] as $val) {
            if($key == "conv.kits") $val["label"] = "Converion kit ".$tmp_count;
            $tmp_count++;
            $tool .= "<tr>".
            "<td style=\"padding-right:30px;\">".$val["label"]."</td>";
            for($i = 0; $i < $tva; $i++) {
               if(isset($val["value"][$i])) {
                  if($tva == 1) $v = ($i == ($tse + 1)) ? "<b>".str_replace(",", ".", $val["value"])."</b>" : str_replace(",", ".", $val["value"]);
                  else $v = ($i == ($tse + 1)) ? "<b>".str_replace(",", ".", $val["value"][$i])."</b>" : str_replace(",", ".", $val["value"][$i]);
               }
               else $v = "&nbsp;";

               $tool .= "<td style=\"text-align:right;background-color:rgb(220,220,220);\">&nbsp;".str_replace("Array", "", strval($v))."</td>";
               if(isset($val["value"][$i]) && $val["summary"] == "1") {
                  if($tva == "1") $sum[$i] += floatval(str_replace(",", ".", $val["value"]));
                  else $sum[$i] += floatval(str_replace(",", ".", $val["value"][$i]));
               }
            }
            $tool .= "</tr>";
         }
         $tool .= "<tr><td>Sum</td>";
         for($i = 0; $i < $tva; $i++) {
            $v = ($i == ($tse + 1)) ? "<b>".$sum[$i]."</b>" : $sum[$i];
            $tool .= "<td style=\"text-align:right;\">".$v."</td>";
         }  
         $tool .= "</tr>";
      }
      $tool .= "<tr><td>&nbsp;</td></tr></table>";
   }

   return $tool;


}




?>