<?php

function m_x0__add_remark($_application) {

   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"])) {
      $_SESSION["perpage"]["tab"][$_application["page_id"]][str_replace("_generate_html", "", __FUNCTION__)."_dsp"] = "1,2";
   }
   $_SESSION["application:page_id"] = $_application["page_id"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = m_x0__add_remark_html($_application);
   return array($headline, $module);

}


function m_x0__add_remark_html($_application) {

   if(strlen($_REQUEST["unid"]) != 32) {
      $module = "ERROR";
   }
   else {
      $module = 
      "Add a comment".
      "<br>".
      "<br>".
      "<input type=\"text\" name=\"add_remark\" maxlength=\"100\" style=\"padding:2px; width:400px; border:solid 1px rgb(169,169,169); font: normal 13px open sans;\">".
      "<br>".
      "<br>".
      "<span class=\"phpbutton\"><a href=\"javascript:m_x0.add_remark('".$_REQUEST["unid"]."');\">Submit</a></span>\r\n";  
   }

   return $module;

}




?>