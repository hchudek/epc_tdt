<?php
                      
   $keys[] = array("tr_created", "Created");
   $keys[] = array("tr_doc_status", "Document Status");
   $keys[] = array("tr_project_number", "Project Number");
   //$keys[] = array("tr_ref_pot", "Ref. POT");
   $keys[] = array("tr_responsible", "T&PM");
   $keys[] = array("tr_tracker_descr", "Tracker Desc.");

   //$keys[] = array("l_case_unid", "Case UNID");
   //$keys[] = array("l_part_id", "Part ID");
   //$keys[] = array("l_part_rev", "Part Rev.");
   //$keys[] = array("l_pot_unid", "POT UNID");
   $keys[] = array("l_p_location", "Production Loc. G3");
   $keys[] = array("l_project_name", "Project Name");
   $keys[] = array("l_t_location", "Transferred Location");
   $keys[] = array("l_tool_id", "Tool ID");

   $keys[] = array("p_name", "Part Name");
   $keys[] = array("p_number", "Part Number");

   $keys[] = array("t_dualsource", "Dualsource");
   $keys[] = array("t_mdp", "MDP");
   $keys[] = array("t_pono", "Purchase Order No") ;
   $keys[] = array("t_supplier", "Supplier");
   $keys[] = array("t_datesuppevt", "Supplier conf. EVT");
   $keys[] = array("t_number", "Tool Number");
   $keys[] = array("t_fotsampling", "FOT sampling");
   $keys[] = array("t_followupsampling", "Followup sampling");
   $keys[] = array("t_prodlocconf", "prod. loc. conf.");


   $keys[] = array("c_customer", "Customer");
   $keys[] = array("c_number", "DIS Number");
   $keys[] = array("c_engineer", "Engineer");


   $keys[] = array("cr_number_s1", "CR Number S1");
   $keys[] = array("cr_crreason_s1", "CR Reason S1");
   $keys[] = array("cr_statusir_s1", "Status IR S1");
   $keys[] = array("cr_statusira_s1", "Status IRA S1");
   $keys[] = array("cr_statusirp_s1", "Status IRP S1");
   $keys[] = array("cr_statusirs_s1", "Status IRS S1");
   $keys[] = array("cr_status_s1", "CR Status S1");
   $keys[] = array("cr_loop_s1", "CR Loop S1");

   $keys[] = array("cr_number_s2", "CR Number S2");
   $keys[] = array("cr_crreason_s2", "CR Reason S2");
   $keys[] = array("cr_statusir_s2", "Status IR S2");
   $keys[] = array("cr_statusira_s2", "Status IRA S2");
   $keys[] = array("cr_statusirp_s2", "Status IRP S2");
   $keys[] = array("cr_statusirs_s2", "Status IRS S2");
   $keys[] = array("cr_status_s2", "CR Status S2");
   $keys[] = array("cr_loop_s2", "CR Loop S2");


   $keys[] = array("d_loops_s1_G04070", "Loops Source 1");
   $keys[] = array("d_loops_s2_G04070", "Loops Source 2");
   $keys[] = array("d_date_revised_G03010", "DM-Tec Revised");
   $keys[] = array("d_date_actual_G03010", "DM-Tec Actual");
   $keys[] = array("d_date_revised_G03030", "Pilot FOT Revised");
   $keys[] = array("d_date_actual_G03030", "Pilot FOT Actual");
   $keys[] = array("d_date_revised_G03040", "Pilot Dim Revised");
   $keys[] = array("d_date_actual_G03040", "Pilot Dim Actual");
   $keys[] = array("d_date_revised_G03050", "Pilot Assessment Revised");
   $keys[] = array("d_date_actual_G03050", "Pilot Assessment Actual");
   $keys[] = array("d_date_revised_G03990", "G3 Revised");
   $keys[] = array("d_date_actual_G03990", "G3 Actual");
   $keys[] = array("d_date_revised_G04030", "Concept Appr. Revised");
   $keys[] = array("d_date_actual_G04030", "Concept Appr. Actual");
   $keys[] = array("d_date_revised_G04050", "Final Design Appr. Revised");
   $keys[] = array("d_date_actual_G04050", "Final Design Appr. Actual");
   $keys[] = array("d_date_revised_G04070", "FOT Revised");
   $keys[] = array("d_date_actual_G04070", "FOT Actual");
   $keys[] = array("d_date_revised_G04080", "Dim Revised");
   $keys[] = array("d_date_actual_G04080", "Dim Actual");
   $keys[] = array("d_date_revised_G04090", "Assessment Revised");
   $keys[] = array("d_date_actual_G04090", "Assessment Actual");
   $keys[] = array("d_date_actual_G04100", "Internal Release S1 Actual");
   $keys[] = array("d_date_revised_G05020", "53P Revised");
   $keys[] = array("d_date_actual_G05020", "53P Actual");
   $keys[] = array("d_date_revised_G05050", "PPAP Release Revised");
   $keys[] = array("d_date_actual_G05050", "PPAP Release Actual");
   $keys[] = array("d_date_revised_fot_loop_0", "FOT revised loop 0");
   $keys[] = array("d_date_actual_fot_loop_0", "FOT actual loop 0");


   $keys[] = array("m_doc_status", "Document Status");
   $keys[] = array("m_process_rules", "Process Rules");
   $keys[] = array("m_process_type", "Process Type");
   $keys[] = array("m_unique", "Unique");

if($_REQUEST["m_e4__refresh"] == "true") {
   require_once("../../../../library/tools/addin_xml.php");
   m_e4__ee_report_tng2_refresh($_application);
}

if($_REQUEST["m_e4__xls"] == "true") {
   m_e4__ee_report_tng2_xls($_application);
}

if(isset($_REQUEST["search"]) &&  $_REQUEST["search"] != "") {
   m_e4__ee_report_tng2_embed_search($_application);
}


function m_e4__ee_report_tng2($_application) {

   global $keys;

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = ($_application["page_id"] == "ee_transfer_view") ? "Transfer view report" : "Report"; 
   $headline = "<span ee_report_tng2_embed=\"".$_REQUEST["m_e4__embed"]."\">".$headline."<span>";

   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_fieldlist"])) $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_fieldlist"] = "l_part_rev,l_project_name";
 
   $fieldlist = split(",", $_SESSION["perpage"]["tab"][$_application["page_id"]][__FUNCTION__."_fieldlist"]);

   if(count($l_data) <= 1 || !$apcu) {
      $l_data = file_get_contents("static/report/report.php");
      $l_data = unserialize($l_data);
   }

   $responsible = array("");
   foreach($l_data as $v) {
      if(!in_array($v["tr_responsible"], $responsible)) $responsible[] = trim($v["tr_responsible"]);
   }
   asort($responsible);
   foreach($responsible as $r) {
      $selected = (strtolower(rawurldecode($r)) == $_REQUEST["m_e4__responsible"]) ? " selected" : "";
      $select .= "<option".$selected.">".rawurldecode($r)."</option>\r\n";
   }
   $select = "<select style=\"width:200px; font:normal 13px open sans; height:21px; padding-left:2px;\" form=\"responsible\" onchange=\"do_fade(true); location.href = '?m_e4__responsible=' + escape(this[this.selectedIndex].text.toLowerCase()) + '&m_e4__embed=".$_REQUEST["m_e4__embed"]."'\">\r\n".$select."</select>\r\n";

   foreach($keys as $v) {
      $td[substr($v[0], 0, strpos($v[0], "_"))] .= "<tr><td><img name=\"".$v[0]."\" style=\"position:relative;top:1px;\"></td><td>&nbsp;".$v[1]."</td></tr>";
   }

   $module = 
   "<div>\r\n".
   "<span><a href=\"javascript:m_e4.refresh();\" style=\"text-decoration:none; color:rgb(0,102,158);\" onmouseover=\"this.style.textDecoration='underline';\" onmouseout=\"this.style.textDecoration='none';\">REFRESH</a>&nbsp;&nbsp;Last update: ".date("Y-m-d H:i:s", filemtime("static/report/report.php"))."</span>".
   "<span>&nbsp;&nbsp;|&nbsp;&nbsp;Select T&PM&nbsp;&nbsp;".$select."</span>\r\n";

   if($_REQUEST["m_e4__embed"] == "smartsearch") {
      require_once("modules/m_e4.smartsearch_tng2.php");
      $module .= "&nbsp;&nbsp;|&nbsp;&nbsp;<span style=\"cursor:pointer;\" onclick=\"m_e4.get_excel();\"><img style=\"position:relative; top:3px;\" type=\"nochange\" src=\"../../../../library/images/excel-icon.png\"></span>\r\n".
      "&nbsp;&nbsp;<span style=\"cursor:pointer;\" onclick=\"location.href='?m_e4__responsible=".$_REQUEST["m_e4__responsible"]."';\"><img style=\"position:relative; top:3px;\" type=\"nochange\" src=\"../../../../library/images/16x16/keyboard-13.png\"></span>\r\n".
      "</div>\r\n<br>".m_e4__smartsearch_get_html($_application, $l_data, $fieldlist, $keys);
      return array($headline, $module);
   }

   $h["l"] = "POT";
   $h["tr"] = "Tracker";
   $h["t"] = "Tool";
   $h["p"] = "Part";
   $h["c"] = "Project Case";
   $h["cr"] = "Control Report";
   $h["d"] = "Dates";
   $h["m"] = "Meta";

   $module .=
   "<table border=\"0\" id=\"m_e4.select\"><tr>";
   foreach($td as $key => $val) {
      $module .= "<td style=\"vertical-align:top; width:190px; overflow:hidden; background:rgb(242,242,242);\"><table style=\"width:100%;padding:1px;\"><tr><td colspan=\"2\" style=\"padding:2px; background:rgb(99,177,229);\">".$h[$key]."</td></tr>".$val."</table></td>\r\n";
   }

   $module .= 
   "&nbsp;&nbsp;|&nbsp;&nbsp;<span style=\"cursor:pointer;\" onclick=\"m_e4.get_excel();\"><img style=\"position:relative; top:3px;\" type=\"nochange\" src=\"../../../../library/images/excel-icon.png\"></span>&nbsp;&nbsp;\r\n".
   "<span style=\"cursor:pointer;\" onclick=\"infobox('Loading Smartsearch', 'Processing ...', 180, 80); m_e4.get_smartsearch('".$_REQUEST["m_e4__responsible"]."');\"><img style=\"position:relative; top:3px;\" type=\"nochange\" src=\"../../../../library/images/16x16/content-41.png\"></span>\r\n".
   "<br><br>\r\n".
   "</div>\r\n".
   "</tr></table>\r\n".
   "<span class=\"phpbutton\" style=\"margin-right:7px;\"><a href=\"javascript:m_e4.select(true);\">Select all</a></span>\r\n".
   "<span class=\"phpbutton\"><a href=\"javascript:m_e4.select(false);\">Deselect all</a></span>\r\n";


   return array($headline, $module);
}



function m_e4__ee_report_tng2_xls($_application) {

   global $keys;

   $_REQUEST["count"] = ($_REQUEST["count"] == "") ? 20 : $_REQUEST["count"];
   $key = explode(";", $_POST["key"]);

   //$l_data = apcu_fetch("l_data");
   $include = $key;


   $apcu = false;

   if(count($l_data) <= 1 || !$apcu) {
      $l_data = file_get_contents("../static/report/report.php");
      $l_data = unserialize($l_data);
      //apcu_store("l_data", $l_data);
   }


   foreach($key as $k) {
      foreach($keys as $v) if($v[0] == $k) { $res[] = $v[1]; break; }
   }
    
   $table[0] = $res;
   $count = 1;
   $responsible = ($_REQUEST["m_e4__responsible"] != "") ?  $_REQUEST["m_e4__responsible"] : "";

   foreach($l_data as $row) {
      $res[$count] = array();
      if($responsible == "" || $responsible == strtolower(rawurldecode($row["tr_responsible"]))) {
         $cell = 0;
         foreach($key as $k) {
            $table[$count][$cell] = trim(utf8_encode(rawurldecode($row[$k])));
            $cell++;
         }
      }
      $count++;
      if($count > $_REQUEST["count"]) break;
   }

   require_once("../../../../zip/create.php");
}


function m_e4__ee_report_tng2_refresh($_application) {

   session_start();
   //$load_type = array("tracker", "pot", "meta", "creport", "date");
   $load_type = array("tracker", "pot", "part", "tool", "case", "creport", "date", "meta" );


   $_application["page_id"] = "ee_report_tng2";


      // TRACKER
      if(in_array("tracker", $load_type)) {
         $r_data = load_data_from_view($_SESSION["remote_domino_path_main"]."/v.load.".$_application["page_id"].".tracker?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($r_data as $key => $val) {
            $r_data["key"][] = $key;
            $do = explode(".", $val["tr_ref_pot"]);
            foreach($do as $r) {
               if($r != "") {
                  if(!in_array($r, array_keys($r_data))) $include_pot[$key][] = $r;
               }
            }
         }
      }

      // POT   
      if(in_array("pot", $load_type)) {
         $pot_data = load_data_from_view($_SESSION["remote_domino_path_main"]."/v.load.".$_application["page_id"].".link?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($include_pot as $key => $ref_pot) {
            foreach($ref_pot as $pot) {
               $l_data[$pot."@".$key] = array_merge($pot_data[$pot], $r_data[$key]);
            }
         }
      }
      // Meta 
      if(in_array("meta", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_main"]."/v.load.".$_application["page_id"].".meta?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($load as $key => $val) {
            $l_data[$key] = array_merge($l_data[$key], $val);
         }
      } 
      // Parts
      if(in_array("part", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_epcmain"]."/v.load.".$_application["page_id"].".part?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($l_data as $key => $val) {
           if(isset($val["l_part_id"])) {
              if(is_array($load[$val["l_part_id"]])) $l_data[$key] = array_merge($l_data[$key], $load[$val["l_part_id"]]);
           }
         }
      }
      // Cases
      if(in_array("case", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_epcmain"]."/v.load.".$_application["page_id"].".case?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($l_data as $key => $val) {
           if(isset($val["l_case_unid"])) {
              if(is_array($load[$val["l_case_unid"]])) $l_data[$key] = array_merge($l_data[$key], $load[$val["l_case_unid"]]);
           }
         }
      }
      // Tools
      if(in_array("tool", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_epcmain"]."/v.load.".$_application["page_id"].".tool?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($l_data as $key => $val) {
           if(isset($val["l_tool_id"])) {
              if(is_array($load[$val["l_tool_id"]])) $l_data[$key] = array_merge($l_data[$key], $load[$val["l_tool_id"]]);
           }
         }
      }
      // CREPORTS
      if(in_array("creport", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_epcmain"]."/v.load.".$_application["page_id"].".creport?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($l_data as $key => $val) {
           $t = split("@", $key);
           $key2 = $val["l_pot_unid"]."@".$t[1]."@S1";
           if(is_array($load[$key2])) $l_data[$key] = array_merge($l_data[$key], $load[$key2]);
           $key2 = $val["l_pot_unid"]."@".$t[1]."@S2";
           if(is_array($load[$key2])) $l_data[$key] = array_merge($l_data[$key], $load[$key2]);
         }
      } 
      // Termine Loop 0
      $t = array();
      if(in_array("date", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_main"]."/v.load.".$_application["page_id"].".date_loop_0?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($load as $key => $val) {
            foreach($val as $v2) { $t = array_merge($t, $v2); }
            $l_data[$key] = array_merge($l_data[$key], $t);
         }
      }

      // Termine
      $t = array();
      if(in_array("date", $load_type)) {
         $load = load_data_from_view($_SESSION["remote_domino_path_main"]."/v.load.".$_application["page_id"].".date?open&start=%%START%%&count=%%COUNT%%&function=plain");
         foreach($load as $key => $val) {
            foreach($val as $v2) { $t = array_merge($t, $v2); }
            $l_data[$key] = array_merge($l_data[$key], $t);
         }
      }


   $arr = array();
   foreach($l_data as $val) {
      foreach(array_keys($val) as $key) {
         if(!in_array($key, $arr)) $arr[] = $key;
      }

   }

   $l_data["keys"] = $arr;
   unlink("../static/report/report.php");
   file_put_contents("../static/report/report.php", serialize($l_data));
   
}

function m_e4__ee_report_tng2_embed_search($_application) {

   $_REQUEST["count"] = ($_REQUEST["count"] == "") ? 20 : $_REQUEST["count"];
   $keys = explode(";", $_REQUEST["search"]);
   $include = $keys;
   $q = explode(" ", strtolower($_REQUEST["q"]));

   $apcu = false;


   if(count($l_data) <= 1 || !$apcu) {
      $l_data = file_get_contents("../static/report/report.php");
      $l_data = unserialize($l_data);
   }

   $count = 0;
   foreach($l_data as $row) {
      $cell = 0;
      $found = true;
      foreach($keys as $key) $k = ".".strtolower(rawurldecode($row[$key])).".";

      foreach($q as $needle) if(!strpos($k, $needle)) {$found = false; break;}
      if($found) {   
         foreach($keys as $key) {
            $table[$count][$key] = trim(utf8_encode(rawurldecode($row[$key])));
            $cell++;
         }
         $count++;
         if($count > $_REQUEST["count"]) break;
      }
   }

   if($_REQUEST["d"] == "count") print "100";
   else print "sqsearch.results =".json_encode($table).";";
   
}


?>