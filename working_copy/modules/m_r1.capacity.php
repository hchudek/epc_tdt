<?php

function m_r1__capacity($_application) {
   $html = (in_array("[mgmt_report]", $_SESSION[remote_userroles])) ? m_r1_capacity_add($_application) : "<div style=\"padding:20px;font:normal 13px century gothic;\">ACCESS DENIED</div>";
   return $html;
}

$headline="Capacity Report";

function m_r1_capacity_add($_application) {

   if($_POST["xls_table"] != "") {
      header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");;
      header("Content-Disposition: attachment;filename=export.xls"); 
      header("Content-Transfer-Encoding: binary ");
      print "<table>";
      print "<tr>";
      print "<td>TPM</td>";
      print "<td>PROJECTS</td>";
      print "<td>TRACKER</td>";
      print "<td>POT</td>";
      print "<td>INIDCATOR</td>";
      print "<td>WORKLOAD</td>";
      print "<tr>";
      print "</table>";
  
      print $_POST["xls_table"];
      unset($_POST["xls_table"]);
      die;
   }
   global $report_data;
   $tpm = ($_POST["tpm"] != "") ? explode(";", $_POST["tpm"]) : array();
   $s1 = m_c1_navigation($report_data, $tpm);
   $s2 = (count($tpm) > 0) ?  m_c1_report($_application, $report_data, $tpm) : "<div style=\"padding:24px 0px 0px 20px;color:rgb(0,102,158);font:normal 30px century gothic,verdana;\">Please select at least 1 TPM</div>";
   $html = 
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font:normal 13px century gotic,verdana;\">".
   "<tr>".
   "<td style=\"vertical-align:top;padding-left:10px;\">".$s1."</td>".
   "<td style=\"vertical-align:top;padding-left:10px;\">".$s2."</td>".
   "</tr>".
   "</table>";
   
   return  $html;
}


function m_c1_navigation($report_data, $tpm) {

   $tab = "SELECT TPM";

   foreach($report_data["tpm"] as $val) {
      $img = (in_array($val, $tpm)) ? "status-5": "status-5-1";
      $html .= "<div style=\"margin:4px 2px 4px 2px;\"><img name=\"img_tpm\" onclick=\"handle_tpm(this);\" src=\"".$_SESSION["php_server"]."/library/images/16x16/".$img.".png\" style=\"vertical-align:top;margin-right:4px;\" /><span>".$val."</span></div>";
   }

   $html .=
   "<form name=\"capacity_report\" method=\"post\" style=\"display:none;\">".
   "<input type=\"text\" name=\"tpm\" value=\"".$_POST["tpm"]."\" />".
   "<textarea name=\"xls_table\"></textarea>".
   "</form>".
   "<span class=\"phpbutton\" style=\"margin-top:4px;margin-left:4px;\"><a href=\"javascript:void(0);\" onclick=\"do_select('img_tpm', true);\">Select</a></span>".
   "<span class=\"phpbutton\" style=\"margin-top:4px;\"><a href=\"javascript:void(0);\" onclick=\"do_select('img_tpm', false);\">Deselect</a></span>".
   "<span class=\"phpbutton\" style=\"margin-top:4px;\"><a href=\"javascript:void(0);\" onclick=\"kill_app(true); document.capacity_report.submit();\">Submit</a></span>";

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font:normal 13px century gothic,verdana;width:240px;table-layout:fixed;margin-bottom:22px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\">".$tab."</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_content\" colspan=\"2\">".$html."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";

   return array($headline, $module);
  
}



function m_c1_report($_application, $report_data, $tpm) {

   $tab = 
   "<div style=\"width:210px;float:left;text-align:left;\">TPM</div>".
   "<div style=\"width:80px;float:left;text-align:right;\">Projects</div>".
   "<div style=\"width:80px;float:left;text-align:right;\">Tracker</div>".
   "<div style=\"width:80px;float:left;text-align:right;\">POT</div>".
   "<div style=\"width:80px;float:left;text-align:right;\">Excluded</div>".
   "<div style=\"width:80px;float:left;text-align:right;\">Indicator</div>".
   "<div style=\"width:80px;float:left;text-align:right;\">Workload</div>";

   $p_type = generate_p_type($_application);

   foreach($tpm as $val) {
      $xml = generate_xml($_SESSION["remote_domino_path_main"]."/v.data.report.capacity?open&restricttocategory=R=".rawurlencode($val)."&count=9999&function=xml:data");
      $tracker[$val] = (isset($xml["tracker"][0])) ? $xml : array("tracker" => array($xml["tracker"]));
   }

   foreach($tpm as $name) {
      foreach($tracker[$name]["tracker"] as $val) {
         $xml = generate_xml($_SESSION["remote_domino_path_main"]."/v.data.report.capacity?open&restricttocategory=U=".$val["unique"]."&count=9999&function=xml:data");
         if(!isset($xml["h2"])) {

            $complexity = generate_xml($_SESSION["remote_domino_path_leanpd"]."/v.project_complexity?open&count=1&restricttocategory=".strtolower($val["project_number"])."&function=xml:data");
    

            $kpi_tracker = 0;
            $meta[$name][$val["unique"]] = (isset($xml["meta"][0])) ? $xml : array("meta" => array($xml["meta"]));
            $meta[$name][$val["unique"]]["tracker_number"] = $val["document_number"];
            $meta[$name][$val["unique"]]["project_number"] = $val["project_number"];
            $error = 0;

            for($i = 0; $i < count($meta[$name][$val["unique"]]["meta"]); $i++) {


               $meta[$name][$val["unique"]]["meta"][$i]["complexity"] = $p_type["project"]["complexity"][trim(strtolower($complexity["complexity"]))];
               $meta[$name][$val["unique"]]["meta"][$i]["type_of_tool"] = $p_type[trim(strtolower($meta[$name][$val["unique"]]["meta"][$i]["process_type"]))]["type_of_tool"];
               $meta[$name][$val["unique"]]["meta"][$i]["phase"] = $p_type[trim(strtolower($meta[$name][$val["unique"]]["meta"][$i]["process_type"]))]["phase"];
               $meta[$name][$val["unique"]]["meta"][$i]["kind_of_project"] = $p_type[trim(strtolower($meta[$name][$val["unique"]]["meta"][$i]["process_rules"]))]["kind_of_project"];
               $meta[$name][$val["unique"]]["meta"][$i]["supplier"] = $p_type["supplier_data"][trim($meta[$name][$val["unique"]]["meta"][$i]["unique_meta"])];
               $meta[$name][$val["unique"]]["meta"][$i]["tool_data"] = $p_type["tool_data"][trim($meta[$name][$val["unique"]]["meta"][$i]["supplier"])];

               // DEFAULT 
               $meta[$name][$val["unique"]]["meta"][$i]["dual_source"] = 1.0;
               $meta[$name][$val["unique"]]["meta"][$i]["sourcing"] = 1.0;

               // SUPPLIER VORHANDEN
               if($meta[$name][$val["unique"]]["meta"][$i]["tool_data"]["dualsource"] == "1") {
                  $meta[$name][$val["unique"]]["meta"][$i]["dual_source"] = $p_type["cr_data"]["sourcing"]["dual_source"];
               }
               if(isset($meta[$name][$val["unique"]]["meta"][$i]["tool_data"]["region"])) {
                  $meta[$name][$val["unique"]]["meta"][$i]["sourcing"] = $p_type["cr_data"]["sourcing"][$meta[$name][$val["unique"]]["meta"][$i]["tool_data"]["region"]];

               }

               $v1 = $meta[$name][$val["unique"]]["meta"][$i]["complexity"];
               $v2 = $meta[$name][$val["unique"]]["meta"][$i]["type_of_tool"];
               $v3 = $meta[$name][$val["unique"]]["meta"][$i]["phase"];
               $v4 = $meta[$name][$val["unique"]]["meta"][$i]["kind_of_project"];
               $v5 = $meta[$name][$val["unique"]]["meta"][$i]["dual_source"];
               $v6 = $meta[$name][$val["unique"]]["meta"][$i]["sourcing"];


               $kpi = $v1 * $v2 * $v3 * $v4 * $v5 * $v6;
               if($kpi < 1) $error++;
               

               $kpi_tracker += $kpi;
               $meta[$name][$val["unique"]]["meta"][$i]["kpi"] = $kpi;
            }
            $meta[$name][$val["unique"]]["kpi"] = $kpi_tracker;
            $meta[$name][$val["unique"]]["error"] = $error;
            $meta[$name]["error"] = $meta[$name]["error"] + $error;
         }
      }
   }
   foreach($meta as $key => $val) {
      $kpi = 0;
      foreach($val as $key2 => $val2) {
         $kpi += $val2["kpi"];
      }
      $meta[$key]["kpi"] = $kpi;
      $meta[$key]["tracker"] = count($val);
   }


   // HTML AUSGEBEN
   $html = "<div id=\"report\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n";
   $av = 0;
   foreach($meta as $key => $val) {
      unset($project_number);
      $pot = 0;
      $tracker = $val["tracker"];
      foreach($val as $val2) {
         $pot += count($val2["meta"]);
         if($val2["project_number"] != "" && !in_array($val2["project_number"], $project_number)) $project_number[] = $val2["project_number"];
      }
      $unique = generate_uniqueid(4);
      $project_html = "<a onmouseover=\"handle_project(this, '".$unique."', true);\" onmouseout=\"handle_project(this, '".$unique."', false);\" style=\"cursor:default;\">".count($project_number)."</a><span style=\"position:absolute;text-align:left;background-color:rgB(239,228,176);z-index:9999;filter:alpha(opacity=92);padding:4px;border:solid 1px rgb(99,99,99);visibility:hidden;\"></span>";
      $p_html[$key] = "<div id=\"".$unique."\" style=\"display:none;\">";
      foreach($project_number as $val2) {
         $p_html[$key] .= "<div>&nbsp;".$val2."&nbsp;</div>";
      }
      $p_html[$key] .= "</div>";
      $av += $val["kpi"];


      if($val["kpi"] == 0) {
         $pot = 0;
         $tracker = 0;
      }


      $error = ($val["kpi"] == 0) ? 0 : $val["error"];

      $html .= 
      "<tr>\r\n".
      "<td style=\"width:210px;\">".$key."</td>\r\n".
      "<td style=\"width:80px;text-align:right;\">".$project_html."</td>\r\n".
      "<td style=\"width:80px;text-align:right;\">".$tracker."</td>\r\n".
      "<td style=\"width:80px;text-align:right;\">".$pot."</td>\r\n".
      "<td style=\"width:80px;text-align:right;\">".$error."</td>\r\n".
      "<td style=\"width:80px;text-align:right;\"><a name=\"tpm_kpi\">".number_format($val["kpi"], 2, ",", "")."</a></td>\r\n".
      "<td style=\"width:80px;text-align:right;\"><a name=\"tpm_workload\"></a></td>\r\n".
      "</tr>\r\n";
   }
   $av = $av / count($meta);
   $html .= "<tr><td colspan=\"5\" style=\"border-top:solid 1px rgb(99,99,99);\">Average</td><td id=\"kpi_av\" style=\"text-align:right;border-top:solid 1px rgb(99,99,99);\">".number_format($av, 2, ",", "")."</td><td id=\"wl_av\" style=\"text-align:right;border-top:solid 1px rgb(99,99,99);\"></td></tr>";
   $html .= "</table></div>\r\n";

   foreach($p_html as $val) {
      $html .= $val;
   }


   //SLIDER
   $f_wl = "3.00";
   $v_wl = "0.20";
   $html .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top:40px;\">".
   "<tr>".
   "<td>Fixed workload:</td></td>".
   "<td style=\"width:40px;\">[<a name=\"add_fixed_workload\">".implode($p_type["cr_data"]["slider"]["fixed"],"/")."</a>]</td>".
   "<td id=\"sliderbox1\"></td>\r\n".
   "<td rowspan=\"2\" style=\"padding:6px 0px 0px 40px;vertical-align:top;\"><span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"do_export('report', 'xls_table');\">Export to Excel</a></span></td>\r\n".
   "</tr>".
   "<tr>".
   "<td>Variable workload:</td>".
   "<td>[<a name=\"add_variable_workload\">".implode($p_type["cr_data"]["slider"]["variable"],"/")."</a>]</td>".
   "<td id=\"sliderbox2\"></td>\r\n".
   "</tr>".
   "</table>";


   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font:normal 13px century gothic,verdana;width:100%;table-layout:fixed;margin-bottom:22px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\">".$tab."</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_content\" style=\"background-color:rgb(255,255,255);border:none;\" colspan=\"2\">".$html."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";

   return $module;

}


function generate_p_type($_application) {

   global $capacity_report_data; $cr_data = $capacity_report_data;
   foreach(array_keys($_application["process"]["type"]) as $key) {
      $key = strtolower($key);
      switch($key) {
         case "con.kit":
            $val["complexity"] = 1.0;
            $val["type_of_tool"] = $cr_data["process_type"]["conv_kit"]["type_of_tool"];
            $val["phase"] = $cr_data["process_type"]["conv_kit"]["phase"];
            break;
         case "dupl./repl.":
            $val["complexity"] = 1.0;
            $val["type_of_tool"] = $cr_data["process_type"]["dupl_repl"]["type_of_tool"];
            $val["phase"] = $cr_data["process_type"]["dupl_repl"]["phase"];
            break;
         case "modification": 
            $val["complexity"] = 1.0;
            $val["type_of_tool"] = $cr_data["process_type"]["modification"]["type_of_tool"];
            $val["phase"] = $cr_data["process_type"]["modification"]["phase"];
            break;
         case "new project": 
            $val["complexity"] = 1.0;
            $val["type_of_tool"] = $cr_data["process_type"]["new_project"]["type_of_tool"];
            $val["phase"] = $cr_data["process_type"]["new_project"]["phase"];
            break;
         case "pilot": 
            $val["complexity"] = 1.0;
            $val["type_of_tool"] = $cr_data["process_type"]["pilot"]["type_of_tool"];
            $val["phase"] = $cr_data["process_type"]["pilot"]["phase"];
            break;
         case "purchase part": 
            $val["complexity"] = 1.0;
            $val["type_of_tool"] = $cr_data["process_type"]["purchase_part"]["type_of_tool"];
            $val["phase"] = $cr_data["process_type"]["purchase_part"]["phase"];
            break;
         case "packaging": 
            $val["complexity"] = 1.0;
            $val["type_of_tool"] = $cr_data["process_type"]["packaging"]["type_of_tool"];
            $val["phase"] = $cr_data["process_type"]["packaging"]["phase"];
            break;
      }
      $p_type[$key]["complexity"] = $val["complexity"];
      $p_type[$key]["type_of_tool"] = $val["type_of_tool"];
      $p_type[$key]["phase"] = $val["phase"];
      $p_type[$key]["phase"] = 1.0;
//print_r($cr_data);die;
   }

   foreach(array_keys($_application["process"]["rules"]) as $key) {
      $key = strtolower($key);
      switch($key) {
         case "assembly  difficult":
            $val["kind_of_project"] = $cr_data["process_rules"]["assembly_difficult"]["kind_of_project"];
            break;
         case "assembly  easy":
            $val["kind_of_project"] = $cr_data["process_rules"]["assembly_easy"]["kind_of_project"];
            break;
         case "assembly  mid": 
            $val["kind_of_project"] = $cr_data["process_rules"]["assembly_mid"]["kind_of_project"];
            break;
         case "modifikation": 
            $val["kind_of_project"] = $cr_data["process_rules"]["modifikation"]["kind_of_project"];
            break;
         case "molding difficult": 
            $val["kind_of_project"] = $cr_data["process_rules"]["molding_difficult"]["kind_of_project"];
            break;
         case "molding easy": 
            $val["kind_of_project"] = $cr_data["process_rules"]["molding_easy"]["kind_of_project"];
            break;
         case "molding mid": 
            $val["kind_of_project"] = $cr_data["process_rules"]["molding_mid"]["kind_of_project"];
            break;
         case "plating": 
            $val["kind_of_project"] = $cr_data["process_rules"]["plating_mid"]["kind_of_project"];
            break;
         case "purchase part": 
            $val["kind_of_project"] = $cr_data["process_rules"]["purchchase_part"]["kind_of_project"];
            break;
         case "stamping difficult": 
            $val["kind_of_project"] = $cr_data["process_rules"]["stamping_difficult"]["kind_of_project"];
            break;
         case "stamping easy": 
            $val["kind_of_project"] = $cr_data["process_rules"]["stamping_easy"]["kind_of_project"];
            break;
         case "stamping mid": 
            $val["kind_of_project"] = $cr_data["process_rules"]["stamping_mid"]["kind_of_project"];
            break;
         case "packaging": 
            $val["kind_of_project"] = $cr_data["process_rules"]["packaging"]["kind_of_project"];
            break;
      }
      $p_type[$key]["kind_of_project"] = $val["kind_of_project"];
   }
   $p_type["project"]["complexity"]["a"] = $cr_data["project"]["complexity_a"];
   $p_type["project"]["complexity"]["b"] = $cr_data["project"]["complexity_b"];
   $p_type["project"]["complexity"]["c"] = $cr_data["project"]["complexity_c"];
   $p_type["project"]["complexity"]["d"] = $cr_data["project"]["complexity_d"];
   $p_type["project"]["complexity"]["e"] = $cr_data["project"]["complexity_e"];
   $p_type["project"]["complexity"]["f"] = $cr_data["project"]["complexity_f"];
   $p_type["project"]["complexity"]["g"] = $cr_data["project"]["complexity_g"];


   // TOOLDATEN
   $tmp = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/a.get_tool_capacity_report?open");
   $tmp_tool = explode("//", $tmp);
   $c = count($tmp_tool);

$c = 1;
   for($i = 0; $i < $c; $i++) {
      $tmp = explode("/", $tmp_tool[$i]);
      $tmp[0] = trim($tmp[0]);
      if($tmp[0] !="") {
         $tool[$tmp[0]]["supplier"] = trim(rawurldecode($tmp[1]));
         $tool[$tmp[0]]["dualsource"] = trim($tmp[2]);
         $tool[$tmp[0]]["region"] = trim(rawurldecode($tmp[3]));
      }
   }

   // SUPPLIER REGION
   $tmp = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/a.get_supplier?open");
   $tmp_supplier = explode("//", $tmp);
   $c = count($tmp_supplier);
   for($i = 0; $i < $c; $i++) {
      $tmp = explode("/", $tmp_supplier[$i]);
      $tmp[0] = trim($tmp[0]);
      if($tmp[0] !="") {
         $supplier[$tmp[0]] = trim(rawurldecode($tmp[1]));
      }
   }


   $p_type["tool_data"] = $tool;
   $p_type["supplier_data"] = $supplier;
   $p_type["cr_data"] = $cr_data;

   return $p_type;
}
  


?>