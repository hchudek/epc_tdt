<?php


function m_u3_configure_userpages($_application) {

   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   $sel_tab=($_REQUEST[$_SESSION["module"][$_module_name]."_tab"]=="") ? "2" : $_REQUEST[$_SESSION["module"][$_module_name]."_tab"]; 
   $_t_tab_names[]="Userpages";

   include_once("../../../library/tools/addin_xml.php");

   $xml = file_get_contents($_SESSION["remote_domino_path_main"]."/v.help_lookup?open&count=99999&function=xml:data");
   $xml_obj = simplexml_load_string($xml);
   $arr_xml = convert_xml_into_array($xml_obj); 

   $languages = array_unique($arr_xml["language"]); sort($languages);


switch($sel_tab) {



   case "2":
     $t_view="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"view_tbl_small\">";
      foreach($_application["page"] as $value) {
         $configure="<img src=\"../../../../library/images/icons/configure.gif\" style=\"border:none;margin-left:6px;margin-right:6px;position:relative;top:2px;\" />";
         $txt=($_application["page"][$value]["status"]=="inactive") ? $_application["page"][$value]["status"] : $_application["page"][$value]["page_descr"];
         if($_application["page"][$value]["type"]=="userpage") $t_view.="<tr><td>".$configure."<a href=\"handle_userpage.php?page=".base64_encode($value)."\" class=\"innerlink\">".ucfirst(substr($value,0,strlen($value)-1)." ".substr($value,strlen($value)-1,1)).": ".ucfirst($txt)."</a></td></tr>";
      }
      $t_view.="</table>";
      break;
}

$tabs=
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tabs\">".
"<tr>";

for($i=0; $i<count($_t_tab_names);$i++) {
   $class=($sel_tab==$i+1) ? "high" : "reg";
   $style=($sel_tab==$i+1) ? " style=\"background-color:#822433;\"" : "";
   $tabs.="<td class=\"".$class."\"".$style."><a href=\"?".str_replace("&".$_SESSION["module"][$_module_name]."_tab=".$_REQUEST[$_SESSION["module"][$_module_name]."_tab"],"",$_SERVER["QUERY_STRING"])."&".$_SESSION["module"][$_module_name]."_tab=".($i+1)."\">".$_t_tab_names[$i]."</a></td>";
}

$tabs.=
"</tr>".
"</table>";

$module=
"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"configure_userpages\" style=\"width:220px;margin-bottom:18px;\">\r\n".
"   <tr>\r\n".
"      <td class=\"module_5_headline\">%%TABS%%</td>\r\n".
"   </tr>\r\n".
"   <tr>\r\n".
"      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
"   </tr>\r\n".
"   <tr>\r\n".
"      <td colspan=\"2\" class=\"module_5_content\"><div style=\"border:solid 1px rgb(200,200,200);position:relative;top:-11px;\" class=\"content\">".$t_view."</div></td>\r\n".
"   </tr>\r\n".
"</table>\r\n";


$module=str_replace("%%TABS%%", $tabs, $module);

   return $module;


}



?>