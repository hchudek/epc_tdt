<?php


function m_x8__dig_rep_reporting($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   // Module Body --------------------------------------------------------------------------------------------------
    $module = m_x8__dig_rep_reporting_body($_application);
   return array($headline, $module);
}


function m_x8__dig_rep_reporting_body($_application) {

   require_once("../../../library/tools/addin_xml.php");
   session_start();
   $data = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/p.get_column?open&column=v.get_cr_rms_dig_rep_selected_month");

   $categories = json_decode($data);
   if($_REQUEST["get"] == "") $_REQUEST["get"] = $categories[0];
   
   $dig_rep = create_dig_rep($_application);

   $module = "\r\n".
   "<script src=\"".$_SESSION["php_server"]."/library/addin/chartjs2.4/chart.bundle.js\"></script>\r\n".
   "<script src=\"".$_SESSION["php_server"]."/library/addin/chartjs2.4/utils.js\"></script>\r\n".
   "<table><tr><td style=\"vertical-align:top;\">".
   "<div id=\"container\">\r\n".
   "<canvas id=\"canvas\"></canvas>\r\n".
   "</div></td>\r\n".
   "<td style=\"vertical-align:top; padding-left:30px; padding-top:10px;\"><div selected=\"".$_REQUEST["get"]."\" categories=\"".str_replace("\"", "'", trim($data))."\" style=\"display:none;\" rdo=\"".implode(",", $dig_rep[1])."\" id=\"m_x8__dig_rep_reporting_tbl\">".json_encode($dig_rep[0])."</div></td></tr></table>\r\n".
   "<div id=\"m_x8__dig_rep_reporting_full\" style=\"display:none;\">".json_encode($dig_rep[2])."</div>\r\n";

   return $module;
}




function create_dig_rep($_application) {

   require_once("../../../library/tools/addin_xml.php");
   session_start();

   if(!isset($_SESSION["perpage"]["tab"][$_application["page_id"]]["m_x8__dig_rep_reporting_cockpit_type"])) $chart = 0;
   else $chart = $_SESSION["perpage"]["tab"][$_application["page_id"]]["m_x8__dig_rep_reporting_cockpit_type"];

   $type = ($_SESSION["perpage"]["tab"][$_application["page_id"]]["m_x8__dig_rep_reporting_cockpit_mdp"] == "1") ? "mdp" : "none-mdp";

   if($chart == "0") {
      $get = $_REQUEST["get"];

      $data = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_cr_rms_dig_rep_selected_month?open&restricttocategory=".rawurlencode($get)."&count=99999&function=plain");
      $row = explode(":", $data);
      $key = explode(";", $row[0]);
      if(trim($data) == "<h2>No documents found</h2>") {
         return "No data";
      }
   }
   else {
      $use_rdo = $_REQUEST["get"];
      $row = array();
      for($i = 0; $i < 12; $i++) {
         $date = date("Y-m", strtotime("-".$i." months"));
         $data = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_cr_rms_dig_rep_selected_month?open&restricttocategory=".rawurlencode($type.":".$date)."&count=99999&function=plain");
         if(trim($data) != "<h2>No documents found</h2>") {
            $r = explode(":", $data);
            $key = explode(";", $r[0]);
            if(!isset($row[0])) $row[0] = implode(";", $key); 
            for($e = 1; $e < count($r); $e++) {
               if(trim($r[$e]) != "") $row[] = $r[$e];
            }
         }
      }
   }


   $data = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rdo.user?open&count=99999&function=plain");
   $rdo = json_decode("{".substr($data, 0, strrpos($data, ","))."}", true);
   unset($data);

   // CREATE ARRAY --------------------------------------------------------------------------------------------
   for($i = 1; $i <= count($row); $i++) {
      if($row[$i] != "") {
         $cell = explode(";", $row[$i]);
         for($e = 0; $e <= count($cell); $e++) {
            if(isset($key[$e])) $data[$i - 1][strtolower($key[$e])] = trim(rawurldecode($cell[$e]));
         }      
      }
   }

   // ADD DATE CALCULATION ------------------------------------------------------------------------------------
   foreach($data as $k => $v) {
      $d[0] = strtotime($v["date samples"]);
      $d[1] = strtotime($v["dim report date"]);
      $d[2] = strtotime($v["date dig rep"]);
      $d[3] = strtotime($v["created"]);
      $data[$k]["calc"]["lower"] = date("Y-m-d", max($d));
      if($v["assessment"] != "") {
         $a = strtotime($v["assessment"]);
         $data[$k]["calc"]["upper"] = date("Y-m-d", $a);
         $data[$k]["calc"]["workingdays"] = calc_workingdays($data[$k]["calc"]["lower"], $data[$k]["calc"]["upper"]);
      }
   }


   // ADD RDO -------------------------------------------------------------------------------------------------
   foreach($data as $k => $v) {
     $data[$k]["calc"]["rdo"] = (isset($rdo[$v["releaser"]])) ? $rdo[$v["releaser"]] : "Unknown";
   }

   $i = 0;
   $table[0] = $key;
   array_push($table[0], "Workingdays", "RDO");

   foreach($data as $v) {
      $i++;
      if(isset($v["calc"]["workingdays"])) {
      if(!in_array($v["calc"]["rdo"], $allrdo)) $allrdo[] = $v["calc"]["rdo"];
         if($chart == "1" && $v["calc"]["rdo"] == $use_rdo || $chart == "0") {
            foreach($key as $k) {
               $table[$i][] = $v[strtolower($k)];
            }
            array_push($table[$i], $v["calc"]["workingdays"], $v["calc"]["rdo"]);
         }
      }
   }
   sort($allrdo);
   if(!in_array("Unknown", $allrdo)) $allrdo[] = "Unknown";


   // CREATE RDO AVERAGE ---------------------------------------------------------------------------------------
   $i = 0;
   foreach($data as $v) {
      if(isset($v["calc"]["workingdays"])) {
         $i++;
         if($chart == "0") {
            if(!isset($report[$v["calc"]["rdo"]])) $report[$v["calc"]["rdo"]] = array($v["calc"]["rdo"], 0, 0, 0, 99999, 0);
            $report[$v["calc"]["rdo"]][1] += $v["calc"]["workingdays"];
            $report[$v["calc"]["rdo"]][2] += 1;
            $report[$v["calc"]["rdo"]][3] = $report[$v["calc"]["rdo"]][1] / $report[$v["calc"]["rdo"]][2];
            if($v["calc"]["workingdays"] < $report[$v["calc"]["rdo"]][4]) $report[$v["calc"]["rdo"]][4] = $v["calc"]["workingdays"];
            if($v["calc"]["workingdays"] > $report[$v["calc"]["rdo"]][5]) $report[$v["calc"]["rdo"]][5] = $v["calc"]["workingdays"];
         }
         else {
            if($v["calc"]["rdo"] == $use_rdo) {
               $key = date("Y-m", strtotime($v["assessment"]));
               if(!isset($report[$key])) $report[$key] = array($key, 0, 0, 0, 99999, 0);
               $report[$key][1] += $v["calc"]["workingdays"];
               $report[$key][2] += 1;
               $report[$key][3] = $report[$key][1] / $report[$key][2];
               if($v["calc"]["workingdays"] < $report[$key][4]) $report[$key][4] = $v["calc"]["workingdays"];
               if($v["calc"]["workingdays"] > $report[$key][5]) $report[$key][5] = $v["calc"]["workingdays"];
            }
         }
      }
   }

   if($chart == "1") $report = array_reverse($report);


   $table2[] = array("RDO", "Workingdays", "Transactions", "Average");  
   foreach($report as $k => $v) {
      $table2[] = $v;
   }

   return array($report, $allrdo, $table);


}




//$xls = "dig_report_responsible1.xlsx";
//require_once("../../../../zip/create.php");



function calc_workingdays($lower, $upper) {

   $lower = strtotime($lower);
   $upper = strtotime($upper);

   $d = 0; while($lower < $upper) {
      if(!in_array(date("N", $lower), array(6, 7))) $d++;
      $lower += 86400;
   } 
   return $d;
}




?>