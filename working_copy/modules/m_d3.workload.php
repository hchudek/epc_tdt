<?php

function m_d3__workload($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   $module = 
   "<table id=\"tbl_".__FUNCTION__."\">\r\n".
   "<tr>\r\n".
   "<td>Report type</td>\r\n".
   "<td><select onchange=\"m_d3__workload.settings.type = this[this.selectedIndex].value;\">\r\n".
   "<option value=\"planned\">Planned tasks</option><option value=\"completed\">Completed tasks</option>\r\n".
   "</select>\r\n".
   "</td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td>Start from (0= current week)</td>\r\n";
   for($i = 52; $i >= -52; $i--) {
      $selected = ($i == 0) ? " selected" : ""; 
      $option .= "<option".$selected.">".$i."</option>\r\n";
   }
   $module .= 
   "<td><select onchange=\"m_d3__workload.settings.adjust = this[this.selectedIndex].text.trim();\">".$option."</select></td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td>Include previous weeks</td>\r\n";
   for($i = 1; $i <= 520; $i++) {
      $selected = ($i == 0) ? " selected" : ""; 
      $option2 .= "<option".$selected.">".$i."</option>\r\n";
   }
   $module .= 
   "<td><select onchange=\"m_d3__workload.settings.history = this[this.selectedIndex].text.trim();\">".$option2."</select></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n".
   "<span class=\"phpbutton\"><a href=\"javascript:m_d3__workload.submit();\">Get report</a></span>\r\n";

   return array($headline, $module);
}

?>