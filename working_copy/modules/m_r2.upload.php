<?php

header("HTTP-ACCEPT: */*");

function m_r2_upload($_application) {
  return (in_array("[pac_report]", $_SESSION[remote_userroles])) ? m_r2_upload_add($_application) : "";
}

function m_r2_upload_add($_application) {


   global $tc_data;
   $root = "c:\\inetpub\\wwwroot\\apps\\epc\\tdtracking2\\files\\";
   $allowed_types = "(xls|xlsx)";

   // *********************
   // START: FILE SPEICHERN
   // *********************
   if (!empty($_FILES) && $_POST["unique"] == "") {
      $_POST["unique"] = generate_uniqueid(16);
      $_POST["result"] = generate_uniqueid(16);
      if(preg_match("/\." . $allowed_types . "$/i", $_FILES["file"]["name"])) {
         $destination = $root.$_POST["unique"].".xls";
         if(!move_uploaded_file($_FILES["file"]["tmp_name"], $destination)) $content = "UPLOAD FAILED";
         if(!copy($root."templates\\emea_pm.xls", $root.$_POST["result"].".xls")) $content = "TEMPLATE NOT FOUND"; 
      }
      else $content = "<div style=\"padding-top:10px;\">&nbsp;Wrong file</div>";
   }
                    
   // ********************
   // ENDE: FILE SPEICHERN
   // ********************


   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;


   $content .= "<form name=\"file_upload\" method=\"post\" enctype=\"multipart/form-data\" style=\"padding:10px 4px 0px 4px;\"><div style=\"position:1absolute;left:0px;\">";
   if($_POST["unique"] != "") {
      $content .= "<div id=\"info_process\" style=\"z-index:99999;position:absolute;\"><img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"margin-right:6px;vertical-align:top;\" />Processing ...</div>\r\n";
      $content .= "<script type=\"text/javascript\" src=\"".$_SESSION["remote_domino_path_excel_db"]."/a.dis_xls?open&unique=".$_POST["unique"]."&result=".$_POST["result"]."\"></script>\r\n";
      unset($_POST);
      unset($_FILES);
   }
   else {
       $content .= "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"300000\" />\r\n".
       "<input type=\"file\" onchange=\"document.getElementsByTagName('body')[0].style.visibility='hidden'; document.file_upload.submit();\" name=\"file\" style=\"cursor:pointer;width:0px;position:absolute;z-index:2;filter:alpha(opacity=0);\"/></div><span class=\"phpbutton\"><a>Upload file</a></span>";
   }
   $content .= "<input type=\"hidden\" name=\"unique\" value=\"".$_POST["unique"]."\" />\r\n";
   $content .= "</form><br />\r\n";

   // Überschrift und Modulsonderzubehör erstellen
   $img = ($file) ? "files-35" : "files-25";

   $onclick = ($file) ? "kill_app(true);location.href='addin/do.php?do=delete&file=".base64_encode(str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, strrpos($_SERVER["SCRIPT_FILENAME"], "\\") + 1)).$file)."&redirect=".base64_encode("edit_tracker.php?".$_SERVER["QUERY_STRING"])."';" : "void(0);";
   $cursor = ($file) ? "pointer" : "default";
   $tabs="<img src=\"../../../../library/images/16x16/".$img.".png\" style=\"border:none;position:relative;top:2px;cursor:".$cursor.";\" onclick=\"".$onclick."\" />&nbsp;&nbsp;<span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\">Upload file</span>";

   

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;table-layout:fixed;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_1_content\" colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   return $module;

}




?>