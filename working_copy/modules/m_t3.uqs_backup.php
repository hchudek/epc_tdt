<?php


function m_t3__uqs($_application) {

   if($_REQUEST["pot"] != "") $_SESSION["pot"] = $_REQUEST["pot"];
   if($_REQUEST["unique"] != "") $_SESSION["tracker"] = $_REQUEST["unique"];

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   return array($headline, $module);

}


function m_t3__uqs_generate_html($_application) {

   global $tc_data;  

   // IS INSPECTION POT ? ---------------------------------------------------------------------------------------------
   $pot_status = explode("@", $tc_data["pot"]["status"]); 
   $inspection_pot = ($pot_status[1] != $_SESSION["tracker"]) ? $pot_status[1] : false;

   if(!$inspection_pot == false) {
      $tc_data["meta"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:meta/".$_SESSION["pot"]."@".$inspection_pot."?open");
   }

 
   if (trim(implode($tc_data["uqs"]), ",") == "") {
     $module = "&nbsp;";
   } 
   else {
   
      $pot_status["000"] = "Ungrabbed";
      $pot_status["100"] = "Grabbed";
      $pot_status["200"] = "Unrealized";
      $pot_status["300"] = "Reconditioned";
      $pot_status["999"] = "Released";
      $pot["status"] = explode("@", $tc_data["pot"]["status"]);

      $dsp_type = (is_array($tc_data["meta"]["process_type"])) ? "" : $_application["process"]["displayed_name"][$tc_data["meta"]["process_type"]]." [".str_replace("v->2", " Version 2", $tc_data["meta"]["process_type"])."]";
      $dsp_rule = (is_array($tc_data["meta"]["process_rule"])) ? "" : $_application["process"]["rules"][$tc_data["meta"]["process_rule"]]["displayed_name"]." [".$tc_data["meta"]["process_rule"]."]";


      $uqs["number"] = (is_array($tc_data["uqs"]["unid"])) ? "&nbsp;" : "<a href=\"".$_SESSION["remote_domino_patd_epcquoting"]."/_/".$tc_data["uqs"]["unid"]."?open\" target=\"_blank\">".rawurldecode($tc_data["uqs"]["number"])."</a>";
      $uqs["name"] = (is_array($tc_data["uqs"]["unid"])) ? "&nbsp;" : rawurldecode($tc_data["uqs"]["name"]);

      if(!$inspection_pot == false) {
         $module = "<div style=\"margin-bottom:12px; padding:2px; margin-left:6px;\"><img src=\"../../../../library/images/16x16/edition-29.png\" style=\"position:relative;left:-5px; vertical-align:top;\">Inspection only</div>\r\n";
      }

      $module .= 
      "<table id=\"tbl_m_t3__uqs\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
      "<td>ID<img src=\"../../../../library/images/16x16/edition-43.png\" onclick=\"m_t3__tracking_case.delete(false, '".$inspection_pot."');\" style=\"vertical-align:top; position:relative; top:2px; left:89px; cursor:pointer;\" /></td>\r\n".
      "<td>".str_replace("+", "", $tc_data["meta"]["id"])."</td>\r\n".
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>Type</td>\r\n".
      "<td>".$dsp_type."</td>\r\n".
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>Rule</td>\r\n".
      "<td>".$dsp_rule."</td>\r\n".
      "</tr>\r\n".
      "<td>POT status</td>\r\n".
      "<td><a href=\"?unique=".$pot["status"][1]."&pot=".$tc_data["pot"]["unique"]."\">".$pot_status[$pot["status"][0]]."</a></td>\r\n".
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>Project case</td>\r\n".
      "<td><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcasesbynumber/".$tc_data["meta"]["disno"]."\" target=\"_blank\">".$tc_data["meta"]["disno"]."</a></td>\r\n".
      "</tr>".
      "<tr>\r\n".
      "<td><nobr>UQS Number";

       if($inspection_pot == false) $module .= "<img src=\"../../../../library/images/16x16/interface-78.png\" onclick=\"uqs.connect('".$tc_data["uqs"]["pot_unid"]."');\" style=\"vertical-align:top; position:relative; top:2px; left:22px; cursor:pointer;\" /></td>\r\n";

      $module .=
      "<td>".$uqs["number"]."&nbsp;</td>\r\n".
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>UQS name</td>\r\n".
      "<td>".$uqs["name"]."</td>\r\n".
      "<tr>\r\n".
      "</table>";
   }
   return str_replace("Array","",$module);

}

?>

