<?php

function m_a1__navigator($_application) {

//print_r($_COOKIE);

   $t_module=str_replace(".php", "", basename(__FILE__));
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   $module.= "%%CONTENT%%";

   if(!in_array($t_module, $_hide_modules)) {
      $n_modules=implode(",",$_hide_modules);
      $n_modules=($n_modules=="") ? $t_module : $n_modules.",".$t_module; 
      $t_replace[0]="action_portlet_collapse.gif";
      $tmp=str_replace("hide_module=".$_REQUEST["hide_module"], "", $_SERVER["QUERY_STRING"]);
      $t_replace[1]=($tmp=="") ? "?hide_module=".$n_modules : "?".$tmp."&hide_module=".$n_modules;
      $t_replace[2]="<div class=\"module_content\">\r\n%%NAVIGATION%%<div><img src=\"../../../../library/images/blank.gif\" style=\"width:4px;\" /></div></div>\r\n";

      $navigation="";


      foreach($_application["page"] as $value) {
         if(is_string($value)) {
            if(get_myarea($_application, $value)) {
               if(check_userrole($_application["page"][$value]["access_user_role"]) || !isset($_application["page"][$value]["access_user_role"])) {
                  if($_application["page"][$value]["navigator"]["level"]) {
                     $t_class=($value==$_application["page_id"]) ? "navigator_high" : "navigator_reg";
                     if(isset($_application["page"][$value]["link_to"])) $t_class = str_replace("navigator_", "navigator_link_", $t_class);
                     $t_attributes=(isset($_application["page"]["link"][$value])) ? $_application["page"]["link"][$value] : "";
                     $t_attributes = str_replace("DOLLAR", "$", $t_attributes);
                     $txt = ($t_attributes != "") ? "*" : ""; 
                     $navigation.="<div class=\"".$t_class."\"><a href=\"".$value.".php".$t_attributes."\" onclick=\"do_fade(true);\">".$_application["page"][$value]["page_descr"].$txt."</a></div>";
                  }
               }
            }
         }
      }

      $t_replace[2]=str_replace("%%NAVIGATION%%", $navigation, $t_replace[2]);
   }
   else {
      $n_hide_module=str_replace($t_module, "", str_replace(",".$t_module, "", $_REQUEST["hide_module"]));
      $t_replace[0]="action_portlet_expand.gif";
      $t_replace[1]="?".str_replace("hide_module=".$_REQUEST["hide_module"], "hide_module=".$n_hide_module, $_SERVER["QUERY_STRING"]);

   }

   $replace=split(",", "%%IMAGE%%,%%HREF%%,%%CONTENT%%");
   $module="<div>".str_replace($replace, $t_replace, $module)."</div>";

   return array($headline, $navigation);

}


?>