<?php

session_start();
ob_start("ob_gzhandler");											// Komprimierung aktivieren
header("Content-Type: text/html; charset=UTF-8"); 
require_once("../../../../library/tools/addin_xml.php");

$form = file_get_authentificated_contents($_SESSION["remote_domino_path_leanpd"]."/f.w.eng_project?open");
print str_replace("action=\"", "action=\"".$_SESSION["php_server"].":9220", str_replace("time-3.png", "../../../../library/images/16x16/time-3.png", $form));

?>