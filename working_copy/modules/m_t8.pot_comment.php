<?php


function m_t8__pot_comment($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   return array($headline, $module);

}


function m_t8__pot_comment_generate_html($_application) {

   global $tc_data;   

   $module = 
   "<table id=\"tbl_m_t8__pot_comment\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\r\n".
   "<tr>\r\n".
   "<td><div contenteditable=\"true\" onblur=\"post.save_field('', '".$tc_data["pot"]["unid"]."', 'pot_comment', this.textContent);\">".$tc_data["pot"]["pot_comment"]."</td>\r\n".
   "</tr>\r\n".
   "</table>\r\n";
   
   return str_replace("Array", "", $module);


}

?>

