<?php
function te_breadcrumbing($_application) {

   $r = "";
   return $r;

   $dsp_breadcrumbing = ($_application["breadcrumbing"] == "0") ? "display:none;" : "";

   foreach($_application["page"][$_application["page_id"]]["template"]["bp"] as $key => $val) {
      $tab[] = array("SYSTEM", $key);
   }
   foreach($_SESSION["perspectives"][$_application["page_id"]] as $key => $val) {
      $tab[] = array("CUSTOM", $key, $val);
   }
   $_SESSION["perpage"][$_application["page_id"]."_tab"] = (isset($_SESSION["perpage"][$_application["page_id"]."_tab"])) ? $_SESSION["perpage"][$_application["page_id"]."_tab"] : "1";

   foreach($tab as $val) $count[$val[0]] = 0;
   $prv_tab = "SYSTEM_1";

   foreach($tab as $val) {
      $count[$val[0]]++;
      $class = ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "tab_high_".$val[0] : "tab_reg_".$val[0];
      $onmouseover = ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "" : "this.className='tab_high_prv_".$val[0]."'";
      $onmouseout = ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "" : "this.className='tab_reg_".$val[0]."'";
      $onclick= ($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "" : "d=document.perpage.active;d.value='".$val[0]."_".$count[$val[0]]."';save_perpage('', true);"; 
      $content .= 
      "<div class=\"".$class."\" onclick=\"".$onclick."\" onmouseover=\"".$onmouseover."\" onmouseout=\"".$onmouseout."\">";
      if($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"] && $_application["grid"]["enabled"] && $val[0] == "SYSTEM") {
         $reset = str_replace(".", "__", implode($_application["page"][$_application["page_id"]]["template"]["bp"][$val[1]], "/"));
         $content .= "&nbsp;&nbsp;<img src=\"".$_SESSION["php_server"]."/library/images/16x16/design-21.png\" style=\"position:absolute;cursor:pointer;\" onclick=\"d=document.perpage.".$val[0]."_".$count[$val[0]].";d.value='".$reset."';save_perpage('SYSTEM', true);\" />&nbsp;&nbsp;&nbsp;";
      }
      if($val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"] && $_application["grid"]["enabled"] && $val[0] == "CUSTOM") {
         $exp = explode(",", $val[2]);
         if(count($exp) > 1) {
            foreach(explode("/", $val[2]) as $gm) {
               $added_modules[] = substr($gm, strrpos($gm, ",") + 1, strlen($gm));
            }
         }
         else $added_modules[] = $val[2];
         $content .= "&nbsp;&nbsp;<img src=\"".$_SESSION["php_server"]."/library/images/16x16/edition-16.png\" style=\"position:absolute;cursor:pointer;\" onclick=\"add_perspective(false, '".$_application["page_id"]."', '".substr($val[1], 0, strpos($val[1], ":"))."', '".substr($val[1], strpos($val[1], ":") + 1, strlen($val[1]))."', '".str_replace(array("__", "/"), array(".", ";"), implode(";", $added_modules))."', 'CUSTOM_".$count["CUSTOM"]."');\" />&nbsp;&nbsp;&nbsp;";

      }
      $txt = $val[1];
      $val[1] = ($_application["grid"]["enabled"] && $val[0] == "CUSTOM" && $val[0]."_".$count[$val[0]] == $_SESSION["perpage"]["tab"][$_application["page_id"]]["active"]) ? "<span ondblclick=\"edit_field('Edit perspective', this.textContent, 20, 'document.perpage.".$val[0]."_".$count[$val[0]]."_dsp.value = v; save_perpage(\\'CUSTOM\\', true);');\">".$txt."</span>" : $txt;
      $content .= "&nbsp;&nbsp;&nbsp;&nbsp;".substr($val[1], strpos($val[1], ":") + 1, strlen($val[1]))."&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n";
      $prv_tab = $val[0]."_".$count[$val[0]];

   }
   

   $dsp_add = ($_application["grid"]["enabled"]) ? "" : "display:none;";

   $r = 
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;padding-left:6px;position:relative;top:4px;".$dsp_breadcrumbing."\">\r\n".
   "<tr>\r\n".
   "<td style=\"width:263px;overflow:hidden;\"><nobr style=\"position:relative;top:-7px;left:-8px;\">".$module."</nobr></td>\r\n".
   "<td>\r\n".
   "<div style=\"white-space:nowrap;text-align:right;\" id=\"te_perspective\">\r\n&nbsp;&nbsp;&nbsp;&nbsp;".$content."&nbsp;".
   "<img src=\"".$_SESSION["php_server"]."/library/images/16x16/interface-78.png\" style=\"position:relative;top:3px;cursor:pointer;".$dsp_add."\" onclick=\"add_perspective(true, '".$_application["page_id"]."', 'CUSTOM_".(1 + $count["CUSTOM"])."');\" />&nbsp;&nbsp;\r\n".
   "</div>\r\n".
   "</td>\r\n".
   "</tr>\r\n".
   "<tr>\r\n".
   "<td colspan=\"2\"><div style=\"width:100%; border-top:solid 1px rgb(199,199,199);position:relative;top:-3px;\"></div></td>\r\n".
   "</tr>\r\n".
   "</table>\r\n";


   return $r;

}

?>