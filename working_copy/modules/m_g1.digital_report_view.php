<?php


function m_g1__digital_report_view($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   
   $headline="Digital Report";

   $module = 
   "<div form=\"".__FUNCTION__."\">Loading ...</div>\r\n";   



   return array($headline, $module);


   // Überschrift und Modulsonderzubehör erstellen
   $img1=($_REQUEST[$_module_id."_f2"]=="") ? "select_reg.png" : "select_high.png";
   $img2=($_REQUEST[$_module_id."_f2"]=="") ? "select_high.png" : "select_reg.png";

   foreach($_REQUEST as $key => $val) {
      if($key != "m_g1_q") $req .= "&".$key."=".$val;
   }

   $nav=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:960px;\">".
   "<tr><td><input onkeyup=\"if(event.keyCode == 13) location.href='?".$req."&m_g1_q=' + escape(this.value);\" value=\"".$_REQUEST["m_g1_q"]."\" style=\"width:260px;font:normal 13px Open Sans; border:solid 1px rgb(169,169,169); padding-left:3px;\"></td>".
   "<td style=\"text-align:right;\">%%FILTER%%</td></table>";


   // Ansicht einbinden
   if(isset($data)) unset($data);
   
   $icon="true";
   $number="true";
   $path = $_SESSION["remote_domino_path_digital_report"]."/v.get_base_tool?open&count=99999&function=plain";
   $eval = file_get_authentificated_contents($path);
   if(strpos(strtolower($eval), "no documents found") < 1) eval($eval);

   foreach($basic_data as $val) {
      $basic_part = explode("|", $val["parts"]);
      foreach($basic_part as $part) {
         $data[] = array(
            $column[0] => $val[$column[0]],
            $column[1] => $val[$column[1]],
            $column[2] => $val[$column[2]],
            $column[3] => $val[$column[3]],
            $column[4] => $val[$column[4]],
            $column[5] => $part,
            $column[6] => $val[$column[6]],
            $column[7] => $val[$column[7]],
            $column[8] => $val[$column[8]],
            $column[9] => $val[$column[9]]
         );
      }
   }
   unset($basic_data);



   include_once("addin/generate_extended_view.php");


   // View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
   foreach($column as $val) $dsp_filter[$val] = false;
   if($_REQUEST["m_g1_q"] != "lol") $t_view=generate_extended_view($dsp_filter, $path, $column, $data, $_w, $_SESSION["module"][$_module_name], true, true, false, false, true, true, 40, array("use:html","edit_base_tool.php?&unid=%%UNID%%"));
   else $t_view = "<br>Please enter a search query";

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed; width:960px;\">\r\n".
   "   <tr>\r\n".
   "      <td>%%NAV%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr><td style=\"position:relative; top:-10px;\">".$t_view."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n".
   "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
   "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
   "</form>\r\n";

   foreach($data as $val) {
      if(!in_array($val["filter0"], $filter)) $filter[] = $val["filter0"];
   }

   sort($filter);


   $dsp_status["0"] = "Procurement input"; 
   $dsp_status["5"] = "Procurement update"; 
   $dsp_status["10"] = "Specification input"; 
   $dsp_status["80"] = "Specification update"; 
   $dsp_status["90"] = "Finalized"; 

   $dsp_filter = "<select style=\"font:normal 13px Open Sans\" onchange=\"location.href=this[this.selectedIndex].value;\"><option value=\"?".str_replace("&m_g1_f0=".$_REQUEST["m_g1_f0"], "", $_SERVER["QUERY_STRING"])."\">All</option>";
   foreach($filter as $val) {
      if($dsp_status[$val] != "") {
         $selected = ($val == $_REQUEST["m_g1_f0"]) ? " selected" : "";
         $dsp_filter .= "<option value=\"?".str_replace("&m_g1_f0=".$_REQUEST["m_g1_f0"], "", $_SERVER["QUERY_STRING"])."&m_g1_f0=".$val."\"$selected>".$dsp_status[$val]."</option>";
      }
   }
   $dsp_filter .= "</select>";
   $module=str_replace("%%FILTER%%", $dsp_filter, str_replace("%%ADDLINE%%","",str_replace("%%NAV%%", $nav, $module)));  

   return array($headline, $module);

}

?>