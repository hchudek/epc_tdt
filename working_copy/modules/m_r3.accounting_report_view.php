<?php


function m_r3__accounting_report_view($_application) {

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;
   
   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);



   if($_REQUEST[$_module_id."_add"] == "") {
      $module = "";
   }
   else {
      // Ansicht einbinden
      if(isset($data)) unset($data);
      $app="epc/projects";
      $icon="true";
      $number="true";
  
      if($_REQUEST[$_module_id."_add"] != "") {
         $path = str_replace(" ", "%20", $_SESSION["remote_injectdb_path"]."/v.unique/".$_REQUEST[$_module_id."_add"]."?open");
         $handle = print_r(@fopen($path, "r"), true);
         $load = (strpos($handle, "Resource id") > -1) ? "true" : "false";
         if($load) {
            $eval = file_get_authentificated_contents($path);
            eval($eval);
         }
      }


      // FILTER DATE
      if($_REQUEST[$_module_id."_df"] != "" || $_REQUEST[$_module_id."_dt"] != "") {
         foreach($data as $val) {
            $add = ($val["filter1"] == "no info found") ? false : true;
            if($add && $_REQUEST[$_module_id."_df"] != "" && strtotime($val["filter1"]) < strtotime($_REQUEST[$_module_id."_df"])) {
               $add = false;
            }
            if($add && $_REQUEST[$_module_id."_dt"] != "" && strtotime($val["filter1"]) > strtotime($_REQUEST[$_module_id."_dt"])) {
                $add = false;
            }
            if($add) $f_data[] = $val;
         }
         unset($data);
         $data = $f_data;
      }

       include_once("addin/generate_extended_view.php");

      // ************************
      // START: CREATED BY FILTER
      //*************************
      foreach($data as $val) if(!in_array($val["filter2"], $created_by)) $created_by[] = $val["filter2"];
      sort($created_by);


      $c_html .= "</select>\r\n";
      // ***********************
      // ENDE: CREATED BY FILTER
      // ***********************


      // Überschrift und Modulsonderzubehör erstellen
  
      if($_REQUEST[$_module_id."_df"] != "") $do .= "From&nbsp;".$_REQUEST[$_module_id."_df"]."&nbsp;";
      if($_REQUEST[$_module_id."_dt"] != "") $do .= "To&nbsp;".$_REQUEST[$_module_id."_dt"];


      // Export Links
      $exp = 
      "<img onclick=\"handle_report.export('".$_module_id."', '".$_module_id."_add=".$_REQUEST[$_module_id."_add"]."&".$_module_id."_fields=".$_REQUEST[$_module_id."_fields"]."&".$_module_id."_df=".$_REQUEST[$_module_id."_df"]."&".$_module_id."_dt=".$_REQUEST[$_module_id."_dt"]."&".$_module_id."_start=1', false);\" src=\"../../../../library/images/table.png\" style=\"margin-right:7px;cursor:pointer;\" />".
      "<img onclick=\"handle_report.export('".$_module_id."', '".$_module_id."_add=".$_REQUEST[$_module_id."_add"]."&".$_module_id."_fields=".$_REQUEST[$_module_id."_fields"]."&".$_module_id."_df=".$_REQUEST[$_module_id."_df"]."&".$_module_id."_dt=".$_REQUEST[$_module_id."_dt"]."&".$_module_id."_start=1', true);\" src=\"../../../../library/images/excel-icon.png\" style=\"margin-right:7px;cursor:pointer;\" />";



      // View erstellen (Spaltenüberschriften, Daten , Tabellenbreite, Modulname, icon, Nummer, Filter?, ColumnSelector?, Navigation?, Zusätzliche Zeile?, Standardzeilen, Hyperlink konfigurieren
      if($_REQUEST[$_module_id."_fields"] == "") $t_view = "<div style=\"font:normal 13px century gothic;\" id=\"".$_module_id."_report_info\"><div style=\"padding-top:10px;\">No fields selected</div></div>";
      else {
         $dsp_filter["Project-No"] = false;
         $dsp_filter["Part No"] = false;
         $dsp_filter["Part Rev."] = false;
         $dsp_filter["Part Desc."] = false;
         $dsp_filter["Part Desc."] = false;
         $t_view = "<div id=\"".$_module_id."_report_info\">".
         generate_extended_view($dsp_filter, $path, $column, $data, $_w, $_SESSION["module"][$_module_name], true, true, true, true, true, false, 40, array("use:html","tracker.php?unique=%%UNID%%")).
         "</div>";
      }
      if(!$load && $_REQUEST[$_module_id."_add"] != "") $t_view = "<div style=\"font:normal 13px century gothic;\" id=\"".$_module_id."_report_info\"><div style=\"padding-top:10px;\">Report expiered</div></div>";

      $strng = substr($_REQUEST[$_module_id."_add"], strrpos($_REQUEST[$_module_id."_add"], "@") + 1, 14);
      $date = substr($strng,0, 4)."-".substr($strng, 4, 2)."-".substr($strng, 6,2);
      $time = substr($strng, 8, 2).":".substr($strng, 10, 2).":".substr($strng, 12, 2);
  
      if($_REQUEST[$_module_id."_add"] == "") $tabs = str_replace("%%INFO%%", "<p id=\"report_loaded\" style=\"display:none;\"></p>", $tabs);
      else $tabs = str_replace("%%INFO%%", "&nbsp;<span style=\"color:rgb(0,102,158);\">[report date:&nbsp;<date>".$date."</date>&nbsp;".$time."]</span>", $tabs);

      $module.=
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tbl_header_pot\" style=\"width:100%;\">\r\n".
      "   <tr>\r\n".
      "      <td style=\"text-align:right;\">".$exp."</td>\r\n".
      "   </tr>\r\n".
      "</table>\r\n".
      "<div id=\"tbl_main_pot\" style=\"width:1px;height:500px;overflow:auto;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"overflow-y:hidden;\">\r\n".
      "   <tr><td colspan=\"2\">".$t_view."</td>\r\n".
      "   </tr>\r\n".
      "</table>\r\n".
      "<form style=\"display:none;\" method=\"post\" name=\"select_project\">\r\n".
      "<input type=\"hidden\" value=\"\" name=\"t_project\" />\r\n".
      "</form></div>\r\n";

      $module=str_replace("%%ADDLINE%%","",str_replace("%%TABS%%", $tabs, $module));

   }

   return array($headline, $module);

}

?>