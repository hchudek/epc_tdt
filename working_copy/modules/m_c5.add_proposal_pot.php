<?php

function m_c5_add_proposal_pot($_application) {

   global $tc_data;

   // Modulnamen festlegen
   $_module_name=str_replace(".php","",substr(basename(__FILE__),5,strlen(basename(__FILE__))));
   $_module_id=substr(basename(__FILE__),0,4);
   $_SESSION["module"][$_module_name]=$_module_id;

   // Überschrift und Modulsonderzubehör erstellen
     $tabs="<img id=\"add_proposal_pot\" src=\"../../../../library/images/icons/create.png\" style=\"border:none;position:relative;top:4px;filter:Gray();\" /><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-add_POT-Create Proposal headline');\">&nbsp;&nbsp;Create Proposal</span>";
   $img[0] = ($tc_data["tracker"]["type"] == "0") ? "select_high" : "select_reg";
   $img[1] = ($tc_data["tracker"]["type"] == "1") ? "select_high" : "select_reg";


   $content=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"create_proposal_pot\">".
   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;background-color:rgb(255,194,54);\"><strong>Proposal part</strong></td></tr>".
   "<tr><td style=\"padding:2px;\"><input type=\"text\" name=\"proposal_part\" /></td></tr>".
   "<tr><td style=\"vertical-align:top;padding:2px;border-bottom:solid 1px #ffffff;background-color:rgb(255,194,54);\"><strong>Proposal tool</strong></td></tr>".
   "<tr><td style=\"padding:2px;\"><input type=\"text\" name=\"proposal_tool\" /></td></tr>".
   "<tr><td style=\"padding:2px 0px 4px 2px;\"><span class=\"phpbutton\"><a href=\"javascript:create_proposal_pot('".$tc_data["tracker"]["unique"]."');\">Create</a></span></td></tr>".
   "</table>";

   $module.=
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:220px;table-layout:fixed;margin-bottom:22px;\">\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_headline\">%%TABS%%</td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td colspan=\"2\"><img src=\"../../../../library/images/blank.gif\" height=\"2\"></td>\r\n".
   "   </tr>\r\n".
   "   <tr>\r\n".
   "      <td class=\"module_2_content\" colspan=\"2\">".$content."</td>\r\n".
   "   </tr>\r\n".
   "</table>\r\n";

   $module=str_replace("%%ADDLINE%%",$addedline,str_replace("%%TABS%%", $tabs, $module));

   return $module;

}




?>