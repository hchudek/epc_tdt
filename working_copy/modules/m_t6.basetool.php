<?php


function m_t6__basetool($_application) {

   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = ($_REQUEST["pot"] == "") ? "No tool selected" : "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   return array($headline, $module);

}


function m_t6__basetool_generate_html($_application) {

      global $tc_data;
   
  
      $module =
      "<table id=\"tbl_m_t6__basetool\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">";
      if($tc_data["pot"]["is_proposal"] == "1") {
      $module .=
         "<tr>\r\n".
         "<td>Number</td>".
         "<td colspan=\"3\">".$tc_data["pot"]["tool_proposal"]." [PROPOSAL]</td>\r\n".
        "</tr>\r\n";
      }
      else {
      $module .=
      "<tr>\r\n".
         "<td>Base Number</td>\r\n".
         "<td colspan=\"3\">".substr(urldecode($tc_data["tool"]["number"]),0,10)."</td>\r\n".
      "</tr>\r\n".
      "<tr>\r\n".
      "<td>Tool type</td>\r\n".
 //     "<td><smartcombo name=\"m_t9__select_pot\" class=\"smartcombo_combobox\" sizex=\"149\" sizey=\"76\" uselist=\"addin/get_values.php?m_t6__basetool:tool_type\" max=\"1\" value=\"\" width=\"100\" highlight=\"true\" onsave=\"smartcombo.save(session.remote_domino_path_epcmain, '%%ID%%', '".$tc_data["tool"]["unid"]."')\"></smartcombo></td>".
      "<td>".embed_selectbox(array("[superuser]", "[t_d]"), true, array("pilot", "production"), array("Pilot", "Production"), $tc_data["tool"]["type"], array("name" => "tooltype", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:110px;font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onchange" => "handle_save_single_field_extdb(session.remote_domino_path_epctlib, '".$tc_data["tool"]["source_unid"]."', 'tooltype', this.value, '');"))."</td>\r\n".
      "<td>Technology</td>\r\n".
      "<td>".embed_selectbox(array("[superuser]", "[t_d]"), true, array("die", "mold", "assembly", "soldering", "packaging", "plating", "coating", "metalworking", "other"), array("Die", "Mold", "Assembly", "Soldering", "Packaging", "Plating", "Coating", "Metalworking", "Other"), $tc_data["tool"]["techno"], array("name" => "technology", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:110px;font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onchange" => "handle_save_single_field_extdb(session.remote_domino_path_epctlib, '".$tc_data["tool"]["source_unid"]."', 'technology', this.value, 'document.getElementById(&#x22;get_technology&#x22;).innerText=&#x22;' + this.value + '&#x22;');"))."<span id=\"get_technology\" style=\"display:none;\">".$tc_data["tool"]["techno"]."</span></td>".
      "</tr>\r\n";

      $dsource = ($tc_data["tool"]["dualsource"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\">";
      $a2p = ($tc_data["tool"]["a2p"] == "1" ) ? "<img src=\"../../../../library/images/16x16/status-11.png\">" : "<img src=\"../../../../library/images/16x16/status-11-1.png\">";
 

      $dsp_dsource = (check_editable(array("[superuser]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"set_checkbox(this, '".$tc_data["tool"]["unid"]."', 'dualsource');\">".$dsource."</a>" : $dsource;
      $dsp_a2p = (check_editable(array("[superuser]", "[t_d]"))) ? "<a href=\"javascript:void(0);\" onclick=\"set_checkbox(this, '".$tc_data["tool"]["unid"]."', 'a2p');\">".$a2p."</a>" : $a2p;

         // ZUFALLIDS F�R FELDER
         $arr = array("pono", "prno", "pos", "calendar_cevt", "old", "cavity", "convkitno", "spec", "costs", "guarantoutput", "coutput");
         foreach($arr as $val) {
            eval("\$unique_".$val." = generate_uniqueid(8);");
         }

         $dsp_calendar = (check_editable(array("[superuser]", "[procurement]"))) ? "<a href=\"javascript:load_calendar('".$unique_calendar_cevt."', '".$_SESSION["remote_database_path"]."addin/calendar2.php?&p_function=sc2&p_use_rule=0&p_id=calendar_".$unique_calendar_cevt."','');\"><img src=\"../../../../library/images/16x16/time-3.png\" style=\"border:none;margin-right:7px;vertical-align:top;position:relative;top:2px;\" /></a>" : "";

         $module .=
         "<td class=\"label\" style=\"width:1%;\"><nobr>A2P:&nbsp;&nbsp;&nbsp;</nobr></td>".
         "<td class=\"data\" colspan=\"3\">".$dsp_a2p."</td>".
         "</tr>".
         "<tr>".
         "<td class=\"label\" style=\"width:1%;\"><nobr>Old Tool No.:</nobr></td>".
         "<td class=\"data\" colspan=\"3\">".embed_input(array("[superuser]","[t_d]"), $tc_data["tool"]["old"], array("id" => $unique_old, "type" => "text", "name" => "oldtool", "style" => "background-color:rgb(220,220,220);width:130px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "post_save_field('".$_SESSION["remote_domino_path_main"]."', '".$tc_data["tool"]["unid"]."', this.name, this.value);"))."</td>".
         "</tr>".
         "<tr>".
         "<td class=\"label\" style=\"width:1%;\"><nobr>No. of cavities:</nobr></td>".
         "<td class=\"data\">".embed_input(array("[superuser]","[t_d]"), $tc_data["tool"]["cav"], array("id" => $unique_cavity, "type" => "text", "name" => "cavity", "style" => "background-color:rgb(220,220,220);width:30px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_cavity."&#x22;)');"))."</td>".
         "<td class=\"label\" style=\"width:1%;\"><nobr>Conversion Kit No.:</nobr></td>".
         "<td class=\"data\">".embed_input(array("[superuser]","[t_d]"), $tc_data["tool"]["convkit"], array("id" => $unique_convkitno, "type" => "text", "name" => "convkitno", "style" => "background-color:rgb(220,220,220);width:30px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_convkitno."&#x22;)');"))."</td>".
         "</tr>".
         "<tr>".
         "<td class=\"label\" style=\"width:1%;\"><nobr>Tool specification:</nobr></td>".
         "<td class=\"data\" colspan=\"3\">".embed_input(array("[superuser]", "[t_d]"), $tc_data["tool"]["spec"], array("id" => $unique_spec, "type" => "text", "name" => "toolspec", "style" => "background-color:rgb(220,220,220);width:324px;border:solid 1px rgb(99,99,99);font:normal 12px century gothic,verdana;color:rgb(0,102,158);", "onblur" => "handle_save_single_field_extdb(session.remote_domino_path_epcmain, '".$tc_data["tool"]["unid"]."', this.name, this.value, 'info_saved(&#x22;".$unique_spec."&#x22;)');"))."</td>".
         "</tr>";


         $dsp_img = (in_array("[t_location]", $_SESSION["remote_userroles"])) ? "visible" : "hidden";
         if (urldecode($tc_data["pot"]["transferred_location"]) == 'Not set') {
            $module .=
            "<tr>".
            "<tdTransf. location<img src=\"../../../../library/images/16x16/files-28.png\" onclick=\"get_value('select', 'production_location', 'transferred_location');handle_save_single_field('".$tc_data["pot"]["unid"]."', 't_location', document.getElementsByName('production_location')[0][document.getElementsByName('production_location')[0].selectedIndex].text, 'location.href=location.href');\" style=\"cursor:pointer;vertical-align:top;position:relative;left:4px;top:1px;visibility:".$dsp_img.";\" /></a></nobr></td>".
            "<td colspan=\"3\">".$t_html."</td>".
            "</tr>";
         } 
         else {
	   $module .=
	   "<tr>".
   	   "<td>Transf. location</td>".
           "<td colspan=\"3\">".urldecode($tc_data["pot"]["transferred_location"])."</td>".
           "</tr>";
         }
           if (urldecode($tc_data["pot"]["production_location"]) == 'Not set') {
            $module .=
            "<tr>".
            "<td class=\"label\" style=\"width:1%;\">Prod. location G3</td>".
            "<td class=\"data\" colspan=\"3\">".$p_html."</td>".
            "</tr>";
         } else {
            $module .=
           "<tr>".
            "<td class=\"label\" style=\"width:1%;\">Prod. location G3</td>".
            "<td class=\"data\" colspan=\"3\">".urldecode($tc_data["pot"]["production_location"])."<div style=\"display:none;\">".$p_html."</div></td>".
            "</tr>";
         }
  
         $tc_data["base_tool"]["scan_location"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["scan_location"]);
         $tc_data["base_tool"]["digital_report"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["digital_report"]);
         $tc_data["base_tool"]["tabulated_report"] = str_replace(array("1", "2"), array("TE", "Supplier"), $tc_data["base_tool"]["tabulated_report"]);

         $dsp_selected_scan_location = $tc_data["base_tool"]["scan_location"];
         if($tc_data["base_tool"]["scan_location"] == "TE" && $tc_data["base_tool"]["selected_scan_location"] != "") {
            $dsp_selected_scan_location .= " [".rawurldecode($tc_data["base_tool"]["selected_scan_location"])."]";
         }

         $dsp_selected_digital_report = $tc_data["base_tool"]["digital_report"];
         if($tc_data["base_tool"]["digital_report"] == "TE" && $tc_data["base_tool"]["selected_digital_report"] != "") {
            $dsp_selected_digital_report .= " [".rawurldecode($tc_data["base_tool"]["selected_digital_report"])."]";
         }

         $dsp_selected_tabulated_report = $tc_data["base_tool"]["tabulated_report"];
         if($tc_data["base_tool"]["tabulated_report"] == "TE" && $tc_data["base_tool"]["selected_tabulated_report"] != "") {
           $dsp_selected_tabulated_report .= " [".rawurldecode($tc_data["base_tool"]["selected_tabulated_report"])."]";
         }


         $module .=
         "<tr>\r\n".
         "<td>Supplier</td>\r\n".
         "<td colspan=\"3\">".rawurldecode($tc_data["base_tool"]["supplier"])."&nbsp;</td>\r\n".
         "</tr>\r\n".
         "<tr>\r\n".
         "<td>Scan location</td>\r\n".
         "<td colspan=\"3\">".$dsp_selected_scan_location."</td>\r\n".
         "</tr>\r\n".
         "<tr>\r\n".
         "<td>Digital report</td>\r\n".
         "<td colspan=\"3\">".$dsp_selected_digital_report."</td>\r\n".
         "</tr>\r\n".
         "<tr>\r\n".
         "<td>Tabulated report</td>\r\n".
         "<td colspan=\"3\">".$dsp_selected_tabulated_report."</td>\r\n".
         "</tr>\r\n";
      }
      "</table>\r\n";
   

   return str_replace("Array","",$module);

}

?>