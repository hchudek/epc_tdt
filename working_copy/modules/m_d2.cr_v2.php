<?php


function m_t16_cr_v2($_application) {
     // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);

   // Module Body --------------------------------------------------------------------------------------------------
   $module = "<div id=\"".__FUNCTION__."_innerhtml\">Loading...</div>\r\n";
   return array($headline, $module);

}


function m_t16__cr_v2_generate_html($_application) {


  // $_pt = str_replace("v->2", "", strtolower(rawurldecode($tc_data["tracker"]["process_type"][1])));
   $_pt = str_replace("v->2", "", strtolower($tc_data["pot"]["meta"]["process_type"]));
  // $form .= "<p id=\"reason_code\" style=\"display:none;\" me:workflow=\"true\">true</p>";

   // START: WORKFLOW INSPECTION 1st source  ------------------------------------------------------------------------------------------------------------
   $sources = array(array("1st source", "G04100"));


   foreach($sources as $key => $source) {

       if($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_is_active"] != "false") {

         $wf_loop = ($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_loop"] != "") ? intval($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_loop"]) : 0;
         $wf_loop_source[$key] = $wf_loop;
         $wf_name = $_application["process:workflow"][$_pt][$source[1]];
        

         foreach($_application["workflow"][$wf_name] as $workflow) {
            $wf[] = $workflow[0];
            $fl[] = $workflow[1];
            $pr[] = $workflow[2];
         }

         for($loop = 0; $loop <= $wf_loop; $loop++) {
            if($loop == 0) {
               $dsp_pot_part_tool .= "<tr>".
               "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);width:1%;border-top:solid 1px rgb(255,255,255);\"><strong><nobr>Workflow ".$source[0]."</nobr></strong></td>";
               if($tc_data["pot"]["status"] != "0" && !is_array($pot_date[$_REQUEST["pot"]]["G04050"][0]["actual_value"]) && is_array($pot_date[$_REQUEST["pot"]]["G05050"][0]["actual_value"])) {
                  $dsp_pot_part_tool .= "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);width:1%;color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);\"><span style=\"cursor:pointer;\" onclick=\"post_save_data('f.save_data', session.remote_domino_path_main, '&unid=".$pot_date[$_REQUEST["pot"]][$source[1]][0]["unid"]."&fld1=wf\$loop/".($wf_loop + 1)."', 'location.reload()');\"><span style=\"background-color:#de8703; border: 1px solid rgb(255,182,8);\">&nbsp;ADD LOOP&nbsp;</span></span></td>";
               } else {
                  $dsp_pot_part_tool .= "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);\">&nbsp;</td>";
               }
               $dsp_pot_part_tool .=
               "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);border-top:solid 1px rgb(255,255,255);\"><strong>Planned</strong></td>".
               "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);width:1%;border-top:solid 1px rgb(255,255,255);\"><strong>Actual</strong>".
               "</td></tr>";
            }
            for($fld = 0; $fld < count($wf); $fld++) {
               $add_js["planned"] = ($fld == 0) ? "pick_date.run_workflow = [['".implode("', '", $fl)."'], [".implode(", ", $pr)."], 'wf\$planned_%%%%_loop_".$loop."']; " : "pick_date.run_workflow = [['".$fl[$fld]."'], [".$pr[$fld]."], 'wf\$planned_%%%%_loop_".$loop."']; ";
               $add_js["actual"] = "pick_date.run_workflow = [['".$fl[$fld]."'], [".$pr[$fld]."], 'wf\$actual_%%%%_loop_".$loop."']; ";
               $d1 = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_planned_".strtolower($fl[$fld])."_loop_".$loop])));       

               $d1_count = "";
               if(count($d1) > 1) {
                  $d1_count = "<span onmouseover=\"document.getElementById('tbl_history_loop_".$loop."_".$fld."').style.display='block';\" onmouseout=\"document.getElementById('tbl_history_loop_".$loop."_".$fld."').style.display='none';\">[".(count($d1) - 1)."]</span>\r\n".
                  "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"tbl_history_loop_".$loop."_".$fld."\" style=\"border:solid 1px rgb(255,255,255); opacity:0.95; z-index:99999; display:none; position:absolute; width:400px;\">\r\n".
                  "<tr>\r\n".
                  "<td style=\"background-color:rgb(189,54,50); color:#ffffff;\">Date</td>\r\n".
                  "<td style=\"background-color:rgb(189,54,50); color:#ffffff;\">Changed</td>\r\n".
                  "<td style=\"background-color:rgb(189,54,50); color:#ffffff;\">By</td>\r\n".
                  "</tr>\r\n";

                  foreach($d1 as $val) {
                     $dsp = explode("@", $val);
                     $d1_count .= 
                     "<tr>\r\n".
                     "<td style=\"background-color:rgb(255,255,255);\">".$dsp[0]."</td>\r\n".
                     "<td style=\"background-color:rgb(255,255,255);\">".$dsp[1]."</td>\r\n".
                     "<td style=\"background-color:rgb(255,255,255);\">".substr($dsp[2], 3, strpos($dsp[2], "/") - 3)."&nbsp;&nbsp;</td>\r\n".
                     "</tr>\r\n";
                  }
                  $d1_count .= "</table>\r\n";
               } 
               $d2 = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_".strtolower($fl[$fld])."_loop_".$loop])));
               $enable_edit = ($d2[0] != "" || strlen($pot_date[$_REQUEST["pot"]][$sources[0][1]][0]["actual_value"]) == 10) ? false : true;

               $planned[$wf[$fld]] = (!$enable_edit) ? substr($d1[0], 0 , 10).$d1_count : "<a onclick=\"".$add_js["planned"]."calendar_big_pick_date('".$pot_date[$_REQUEST["pot"]][$source[1]][0]["unid"]."', 'wf\$planned_".strtolower($fl[$fld])."_loop_".$loop."', '".rawurlencode("Set planned date loop ".$loop)."', '', '', '', '', undefined, 'save_workflow_date');\"><img style=\"cursor:pointer;\" src=\"".$_SESSION["php_server"]."/library/images/16x16/time-3.png\" /></a><span style=\"position:relative; top:-2px;left:5px;\">".substr($d1[0], 0 , 10).$d1_count."</span>\r\n";
               $actual[$wf[$fld]] = (!$enable_edit) ? substr($d2[0], 0 , 10) : "<a onclick=\"".$add_js["actual"]."calendar_big_pick_date('".$pot_date[$_REQUEST["pot"]][$source[1]][0]["unid"]."', 'wf\$actual_".strtolower($fl[$fld])."_loop_".$loop."', '".rawurlencode("Set actual date for loop ".$loop)."', '".$_application["workflow"][$wf_name][$fld][3]."', '', '".substr($d1[0], 0 , 10)."', '', undefined, 'save_workflow_date');\"><img style=\"cursor:pointer;\" src=\"".$_SESSION["php_server"]."/library/images/16x16/time-3.png\" /></a><span style=\"position:relative; top:-2px;left:5px;\">".substr($d2[0], 0 , 10)."</span>\r\n";

               $_p = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_planned_".strtolower($fl[$fld])."_loop_".$loop])));
               $planned[$d] = ($_p != "") ? substr($_p[0], 0 , 10)." [".(count($_p) - 1)."]" : "\r\n";

               $_a = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_".strtolower($fl[$fld])."_loop_".$loop])));
               $actual[$d] = ($_a != "") ? substr($_a[0], 0 , 10) : "\r\n";


               $dsp_pot_part_tool .= "<tr>";

               if($fld == 0) $dsp_pot_part_tool .= "<td class=\"descr\" rowspan=\"".count($wf)."\" style=\"width:1%; border-top:solid 1px rgb(255,255,255);border-right:solid 1px rgb(255,255,255); font:normal 16px Open Sans;color:rgb(66,66,66);padding-left:4px;\">Loop&nbsp;".$loop."</td>";
               $dsp_pot_part_tool .= "<td class=\"descr\">".$wf[$fld]."</td>";
               if ($loop >= $wf_loop) {
                 $dsp_pot_part_tool .=
                 "<td class=\"descr\">".$planned[$wf[$fld]]."</td>".
                 "<td class=\"descr\">".$actual[$wf[$fld]]."</td>";
               } else {
                 $dsp_pot_part_tool .=
                 "<td class=\"descr\">".$planned[$d]."</td>".
                 "<td class=\"descr\">".$actual[$d]."</td>";
               }
               "</tr>";
            }
         }
      }
   }
   // ENDE: WORKFLOW INSPECTION 1st source  ------------------------------------------------------------------------------------------------------------
   // Start: CR 1st source  ------------------------------------------------------------------------------------------------------------

   $maxloop = $wf_loop + 1;
   $dsploop = ($_REQUEST["loop"] == "") ? $wf_loop : intval($_REQUEST["loop"]);
     for($w = 1; $w <=2; $w++) {
       $file = document_check($_SESSION["remote_domino_path_epcmain"], "v.get_creport_by_pot_and_loop", $_REQUEST["pot"]."@".$dsploop."@".($w));
       if(strlen($file) <= 1) {
         $file = "<cr><loop>".$maxloop."</loop></cr>";
       }
       $tc_data["cr"][$w - 1] = process_xml_from_string($file);
       $tc_data["cr"][$w - 1]["load"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.get_creport_by_pot?open&count=99999&restricttocategory=".$_REQUEST["pot"]."@".($w)."&function=xml:data");
     }

   $num = ($_REQUEST["loop"] != "") ? $_REQUEST["loop"] : $wf_loop;
   $dsp_pot_part_tool .=
     "<tr>".
     "<th style=\"background-image:url(../../../../library/images/bg_sel.gif); color:rgb(255,255,255);\" colspan=\"4\"><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Control Report headline');\">Control Report<a name=\"control_report\">&nbsp;</a>Primary Source&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loop:</span></strong>";
     if($maxloop > 1) {
       for($z = 0; $z < $maxloop; $z++) $dsp_loops[] = $z;
       $dsp_pot_part_tool .= embed_selectbox(false, true, $dsp_loops, $dsp_loops, $num, array("name" => "select_creport", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:60px;font:normal 11px century gothic,verdana;color:rgb(0,0,0);height:20px;padding:0px;margin-left:10px;", "onchange" => "loop = (this.value != '') ?(Number(this.value)) : ''; location.href='?".str_replace("&loop=".$_REQUEST["loop"], "", $_SERVER["QUERY_STRING"])."&loop=' + loop + '#control_report';"));
      }
    // STATUS anzeigen
    if($tc_data["cr"]["0"]["status"] == "0") $dsp_status = "in preparing";
    if($tc_data["cr"]["0"]["status"] == "1") $dsp_status = "to process";
    if($tc_data["cr"]["0"]["status"] == "2") $dsp_status = "in process";
    if($tc_data["cr"]["0"]["status"] == "3") $dsp_status = "prod. eval.";
    if($tc_data["cr"]["0"]["status"] == "4") $dsp_status = "withdrawn";
    if($tc_data["cr"]["0"]["status"] == "5") $dsp_status = "process eval.";
    if($tc_data["cr"]["0"]["status"] == "5" && $tc_data["cr"]["0"]["DateIRA"] != "false" && $tc_data["cr"]["0"]["StatusIRA"] != "G") $dsp_status = "forwarded to PE";
    if($tc_data["cr"]["0"]["status"] == "5" && $tc_data["cr"]["0"]["StatusIRA"] == "G") $dsp_status = "Process rejected";
    if($tc_data["cr"]["0"]["status"] == "6") $dsp_status = "prod. & proc. eval.";
    if($tc_data["cr"]["0"]["status"] == "7") $dsp_status = "obsolete";
    if($tc_data["cr"]["0"]["status"] == "8") $dsp_status = "deleted";

    $ptr = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_last_process_trial_run?open&restricttocategory=".$tc_data["cr"][0]["number"]."&count=1&function=xml:data");
    if(!strpos(strtolower($ptr), "no documents found")) {
      $tmp = process_xml_from_string($ptr);
      $tc_data["cr"][0]["status_ira_process"][0] = $tmp["cr"];
      unset($tmp);
    }

    $dsp_pot_part_tool .=  "</th></tr>";

    if(isset($tc_data["cr"]["0"]["number"])) {
      $dsp_pot_part_tool .= "<tr>".
        "<td class=\"descr_top\" style=\"width:1%;\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data_top\"><a id=\"get_cr_number\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"]["0"]["number"]."\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"]["0"]["number"]."</a></td>".
        "<td class=\"descr_top\" style=\"width:1%;\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data_top\">".format_date($tc_data["cr"]["0"]["issued"])."</td>".
        "</tr>".
        "<tr>".
        "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRT:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data\">".$tc_data["cr"]["0"]["status_ir"]."</td>".
        "<td class=\"descr\" style=\"width:1%;\"><nobr>Drw. Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data\">".$tc_data["cr"]["0"]["drwrev"]."</td>".
        "</tr>".
        "<tr>".
        "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRP:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data\">".$tc_data["cr"]["0"]["status_irp"]."</td>".
        "<td class=\"descr\" style=\"width:1%;\"><nobr>Part Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data\">".$tc_data["cr"]["0"]["rev"]."</td>".
        "</tr>".
        "<tr>".
        "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRS:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data\">".$tc_data["cr"]["0"]["status_irs"]."</td>".
        "<td class=\"data\" colspan=\"2\">&nbsp;</td>".
        "</tr>".
        "<tr>".
        "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data\" style=\"border-bottom:none;\">".$tc_data["cr"]["0"]["status_ira"]."<img src=\"../../../../library/images/blank.gif\" /></td>".
        "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
        "<td class=\"data\" style=\"border-bottom:none;\">".$dsp_status."</td>".
        "</tr>";

      $cnt = count($tc_data["cr"][0]["status_ira_process"]);
      if($cnt > 0) {
        $dsp_pot_part_tool .=
          "<tr>".
          "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);border-top:solid 1px rgb(255,255,255);\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"][0]["status_ira_process"][$cnt - 1]["number"]."?open\" style=\"color:rgb(255,255,255);\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"][0]["status_ira_process"][$cnt - 1]["number"]."</a><img src=\"../../../../library/images/blank.gif\" /></td>".
          "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);\">".format_date($tc_data["cr"][0]["status_ira_process"][$cnt - 1]["date"])."</td>".
          "</tr>".
          "<tr>".
          "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".$tc_data["cr"][0]["status_ira_process"][$cnt - 1]["status_ira"]."</td>".
          "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".rawurldecode($tc_data["cr"][0]["status_ira_process"][$cnt - 1]["status"])."</td>".
          "</tr>";
      }

      // Create Process trial run Button
      $do_trial_run = (in_array("[composeproz]", $_SESSION["remote_userroles"])) ? true : false;			                                                           // Rolle
      if ($tc_data["pot"]["status"] == "0") $do_trial_run = false;                                                                                                                 // POT inactive
      if(!in_array(substr($tc_data["tool"]["number"],0,2), Array(21, 23, 28))) $do_trial_run = false;		                                                                   // Tool Prefix
      if(strtoupper($tc_data["pot"]["meta"]["process_type"]) == "pilot" || $tc_data["pot"]["meta"]["process_type"]=="Not selected") $do_trial_run = false;                         // Prozesstyp
      if (is_array($pot_date[$_REQUEST["pot"]]["G04050"][0]["actual_value"])) $do_trial_run = false;                                                                               // Design Approval has no actual
      if (!is_array($pot_date[$_REQUEST["pot"]]["G05050"][0]["actual_value"])) $do_trial_run = false;                                                                              // PPAP Release has actual
      if($cnt > 0 && is_array($tc_data["cr"][0]["status_ira_process"][$cnt - 1]["status_ira"])) $do_trial_run = false;	                                                           // Ein unbewerterter PTR exisitiert
      if ($tc_data["cr"]["0"]["DateIRA"] == "false" || is_array($tc_data["cr"]["0"]["status_ir"])) $do_trial_run = false;			                                   // Product assessment available ?
      if($num != $wf_loop) $do_trial_run = false;                                                                                                                                  // actual loop ?
      if($do_trial_run) {
        $dsp_pot_part_tool .= "<tr>".
          "<td class=\"descr\" colspan=\"4\" id=\"m_b1__tracker_cr_s0\" style=\"visibility:hidden;\"><span class=\"phpbutton\"><a href=\"javascript:run_ajax('run_ajax','addin/run_ajax.php?".rawurlencode(rawurlencode($_SESSION["remote_domino_path_epcmain"]."/a.copy_cr?open&cr_reason=process%20approval&cr_type=2&unid=".$tc_data["cr"][0]["unid"]."&user=".rawurlencode($_SESSION["domino_user"])))."','open_cr_copy()', false);\">Create process trial run</a></span></td>".
          "</tr>";
      }

    } else {
      $fot_date = ", '".substr($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_fot_loop_".$wf_loop_source[0]],0,10)."'";
      $dimreport_date = ", '".substr($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_dimreport_loop_".$wf_loop_source[0]],0,10)."'";

      $_p = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$sources[0][1]][0]["wf_actual_".strtolower("G04070")."_loop_".$wf_loop])));
      $fot_date = ", '".substr($_p[0], 0, 10)."'";

      $_d = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$sources[0][1]][0]["wf_actual_".strtolower("G04080")."_loop_".$wf_loop])));
      $dimreport_date = ", '".substr($_d[0], 0, 10)."'";

    }


    // Create new CR Button
    $do_new_cr = (in_array("[t_d]", $_SESSION["remote_userroles"]) || in_array("[cr]", $_SESSION["remote_userroles"])) ? true : false;       // Rolle
    if ($tc_data["pot"]["status"] == "0") $do_new_cr = false;                                                                                // POT inactive
    if ($tc_data["pot"]["meta"]["process_type"] == "Not selected")  $do_new_cr = false;                                                      // Prozesstyp
    if (is_array($pot_date[$_REQUEST["pot"]]["G03980"][0]["actual_value"])) $do_new_cr = false;                                              // Design Approval has no actual
    if (!is_array($pot_date[$_REQUEST["pot"]]["G05220"][0]["actual_value"])) $do_new_cr = false;                                             // PPAP Release has actual
    if ($wf_loop_source[0] != $num ) $do_new_cr = false;                                                                                     // current loop is highest loop
    if ($wf_loop_source[0] == $tc_data["cr"]["0"]["loop"] ) $do_new_cr = false;                                                              // highest loop has no CR

    if($do_new_cr) {
      $dsp_pot_part_tool .= "<tr>".
        "<td class=\"data_top\" colspan=\"4\" style=\"padding:10px 10px 10px 2px;\"><span class=\"phpbutton\">".
        "<a href=\"javascript:void(0);\" onclick=\"create_cr('".$tc_data["pot"]["unique"]."', '".strtolower($tc_data["pot"]["unid"])."', '".strip_tags($tc_data["pot"]["source_link_unid"])."' , '".strip_tags($tc_data["pot"]["source_part_unid"])."', '".$_REQUEST["unique"]."', '".strip_tags($tc_data["pot"]["source_tool_unid"])."' , '1', '".rawurlencode("correction loop")."', '".($maxloop -1)."'".$fot_date.$dimreport_date.");\">Create NEW CR</a>".
        "</span></td>".
        "</tr>";
    }
    // ENDE: CR 1st source  ------------------------------------------------------------------------------------------------------------
    // ENDE: 1st source  ---------------------------------------------------------------------------------------------------------------
    // START: WORKFLOW INSPECTION 2nd source  ------------------------------------------------------------------------------------------
    $sources = array(array("2nd source", "G04120"));

    // is the alternate source active?

    if ($pot_date[$_REQUEST["pot"]][$sources[0][1]][0]["wf_is_active"] != "false") {
      foreach($sources as $key => $source) {

        $wf_loop = ($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_loop"] != "") ? intval($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_loop"]) : 0;
        $wf_loop_source[$key] = $wf_loop;
        $wf_name = $_application["process:workflow"][$_pt][$source[1]];

        unset($wf); unset($fl);
         foreach($_application["workflow"][$wf_name] as $workflow) {
            $wf[] = $workflow[0];
            $fl[] = $workflow[1];
            $pr[] = $workflow[2];
         }


        for($loop = 0; $loop <= $wf_loop; $loop++) {
          if($loop == 0) {
            $dsp_pot_part_tool .= "<tr>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);width:1%;border-top:solid 1px rgb(255,255,255);\"><strong><nobr>Workflow ".$source[0]."</nobr></strong></td>";
            if($tc_data["pot"]["status"] != "0" && !is_array($pot_date[$_REQUEST["pot"]]["G04050"][0]["actual_value"]) && is_array($pot_date[$_REQUEST["pot"]]["G05050"][0]["actual_value"])) {
              $dsp_pot_part_tool .= "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><span style=\"cursor:pointer;\" onclick=\"post_save_data('f.save_data', session.remote_domino_path_main, '&unid=".$pot_date[$_REQUEST["pot"]][$source[1]][0]["unid"]."&fld1=wf\$loop/".($wf_loop + 1)."', 'location.reload()');\"><span style=\"background-color:#de8703; border: 1px solid rgb(255,182,8);\">&nbsp;ADD LOOP&nbsp;</span></span></td>";
            } else {
              $dsp_pot_part_tool .= "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);\">&nbsp;</td>";
            }
            $dsp_pot_part_tool .=
              "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);border-top:solid 1px rgb(255,255,255);\"><strong>Planned</strong></td>".
              "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_sel2.gif);width:1%;border-top:solid 1px rgb(255,255,255);\"><strong>Actual</strong>".
              "</td></tr>";
          }
          for($fld = 0; $fld < count($wf); $fld++) {
            $add_js["planned"] = ($fld == 0) ? "pick_date.run_workflow = [['".implode("', '", $fl)."'], [".implode(", ", $pr)."], 'wf\$planned_%%%%_loop_".$loop."']; " : "pick_date.run_workflow = [['".$fl[$fld]."'], [".$pr[$fld]."], 'wf\$planned_%%%%_loop_".$loop."']; ";
            $add_js["actual"] = "pick_date.run_workflow = [['".$fl[$fld]."'], [".$pr[$fld]."], 'wf\$actual_%%%%_loop_".$loop."']; ";
            $d1 = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_planned_".strtolower($fl[$fld])."_loop_".$loop])));       

            $d1_count = "";
            if(count($d1) > 1) {
               $d1_count = "<span onmouseover=\"document.getElementById('tbl_history_loop_".$loop."_".$fld."').style.display='block';\" onmouseout=\"document.getElementById('tbl_history_loop_".$loop."_".$fld."').style.display='none';\">[".(count($d1) - 1)."]</span>\r\n".
               "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"tbl_history_loop_".$loop."_".$fld."\" style=\"border:solid 1px rgb(255,255,255); opacity:0.95; z-index:99999; display:none; position:absolute; width:400px;\">\r\n".
               "<tr>\r\n".
               "<td style=\"background-color:rgb(189,54,50); color:#ffffff;\">Date</td>\r\n".
               "<td style=\"background-color:rgb(189,54,50); color:#ffffff;\">Changed</td>\r\n".
               "<td style=\"background-color:rgb(189,54,50); color:#ffffff;\">By</td>\r\n".
               "</tr>\r\n";

               foreach($d1 as $val) {
                  $dsp = explode("@", $val);
                  $d1_count .= 
                  "<tr>\r\n".
                  "<td style=\"background-color:rgb(255,255,255);\">".$dsp[0]."</td>\r\n".
                  "<td style=\"background-color:rgb(255,255,255);\">".$dsp[1]."</td>\r\n".
                  "<td style=\"background-color:rgb(255,255,255);\">".substr($dsp[2], 3, strpos($dsp[2], "/") - 3)."&nbsp;&nbsp;</td>\r\n".
                  "</tr>\r\n";
               }
               $d1_count .= "</table>\r\n";
            } 
            $d2 = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_".strtolower($fl[$fld])."_loop_".$loop])));       
            $enable_edit = ($d2[0] != "" || strlen($pot_date[$_REQUEST["pot"]][$sources[0][1]][0]["actual_value"]) == 10) ? false : true;


            $planned[$wf[$fld]] = (!$enable_edit) ? substr($d1[0], 0, 10).$d1_count : "<a onclick=\"".$add_js["planned"]."calendar_big_pick_date('".$pot_date[$_REQUEST["pot"]][$source[1]][0]["unid"]."', 'wf\$planned_".strtolower($fl[$fld])."_loop_".$loop."', '".rawurlencode("Set planned date loop ".$loop)."', '', '', '', '', undefined, 'save_workflow_date');\"><img style=\"cursor:pointer;\" src=\"".$_SESSION["php_server"]."/library/images/16x16/time-3.png\" /></a><span style=\"position:relative; top:-2px;left:5px;\">".substr($d1[0], 0, 10).$d1_count."</span>\r\n";
            $actual[$wf[$fld]] = (!$enable_edit) ? substr($d2[0], 0, 10) : "<a onclick=\"".$add_js["actual"]."calendar_big_pick_date('".$pot_date[$_REQUEST["pot"]][$source[1]][0]["unid"]."', 'wf\$actual_".strtolower($fl[$fld])."_loop_".$loop."', '".rawurlencode("Set actual date for loop ".$loop)."', '".$_application["workflow"][$wf_name][$fld][3]."', '', '".substr($d1[0], 0 , 10)."', '', undefined, 'save_workflow_date');\"><img style=\"cursor:pointer;\" src=\"".$_SESSION["php_server"]."/library/images/16x16/time-3.png\" /></a><span style=\"position:relative; top:-2px;left:5px;\">".substr($d2[0], 0, 10)."</span>\r\n";

            $_p = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_planned_".strtolower($fl[$fld])."_loop_".$loop])));
            $planned[$d] = ($_p != "") ? substr($_p[0], 0 , 10)." [".(count($_p) - 1)."]" : "\r\n";

            $_a = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_".strtolower($fl[$fld])."_loop_".$loop])));
            $actual[$d] = ($_a != "") ? substr($_a[0], 0 , 10) : "\r\n";
            $dsp_pot_part_tool .= "<tr>";
            if($fld == 0) $dsp_pot_part_tool .= "<td class=\"descr\" rowspan=\"".count($wf)."\" style=\"width:1%; border-top:solid 1px rgb(255,255,255);border-right:solid 1px rgb(255,255,255); font:normal 16px Open Sans;color:rgb(66,66,66);padding-left:4px;\">Loop&nbsp;".$loop."</td>";
            $dsp_pot_part_tool .= "<td class=\"descr\">".$wf[$fld]."</td>";

            if ($loop >= $wf_loop) {
              $dsp_pot_part_tool .=
              "<td class=\"descr\">".$planned[$wf[$fld]]."</td>".
              "<td class=\"descr\">".$actual[$wf[$fld]]."</td>";
            } else {
              $dsp_pot_part_tool .=
              "<td class=\"descr\">".$planned[$d]."</td>".
              "<td class=\"descr\">".$actual[$d]."</td>";
            }
            $dsp_pot_part_tool .= "</tr>";
          }
        }
      }
      // ENDE: WORKFLOW INSPECTION 2nd source  ------------------------------------------------------------------------------------------------------------
      // Start: CR 2nd source  ------------------------------------------------------------------------------------------------------------
      unset($dsp_loops);
      $maxloop = $wf_loop + 1;
      $dsploop = ($_REQUEST["sloop"] == "") ? $wf_loop : intval($_REQUEST["sloop"]);
      for($w = 1; $w <=2; $w++) {
        $file = document_check($_SESSION["remote_domino_path_epcmain"], "v.get_creport_by_pot_and_loop", $_REQUEST["pot"]."@".$dsploop."@".($w));
        if(strlen($file) <= 1) {
          $file = "<cr><loop>".$maxloop."</loop></cr>";
        }
        $tc_data["cr"][$w - 1] = process_xml_from_string($file);
        $tc_data["cr"][$w - 1]["load"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.get_creport_by_pot?open&count=99999&restricttocategory=".$_REQUEST["pot"]."@".($w)."&function=xml:data");
      }

      $num = ($_REQUEST["sloop"] != "") ? $_REQUEST["sloop"] : $wf_loop;
      $dsp_pot_part_tool .=
        "<tr>".
        "<th style=\"background-image:url(../../../../library/images/bg_sel.gif); color:rgb(255,255,255);\" colspan=\"4\"><strong><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Control Report headline');\">Control Report<a name=\"control_report\">&nbsp;</a>Alternative Source&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loop:</span></strong>";
      if($maxloop > 1) {
        for($z = 0; $z < $maxloop; $z++) $dsp_loops[] = $z;
        $dsp_pot_part_tool .= embed_selectbox(false, true, $dsp_loops, $dsp_loops, $num, array("name" => "select_creport", "style" => "background-color:rgb(220,220,220);border:solid 1px rgb(99,99,99);width:60px;font:normal 11px century gothic,verdana;color:rgb(0,0,0);height:20px;padding:0px;margin-left:10px;", "onchange" => "loop = (this.value != '') ?(Number(this.value)) : ''; location.href='?".str_replace("&sloop=".$_REQUEST["sloop"], "", $_SERVER["QUERY_STRING"])."&sloop=' + loop + '#control_report';"));
      }
      // STATUS anzeigen
      if($tc_data["cr"]["1"]["status"] == "0") $dsp_status = "in preparing";
      if($tc_data["cr"]["1"]["status"] == "1") $dsp_status = "to process";
      if($tc_data["cr"]["1"]["status"] == "2") $dsp_status = "in process";
      if($tc_data["cr"]["1"]["status"] == "3") $dsp_status = "prod. eval.";
      if($tc_data["cr"]["1"]["status"] == "4") $dsp_status = "withdrawn";
      if($tc_data["cr"]["1"]["status"] == "5") $dsp_status = "process eval.";
      if($tc_data["cr"]["1"]["status"] == "5" && $tc_data["cr"]["1"]["DateIRA"] != "false" && $tc_data["cr"]["1"]["StatusIRA"] != "G") $dsp_status = "forwarded to PE";
      if($tc_data["cr"]["1"]["status"] == "5" && $tc_data["cr"]["1"]["StatusIRA"] == "G") $dsp_status = "Process rejected";
      if($tc_data["cr"]["1"]["status"] == "6") $dsp_status = "prod. & proc. eval.";
      if($tc_data["cr"]["1"]["status"] == "7") $dsp_status = "obsolete";
      if($tc_data["cr"]["1"]["status"] == "8") $dsp_status = "deleted";

      $ptr = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/v.get_last_process_trial_run?open&restricttocategory=".$tc_data["cr"][1]["number"]."&count=1&function=xml:data");
      if(!strpos(strtolower($ptr), "no documents found")) {
        $tmp = process_xml_from_string($ptr);
        $tc_data["cr"][1]["status_ira_process"][0] = $tmp["cr"];
        unset($tmp);
      }

      $dsp_pot_part_tool .=  "</th></tr>";

      if(isset($tc_data["cr"]["1"]["number"])) {
        $dsp_pot_part_tool .= "<tr>".
          "<td class=\"descr_top\" style=\"width:1%;\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data_top\"><a id=\"get_cr_number\" href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"]["1"]["number"]."\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"]["1"]["number"]."</a></td>".
          "<td class=\"descr_top\" style=\"width:1%;\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data_top\">".format_date($tc_data["cr"]["1"]["issued"])."</td>".
          "</tr>".
          "<tr>".
          "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRT:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\">".$tc_data["cr"]["1"]["status_ir"]."</td>".
          "<td class=\"descr\" style=\"width:1%;\"><nobr>Drw. Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\">".$tc_data["cr"]["1"]["drwrev"]."</td>".
          "</tr>".
          "<tr>".
          "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRP:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\">".$tc_data["cr"]["1"]["status_irp"]."</td>".
          "<td class=\"descr\" style=\"width:1%;\"><nobr>Part Rev.:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\">".$tc_data["cr"]["1"]["rev"]."</td>".
          "</tr>".
          "<tr>".
          "<td class=\"descr\" style=\"width:1%;\"><nobr>Status IRS:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\">".$tc_data["cr"]["1"]["status_irs"]."</td>".
          "<td class=\"data\" colspan=\"2\">&nbsp;</td>".
          "</tr>".
          "<tr>".
          "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\" style=\"border-bottom:none;\">".$tc_data["cr"]["1"]["status_ira"]."<img src=\"../../../../library/images/blank.gif\" /></td>".
          "<td class=\"descr\" style=\"width:1%;border-bottom:none;\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
          "<td class=\"data\" style=\"border-bottom:none;\">".$dsp_status."</td>".
          "</tr>";

        $cnt = count($tc_data["cr"][1]["status_ira_process"]);
        if($cnt > 0) {
          $dsp_pot_part_tool .=
            "<tr>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>CR No.:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);border-top:solid 1px rgb(255,255,255);\"><a href=\"".$_SESSION["remote_domino_path_epcmain"]."/v.n.allcreportsbynumber/".$tc_data["cr"][1]["status_ira_process"][$cnt - 1]["number"]."?open\" style=\"color:rgb(255,255,255);\" class=\"linkdata\" target=\"_blank\">".$tc_data["cr"][1]["status_ira_process"][$cnt - 1]["number"]."</a><img src=\"../../../../library/images/blank.gif\" /></td>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);\"><nobr>Date Issued:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);\">".format_date($tc_data["cr"][1]["status_ira_process"][$cnt - 1]["date"])."</td>".
            "</tr>".
            "<tr>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status IRA:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".$tc_data["cr"][1]["status_ira_process"][$cnt - 1]["status_ira"]."</td>".
            "<td class=\"descr\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);width:1%;border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\"><nobr>Status:&nbsp;&nbsp;&nbsp;</nobr></td>".
            "<td class=\"data\" style=\"background-image:url(../../../../library/images/bg_cr.gif);color:rgb(255,255,255);border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\">".rawurldecode($tc_data["cr"][1]["status_ira_process"][$cnt - 1]["status"])."</td>".
            "</tr>";
        }

        // Create Process trial run Button
        $do_trial_run = (in_array("[composeproz]", $_SESSION["remote_userroles"])) ? true : false;			                                                           // Rolle
        if ($tc_data["pot"]["status"] == "0") $do_trial_run = false;                                                                                                               // POT inactive
        if(!in_array(substr($tc_data["tool"]["number"],0,2), Array(21, 23, 28))) $do_trial_run = false;		                                                                   // Tool Prefix
        if(strtoupper($tc_data["pot"]["meta"]["process_type"]) == "pilot" || $tc_data["pot"]["meta"]["process_type"]=="Not selected") $do_trial_run = false;                       // Prozesstyp
        if (is_array($pot_date[$_REQUEST["pot"]]["G03980"][0]["actual_value"])) $do_trial_run = false;                                                                             // Design Approval has no actual
        if (!is_array($pot_date[$_REQUEST["pot"]]["G05220"][0]["actual_value"])) $do_trial_run = false;                                                                            // PPAP Release has actual
        if($cnt > 0 && is_array($tc_data["cr"][1]["status_ira_process"][$cnt - 1]["status_ira"])) $do_trial_run = false;	                                                   // Ein unbewerterter PTR exisitiert
        if ($tc_data["cr"]["1"]["DateIRA"] == "false" || is_array($tc_data["cr"]["1"]["status_ir"])) $do_trial_run = false;			                                   // Product assessment available ?
        if($num != $wf_loop) $do_trial_run = false;                                                                                                                                // actual loop ?
        if($do_trial_run) {
          $dsp_pot_part_tool .= "<tr>".
            "<td class=\"descr\" colspan=\"4\" id=\"m_b1__tracker_cr_s1\" style=\"visibility:hidden;\"><span class=\"phpbutton\"><a href=\"javascript:run_ajax('run_ajax','".rawurlencode(rawurlencode($_SESSION["remote_domino_path_epcmain"]."/a.copy_cr?open&cr_reason=process%20approval&cr_type=2&unid=".$tc_data["cr"][1]["unid"]."&user=".rawurlencode($_SESSION["domino_user"])))."','open_cr_copy()');\">Create process trial run</a></span></td>".
            "</tr>";
        }
      } else {
        $fot_date = ", '".substr($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_fot_loop_".$wf_loop_source[0]],0,10)."'";
        $dimreport_date = ", '".substr($pot_date[$_REQUEST["pot"]][$source[1]][0]["wf_actual_dimreport_loop_".$wf_loop_source[0]],0,10)."'";

         $_p = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$sources[0][1]][0]["wf_actual_".strtolower("G04070")."_loop_".$wf_loop])));
         $fot_date = ", '".substr($_p[0], 0, 10)."'";

         $_d = array_reverse(explode("\\", rawurldecode($pot_date[$_REQUEST["pot"]][$sources[0][1]][0]["wf_actual_".strtolower("G04080")."_loop_".$wf_loop])));
         $dimreport_date = ", '".substr($_d[0], 0, 10)."'";
      }

      // Create new CR Button
      $do_new_cr = (in_array("[t_d]", $_SESSION["remote_userroles"]) || in_array("[cr]", $_SESSION["remote_userroles"])) ? true : false;       // Rolle
      if ($tc_data["pot"]["status"] == "0") $do_new_cr = false;                                                                                // POT inactive
      if ($tc_data["pot"]["meta"]["process_type"] == "Not selected")  $do_new_cr = false;                                                      // Prozesstyp
      if (is_array($pot_date[$_REQUEST["pot"]]["G04050"][0]["actual_value"])) $do_new_cr = false;                                              // Design Approval has no actual
      if (!is_array($pot_date[$_REQUEST["pot"]]["G05050"][0]["actual_value"])) $do_new_cr = false;                                             // PPAP Release has actual
      if ($wf_loop_source[0] != $num ) $do_new_cr = false;                                                                                     // current loop is highest loop
      if ($wf_loop_source[0] == $tc_data["cr"]["1"]["loop"] ) $do_new_cr = false;                                                              // highest loop has no CR
      if($do_new_cr) {
        $dsp_pot_part_tool .= "<tr>".
          "<td class=\"data_top\" colspan=\"4\" style=\"padding:10px 10px 10px 2px;\"><span class=\"phpbutton\">".
          "<a href=\"javascript:void(0);\" onclick=\"create_cr('".$tc_data["pot"]["unique"]."', '".strtolower($tc_data["pot"]["unid"])."', '".strip_tags($tc_data["pot"]["source_link_unid"])."' , '".strip_tags($tc_data["pot"]["source_part_unid"])."', '".$_REQUEST["unique"]."', '".strip_tags($tc_data["pot"]["source_tool_unid"])."' , '2', '".rawurlencode("correction loop")."', '".($maxloop -1)."'".$fot_date.$dimreport_date.");\">Create NEW CR</a>".
          "</span></td>".
          "</tr>";
      }
    }
    // ENDE: CR 2nd source  ------------------------------------------------------------------------------------------------------------
    // ENDE: 2nd source  ---------------------------------------------------------------------------------------------------------------
//print $pot_date[$_REQUEST["pot"]]["G04050"][0]["actual_value"];

}

?>



   