<?php


include_once("../../../library/tools/addin_xml.php");

function m_x2_configuration($_application) {


   $xml = generate_xml($_SESSION["remote_domino_path_main"]."/v.help_lookup?open&count=99999&function=xml:data");
   $languages = array_unique($xml["language"]); sort($languages);

   $module_id = "m_x2";
   
   $t_visibility = (isset($_REQUEST[$module_id."_visibility"])) ? $_REQUEST[$module_id."_visibility"] : "false";

   $txt = ($t_visibility == "false") ? "Show configuration" : "Hide configuration"; 
   $attr = ($t_visibility == "false") ? "true" : "false"; 

   $html = 
   "<div class=\"selector\" style=\"margin-top:24px;\">\r\n<img src=\"../../../../library/images/icons/configure.png\" style=\"position:relative;top:4px;margin-right:6px;filter:Gray();\" /><a name=\"selector\" href=\"?".str_replace("&".$module_id."_visibility=".$_REQUEST[$module_id."_visibility"], "", $_SERVER["QUERY_STRING"])."&".$module_id."_visibility=".$attr."\" style=\"text-decoration:none;color:rgb(0,102,158);\">".$txt."</a>\r\n</div>\r\n";

   if($t_visibility == "true") {
      $img[0] = ($_application["preferences"]["header"]["type"] == "0" || !isset($_application["preferences"]["header"]["type"])) ? "high" : "reg";
      $img[1] = ($_application["preferences"]["header"]["type"] == "1") ? "high" : "reg";

      $oput[0] = "<tr><td style=\"width:1%;white-space:nowrap;\"><a href=\"javascript:save_perportal_field('type','0@header');\"><img src=\"../../../../library/images/icons/select_".$img[0].".png\" style=\"width:12px;border:none;\" /></a>Full header&nbsp;&nbsp;</td></tr><tr><td><a href=\"javascript:save_perportal_field('type','1@header');\"><img src=\"../../../../library/images/icons/select_".$img[1].".png\" style=\"width:12px;border:none;\" /></a>Small header&nbsp;&nbsp;(Application restart required)</td></tr>\r\n";

      $df["php"] = array("W - Y", "W-y d.m", "W-y D d.m", "d.m.Y", "m/d/Y", "Y-m-d", "W: d.m.Y", "W: m/d/Y", "W: Y-m-d", "D, d.m.Y", "D, m/d/Y", "D, Y-m-d");
      $df["dsp"] = array("CW  - YYYY", "CW-YY DD.MM",  "CW-YY WD DD.MM", "DD.MM.YYYY", "MM/DD/YYYY", "YYYY-MM-DD", "CW: DD.MM.YYYY", "CW: MM/DD/YYYY", "CW: YYYY-MM-DD", "WD, DD.MM.YYYY", "WD, MM/DD/YYYY", "WD, YYYY-MM-DD");
      for($i = 0; $i < count($df["php"]); $i++) {
         $img = ($_application["preferences"]["settings"]["date_format"] == $df["php"][$i]) ? "high" : "reg";
         $oput[1] .= "<tr><td style=\"width:1%;white-space:nowrap;\"><a href=\"javascript:save_perportal_field('date_format','".$df["php"][$i]."@settings');\"><img src=\"../../../../library/images/icons/select_".$img.".png\" style=\"width:12px;border:none;\" /></a>".$df["dsp"][$i]."</td><td>e.g. ".date($df["php"][$i], time())."</td></tr>\r\n";
     }


      foreach($languages as $lang) {
         $s_img = ($_application["preferred_language"] == $lang) ? "select_high" : "select_reg";
         $oput[2] .= "<tr><td><a href=\"javascript:void(0);\" onclick=\"run_ajax('run_ajax','".$_SESSION["remote_domino_path_perportal"]."/a.setfield?open&field=td%24preferred_language&value=".$lang."&unid=".$_SESSION["remote_perportal_unid"]."','location.reload()');\"><img src=\"../../../../library/images/icons/".$s_img.".png\" style=\"width:12px;border:none;\" /></a>".$lang."</td></tr>";
      

      }

      $html .= 
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"tbl_configuration\">".
      "<tr>\r\n".
      "<td style=\"width:1%;background-color:rgb(255,255,255);\"><nobr>Application style&nbsp;</nobr></td>".
      "<td style=\"padding:0px;\">".
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;border-top:solid 2px rgb(255,255,255);\">".$oput[0]."</table>".
      "</td>".
      "</tr>\r\n". 
      "<tr>\r\n".
      "<td style=\"width:1%;background-color:rgb(255,255,255);\"><nobr>Select date format&nbsp;</nobr></td>".
      "<td style=\"padding:0px;\">".
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;border-top:solid 2px rgb(255,255,255);\">".$oput[1]."</table>".
      "</td>".
      "</tr>\r\n". 


      "<tr>\r\n".
      "<td style=\"width:1%;background-color:rgb(255,255,255);\">Preferred language</td>".
      "<td style=\"padding:0px;\">".
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;border-top:solid 2px rgb(255,255,255);\">".$oput[2]."</table>".
      "</td>".
      "</tr>\r\n". 




      "</table>\r\n";
   } 

   return $html;
}

?>






