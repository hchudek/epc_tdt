<?php


// DOMINO UNID ANSTATT UNIQUE -> REDIRECT
if(strlen($_REQUEST["unid"]) == 32) {
   $unique= file_get_contents($_SESSION["remote_domino_path_main"]."/v.unique_by_unid?open&restricttocategory=".strtolower($_REQUEST["unid"])."&function=plain");
   header("Location: ?&unique=".$unique);
}

function calc_new_date($t_date, $operation, $wd) {

   if(strtolower($t_date) == "undefined") return "undefined";
   else {
      $t_time = strtotime(str_replace("/", "-", $t_date));
      $i = 0; while($i < intval($operation)) {
         $t_time += 86400;
         $t_date = date('Y-m-d', $t_time);
         if($wd[str_replace("-", "/", $t_date)] == "1" || !isset($wd[str_replace("-", "/", $t_date)])) $i++;
      }
      return $t_date;
   }
}

function m_b1_tracker($_application) {

   global $tc_data;
   $supervisor = in_array("[supervisor]", $_SESSION["remote_userroles"]);
   set_html_title($tc_data["project"]["number"]);


   $tab = 1;
   if($_REQUEST["pot"] != "") $tab = 99;
   if($_REQUEST["project"] != "") $tab = 2;
   if($_REQUEST["tree"] == "true") $tab = 3;
 
   $sel_tab=($_REQUEST[$_SESSION["module"][$_module_name]."_tab"]=="") ? "1" : $_REQUEST[$_SESSION["module"][$_module_name]."_tab"]; 


   // ***************************
   // START: Projekttermine laden
   // ***************************
   $project_date = generate_xml($_SESSION["remote_domino_path_main"]."/v.dates_by_project?open&count=99999&restricttocategory=".urlencode($tc_data["project"]["number"])."&function=xml:data");
   // **************************
   // ENDE: Projekttermine laden
   // **************************

   // *****************
   // START: POTS laden
   // *****************
   $pots_tmp = generate_xml($_SESSION["remote_domino_path_main"]."/v.pots_by_tracker?open&count=99999&restricttocategory=".$tc_data["tracker"]["unique"]."&function=xml:data");
   if(is_array($pots_tmp["pot"])) $pots_tracker = $pots_tmp["pot"];
   else $pots_tracker[0] = $pots_tmp["pot"];
   // ****************
   // ENDE: POTS laden
   // ****************


   // ************************
   // START: POT-Termine laden
   // ************************
   foreach($pots_tracker as $pot) {
      $pot_date[$pot] = generate_xml($_SESSION["remote_domino_path_main"]."/v.dates_by_tracker?open&count=99999&restricttocategory=".$pot."@".$tc_data["tracker"]["unique"]."&function=xml:data");
      if(isset($pot_date[$pot]["h2"])) unset($pot_date[$pot]);
      else {
         foreach(array_keys($_application["process"]["subprocess"]) as $val) {
            if(!isset($pot_date[$pot][$val][0])) {
               $tmp = $pot_date[$pot][$val];
               unset($pot_date[$pot][$val]);
               $pot_date[$pot][$val][0] = $tmp;
            }
         }
      }
   }
   // ***********************
   // ENDE: POT-Termine laden
   // ***********************


   // ******************************
   // START: TERMINE NEU KALKULIEREN
   // ******************************

   //if(!$_POST) unset($_SESSION["posted"]);

   if($_POST && $_POST != $_SESSION["posted"]) {
     $loop = count($pot_date[$tc_data["pot"]["unique"]][$_POST["phase"]]) - 1;
      $pot_date[$tc_data["pot"]["unique"]][$_POST["phase"]][$loop][$_POST["data_type"]] = $_POST["value"];

      // START: Werktagedefinition einbinden
      $t_year = date("Y", time());
      $go_past = 2;
      $go_future = 4;

      $start_date = mktime(0, 0, 0, 1, 1, intval($t_year) - $go_past);
      $end_date = mktime(0, 0, 0, 12, 31, intval($t_year) + $go_future);

      $this_date = $start_date;

      $i = 86400; while($this_date <= $end_date) {
         $is_weekday = (in_array(date("w", $this_date), explode(":", "0:6")) == 0) ? 1 : 0;
         $wd[date("Y/m/d", $this_date)] = $is_weekday;
         $this_date += $i;
      }

      $xml_wd = generate_xml($_SESSION["remote_domino_path_perportal"]."/v.get_workingdays?open&restricttocategory=".$_SESSION["remote_perportal_unid"]."&function=xml:data");

      foreach(array_keys($xml_wd) as $key) {
         $count = 0;
         $y = substr($key,-4);
         for($d = 1; $d < 32; $d++) {
            for($m = 1; $m < 13; $m++) {
               if(checkdate($m, $d, intval($y)) == true) {
                  $count++;
                  $c_date =  mktime(0, 0, 0, $m, $d, intval($y));
                  $wd[date("Y/m/d", $c_date)] = substr($xml_wd[$key], $count - 1, 1);
               }
            }
         } 
      }
      // ENDE: Werktagedefinition einbinden


      if($_POST["use_rule"] == "0" && $_POST["data_type"] != "revised_value_all" && $_POST["value"] != "") {				// Ein Termin �ndern
         $t_loop = $_POST["loop"];
         $t_unid = $pot_date[$tc_data["pot"]["unique"]][$_POST["phase"]][$t_loop]["unid"];
         $t_field = "DATE_".strtoupper(str_replace("_value", "", $_POST["data_type"]));
         $t_value = $pot_date[$tc_data["pot"]["unique"]][$_POST["phase"]][$loop][$_POST["data_type"]]."@".date("Y-m-d",time())."@".$_SESSION["domino_user"];
         $f_do = "\r\n<script type=\"text/javascript\">document.getElementsByTagName('body')[0].style.display='none';handle_save_single_field(\"".$t_unid."\", \"".$t_field."\", \"".$t_value."\", \"location.href=location.href\");\r\n</script>\r\n";
         unset($_POST);
      }

      if($_POST["use_rule"] == "1" && $_POST["data_type"] != "revised_value_all") {	
									// Termine mit Regel �ndern
         $recalc = false;
         $t_loop = $_POST["loop"];
         $t_unid = $pot_date[$tc_data["pot"]["unique"]][$_POST["phase"]][$t_loop]["unid"];
         $t_field = "DATE_REVISED";
         $t_value = $pot_date[$tc_data["pot"]["unique"]][$_POST["phase"]][$t_loop][$_POST["data_type"]]."@".date("Y-m-d",time())."@".$_SESSION["domino_user"];
         $f_do = "\r\n<p style=\"display:none;\"><a name=\"save_unid\">".$t_unid."</a><a name=\"save_field\">".$t_field."</a><a name=\"save_value\">".$t_value."</a>\r\n";

         $p_add = false; foreach($_application["process"]["subprocess"] as $p_key=>$p_val) {
            if($p_key == $_POST["phase"]) $p_add = true;
            if($p_add == true) $recalc[] = $p_key;
         }

         foreach($pot_date[$tc_data["pot"]["unique"]] as $key => $t_pot_date) {
            if(in_array($key, $recalc) && $key != $_POST["phase"]) {
               $loop = count($pot_date[$tc_data["pot"]["unique"]][$key]) - 1; 

               $from_id = $_application["process"]["rules"][$tc_data["pot"]["meta"]["process_rules"]][$key]["from_id"];
               $operation = $_application["process"]["rules"][$tc_data["pot"]["meta"]["process_rules"]][$key]["operation"];

               $n_date = calc_new_date($pot_date[$tc_data["pot"]["unique"]][$from_id][0]["revised_value"], $operation, $wd);
               $pot_date[$tc_data["pot"]["unique"]][$key][$loop]["revised_value"] = $n_date;
               $t_unid = $t_pot_date[$loop]["unid"];
               $t_field = "DATE_REVISED";
               $t_value = $n_date."@".date("Y-m-d",time())."@".$_SESSION["domino_user"];
               if(count($pot_date[$tc_data["pot"]["unique"]][$key][$loop]["actual_value"]) == 0) $f_do .= "\r\n<a name=\"save_unid\">".$t_unid."</a><a name=\"save_field\">".$t_field."</a><a name=\"save_value\">".$t_value."</a>\r\n";

            }
         }
         if($f_do != "") $f_do .= "\r\n</p><script type=\"text/javascript\">save_multiple_dates();</script>\r\n";
         unset($_POST);
      }

      if($_POST["data_type"] == "revised_value_all") {									// Einen Termin in allen POT �ndern

         foreach($tc_data["tracker"]["ref_pot"] as $t_pot) {
            foreach($pot_date[$t_pot] as $key=>$t_pot_date) {
               $t_phase = str_replace("M_", "", $_POST["phase"]);
               if($t_phase == $key) {

                  $loop = count($t_pot_date) - 1; 
                  $t_data_type = str_replace("_all", "", $_POST["data_type"]);
                  $t_value = $_POST["value"]."@".date("Y-m-d",time())."@".$_SESSION["domino_user"];
                  $t_unid = $t_pot_date[$loop]["unid"];
                  $t_field = ($_SESSION["header"]["filter_tracker_dates"] == "2") ? "DATE_ACTUAL" : "DATE_REVISED";
                  $do_dsp = (in_array($t_pot, $tc_data["tracker"]["hide_pot"])) ? 0 : 1;
                  if(count($t_pot_date[$loop]["actual_value"]) == 0 && $do_dsp) $f_do .= "\r\n<a name=\"save_unid\">".$t_unid."</a><a name=\"save_field\">".$t_field."</a><a name=\"save_value\">".$t_value."</a>\r\n";

              }
            }
         }
         if($f_do != "") $f_do .= "\r\n</p><script type=\"text/javascript\">save_multiple_dates();</script>\r\n";
      }
      $_SESSION["posted"] = $_POST;
   }
   // *****************************
   // ENDE: TERMINE NEU KALKULIEREN
   // *****************************


   // *******************
   // START: FORM CONTENT
   // *******************
   $class = array("reg", "reg", "reg");
   $style = array("", "", "");
   switch($tab) {
      case "1":
         $class[0] = "high";
         $style[0] = " style=\"background-color:#822433;\"";
         break;
      case "2":
         $class[1] = "high";
         $style[1] = " style=\"background-color:#822433;\"";
         break;
      case "3":
         $class[2] = "high";
         $style[2] = " style=\"background-color:#822433;\"";
         break;
   }

   $rel = "-1px";
   $bgcol = (strtolower($_REQUEST["tree"]) == "true") ? "130,36,51" : "255,255,255";
   $t_img = (strtolower($_REQUEST["tree"]) == "true") ? "tree_high" : "tree";

   $cspan = (count($pots_tracker) - count($tc_data["tracker"]["hide_pot"]) + 4);
   if($supervisor) $form .= "<div style=\"position:absolute;top:43px;font:normal 12px centuty gothic;background-color:rgb(255,182,12);padding:2px;\">Supervisor: <a style=\"font:normal 12px centuty gothic;color:rgb(0,102,158);text-decoration:none;\" href=\"".$_SESSION["remote_domino_path_main"]."/v.get_xml_data/".$_REQUEST["unique"]."?open&pot=".$_REQUEST["pot"]."&project=".$_REQUEST["project"]."\" target=\"_blank\">View XML</a></div>";

   $form .= 
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tbl_tracker\" style=\"width:100%;position:relative;top:%%REL%%;\">\r\n".
   "<tr><td style=\"background-color:rgb(".$bgcol.");border-right:solid 2px rgB(255,255,255);\"><a href=\"?&unique=".$_REQUEST["unique"]."&tree=true\"><img src=\"../../../../library/images/icons/".$t_img.".png\" style=\"border:none;margin-right:6px;\" /></a></td>".
   "<td class=\"tabs_".$class[0]."\"$style[0]><a href=\"?&unique=".$tc_data["tracker"]["unique"]."\">Date</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>".
   "<td class=\"tabs_".$class[1]."\"$style[1]><a href=\"?&unique=".$tc_data["tracker"]["unique"]."&project=".base64_encode($tc_data["project"]["number"])."\"><nobr>".$tc_data["project"]["number"]."</nobr></a></td>";


   $status_filter = str_replace("Inactive", "0", str_replace("Active", "1", explode("," ,$tc_data["tracker"]["status_filter"])));


   $count = 0; foreach($pots_tracker as $pot) {

      if($pot != "") {
         $rel = "-7px";
         $dsp_tab = str_replace(" / ", "<br />", $tc_data["tracker"]["pot_dsp_name"][$count]);
         $count++;
         $class[3] = ($pot == $_REQUEST["pot"]) ? "high" : "reg";
         $style[3] = ($pot == $_REQUEST["pot"]) ? " style=\"background-color:#822433;\"" : "";
         if($pot != $_REQUEST["pot"] && $tc_data["tracker"]["pot_status"][$count - 1] == "0") {
            $style[3] = " style=\"background-color:#de3831;\"";
         }

         $do_dsp = (!in_array($tc_data["tracker"]["ref_pot"][$count - 1], $tc_data["tracker"]["hide_pot"]));
         $do_dsp = (in_array($tc_data["tracker"]["pot_status"][$count - 1], $status_filter)) ? $do_dsp : false;
         $dsp_status_filter = ($_REQUEST["status_filter"] != "") ? "&status_filter=".$_REQUEST["status_filter"] : "";
         if($do_dsp) $form .= "<td class=\"tabs_".$class[3]."\"".$style[3]."><a href=\"?&unique=".$tc_data["tracker"]["unique"]."&pot=".$pot.$dsp_status_filter."\"><nobr>".$dsp_tab."</nobr></a></td>";
      }
   } 
   $form .= "<td style=\"display:none;\" class=\"tabs_".$class[2]."\"$style[2]><nobr>Tree</nobr></td>";
   $form .= "<td class=\"tabs_reg\" style=\"width:1%;\"><a href=\"add_pot.php?&unique=".$tc_data["tracker"]["unique"]."&project=".base64_encode($tc_data["project"]["number"])."\">Add&nbsp;POT</a></td>".
   "<td style=\"width:99%;\">&nbsp;</td>".
   "</tr><tr><td colspan=\"".$cspan."\" style=\"border-top:solid 2px rgb(255,255,255);border-bottom:solid 2px rgb(255,255,255);background-color:rgb(90,90,90);\"><img src=\"../../../../library/images/blank.gif\" style=\"width:2px;\"/></td></tr>\r\n";



   switch (strtoupper($tc_data["project"]["phasedesc"])) {
      case "DEFINITION": $phase = "G01"; break;
      case "CONCEPT": $phase = "G02"; break;
      case "DESIGN": $phase = "G03"; break;
      case "VALIDATION": $phase = "G04"; break;
      case "INDUSTRIALIZE": $phase = "G05"; break;
      case "PRODUCTION": $phase = "G06"; break;
      default: $phase = "G00";
   }


   // *****
   // TAB 1
   // *****
   if($tab == "1") { 	

      if(!isset($_SESSION["header"]["filter_tracker_dates"])) $_SESSION["header"]["filter_tracker_dates"] = "1"; 

      $img[0] = ($_SESSION["header"]["filter_tracker_dates"] == "1") ? "high" : "reg";
      $img[1] = ($_SESSION["header"]["filter_tracker_dates"] == "2") ? "high" : "reg";
      $img[2] = ($_SESSION["header"]["filter_tracker_dates"] == "3") ? "high" : "reg";

      $form .= "<tr><td colspan=\"".$cspan."\" style=\"height:32px;border-bottom:dotted 1px rgb(99,99,99);\">".
      "<div id=\"anchor_date_tab\" style=\"background-color:rgb(255,194,54);font:bold 12px verdana;color:rgb(0,0,0);height:23px;width:100%;padding-left:10px;margin-top:8px;margin-bottom:2px; \"><span style=\"position:relative; top:4px; cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-date overview headline');\">Date overview</span></div>".
      "<span><a href=\"addin/set_header.php?set=".base64_encode("filter_tracker_dates:1")."&redirect=".base64_encode("edit_tracker.php?".$_SERVER["QUERY_STRING"])."\"><img src=\"../../../../library/images/icons/select_".$img[0].".png\" style=\"border:none;width:12px;margin-right:4px;vertical-align:-2px;\" /></a>Planned / revised dates</span>".
      "<span><a href=\"addin/set_header.php?set=".base64_encode("filter_tracker_dates:2")."&redirect=".base64_encode("edit_tracker.php?".$_SERVER["QUERY_STRING"])."\"><img src=\"../../../../library/images/icons/select_".$img[1].".png\" style=\"border:none;width:12px;margin-right:4px;margin-left:9px;vertical-align:-2px;\" /></a>Actual dates</span>".
      "<span><a href=\"addin/set_header.php?set=".base64_encode("filter_tracker_dates:3")."&redirect=".base64_encode("edit_tracker.php?".$_SERVER["QUERY_STRING"])."\"><img src=\"../../../../library/images/icons/select_".$img[2].".png\" style=\"border:none;width:12px;margin-right:4px;margin-left:9px;vertical-align:-2px;\" /></a>All dates</span>".
      "</td></tr>\r\n<tr><td><img src=\"../../../../library/images/blank.gif\" style=\"width:1px;\" /></td></tr>\r\n";		


      $arr = array_keys($_application["process"]["subprocess"]);
      for($i = 0; $i < count($arr); $i++) {
         $val = $arr[$i];

         $t_date = "";
         if(in_array($_SESSION["header"]["filter_tracker_dates"], explode(":", "1:3"))) {
            $t_date1 = (isset($_application["process"]["subprocess"][$val])) ? strtotime($project_date[$val]["revised_value"]) : "&nbsp;";
            if($t_date1 > 0) $t_date1 = date($_application["preferences"]["settings"]["date_format"], $t_date1);
         }
         if(in_array($_SESSION["header"]["filter_tracker_dates"], explode(":", "2:3"))) {
            $t_date2 = (isset($_application["process"]["subprocess"][$val])) ? strtotime($project_date[$val]["actual_value"]) : "&nbsp;";
            if($t_date2 > 0) $t_date2 = date($_application["preferences"]["settings"]["date_format"], $t_date2);
         }
         $t_date = ($_SESSION["header"]["filter_tracker_dates"] == "1") ? $t_date1 : $t_date2;
         $t_date = ($_SESSION["header"]["filter_tracker_dates"] == "3") ? $t_date1."<br />&nbsp".$t_date2 : $t_date;
         $count = 0;
         unset($p_dates); foreach($pots_tracker as $pot) {
         $process_type = false;
            for($chk = 0; $chk < count($tc_data["tracker"]["ref_pot"]); $chk++) {
               if($tc_data["tracker"]["ref_pot"][$chk] == $pot) {
                  $process_type = $tc_data["tracker"]["process_type"][$chk];
                  if(!isset($tc_data["tracker"]["process_type"][$chk])) $process_type = $tc_data["tracker"]["process_type_tmp"];
                  break;
               }
            }

            if($pot !="") {
               $tmp_loop = count($pot_date[$pot][$val]) - 1;
               $dsp_pot_date = (strtotime($pot_date[$pot][$val][$tmp_loop]["revised_value"]) > 0 ) ? date($_application["preferences"]["settings"]["date_format"], strtotime($pot_date[$pot][$val][$tmp_loop]["revised_value"])) : $pot_date[$pot][$val][$tmp_loop]["revised_value"];
               if($_SESSION["header"]["filter_tracker_dates"] == "1") $tmp = (count($pot_date[$pot][$val][$tmp_loop]["revised_value"]) > 0) ? $dsp_pot_date."&nbsp;&nbsp;[".$pot_date[$pot][$val][$tmp_loop]["revision"]."]" : "&nbsp;";
               else {
                  $tmp = (count($pot_date[$pot][$val][$tmp_loop]["actual_value"]) > 0 && strtotime($pot_date[$pot][$val][$tmp_loop]["actual_value"]) > 0) ? date($_application["preferences"]["settings"]["date_format"], strtotime($pot_date[$pot][$val][$tmp_loop]["actual_value"])) : "&nbsp;";
                  if($_SESSION["header"]["filter_tracker_dates"] == "3") {
                     if(strtotime($pot_date[$pot][$val][$tmp_loop]["revised_value"]) != "") $tmp = date($_application["preferences"]["settings"]["date_format"], strtotime($pot_date[$pot][$val][$tmp_loop]["revised_value"]))."&nbsp;&nbsp;[".$pot_date[$pot][$val][$tmp_loop]["revision"]."]"."<br />&nbsp;".$tmp;
                     else if($_SESSION["header"]["filter_tracker_dates"] == "3") $tmp = "<br />&nbsp;".$tmp;
                  }
               }
               $count++;

               $do_dsp = (in_array($tc_data["tracker"]["ref_pot"][$count - 1], $tc_data["tracker"]["hide_pot"])) ? 0 : 1;
               $do_dsp = (in_array($tc_data["tracker"]["pot_status"][$count - 1], $status_filter)) ? $do_dsp : false;
               $style = (in_array($arr[$i], $_application["process"]["type"][rawurldecode($process_type)])) ? "" : "background-image:url(../../../../library/images/bg_no_date.gif);";
               if(!in_array($arr[$i], $_application["process"]["type"][rawurldecode($process_type)])) $tmp = "&nbsp;";
               if($do_dsp) $p_dates .= "<td class=\"tracker_date\" style=\".$style.\">&nbsp;".$tmp."<br /><img src=\"../../../../library/images/blank.gif\" style=\"width:130px;height:1px;\" /></td>";
            }
         }

         $onclick = ($_SESSION["header"]["filter_tracker_dates"] == "3" || substr($val, -2) == "99") ? "void(0);" : "load_calendar('m_".strtolower($val)."','".$_SESSION["remote_database_path"]."addin/calendar2.php?&p_use_rule=0&p_data_type=revised_value_all&p_id=calendar_m_".strtolower($val)."','');";
         $cursor = ($_SESSION["header"]["filter_tracker_dates"] == "3" || substr($val, -2) == "99") ? "default" : "pointer"; 

         $dsp_process = explode(":", $_application["process"]["display"][$val]);

         $tmp_sep = (substr($val, -2) == "99") ? "<tr><td style=\"border-top:solid 1px rgb(255,255,255);border-bottom:solid 1px rgb(255,255,255);\" colspan=\"".$cspan."\"><img src=\"../../../../library/images/te_orange.png\" style=\"width:100%;height:2px;\" /></td></tr>\r\n" : "";


         if(in_array($phase, $dsp_process) || $tc_data["tracker"]["date_filter"] == "0") {
            $form .= "<tr>".
            "<td colspan=\"2\">".
            "<span onclick=\"".$onclick."\" style=\"cursor:".$cursor.";white-space:nowrap;\">".
            substr($_application["process"]["subprocess"][$val], 0, strpos($_application["process"]["subprocess"][$val],":")).
            "</span><br />".
            "<div id=\"calendar_m_".strtolower($val)."\" style=\"position:absolute;\"></div>".
            "</td><td class=\"project_date\" style=\"background-image:url(../../../../library/images/bg_sel.gif);\">&nbsp;".$t_date."</td>".
            $p_dates."<td colspan=\"1\" style=\"background-color:rgb(220,220,220);border:solid 1px rgb(255,255,255);\">&nbsp;</td></tr>\r\n".$tmp_sep;
         }
      }
   }



   // *****
   // TAB 2
   // *****
   if($tab == "2") {
      $form .= "<table><td>";
    
      include("modules/Elements/m_b1.tracker/project.php");
      $form .= $html ;


   }



   // ********************
   // TAB 3 - X (POT TABS)
   // ********************
   if($tab == "99") {	


      $dsp_process = 
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:0px;width:500px;\" id=\"tbl_pot_process\">";

      $dsp_pot_info = (isset($pot_date[$tc_data["pot"]["unique"]])) ? "<strong>POT</strong>" : "<a style=\"font:normal 12px century gothic, verdana;\" href=\"javascript:create_process('".$tc_data["tracker"]["unique"]."','".$tc_data["pot"]["unique"]."');\">Create process</a>";
      if($tc_data["pot"]["meta"]["process_rules"] == "Not selected" || $tc_data["pot"]["meta"]["process_type"] == "Not selected") $dsp_pot_info = "Select type & rules";

      $actual = $pot_date[$tc_data["pot"]["unique"]]["G0408"][count($pot_date[$tc_data["pot"]["unique"]]["G0408"]) - 1]["actual_value"];
      
      $dsp_process .="<tr><td>".
      "<input type=\"hidden\" name=\"last_actual_date\" value=\"".rawurlencode(str_replace("01.01.1970", "", date("d.m.Y", strtotime($actual))))."\" />\r\n".
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tbl_inner_process\">".
      "<tr><th>&nbsp;</th><th><span style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-tracker-Subprocess headline');\"><strong>Subprocess</strong></span></th><th style=\"width:30px;\">&nbsp;</th><th style=\"width:120px;\"><strong>Project</strong></th><th style=\"width:120px;\">".$dsp_pot_info."</th></tr>";


      $a_key = array_keys($_application["process"]["subprocess"]);
      

      for($a_key_count = 1; $a_key_count  <= count($a_key); $a_key_count++) {
         $val = $a_key[$a_key_count];

         $t_date["revised"] = (isset($_application["process"]["subprocess"][$val])) ? $project_date[$val]["revised_value"] : "&nbsp;";
         $t_date["actual"] = (isset($_application["process"]["subprocess"][$val])) ? $project_date[$val]["actual_value"] : "&nbsp;";
         if(is_array($t_date["revised"])) $t_date["revised"] = "&nbsp;";
         if(is_array($t_date["actual"])) $t_date["actual"] = "&nbsp;";

         // Datumformat einrichten
         if(strtotime($t_date["revised"]) > 0) $t_date["revised"] = date($_application["preferences"]["settings"]["date_format"], strtotime($t_date["revised"]));
         if(strtotime($t_date["actual"]) > 0) $t_date["actual"] = date($_application["preferences"]["settings"]["date_format"], strtotime($t_date["actual"]));

         $loop = count($pot_date[$tc_data["pot"]["unique"]][$val]);
         if($loop > $maxloop) $maxloop = $loop;
         unset($prvval);

         for($i = 0; $i < $loop; $i++) {


            // LOOPS UNTEREINANDER ANORDNEN
            $a_key_check = 0;
            if($loop > 1) {
               while(count($pot_date[$tc_data["pot"]["unique"]][$val]) == count($pot_date[$tc_data["pot"]["unique"]][$a_key[$a_key_check + 1 + $a_key_count]])) {
                  $a_key_check++;
               }
            }
            $dsp_block = $a_key_check + 1;

            for($ii = 0; $ii < $dsp_block; $ii++) {
               $val = $a_key[$a_key_count + $ii];
               $indicator = "";
               unset($p_date); 
               $p_date["revised"] = (count($pot_date[$tc_data["pot"]["unique"]][$val][$i]["revised_value"]) > 0) ? $pot_date[$tc_data["pot"]["unique"]][$val][$i]["revised_value"] : "&nbsp;";
               $p_date["actual"] = (count($pot_date[$tc_data["pot"]["unique"]][$val][$i]["actual_value"]) > 0) ? $pot_date[$tc_data["pot"]["unique"]][$val][$i]["actual_value"] : "&nbsp;";
               $p_date["revision"] = (count($pot_date[$tc_data["pot"]["unique"]][$val][$i]["revision"]) > 0) ? $pot_date[$tc_data["pot"]["unique"]][$val][$i]["revision"] : "&nbsp;";
               $style = (substr($val, -2) == "99") ? "background-image:url(../../../../library/images/bg_sel.gif);color:rgb(255,255,255);" : "";


               // ****************
               // START: INDICATOR
               // ****************  

               // INDICATORFARBE FESTLEGEN
               $class = "process_white";	
               $loop2 = count($pot_date[$tc_data["pot"]["unique"]][$a_key[$a_key_count]]);

               if(strtotime($p_date["revised"]) > 0 &&  strtotime($p_date["revised"]) > (time() - 86400) && strtotime($p_date["revised"]) - time() - (86400 * 13) < -86400) $class= "process_yellow";			// REVISED DATE WENIGER ALS 2 WOCHEN IN DER ZUKUNFT
               if(strtotime($p_date["revised"]) > 0  && strtotime($p_date["revised"]) - time() < -86400) $class = "process_red";										// ACTUAL TIME LIEGT VOR REVISED DATE	
               if(isset($a_key[$a_key_count]) && (strtotime($p_date["revised"]) - strtotime($pot_date[$tc_data["pot"]["unique"]][$a_key[$a_key_count]][$i]["revised_value"])) > 0) $class = "process_orange";				// R Reihenfolge nicht auf dem Zeitstrahl	
               if(strtotime($p_date["actual"]) > 0) $class = "process_green";																// ACTUAL DATE IST GESETZT
               $prvval = $val;

               // Datumformat einrichten
               if(strtotime($p_date["revised"]) > 0) $p_date["revised"] = date($_application["preferences"]["settings"]["date_format"], strtotime($p_date["revised"]));
               if(strtotime($p_date["actual"]) > 0) $p_date["actual"] = date($_application["preferences"]["settings"]["date_format"], strtotime($p_date["actual"]));

               if(strtolower($p_date["revised"]) == "undefined") $class = "process_gray";
               if(($i +1 == $loop || 1 == 1) && str_replace("&nbsp;", "", $p_date["actual"]) != "!") {

                  $onclick = (substr($val, -2) == "99") ? "void(0);" : "handle_menu('menu_".strtolower($val."_".$i)."')";
                  $cursor = (substr($val, -2) == "99") ? "default" : "pointer";


                  $indicator .= "<div class=\"".$class."\" style=\"cursor:".$cursor.";\" onclick=\"".$onclick."\">&nbsp;</div>";
                  if(str_replace("&nbsp;", "", $p_date["actual"]) == "" || substr($_application["process"]["subprocess"][$val], strpos($_application["process"]["subprocess"][$val], ":") + 1, 1) != "0") {

                     $n_rev = ($p_date["revision"] == "P") ? "r1" : "r".(intval(substr($p_date["revision"],1,100) + 1));

                     $indicator .= 
                     "<div id=\"menu_".strtolower($val."_".$i)."\" class=\"menu\" style=\"width:210px;\">";
                     if(str_replace("&nbsp;", "", $p_date["actual"]) == "") {
                        // REVISED DATE 
                        // ----------------------------------------------------------------------------------
                        if($_application["process"]["write_revised"][$val] == "" || check_editable(explode(":", $_application["process"]["write_revised"][$val]))) {
                           $indicator .= "<a style=\"font:normal 12px century gothic, verdana;\" href=\"javascript:void(0);\" onclick=\"d=document.getElementById('reason_code');d.new_revision='".$n_rev."';d.unid='".$pot_date[$tc_data["pot"]["unique"]][$val][$i]["unid"]."';d.p_id='calendar_".strtolower($val."_".$i)."';d.innerText='".base64_encode($_application["process"]["reason_codes"][$val])."'; load_calendar('".strtolower($val."_".$i)."','".$_SESSION["remote_database_path"]."addin/calendar2.php?&p_use_rule=0&p_data_type=revised_value&p_id=calendar_".strtolower($val."_".$i)."','');\">1Set new revised date</a><br />";
                        }
                        else {
                           $indicator .= "<span style=\"font:normal 12px century gothic, verdana; color:rgb(99,99,99);\">Set new revised date</span><br />";
                        }
                        // REVISED DATE (WITH RULE)
                        // ----------------------------------------------------------------------------------
                        if(($i + 1) == $loop) {
                           if(strpos($_SESSION["domino_user"], $tc_data["tracker"]["responsible"]) > 0) {
                              $indicator .= "<a style=\"font:normal 12px century gothic, verdana;\" href=\"javascript:void(0);\" onclick=\"d=document.getElementById('reason_code');d.new_revision='".$n_rev."';d.unid='".$pot_date[$tc_data["pot"]["unique"]][$val][$i]["unid"]."';d.p_id='calendar_".strtolower($val."_".$i)."';d.innerText='".base64_encode($_application["process"]["reason_codes"][$val])."';load_calendar('".strtolower($val."_".$i)."','".$_SESSION["remote_database_path"]."addin/calendar2.php?&p_use_rule=1&p_data_type=revised_value&p_id=calendar_".strtolower($val."_".$i)."','');\">Set new revised date and use rule</a><br />";
                           }
                           else {
                              $indicator .= "<span style=\"font:normal 12px century gothic, verdana; color:rgb(99,99,99);\">Set new revised date and use rule</span><br />";

                           }
                        }
                        // ACTUAL DATE
                        // ----------------------------------------------------------------------------------
                        if($_application["process"]["write_actual"][$val] == "" || check_editable(explode(":", $_application["process"]["write_actual"][$val]))) {
                           $indicator .= "<a style=\"font:normal 12px century gothic, verdana;\" href=\"javascript:load_calendar('".strtolower($val."_".$i)."','".$_SESSION["remote_database_path"]."addin/calendar2.php?&p_use_rule=0&p_data_type=actual_value&p_id=calendar_".strtolower($val."_".$i)."','');\">Set actual date</a><br />";
                        }
                        else {
                           $indicator .= "<span style=\"font:normal 12px century gothic, verdana; color:rgb(99,99,99);\">Set actual date</span><br />";

                        }
                     }
                     if(substr($_application["process"]["subprocess"][$val], strpos($_application["process"]["subprocess"][$val], ":") + 1, 1) != "0") {
                        if(($i + 1) == $loop && (in_array("[t_d]", $_SESSION["remote_userroles"]) | in_array("[procurement]", $_SESSION["remote_userroles"]))) $indicator .= "<a style=\"font:normal 12px century gothic, verdana;\" href=\"javascript:void(0);\" onclick=\"document.getElementById('menu_".strtolower($val."_".$i)."').style.display='none';create_loops('".substr($_application["process"]["subprocess"][$val], strpos($_application["process"]["subprocess"][$val], ":") + 1, 999)."','".$tc_data["tracker"]["unique"]."','".$tc_data["pot"]["unique"]."');\">Create new loop</a><br />";
                     }
                     $indicator .=
                     "</div>".
                     "<div id=\"calendar_".strtolower($val."_".$i)."\" style=\"position:absolute;\"></div>".
                     "<a name=\"phase_id\" style=\"display:none;\">".strtolower($val)."</a>";
                  }
                  else $indicator = str_replace("cursor:pointer", "cursor:default;", $indicator)."</div>";
               }
               else {
                  $use_class = (strtotime($p_date["actual"]) > 0) ? $class = "process_green" : "process_gray";
                  $indicator .= "<div class=\"".$use_class."\">&nbsp;</div>";
               }
               // ***************
               // ENDE: INDICATOR
               // ***************
               $dsp_locked = (str_replace("&nbsp;", "", $p_date["actual"]) == "") ? "" : "<img src=\"../../../../library/images/icons/ico_data_locked.png\" style=\"margin-top:2px;\" />";
               $dsp_loop = ($loop > 1 && $i > 0) ? "&nbsp;[loop ".($i)."]" : ""; 

               if(in_array($val, $_application["process"]["type"][$tc_data["pot"]["meta"]["process_type"]]) && (in_array($phase, explode(":", $_application["process"]["display"][$val])) || $tc_data["tracker"]["date_filter"] == "0")) {
                  $rev = (str_replace("&nbsp;", "", $p_date["revision"]) != "") ? "&nbsp;&nbsp;[<a href=\"javascript:load_date_pot_tracker('".base64_encode($pot_date[$tc_data["pot"]["unique"]][$val][$i]["unid"])."', '".base64_encode($val."_".$i)."', '".base64_encode($tc_data["pot"]["unique"])."', '".base64_encode($tc_data["tracker"]["unique"])."', '".base64_encode($_application["preferences"]["settings"]["date_format"])."');\" style=\"font:normal 12px century gothic;\" name=\"dsp_date_pot_tracker\" val=\"".$val."_".$i."\">".$p_date["revision"]."</a>]<div id=\"date_".strtolower($val."_".$i)."\" style=\"position:absolute;display:none;\">&nbsp;</div></a>" : "";
                  if($i < $loop - 1) $rev = "&nbsp;&nbsp;[".$p_date["revision"]."]";
                  $dsp_process .= 
                  "<tr>".
                  "<td style=\"padding-top:2px;vertical-align:top;text-align:center;\">".$indicator.$dsp_locked.$delta."</td>".
                  "<td style=\"vertical-align:top;padding-left:4px;".$style."\" ><div style=\"cursor:url(../../../../library/images/icons/help.cur),pointer;\" onclick=\"javascript:load_help('".$_application["preferred_language"]."-indicator-".$val."', 'small');\">".substr($_application["process"]["subprocess"][$val], 0,strpos($_application["process"]["subprocess"][$val], ":")).$dsp_loop."</div></td><td style=\"text-align:center;\">P/R<br />A</td><td>".$t_date["revised"]."<br />".str_replace("undefined", "&nbsp;", $t_date["actual"])."</td><td>".$p_date["revised"].$rev."<br />".str_replace("undefined", "&nbsp;", $p_date["actual"]).
                  "<input type=\"hidden\" name=\"v_".$val."_actual\" value=\"".$pot_date[$tc_data["pot"]["unique"]][$val][$i]["actual_value"]."\" />".
                  "<input type=\"hidden\" name=\"i_".$val."_actual\" value=\"".$pot_date[$tc_data["pot"]["unique"]][$val][$i]["unid"]."\" />".
                  "</td></tr>\r\n";
                  $dsp_process = str_replace("Array", "", $dsp_process); 
                  if(substr($val, -2) == "99") $dsp_process .= "<td style=\"border-top:solid 2px rgb(255,255,255);border-bottom:solid 2px rgb(255,255,255);\" colspan=\"5\"><img src=\"../../../../library/images/te_orange.png\" style=\"width:100%;height:2px;\" /></td></tr>";
                  else $dsp_process .= "<td style=\"margin:0px;padding:0px;border-bottom:solid 2px;\" colspan=\"5\"><img src=\"../../../../library/images/blank.gif\" style=\"width:100%;height:2px;\" /></td></tr>";
               }
               else {
                  if(!strtolower($tc_data["pot"]["meta"]["process_type"]) == "not selected" && !strtolower($tc_data["pot"]["meta"]["process_type"]) == "not selected") $dsp_process .=  "<tr><td style=\"border-bottom:solid 2px rgb(255,255,255);\">&nbsp;</td><td colspan=\"4\" style=\"border-bottom:solid 2px rgb(255,255,255);color:rgb(99,99,99);\">".substr($_application["process"]["subprocess"][$val], 0,strpos($_application["process"]["subprocess"][$val], ":"))." excluded by rule.</td></tr>";
               }

            }

         }
         if($dsp_block > 1) $a_key_count = $a_key_count + $dsp_block -1;
      }

      $dsp_process .= "</table></td></tr>";

      $dsp_process .=
      "</table>\r\n<div style=\"display:none;\" id=\"reason_code\" p_id=\"\" unid=\"\" new_revision=\"\"></div>";

      $dsploop = ($_REQUEST["loop"] == "") ? ($maxloop - 1) : intval($_REQUEST["loop"]);

      for($w = 1; $w <=2; $w++) {
         $file = document_check($_SESSION["remote_domino_path_epcmain"], "v.get_creport_by_pot_and_loop", $_REQUEST["pot"]."@".$dsploop."@".($w));
         if(strlen($file) <= 1) {
            $file = "<cr><loop>".$maxloop."</loop></cr>";
         }
         $tc_data["cr"][$w - 1] = process_xml_from_string($file);
         $tc_data["cr"][$w - 1]["load"] = generate_xml($_SESSION["remote_domino_path_epcmain"]."/v.get_creport_by_pot?open&count=99999&restricttocategory=".$_REQUEST["pot"]."@".($w)."&function=xml:data");
      }


      include("modules/Elements/m_b1.tracker/pot.php");	
      include("modules/Elements/m_b1.tracker/tool_part.php");

      $form .= 
      "<tr><td colspan=\"".($cspan+1)."\">".
      "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"tbl_pot\" style=\"width:700px;\">".
      "<tr>".
      "<td style=\"vertical-align:top;border-right:solid 2px rgb(255,255,255);padding:0px;\">".$dsp_process."</td>". 
      "<td style=\"vertical-align:top;border-right:solid 2px rgb(255,255,255);padding:0px;\">".$dsp_pot_part_tool."</td>".
      "</tr>".
      "</table>".
      "</td></tr>";				
    
   }


   $form .= "</table>".
   "<p><form name=\"recalculate\" method=\"post\" style=\"display:none;\">".
   "<input name=\"phase\" value=\"\" />".
   "<input name=\"loop\" value=\"\" />".
   "<input name=\"data_type\" value=\"\" />".
   "<input name=\"use_rule\" value=\"X\">".
   "<input name=\"value\" value=\"\" />".
   "</form></p>\r\n".
   $f_do;   
   // ******************
   // ENDE: FORM CONTENT
   // ******************


   // *********************
   // START: TREE einbinden
   // *********************
   if($tab == "3") {
      require_once("dd/part_tree.php");
      $form .= generate_tree_module($_application, $pot_date);
   }
   // ********************
   // ENDE: TREE einbinden
   // ********************



   $module=
   "<p id=\"selected_tab\" style=\"display:none;\">".$_REQUEST["m_b1_tab"]."</p>\r\n".
   "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"domino_form\" style=\"width:100%;visibility:visible;\">\r\n";
   if($_REQUEST["flatform"] != "true") {
      $module.=
      "   <tr>\r\n".
      "      <td>".$form."</td>\r\n".
      "   </tr>\r\n";
   }
   else $module.="<td colspan=\"2\">".$form."</td>\r\n";
   $module.="</table>\r\n";
   $module = str_replace("%%REL%%", $rel, str_replace("%%TABS%%", $tabs, $module));

   return $module;
}

?>



   