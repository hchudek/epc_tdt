<?php


function m_d1__cr_v1($_application) {

   $load = array("part", "pot", "tool", "meta", "tracker", "project");
   require_once("addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);

 // print_r($tc_data);
   // Module Headline ----------------------------------------------------------------------------------------------
   $headline = rawurldecode($_application["module"]["description"][str_replace("__", ".", __FUNCTION__)][0]);
   if($tc_data["tool"]["is_rms"] == "1") $headline = str_replace("(Eval. Report)", "(RMS)", $headline);

   // Module Body --------------------------------------------------------------------------------------------------
   $g = trim(file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.dates_g3?open&restricttocategory=".$_REQUEST["pot"]."&count=1&function=plain"));

   $g3 = (strpos($g, "No documents found") || $g == "") ? "0" : "1";
   $ts = rawurldecode($tc_data["tool"]["supplier"]);
   $ps = rawurldecode($tc_data["part"]["supplier"]);
 
   if(!strpos($ps, "[")) $ps = "";

   if(strpos($ts, "[")) $ts = trim(substr($ts, 0, strpos($ts, "[")));
   $pl = rawurldecode($tc_data["tool"]["production_location"]);
   if(strpos($pl, "[")) $pl = trim(substr($pl, 0, strpos($pl, "[")));

   $is_pilot = (in_array($tc_data["meta"]["process_type"], array("Pilot"))) ? "1" : "0";

   if(substr($tc_data["tool"]["number"], 0, 1) == "2") $check["resin_primary"] = $tc_data["tool"]["resin_primary"];
   if(substr($tc_data["tool"]["number"], 0, 1) == "2" && $tc_data["tool"]["dualsource"] == "1" ) $check["resin_secondary"] = $tc_data["tool"]["resin_secondary"];

   $module = "<div tool_unid=\"".$tc_data["tool"]["unid"]."\" is_pilot=\"".$is_pilot."\" transferred_location=\"".$tc_data["tool"]["transferred_location"]."\" archived=\"".str_replace("No", "", $tc_data["tracker"]["archived"])."\" pemgr=\"".rawurldecode($tc_data["meta"]["pemgr_name"])."\" sq=\"".rawurldecode($tc_data["meta"]["sq_name"])."\" tpm=\"".$tc_data["tracker"]["responsible"]."\" check=\"".str_replace("\"", "'", str_replace("[]", "''", json_encode($check)))."\" process_type=\"".strtoupper($tc_data["meta"]["process_type"])."\" part_supplier=\"".rawurlencode($ps)."\" tool_supplier=\"".$ts."\" production_location=\"".$pl."\" g3_actual=\"".$g3."\" dualsource=\"".trim($tc_data["tool"]["dualsource"])."\" is_rms=\"".$tc_data["tool"]["is_rms"]."\" form=\"".__FUNCTION__."\" is_proposal=\"".$tc_data["pot"]["is_proposal"]."\" pot_unid=\"".$tc_data["pot"]["unid"]."\" pot=\"".$_REQUEST["pot"]."\" tracker=\"".$_REQUEST["unique"]."\"></div>\r\n";
   $_SESSION["request:pot"] = $_REQUEST["pot"];
   $_SESSION["request:tracker"] = $_REQUEST["unique"];
   $_SESSION["request:part"] = $tc_data["part"]["number"];
   $_SESSION["request:rev"] = $tc_data["part"]["rev"];
   return array($headline, str_replace("Array", "", $module));

}



?>
