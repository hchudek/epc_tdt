function select_supplier(unid, type) {
   if(type == "procurement") {
      if(document.getElementsByName("edit_procurement_data")[0].innerText == "0") return false;
   }  
   url = session.remote_database_path + "/addin/data_picker.php?use_sortkey=2";
   document.getElementsByName("ifrm")[0].src = url;
   document.getElementsByName("ifrm")[0].style.visibility = "visible";
   document.getElementsByName("ifrm")[0].unid = unid;
}


function set_supplier(e) {
   document.getElementById("dsp_supplier").innerText = unescape(e);
   document.getElementsByName("ifrm")[0].style.visibility = "hidden";
   unid = document.getElementsByName("ifrm")[0].unid;
   handle_save_single_field_extdb(session.remote_domino_path_base_tool, unid, "supplier", e, "handle_button(\\'send\\')");
}


function save_selected(e, unid, value, type) {
   if(type == "procurement") {
      if(document.getElementsByName("edit_procurement_data")[0].innerText == "0") return false;
   }
   d = document.getElementsByName(e.name);
   for(i = 0; i < d.length; i++) {
      d[i].src = "../../../../library/images/16x16/status-5-1.png";
   }
   e.src = "../../../../library/images/16x16/status-5.png";
   handle_save_single_field_extdb(session.remote_domino_path_base_tool, unid, e.name, value, "handle_button(\\'send\\')");
}


function handle_button(button) {
   c = (document.getElementsByName("handle_button_send")[0].innerText).split(",");
   check = false;
   for(i = 0; i < c.length; i++) if((document.getElementsByName(c[i])[0].src).indexOf("status-5.png") > 0) check = true;
   for(i = 0; i < c.length; i++) {
      if((document.getElementsByName(c[i])[0].src).indexOf("status-5-1.png") > 0 && (document.getElementsByName(c[i])[1].src).indexOf("status-5-1.png") > 0) check = false;
   }
   if(document.getElementById("dsp_supplier").innerText == "") check = false;
   document.getElementById("button_" + button).style.visibility = (check) ? "visible" : "hidden";
}


function change_status(unid, status) {
   handle_save_single_field_extdb(session.remote_domino_path_base_tool, unid, "wf_status", status, "location.reload();");
}


function save_select(e, unid) {
   handle_save_single_field_extdb(session.remote_domino_path_base_tool, unid, e.name, e[e.selectedIndex].value, "");
}

function save_change_status(e, unid, status) {
   if(e.selectedIndex == 0) return false;
   js = "change_status(\\'" + unid + "\\',\\'" + status + "\\')";
   handle_save_single_field_extdb(session.remote_domino_path_base_tool, unid, e.name, e[e.selectedIndex].value, js);
}


function check_finished() {
   r = true;
   if(document.getElementsByName("scan_location")[0].src.indexOf("status-5.png") > 0) {
      if(document.getElementsByName("selected_scan_location")[0].selectedIndex == 0) r = false;
   }
   if(document.getElementsByName("digital_report")[0].src.indexOf("status-5.png") > 0) {
      if(document.getElementsByName("selected_digital_report")[0].selectedIndex == 0) r = false;
   }
   if(document.getElementsByName("tabulated_report")[0].src.indexOf("status-5.png") > 0) {
      if(document.getElementsByName("selected_tabulated_report")[0].selectedIndex == 0) r = false;
  }
  if(document.getElementsByName("wf_status")[0].innerText == "90") r = false;
  document.getElementById("button_finished").style.visibility = (r) ? "visible" : "hidden";
}


function dsp_select(dsp) {
   document.getElementsByName(dsp)[0].style.display = "block";
}