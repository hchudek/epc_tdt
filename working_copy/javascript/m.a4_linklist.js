function init_linklist() {
   p = document.getElementById(arguments[0]);
   if(p) {
      d = p.getElementsByTagName("a");
      if(d.length > 0) {
        $(document).ready(function() {
            $(p).sortable({   
               placeholder: "ui-sortable-placeholder"   
            });
         });
         for(i = 0; i < d.length; i++) {
            d[i].setAttribute("id", arguments[0] + "_" + String(i + 1));
            d[i].innerHTML = "<img style=\"width:16px;\" /><span>" + d[i].textContent + "</span><br />";
         }
         do_enable_linklist(d, true);
         do_change_html_tag(p, "a", "div");
      }
      else {
         p.textContent = "No bookmarks found.";
      }
   }
} 

function do_change_html_tag(p, tf, tt) {
   d = document.getElementById("linklist");
   html = (d.innerHTML).replace(new RegExp("<" + tf, "g"), "<" + tt).replace(new RegExp("</" + tf, "g"), "</" + tt);
   d.innerHTML = (html);

}

function save_linklist() {
   d = document.getElementById(arguments[0]).getElementsByTagName("div");
   oput = "linklist=";
   for(i = 0; i < d.length; i++) {
      if(typeof d[i].getAttribute("is_deleted") != "string") {
         oput += escape("<a href=\"" + d[i].getAttribute("href") + "\">" + d[i].textContent + "</a>");
      }
   }
   url = "addin/edit_linklist.php";
   js = "location.href=location.href";
   if(oput.length < 10) oput += "kill";
   run_ajax("run_ajax", url, js, oput);
}

function delete_from_list(e) {
   with(e.parentNode) {
      if(typeof getAttribute("is_deleted") == "string") {
         removeAttribute("style");
         removeAttribute("is_deleted");
      }
      else {
         setAttribute("style", "text-decoration:line-through;opacity:0.6;");
         setAttribute("is_deleted", "true");
      }
   }
   do_enable_linklist(e.parentNode.parentNode.getElementsByTagName(e.parentNode.tagName), false);
}

function do_enable_linklist(d) {
   tpe = (typeof arguments[1] == "undefined") ? JSON.parse(d[0].parentNode.getAttribute("mode")) : arguments[1];
   if(document.getElementById("edit_linklist")) {
      document.getElementById("edit_linklist").firstChild.textContent = (tpe) ? "Edit linklist" : "Freeze edit";
      document.getElementById("edit_linklist").style.display = "block";
   }
   if(!tpe) document.getElementById('save_linklist').style.display = "block";
   attr = new Array;
   img = new Array;
   if(tpe) {
      attr["onclick"] = "location.href=this.getAttribute('href')";
      attr["class"] = "link";
      attr["onmouseover"] = "this.setAttribute('style', 'text-decoration:underline;cursor:pointer;')";
      attr["onmouseout"] = "this.setAttribute('style', 'text-decoration:none;cursor:default;')";
      attr["ondblclick"] = "";
      attr["onmousedown"] = "";
      attr["onmouseup"] = "";
      img["src"] = session.php_server + "/library/images/16x16/edition-50.png";
      img["style"] = "cursor:pointer;vertical-align:top;margin-right:4px;";
      img["onclick"] = "";
      $(d[0].parentNode).sortable("disable");
   }
   else {
      attr["onclick"] = "";
      attr["class"] = "";
      attr["onmouseover"] = "";
      attr["onmouseout"] = "";
      attr["onmousedown"] = "if(typeof this.getAttribute('is_deleted') == 'string') $(this.parentNode).sortable('disable');";
      attr["onmouseup"] = "$(this.parentNode).sortable('enable');";
      attr["ondblclick"] = "if(typeof this.getAttribute('is_deleted') != 'string') {edit_field('Edit bookmark', this.firstChild.nextSibling.textContent, 55, 'document.getElementById(\\'' + this.getAttribute('id') + '\\').firstChild.nextSibling.textContent = v; unset_edit_field();');}";

      img["src"] = session.php_server + "/library/images/16x16/edition-43.png";
      img["style"] = "cursor:pointer;vertical-align:top;margin-right:4px;";
      img["onclick"] = "delete_from_list(this);";
      $(d[0].parentNode).sortable("enable");
   }
   mode = (tpe) ? false : true;
   for(i = 0; i < d.length; i++) {
      cursor = (tpe) ? "pointer" : "move";
      for(key in img) {
         if(img[key] == "") d[i].firstChild.removeAttribute(key);
         else d[i].firstChild.setAttribute(key, img[key]);
      }
      if(d[i].getAttribute("is_deleted") != "true") {
         for(key in attr) {
            if(attr[key] == "") d[i].removeAttribute(key);
            else d[i].setAttribute(key, attr[key]);
         }
      }
      else {
         cursor = "default";
      }
      d[i].firstChild.nextSibling.setAttribute("style", "cursor:" + cursor + ";");
   }
   d[0].parentNode.setAttribute("mode", mode);
}