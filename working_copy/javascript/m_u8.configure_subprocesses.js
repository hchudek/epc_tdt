m_u8__configure_subprocesses = {
   
   save: {
      rc: function() {
         arr = [];
         v1 = (arguments[1][0].parentNode.innerHTML.substr(0, arguments[1][0].parentNode.innerHTML.indexOf("<"))).trim();
         if(v1 != "") arr.push(v1);
         for(k in arguments[1]) if(!isNaN(k)) {
            if(arguments[1][k].textContent.trim() != "") arr.push(arguments[1][k].textContent.trim());
         }
         if(m_u8__configure_subprocesses["_" + arguments[0]].reason_codes != arr.join(";")) {
            m_u8__configure_subprocesses["_" + arguments[0]].reason_codes = arr.join(";");
            data = "&unid=" + arguments[0] + "&fld1=" + escape("reason_codes" + "/" + arr.join(";")) + "&split=" + escape(";");
            post.save_data("f.save_data", "", data, "");
         }
      },

      fld: function(unid, f, v) {
         if(m_u8__configure_subprocesses["_" + arguments[0]][f] != v) {
            cd = document.getElementById(unid + ":" + f);
            if(cd) cd.textContent = v;
            m_u8__configure_subprocesses["_" + arguments[0]][f] = v;
            data = "&unid=" + arguments[0] + "&fld1=" + escape(f + "/" + v);
            post.save_data("f.save_data", "", data, "m_u8__configure_subprocesses.activate(true)");     
         }
      }
   },

   activate: function() {
      if(typeof arguments[1] == "boolean") {
         do_fade(true);
         url = session.remote_domino_path_main + "/a.export_process?open";
         ifrm = document.querySelector("[id='tbl_m_u8__configure_subprocesses_save']");
         ifrm.src = url;
         ifrm.onload = function() {
            location.reload();
         }
      } 
      d = document.querySelector("[id='tbl_m_u8__configure_subprocesses_activate']");
      d.style.visibility = (arguments[0]) ? "visible" : "hidden";
   },

   search: function() {
      d = document.getElementById("tbl_m_u8__subprocesses").getElementsByTagName("tbody");
      v = (arguments[0].value.toLowerCase().trim()).split(" ");
      for(k in d) if(!isNaN(k)) {
         dsp = true;
         key = unescape(d[k].getAttribute("key")).toLowerCase();
         for(i = 0; i < v.length; i++) {
            if(key.indexOf(v[i]) == -1) {
               dsp = false;
               break;
            }
         }
         d[k].style.display = (dsp) ? "table-row-group" : "none";
      }
   },

   edit: function(unid) {

      data = m_u8__configure_subprocesses["_" + unid];

      form = document.getElementById("tbl_m_u8__configure_subprocesses_form");
      form.innerHTML = "";

      div = document.createElement("div");
      div.setAttribute("class", "headline");
      div.textContent = "Edit " + data.id + ": " + data.name;
      form.appendChild(div);

      table = document.createElement("table");
      table.setAttribute("border", "0");
      table.setAttribute("cellpadding", "0");
      table.setAttribute("cellspacing", "2");

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.setAttribute("style", "vertical-align:top;");
      td.setAttribute("oncontextmenu", "protocol.embed('" + unid + "', 'id', ''); return false;");
      td.innerHTML = "<history>Subprocess ID:</history>";
      tr.appendChild(td);

      td = document.createElement("td");
      td.innerHTML = "<input type=\"text\" value=\"" + data.id + "\" maxlength=\"8\" onblur=\"m_u8__configure_subprocesses.save.fld('" + unid + "', 'id', this.value.trim());\">";
      tr.appendChild(td);
      table.appendChild(tr);

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.setAttribute("style", "vertical-align:top;");
      td.setAttribute("oncontextmenu", "protocol.embed('" + unid + "', 'name', ''); return false;");
      td.innerHTML = "<history>Subprocess name:</history>";
      tr.appendChild(td);

      td = document.createElement("td");
      td.innerHTML = "<input type=\"text\" value=\"" + data.name + "\" maxlength=\"80\" onblur=\"m_u8__configure_subprocesses.save.fld('" + unid + "', 'name', this.value.trim());\">";
      tr.appendChild(td);
      table.appendChild(tr);

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.setAttribute("style", "vertical-align:top;");
      td.setAttribute("oncontextmenu", "protocol.embed('" + unid + "', 'deactivatable', ''); return false;");
      td.innerHTML = "<history>Is deactivatable:</history>";
      tr.appendChild(td);

      td = document.createElement("td");
      s1 = (data.deactivatable.toLowerCase() == "yes") ? " selected" : "";
      s2 = (data.deactivatable.toLowerCase() == "no") ? " selected" : "";
      td.innerHTML = 
      "<select onchange=\"m_u8__configure_subprocesses.save.fld('" + unid + "', 'deactivatable', this[this.selectedIndex].text.trim());\">" + 
      "<option" + s1 + ">Yes</option>" + 
      "<option" + s2 + ">No</option>" + 
      "</select>";
      tr.appendChild(td);
      table.appendChild(tr);

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.setAttribute("style", "vertical-align:top;");
      td.setAttribute("oncontextmenu", "protocol.embed('" + unid + "', 'reason_codes', ''); return false;");
      td.innerHTML = "<history>Reason code:</history>";
      tr.appendChild(td);

      td = document.createElement("td");
      p = document.createElement("p");
      p.setAttribute("contenteditable", "true");
      p.setAttribute("onblur", "m_u8__configure_subprocesses.save.rc('" + unid + "', this.getElementsByTagName('div'))");

      rc = (data.reason_codes).split(";");
      for(k in rc) if(!isNaN(k)) {
         div = document.createElement("div");
         div.textContent = rc[k];
         p.appendChild(div);
      }

      td.appendChild(p);
      tr.appendChild(td);
      table.appendChild(tr);
      form.appendChild(table);


   },

<?php
session_start();
require_once("../../../../library/tools/addin_xml.php");						
$file = process_xml($_SESSION["remote_domino_path_main"]."/v.subprocesses?open&count=99999&function=xml:data");
foreach($file["subprocess"] as $subprocess) {
   print "   _".$subprocess["@attributes"]["unid"].": {\r\n";
   foreach($subprocess as $key => $val) {
      if($key != "@attributes") {
         if(is_array($val)) $val = "";
         print "      ".$key.": \"".rawurldecode($val)."\",\r\n";
      }
   }
   print "   },\r\n";
}
?>

}

