function init_tracker() {
   if(session.page_id == "tracker") {
      head = document.getElementsByTagName("head")[0];
      script = document.createElement("script");
      script.setAttribute("src", "modules/te_header.php?get_project_number");
      script.setAttribute("async", "async");
      script.setAttribute("onload", "d = document.getElementsByTagName('title')[0]; d.textContent = tdproject.number + ' | ' + d.textContent");
      head.appendChild(script);
   }
}

function init_application() {

   el = document.querySelectorAll("[anchor='img:head']");

   if(el) {
      for(k in el) if(typeof el[k] == "object") {
         el[k].onmouseover = function() {
            img = this.getElementsByTagName("img")[0];
            img.src = img.src.replace(/_b.gif/, ".gif");
         }
         el[k].onmouseout = function() {
            img = this.getElementsByTagName("img")[0];
            img.src = img.src.replace(/.gif/, "_b.gif");
         }
      }
   }

   if(typeof arguments[0] != "undefined") {
      window.setTimeout("location.href=?logout", arguments[0]);
   }

   if(document.getElementById("tbl_header_pot")) {
      t_width = (document.getElementById("tbl_header_pot").offsetWidth);
      document.getElementById("tbl_main_pot").style.width = String(t_width -10) + "px";
   }

   if(typeof session.keep_alive == "string") keep_alive(session.keep_alive);

   if(document.getElementById("tdt4grid")) {
      nest_grid();
   }

   $(document).scroll(function() { 
      header_buttons = document.getElementById("header_buttons");
      if(header_buttons) {
         s = $(document).scrollTop();
         if(s < 76) header_buttons.style.top = String(108 - s) + "px";
         else header_buttons.style.top = "34px";
      }
   });
   d = document.getElementsByTagName("li");
   for(i = 0; i < d.length; i++) {
	   try{
		   if(d[i].style.visibility == "hidden") d[i].style.visibility = "visible";
	   }catch(e){}
   }

   if(document.getElementById("add_usernavigation")) {
      include_add_usernavigation();
   }

}

ftsearch = {
   init: function() {
      script = document.createElement("script");
      script.src = "addin/ftsearch.php";
      script.onload = function() { 
         d = document.getElementById("ftsearch"); 
         d.style.visibility = "visible"; 
         d.value = "Search TD Tracking";
         d.onclick = function() {
            this.style.color = "rgba(0,0,0,0.92)";
            if(!this.hasAttribute("clear")) this.value = "";
            this.setAttribute("clear", "");
         } 
      }
      script.id = "ftsearch-jsonp";
      document.getElementsByTagName("head")[0].appendChild(script);
   },
   do: function() {
      ftsearch.input = arguments[0];
      ftsearch.res = document.getElementById("ftsearch-res");
      ftsearch.input.setAttribute("clock", new Date().getTime());
      window.setTimeout(function() { 
         d = Number(ftsearch.input.getAttribute("clock")) - new Date().getTime();
         if(d < -800) {
            ftsearch.res.innerHTML = "<table style=\"color:rgba(0,0,0,0.7);\"><tr><td></td></tr></table>";
            v = ((ftsearch.input.value.toLowerCase()).trim()).split(" ");
            res = [];
            ftsearch.res.style.display = "block";
            for(k in ftsearch.data) {
               found = true;
               hs = unescape(ftsearch.data[k].key);
               for(s in v) {
                  if(hs.indexOf(v[s]) == -1) { found = false; break; }
               }
               if(found) res.push(ftsearch.data[k]);
               if(res.length > 24) break;
            }    
            ftsearch.execute(res);       
         }
      }, 850);
   },
   execute: function(res) {
      if(res.length == 0) out = "<b style=\"position: relative; left: 13px;\">Nothing found.</b>";
      else {
         table = document.createElement("table");
         table.setAttribute("border", "0");
         table.setAttribute("cellpadding", "0");
         table.setAttribute("cellspacing", "0");
         table.setAttribute("id", "tbl-ftsearch");
         table.setAttribute("style", "width: " + String($("body").width() - 60) + "px");
         tr = document.createElement("tr");
         table.appendChild(tr);
         for(s in res[0]) {
            if(s != "key" && s != "unid") {
               th = document.createElement("th");
               tr.appendChild(th);
               th.textContent = unescape(s);
            }
         }
         for(k in res) {
            tr = document.createElement("tr");
            table.appendChild(tr);
            tr.setAttribute("onclick", "location.href = 'tracker.php?&unique=" + unescape(res[k].unid) + "';");
            for(s in res[k]) {
               if(s != "key" && s != "unid") {
                  td = document.createElement("td");
                  tr.appendChild(td);
                  td.textContent = unescape(res[k][s]);
               }
            }
         }
         out = table.outerHTML;
      }
      more = (res.length > 24) ? "<b style=\"position: relative; top: -18px; left: 13px;\">Probably more results. Refine your search.</b>" : "";
      document.getElementById("ftsearch-res").style.width = String($("body").width() - 40) + "px";
      document.getElementById("ftsearch-res").innerHTML = "<table style=\"width:100%; text-align:right; color:rgba(0,0,0,0.7);\"><tr><td style=\"cursor:pointer;\" onclick=\"ftsearch.res.style.display = 'none';\">Close search&nbsp;&nbsp;</td></tr></table>" + 
      "<span style=\"color:rgba(0,0,0,0.98); font:normal 13px open sans;\">" + out + more + "</span>";
   },
}

tooltipp = function(use_tag) {
   d = document.getElementsByTagName(use_tag);
   for(k in d) {
      with(d[k].parentNode) {
         setAttribute("onmouseover", "if(tipp.use) this.getElementsByTagName('" + use_tag + "')[0].style.display='block'");
         setAttribute("onmouseout", "this.getElementsByTagName('" + use_tag + "')[0].style.display='none'");
      }
   }
}

tipp = {use: true}

uroles = {
    nest: function() {
<?php 
// Keine Rollenpr�fung von TPM
session_start(); 
if($_SESSION["page_id"] == "tracker") {
   $load = array("tracker");
   require_once("../addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   if(strtolower($tc_data["tracker"]["responsible"]) == strtolower($_SESSION["domino_user_cn"])) {
      print "       tpm = true;\r\n";
   }
   else {
      print "       tpm = false;\r\n";
   }
}
?>
       session.userroles = session.remote_userroles.split(":");
       use_attr = (typeof arguments[2] == "undefined") ? "name" : arguments[2];
       a = document.createElement("a");
       a.innerHTML = arguments[1];
       d = (typeof arguments[1] == "string") ? a.querySelectorAll("[" + use_attr + "]") : document.getElementById(arguments[0]).querySelectorAll("[" + use_attr + "]");
       for(k in d) if(!isNaN(k) && d[k].hasAttribute(use_attr) && uroles[arguments[0]][d[k].getAttribute(use_attr)].join("") != "") {
          if(typeof uroles[arguments[0]][d[k].getAttribute(use_attr)] == "object") {
             ro = true;
             for(u in session.userroles) {
                if(session.userroles[u].trim() != "") {
                   if(in_array(session.userroles[u], uroles[arguments[0]][d[k].getAttribute(use_attr)])) {
                      ro = false;
                      break;
                   }
                }
             }
             if(ro && !tpm) {
                switch(d[k].tagName.toLowerCase()) {
                   case "input": attr = ["readonly", "readonly"]; break;
                   case "select": attr = ["disabled", "disbaled"]; break;
                   case "div": attr = ["contenteditable", "false"]; break;
                }
                d[k].setAttribute(attr[0], attr[1]);
             }
             else {
                if(typeof validate.table[d[k].getAttribute(use_attr)] != "undefined") {
                   if(validate.table[d[k].getAttribute(use_attr)].code.length > 0) {
                      validate.nest(d[k], d[k].getAttribute(use_attr));
                   }
                }
             }
          }
       }
       if(typeof arguments[1] == "string") document.getElementById(arguments[0]).getElementsByClassName("inner_box")[0].innerHTML = a.innerHTML;
    }
}

<?php 
$file = file_get_contents("jload/userroles.js");
print $file;
?>


// window.onerror = function(err, url, line) {
//   return true;
// }


init_view = function() {
   use_id = arguments[0];
   c = (typeof arguments[1] == "undefined") ? "" : arguments[1];
   $(function() {
      $(use_id).sortable({
         placeholder: "ui-state-highlight-sort"
      });
      $(use_id).sortable({
         cancel: ".ui-state-disabled-sort"
      });
      $(use_id).sortable({
         connectWith: ".connected_sortable" + c
      }).disableSelection();
   });
}



in_array = function() {
   r = false;
   for(arg in arguments[1]) {
      if(arguments[0] == arguments[1][arg]) {
         r = true;
         break;
      }
   }
   return r;
}

do_fade = function() {_(arguments);}
infobox = function() {_(arguments);}
do_element_search = function() {_(arguments);}
do_smartsearch = function() {_(arguments);}
do_smartcombo = function() {_(arguments);}
include_css = function() {_(arguments);}
<?php
$path_translated =  str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
print "create_new_tracker = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/")."\");}\r\n";
print "include_add_pot = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/include_add_pot/")."\");}\r\n";
print "include_add_usernavigation = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/")."\");}\r\n";
?>


indicate_save = function() {
   document.getElementById("indicate_save").firstChild.src = session.php_server + "/library/images/16x16/green/hardware-20.png";
   window.setTimeout('document.getElementById("indicate_save").firstChild.src = session.php_server + "/library/images/16x16/white/hardware-20.png"', 1000);
}


helper = {
   search:function(e, d) {
      v = e.value.toLowerCase().trim().split(" ");
      d = d.getElementsByTagName("tr");
      for(k in d) if(!isNaN(k)) {
         dsp = true;
         if(d[k].hasAttribute("searchkey")) {
            n = d[k].getAttribute("searchkey");
            for(s in v) if(n.indexOf(v[s]) == -1) {dsp = false; break;}
            d[k].style.display = (dsp) ? "table-row" : "none";
         }
      }
   },
}


protocol = {

   embed: function() {
      if(typeof arguments[0] == "object") {

         e = arguments[0];
         while(e.tagName.toLowerCase() != "table") e = e.parentNode;
         unid = e.getAttribute("unid");
         fld = arguments[0].getAttribute("field");
         msg = arguments[0].textContent;
      }
      else {
         unid = arguments[0];
         fld = arguments[1];
         msg = arguments[2];
      }
      protocol.translate = (typeof arguments[1] == "object") ? arguments[1] : [];
      url = "addin/run_ajax.php?" + escape(session.remote_domino_path_protocol + "/v.tdt.protocol?open&count=9999&restricttocategory=" + escape(unid + "-" + fld) + "&function=json:embed_history");
      js = "protocol.write('" + unid + "-" + fld + "', document.getElementById('run_ajax').innerHTML.trim(), '" + msg + "')";
      run_ajax("run_ajax", url, js);
   },

   embed_indicator: function() {
      e = arguments[0];
      while(e.tagName.toLowerCase() != "tbody") e = e.parentNode;
      unid = e.getAttribute("unid");
      fld = arguments[0].getAttribute("field");
      msg = unescape(arguments[1]);
      url = "addin/run_ajax.php?" + escape(session.remote_domino_path_main + "/v.all_by_form/" + unid + "?open&type=" + fld); 
      js = "protocol.write('" + unid + "-" + fld + "', document.getElementById('run_ajax').innerHTML.trim(), '" + msg + "', false)";
      run_ajax("run_ajax", url, js);
   },
   

   write: function() {

      if(arguments[1].indexOf("No documents found") != -1) {
         html = "No history archived.";
      }
      else {

         eval(arguments[1]);
         arr = ["Num", arguments[0].split("-")[1], "created_by", "created_date"];
         table = document.createElement("table");
         table.setAttribute("border", "0");
         table.setAttribute("cellpadding", "0");
         table.setAttribute("cellspacing", "2");
         table.setAttribute("class", "protocol");
         tr_head = document.createElement("tr");
         for(num in embed_history) {
            tr_body = document.createElement("tr");
            for(k in arr) {
               if(k == 0) {
                  if(num == 0) {
                     th = document.createElement("th");
                     th.textContent = arr[0];
                     tr_head.appendChild(th);
                  }
                  td = document.createElement("td");
                  td.textContent = String(embed_history.length - num);
                  tr_body.appendChild(td);
               }
               else {
                  if(num == 0) {
                     th = document.createElement("th");
                     th.textContent = (k == 0) ? arguments[2] : arr[k].replace(/_/g, " ").substr(0, 1).toUpperCase() + arr[k].replace(/_/g, " ").substr(1, arr[k].length);
                     tr_head.appendChild(th);
                  }
                  td = document.createElement("td");
                  if(k == 0) {
                     v = unescape(embed_history[num][arr[k]]);
                     for(t in protocol.translate) {
                        if(v == t) {
                           v = protocol.translate[t];
                           break;
                        }
                     }
                     td.textContent = v;
                  }
               
                  else td.textContent = unescape(embed_history[num][arr[k]]);
                  tr_body.appendChild(td);
               }
            }
            if(num == 0) table.appendChild(tr_head);
            table.appendChild(tr_body);
         }
         html = table.outerHTML;
      }
      arguments[3] = (typeof arguments[3] == "undefined") ? true : arguments[3];
      head = "<a href=\"javascript:protocol.refresh('" + arguments[0] + "', '" + arguments[2] + "');\">";
      if(arguments[3]) head += "<img src=\"../../../../library/images/16x16/white/transfers-74.png\" style=\"vertical-align:top; margin-right:4px;\">"; 
      head = head.replace(/&/g, '') + "</a>History";
      infobox(head, html, 710, 327);
   },

   refresh: function() {
      arg = (arguments[0]).split("-");
      document.getElementById("confirm_box").getElementsByTagName("div")[1].innerHTML = 
      "<p style=\"margin-top:16px; margin-left:10px;\">" +
      "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"margin-right:7px; vertical-align:top;\"><span style=\"position:relative; top:-1px;\">Processing ...</span>" +
      "</p>";
      url = "addin/run_ajax.php?" + escape(session.remote_domino_path_protocol + "/a.archive?open");
      js = "box=document.getElementById('confirm_box');box.parentNode.removeChild(box); protocol.embed('" + arg[0] + "', '" + arg[1] + "', '" + arguments[1] + "')";
      run_ajax("run_ajax", url, js);
   }
   
}


function msgbox(t1, t2) {
   do_fade(true);
   div = document.createElement("div");
   div.setAttribute("id", "confirm_box");
   document.getElementsByTagName("body")[0].appendChild(div);
   p_left = Math.round(window.innerWidth / 2) - Math.round(div.offsetWidth / 2);
   p_top = Math.round(window.innerHeight / 2) - Math.round(div.offsetHeight / 2);
   div.setAttribute("style", "left:" + p_left + "px;top:" + p_top + "px;");
   h = document.createElement("div");
   h.setAttribute("class", "header");
   h.textContent = t1;
   div.appendChild(h);
   b = document.createElement("div");
   b.innerHTML = "<div class=\"body\">" + t2 + "</div>" +
   "<div class=\"button\">" +  
   "<span class=\"phpbutton\"><a href=\"javascript:do_fade(false); confirm_box.parentNode.removeChild(confirm_box);\">OK</a></span>" +
   "</div>";
   div.appendChild(b);
} 


function edit_field(descr, txt, len, js) {
   do_fade(true);
   div = document.createElement("div");
   div.setAttribute("id", "confirm_box");
   document.getElementsByTagName("body")[0].appendChild(div);
   p_left = Math.round(window.innerWidth / 2) - Math.round(div.offsetWidth / 2);
   p_top = Math.round(window.innerHeight / 2) - Math.round(div.offsetHeight / 2);
   h = document.createElement("div");
   h.setAttribute("class", "header");
   h.textContent = descr;
   div.appendChild(h);
   b = document.createElement("div");
   dsp = (txt == "") ? "none" : "block";
   inner_html = "<form style=\"margin-top:16px;\" action=\"javascript:v = document.getElementsByName('edit_field')[0].value; if(v != '') {" + js + "}\">" +
   "<input type=\"text\" maxlength=\"" + len + "\"  name=\"edit_field\" value=\"" + txt.substr(0, len) + "\" onkeyup=\"dsp_ok_button = (this.value.length > 0) ? 'block' : 'none'; document.getElementById('ok_button').style.display = dsp_ok_button;\" style=\"margin-left:10px;width:377px;\" />" +
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;margin-top:15px;\"><tr>" + 
   "<td style=\"width:50%;border-top:solid 1px rgb(222,222,232);border-right:solid 1px rgb(222,222,232);\"><span id=\"cancel_button\" style=\"display:inline-block;\"><a href=\"javascript:void(0);\" onclick=\"unset_edit_field();\" style=\"text-align:center;display:inline-block;width:200px;padding-top:7px;height:25px;background:rgb(255,255,255);text-decoration:none;color:rgb(0,0,0);\" onmouseover=\"this.style.background='rgb(243,243,247)';\" onmouseout=\"this.style.background='rgb(255,255,255)';\">Cancel</a></span></td>" + 
   "<td style=\"width:50%;border-top:solid 1px rgb(222,222,232);\"><span id=\"ok_button\" style=\"display:" + dsp + ";\"><a href=\"javascript:void(0);\" onclick=\"e = this; while(e.tagName.toLowerCase() != 'form') e = e.parentNode; e.submit();\" style=\"text-align:center;display:inline-block;width:200px;padding-top:7px;height:25px;background:rgb(255,255,255);text-decoration:none;color:rgb(0,0,0);\" onmouseover=\"this.style.background='rgb(243,243,247)';\" onmouseout=\"this.style.background='rgb(255,255,255)';\">OK</a></span></td>" +
   "</tr></table>" + 
   "</form>";
   b.innerHTML = inner_html;
   div.appendChild(b);
   div.setAttribute("style", "left:" + p_left + "px;top:" + (-20 + p_top - (div.offsetHeight / 2)) + "px;");
   document.getElementsByName("edit_field")[0].focus();
}



function unset_edit_field() {
   d = document.getElementById("confirm_box");
   if(d) {
      d.parentNode.removeChild(d);
      do_fade(false);
   }
}

function do_allow_drop(e) {
   e.preventDefault();
}

function do_drag(e) {
   e.dataTransfer.setData("Text",e.target.id);

}


function save_perpage() {
   type = (typeof arguments[0] == "undefined") ? "" : arguments[0].toLowerCase();
   js = (typeof arguments[1] == "undefined") ? "" : arguments[1];
   if(js == true) js = "location.href=location.href";
   f = document.getElementsByName("perpage")[0];
   d = f.getElementsByTagName("input");
   r = [];
   for(i = 0; i < d.length; i++) {
      r.push(d[i].name + "=" + escape(d[i].value));
   }
   url = session.php_server + "/library/addin/handle_perpage.php?type=" + type.substr(0, 6) + "&app=4&page_id=" + document.getElementById("tdt4grid").getAttribute("page_id") + "&action=" + f.getAttribute("action");
   run_ajax("run_ajax", url, js, r.join("&"));
}


function enable_overflow_y(id) {
   d = document.getElementById(id).getElementsByTagName("div");
   d[0].setAttribute("class", "inner_box_overflow");
}

function fix_header(id) {
   if(document.getElementById("grid_enabled").getAttribute("grid_enabled") == "0") {
      $("#" + id + "_header").attr("w", $("#" + id + "_header")[0].offsetWidth);
      y = $(document).scrollTop();
      p = $("#" + id).offset();
      $("#" + id + "_header").attr("style", "height:26px;position:fixed;width:" + $("#" + id + "_header").attr("w") + "px;top:" + String(32 + p.top - y) + "px;background-color:rgb(255,255,255);opacity:0.9;z-index:99;");
      $(document).scroll(function() {
         y = $(this).scrollTop();
         p = $("#" + id).offset();
         $("#" + id + "_header").attr("style", "height:26px;position:fixed;width:" + $("#" + id + "_header").attr("w") + "px;top:" + String(32 + p.top - y) + "px;background-color:rgb(255,255,255);opacity:0.9;z-index:99;");
      });
   }
   else {
      d = document.getElementById("tbl_" + id.replace("_header", ""));
      d.setAttribute("style", "");
   }
}


function handle_bestpractise(e) {
   d = document.bestpractise;
   d.redirect.value = location.href;
   d.v.value = e.getAttribute("bp");
   d.submit();
}


function reset_page() {
   page_id = document.getElementById("tdt4grid").getAttribute("page_id");
   url = session.remote_domino_path_perportal + "/a.setfield?open&unid=" + session.remote_perportal_unid + "&field=td$grid_" + page_id + "&value=";
   js = "location.href=location.href;";
   run_ajax("run_ajax", url, js);
}



function keep_alive(r, t) {
   u = 600;
   i = (typeof t == "undefined") ? 0 : u;
   if(r > i) {
      url = session.php_server + "/library/tools/keep_alive.php?" + String(session.keep_alive  - r) + "/" + session.keep_alive + "/" + u;
      js = "window.setTimeout(\"t = document.getElementById('keep_alive').innerText; document.getElementsByName('img_plugged')[0].alt = t; keep_alive(" + (r - i) + ", t)\", " + (i * 1000) + ")";
      run_ajax("keep_alive", url, js);
   }
   else {
      document.getElementsByName('img_plugged')[0].alt = "Terminated";
   }
}

function reset_settings() {
   check = confirm("Are you sure?");
   if(check) {
      js = "location.href=location.href";
js = "alert(document.getElementById('run_ajax').innerHTML + ' settings removed')"
      url = session.remote_domino_path_perportal + "/a.kill_settings?open&app=" + session.remote_application_id + "&unid=" + session.remote_perportal_unid;
      run_ajax("run_ajax", url, js);
   }
}

function load_help(unique_id){
   rdp = session.remote_domino_path_main;
   t_url = rdp + "/v.help_lookup/" + escape(unique_id.toLowerCase()) + "?open";
   t_width = document.getElementsByTagName("body")[0].offsetWidth - 60;
   t_height = document.getElementsByTagName("body")[0].offsetHeight - 50;
   t_js = "document.getElementById('embedded_window_headline_content').innerText = document.getElementsByName('help_subject')[0].innerText;" +
  "document.getElementById('embedded_window_message').innerHTML = document.getElementsByName('help_body')[0].innerHTML.replace(/.com/g,'.com:9220');";

                
   if(typeof arguments[1] == "undefined") t_js += "handle_embedded_window(20, 40, " + t_width + ", " + t_height + ");";
   else {
      t_js += "handle_embedded_window(20, 40, " + t_width + ", 150);";
   }

   run_ajax("run_ajax", t_url, t_js);
}

function check_at_function() {
   if(document.getElementsByName("do_at_function")) do_at_function(0);
}

function do_at_function() {
   num = (typeof arguments[0] == "undefined") ? 0 : arguments[0];
   if(num == 0) {
      for(i = 0;i < document.getElementsByName("do_at_function").length; i++) {
         document.getElementsByName("do_at_function")[i].style.display = "none";
      }
   }

   if(document.getElementsByName("do_at_function")[num]) {
      at_function =  document.getElementsByName("do_at_function")[num].innerText.replace(/"/g, '\\"');
      url = document.getElementsByName("remote_domino_path")[0].innerText + "/a.n.evaluate_at_function?open=php&eval=" + at_function;
      include_res = "document.getElementsByName('do_at_function')[" + num + "].innerHTML = document.getElementById('include_at_function').innerHTML;"; 
      run_ajax("include_at_function", url, include_res + "document.getElementsByName('do_at_function')[" + num + "].style.display='inline'; do_at_function(num + 1)");
   }
}


function container() {
   for(i = 0; i < arguments.length; i++) {
      arg = arguments[i].replace("=> ", "=>");
      this[arg.substr(0, arg.indexOf("=>")).replace(/ /g,"")] = arg.substr(arg.indexOf("=>") + 2, arg.length);
   }
}


function handle_embedded_window(xpos,ypos,width,height) {
   s=document.getElementById("embedded_window").style;
   s.left=String(xpos);
   s.top=String(ypos);
   s.width=String(width)+"px";
   s.height=String(height)+"px";
   s.visibility="visible";
   document.getElementById("embedded_window_vertical_line").style.height=String(height-4)+"px";
   kill_app();
}



function fade_in_out(id) {
   dsp = (document.getElementById(id).style.display == "block") ? "none" : "block";
   document.getElementById(id).style.display = dsp;
}

function get_keystr() {
   return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";  
}

function base64_encode(input) {  
   keyStr = get_keystr();
   input = escape(input);  
   var output = "";  
   var chr1, chr2, chr3 = "";  
   var enc1, enc2, enc3, enc4 = "";  
   var i = 0;  
   do {  
      chr1 = input.charCodeAt(i++);  
      chr2 = input.charCodeAt(i++);  
      chr3 = input.charCodeAt(i++);  
      enc1 = chr1 >> 2;  
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);  
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);  
      enc4 = chr3 & 63;  
      if (isNaN(chr2)) enc3 = enc4 = 64;  
      else if (isNaN(chr3)) enc4 = 64;  
      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);  
      chr1 = chr2 = chr3 = "";  
      enc1 = enc2 = enc3 = enc4 = "";  

   } while (i < input.length);  
   return output;  
} 

function base64_decode(input) {  
   keyStr = get_keystr();
   var output = "";  
   var chr1, chr2, chr3 = "";  
   var enc1, enc2, enc3, enc4 = "";  
   var i = 0;  
   var base64test = /[^A-Za-z0-9\+\/\=]/g;  
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");  
   do {  
      enc1 = keyStr.indexOf(input.charAt(i++));  
      enc2 = keyStr.indexOf(input.charAt(i++));  
      enc3 = keyStr.indexOf(input.charAt(i++));  
      enc4 = keyStr.indexOf(input.charAt(i++));  
      chr1 = (enc1 << 2) | (enc2 >> 4);  
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);  
      chr3 = ((enc3 & 3) << 6) | enc4;  
      output = output + String.fromCharCode(chr1);  
      if (enc3 != 64) output = output + String.fromCharCode(chr2);  
      if (enc4 != 64) output = output + String.fromCharCode(chr3);  
      chr1 = chr2 = chr3 = "";  
      enc1 = enc2 = enc3 = enc4 = "";  
   } while (i < input.length);  
   return unescape(output);  
}  


function ajax_submit(f, url) {
   js = (typeof arguments[2] == "undefined") ? "" : arguments[2];
   d = document.getElementsByName(f)[0].getElementsByTagName("input");
   r = [];
   for(i = 0; i < d.length; i++) {
      r.push(d[i].name + "=" + escape(d[i].value));
   }
   run_ajax("run_ajax", url, js, r.join("&"));
}


run_ajax = function(_id,_file,_runJS) {
   _post = (typeof arguments[3] == "undefined") ? null : arguments[3];
   req = null;
   try { req = new XMLHttpRequest(); }
   catch (ms) {
      try { req = new ActiveXObject("Msxml2.XMLHTTP"); }
      finally {}
   }
   if (req == null) eval("document.getElementById(_id).innerHTML='error creating request object';");
   else req.open("post",_file, true);
   req.onreadystatechange = function() {
      switch(req.readyState) {
         case 4:
            document.getElementById(_id).innerHTML = req.responseText;
            if(_runJS != "undefined") eval(_runJS);
            break;
         default:
            return false; 
            break;
      }
   }

   req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
   req.send(_post);
}


function handle_fade_in_out(id) {
   if(typeof arguments[1]=="undefined" || arguments[1]=="display") {
      dsp=(document.getElementById(id).style.display=="none") ? "block" : "none";
      document.getElementById(id).style.display=dsp;
   }
   else {
      dsp=(document.getElementById(id).style.visibility=="hidden") ? "visible" : "hidden";
      document.getElementById(id).style.visibility=dsp;
   }
}

function save_query_string(page, location_search) {
   url = session.remote_domino_path_perportal + "/a.setfield?open&unid=" + session.remote_perportal_unid + "&field=td$" + page + "_link&value="+escape(location_search);
   js = "location.href = '" + location.href.substr(0, location.href.indexOf(".php") + 4) + location_search + "'";
   run_ajax("run_ajax", url, js);
}

function save_homepage() {
   url = session.remote_domino_path_perportal + "/a.setfield?open&unid=" + session.remote_perportal_unid + "&field=td$homepage&value="+escape(arguments[0]);
   js = "location.href='" + session.remote_domino_path_main + "/login?open'";
   run_ajax("run_ajax", url, js);
}

function do_trouble_mail() {
   body = escape("Please create a ServiceNow Ticket for Assignment group CAP-AP-EMEA-LOCAL-GERMANY-DOMINOAPPS " + String.fromCharCode(13, 10, 13, 10) + String.fromCharCode(13, 10, 13, 10)+ "Reported by - "+session.domino_user_cn + String.fromCharCode(13, 10, 13, 10)+"Reported from website - "+ location.href + String.fromCharCode(13, 10, 13, 10) +"Type of Ticket (Incident or Request) ? "+ String.fromCharCode(13, 10, 13, 10)+ "Who does this affect (A single person, Multiple people, Entire department / Organization) ? "+ String.fromCharCode(13, 10, 13, 10)+"What is the impact on the business (Critical business disruption, Limited business disruption, No business disruption) ? "+ String.fromCharCode(13, 10, 13, 10)+ "Preferred method of contact (Email, Skype, Phone) - "+ String.fromCharCode(13, 10, 13, 10)+"Briefly describe your issue"+ String.fromCharCode(13, 10, 13, 10)+"Anything else we should know? "+ String.fromCharCode(13, 10, 13, 10)+ String.fromCharCode(13, 10, 13, 10)+"This eMail was automatically generated.");
   location.href = "mailto:teconnectivity@service-now.com?cc=manjeera.reddyvari@te.com;klaus.kraemer@te.com;nikhil.bhagwat@te.com&subject=ServiceNow%20Ticket%20from%20" + session.domino_user_cn + "%20on%20Configuration%20Item%20"+session.remote_database_name+"&body=" + body;
}

function load_history(unid, id) {
   url = session.remote_domino_path_epcmain + "/v.show_history/" + unid + "?open";
   dsp = (document.getElementById(id + "_history").style.display == "none") ? "block" : "none";
   document.getElementById(id + "_history").style.zIndex = 99999;
   js = "document.getElementById('" + id + "_history').style.display = '" + dsp + "'";
   run_ajax(id + "_history", url, js);
}

function handle_area(e) {
   do_fade(true);
   p = session.php_server + "/library/images/16x16/";
   if(typeof arguments[1] == "undefined") e.firstChild.style.visibility = "hidden";
   e.firstChild.src = (e.firstChild.src.indexOf("status-5-1") > 0) ? p + "status-5.png" : p + "status-5-1.png";
   d = document.getElementsByName(e.getAttribute("name"));
   arr = [];
   for(i = 0; i < d.length; i++) {
      if(d[i].firstChild.src.indexOf("status-5-1") == -1) arr.push(d[i].getAttribute("key"));
   }
   db = session.remote_domino_path_perportal;
   unid = session.remote_perportal_unid;
   field = "td$area";
   postsave = "location.reload()";
   handle_save_single_field_extdb(db, unid, field, arr.join(":"), postsave)
}
