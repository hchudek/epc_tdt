<?php
$path_translated =  str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
print "include_m_b2__image = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/m_b2.image/")."\");}\r\n";
?>


m_b2__image = {
   upload: function() {
      e = arguments[0];
      while(e.tagName.toLowerCase() != "form") e = e.parentNode;
      e.setAttribute("method", "post");
      e.setAttribute("enctype", "multipart/form-data"); 
      size = arguments[0].files[0].size;
      name = arguments[0].files[0].name.toLowerCase();
      ext = name.substr(name.lastIndexOf(".")  + 1, name.length);
      err = [];
      if(size > 2097152) err.push("File size is too big (max. 2 mb)");
      if(!in_array(ext, ["gif", "jpg", "jpeg", "png"])) err.push("File is not a gif, jpg or png resource"); 
      if(err.length > 0) {
         m_b2__image.error(err);
      }
      else {
         do_fade(true);
         e.submit();      
      }
   },

   error: function(err) {
      html = "<div style=\"margin-bottom:10px;\">Upload not possible:</div>";
      for(k in err) {
         html += "<div>-&nbsp;" + err[k] + "</div>";
      }
      infobox("Error", html, 300, 120);
   },

   kill: function() {
      if(typeof arguments[1] == "undefined") {
         html = "<div style=\"margin-bottom:10px;\">Delete image?</div>" +
         "<span class=\"phpbutton\" style=\"margin-right:4px;\"><a href=\"javascript:void(0);\" onclick=\"m_b2__image.kill('"+ arguments[0] + "', true);\">OK</a></span>" +      
         "<span class=\"phpbutton\" style=\"margin-right:4px;\"><a href=\"javascript:void(0);\" onclick=\"box=document.getElementById('confirm_box'); box.parentNode.removeChild(box); do_fade(false);\">Cancel</a></span>";
         infobox("Delete image", html, 126, 104);
      }
      else {
         form = document.createElement("form");
         form.setAttribute("action", "addin/image_delete.php");
         form.setAttribute("method", "post");
         input = document.createElement("input");
         input.setAttribute("name", "filename");
         input.setAttribute("value", arguments[0]);
         form.appendChild(input);
         input = document.createElement("input");
         input.setAttribute("name", "redirect");
         input.setAttribute("value", location.href);
         form.appendChild(input);
         form.submit();
      }
   },

 
}

