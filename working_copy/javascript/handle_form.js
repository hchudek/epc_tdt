function handle_section() {
   dsp = (document.getElementById(arguments[0]).style.display != "none") ? "none" : "table-row-group";
   img = (dsp == "table-row-group") ? "minus.jpg" : "plus.jpg"; 
   document.getElementById(arguments[0]).style.display = dsp;
   document.getElementById(arguments[0] + "_img").src = session.php_server + "/library/images/" + img;
}


function include_calendar() {
   url = session.php_server + "/apps/epc/main/addin/calendar2.php?&p_use_rule=" + arguments[0] + "&p_data_type=&p_id=" + arguments[0]+"_calendar";
   run_ajax(arguments[0] + "_calendar", url, "document.getElementById('" + arguments[0] + "_calendar').style.dispay='block'");
}
 

function check_pnum() {
   val = (document.getElementsByName("p_num")[0].value).toLowerCase();
   if(document.getElementById("prj_" + val)) {
      url = session.php_server + "/library/images/16x16/status-11.png";
      cursor = "pointer";
      onclick = "create_dummy_project('" + document.getElementById("prj_" + val).innerHTML + "')";
   }
   else {
      url = session.php_server + "/library/images/16x16/status-11-1.png";
      cursor = "default";
      onclick = "void(0)";
   }
   document.getElementsByName("p_num_img")[0].src = url;
   document.getElementsByName("p_num_img")[0].style.cursor = cursor;
   document.getElementsByName("p_num_img")[0].onclick = new Function(onclick);
}

function create_dummy_project(unid) {
   url = (session.remote_domino_path_leanpd + "/f.w.eng_project?open&ref_number=" + document.getElementsByName("p_num")[0].value + "&ref="  + unid + "&EXTERNAL_PRJ_MGE_SYSTEM_PRJ_ID=" + document.getElementById("p_num_new").innerHTML);
   document.getElementsByName("create_new_project")[0].src = url;
   document.getElementsByName("create_new_project")[0].style.height = "180px";
   window.setTimeout('document.getElementsByName("create_new_project")[0].style.display = "block"', 1000);
}

function create_new_tracker() {

   document.getElementById("t_body").style.visibility = "hidden";
   document.getElementsByTagName("body")[0].style.visibility = "visible";
   d = document.getElementsByTagName("body")[0].firstChild;
   d.style.display = "block";
   d.style.zIndex = "99999";
   d.style.position = "absolute";
   d.style.top = "210px";
   d.style.left = "23px";
   d.style.width = "100%";
   d.backgroundColor = "rgb(255,255,255)";
   d.innerHTML ="<span style='font:normal 13px century gothic,verdana;position:relative;top:3px;'>&nbsp;&nbsp;Creating tracker... Please wait...</span>";
   kill_app(true);
   save = new container(
   "form => f.tracker", 
   "created_by_user => " + session.domino_user,
   "responsible => " + session.domino_user.substring(3,session.domino_user.indexOf("OU")-1),
   "document_number => %%CREATE%%",
   "tracker_active => 1",
   "doc$status => 1",
   "project_descr => " + document.getElementById("project_descr").innerText,
   "project_number => " + document.getElementById("project_number").innerText);
   create_new_document("create_project_dates", document.getElementById("project_number").innerText + "&user=" + session.domino_user);
}



function save_transferred_location(e, unid, p_location, p_date) {

   vendor = (typeof e == "object") ? p_location.substr(0, p_location.indexOf(":")) : e;
   save_location = "t_location";
   if(vendor.toLowerCase() == "select outside vendor") {
      url = session.remote_database_path + "/addin/data_picker.php?use_sortkey=2";
      document.getElementsByName("ifrm")[0].src = url;
      document.getElementsByName("ifrm")[0].style.visibility = "visible";
      document.getElementsByName("ifrm")[0].unid = unid;
      document.getElementsByName("ifrm")[0].p_location = p_location;
      document.getElementsByName("ifrm")[0].p_date = p_date;
      for(i = 1; i <= 8; i++) {
         eval("document.getElementsByName('ifrm')[0].p_c" + String(i) + " = p_c" + String(i));
      }
      return false;
   }
   else {
      handle_save_single_field(unid, save_location, vendor, "location.href=location.href");
   }
}


post = {
   save_data: function(form, db, data, js) {
      if(db == "") db = session.remote_domino_path_main;
      url = session.remote_database_path + "addin/save_multiple_fields.php?form=" + escape(form) + "&db=" + escape(db);
      run_ajax("run_ajax", url, js, data);  
   },

   save_field: function(db, unid, fld, val) {
      split = (typeof arguments[5] == "undefined") ? "" : arguments[5];
      data = "unid=" + unid + "&fld1=" + fld + "/" + escape(val) + "&split=" + escape(split);
      js = (typeof arguments[4] == "string") ? arguments[4] : "indicate_save()";
      post.save_data("f.save_data", db, data, js);
   }
}


function handle_save_single_field(unid, field, value, postsave) {
   t = (typeof arguments[4] == "undefined") ? true : arguments[4];
   if(t) {
      data = 
      save = new container(field + " => " + value);
      save_data(unid, postsave, 0);
   }
}


function save_data(unid, postsave, num) {
   p = new Array;
   for(property in save) p.push(property);  
   if(p.length > num) {
      url = session.remote_domino_path_main + "/a.save_data?open&unid=" + unid + "&f=" + escape(p[num]) + "&v=" + escape(save[p[num]]);
      js = "save_data('" + unid + "', '" + postsave + "', " + (num + 1) + ")";
      run_ajax("run_ajax", url, js);
   }
   else eval(unescape(postsave));
}

function reload_save_vd(e) {
   url = "modules/m_e2.ee_edit_pot.php?session=r_data";
   js = "";
   run_ajax("run_ajax", url, js);
}


function save_vd(e) {
   if(e.old != e.value) {
      save = new container(
      e.field + " => " +  e.value,
      "set_history => " + session.domino_user + "////" + e.field + "////" + e.old + "////" + e.value);
      save_data_extdb(session.remote_domino_path + "/" + e.db, e.unid, "reload_save_vd()", 0);
      e.style.backgroundColor = '#f0ff86';
   }
   else {
      e.style.backgroundColor = '#ffffff';
   }
}


function handle_save_single_field_extdb(db, unid, field, value, postsave) {
    save = new container(field + " => " + value);
    save_data_extdb(db, unid, postsave, 0);
}

function save_data_extdb(db, unid, postsave, num) {
   p = new Array;
   for(property in save) p.push(property);
   if(p.length > num) {
      url = db + "/a.save_data?open&user=" + session.domino_user + "&unid=" + unid + "&f=" + escape(p[num]) + "&v=" + escape(save[p[num]]);
      js = "save_data_extdb('" + db + "', '" + unid + "', '" + postsave + "', " + (num + 1) + ")";
      run_ajax("run_ajax", url, js);
   }
   else eval(unescape(postsave));
}


function save_production_location(e, unid, p_location, p_date, p_c1, p_c2, p_c3, p_c4, p_c5, p_c6, p_c7, p_c8) {

   vendor = (typeof e == "object") ? p_location.substr(0, p_location.indexOf(":")) : e;
   save_location = "p_location";
   if(vendor.toLowerCase() == "select outside vendor") {
      url = session.remote_database_path + "/addin/data_picker.php?use_sortkey=2";
      document.getElementsByName("ifrm")[0].src = url;
      document.getElementsByName("ifrm")[0].style.visibility = "visible";
      document.getElementsByName("ifrm")[0].unid = unid;
      document.getElementsByName("ifrm")[0].p_location = p_location;
      document.getElementsByName("ifrm")[0].p_date = p_date;
      for(i = 1; i <= 8; i++) {
         eval("document.getElementsByName('ifrm')[0].p_c" + String(i) + " = p_c" + String(i));
      }
      return false;
   }
   else {
      p_c1 = unescape(p_c1);
      p_c2 = unescape(p_c2);
      p_c3 = unescape(p_c3);
      p_c4 = unescape(p_c4);
      p_c5 = unescape(p_c5);
      p_c6 = unescape(p_c6);
      p_c8 = unescape(p_c8);

      save = new container(
      "p_location => " + vendor,
      "mail_date => " + p_date,
      "mail_c1 => " + p_c1,
      "mail_c2 => " + p_c2,
      "mail_c3 => " + p_c3,
      "mail_c4 => " + p_c4,
      "mail_c5 => " + p_c5,
      "mail_c6 => " + p_c6,
      "mail_c7 => " + p_c7,
      "mail_c8 => " + p_c8,
      "mail_c9 => " + unescape(vendor).replace(/"/g, ""),
      "mail_c10 => " + document.getElementById("get_technology").innerText,
//      "mail_c11 => " + document.getElementById("get_evt_revised").innerText,
//      "mail_c12 => " + document.getElementById("get_cer_number").innerText,
      "mail_do => 1",
      "mail_delete => ",
      "mail_sendfrom => automailer-do not reply",
      "mail_sendto => " + (p_location.substr(p_location.indexOf(":") + 1, p_location.length)).replace(/,/g ,":"),
      "mail_replyto => nobody@te.com",
      "mail_template => t1");
      save_data(unid, "location.href = location.href", 0);
   }
}


function create_new_document(exception, param) {
   url = session.remote_domino_path_main + "/a.create_document?open";
   if(exception != "") url += "&exception=" + exception + "&param=" + param;
   run_ajax("run_ajax", url, "save_data(document.getElementById('run_ajax').innerText, '" + escape("location.href=(session.remote_database_path+'edit_tracker.php?&unid='+document.getElementById('run_ajax').innerText)") + "', 0)", false);
}

function do_add_pot() {
   num = (typeof arguments[0] == "undefined") ? 0 : arguments[0];
   pot = (document.getElementsByName("add_pot")[0].value).split(";");
   if(document.getElementById('add_proposal_pot').style.display == "none") {
      location.href = location.href;
   }
   else {
      if(pot.length > num) {
         d = document.getElementsByName("data_" + pot[num])[0];
         arg = (d.value).split("///");
         add_pot(arg[0], arg[1], arg[2], arg[3], (num + 1));
      }
      else {
         location.href = location.href;
      }
   }
}


function add_pot(tracker_id, pot_id, part_number, tool_number, num) {
   js = "do_add_pot(" + num + ");";
   if(document.getElementById("id_" + pot_id + "_3") && document.getElementById("id_" + pot_id + "_5")) {
      alert("POT [part " + part_number + " / Tool " + tool_number + "] already added!");
      eval(js);
   }
   else {
      var agree = confirm("Add POT [part " + part_number + " / Tool " + tool_number + "]?"); 
      if(agree) {
         url = session.remote_domino_path_main + "/a.save_data?open&exception=add_pot&unid=" + tracker_id + "&f=ref$pot&v=" + escape(pot_id);
         run_ajax("run_ajax", url, js);
      }
      else eval(js);
   }
}


function handle_selectbox() {
   t_name = arguments[0] + "_img";
   i = 0; while(i < document.getElementsByName(t_name).length) {
      t_img = (i == arguments[1]) ? "select_high" : "select_reg";
      document.getElementsByName(t_name)[i].src = session.php_server + "/library/images/icons/" + t_img + ".png";
      i++;
   }
}

function handle_field() {
   t_id = "info_" + arguments[0];
   document.getElementById(t_id).style.display = "block";
   for(i = 30; i < 88; i++) window.setTimeout("document.getElementById('" + t_id + "').style.filter = 'alpha(opacity=" + i + ")'", 3 * i);
   for(i = 0; i < 88; i++) window.setTimeout("document.getElementById('" + t_id + "').style.filter = 'alpha(opacity=" + (92 - i) + ")'", 2000 + (5 * i));
   window.setTimeout("document.getElementById('" + t_id + "').style.display = 'none'", 2400);
}

function save_multiple_dates() {
   num = (typeof arguments[0] == "undefined") ? 0 : arguments[0];
   if(document.getElementsByName("save_unid")[num]) {
      document.getElementsByTagName("body")[0].style.display = "none";
      t_unid = document.getElementsByName("save_unid")[num].innerText;
      t_field = document.getElementsByName("save_field")[num].innerText;
      t_value = document.getElementsByName("save_value")[num].innerText;
      url = session.remote_domino_path_main + "/a.save_data?open&unid=" + t_unid + "&f=" + escape(t_field) + "&v=" + escape(t_value);
      js = "save_multiple_dates(" + (num + 1) + ")";
      run_ajax("run_ajax", url, js, false);
   }
   else location.href = location.href;
}

function save_multiple_field(save, field, unid, num) {
   if(typeof num == "undefined") num = 0;
   save_arr = save.split("$$$$");
   if(typeof save_arr[num] != "undefined") {
      exception = (num == 0) ? "" : "multi"; 
      url = session.remote_domino_path_main + "/a.save_data?open&unid=" + unid + "&f=" + field + "&v=" + escape(save_arr[num]) + "&exception=" + exception;
      js = "save_multiple_field('" + save + "', '" + field + "', '" + unid + "', " + (num + 1) + ")";
      run_ajax("run_ajax", url, js);
   }
   else location.href = location.href;
}


function nopic(nopic){
   nopic.src='../../../../library/images/PhotoNA.jpg'
}


function create_proposal_pot(unique) {
   kill_app(true);
   part = document.getElementsByName("proposal_part")[0].value;
   tool = document.getElementsByName("proposal_tool")[0].value;
   url = session.remote_domino_path_main + "/a.create_proposal_pot?open&part=" + escape(part) + "&tool=" + escape(tool) + "&unique=" + unique;
   js = "document.getElementById('add_proposal_pot').style.display='none';add_pot('" + unique + "', document.getElementById('run_ajax').innerText, '" + part + "', '" + tool + "')";
   run_ajax("run_ajax", url, js);
}

function change_selected(e) {
   e.src = (e.src.indexOf("select_reg") > 0) ? "../../../../library/images/icons/select_high.png" : "../../../../library/images/icons/select_reg.png";
   res = false;
   if(arguments[1] != "undefined") {
      eval("res = " + arguments[1] + "(e)"); 
   }
   return res;
}


function get_selected_value(e) {
   d = document.getElementsByName(e.name);
   arr = new Array();
   for(i = 0; i < d.length; i++) {
      if(d[i].src.indexOf("select_reg") < 0) arr.push(d[i].nextSibling.innerText);
   }
   return arr.join(",");
}



function change_status(f, v, s) {
   kill_app(true);
   t = (typeof arguments[3] == "undefined") ? true : arguments[3];
   if(t) {
      kill_app(true);
      url = session.remote_domino_path_main + "/a.change_search?open&f=" + f + "&v=" + v + "&s=" + s;
      js = "location.href=location.href";
      run_ajax("run_ajax", url, js);
   }
}


function info_saved(id) {
   d = document.getElementById(id);
   d.style.backgroundRepeat = "no-repeat";
   d.style.backgroundPosition = "left center";
   d.style.backgroundColor = "rgb(183,200,130)";
   window.setTimeout('d.style.backgroundImage = "url(" + session.php_server + "/library/images/blank.gif)";d.style.backgroundColor = "rgb(220,220,220)";', 1000);
}

