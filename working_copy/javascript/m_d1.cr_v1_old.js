m_d1 = {

   table: [],
   
   embed: function() {
      d = document.querySelector("[form='m_d1__cr_v1']");
      if(d.getAttribute("is_proposal") == "1") {
         d.textContent = "Control report data not available [PROPOSAL]";
      }
      else {
         script = document.createElement("script");
         script.setAttribute("src", session.remote_database_path + "javascript/jload/get_cr.js?pot=<?php print $_SESSION["pot"]; ?>"); 
         script.setAttribute("type", "text/javascript");
         script.setAttribute("onload", "m_d1.nest()");
         document.getElementsByTagName("head")[0].appendChild(script);
      }
   },


   nest: function() {
      if(m_d1.pot.tmp.indexOf("No documents found") == -1) {
         m_d1.pot.tmp = m_d1.pot.tmp.split(":");
         m_d1.pot.h = m_d1.pot.tmp[0].split(";");
         for(k in m_d1.pot.h) m_d1.table[m_d1.pot.h[k]] = [];
         m_d1.table.searchkey = [];
         m_d1.table.len = m_d1.pot.tmp.length - 1;
         for(i = 1; i < m_d1.table.len; i++) {
            tmp = m_d1.pot.tmp[i].split(";");
            searchkey = [];
            for(k in tmp) {
               m_d1.table[m_d1.pot.h[k]].push(unescape(tmp[k]));
               tmpd = document.createElement("div");
               tmpd.innerHTML = unescape(tmp[k]);
               searchkey.push(tmpd.textContent.toLowerCase());
            }
            m_d1.table.searchkey[i - 1] = searchkey.join(" ");
         }
      }
      m_d1.create();
   },

   create: function() {
      table = document.createElement("table");
      tr = document.createElement("tr");
      table.appendChild(tr);
      for(k in m_d1.pot.h) {
         if(m_d1.pot.h[k].toLowerCase() != "unid") {
            th = document.createElement("th");
            th.textContent = (m_d1.pot.h[k].substr(0, 1).toUpperCase() + m_d1.pot.h[k].substr(1, m_d1.pot.h[k].length - 1)).replace(/_/g, " ");
            tr.appendChild(th);
         }
      }
      for(i = 0; i < m_d1.table.len - 1; i++) {
         tr = document.createElement("tr");
         tr.setAttribute("searchkey", m_d1.table.searchkey[i]);
         table.appendChild(tr);
         for(k in m_d1.pot.h) {
            if(m_d1.pot.h[k].toLowerCase() != "unid") {
               td = document.createElement("td");
               td.innerHTML = m_d1.table[m_d1.pot.h[k]][i];
               tr.appendChild(td);
               if(in_array(m_d1.pot.h[k].toLowerCase(), ["ir", "irp", "irs", "ira"])) m_d1.colorize(td, i, k);
            }
         }
      }

      input = document.createElement("input");
      input.setAttribute("value", "");
      input.setAttribute("onkeyup", "helper.search(this, document.querySelector('[form=\\'m_d1__cr_v1\\']').getElementsByTagName('table')[1])");
      visibility = (document.getElementById("m_x3__workflow")) ? "visible" : "hidden";

      h = document.createElement("table");
      h.setAttribute("border", "0");
      h.setAttribute("cellpadding", "0");
      h.setAttribute("cellspacing", "0");
      h.setAttribute("style", "width:974px;");
      h.innerHTML = "<tr><td style=\"width:24%;\">" + input.outerHTML + "</td>" +
      "<td><span anchor=\"create_new_control_report\" class=\"phpbutton\" style=\"position:relative;left:3px; top:-6px; visibility:" + visibility + ";\"><a href=\"javascript:m_d1.cr_nest();\">Create new control report</a></span></td></tr></table>";

      with(document.querySelector("[form='m_d1__cr_v1']")) {
         appendChild(h);
         appendChild(table);
      }
   },

   colorize: function(td, i, k) {
      c = "status0";
      usecolor = {status0:"rgb(255,255,255)", status1: "rgb(0,204,0)", status2: "rgb(255,255,0)", status3: "rgb(255,76,76)"};
      ir = m_d1.table["IR"][i];
      irp = m_d1.table["IRP"][i];
      irs = m_d1.table["IRS"][i];
      ira = m_d1.table["IRA"][i];

      if(ir == "E" | irp == "G" | irs == "G" | ira=="G") {c = "status3"} else {c = "status2"};
      if(ir == "A" && irp == "B" && irs == "L" && (ira=="" | ira=="N" | ira=="C1" | ira=="B")) { c = "status1" };
      if(ir == "") {c = "status0"}

      td.setAttribute("style", "background:" + usecolor[c]);
   },


   cr_nest: function() {
<?php
session_start();
require_once("../../../../library/tools/addin_xml.php");

$load = array("meta");
require_once("../addin/load_tracker_data.php");
$tc_data = create_tc_data($load);
$key = (strpos($tc_data["meta"]["process_type"], "v->2")) ? $_SESSION["request:pot"]."@".$_SESSION["request:unique"] : $_SESSION["request:pot"]."@".$_SESSION["request:unique"]."@G04070";
$loop = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.get_loop?open&key=".$key);
print "      ".$loop;
print "      m_d1.process_type = \"".$tc_data["meta"]["process_type"]."\";\r\n";
?>


      wf_container = document.querySelector("[form='m_x3__workflow']");
      if(wf_container) {
         if(wf_container.hasAttribute("v2_loop")) {
            if(wf_container.hasAttribute("v2_loop")) loop = wf_container.getAttribute("v2_loop").split(",");
         }
         else {
            for(c in loop) if(loop[c] > 0) loop[c]--;
         }
      }
      m_d1.loop = loop;


      has_cr = [-1, -1];
      for(k in m_d1.table["Loop"]) {
         if(m_d1.table.Source[k] == "1") if(Number(m_d1.table.Loop[k]) > has_cr[0]) has_cr[0] = m_d1.table.Loop[k];
         if(m_d1.table.Source[k] == "2") if(Number(m_d1.table.Loop[k]) > has_cr[1]) has_cr[1] = m_d1.table.Loop[k];
      }

      arr = {
           label: ["Create<br>Product CR for<br>Source 1 / Loop " + loop[0], "Create Product<br>and process CR for<br>Source 1 / Loop " + loop[0], "Create process<br>trial run for<br>Source 1", "Create<br>Product CR for<br>Source 2 / Loop " + loop[1], "Create Product<br>and process CR for<br>Source 2 / Loop " + loop[1], "Create process<br>trial run for<br>Source 2"],
           message: {
              label1: [
              "Select this option to create a Product CR for source 1",
              "You are not authorized to perfom this operation.",
              "No dual source.",
              "No mold.",
              "CR already exists for this loop.",
              "Tool already transfered",
              "Pilot Tool"
              ],
              label2: [
              "Select this option to create a Product & Process CR for source 1",
              "You are not authorized to perfom this operation.",
              "No dual source.",
              "No mold.",
              "CR already exists for this loop.",
              "Tool already transfered",
              "Pilot Tool"
              ],
              label3: [
              "Select this option to create a PTR CR for source 1",
              "You are not authorized to perfom this operation.",
              "No dual source.",
              "No mold.",
              "No Product CR created.",
              "Tool already transfered",
              "Pilot Tool"
              ],
              label4: [
              "Select this option to create a Product CR for source 2",
              "You are not authorized to perfom this operation.",
              "No dual source.",
              "No mold.",
              "CR already exists for this loop.",
              "Tool already transfered",
              "Pilot Tool"
              ],
              label5: [
              "Select this option to create a Product & Process CR for source 2",
              "You are not authorized to perfom this operation.",
              "No dual source.",
              "No mold.",
              "CR already exists for this loop.",
              "Tool already transfered",
              "Pilot Tool"
              ],
              label6: [
              "Select this option to create a PTR CR for source 2",
              "You are not authorized to perfom this operation.",
              "No dual source.",
              "No mold.",
              "No Product CR created.",
              "Tool already transfered",
              "Pilot Tool"
              ],
           },
      };
      table = document.createElement("table");
      table.setAttribute("id", "control_report_select");
      tr = document.createElement("tr");
      table.appendChild(tr);
      td = document.createElement("td");
      td.setAttribute("colspan", "2");

      <?php session_start(); 
      print "userroles = \"".implode(",", $_SESSION["remote_userroles"])."\".split(\",\");\r\n";
      require_once("../addin/load_tracker_data.php");
      $load = array("pot", "tool");
      $tc_data = create_tc_data($load);
      print "      tool = {number: \"".$tc_data["tool"]["number"]."\", dualsource: \"".$tc_data["tool"]["dualsource"]."\", transferred_location: \"".$tc_data["tool"]["transferred_location"]."\"}\r\n"; 
      print "      tool.transferred_location = tool.transferred_location.replace(/Array/g, \"\");\r\n";
      ?>
      op = []; cu = []; js = [];
      op[0] = (tool.transferred_location == "" || !in_array("[t_location]", userroles)) ? 0.5 : 1.0;
      op[1] = (tool.transferred_location == "" || !in_array("[t_location]", userroles) || tool.dualsource != "1") ? 0.5 : 1.0;
      cu[0] = (tool.transferred_location == "" || !in_array("[t_location]", userroles)) ? "default" : "pointer";
      cu[1] = (tool.transferred_location == "" || !in_array("[t_location]", userroles) || tool.dualsource != "1") ? "default" : "pointer";
      js[0] = (tool.transferred_location == "" || !in_array("[t_location]", userroles)) ? "void(0);" : "m_d1.transfer = true; m_d1.key = 0; m_d1.control_report_create(0);";
      js[1] = (tool.transferred_location == "" || !in_array("[t_location]", userroles) || tool.dualsource != "1") ? ";" : "m_d1.transfer = true; m_d1.key = 3; m_d1.control_report_create(3);";

      transfer = [
         "<span style=\"position:relative; top:20px; left:880px; display:inline-block; opacity: " + String(op[0]) + "\"><a style=\"cursor: " + cu + "; border:solid 2px rgb(99,177,229); font:normal 12px Open Sans; text-decoration:none; color:rgb(255,255,255); display:inline-block; width:134px; height:51px; background-color:rgb(0,102,161); text-align:center; padding-top:4px;\" href=\"javascript:" + js[0] + "\">CREATE<br>TRANSFER<br>SOURCE 1</a></span>",
         "<span style=\"position:relative; top:20px; left:890px; display:inline-block; opacity: " + String(op[1]) + "\"><a style=\"cursor: " + cu + "; border:solid 2px rgb(99,177,229); font:normal 12px Open Sans; text-decoration:none; color:rgb(255,255,255); display:inline-block; width:134px; height:51px; background-color:rgb(0,102,161); text-align:center; padding-top:4px;\" href=\"javascript:" + js[1] + "\">CREATE<br>TRANSFER<br>SOURCE 2</a></span>"
      ];

      td.innerHTML = "<span style=\"position:absolute;\">" + transfer.join("") + "</span><h1>Please select a control report type</h1>";
      tr.appendChild(td);
      for(k in arr.label) {

         chk = true;
         msg = [0];


         if(k == 0 && !in_array("[cr]", userroles)) {
            chk = false;
            msg.push(1);
         } 
         if(k != "0" && pilot[0] == "yes") {
            chk = false;
            msg.push(6);
         } 
         if(k == 1 && !in_array("[cr]", userroles)) {
            chk = false;
            msg.push(1);
         } 
         if(k == 2 && !in_array("[composeproz]", userroles)) {
            chk = false;
            msg.push(1);
         } 
         if(k == 3 && !in_array("[cr]", userroles)) {
            chk = false;
            msg.push(1);
         } 
         if(k == 4 && !in_array("[cr]", userroles)) {
            chk = false;
            msg.push(1);
         } 
         if(k == 5 && !in_array("[composeproz]", userroles)) {
            chk = false;
            msg.push(1);
         } 

         if(k > 2 && tool.dualsource != "1") {
            chk = false;
            msg.push(2);
         }
         if(k != "0" && k != "3" && !in_array(tool.number.substr(0, 2), ["21", "23", "28"])) {
            chk = false;
            msg.push(3);
         }

         if(((k == 0 || k == 1) && Number(loop[0]) <= Number(has_cr[0])) || ((k == 3 || k == 4) && Number(loop[1]) <= Number(has_cr[1]))) {
            chk = false;
            msg.push(4);
         }

         if((k == 2 && has_cr[0] == -1) || (k == 5 && has_cr[1] == -1)) {
            chk = false;
            msg.push(4);
         }

         if(tool.transferred_location != "") {
            chk = false;
            msg=[5];
         }

         opacity = (chk) ? 1.0 : 0.5;
         cursor = (chk) ? "pointer" : "default";
         js = (chk) ? "m_d1.transfer = false; m_d1.key = " + k + "; m_d1.control_report_create(" + k + ")" : "void(0)";

         tr = document.createElement("tr");
         table.appendChild(tr);
         td = document.createElement("td");
         td.setAttribute("style", "vertical-align:top; height:120px; width:1%;");
         td.innerHTML = "<span style=\"width:178px; height:51px; opacity:" + opacity + ";\" class=\"phpbutton\"><a style=\"text-align:center; height:49px; cursor:" + cursor + "\" href=\"javascript:" + js + ";\">" + arr.label[k] + "</a></span>";
         tr.appendChild(td);
         td = document.createElement("td");
         td.setAttribute("style", "vertical-align:top; height:120px; margin-left:20px;");
         td.innerHTML = ""; 
         for(i in msg) if(msg.length == 1 || msg.length > 1 && i != -1) td.innerHTML += "<b>" + arr.message["label" + String(Number(k) + 1)][msg[i]] +"</b>";
         tr.appendChild(td);
      } 

      infobox("Create Control Report", table.outerHTML, 1200, 900);
   
   },

   control_report_create: function(k) {
      if(in_array(k, [2, 5]) && typeof arguments[2] == "undefined") {
         loop = (k == 2) ? "1" : "2";
         d = document.getElementById("confirm_box").getElementsByClassName("body")[0];
         d.innerHTML = "<h1>Please select the control report to copy</h1>";
         table = document.createElement("table");
         table.setAttribute("border", "0");
         table.setAttribute("cellpadding", "0");
         table.setAttribute("cellspacing", "0");
         tr = document.createElement("tr");
         table.appendChild(tr);
         for(n in m_d1.pot.h) {
            if(m_d1.pot.h[n].toLowerCase() != "unid") {
               th = document.createElement("th");
               th.textContent = (m_d1.pot.h[n].substr(0, 1).toUpperCase() + m_d1.pot.h[n].substr(1, m_d1.pot.h[n].length - 1)).replace(/_/g, " ");
               tr.appendChild(th);
            }
         }
         for(i = 0; i < m_d1.table.len - 1; i++) {
            if(unescape(m_d1.table[m_d1.pot.h[2]][i]) == loop && unescape(m_d1.table[m_d1.pot.h[4]][i]).indexOf("evaluated") > -1) {
               tr = document.createElement("tr");
               tr.setAttribute("onclick", "m_d1.control_report_create(" + k + ", false,'" + unescape(m_d1.table[m_d1.pot.h[0]][i]) + "')");
               table.appendChild(tr);
               for(n in m_d1.pot.h) {
                  if(m_d1.pot.h[n].toLowerCase() != "unid") {
                     td = document.createElement("td");
                     a = document.createElement("a");
                     a.innerHTML = unescape(m_d1.table[m_d1.pot.h[n]][i]);
                     td.innerHTML = a.textContent;
                     tr.appendChild(td);
                  }
               }
            }
         }
         d.innerHTML = "<span form=\"m_d1__cr_v1:copy\">" + d.innerHTML + table.outerHTML + "</span>";
      }
      else {
         if(typeof arguments[1] == "undefined" || arguments[1] == false) {
            d = document.getElementById("form:control_report");
            arguments[2] = (typeof arguments[2] == "undefined") ? "" : arguments[2];
            if(d) d.parentNode.removeChild(d);
            script = document.createElement("script");
            script.setAttribute("src", session.remote_database_path + "javascript/jload/control_report.js?cr=" + arguments[2] + "&k=" + String(Number(k) + 1) + "&pot=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("pot") + "&tracker=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("tracker") + "&process_type=" + m_d1.process_type);
            script.setAttribute("type", "text/javascript");
            script.setAttribute("id", "form:control_report");
            script.setAttribute("onload", "m_d1.control_report_create(" + k + ", true, '" + arguments[2] + "')");
            document.getElementsByTagName("head")[0].appendChild(script);
         }
         else {
            control_report.nest(k);
         }
      }
   },

}



