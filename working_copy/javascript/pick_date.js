calendar_big_pick_date = function(unid, fld) {

   descr = (typeof arguments[2] == "undefined") ? "" : unescape(arguments[2]);
   rc = (typeof arguments[3] == "undefined") ? "" : unescape(arguments[3]);
   dsp = (typeof arguments[4] == "undefined") ? "" : unescape(arguments[4]);
   planned_date = (typeof arguments[5] == "undefined") ? "" : unescape(arguments[5]);
   key = (typeof arguments[6] == "undefined") ? "" : unescape(arguments[6]);
   do_show = (typeof arguments[7] == "undefined") ? true : arguments[7];

   year = <?php print date("Y", mktime()); ?>;
   visibility = (do_show) ? "visibility:visible;" : "visibility:hidden;";


   p_x = window.innerWidth - 70;
   p_y = window.innerHeight - 70;
   p_left = 30;
   p_top = 30;

   if(typeof control_report == "object") {
      if(control_report.embed_calendar) {
         p_x = 1200;
         p_y = 900;
         p_left = Math.round(window.innerWidth / 2) - Math.round(p_x / 2);
         p_top = Math.round(window.innerHeight / 2) - Math.round(p_y / 2) - 10;
      }
   }

   if(typeof m_e3__ee_edit_pot != "undefined") {
      if(typeof m_e3__ee_edit_pot.html == "string") {
         p_x = 1180;
         p_y = 700;
         rc = "";
         p_left = Math.round(window.innerWidth / 2) - Math.round(p_x / 2);
         p_top = Math.round(window.innerHeight / 2) - Math.round(p_y / 2) - 40;
      }
   }
   do_fade(true);
   div_frame = document.createElement("div");
   div_frame.setAttribute("id", "confirm_box_cal");
   div_frame.setAttribute("rc", rc);
   div_frame.setAttribute("dsp", dsp);
   div_frame.setAttribute("key", key);
   div_frame.setAttribute("planned_date", planned_date);
   div_frame.setAttribute("style", "left:" + String(p_left) + "px; top:" + p_top + "px; width:" + p_x + "px; height:" + p_y + "px;" + visibility);
   document.getElementsByTagName("body")[0].appendChild(div_frame);
   h = document.createElement("div");
   h.setAttribute("class", "header");
   h.setAttribute("style", "width:" + (p_x - 5) + "px;");
   h.innerHTML = "<table style=\"width:100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>" +
   "<td>" + descr + "</td>" + 
   "<td style=\"text-align:right;position:relative;top:-1px;left:-4px;\"><span style=\"cursor:pointer;\" onclick=\"do_fade(false); d=document.getElementById('confirm_box_cal'); d.parentNode.removeChild(d);\">[x]</span></td>" + 
   "</tr></table>";
   div_frame.appendChild(h);
   table_frame = document.createElement("table");
   table_frame.setAttribute("border", "0");
   table_frame.setAttribute("cellpadding", "0");
   table_frame.setAttribute("cellspacing", "0");
   tr_frame = document.createElement("tr");
   td_frame = document.createElement("td");
   td_frame.setAttribute("id", "calendar_year_pick_date");
   td_frame.setAttribute("style", "white-space:nowrap;");
   embed_calendar_year_date_picker(td_frame, unid, fld, year);
   tr_frame.appendChild(td_frame);
   td_frame = document.createElement("td");
   td_frame.setAttribute("id", "calendar_year_nested");
   embed_calendar_big_date_picker(td_frame, unid, fld, year);
   tr_frame.appendChild(td_frame); 
   table_frame.appendChild(tr_frame);
   div_frame.appendChild(table_frame);
}

function recalc_calendar_big_pick_date(unid, fld, year) {
   d = document.getElementById("calendar_year_nested");
   d.innerHTML = "";
   embed_calendar_big_date_picker(d, unid, fld, year);
   d = document.getElementById("calendar_year_pick_date");
   d.innerHTML = "";
   embed_calendar_year_date_picker(d, unid, fld, year);
}

function embed_calendar_year_date_picker(d, unid, fld, year) {
   while(d.firstChild) d.removeChild(d.firstChild);
   div = new Array;
   div[0] = document.createElement("div");
   div[0].innerHTML = "<a href=\"javascript:void(0);\" onclick=\"recalc_calendar_big_pick_date('" + unid + "', '" + fld + "', " + String(year - 1) + ");\"><img src=\"" + session.php_server + "/library/images/24x24/interface-76.png\" /></a>";
   div[0].setAttribute("class","calendar_year_nav");
   div[1] = document.createElement("div");
   div[1].appendChild(document.createTextNode(year));
   div[1].setAttribute("class","calendar_year_date");
   div[2] = document.createElement("div");
   div[2].innerHTML = "<a href=\"javascript:void(0);\" onclick=\"recalc_calendar_big_pick_date('" + unid + "', '" + fld + "', " + String(year + 1) + ");\"><img src=\"" + session.php_server + "/library/images/24x24/interface-75.png\" /></a>";
   div[2].setAttribute("class","calendar_year_nav");
   span = document.createElement("span");
   span.setAttribute("style", "position:relative;left:-2px");
   for(i = 0; i < div.length; i++) span.appendChild(div[i]);
   d.appendChild(span);
}


function embed_calendar_big_date_picker(nest_calendar, unid, fld, year) {

   date = [];
   month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
   weekday = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];

   table = document.createElement("table");
   table.setAttribute("id", "calendar_big_pick_date_" + String(year));
   table.setAttribute("border", "0");
   table.setAttribute("cellpadding", "0");
   table.setAttribute("cellspacing", "0");
   table.setAttribute("style", "display:block;padding:10px;table-layout:fixed;");
   table.setAttribute("class", "calendar_big_pick_date");
   table.setAttribute("unselectable", "on");
   for(d = -1; d < 31; d++) { 
      tr = document.createElement("tr");
      for(m = 0; m < 12; m++) {
         if(d == -1) {
            td = document.createElement("td");
            td.setAttribute("class", "month");
            td.appendChild(document.createTextNode(month[m]));
         }
         else { 
            date[m] = new Date(year, m, d + 1, 0 ,0, 0);
            html = (date[m].getMonth() == m) ? get_date("d", date[m].getTime() / 1000) + " " + weekday[date[m].getDay()]: "";
            if(date[m].getDay() == "1") html = "<span style=\"position:absolute;padding-top:2px;\">" + html + "</span><span style=\"font-size:15px; width:100%; display:inline-block; text-align:right; color:rgb(66,66,66);\">" +date[m].getWeek() + "&nbsp;</span>";
            brght = (m == 11) ? "border-right:solid 1px rgb(66,66,66);" : "";
            bbtm = (d == 30) ? "border-bottom:solid 1px rgb(66,66,66);" : "";
            td = document.createElement("td");
            td.setAttribute("style", "vertical-align:top;border-top:solid 1px rgb(66,66,66);border-left:solid 1px rgb(66,66,66);" + brght + bbtm);
            td.setAttribute("id", "calendar_big_" + get_date('Y_m_d', date[m].getTime() / 1000));
            w = Math.round((document.getElementById("confirm_box_cal").offsetWidth - 285) / 12);
            h = Math.round((document.getElementById("confirm_box_cal").offsetHeight - 120) / 31);
            t_day = <?php print (20 + intval(date("Y", mktime())))."0101"; ?>;
            f_day = Number(get_date('Ymd', date[m].getTime() / 1000));

            href = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"calendar_date\" style=\"width:" + w + "px;height:" + h + "px;\"><tr><td style=\"vertical-align:middle;\">" + html + "</td></tr></table>";
            if(html != "" && (t_day - f_day) >= 0) {
               td.setAttribute("date", get_date('Y-m-d', date[m].getTime() / 1000));
               td.setAttribute("onmouseover", "this.style.backgroundColor='rgb(61,183,228)'");
               td.setAttribute("onmouseout", "this.style.removeProperty('background-color')");
               td.setAttribute("ondragover", "allow_drop(event)");
               if(typeof pick_date == "undefined") pick_date = {execute: {}};
               use_function = (typeof pick_date.execute == "string") ? pick_date.execute : "save_calendar_date";
               if(fld.substr(0, 10) == "superuser:") href = "<a href=\"javascript:m_t1__process_indicator.superuser.date('" + get_date('Y-m-d', date[m].getTime() / 1000)  + "');\">" + href + "</a>";
               else href = "<a href=\"javascript:pick_date." + use_function + "('" + unid + "', '" + fld + "', '" + get_date('Y-m-d', date[m].getTime() / 1000)  + "');\">" + href + "</a>";
            }
            else {
               td.setAttribute("style", "cursor:default;" + td.getAttribute("style"));
            }

            if(html !="") td.setAttribute("class", weekday[date[m].getDay()].toLowerCase());
            td.innerHTML = (html != "") ? href : "&nbsp;";     
         }
         tr.appendChild(td); 
      }
      table.appendChild(tr);
   }
   if(typeof m_e3__ee_edit_pot != "undefined") {
      if(typeof m_e3__ee_edit_pot.html == "string") table.innerHTML = table.innerHTML.replace(/pick_date.save_calendar_date/g, "m_e3__ee_edit_pot.save_calendar_date");
   }
   if(document.getElementById("confirm_box_cal").getAttribute("rc") != "" || unid == "control_report") table.innerHTML = table.innerHTML.replace(/pick_date.save_calendar_date/g, "embed_rc");
   nest_calendar.appendChild(table);
}

function embed_rc(unid, f, v) {
   if(unid == "control_report") {
      control_report.calendar(f, v);
      return false;
   }
   planned_date = document.getElementById("confirm_box_cal").getAttribute("planned_date");
   if(document.getElementById("reason_code").textContent != "true") {
      pick_date.save_calendar_date(unid, f, v);
   }
   else {
      document.getElementById("calendar_year_pick_date").style.visibility = "hidden";
      d = document.getElementById("calendar_year_nested");
      d.innerHTML = "<div id=\"select_reason_code\" style=\"font:normal 40px Open Sans; margin-top:14px; margin-bottom:18px;\">Select reason code</div>" + 
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>" + 
      "<td><b>&nbsp;" + document.getElementById("confirm_box_cal").getAttribute("dsp") + "&nbsp;&nbsp;&nbsp;&nbsp;</b></td>" + 
      "<td>" + v + "&nbsp;&nbsp;&nbsp;&nbsp;</td>" +
      "<td><select style=\"font:normal 13px Open Sans; width:200px;\" onchange=\"if(this.selectedIndex > 0) pick_date.save_calendar_date('" + unid + "', '" + f + "', '" + v + "/' + this[this.selectedIndex].text);\"></select></td>" + 
      "</tr></table>";
      select = d.getElementsByTagName("select")[0];
      rc = document.getElementById("confirm_box_cal").getAttribute("rc").split(":");
      option = document.createElement("option");
      select.appendChild(option);
      for(key in rc) {
         option = document.createElement("option");
         option.textContent = rc[key];
         select.appendChild(option);
      }
   }
}


get_date = ""; if(typeof get_date != "function") {
   get_date = function(strng, date) {
      date = new Date(date * 1000);
      strng = strng.replace(/Y/g, date.getFullYear());
      strng = strng.replace(/m/g, ("0" + (date.getMonth() + 1)).substr(-2));
      strng = strng.replace(/d/g, ("0" + date.getDate()).substr(-2));
      strng = strng.replace(/W/g, date.getDay());
      return strng;
   }
}


Date.prototype.getWeek = function () { return $.datepicker.iso8601Week(this); }


