m_u9__configure_responsible = {
   nest: function() {
      d = document.getElementById("tbl_m_u9__userlist").getElementsByTagName("td");
      for(k in d) if(!isNaN(k)) {
         d[k].setAttribute("key", d[k].textContent);
         d[k].textContent = d[k].getAttribute("key")
      }
   },

   search: function() {
      use_dsp = (typeof arguments[1] == "undefined") ? true : false;
      d = (typeof arguments[1] == "undefined") ? document.getElementById("tbl_m_u9__userlist").getElementsByTagName("td") : arguments[1];
      v = (arguments[0].value.toLowerCase().trim()).split(" ");
      for(k in d) if(!isNaN(k)) {
         dsp = true;
         key = d[k].getAttribute("key").toLowerCase();
         for(i = 0; i < v.length; i++) {
            if(key.indexOf(v[i]) == -1) {
               dsp = false;
               break;
            }
         }
         if(use_dsp) d[k].parentNode.style.display = (dsp) ? "table-row" : "none";
         else d[k].style.display = (dsp) ? "block" : "none";
      }
   },

   edit: function(e) {
      d = document.getElementById("tbl_m_u9__userlist").getElementsByTagName("td");
      h_next = e.getAttribute("h_next");
      form = document.getElementById("tbl_m_u9__configure_responsible_form");
      form.innerHTML = "";

      div = document.createElement("div");
      div.setAttribute("class", "headline");
      div.textContent = "Edit " + e.getAttribute("key");
      form.appendChild(div);

      table = document.createElement("table");
      table.setAttribute("border", "0");
      table.setAttribute("cellpadding", "0");
      table.setAttribute("cellspacing", "2");

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.setAttribute("oncontextmenu", "protocol.embed('" + e.getAttribute("unid") + "', 'h_next', ''); return false;");
      td.innerHTML = "<history>Manager:</history>";
      tr.appendChild(td);

      td = document.createElement("td");
      select = document.createElement("select");
      select.setAttribute("onchange", "m_u9__configure_responsible.save('h_next', this[this.selectedIndex].value, '" + e.getAttribute("unid") + "')");
      option = document.createElement("option");
      select.appendChild(option);
      for(k in d) if(!isNaN(k)) {
         option = document.createElement("option");
         option.setAttribute("value", d[k].getAttribute("username"));
         option.textContent = d[k].textContent;
         if(d[k].getAttribute("username") == h_next) option.setAttribute("selected", "selected");
         select.appendChild(option);
      }
      td.appendChild(select);
      tr.appendChild(td);
      table.appendChild(tr);

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.setAttribute("oncontextmenu", "protocol.embed('" + e.getAttribute("unid") + "', 'subprocess', ''); return false;");
      td.innerHTML = "<history>Subprocesses:</history>" + 
      "<br><br>" + 
      "<a href=\"javascript:m_u9__configure_responsible.select_all(true);\" style=\"text-decoration:none; color:rgb(0,102,158);\">Select all</a>" +
      "&nbsp;&nbsp;&nbsp;" +
      "<a href=\"javascript:m_u9__configure_responsible.select_all(false);\" style=\"text-decoration:none; color:rgb(0,102,158);\">Deselect all</a>";

      tr.appendChild(td);

      td = document.createElement("td");
      input = document.createElement("input");
      input.setAttribute("onkeyup", "m_u9__configure_responsible.search(this, this.parentNode.getElementsByTagName('div'))");
      input.setAttribute("style", "margin-bottom:5px;");
      td.appendChild(input);

      sp = document.getElementsByTagName("subprocess");
      sp_user = e.getAttribute("subprocess").toLowerCase().split(":");

      p = document.createElement("p");
      for(k in sp) if(!isNaN(k)) {
         val = unescape(sp[k].textContent);
         div = document.createElement("div");
         div.setAttribute("value", val);
         div.setAttribute("key", val.replace(/v->2/g, " version 2").toLowerCase());
         img = (in_array(val.toLowerCase(), sp_user)) ? "status-5" : "status-5-1"
         div.innerHTML = "<img name=\"m_u9__configure_responsible_subprocess\" onclick=\"m_u9__configure_responsible.handle_subprocess(this, '" + e.getAttribute("unid") + "');\" src=\"../../../../library/images/16x16/" + img + ".png\" style=\"vertical-align:top; margin-right:6px;\">" + val.replace(/v->2/g, " Version 2");
         p.appendChild(div);
      }
      td.appendChild(p);

      tr.appendChild(td);
      table.appendChild(tr);

      form.appendChild(table);
   },

   handle_subprocess: function(e, unid) {
      do_fade(true, true);
      e.src = (e.src.indexOf('5-1') == -1) ? e.src.replace("status-5", "status-5-1") : e.src.replace("status-5-1", "status-5");
      d = document.getElementsByName(e.getAttribute("name"));
      arr = [];
      for(k in d) if(!isNaN(k)) {
         if(d[k].src.indexOf("5-1") == -1) arr.push(d[k].parentNode.getAttribute("value"))
      }
      data = "&unid=" + unid + "&split=" + escape(":") + "&fld1=" + escape("subprocess/" + arr.join(":"));
      post.save_data("f.save_data", "", data, "document.getElementById('run_ajax').style.display='none'; m_u9__configure_responsible.activate();");
   },

   select_all: function() {
      d = document.getElementsByName("m_u9__configure_responsible_subprocess");
      for(k in d) if(!isNaN(k)) {
         if(d[k].parentNode.style.display != "none") {
            d[k].src = (arguments[0]) ? "../../../../library/images/16x16/status-5.png" : "../../../../library/images/16x16/status-5-1.png";
         }
      }
      d[0].src = (!arguments[0]) ? "../../../../library/images/16x16/status-5.png" : "../../../../library/images/16x16/status-5-1.png";
      d[0].click();
   },

   save: function() {
      d = document.getElementById("tbl_m_u9__userlist").getElementsByTagName("td");
      for(k in d) if(!isNaN(k)) {
         if(d[k].getAttribute("unid") == arguments[2]) {
            d[k].setAttribute("h_next", arguments[1]);
         }
      }
      data = "&unid=" + arguments[2] + "&fld1=" + escape(arguments[0] + "/" + arguments[1]);
      post.save_data("f.save_data", "", data, "");
   },

   activate: function() {
      if(typeof arguments[0] == "undefined") {
         d = document.querySelector("[anchor='activator']");
         if(d) d.style.visibility = "visible";
      }
      else {
         do_fade(true);
         run_ajax("run_ajax", session.remote_domino_path_main + "/a.export_process?open", "location.reload()");
      }
   }

}

