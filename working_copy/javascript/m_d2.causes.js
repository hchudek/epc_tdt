m_d2__causes = {
   do: function(e) {
      arr = [];
      dsp = (e.src.indexOf("11-1") != -1) ? "status-11" : "status-11-1";
      e.src = session.php_server + "/library/images/16x16/" + dsp + ".png";
      d = document.getElementsByName(e.getAttribute("name"));
      for(k in d) if(!isNaN(k)) {
         if(d[k].src.indexOf("11-1") == -1) arr.push(d[k].nextSibling.getAttribute("value"));

      }
      form = document.getElementsByName("form_m_d2__causes")[0];
      v = form.getAttribute("action").substr(0, form.getAttribute("action").indexOf("?") + 1) + "do=" + arr.join(",");
      form.setAttribute("action", v);
   }
}

