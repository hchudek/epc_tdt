m_e1 = {
   nest: function() {
      d = document.querySelector("[id='m_e1__ee_report_tng']").getElementsByClassName("inner_box_x")[0].getElementsByTagName("img");
      for(k in d) if(!isNaN(k)) {
         if(!d[k].hasAttribute("type")) { 
            d[k].src = (in_array(d[k].getAttribute("name"), m_e1.active)) ? session.php_server + "/library/images/16x16/status-5.png" : session.php_server + "/library/images/16x16/status-5-1.png";
            d[k].style.cursor = "pointer";
            d[k].onclick = function() {
               if(in_array(this.getAttribute("name"), m_e1.active)) {
                  this.src = session.php_server + "/library/images/16x16/status-5-1.png";
                  m_e1.select();
               }
               else {
                  this.src = session.php_server + "/library/images/16x16/status-5.png";
                  m_e1.select();
               }
            }
         }
      }
   },
   refresh: function() {
      infobox("Refreshing report", "<img src=\"../../../../library/images/ajax/ajax-loader.gif\"><span style=\"position:relative; top:-2px; left:7px;\">Processing...</span>", 300, 80);
      run_ajax("run_ajax", "modules/m_e1.ee_report_tng.php?m_e1__refresh=true", "location.reload()"); 
   },
   save_perpage: function() {
      document.querySelector("[name='m_e1__ee_report_tng_fieldlist']").value = m_e1.active.join(",");
      save_perpage("", "");
   },
   get_excel: function() {
      do_fade(true);
      infobox("Creating Excel", "<img src=\"../../../../library/images/ajax/ajax-loader.gif\"><span style=\"position:relative; top:-2px; left:7px;\">Processing...</span>", 300, 80);
      js = "infobox_handle.close('confirm_box')";
      responsible = document.querySelector("[form='responsible']")[document.querySelector("[form='responsible']").selectedIndex].text;
      url = "modules/m_e1.ee_report_tng.php?m_e1__xls=true&count=99999&m_e1__responsible=" + escape(responsible.toLowerCase());
      if(typeof m_e1.active == "undefined") m_e1.active = document.querySelector("[name='m_e1__ee_report_tng_fieldlist']").value.split(",");
      run_ajax("run_ajax", url, js, "&key=" + m_e1.active.join(";"));
   },
   select: function() {
      d = document.querySelector("[id='m_e1__ee_report_tng']").getElementsByClassName("inner_box_x")[0].getElementsByTagName("img");
      m_e1.active = [];
      for(k in d) if(!isNaN(k)) {
         if(typeof arguments[0] == "undefined") {
            if(d[k].src.indexOf("5-1") == -1) if(!d[k].hasAttribute("type")) m_e1.active.push(d[k].name);
         }
         else {
            if(!d[k].hasAttribute("type")) {
               d[k].src = (arguments[0]) ? session.php_server + "/library/images/16x16/status-5.png" : session.php_server + "/library/images/16x16/status-5-1.png";
               if(arguments[0]) m_e1.active.push(d[k].name);
            }
         }
      }
      m_e1.save_perpage();
   },
   get_smartsearch() {
      location.href = "?m_e1__responsible=" + escape(arguments[0].toLowerCase()) + "&m_e1__embed=smartsearch";
   }
}

