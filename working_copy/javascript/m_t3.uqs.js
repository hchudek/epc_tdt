<?php
$path_translated =  str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
print "include_m_t3__uqs = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/m_t3.uqs/")."\");}\r\n";
?>

uqs = {

   connect: function(pot_unid) {

      include_css(session.php_server + "/library/css5/te.tools/smartsearch.css");
      html =  
      "<span id=\"smartsearch\" pot_unid=\"" + pot_unid + "\">" + 
      "<div id=\"smartserach.head\" style=\"display:none;\">" +
      "<input class=\"readonly\" readonly=\"readonly\" type=\"text\" id=\"smartsearch_search\" value=\"\" onkeydown=\"this.setAttribute(\\'clock\\', Date.now());\" onkeyup=\"window.setTimeout(\\'smartsearch.check_do_filter()\\', 800);\" />" + 
      "<span>&nbsp;&nbsp;&nbsp;&nbsp;Results:&nbsp;<a id=\"smartsearch_results\"></a></span>" + 
      "</div>" + 
      "<div id=\"smartsearch.body\" clear=\"true\">Loading ...</div>" +
      "</span>";
      infobox("UQS<img src=\"../../../../library/images/16x16/white/edition-54.png\" style=\"position:relative;top:3px;width:12px;vertical-align:top; margin-left:6px;margin-right:6px;\">POT", html, 1100,700);
      url = session.remote_database_path + "modules/elements/m_t3.uqs/view.php";
      js = "document.getElementById('smartserach.head').style.display = 'block';" + 
      "do_smartsearch('smartsearch.body');" + 
      "r = document.getElementById('smartsearch.body').getElementsByTagName('p').length;" +
      "d = document.getElementById('smartsearch_results');" + 
      "d.setAttribute('count', r);d.innerHTML = r";
      run_ajax("smartsearch.body", url, js);
   },

   save: function(uqs_unid) {
      pot_unid = document.getElementById("smartsearch").getAttribute("pot_unid");
      js = "location.reload()";
      post.save_field("", pot_unid, "ref$uqs", uqs_unid, js);
   }

}
m_t3 = {
   change_status: function() {
      select = document.createElement("select");
      select.setAttribute("style", "width:280px; border:solid 1px rgb(222,222,222); font:normal 13px open sans; position:relative; top:3px;");
      select.setAttribute("onchange", "if(this.selectedIndex > 0) m_t3.save_status(this)");
      select.setAttribute("unid", arguments[0]);
      option = document.createElement("option");
      select.appendChild(option);
      for(k in arguments[1]) {
         if(in_array(k, arguments[2])) {
            option = document.createElement("option");
            option.textContent = arguments[1][k];
            option.setAttribute("value", k);
            select.appendChild(option);
         }
      }
      infobox("Change POT status", select.outerHTML, 300, 80);
   },

   save_status: function() {
      infobox_handle.close(true, "confirm_box");
      data = "&unid=" + escape(arguments[0].getAttribute("unid")) + "&run_agent=" + escape("a.set_date?form = \"f.date\" & ref$tracker = \"<?php session_start(); print $_SESSION["tracker"]; ?>\" & ref$pot = \"<?php session_start(); print $_SESSION["pot"]; ?>\" & @ismember(process_status; \"10\": \"20\" : \"30\")") + "&fld1=" + escape("pot_status/" + arguments[0][arguments[0].selectedIndex].value) + "&fld2=" + escape("pot_status_changed/<?php print date("Y-m-d H:m:s", mktime()); ?>");
      js = "location.reload()";
      post.save_data("f.save_data", "", data, js);
   }

}


