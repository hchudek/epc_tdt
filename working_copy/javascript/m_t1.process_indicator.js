<?php
session_start();
$path_translated =  str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
print "include_m_t1__process_indicator = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/m_t1.process_indicator/")."\");}\r\n";
print "include_activate_pot = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/")."\");}\r\n";
?>


m_t1__process_indicator = {

   container: {},

   superuser: {
      date: function() {
         d = document.getElementById('confirm_box_cal'); 
         d.parentNode.removeChild(d);
         eval(m_t1__process_indicator.container.getAttribute("do").replace(/true,/, "'" + arguments[0] + "\',"));
      },
      planned_date: 0,
   },

   textarea: function() {
      arr = [];
      ta = document.getElementById("m_t1__process_indicator").getElementsByTagName("textarea");
      for(k in ta) if(!isNaN(k)) {
         if(unescape(ta[k].getAttribute("responsible")) == session.domino_user_cn) {
            ta[k].setAttribute("maxlength", "99");
            ta[k].setAttribute("style", "font:normal 13px open sans; position:relative; left:4px; width:220px; height:159px; border:solid 1px rgb(66,66,66); opacity:0.92;");
            ta[k].onblur = function() {
               post.save_field(session.remote_domino_path_main, this.getAttribute("unid"), "remark", escape(this.value));
            }
         }
         else {
            arr.push(k);
         }
      }
      if(arr.length > 0) {
         arr.reverse();
         for(k in arr) {
            ta[arr[k]].outerHTML = "<div style=\"position:relative; left:26px; top:-19px; width:160px; margin-left:30px; white-space:normal; background:transparent;\">" + ta[arr[k]].textContent + "</div>";
         }
      }
   },

   set_status: "",

   responsibles: [],

   posmenu: [],

   loop: {
      filter: function(e) {
         p = session.php_server + "/library/images/16x16/white/";
         d = e.parentNode.getElementsByTagName("img");
         for(k in d) if(!isNaN(k)) {
            if(d[k] == e) {
               d[k].src = p + "status-11.png";
               v = k;
            }
            else {
               d[k].src = p + "status-11-1.png";
            }
         }
         document.getElementsByName("m_t1__process_indicator_loop_filter")[0].value = Number(v) + 1;
         do_fade(true, true);
         save_perpage("", true);
      },
   },


   dualsource: {
      filter: function(e) {
         p = session.php_server + "/library/images/16x16/white/";
         e.src = (e.src.indexOf("status-5-1") > -1) ? p + "status-5.png" : p + "status-5-1.png";
         arr = [];
         d = e.parentNode.getElementsByTagName("img");
         for(k in d) if(!isNaN(k)) {
            if(d[k].src.indexOf("status-5-1") == -1) arr.push(Number(k) + 1);
         }
         document.getElementsByName("m_t1__process_indicator_dualsource_filter")[0].value = arr.join(",");
         do_fade(true, true);
         save_perpage("", true);
      },
   },

   add_attribute: function() {
      d = document.getElementById("process_indicator").getElementsByTagName("tbody");
      for(k in d) if(!isNaN(k)) {
         attr = (d[k].getAttribute("id") == "tbl_m_t1__process_indicator_head") ? "h" : "c";
         plmc = (d[k].getAttribute("is_plmc") == "1") ? "_plmc" : "";
         td = d[k].getElementsByTagName("td");
         for(i = 0; i < td.length; i++) td[i].setAttribute(attr, String(i + 1) + plmc);
      }
   },

   change_search: function(e) {
      d = document.getElementsByName(e.getAttribute("name"));
      for(i = 0; i < d.length; i++) {
         src = (d[i] == e) ? "status-11" : "status-11-1"; 
         d[i].src = session.php_server + "/library/images/16x16/" + src + ".png";
         if(d[i] == e) document.getElementsByName("search_type")[0].value = String(i);
      }
      do_search_process_indicator(document.getElementsByName("search_string")[0].value.toLowerCase(), document.getElementsByName("search_string")[0].parentNode.parentNode.id);
   },

   change_responsible: function() {
      if(typeof arguments[0] == "object") {
         e = arguments[0];
         while(e.tagName.toLowerCase() != "tbody") e = e.parentNode;
         url = "addin/run_ajax.php?" + escape(session.remote_domino_path_main + "/v.get_responsibilities?open&count=9999&restricttocategory=" + escape(arguments[1]) + "&function=plain");
         js = "m_t1__process_indicator.change_responsible(document.getElementById('run_ajax').textContent.trim(), '" + e.getAttribute("unid") + "', '" + arguments[0].getAttribute("responsible") + "', '" + arguments[0].getAttribute("gate") + "')";
         run_ajax("run_ajax", url, js);
      }
      else {
         data = [];
         if(arguments[0].indexOf("No documents found") != -1) {
            infobox("Error", "<img src=\"../../../../library/images/16x16/leisure-17.png\" style=\"vertical-align:top; margin-right:4px; margin-top:1px;\">Configuration error", 180, 70);
         }
         else {
            eval(arguments[0]);
            m_t1__process_indicator.responsibles = data;
            input = document.createElement("input");
            input.setAttribute("value", arguments[2]);
            input.setAttribute("style", "padding-left:3px; width:242px; height:17px; border:solid 1px rgb(169,169,169); font:normal 13px Open Sans;");
            input.setAttribute("id", "m_t1__process_indicator_responsibles");
            html = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>Select new responsible:&nbsp;&nbsp;</td><td>" + input.outerHTML +"</td></tr></table>";      
            infobox("Change responsible for " + arguments[3], html, 420,300);
            window.setTimeout("m_t1__process_indicator.init_autocomplete('" + input.id + "', '" + arguments[1] + "')", 500);
         }
      }
   },


   init_autocomplete: function(id, unid) {
      $("#" + id).autocomplete({
         source: m_t1__process_indicator.responsibles,
         select: function(event, ui) {
            m_t1__process_indicator.save(unid, "responsible", ui.item.value.trim());
         },
      });

   },

   set_date: function() {
      today = "<?php print date("Y-m-d", mktime()); ?>";
      v = (typeof arguments[2] == "string") ? arguments[2] : today;
      if(arguments[4] == 99) {
         dif = Number(arguments[2].replace(/-/g, "")) - Number(m_t1__process_indicator.planned_date.replace(/-/g, ""));
         arguments[4] = (dif > 0) ? 30 : 20;
      }
      if(Number(arguments[4]) == 30) {
         select = document.createElement("select");
         select.setAttribute("onchange", "m_t1__process_indicator.set_date(\\'" + arguments[0] + "\\', \\'" + arguments[1] + "\\', \\'" + v + "/\\' + this[this.selectedIndex].text, \\'" + arguments[3] + "\\', \\'\\', \\'" + arguments[5] + "\\')");
         select.setAttribute("style", "min-width:200px; font:normal 13px Open Sans;");
         option = document.createElement("option");
         select.appendChild(option);   
         if(arguments[4] == "99") {
            option = document.createElement("option");
            option.textContent = "Corrected by superuser (" + session.domino_user_cn + ")";
            select.appendChild(option);
         }
         else {
            for(k in m_t1__process_indicator.reason_codes[arguments[3].toLowerCase().trim()]) {
               option = document.createElement("option");
               option.textContent = m_t1__process_indicator.reason_codes[arguments[3].toLowerCase().trim()][k];
               select.appendChild(option);
            }
         }
         html = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>Select&nbsp;reason&nbsp;code:&nbsp;&nbsp;</td><td>" + select.outerHTML + "</td></tr></table>";
         infobox("Select reason code for " + unescape(arguments[5]), html, 430,200);
      }
      else {
         if(document.getElementById("confirm_box")) infobox_handle.close(false, "confirm_box");
         j = "tc_data.meta = " + document.querySelector("[form='m_t1__process_indicator']").textContent;
         if(typeof tc_data != "object") tc_data = {}; eval(j);
         script = document.createElement("script");
         script.setAttribute("src", session.remote_database_path + "javascript/jload/recalc_process.js?process_rule=" + escape(tc_data.meta.process_rule));
         script.setAttribute("type", "text/javascript");
         script.setAttribute("onload", "recalc_process.start('" + arguments[0] + "', '" + arguments[1] + "', '" + v + "', '" + arguments[3] + "')");
         document.getElementsByTagName("head")[0].appendChild(script);
      }
   },

   activate: function() {
      do_fade(true, true);
      data = "&unid=" + escape(arguments[0]) + "&fld1=" + escape("wf$is_active/" + arguments[1]);
      js = "location.reload()";
      post.save_data("f.save_data", '', data, js);
   },

   save: function() {
      js = "infobox_handle.close(false); location.reload()";
      post.save_field("", arguments[0], arguments[1], arguments[2], js);
   },



   reason_codes: {
<?php
$include = str_replace("\\", "/", substr($_SERVER["PATH_TRANSLATED"], 0, strpos($_SERVER["PATH_TRANSLATED"], "javascript")))."modules/Elements/PROCESS/subsprocesses.php";
include_once($include);
foreach($_application["process"]["reason_codes"] as $key => $val) {
   print "      ".strtolower($key).": [\"".str_replace(":", "\", \"", $val)."\"],\r\n";
}
?>
   },

   nest: function() {
      d = document.getElementsByClassName("menuwrapper");
      for(k in d) if(!isNaN(k)) {
         position = $(d[k]).position();
         m_t1__process_indicator.posmenu[k] = position.top;
      }
      $(document.getElementById("m_t1__process_indicator").getElementsByClassName("inner_box")[0]).scroll(function() {
         s = $(this).scrollTop() * -1;
         d = document.getElementsByClassName("menuwrapper");
         for(k in d) if(!isNaN(k)) {
            d[k].style.top = String(m_t1__process_indicator.posmenu[k] + s) + "px";
            d[k].style.display = (m_t1__process_indicator.posmenu[k] + s < 31) ? "none" : "block";
         }
         if(document.getElementById("process_indicator")) {
            d = document.querySelectorAll("[form='remark']");
            for(k in d) if(!isNaN(k)) {
               d[k].style.top = String(m_t1__process_indicator.posmenu[k] + s + 6) + "px";
            }
         }         
      });
   },
}


create_loop = {
   init: function (phases, tracker, pot, num, loop, source) {
      do_fade(true, true);
      arr = phases.split("/");
      if(typeof arr[num] != "undefined") {
         js = "create_loop.init('" + phases + "', '" + tracker + "', '" + pot + "', " + (num + 1) + ", " + String(loop) + ", " + String(source) + ")";
         create_loop.execute(arr[num], tracker, pot, js, loop, source, 'a.create_new_loop');
      }
      else location.href = location.href;
   },

   delete: function (phases, tracker, pot, num, loop, source) {
      process = (typeof arguments[6] == "undefined") ? 0 : arguments[6];
      switch(process) {
         case 0: 
            html = "<span id=\"___b\">" +
            "<div style=\"margin-bottom:11px;\">Deleting a loop might damage the datastructure (e. g. if a control report is linked to the loop).</div>" + 
            "<span class=\"phpbutton\"><a href=\"javascript:create_loop.delete('" + phases + "', '" + tracker + "', '" + pot + "', " + num + ", " + loop + ", " + source + ", 1);\">Continue</a></span>" + 
            "<span class=\"phpbutton\"><a href=\"javascript:infobox_handle.close(true, 'confirm_box');\">Cancel</a></span>" + 
            "</span>";  
            infobox("<span id=\"___h\">Are you sure?</span>", html, 320, 120);
            break;
         case 1:
            document.getElementById("___h").innerHTML = "Processing ...";
            document.getElementById("___b").innerHTML = "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"margin-right:5px; position:relative; top:3px;\">Processing ...";
            js = "location.reload()";
            create_loop.execute(phases, tracker, pot, js, (loop - 1), source, 'a.delete_loop');
            break;
      }
   },

   execute: function(phase, tracker, pot, js, loop, source, agent) {
      url = "addin/run_agent.php?agent=" + agent + "&tracker=" + tracker + "&pot=" + pot + "&phase=" + phase + "&loop=" + String(loop) + "&source=" + String(source);
      run_ajax("run_ajax", url, js);
   }
}


pick_date = {

   execute: "save_calendar_date",

   run_ajax: function(_id,_file,_runJS) {
      _post = (typeof arguments[3] == "undefined") ? null : arguments[3]; 
      req = null;
      try { req = new XMLHttpRequest(); }
      catch (ms) {
         try { req = new ActiveXObject("Msxml2.XMLHTTP"); }
         finally {}
      }
      if (req == null) eval("document.getElementById(_id).innerHTML='error creating request object';");
      else req.open("post",_file, true);
      req.onreadystatechange = function() {
         switch(req.readyState) {
            case 4:
              document.getElementById(_id).innerHTML = req.responseText;    
              if(_runJS != "undefined") eval(_runJS); 
               break;
            default:
               return false; 
               break;
         }
      }
      req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      req.send(_post);
   },

   save_calendar_date: function(unid, f) {
      key = arguments[3];
      ref_meta = "<?php session_start(); print $_SESSION["meta"]["unid"]; ?>";
      v = (arguments[2].indexOf("/") > -1) ? arguments[2].split("/") : [arguments[2], "-"];
      data = "&ref_tracker=" + escape('<?php session_start(); print $_SESSION["tracker"]; ?>') + 
      "&ref_meta=" + escape(ref_meta) + 
      "&ref_pot=" + escape('<?php session_start(); print $_SESSION["pot"]; ?>') + 
      "&run_agent=" + escape("a.recalc_process_v2") + 
      "&run_agent_unid=" + escape(unid) + 
      "&key=" + escape(key) + 
      "&type=" + escape("actual") + 
      "&fld1=" + escape(unid + ";" + f + ";" + v[0]) +
      "&fld2=" + escape(unid + ";reason_code_actual;" + v[1]) +
      "&fld3=" + escape(unid + ";do_recalc;true") +
      "&fld4=" + escape(unid + ";ref$meta;" + ref_meta);
      if(m_t1__process_indicator.set_status != "") {
         data += 
         "&fld5=" + escape("<?php session_start(); print $_SESSION["pot:unid"]; ?>;pot_status;" + m_t1__process_indicator.set_status + "@<?php session_start(); print $_SESSION["tracker"]; ?>;true") +
         "&fld6=" + escape("<?php session_start(); print $_SESSION["pot:unid"]; ?>;pot_status_changed;<?php print date("Y-m-d", mktime()); ?>;true");
      }
      url = "addin/save_multiple_fields.php?form=f.save_process_indicator";
      js = "pick_date.run_ajax('run_ajax', 'addin/run_ajax.php?" + escape(session.remote_domino_path_main + "/a.recalc_process_v2?open&unid=" + unid) + "', 'location.reload()')";
      infobox("Recalculating", "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"margin-right:7px; vertical-align:top;\"><span style=\"position:relative; top:-2px;\">Processing ...</span>", 240, 68);
      pick_date.run_ajax("run_ajax", url, js, data);  

   },

   set_v1_date: function(unid, f, v) {

      document.getElementById("confirm_box_cal").innerHTML = "";
      data = "&unid=" + escape(unid)+ "&fld1=" + escape(unid + ";" + f + ";" + v);
      js = "location.reload()";
      post.save_data("f.save_process_indicator", "", data, js);
   },

   set_v1_date_rule: function(unid, f, v) {
      include_css(session.php_server + "/library/css5/resource.db/add_pick_date_result.css");

      d = document.getElementById("calendar_year_nested");
      r = document.getElementsByTagName("rule");
      tbl = document.createElement("table");
      tbl.setAttribute("border", "0");
      tbl.setAttribute("cellpadding", "1");
      tbl.setAttribute("cellspacing", "0");
      tbl.setAttribute("id", "pick_date_result");
      tr = document.createElement("tr");
      th = document.createElement("th");
      th.innerText = "Process";
      tr.appendChild(th);
      th = document.createElement("th");
      th.innerText = "Previous Process";
      tr.appendChild(th);
      th = document.createElement("th");
      th.innerText = "Old date";
      tr.appendChild(th);
      th = document.createElement("th");
      th.innerText = "Calculated date";
      tr.appendChild(th);
      th = document.createElement("th");
      th.innerText = "Rule";
      tr.appendChild(th);
      tbl.appendChild(tr);

      flag = false;
      data = "";
      fld = 0;

      for(k in r) if(!isNaN(k)) {
         if(r[k].getAttribute("loop") == pick_date.loop || (r[k].getAttribute("rid") != "G04070" && r[k].getAttribute("rid") != "G04080" && r[k].getAttribute("rid") != "G04090")) {
            if(r[k].getAttribute("documentunid") == unid) {
               flag = true;            
               operation = "Selected";
               new_date = v;
            }
            else {
               operation = r[k].getAttribute("operation");
               if(flag) {
                  if(r[k].getAttribute("status") == "40") {
                     new_date = r[k].textContent;
                  }
                  else {
                     new_date = (typeof pick_date["recalc_" + r[k].getAttribute("from_rid")] == "string") ? pick_date["recalc_" + r[k].getAttribute("from_rid")] : r[k].textContent;
                     if(Number(operation.replace("+","")) > 0) new_date = pick_date.calculate(new_date.split("-"), operation.replace("+","")).join("-");
                  }
               }
               else {
                  new_date = r[k].textContent;
               }
            }
            c = (flag && r[k].getAttribute("status") != "40") ? "rgb(0,0,0)" : "rgb(152,153,153)";
            if(flag && (new_date != r[k].textContent || r[k].getAttribute("documentunid") == unid)) {
               fld++;
               data += "&fld" + String(fld) + "=" + escape(r[k].getAttribute("documentunid") + ";date_revised;" + new_date);
            }
            pick_date["recalc_" + r[k].getAttribute("rid")] = new_date;
            tr = document.createElement("tr");
            td = document.createElement("td");
            td.setAttribute("style", "color:" + c);
            td.textContent = pick_date[r[k].getAttribute("rid")];
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("style", "color:" + c);
            td.textContent = pick_date[r[k].getAttribute("from_rid")];
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("style", "color:" + c);
            td.textContent = r[k].textContent;
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("style", "color:" + c);
            td.textContent = new_date;
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("style", "color:" + c);
            td.textContent = operation;
            tr.appendChild(td);
            tbl.appendChild(tr);
         }
      }
      d.innerHTML = tbl.outerHTML;

      d = document.getElementById("calendar_year_pick_date");
      d.innerHTML = 
      "<div style=\"width:170px; margin-top:10px;\">" +
      "<span class=\"phpbutton\"><a href=\"javascript:document.getElementById('pick_date_result').style.opacity='0.1'; d.parentNode.removeChild(d);\" onclick=\"post.save_data('f.save_process_indicator', '', '" + data + "', 'location.reload()');\">Apply</a></span>" + 
      "<span class=\"phpbutton\"><a href=\"javascript:document.getElementById('pick_date_result').style.opacity='0.1'; location.href=location.href\">Cancel</a></span>" +     
      "</div>";
   },

   calculate: function() {
      calc_t_date_parse = Date.parse(arguments[0].join("-"));
      for(i = 0; i < Number(arguments[1]); i++) {
         calc_t_date_parse += 86400000;
         calc_t_date = new Date(calc_t_date_parse);
         if(calc_t_date.getDay() == 1 || calc_t_date.getDay() == 6) i--;
      }  
      return [String(calc_t_date.getFullYear()), ("0" + String(calc_t_date.getMonth() + 1)).substr(-2), ("0" + String(calc_t_date.getDate())).substr(-2)];
   },

   get_rule: function() {
      rule = document.getElementsByTagName("rule");
      ret = false;
      for(rr = 0; rr < rule.length; rr++) {
         if(rule[rr].getAttribute(arguments[0]) == arguments[1]) {
            ret = rule[rr].textContent;
            break;
         }
      }
      return ret;
   },


<?php
include_once("../modules/Elements/PROCESS/subsprocesses.php");

foreach($_application["process"]["subprocess"] as $key => $val) {
   print "   ".$key.": \"".substr($val, 0, strpos($val, ":"))."\",\r\n";
}
?>

}






function save_process_indicator() {
   save = (typeof arguments[0] == "undefined") ? "" : arguments[0];
   arr = new Array;
   d = document.getElementById("process_indicator").getElementsByClassName("date_process");
   c = 0;
   for(i = 0; i < d.length; i++) {
      unid = d[i].getAttribute("unid");
      f = d[i].getElementsByTagName("div");
      for(j = 0; j < f.length; j++) {
         c++;
alert(f[j].getAttribute("id"));
         nme = f[j].getAttribute("id").substr(f[j].getAttribute("id").indexOf("_") + 1, f[j].getAttribute("id").length);

         arr.push("fld" + String(c) + "=" + escape(unid + ";" + nme + ";" + "2011-01-01"));
      }
   }
   url = "addin/save_multiple_fields.php?form=f.save_process_indicator";
   run_ajax("run_ajax", url, "", arr.join("&"));  

alert(arr.join("&"));
}


function save_date() {
   strng = "fld1=" + escape(arguments[0] + ";" + arguments[1] + ";" + arguments[2]);
   url = "addin/save_multiple_fields.php?form=f.save_process_indicator";
   run_ajax("run_ajax", url, "d=document.getElementById('confirm_box');if(d) d.parentNode.removeChild(d);location.reload();", strng);  


}


function do_search_process_indicator(strng, id) {
   arr = (strng.trim()).split(" ");
   search_type = Number(document.getElementsByName("search_type")[0].value);
   d = document.getElementById(id).getElementsByTagName("tbody");
   for(i = 0; i < d.length; i++) {
      found = true;
      key = (d[i].getAttribute("key") == null) ? "" : d[i].getAttribute("key");
      if(key != "") {
         for(e in arr) {
            if(key.indexOf(arr[e]) == -1) { 
               found = false;
               break;
            }
         }
         if(search_type == 1) found = (found) ? false : true;
         dsp = (found || strng.length == 0) ? "" : "none";
         d[i].setAttribute("style", "display:" + dsp);
      }
   }
}


function edit_dates() {
   do_fade(true);
   div = document.createElement("div");
   div.setAttribute("id", "edit_dates");
   div.setAttribute("init", "true");
   document.getElementsByTagName("body")[0].appendChild(div);
   p_left = 30;
   p_top = 30;
   p_width = window.innerWidth - 70;
   p_height = window.innerHeight - 100;
   div.setAttribute("style", "left:" + p_left + "px;top:" + p_top + "px;width:" + p_width + "px;height:" + p_height + "px;");
   h = document.createElement("div");
   h.setAttribute("class", "header");
   h.textContent = "POT";
   div.appendChild(h);
   b = document.createElement("div");
   b.innerHTML = "<div class=\"body\">" + 
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >" + 
   "<tr>" + 
   "<td style=\"vertical-align:top;height:60px;width:290px;text-align:left;padding-right:10px;\" id=\"calendar_year\"></td>" +
   "<td rowspan=\"2\" id=\"calendar_big\" style=\"vertical-align:top;padding-left:15px;padding-top:11px;\"><img style=\"position:absolute;left:" + ((window.innerWidth / 2)- 90) + "px;top:" + ((window.innerHeight / 2)- 108) + "px;\" src=\"" + session.php_server + "/library/images/ajax/ajax-loader.gif\" /></td>" +
   "</tr>" +
   "<tr>" + 
   "<td style=\"vertical-align:top;height:99%;\" id=\"calendar_year_dates\">" + 
   "<td>" + 
   "</table>" + 
   "</div>";
   div.appendChild(b);

   if(!document.getElementById("m_t1.edit_dates.css")) {
      link = document.createElement("link");
      link.setAttribute("id", "m_t1.edit_dates.css");
      link.setAttribute("type", "text/css");
      link.setAttribute("rel", "stylesheet");
      link.setAttribute("href", session.php_server + "/library/css5/epc.m_t1.edit_dates.css");
      document.getElementsByTagName("head")[0].appendChild(link);
   }
   if(!document.getElementById("m_t1.edit_dates.js")) {
      script = document.createElement("script");
      script.setAttribute("id", "m_t1.edit_dates.js");
      script.setAttribute("type", "text/javascript");
      script.setAttribute("src", session.remote_database_path + "/javascript/m_t1.edit_dates.js");
      script.setAttribute("onload", "document.getElementById('calendar_big').innerHTML = '';embed_page(<?php print date("Y", mktime());?>);");
      window.setTimeout('document.getElementsByTagName("head")[0].appendChild(script)', 1);
   }
}


function embed_history(tpe, unid) {
   url= "addin/run_ajax.php?" + escape(session.remote_domino_path_main + "/v.t1_history?open&type=" + tpe + "&count=1&restricttocategory=" + unid + "&function=plain");
   js = "create_changeable_protocol(false)";
   do_fade(true);
   run_ajax("run_ajax", url, js);
}


function create_changeable_protocol() {
   if(arguments[0] == false) {
      script = document.createElement("script");
      script.setAttribute("src", "modules/elements/process/get_rc.js");
      script.setAttribute("id", "get_rc.js");
      script.setAttribute("type", "text/javascript");
      script.setAttribute("onload", "create_changeable_protocol(true)");
      document.getElementsByTagName("head")[0].appendChild(script);
   }
   else {
      html = document.getElementById('run_ajax').innerHTML.trim();
      for(key in rc) {
         s = "<select onchange=\"url = session.remote_domino_path_main + '/a.change_rc?open&unid=' + this.parentNode.getAttribute('unid') + '&element=' + this.parentNode.getAttribute('element') + '&value=' + escape(this[this.selectedIndex].text); run_ajax('run_ajax', url, '')\">";
         arr = unescape(rc[key]).split(":");
         for(o in arr) {
            selected = (arr[o].toLowerCase() == "no reason code") ? " selected" : "";
            s += "<option" + selected + ">" + arr[o] + "</option>";
         }
         s += "</select>"; 
         while(html.indexOf("select_" + key) != -1) html = html.replace("select_" + key, s);
      }
      infobox("Protocol", html, 600, 300);
   }
}



