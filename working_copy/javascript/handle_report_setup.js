function save_config () {
   save = new container(
   "form => f.configuration",
   "complexity_a => " + document.getElementsByName("complexity_a")[0].value,
   "complexity_b => " + document.getElementsByName("complexity_b")[0].value,
   "complexity_c => " + document.getElementsByName("complexity_c")[0].value,
   "complexity_d => " + document.getElementsByName("complexity_d")[0].value,
   "complexity_e => " + document.getElementsByName("complexity_e")[0].value,
   "complexity_f => " + document.getElementsByName("complexity_f")[0].value,
   "complexity_g => " + document.getElementsByName("complexity_g")[0].value,

   "conkit_type_of_tool => " + document.getElementsByName("conkit_type_of_tool")[0].value.replace(",", "."),
   "repl_type_of_tool => " + document.getElementsByName("repl_type_of_tool")[0].value.replace(",", "."),
   "modif_type_of_tool => " + document.getElementsByName("modif_type_of_tool")[0].value.replace(",", "."),
   "newprj_type_of_tool => " + document.getElementsByName("newprj_type_of_tool")[0].value.replace(",", "."),
   "pilot_type_of_tool => " + document.getElementsByName("pilot_type_of_tool")[0].value.replace(",", "."),
   "purch_type_of_tool => " + document.getElementsByName("purch_type_of_tool")[0].value.replace(",", "."),
   "packaging_type_of_tool => " + document.getElementsByName("packaging_type_of_tool")[0].value.replace(",", "."),
   
   "assyeasy_kind_of_project => " + document.getElementsByName("assyeasy_kind_of_project")[0].value.replace(",", "."),
   "assymid_kind_of_project => " + document.getElementsByName("assymid_kind_of_project")[0].value.replace(",", "."),
   "assydiff_kind_of_project => " + document.getElementsByName("assydiff_kind_of_project")[0].value.replace(",", "."),
   "moldeasy_kind_of_project => " + document.getElementsByName("moldeasy_kind_of_project")[0].value.replace(",", "."),
   "moldmid_kind_of_project => " + document.getElementsByName("moldmid_kind_of_project")[0].value.replace(",", "."),
   "molddiff_kind_of_project => " + document.getElementsByName("molddiff_kind_of_project")[0].value.replace(",", "."),
   "packaging_kind_of_project => " + document.getElementsByName("packaging_kind_of_project")[0].value.replace(",", "."),
   "stampeasy_kind_of_project => " + document.getElementsByName("stampeasy_kind_of_project")[0].value.replace(",", "."),
   "stampmid_kind_of_project => " + document.getElementsByName("stampmid_kind_of_project")[0].value.replace(",", "."),
   "stampdiff_kind_of_project => " + document.getElementsByName("stampdiff_kind_of_project")[0].value.replace(",", "."),
   "modif_kind_of_project => " + document.getElementsByName("modif_kind_of_project")[0].value.replace(",", "."),
   "plating_kind_of_project => " + document.getElementsByName("plating_kind_of_project")[0].value.replace(",", "."),
   "purch_kind_of_project => " + document.getElementsByName("purch_kind_of_project")[0].value.replace(",", "."),

   "convkit_hour => " + document.getElementsByName("convkit_hour")[0].value.replace(",", "."),
   "repl_hour => " + document.getElementsByName("convkit_hour")[0].value.replace(",", "."),
   "modif_hour => " + document.getElementsByName("convkit_hour")[0].value.replace(",", "."),
   "newprj_hour => " + document.getElementsByName("convkit_hour")[0].value.replace(",", "."),
   "pilot_hour => " + document.getElementsByName("pilot_hour")[0].value.replace(",", "."),
   "purch_hour => " + document.getElementsByName("purch_hour")[0].value.replace(",", "."),
   "packaging_hour => " + document.getElementsByName("packaging_hour")[0].value.replace(",", "."),
   "convkit_week => " + document.getElementsByName("convkit_week")[0].value.replace(",", "."),
   "repl_week => " + document.getElementsByName("convkit_week")[0].value.replace(",", "."),
   "modif_week => " + document.getElementsByName("convkit_week")[0].value.replace(",", "."),
   "newprj_week => " + document.getElementsByName("convkit_week")[0].value.replace(",", "."),
   "pilot_week => " + document.getElementsByName("pilot_week")[0].value.replace(",", "."),
   "purch_week => " + document.getElementsByName("purch_week")[0].value.replace(",", "."),
   "packaging_week => " + document.getElementsByName("packaging_week")[0].value.replace(",", "."),

   "source_emea => " + document.getElementsByName("source_emea")[0].value.replace(",", "."),
   "source_global => " + document.getElementsByName("source_global")[0].value.replace(",", "."),
   "source_local => " + document.getElementsByName("source_local")[0].value.replace(",", "."),
   "source_dual => " + document.getElementsByName("source_dual")[0].value.replace(",", "."),

   "fix_start => " + document.getElementsByName("fix_start")[0].value.replace(",", "."),   
   "fix_end => " + document.getElementsByName("fix_end")[0].value.replace(",", "."),
   "fix_default => " + document.getElementsByName("fix_default")[0].value.replace(",", "."),
   "fix_grid => " + document.getElementsByName("fix_grid")[0].value.replace(",", "."),
   "var_start => " + document.getElementsByName("var_start")[0].value.replace(",", "."),
   "var_end => " + document.getElementsByName("var_end")[0].value.replace(",", "."),
   "var_default => " + document.getElementsByName("var_default")[0].value.replace(",", "."),
   "var_grid => " + document.getElementsByName("var_grid")[0].value.replace(",", ".")
   );

   save_data(session.report_setup, "location.href=location.href", 0)

}


