<?php
$unique = $_SESSION["tracker"];
$pot = $_SESSION["pot"];
$path_translated =  str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
$params = "var dt = new Date().getMilliseconds() + ''; var args = { 'load': 'include_m_t14__tree', 'nocache': dt, 'unique': '$unique', 'pot': '$pot'  };\r\n";
print "include_m_t14__tree = function() {".$params."_(args, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/m_t14.tree/")."\", 'include_m_t14__tree');}\r\n";
?>

var m_t14 = {};
m_t14.handle_save_single_field_tree = function (unid, field, value, postsave) {
	t = (typeof arguments[4] == "undefined") ? true : arguments[4];
	if (t) {
		data = save = new container(field + " => " + value);
		m_t14.save_data_tree(unid, postsave, 0);
	}
}
m_t14.save_tree = function (tracker_unid) {
	tree = treeObj.getNodeOrders().replace("1-0", "1-null");
	m_t14.handle_save_single_field_tree(tracker_unid, "tracker_tree", tree,
			"m_t14.reloadTree()");
}
m_t14.add_to_tree = function (tracker_unid, add_part) {
	tree = treeObj.getNodeOrders().replace("1-0", "1-null") + "," + add_part;
	m_t14.handle_save_single_field_tree(tracker_unid, "tracker_tree", tree,
			"m_t14.reloadTree()");
}
m_t14.reset_tree = function(tracker_unid) {
	tree = "";
	m_t14.handle_save_single_field_tree(tracker_unid, "tracker_tree", tree,
			"m_t14.reset_added_dummy_pot(\\'" + tracker_unid + "\\')");
}
m_t14.reset_added_dummy_pot = function(tracker_unid) {
	val = "";
	m_t14.handle_save_single_field_tree(tracker_unid, "tracker_add_pot", val,
			"m_t14.reloadTree()");
}
m_t14.add_dummy_pot = function (tracker_unid) {
	if (document.getElementsByName("add_pot")[0].value != "") {
		val = (document.getElementById("added_pot").innerText + ":" + document
				.getElementsByName("add_pot")[0].value);
		m_t14.handle_save_single_field_tree(tracker_unid, "tracker_add_pot", val,
				"m_t14.reloadTree()");
	}
}
m_t14.save_data_tree = function (unid, postsave, num) {
	p = new Array;
	for (property in save)
		p.push(property);
	if (p.length > num) {
		url = session.remote_domino_path_main + "/a.save_data?open&unid="
				+ unid + "&f=" + escape(p[num]) + "&v=" + escape(save[p[num]]);
		js = "m_t14.save_data_tree('" + unid + "', '" + postsave + "', " + (num + 1) + ")";
		m_t14.run_ajax_tree("m_t14.run_ajax_tree", url, js, true);
	} else
		eval(unescape(postsave));
}

m_t14.run_ajax_tree = function (_id, _file, _runJS, _usePHP) {

	_usePHP = (typeof arguments[3] == "undefined") ? false : arguments[3];
	if (_usePHP)
		_file = session.remote_database_path + "addin/get_html.php?"
				+ base64_encode(_file);
	req = null;
	try {
		req = new XMLHttpRequest();
	} catch (ms) {
		try {
			req = new ActiveXObject("Msxml2.XMLHTTP");
		} finally {
			
		}
	}
	if (req == null)
		eval("document.getElementById(_id).innerHTML='error creating request object';");
	else
		req.open("post", _file, true);
		req.onreadystatechange = function() {

		switch (req.readyState) {
		case 4:
			m_t14.reloadTree();
			break;
		default:
			return false;
			break;
		}	
	};
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}
m_t14.initJSTree = function (){
	treeObj = new JSDragDropTree();
	treeObj.setTreeId('dhtmlgoodies_tree2');
    treeObj.setMaximumDepth(10);
    treeObj.setMessageMaximumDepthReached('Maximum depth reached');
    treeObj.initTree();
    treeObj.expandAll();
}
   
m_t14.reloadTree = function(){
   	$.get( session.remote_database_path + "/modules/m_t14.tree-reload.php", {sort:"", page:""}, function(result){
   		$("#m_t14__tree_innerhtml").html(result);
   		m_t14.initJSTree();
  	}, "html");
}


