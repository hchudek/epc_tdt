smartselect = {
   populate: function() {
      arr = [];
      sel = document.querySelectorAll("[form='" + arguments[0].getAttribute("form") + "']");
      for(k in sel) if(!isNaN(k)) {
         option = sel[k].getElementsByTagName("option");
         for(o in option) if(!isNaN(o) && o > 0) {
            if(option[o].textContent.trim() != "") {
               if(!in_array(option[o].textContent, arr)) arr.push(option[o].textContent);
            }
         }
      }

      if(arr.length > 0) {
         arr.sort();
         sm = document.getElementById("smartselect");
         smartselect.nest_search(sm);      
         for(k in arr) {
            href = document.createElement("a");
            href.setAttribute("href", "javascript:void(0);");
            href.setAttribute("useform", arguments[0].getAttribute("form"));
            href.setAttribute("k", arr[k]);
            href.setAttribute("key", arr[k].toLowerCase());
            href.textContent = arr[k];
            href.onclick = function() {
               smartselect.do(this.getAttribute("useform"), this.getAttribute("k"));
            }
            href.onmouseover = function() {
               sel = document.querySelectorAll("[form='" + this.getAttribute("useform") + "']");
               for(k in sel) if(!isNaN(k)) {
                  highlight = false;
                  option = sel[k].getElementsByTagName("option");
                  for(o in option) if(!isNaN(o)) {
                     if(option[o].textContent == this.textContent) {
                        highlight = true;
                        break;
                     }
                  }
                  sel[k].style.background = (highlight) ? "rgb(214,227,66)" : "rgb(255,255,255)";
               }
            }
            href.onmouseout = function() {
               sel = document.querySelectorAll("[form='" + this.getAttribute("useform") + "']");
               for(k in sel) if(!isNaN(k)) sel[k].style.background = "rgb(255,255,255)";
            }
            sm.appendChild(href);
         }
      }
   },

   do: function() {
      sel = document.querySelectorAll("[form='" + arguments[0] + "']");
      for(k in sel) if(!isNaN(k)) {
         option = sel[k].getElementsByTagName("option");
         for(o in option) if(!isNaN(o)) {
            if(option[o].textContent == arguments[1]) {
               sel[k].selectedIndex = o;
               option[o].parentNode.onchange();
            }
         }
      }
   },
  
   nest_search: function() {
      input = document.createElement("input");
      input.onkeypress = function(event) {
         arr = [" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
         str = String.fromCharCode(event.charCode).toLowerCase();
         if(!in_array(str, arr)) return false;
      },
      input.onkeyup = function() {
         v = (this.value.trim()).split(" ");
         h = document.getElementById("smartselect").getElementsByTagName("a");
         for(k in h) if(!isNaN(k)) { 
            dsp = true;
            innerhtml = h[k].textContent;
            for(n in v) if(!isNaN(n) && v[n].trim() != "") {
               if(h[k].getAttribute("key").indexOf(v[n].toLowerCase()) == -1) {
                  dsp = false;
                  break;
               }
               else {
                  re = new RegExp(v[n], "g");
                  if(v[n].toLowerCase() == "b") {innerhtml = innerhtml.replace(/\<b\>/g, "<_>").replace(/\<\/b\>/g, "<__>");}
                  innerhtml = innerhtml.replace(new RegExp(v[n], "gi"), "<b>$&</b>");
                  if(v[n].toLowerCase() == "b") innerhtml = innerhtml.replace(/\<_\>/g, "<b>").replace(/\<__\>/g, "</b>");

               }
            }
            h[k].style.display = (dsp) ? "block" : "none";
            h[k].innerHTML = innerhtml;
         }
      }
      el = (typeof arguments[0] == "object") ? arguments[0] : document.getElementById("smartselect");
      while(el.firstChild) el.removeChild(el.firstChild);
      el.appendChild(input);
   },
}