date = {
   getIsoDay: function() {
      return parseInt(String(new Date(arguments[0][0], arguments[0][1] - 1, arguments[0][2]).getDay()).replace(/0/, "7"));
   },
   daysInMonth: function() {
      return new Date(arguments[0][0], arguments[0][1], 0).getDate();
   },
   timestamp: function() {
      return new Date(arguments[0][0], arguments[0][1] - 1, arguments[0][2]).getTime();
   },

   today: new Date(<?php print strtotime(date("Y-m-d", mktime())) * 1000; ?>),
}


gadget = {

   calendar: {

      styles: {
         setcolor: "rgb(239,228,176);",
         active_button: "opacity:1.0; cursor:pointer;",
         inactive_button: "opacity:0.5; cursor:default;",
      },
 
      init: false,

      pointer: [date.today.getFullYear(), date.today.getMonth() + 1, 1],

      month: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      weekday: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],

      nest: function() {

         if(!gadget.calendar.init) {
            for(k in gadget.calendar.elements) gadget.calendar.elements[k][2] = gadget.calendar.elements[k][1];
         }

         gadget.calendar.id = arguments[0];
         calendar = document.getElementById(gadget.calendar.id);
   
         include = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td style=\"vertical-align:top; padding-right:14px; position:relative; top:12px;\">" +
         "<a href=\"javascript:gadget.calendar.prvmonth();\"><img src=\"../../../../library/images/16x16/chevron-big-3-01.png\" style=\"position:relative; top:2px; margin-left:20px;\"></a>" + 
         "<span gadget=\"calendar.dateinfo\">" + gadget.calendar.month[gadget.calendar.pointer[1] - 1] + " " + gadget.calendar.pointer[0] + "</span>" +
         "<a href=\"javascript:gadget.calendar.nextmonth();\"><img src=\"../../../../library/images/16x16/chevron-big-4-01.png\" style=\"position:relative; top:2px;\"></a>" +
         "<div style=\"height:22px;\">&nbsp;</div>";
         for(k in gadget.calendar.elements) {
            add_style = (gadget.calendar.elements[k][1].trim() != "" && gadget.calendar.elements[k][1] != gadget.calendar.elements[k][2]) ? "background:" + gadget.calendar.styles.setcolor : "";
            add_style_button = (gadget.calendar.elements[k][3]) ? "opacity:1.0" : "opacity:0.5";
            add_style_img = [];
            add_style_img[0] = (gadget.calendar.elements[k][3] && gadget.calendar.elements[k][1].trim() != "") ? gadget.calendar.styles.active_button : gadget.calendar.styles.inactive_button;
            add_style_img[1] = add_style_img[0];
            add_style_img[2] = (gadget.calendar.elements[k][1].trim() != "" && gadget.calendar.elements[k][3]) ? gadget.calendar.styles.active_button : gadget.calendar.styles.inactive_button;
            add_style_img[3] = (gadget.calendar.elements[k][1].trim() != "" && gadget.calendar.elements[k][3] && typeof gadget.calendar.elements[k][4] == "object") ? gadget.calendar.styles.active_button : gadget.calendar.styles.inactive_button;
            include += "<span gadget=\"calendar.elements\">" + 
            "<div style=\"" + add_style_button + "\" id=\"gadget.calendar.elements." + k + "\" draggable=\"" + String(gadget.calendar.elements[k][3]) + "\" ondragstart=\"gadget.calendar.drag(event);\">" + gadget.calendar.elements[k][0] + "</div>" +
            "<input style=\"" + add_style + "\" readonly=\"readonly\" type=\"text\" value=\"" + gadget.calendar.elements[k][1] + "\" style=\"margin-left:2px;\">" +
            "<a href=\"javascript:void(0);\" style=\"" + add_style_img[0] +"\" onclick=\"if(Number(this.style.opacity) == 1) gadget.calendar.changeday(this, -1);\"><img src=\"../../../../library/images/16x16/interface-79.png\"></a>" +
            "<a href=\"javascript:void(0);\" style=\"" + add_style_img[1] + "\" onclick=\"if(Number(this.style.opacity) == 1) gadget.calendar.changeday(this, +1);\"><img src=\"../../../../library/images/16x16/interface-78.png\"></a>" +
            "<a href=\"javascript:void(0);\" style=\"" + add_style_img[2] + "\" onclick=\"if(Number(this.style.opacity) == 1) gadget.calendar.delete(this, '" + k + "');\"><img src=\"../../../../library/images/16x16/edition-44.png\"></a>" +
            "<a href=\"javascript:void(0);\" style=\"" + add_style_img[3] + "\" onclick=\"if(Number(this.style.opacity) == 1) gadget.calendar.userule(this, '" + k + "');\"><img src=\"../../../../library/images/16x16/setting-7.png\"></a>" +
            "</span>";
         }
         include += "<br><span class=\"phpbutton\"><a href=\"javascript:gadget.calendar.submit();\">Submit</a></span>" + 
         "</td><td gadget=\"calendar\" calendar=\"" + gadget.calendar.pointer.join("-") + "\"></td></tr></table>";

         calendar.innerHTML = include;

         table = document.createElement("table");
         firstweekdayofmonth = date.getIsoDay(gadget.calendar.pointer) - 1;
         timestamp = date.timestamp(gadget.calendar.pointer) - (firstweekdayofmonth * 86400000) + 80000000;
         daysinmonth = date.daysInMonth(gadget.calendar.pointer);
         for(r = 0; r < 7; r++) {
            tr = document.createElement("tr");
            for(d = 0; d < 7; d++) {
               td = document.createElement("td");
               is_date = [new Date(timestamp).getFullYear(), ("0" + String(new Date(timestamp).getMonth() + 1)).substr(-2), ("0" + String(new Date(timestamp).getDate())).substr(-2)];
               if(String(new Date(timestamp).getMonth()) == new Date(date.timestamp(gadget.calendar.pointer)).getMonth()) {
                  add_style = ["border:solid 1px rgb(99,99,99);", "border:solid 1px rgb(99,99,99);"];
                  add_drop = "gadget.calendar.drop(event, this)";
                  add_dragover = "event.preventDefault()";
                  add_is_date = is_date.join("-");
                  output = gadget.calendar.weekday[date.getIsoDay(is_date) - 1] + ", " + is_date.join("-");
                  switch(date.getIsoDay(is_date)) {
                     case 6: add_style[0] += "background:rgba(99,99,99,0.5);"; break;
                     case 7: add_style[0] += "background:rgba(99,99,99,0.5);"; break;
                     default: add_style[0] += "background:rgb(252,212,80);";  
                  }
               }
               else {
                  add_style = ["color:rgb(129,129,129); border:solid 1px rgb(255,255,255);", "border:solid 1px rgb(255,255,255);"];
                  add_drop = "";
                  add_dragover = "";
                  add_is_date = "";
                  output = "";
               }
               td.innerHTML = 
               "<div style=\"" + add_style[0] + "\">" + output + "</div>" + 
               "<div style=\"" + add_style[1] + "\" ondrop=\"" + add_drop + "\" ondragover=\"" + add_dragover + "\" is_date=\"" + add_is_date + "\"></div>";
               tr.appendChild(td);
               timestamp += 86400000;
            }
            table.appendChild(tr);
            if(String(new Date(timestamp).getMonth()) != new Date(date.timestamp(gadget.calendar.pointer)).getMonth()) break;
         }
         gadget.calendar[gadget.calendar.pointer.join("-")] = table;
         calendar.querySelector("[calendar='" + gadget.calendar.pointer.join("-") + "']").appendChild(table);
         for(k in gadget.calendar.elements) {
            if(gadget.calendar.elements[k][1].trim() != "") {
               if(document.querySelector("[is_date='" + gadget.calendar.elements[k][1].trim() + "']")) {
                  gadget.calendar.drop("gadget.calendar.elements." + String(k), document.querySelector("[is_date='" + gadget.calendar.elements[k][1].trim() + "']"));
               } 
            }
         }
         gadget.calendar.init = true;
      },

      changeday: function(e) {
         while(e.tagName.toLowerCase() != "input") e = e.previousSibling;
         timestamp = date.timestamp(e.value.split("-")) + 80000000;
         if(!isNaN(timestamp)) {
            timestamp += 86400000 * Number(arguments[1]);
            is_date = [new Date(timestamp).getFullYear(), ("0" + String(new Date(timestamp).getMonth() + 1)).substr(-2), ("0" + String(new Date(timestamp).getDate())).substr(-2)];
            d = document.querySelector("[is_date='" + is_date.join("-") + "']");
            if(d) {
               gadget.calendar.drop(e.previousSibling.id, d);
            }
            else {
               with(e) {
                  value = is_date.join("-");
                  style.background = gadget.calendar.styles.setcolor;
               }
               eval(e.previousSibling.id + "[1] = \"" + is_date.join("-") + "\";");
               d = document.querySelectorAll("[date='" + e.previousSibling.id + "']");
               for(k in d) if(!isNaN(k)) d[k].parentNode.removeChild(d[k]);
            }
         }
      },

      userule: function() {
         input = document.getElementById("gadget.calendar.elements." + arguments[1]).nextSibling;
         for(_k in gadget.calendar.elements[arguments[1]][4]) {
            calc = (gadget.calendar.elements[arguments[1]][4][_k]).split(":");
            calc[0] = calc[0].toLowerCase();
            calc[1] = Number(calc[1]);
            if(gadget.calendar.elements[calc[0]][3]) {
               target = document.getElementById("gadget.calendar.elements." + calc[0]).nextSibling;
               target.value = input.value;
               target.nextSibling.nextSibling.setAttribute("style", gadget.calendar.styles.active_button);
               timestamp = new Date(input.value).getTime();
               calc[2] = 0;
               for(pp = 0; pp < calc[1]; pp++) {
                  timestamp += 86400000;
                  day = date.getIsoDay([new Date(timestamp).getFullYear(), new Date(timestamp).getMonth() + 1, new Date(timestamp).getDate()]);
                  if(day == 6 || day == 7) pp--;
                  calc[2]++
               }

               for(pp = 0; pp < calc[2]; pp++) target.nextSibling.nextSibling.click();   
            }         
         } 
      },

      delete: function(e, k) {
         while(e.tagName.toLowerCase() != "input") e = e.previousSibling;
         with(e) {
            value = gadget.calendar.elements[k][2];
            style.background = "rgb(255,255,255)";
         }
         gadget.calendar.elements[k][1] = e.value;
         gadget.calendar.nest(gadget.calendar.id);
      },

      prvmonth: function() {

         gadget.calendar.pointer[1]--;
         if(gadget.calendar.pointer[1] <= 0) {
            gadget.calendar.pointer[1] = 12;
            gadget.calendar.pointer[0]--;
         }
         gadget.calendar.nest(gadget.calendar.id);
      },

      nextmonth: function() {
         gadget.calendar.pointer[1]++;
         if(gadget.calendar.pointer[1] >= 13) {
            gadget.calendar.pointer[1] = 1;
            gadget.calendar.pointer[0]++;
         }
         gadget.calendar.nest(gadget.calendar.id);
      },
       
      drag: function() {
         if(typeof arguments[1] == "undefined") {
            arguments[0].dataTransfer.setData("text", arguments[0].target.id);
         }
         else {
            arguments[0].dataTransfer.setData("text", arguments[1].getAttribute("date"))
         }
      },

      drop: function() {
         arguments[2] = (typeof arguments[2] == "undefined") ? true : arguments[2];
         use_id = (typeof arguments[0] == "string") ? arguments[0] : arguments[0].dataTransfer.getData("text");
         d = document.querySelectorAll("[date='" + use_id + "']");
         for(k in d) if(!isNaN(k)) d[k].parentNode.removeChild(d[k]);
         p = document.createElement("p");
         p.textContent = eval(use_id + "[0]");
         p.setAttribute("date", use_id);
         p.setAttribute("draggable", eval(use_id + "[3]"));
         p.setAttribute("ondragstart", "gadget.calendar.drag(event, this)");
         arguments[1].appendChild(p);
         eval(use_id + "[1] = \"" + arguments[1].getAttribute("is_date") + "\";");
         with(document.getElementById(use_id).nextSibling) {
            value = arguments[1].getAttribute("is_date");
            if(eval(use_id + "[1] != " + use_id + "[2]")) {
               document.getElementById(use_id).nextSibling.style.background = gadget.calendar.styles.setcolor.replace(/;/g, "");
            }
         }
         d = document.getElementById(use_id).nextSibling;
         for(e = 1; e < 5; e++) {
            switch(e) {
               case 3: add_style_img = (eval(use_id + "[1] != \"\" && " + use_id + "[3]")) ? gadget.calendar.styles.active_button : gadget.calendar.styles.inactive_button; break;
               case 4: add_style_img = (eval(use_id + "[1] != \"\" && " + use_id + "[3] && typeof " + use_id + "[4] ==\"object\"")) ? gadget.calendar.styles.active_button : gadget.calendar.styles.inactive_button; break;
               default: add_style_img = (eval(use_id + "[1] != " + use_id + "[2]")) ? gadget.calendar.styles.active_button : gadget.calendar.styles.inactive_button;
            }
            d = d.nextSibling; 
            d.setAttribute("style", add_style_img)}
      },

      submit: function() {
         $(document.getElementById("confirm_box")).fadeOut(1000);
         form = document.createElement("form");
         form.setAttribute("method", "post");
         form.setAttribute("action", session.remote_domino_path_main + "/f.save_process_indicator?create");
         //new
         form.style.display = "none";
         form.innerHTML = "<input name=\"r\" value=\"" + location.href + "\">";
         cnt = 0;
         for(k in gadget.calendar.elements) {
            if(gadget.calendar.elements[k][1] != gadget.calendar.elements[k][2]) {
               cnt++;
               input = document.createElement("input");
               input.setAttribute("name", "fld" + String(cnt));
               input.setAttribute("value", gadget.calendar.unid + ";" + k + ";" + gadget.calendar.elements[k][1]);
               form.appendChild(input);
            }
         }
         //new
         document.getElementsByTagName("body")[0].appendChild(form);

         form.submit();
      },
   },
}

