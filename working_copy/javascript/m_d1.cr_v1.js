m_d1 = {

   table: [],
   tflag: false,
   cr_loop: [[], []],

   pt: {<?php
$file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.type.json?open&count=9999&function=plain");
$file = str_replace("@newline", "\r\n      ", str_replace(", ]", "]", str_replace("; ", ";\r\n", $file)));
print str_replace(", \r", "],", substr($file, 0, strrpos($file, ",")))."]\r\n";
?>
   },
   
   embed: function() {
      d = document.querySelector("[form='m_d1__cr_v1']");
      if(d.getAttribute("is_proposal") == "1") {
         d.textContent = "Control report data not available [PROPOSAL]";
      }
      else {
         script = document.createElement("script");
         script.setAttribute("src", session.remote_database_path + "javascript/jload/get_cr.js?pot=<?php print $_SESSION["pot"]; ?>"); 
         script.setAttribute("type", "text/javascript");
         script.setAttribute("onload", "m_d1.nest(['rms', 'creport'])");
         document.getElementsByTagName("head")[0].appendChild(script);
      }
   },

   add_internal_supplier() {
      script = document.createElement("script");
      script.setAttribute("src", session.remote_domino_path_main + "/p.supplier_rms?open"); 
      document.getElementsByTagName("head")[0].appendChild(script);
   },

   colorize: function(td, i, k) {
      c = "status0";
      usecolor = {status0:"rgb(255,255,255)", status1: "rgb(0,204,0)", status2: "rgb(255,255,0)", status3: "rgb(255,76,76)"};
      ir = m_d1.table["IR"][i];
      irp = m_d1.table["IRP"][i];
      irs = m_d1.table["IRS"][i];
      ira = m_d1.table["IRA"][i];

      if(ir == "E" | irp == "G" | irs == "G" | ira=="G") {c = "status3"} else {c = "status2"};
      if(ir == "A" && irp == "B" && irs == "L" && (ira=="" | ira=="N" | ira=="C1" | ira=="B")) { c = "status1" };
      if(ir == "") {c = "status0"}

      td.setAttribute("style", "background:" + usecolor[c]);
   },

   check_start: function() {
      d = document.getElementById("m_d1__cr_v1").querySelector("[form='m_d1__cr_v1']");
      c = JSON.parse(d.getAttribute("check").replace(/'/g, "\"").replace(/\[\]/g, "\"\""));
      ret = [];
      for(k in c) {
         if(c[k].trim() == "") ret.push(k);
      }
      return ret;
   },

   cr_nest: function() {
      is_rms = (document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_rms") == "1") ? true : false;
      if(!is_rms) {
	     d = document.getElementsByName("maintplant")[0];
		 
         if(document.querySelector("[form='m_d1__cr_v1']").getAttribute("part_supplier").trim() == "") {
            infobox("CONTROL REPORT CREATION NOT POSSIBLE", "Part supplier not set or insufficient.", 380, 80);
            return false;
         }
		 if(d.value == "") {
            infobox("CONTROL REPORT CREATION NOT POSSIBLE", "Maintenance Plant insufficient.", 380, 80);
            return false;
         }
		  
		  
		 
		 
      }
      if(is_rms) {
	  
         err = m_d1.check_start();
         if(err.length > 0) {
		
		 if(document.getElementsByName("exttool")[0].value!="Yes")
		 {
            html = "";
            for(k in err) html += "<div>" + (err[k].substr(0, 1).toUpperCase() + err[k].substr(1, err[k].length)).replace(/\_/g, " ") + " not set.</div>";
            infobox("RMS CREATION NOT POSSIBLE", html, 500, 300);
            return false;
			}
         }

      }
<?php
session_start();
require_once("../../../../library/tools/addin_xml.php");

$load = array("meta");
require_once("../addin/load_tracker_data.php");
$tc_data = create_tc_data($load);
$key = (strpos($tc_data["meta"]["process_type"], "v->2")) ? $_SESSION["request:pot"]."@".$_SESSION["request:unique"] : $_SESSION["request:pot"]."@".$_SESSION["request:unique"]."@G04070";
$loop = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.get_loop?open&key=".$key);


print "      ".$loop;
print "      m_d1.process_type = \"".$tc_data["meta"]["process_type"]."\";\r\n";
?>


      wf_container = document.querySelector("[form='m_x3__workflow']");
      if(wf_container) {
         if(wf_container.hasAttribute("v2_loop")) {
            if(wf_container.hasAttribute("v2_loop")) loop = wf_container.getAttribute("v2_loop").split(",");
         }
         else {
            for(c in loop) if(loop[c] > 0) loop[c]--;
         }
      }
      m_d1.loop = loop;
      has_cr = [-1, -1];
      has_cr_loop = [-1, -1];

      if (typeof m_d1.table.creport != 'undefined') {
         for(k in m_d1.table.creport.h) {
            if(m_d1.table.creport.h[k].toLowerCase() == "source") _source = Number(k) + 1;
            if(m_d1.table.creport.h[k].toLowerCase() == "loop") _loop = Number(k) + 1;
         }
         for(k in m_d1.table.creport.b) {
            // LOOP == PRODUCT CR ???
            // if war auskommentiert -> CR creation failed if more CR's as loop
            if(Number(m_d1.table.creport.b[k][_loop]) == m_d1.loop[Number(m_d1.table.creport.b[k][_source]) - 1]) {
              has_cr_loop[Number(m_d1.table.creport.b[k][_source]) - 1]++;
            }
            has_cr[Number(m_d1.table.creport.b[k][_source]) - 1]++;
         }
      }
      if(is_rms) {
         headline = "Create Control Report (RMS)";
         use_cases = {
            "01": ["New Product - Series LOOP", [], []],
            "02": ["New Product - Pilot LOOP", [], []],   
            "03": ["New Product - Tool source LOOP", [], []],
            "08": ["Repl./ Dupl. - Series LOOP", [], []],
            "09": ["Repl./ Dupl. - Pilot LOOP", [], []],
            "10": ["Repl./ Dupl. - Tool source LOOP", [], []],
            "11": ["Con. Kit", [], []],
            "15": ["Dual material", [], []],
         };

         keys = []; for(k in use_cases) keys.push(k); keys.sort();

         dualsource = document.querySelector("[form='m_d1__cr_v1']").getAttribute("dualsource");
         g3_actual = document.querySelector("[form='m_d1__cr_v1']").getAttribute("g3_actual");
         tool_supplier = document.querySelector("[form='m_d1__cr_v1']").getAttribute("tool_supplier");
         production_location = document.querySelector("[form='m_d1__cr_v1']").getAttribute("production_location");
         process_type = document.querySelector("[form='m_d1__cr_v1']").getAttribute("process_type");
         transferred_location = document.querySelector("[form='m_d1__cr_v1']").getAttribute("transferred_location");

         dupl_repl = (typeof m_d1.pt[1] == "undefined") ? []: m_d1.pt[1];
         con_kit = (typeof m_d1.pt[2] == "undefined") ? []: m_d1.pt[2];
         pilot = (typeof m_d1.pt[3] == "undefined") ? []: m_d1.pt[3];
         new_product = (typeof m_d1.pt[4] == "undefined") ? []: m_d1.pt[4];

         // APPLY CONDITION 1 - G3 date_actual is set
         use_cases["01"][1][0] = (g3_actual == "0") ? true: true;
         use_cases["02"][1][0] = (in_array(process_type.toUpperCase(), pilot)) ? true : true;
         use_cases["03"][1][0] = (g3_actual == "0") ? true : true;
         use_cases["08"][1][0] = (g3_actual == "0") ? true : true;
         use_cases["09"][1][0] = (g3_actual == "0") ? true : true;
         use_cases["10"][1][0] = (g3_actual == "0") ? true : true;
         use_cases["11"][1][0] = (g3_actual == "0") ? true : true;
         use_cases["15"][1][0] = (g3_actual == "0") ? true : true;

         // APPLY CONDITION 2 - production location is added
         use_cases["01"][1][1] = (production_location == "") ? false : true;
         use_cases["02"][1][1] = (production_location == "") ? true : true; //changed 20180427 manjeera --> true : true from true : false
         use_cases["03"][1][1] = (production_location == "") ? false : true;
         use_cases["08"][1][1] = (production_location == "") ? false : true;
         use_cases["09"][1][1] = (production_location == "") ? true : true;  //changed 20180427 manjeera --> true : true from true : false
         use_cases["10"][1][1] = (production_location == "") ? false : true;
         use_cases["11"][1][1] = (production_location == "") ? true : true;
         use_cases["15"][1][1] = (production_location == "") ? true : true;

         // APPLY CONDITION 3 - production location is equal to tool supplier
         use_cases["01"][1][2] = (production_location != tool_supplier) ? true : true;
         use_cases["02"][1][2] = (production_location != tool_supplier) ? true : true;
         use_cases["03"][1][2] = (production_location != tool_supplier) ? true : true;
         use_cases["08"][1][2] = (production_location != tool_supplier) ? true : true;
         use_cases["09"][1][2] = (production_location != tool_supplier) ? true : true; //changed 20180427 manjeera --> true : true from false : true
         use_cases["10"][1][2] = (production_location != tool_supplier) ? true : true;
         use_cases["11"][1][2] = (production_location != tool_supplier) ? false : true; //changed 20161124 jh --> false : true
         use_cases["15"][1][2] = (production_location != tool_supplier) ? true : true;  //changed 20180427 manjeera --> true : true from false : true

         // APPLY CONDITION 4 - process type is SUS_D or SUS_M or SUS_S --- DUPL REPL
         use_cases["01"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? false : true;
         use_cases["02"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? false : true;
         use_cases["03"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? false : true;
         use_cases["08"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? true : false;
         use_cases["09"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? true : false;
         use_cases["10"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? true : false;
         use_cases["11"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? false : true;
         use_cases["15"][1][3] = (in_array(process_type.toUpperCase(), dupl_repl)) ? true : true;

         // APPLY CONDITION 5 - process type is SUS_CK or SUS_DC or SUS_MC or Con.Kit --- CONVERSION KIT
         use_cases["01"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true;
         use_cases["02"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true;
         use_cases["03"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true;
         use_cases["08"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true;
         use_cases["09"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true;
         use_cases["10"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true;
         use_cases["11"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true; //changed 20161124 jh --> true : false --> change back on 20170601
         use_cases["15"][1][4] = (in_array(process_type.toUpperCase(), con_kit)) ? true : true;


         // APPLY CONDITION 6 - dual source checked
         use_cases["01"][1][5] = (dualsource == "1") ? true : true;
         use_cases["02"][1][5] = (dualsource == "1") ? true : true;
         use_cases["03"][1][5] = (dualsource == "1") ? true : true;
         use_cases["08"][1][5] = (dualsource == "1") ? true : true;
         use_cases["09"][1][5] = (dualsource == "1") ? true : true;
         use_cases["10"][1][5] = (dualsource == "1") ? true : true;
         use_cases["11"][1][5] = (dualsource == "1") ? true : true;
         use_cases["15"][1][5] = (dualsource == "1") ? true : false;


         // APPLY CONDITION 7 - is Pilot  --- PILOT  --- NEW PRODUCT
         use_cases["01"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? false : true;
         use_cases["02"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? false : true;
         use_cases["03"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? false : true;
         use_cases["08"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? false : true;
         use_cases["09"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? true : true;
         use_cases["10"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? false : true;
         use_cases["11"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? false : true;
         use_cases["15"][1][6] = (in_array(process_type.toUpperCase(), pilot)) ? false : true;


         // APPLY CONDITION 8 - CR or RMS is existing in loop
         use_cases["01"][1][7] = (in_array(m_d1.loop[0], m_d1.cr_loop[0])) ? false : true;
         use_cases["02"][1][7] = (in_array(m_d1.loop[0], m_d1.cr_loop[0])) ? false : true;
         use_cases["03"][1][7] = (in_array(m_d1.loop[0], m_d1.cr_loop[0])) ? false : true;
         use_cases["08"][1][7] = (in_array(m_d1.loop[0], m_d1.cr_loop[0])) ? false : true;
         use_cases["09"][1][7] = (in_array(m_d1.loop[0], m_d1.cr_loop[0])) ? false : true;
         use_cases["10"][1][7] = (in_array(m_d1.loop[0], m_d1.cr_loop[0])) ? false : true;
         use_cases["11"][1][7] = (in_array(m_d1.loop[0], m_d1.cr_loop[0])) ? false : true;
         use_cases["15"][1][7] = (in_array(m_d1.loop[1], m_d1.cr_loop[1])) ? false : true;


         // APPLY CONDITION 9 - Transferred Location
         use_cases["01"][1][8] = (transferred_location != "") ? true : false;
         use_cases["02"][1][8] = (transferred_location != "") ? true : true;
         use_cases["03"][1][8] = (transferred_location != "") ? false : true;
         use_cases["08"][1][8] = (transferred_location != "") ? true : false;
         use_cases["09"][1][8] = (transferred_location != "") ? true : true;
         use_cases["10"][1][8] = (transferred_location != "") ? false : true;
         use_cases["11"][1][8] = (transferred_location != "") ? true : true;
         use_cases["15"][1][8] = (transferred_location != "") ? true : false;



         // APPLY CONDITION 10 - Transferred location is equal to tool supplier
         use_cases["01"][1][9] = (transferred_location == tool_supplier) ? false : true;
         use_cases["02"][1][9] = (transferred_location == tool_supplier) ? true : true;
         use_cases["03"][1][9] = (transferred_location == tool_supplier) ? false : true;
         use_cases["08"][1][9] = (transferred_location == tool_supplier) ? false : true;
         use_cases["09"][1][9] = (transferred_location == tool_supplier) ? true : true;
         use_cases["10"][1][9] = (transferred_location == tool_supplier) ? false : true;
         use_cases["11"][1][9] = (transferred_location == tool_supplier) ? true : true;
         use_cases["15"][1][9] = (transferred_location == tool_supplier) ? true : true;

         // APPLY REMARKS
         process_type = process_type.replace(/V->2/, "");
         for(k in keys) {
            msg = [];
            l = (keys[k] == "15") ? m_d1.loop[1] : m_d1.loop[0];
            msg.push((!use_cases[keys[k]][1][0]) ? (keys[k] == "02") ? "G3 actual date set." : "G3 actual date not set." : ""); 
            msg.push((!use_cases[keys[k]][1][1]) ? (in_array(keys[k], ["02", "09"])) ? "Production location added" : "Production location not added." : "");
            msg.push((!use_cases[keys[k]][1][2]) ? (in_array(keys[k], ["03", "10"])) ? "Production location and tool supplier are not different." : "Production location and tool supplier are different." : "");
            msg.push((!use_cases[keys[k]][1][3]) ? (in_array(keys[k], ["01", "02", "03", "11"])) ? "Process type "+ process_type +" is member of Duplication / Replacement." : "Process type "+ process_type +" is not member of Duplication / Replacement." : "");
            msg.push((!use_cases[keys[k]][1][4]) ? "Process type "+ process_type +" is not member of Conversion Kit." : "");  
            msg.push((!use_cases[keys[k]][1][5]) ? (keys[k] == "15") ? "No dual source." : "Dual source checked." : "");  
            msg.push((!use_cases[keys[k]][1][6]) ? "Process type "+ process_type +" is not allowed." : ""); 
            msg.push((!use_cases[keys[k]][1][7]) ? "CR or RMS already created in loop " + l + "." : "");
            msg.push((!use_cases[keys[k]][1][8]) ? (in_array(keys[k], ["03", "10"])) ? "Transfer location set." : "Transfer location not set." : "");
            msg.push((!use_cases[keys[k]][1][9]) ? (in_array(keys[k], ["03", "10"])) ? "Transferred location and tool supplier are not different." : "Transferred location and tool supplier are different." : "");  
            msg = msg.filter(Boolean);
            use_cases[keys[k]][2] = msg;
         }
         table = document.createElement("table");

         for(k in keys) {
            chk = true; for(uc in use_cases[keys[k]][1]) if(!use_cases[keys[k]][1][uc]) chk = false;

// chk = true;

            use_style = (!chk) ? "width:470px; opacity:0.65;" : "width:470px; opacity:1.0;";
            use_cursor = (!chk) ? "default" : "pointer";
            use_href = (!chk) ? "void(0);" : "m_d1.control_report_create('" + keys[k] + "');";
            a = document.createElement("a");
            a.setAttribute("style", "font-size:16px; height:40px; padding-top:17px; cursor:" + use_cursor);
            a.setAttribute("href", "javascript:" + use_href);
            a.textContent = use_cases[keys[k]][0];       
            tr = document.createElement("tr");
            td = document.createElement("td");
            td.setAttribute("style", "vertical-align:top;");
            _l = (m_d1.loop[0] == "") ? "0" : m_d1.loop[0];
            td.innerHTML = "<span class=\"phpbutton\" style=\"height:58px;" + use_style + "\">" + a.outerHTML.replace(/LOOP/, "[Loop " + _l + "]") + "</span>";
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("style", "vertical-align:top; height:90px;");
            for(r in use_cases[keys[k]][2]) td.innerHTML += "<div style=\"padding-left:10px; color:rgb(33,33,33);\"><img style=\"-webkit-filter: grayscale(10%); position:relative; top:1px; left:-6px;\" src=\"data:image/gif;base64,<?php $file=file_get_contents("../../../../library/images/stop.gif"); print base64_encode($file); ?>\">" + use_cases[keys[k]][2][r] + "</div>";
            tr.appendChild(td);
            table.appendChild(tr);
         }   
      }
      else {
         headline = "Create Control Report";
         arr = {
              label: ["Create<br>Product CR for<br>Source 1 / Loop " + loop[0], "Create Product<br>and process CR for<br>Source 1 / Loop " + loop[0], "Create process<br>trial run for<br>Source 1", "Create<br>Product CR for<br>Source 2 / Loop " + loop[1], "Create Product<br>and process CR for<br>Source 2 / Loop " + loop[1], "Create process<br>trial run for<br>Source 2"],
              message: {
                 label1: [
                 "Select this option to create a Product CR for source 1",
                 "You are not authorized to perfom this operation.",
                 "No dual source.",
                 "No mold.",
                 "CR already exists for this loop.",
                 "Tool already transfered",
                 "Pilot Tool",
                 "Process indicator not activated"
                 ],
                 label2: [
                 "Select this option to create a Product & Process CR for source 1",
                 "You are not authorized to perfom this operation.",
                 "No dual source.",
                 "No mold.",
                 "CR already exists for this loop.",
                 "Tool already transfered",
                 "Pilot Tool",
                 "Process indicator not activated"
                 ],
                 label3: [
                 "Select this option to create a PTR CR for source 1",
                 "You are not authorized to perfom this operation.",
                 "No dual source.",
                 "No mold.",
                 "No Product CR created.",
                 "Tool already transfered",
                 "Pilot Tool",
                 "Process indicator not activated"
                 ],
                 label4: [
                 "Select this option to create a Product CR for source 2",
                 "You are not authorized to perfom this operation.",
                 "No dual source.",
                 "No mold.",
                 "CR already exists for this loop.",
                 "Tool already transfered",
                 "Pilot Tool",
                 "Process indicator not activated"
                 ],
                 label5: [
                 "Select this option to create a Product & Process CR for source 2",
                 "You are not authorized to perfom this operation.",
                 "No dual source.",
                 "No mold.",
                 "CR already exists for this loop.",
                 "Tool already transfered",
                 "Pilot Tool",
                 "Process indicator not activated"
                 ],
                 label6: [
                 "Select this option to create a PTR CR for source 2",
                 "You are not authorized to perfom this operation.",
                 "No dual source.",
                 "No mold.",
                 "No Product CR created.",
                 "Tool already transfered",
                 "Pilot Tool",
                 "Process indicator not activated"
                 ],
              },
         };
         table = document.createElement("table");
         table.setAttribute("id", "control_report_select");
         tr = document.createElement("tr");
         table.appendChild(tr);
         td = document.createElement("td");
         td.setAttribute("colspan", "2");

         <?php session_start(); 
         print "userroles = \"".implode(",", $_SESSION["remote_userroles"])."\".split(\",\");\r\n";
         require_once("../addin/load_tracker_data.php");
         $load = array("pot", "tool");
         $tc_data = create_tc_data($load);
         print "      tool = {number: \"".$tc_data["tool"]["number"]."\", dualsource: \"".$tc_data["tool"]["dualsource"]."\", transferred_location: \"".$tc_data["tool"]["transferred_location"]."\", exttool: \"".$tc_data["tool"]["exttool"]."\"}\r\n"; 
         print "      tool.transferred_location = tool.transferred_location.replace(/Array/g, \"\");\r\n";
         ?>
         op = []; cu = []; js = [];
         op[0] = (tool.transferred_location == "" || !in_array("[t_location]", userroles)) ? 0.5 : 1.0;
         op[1] = (tool.transferred_location == "" || !in_array("[t_location]", userroles) || tool.dualsource != "1") ? 0.5 : 1.0;
         cu[0] = (tool.transferred_location == "" || !in_array("[t_location]", userroles)) ? "default" : "pointer";
         cu[1] = (tool.transferred_location == "" || !in_array("[t_location]", userroles) || tool.dualsource != "1") ? "default" : "pointer";
         js[0] = (tool.transferred_location == "" || !in_array("[t_location]", userroles)) ? "void(0);" : "m_d1.transfer = true; m_d1.key = 0; m_d1.control_report_create(0);";
         js[1] = (tool.transferred_location == "" || !in_array("[t_location]", userroles) || tool.dualsource != "1") ? ";" : "m_d1.transfer = true; m_d1.key = 3; m_d1.control_report_create(3);";

         transfer = [
            "<span style=\"position:relative; top:20px; left:880px; display:inline-block; opacity: " + String(op[0]) + "\"><a style=\"cursor: " + cu + "; border:solid 2px rgb(99,177,229); font:normal 12px Open Sans; text-decoration:none; color:rgb(255,255,255); display:inline-block; width:134px; height:51px; background-color:rgb(0,102,161); text-align:center; padding-top:4px;\" href=\"javascript:" + js[0] + "\">CREATE<br>TRANSFER<br>SOURCE 1</a></span>",
            "<span style=\"position:relative; top:20px; left:890px; display:inline-block; opacity: " + String(op[1]) + "\"><a style=\"cursor: " + cu + "; border:solid 2px rgb(99,177,229); font:normal 12px Open Sans; text-decoration:none; color:rgb(255,255,255); display:inline-block; width:134px; height:51px; background-color:rgb(0,102,161); text-align:center; padding-top:4px;\" href=\"javascript:" + js[1] + "\">CREATE<br>TRANSFER<br>SOURCE 2</a></span>"
         ];

         td.innerHTML = "<span style=\"position:absolute;\">" + transfer.join("") + "</span><h1>Please select a control report type</h1>";
         tr.appendChild(td);
         for(k in arr.label) {

            chk = true;
            msg = [0];


            if(m_d1.process_type=="Array") {
               chk = false;
               msg.push(7);
            } 
            if(k == 0 && !in_array("[cr]", userroles)) {
               chk = false;
               msg.push(1);
            }
            if(k != "0" && pilot[0] == "yes") {
               chk = false;
               msg.push(6);
            } 
            if(k == 1 && !in_array("[cr]", userroles)) {
               chk = false;
               msg.push(1);
            } 
            if(k == 2 && !in_array("[composeproz]", userroles)) {
               chk = false;
               msg.push(1);
            } 
            if(k == 3 && !in_array("[cr]", userroles)) {
               chk = false;
               msg.push(1);
            } 
            if(k == 4 && !in_array("[cr]", userroles)) {
               chk = false;
               msg.push(1);
            } 
            if(k == 5 && !in_array("[composeproz]", userroles)) {
               chk = false;
               msg.push(1);
            } 

            if(k > 2 && tool.dualsource != "1") {
               chk = false;
               msg.push(2);
            }
            if(k != "0" && k != "3" && !in_array(tool.number.substr(0, 2), ["21", "23", "28"])) {
               chk = false;
               msg.push(3);
            }

            if(((k == 0 || k == 1) && Number(has_cr_loop[0]) > -1) || ((k == 3 || k == 4) && Number(has_cr_loop[1]) > -1)) {
               chk = false;
               msg.push(4);
            }
            
            if((k == 2 && has_cr[0] == -1) || (k == 5 && has_cr[1] == -1)) {
               chk = false;
               msg.push(4);
            }

            if(tool.transferred_location != "") {
               chk = false;
               msg=[5];
            }
            opacity = (chk) ? 1.0 : 0.5;
            cursor = (chk) ? "pointer" : "default";
            js = (chk) ? "m_d1.transfer = false; m_d1.key = " + k + "; m_d1.control_report_create(" + k + ")" : "void(0)";

            tr = document.createElement("tr");
            table.appendChild(tr);
            td = document.createElement("td");
            td.setAttribute("style", "vertical-align:top; height:120px; width:1%;");
            td.innerHTML = "<span style=\"width:178px; height:51px; opacity:" + opacity + ";\" class=\"phpbutton\"><a style=\"text-align:center; height:49px; cursor:" + cursor + "\" href=\"javascript:" + js + ";\">" + arr.label[k] + "</a></span>";
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("style", "vertical-align:top; height:120px; margin-left:20px;");
            td.innerHTML = ""; 
            for(i in msg) if(msg.length == 1 || msg.length > 1 && i != -1) td.innerHTML += "<b>" + arr.message["label" + String(Number(k) + 1)][msg[i]] +"</b>";
            tr.appendChild(td);
         }
      } 
      if(!m_d1.tflag) table.innerHTML = table.innerHTML.replace(/\'/g, "\\'");
      m_d1.tflag = true;
      infobox(headline, table.outerHTML, 1200, 900);
   
   },

   control_report_create: function(k) {
      if(in_array(k, [2, 5]) && typeof arguments[2] == "undefined" && document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_rms") != "1") {
         loop = (k == 2) ? "1" : "2";
         d = document.getElementById("confirm_box").getElementsByClassName("body")[0];
         d.innerHTML = "<h1>Please select the control report to copy</h1>";
         table = document.createElement("table");
         table.setAttribute("border", "0");
         table.setAttribute("cellpadding", "0");
         table.setAttribute("cellspacing", "0");
         tr = document.createElement("tr");
         table.appendChild(tr);

         m_d1.pot = m_d1.table.creport;
         for(n in m_d1.pot.h) {
            if(m_d1.pot.h[n].toLowerCase() != "unid") {
               th = document.createElement("th");
               th.textContent = (m_d1.pot.h[n].substr(0, 1).toUpperCase() + m_d1.pot.h[n].substr(1, m_d1.pot.h[n].length - 1)).replace(/_/g, " ");
               tr.appendChild(th);
            }
         } 

         m_d1.table.len = m_d1.table.creport.b.length;
         for(i = 0; i <= m_d1.table.len - 1; i++) {
//            if(unescape(m_d1.table[m_d1.pot.h[2]][i]) == loop && unescape(m_d1.table[m_d1.pot.h[4]][i]).indexOf("evaluated") > -1) {
              if (unescape(m_d1.table.creport.b[i][6]).indexOf("evaluated") > -1) {
               tr = document.createElement("tr");

               a = document.createElement("a");
               a.innerHTML= unescape (m_d1.table.creport.b[i][1]);

//               tr.setAttribute("onclick", " alert('"+ a.textContent +"'); m_d1.control_report_create(" + k + ", false,'" + a.textContent + "')");
               tr.setAttribute("onclick", "m_d1.control_report_create(" + k + ", false,'" + a.textContent + "')");
               table.appendChild(tr);
               c = -1; 
               for(n in m_d1.pot.h) {
                  if(m_d1.pot.h[n].toLowerCase() != "unid" && m_d1.pot.h[n].toLowerCase() != "_" && m_d1.pot.h[n].toLowerCase() != "type") {
                     c++;
                     td = document.createElement("td");
                     td.setAttribute("style", "width:200px;");
                     a = document.createElement("a");
                     a.innerHTML = unescape(m_d1.table.creport.b[i][n]);

                     td.innerHTML = a.textContent;
                     tr.appendChild(td);
                  }
               }
            }
         }
         d.innerHTML = "<span form=\"m_d1__cr_v1:copy\">" + d.innerHTML + table.outerHTML + "</span>";
      }
      else {
         if(typeof arguments[1] == "undefined" || arguments[1] == false) {
            document.getElementById("confirm_box").getElementsByClassName("body")[0].innerHTML = "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"position:relative; left:-1px; top:2px; \">&nbsp;&nbsp;Processing ...";
            d = document.getElementById("form:control_report");
            arguments[2] = (typeof arguments[2] == "undefined") ? "" : arguments[2];
            if(d) d.parentNode.removeChild(d);
            script = document.createElement("script");
//          document.write(session.remote_database_path + "javascript/jload/control_report.js?is_pilot=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_pilot") + "&is_rms=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_rms") + "&cr=" + arguments[2] + "&k=" + String(Number(k) + 1) + "&pot=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("pot") + "&tracker=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("tracker") + "&process_type=" + m_d1.process_type);
            script.setAttribute("src", session.remote_database_path + "javascript/jload/control_report.js?is_pilot=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_pilot") + "&is_rms=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_rms") + "&cr=" + arguments[2] + "&k=" + String(Number(k) + 1) + "&pot=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("pot") + "&tracker=" + document.querySelector("[form='m_d1__cr_v1']").getAttribute("tracker") + "&process_type=" + m_d1.process_type);
            script.setAttribute("type", "text/javascript");
            script.setAttribute("id", "form:control_report");
            script.setAttribute("onload", "m_d1.control_report_create('" + k + "', true, '" + arguments[2] + "')");
            document.getElementsByTagName("head")[0].appendChild(script);
         }
         else {
            control_report.nest(k);
         }
      }
   },
}