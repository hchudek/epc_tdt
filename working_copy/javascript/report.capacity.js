function handle_tpm(e) {
   e.src = (e.src.indexOf("status-5-1") > 0) ? session.php_server + "/library/images/16x16/status-5.png" : session.php_server + "/library/images/16x16/status-5-1.png";
   d = document.getElementsByName(e.name);
   arr = new Array();
   for(i = 0; i < d.length; i++) {
      if(d[i].src.indexOf("status-5-1") == -1) {
         arr.push(d[i].nextSibling.innerText);
      }
   }
   document.capacity_report.tpm.value = arr.join(";");
}

function recalc_working_hours() {
   d = document.getElementsByName("tpm_kpi");
   r = document.getElementsByName("tpm_workload");
   f_wl = Number(document.getElementsByName("add_fixed_workload")[0].innerText.replace(",", "."));
   v_wl = Number(document.getElementsByName("add_variable_workload")[0].innerText.replace(",", "."));
   total = 0;
   for(i = 0; i < d.length; i++) {
      kpi = Number(d[i].innerText.replace(/,/g, "."));
      res = (kpi * v_wl + f_wl).toFixed(2);
      r[i].innerText = res.replace(".", ",");
      total += Number(res);
   }
   document.getElementById("wl_av").innerText = ((total / d.length).toFixed(2)).replace(".", ",");
}

function slider1_handler(pos, slider) {
   document.getElementsByName("add_fixed_workload")[0].innerText = pos.toFixed(2).replace(".", ",");
   recalc_working_hours();
}

function slider2_handler(pos, slider) {
   document.getElementsByName("add_variable_workload")[0].innerText = pos.toFixed(2).replace(".", ",");
   recalc_working_hours();
}

function embed_slider() {
   slider1_arr = (document.getElementsByName("add_fixed_workload")[0].innerText.replace(",", ".")).split("/");
   document.getElementsByName("add_fixed_workload")[0].innerText = slider1_arr[2];
   slider1 = new dhtmlxSlider("sliderbox1", 500, "dhx_skyblue", null, slider1_arr[0], slider1_arr[1], slider1_arr[2], slider1_arr[3]);
   slider1.attachEvent("onChange", slider1_handler);
   slider1.init();
   slider2_arr = (document.getElementsByName("add_variable_workload")[0].innerText.replace(",", ".")).split("/");
   document.getElementsByName("add_variable_workload")[0].innerText = slider2_arr[2];
   slider2 = new dhtmlxSlider("sliderbox2", 500, "dhx_skyblue", null, slider2_arr[0], slider2_arr[1], slider2_arr[2], slider2_arr[3]);
   slider2.attachEvent("onChange", slider2_handler);
   slider2.init();
}


function do_select() {
   d = document.getElementsByName(arguments[0]);
   for(i = 0; i < d.length; i++) {
      d[i].src = (arguments[1] == true) ? session.php_server + "/library/images/16x16/status-5.png" : session.php_server + "/library/images/16x16/status-5-1.png";
   }
   arr = new Array();
   for(i = 0; i < d.length; i++) {
      if(d[i].src.indexOf("status-5-1") == -1) {
         arr.push(d[i].nextSibling.innerText);
      }
   }
   document.capacity_report.tpm.value = arr.join(";");
}


function do_export(id, e) {
   document.getElementsByName(e)[0].innerText = document.getElementById(id).innerHTML;
   document.capacity_report.submit();
}


function handle_project(e, unique, flag) {
   html = (flag) ? document.getElementById(unique).innerHTML : "";
   e.nextSibling.style.visibility = (flag) ? "visible" : "hidden";
   e.nextSibling.innerHTML = html;
}


