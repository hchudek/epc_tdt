m_x7 = {
   nest: function() {
      script = document.createElement("script");
      script.src = "modules/m_x7.dig_rep_info.php?callback=m_x7.execute";
      document.getElementsByTagName("body")[0].appendChild(script);
   },
   
   execute: function(data) {
      d = document.querySelector("[id='m_x7__dig_rep_info']").getElementsByClassName("inner_box")[0];
      d.innerHTML = "";
      input = document.createElement("input");
      input.onkeyup = function() {
         v = this.value.toLowerCase().split(" ");
         tr = document.querySelector("[id='m_x7__dig_rep_info']").getElementsByClassName("inner_box")[0].getElementsByTagName("tr");
         for(k in tr) if(typeof tr[k] == "object") { 
            dsp = true;
            for(e in v) {
               if(tr[k].hasAttribute("key")) if(tr[k].getAttribute("key").indexOf(v[e]) == -1) { dsp = false; break; }
            }
            tr[k].style.display = (dsp) ? "table-row" : "none";
         }
      }
      d.appendChild(input);
      if(typeof arguments[0] == "object") {
         table = document.createElement("table");
         tr = document.createElement("tr");
         for(k in data.th) {
            th = document.createElement("th");
            th.textContent = data.th[k].value;
            th.style.width = data.th[k].width + "px";
            tr.appendChild(th);
         }
         table.appendChild(tr);
         for(r in data.td) {
            tr = document.createElement("tr");
            key = [];
            for(k in data.th) {
               td = document.createElement("td");
               td.style.width = data.th[k].width + "px";
               t = unescape(data.td[r][data.th[k].value]);
               key.push((t + "=>").substr(0, (t + "=>").indexOf("=>")));
               t = (t.indexOf("=>") == -1) ? "<span>" + t + "</span>" : "<a href=\"" + session.remote_domino_path_epcmain + "/_/" + t.substr(t.indexOf("=>") + 2, t.length) + "?open\" target=\"_blank\">" + t.substr(0, t.indexOf("=>")) + "<a>"; 
               td.innerHTML = t;
               tr.appendChild(td);
            }
            tr.setAttribute("key", (key.join(" ")).toLowerCase());
            table.appendChild(tr);
         }
         d.appendChild(table);
      }
   }
}
