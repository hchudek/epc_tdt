<?php

session_start();

header("Content-type: text/javascript");

require_once("../../../../library/addin/get_browser_info.php");
$include = explode(":", str_replace(" ", "", base64_decode($_REQUEST["include"])));

print "/* included: ".date("ymd H:i:s")." */\r\n";

foreach($include as $file) {
   include_file($file.".js", "/* %comment% */");
   include_file($file.".".browserinfo("browser").".js", "/* %comment% */");
   include_file($file.".".browserinfo("browser").".".browserinfo("version").".js", "/* %comment% */");
}

function include_file($file, $comment) {
   if(file_exists($file)) {
      print "\r\n\r\n";
      print str_replace("%comment%", substr($file, 0, strlen($file))." ".filesize($file)." ".date("ymd G:i:s", filemtime($file)), $comment)."\r\n";
      include_once $file;
   }
}

// SESSION ANLEGEN
if($_SESSION["js_session_data"] == true) {
   print "session = new container(\r\n";
   $key = array_keys($_SESSION);
   for($i = 0; $i < count(array_keys($_SESSION)); $i++) {
      $res = (is_array($_SESSION[$key[$i]]) == true) ? implode(":", $_SESSION[$key[$i]]) : $_SESSION[$key[$i]];
      $delim = ($i == count(array_keys($_SESSION)) - 1) ? "" : ",";
      if(substr($key[$i], 0, 2) != "__") print "   \"".$key[$i]." => ".$res."\"".$delim."\r\n";
   }
   print ");\r\n\r\n";
}

// BROWSERINFO ANLEGEN
print "browserinfo = new container(\r\n";
print "   \"name => ".browserinfo("browser")."\",\r\n";
print "   \"version => ".browserinfo("version")."\"\r\n";
print ");\r\r\n";

?>