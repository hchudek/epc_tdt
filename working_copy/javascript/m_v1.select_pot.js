m_v1__select_pot = {
   init: function() {
      d = document.getElementById("m_v1__select_pot_body").getElementsByTagName("p");
      if(d.length == 0) {
         document.getElementById("m_v1__select_pot").getElementsByClassName("inner_box")[0].innerHTML = "<span>No POT found</span>";
      }
      else {
         for(k in d) if(!isNaN(k)) {
            d[k].setAttribute("class", "p_reg");
            d[k].setAttribute("onmouseover", "this.className='p_high'");
            d[k].setAttribute("onmouseout", "this.className='p_reg'");
            d[k].setAttribute("onclick", "m_v1__select_pot.do_select_single(this.getElementsByTagName('a'))");
            searchstring = [];
            content = d[k].getElementsByTagName("a");
            for(c in content) if(!isNaN(c)) {
               searchstring.push((content[c].textContent));
            }
            d[k].setAttribute("key", searchstring.join(" ").toLowerCase().trim());
         }   
      }
   },

   do_select_single: function(e) {
     e[0].firstChild.src = (e[0].firstChild.src.indexOf("status-5-1.png") > -1) ? e[0].firstChild.src.replace("status-5-1", "status-5") : e[0].firstChild.src.replace("status-5", "status-5-1");
   },

   do_select_all: function() {
      r_from = (arguments[0]) ? "status-5-1" : "status-5";
      r_to = (arguments[0]) ? "status-5" : "status-5-1"; 
      d = document.getElementById("m_v1__select_pot_body").getElementsByTagName("p");
      for(k in d) if(!isNaN(k) && d[k].style.display != "none") {
         if(arguments[0] || !arguments[0] && d[k].getElementsByTagName("a")[0].firstChild.src.indexOf("status-5-1") == -1) {
            d[k].getElementsByTagName("a")[0].firstChild.src = d[k].getElementsByTagName("a")[0].firstChild.src.replace(r_from, r_to);
         }
      }
   },

   get_selected_pot: function() {
      d = document.getElementById("m_v1__select_pot_body").getElementsByTagName("p");
      arr = [];
      for(k in d) if(!isNaN(k)) {
         e = d[k].getElementsByTagName("a")[0].firstChild;
         if(e.src.indexOf("status-5-1") == -1) arr.push(e.getAttribute("unique"));
      }
      return arr;
   },

   basicsearch: function() {
      d = document.getElementById("m_v1__select_pot_body").getElementsByTagName("p");
      l1 = d.length;
      r = l1;
      v = (arguments[0].toLowerCase()).split(" ");
      l2 = v.length;
      for(i = 0; i < l1; i++) {
         dsp = "block";
         for(e = 0; e < l2; e++) {
            if(d[i].getAttribute("key").indexOf(v[e]) == -1) {
               dsp = "none";
               r--;
               break;
            }
         }
         d[i].style.display = dsp;
      }
      for(k in d) if(!isNaN(k)) {
         if(d[k].style.display == "none" && d[k].getElementsByTagName("a")[0].firstChild.src.indexOf("status-5-1") == -1) {
            d[k].getElementsByTagName("a")[0].firstChild.src = d[k].getElementsByTagName("a")[0].firstChild.src.replace("status-5", "status-5-1");
         }
      }
      m_v1__select_pot.do_highlight(d, l1, v.join(" "));
   },


   do_highlight: (false) ? function() {} : function(d, l1) {
      v = arguments[2].split(" ");
      childs = (d[0].childNodes.length);
      for(i = 0; i < l1; i++) {
         if(d[i].style.display == "block") {
            for(e = 1; e < childs; e++) {
               tct = d[i].childNodes[e].textContent; 
               for(a = 0; a < v.length; a++) { 
                  s_pos = (tct.toLowerCase()).indexOf(v[a]);
                  if(s_pos > -1 && tct.substr(s_pos + 1, 1) != ">") {
                     e_pos = s_pos + v[a].length;
                     tct = tct.substr(0, s_pos) + "<b>" + tct.substr(s_pos, v[a].length) + "</b>" + tct.substr(e_pos, tct.length);
                  }
               }
               d[i].childNodes[e].innerHTML = tct;
            }
         }
      }
   },


   load: function(prj) {
      d = document.getElementById("m_v1__select_pot").getElementsByClassName("inner_box")[0];
      url = session.remote_database_path + "modules/m_v1.select_pot.php?m_v1_get__project=" + escape(prj.toUpperCase());
      js = "r=document.getElementById('run_ajax'); document.getElementById('m_v1__select_pot').getElementsByClassName('inner_box')[0].innerHTML=r.innerHTML; r.innerHTML=''; m_v1__select_pot.init();";
      run_ajax("run_ajax", url, js);
   }

}

