function include_m_t8__pot_comment() {

   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_t8.pot_comment.php");
   require_once($path."/addin/basic_php_functions.php");
   $load = array("pot");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);

   $html = rawurlencode(m_t8__pot_comment_generate_html($_application));
   print "html = \"".$html."\";\r\n";
   ?>

   document.getElementById("m_t8__pot_comment_innerhtml").innerHTML = unescape(html);
   
}
