function include_m_e3__ee_edit_pot() {

   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   $_REQUEST["load"] = "";
   require_once(substr($file, 0, strpos($file, "apps"))."library/tools/addin_xml.php");
   require_once($path."/modules/m_e3.ee_edit_pot.php");
   require_once($path."/addin/basic_php_functions.php");
   require_once($path."/application.ini");

   $_application["page_id"] = "ee_edit_pot";

   $html = rawurlencode(m_e3__ee_edit_pot_view_generate_html($_application));
   print "html = \"".$html."\";\r\n";

   ?>

   document.getElementById("m_e3__ee_edit_pot_innerhtml").innerHTML = unescape(html);
   do_smartsearch("smartsearch.body", 5000); 
  
}

