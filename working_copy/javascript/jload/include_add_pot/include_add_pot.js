add_pot = {

   init_workspace: function(id) {
      <?php
      $path_translated = str_replace(basename(__FILE__), "", str_replace("\\", "/", __FILE__));
      require_once(substr($path_translated, 0, strpos($path_translated, "javascript"))."addin/basic_php_functions.php");
      print "   unique = [\"".generate_uniqueid(16)."\", \"".generate_uniqueid(16)."\"];\r\n"; 	
      ?>
      tbl_frm = document.createElement("table");
      tbl_frm.setAttribute("border", "0");
      tbl_frm.setAttribute("cellpadding", "1");
      tbl_frm.setAttribute("cellspacing", "0");
      tbl_frm.setAttribute("style", "width:1070px;");
      tr_frm = document.createElement("tr");

      td_frm = document.createElement("td");
      td_frm.setAttribute("style", "height:46px;overflow:hidden;");
      td_frm.innerHTML = "<div>Loading ...</div>";
      td_frm.setAttribute("id", "include_" + unique[0]);
      tr_frm.appendChild(td_frm);

      td_frm = document.createElement("td");
      td_frm.setAttribute("rowspan", "2");
      td_frm.setAttribute("id", "include_" + unique[1]);
      td_frm.innerHTML = "<div>Loading ...</div>";
      tr_frm.appendChild(td_frm);
      tbl_frm.appendChild(tr_frm);

      tr_frm = document.createElement("tr");
      td_frm = document.createElement("td");
      td_frm.setAttribute("style", "width:300px;");
      html = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"table-layout:fixed;font:normal 13px Open Sans;position:fixed;margin-left:2px;\"><tr>" + 
      "<td style=\"width:19px;border-bottom:solid 1px rgb(99,99,99);padding-left:2px;\">&nbsp;</td>" +
      "<td style=\"width:80px;border-bottom:solid 1px rgb(99,99,99);padding-left:2px;\"><b>Part</b></td>" +
      "<td style=\"width:26px;text-align:center;border-bottom:solid 1px rgb(99,99,99);padding-left:2px;\"><b>R</b></td>" + 
      "<td style=\"width:136px;border-bottom:solid 1px rgb(99,99,99);padding-left:2px;\"><b>Tool</b></td>" + 
      "</tr></table>";
      td_frm.innerHTML = html + "<div id=\"include_add_pot:pot_area\" ondrop=\"add_pot.drop(this, event);\" ondragover=\"allow_drop(event);\" style=\"margin-left:2px;margin-top:23px;width:268px;height:688px;overflow-y:auto;background-color:rgb(243,243,247);border:solid 1px rgb(222,222,232);position:fixed;\"></div>";
      tr_frm.appendChild(td_frm);
      tbl_frm.appendChild(tr_frm);

      document.getElementById(id).appendChild(tbl_frm);

      add_pot.potlist("include_" + unique[0]);
      add_pot.potsearch("include_" + unique[1]);
   },

   potlist: function() {
      include_potlist(document.getElementById(arguments[0]));
   },

   potsearch: function() {
      include_potsearch(document.getElementById(arguments[0]));
   },
 
   potsave: function() {
      d = document.getElementById("include_" + unique[0]);
      d.innerHTML = "<div style=\"position:fixed; margin-left:2px; margin-top:3px;\">" +
      "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"add_pot.save();\">Save tracker</a></span>" + 
      "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"location.reload();\">Cancel</a></span>" + 
      "<span class=\"phpbutton\" style=\"display:none;\" id=\"pot_all\"><a href=\"javascript:void(0);\" id=\"button_container\" onclick=\"add_pot.use_container(this);\">All POT</a></span>" + 
      "<span id=\"smartsearch.container\" style=\"display:none;\"></span>" + 
      "<div>";
      url = session.remote_database_path + "modules/elements/include_pot/view.php?get=_";
      js = "document.getElementById('pot_all').style.display='inline';";
      run_ajax("smartsearch.container", url, js);
   },

   use_container: function(e) {
      d_body = document.getElementById("smartsearch.body");
      d_container = document.getElementById("smartsearch.container");
      put_container = (d_body.innerHTML).replace(/c>/g, "cc>").replace(/<p/g, "<pp").replace(/p>/g, "pp>");
      get_container = (d_container.innerHTML).replace(/cc>/g, "c>").replace(/<pp/g, "<p").replace(/pp>/g, "p>");
      d_body.innerHTML = get_container;
      d_container.innerHTML = put_container;
      if(e.textContent.toLowerCase() == "all pot") {
         e.textContent = "Project pot";
         smartsearch.min_results = 2000;
      }
      else {
         e.textContent = "All pot";
         smartsearch.min_results = 99999;
      }
      r = document.getElementById("smartsearch.body").getElementsByTagName("p").length; 
      document.getElementById("smartsearch_results").setAttribute("count", r);
      document.getElementById("smartsearch_results").textContent = String(r) + " / " + String(r);
      smartsearch.init("smartsearch.body");
      add_pot.mark_added();
      smartsearch.check_do_filter();
   },

   drop: function(t, e) {
      e.preventDefault();
      data = e.dataTransfer.getData("text/html").split("+");
      pot = (unescape(data[0]).trim() + unescape(data[1]).trim() + unescape(data[2]).trim()).toLowerCase();
      in_tracker = add_pot.in_tracker();
      if(typeof arguments[2] != "undefined") {
         if(!in_array(pot, in_tracker)) {
            el = e.target;
            while(el.tagName.toLowerCase() != "table") el = el.parentNode;
            d = el.getElementsByTagName("td");
            c = confirm("Change proposal \n" + d[1].textContent + " " + d[2].textContent + " " + d[3].textContent + "\nto " + data[0] + " " + data[1] + " " + data[2]);
            if(c) {
               ppot = el.getAttribute("ref_pot").trim();
               html = "<table ref_pot=\"" + unescape(data[3]).trim() + "\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\" style=\"table-layout:fixed;font:normal 13px Open Sans;position:relative;left:-3px;\"><tr>" + 
               "<td style=\"width:19px; padding-left:1px;\"><img id=\"img_" + data[3].trim() + "\"  src=\"../../../../library/images/16x16/red/hardware-40.png\" /></td>" +
               "<td style=\"width:80px;padding-left:5px;\">" + unescape(data[0]) + "</td>" +
               "<td style=\"width:23px;padding-left:2px;text-align:center;\">" + unescape(data[1]) + "</td>" + 
               "<td style=\"width:134px;padding-left:3px;\">" + unescape(data[2]) + "</td>" + 
               "</tr></table>";
               el.outerHTML = html;
               add_pot.replace_proposal_pot(ppot, data[3].trim());
               add_pot.mark_added();
            }
            add_pot.droptime = new Date().getTime();
         }
      }
      else {
         if(typeof add_pot.droptime == "undefined" || add_pot.droptime - new Date().getTime() < -100) {
            if(data[4] != "ungrabbed") {
                add_pot.embed_error("Not ungrabbed!");
            }
            else {
               if(in_array(pot, in_tracker)) {
               add_pot.embed_error("Already added!");
               }
               else {
                  html = "<table ref_pot=\"" + unescape(data[3]).trim() + "\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\" style=\"table-layout:fixed;font:normal 13px Open Sans;position:relative;left:-3px;\"><tr>" + 
                  "<td style=\"width:19px;padding-left:2px;\"><img src=\"../../../../library/images/16x16/edition-43.png\" onclick=\"add_pot.delete(this);\" style=\"cursor:pointer;\" /></td>" +
                  "<td style=\"width:80px;padding-left:2px;\">" + unescape(data[0]) + "</td>" +
                  "<td style=\"width:23px;padding-left:2px;text-align:center;\">" + unescape(data[1]) + "</td>" + 
                  "<td style=\"width:134px;padding-left:2px;\">" + unescape(data[2]) + "</td>" + 
                  "</tr></table>";
                  t.innerHTML += html;
                  add_pot.mark_added();
               }
            }
         }
      }
   },

   embed_error: function() {
      d = document.getElementById("workspace");
      div = document.createElement("div");
      div.setAttribute("style", "position:absolute;top:20px; left:1px; width:1100px; height:780px; background-color:rgb(255,255,255); opacity:0.94;");
      div.innerHTML = 
      "<div style=\"padding-top:14px; padding-left:10px; padding-bottom:10px;\">" + arguments[0] + "</div>" + 
      "<span class=\"phpbutton\" style=\"margin-left:10px;\"><a href=\"javascript:void(0);\" onclick=\"d=this.parentNode.parentNode; d.parentNode.removeChild(d);\">OK</a></span>";
      d.appendChild(div);
   },

   replace_proposal_pot: function(proposal_pot) {
      pot = unescape(arguments[1]);
      tracker = "<?php session_start(); print $_SESSION["tracker"]; ?>";
      url = "addin/run_agent.php?&agent=a.relocate_pot&tracker=" + tracker + "&proposal_pot=" + proposal_pot + "&new_pot="  + pot;
      js = "window.setTimeout('document.getElementById(\\'img_" + arguments[1] + "\\').src=\\'" + session.php_server + "/library/images/16x16/green/hardware-40.png\\'', 1000)";
      run_ajax("run_ajax", url, js);
   },

   
   drag: function(e) {
      e.dataTransfer.setData("text/html", e.target.getAttribute("pid"));
   },

   delete: function(t) {
      while(t.tagName.toLowerCase() != "table") t = t.parentNode;
      t.parentNode.removeChild(t);
      add_pot.mark_added();
   },

   in_tracker: function() {
      tbody = document.getElementById("include_add_pot:pot_area").getElementsByTagName("tbody");
      in_tracker = [];
      for(k in tbody) if(!isNaN(k)) {
         td = tbody[k].getElementsByTagName("td");
         in_tracker.push((td[1].textContent.trim() + td[2].textContent.trim() + td[3].textContent.trim()).toLowerCase());
      }
      return(in_tracker);
   },

   mark_added: function() {
      in_tracker = add_pot.in_tracker();
      p = document.getElementById("smartsearch.body").getElementsByTagName("p");
      for(k in p) if(!isNaN(k)) {
         a = p[k].getElementsByTagName("a");
         key = (a[4].textContent.trim() + a[5].textContent.trim() + a[6].textContent.trim()).toLowerCase();
         dsp = (in_array(key, in_tracker)) ? "Yes" : "No";
         a[0].textContent = dsp;
         if(typeof smartsearch == "object") smartsearch.add_key(p[k]);
      }
   },

   save: function() {
      table = document.getElementById("include_add_pot:pot_area").getElementsByTagName("table");
      ref_pot = [];
      for(k in table) if(!isNaN(k)) {
         ref_pot.push(table[k].getAttribute("ref_pot"));
      }
      unid = (document.getElementById("trackerunid")) ? document.getElementById("trackerunid").textContent.trim() : document.getElementById("workspace").getAttribute("tracker").trim();
      post.save_field("", unid, "ref$pot", ref_pot.join(";"), "add_pot.create_meta_documents('" + ref_pot.join(";") + "')", ";"); 
   },

   create_meta_documents: function() {
      data = "ref$pot=" + escape(arguments[0]) + "&ref$tracker=" + escape("<?php session_start; print $_SESSION["tracker"]; ?>");
      url = "addin/create_document.php?form=f.create.meta";
      js = "location.reload()";
      run_ajax("run_ajax", url, js, data);
   }
}


add_proposal_pot = {

   init_workspace: function(id) {
      tbl = document.createElement("table");
      tbl.setAttribute("id", "tbl_add_proposal_pot");
      tbl.setAttribute("border", "0");
      tbl.setAttribute("cellpadding", "0");
      tbl.setAttribute("cellspacing", "2");

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.textContent = "Proposal part";
      tr.appendChild(td);
      td = document.createElement("td");
      input = document.createElement("input");
      input.setAttribute("value", "");
      input.setAttribute("name", "proposal_part");
      input.setAttribute("maxlength", "20");
      input.setAttribute("onkeyup", "add_proposal_pot.check();");
      input.setAttribute("type", "text");
      td.appendChild(input);
      tr.appendChild(td);
      tbl.appendChild(tr);

      tr = document.createElement("tr");
      td = document.createElement("td");
      td.textContent = "Proposal tool";
      tr.appendChild(td);
      td = document.createElement("td");
      input = document.createElement("input");
      input.setAttribute("value", "");
      input.setAttribute("name", "proposal_tool");
      input.setAttribute("maxlength", "20");
      input.setAttribute("onkeyup", "add_proposal_pot.check();");
      input.setAttribute("type", "text");
      td.appendChild(input);
      tr.appendChild(td);
      tbl.appendChild(tr);

      span = document.createElement("span");
      span.setAttribute("class", "phpbutton");
      span.setAttribute("style", "margin-top:10px; margin-left:2px; display:none;");
      span.setAttribute("id", "button_add_propsal_pot_create");
      a = document.createElement("a");
      a.setAttribute("href", "javascript:add_proposal_pot.create('<?php session_start(); print $_SESSION["tracker"]; ?>');");
      a.textContent = "Create";
      span.appendChild(a);
      td.appendChild(span);
      tr.appendChild(td);

      with(document.getElementById(id)) {
         appendChild(tbl);
         appendChild(span);
      }      
   },

   create: function(unique) {
      part = (document.getElementsByName("proposal_part")[0].value).trim();
      tool = (document.getElementsByName("proposal_tool")[0].value).trim();
      url = "addin/run_agent.php?agent=a.create_proposal_pot&part=" + escape(part) + "&tool=" + escape(tool) + "&unique=" + unique;
      js = "location.reload()";
      run_ajax("run_ajax", url, js);
   },

   check: function() {
      d = document.getElementById("tbl_add_proposal_pot").getElementsByTagName("input");
      create = true;
      for(k in d) if(!isNaN(k)) {
         if(d[k].value.trim() == "") create = false;
      }
      document.getElementById("button_add_propsal_pot_create").style.display = (create) ? "inline-block" : "none"; 
   }
}


add_inspection_pot = {

   p: [],

   init_workspace: function() {
      d = document.getElementById("workspace");
      d.innerHTML = 
      "<span id=\"smartsearch_inspection_pot\">" + 
      "<div id=\"smartsearch.head\">" +
      "<input class=\"readonly\" readonly=\"readonly\" type=\"text\" id=\"smartsearch_search\" value=\"\" onkeydown=\"this.setAttribute('clock', Date.now());\" onkeyup=\"window.setTimeout('smartsearch.check_do_filter()', 800);\" />" + 
      "<span style=\"font:normal 13px Open Sans\">&nbsp;&nbsp;&nbsp;&nbsp;Results:&nbsp;<a id=\"smartsearch_results\" style=\"position:relative;top:5px;font:normal 13px Open Sans\"></a></span>" + 
      "</div>" + 
      "<div id=\"smartsearch.body\" clear=\"true\"><br>Loading ...</div>" +
      "</span>";
      if(add_inspection_pot.p.length == 0) {
         style = document.createElement("style");
         style.textContent = "@import url(\"" + session.php_server + "/library/css5/resource.db/add_inspection_pot.css" + "\")";
         script = document.createElement("script");
         script.setAttribute("src", session.remote_domino_path_main + "/v.get_inspection_pot?open&count=99999&function=plain");
         script.setAttribute("type", "text/javascript");
     
         script.setAttribute("onload", "add_inspection_pot.nest_pot('" + arguments[1] + "')");
 
         document.getElementsByTagName("head")[0].appendChild(style);
         document.getElementsByTagName("head")[0].appendChild(script);
      }
      else add_inspection_pot.nest_pot();
   },

   nest_pot: function() {
      arguments[0] = arguments[0].split(",");
      div = document.createElement("div");
      counter = 0;
      for(k in add_inspection_pot.p) if(!isNaN(k)) {
         v = add_inspection_pot.p[k];
         if(!in_array(v.unique, arguments[0])) {
            counter++;
            p = document.createElement("p");
            p.setAttribute("do", "add_inspection_pot.save('" + arguments[0].join(",") + "," + v.unique + "')");
            for(e in v) if(e != "unique") {        
               a = document.createElement("a");
               a.textContent = unescape(v[e]);
               p.appendChild(a);
            }
            if(k == 0) {
               c = document.createElement("c");
               nme = ["Status", "Part number", "R", "Tool number", "Project", "Case"];
               for(e = 0; e < nme.length; e++) {        
                  a = document.createElement("a");
                  a.textContent = nme[e];
                  c.appendChild(a);
               }
               div.appendChild(c);
            }
            div.appendChild(p);
         }
      }
      document.getElementById("smartsearch.body").innerHTML = div.innerHTML;
      document.getElementById("smartsearch_results").setAttribute("count", counter);
      document.getElementById("smartsearch_results").textContent = String(counter) + " / " + String(counter);
      do_smartsearch("smartsearch.body", 99999);
   },

   save: function() {
      $("#workspace").fadeOut(800);
      data = 
      "&unid=" + escape(document.getElementById("workspace").getAttribute("tracker")) + 
      "&fld1=" + escape("ref$pot/" + arguments[0]) + 
      "&split=" + escape(",");
      post.save_data("f.save_data", "", data, "location.reload()");
   }
}


function include_add_pot() {
   include_css(session.php_server + "/library/css5/te.tools/workspace.css");
   switch(arguments[0]) {
      case "project":
         infobox("Add POT to tracker", "<div id=\"workspace\" tracker=\"" + arguments[1] + "\"></div>", 1100,800);
         window.setTimeout("add_pot.init_workspace('workspace');", 250);
         break;
      case "proposal":
         infobox("Add proposal POT to tracker", "<div id=\"workspace\"></div>", 274,133);
         window.setTimeout("add_proposal_pot.init_workspace('workspace');", 250);
         break;
      case "inspection":
         infobox("Add inspection POT to tracker", "<div id=\"workspace\" tracker=\"" + arguments[1] + "\"></div>", 714,386);
         window.setTimeout("add_inspection_pot.init_workspace('workspace', '" + arguments[2] + "');", 250);
         break;
   }
}

<?php
$path_translated =  str_replace(basename(__FILE__), "", str_replace("\\", "/", __FILE__));
print "include_potlist = function() {_(arguments, \"".base64_encode($path_translated)."\");}\r\n";
print "include_potsearch = function() {_(arguments, \"".base64_encode($path_translated)."\");}\r\n";
?>



