function include_potlist(target) {
   arr = [];
   <?php
   session_start();
   $path_translated = str_replace(basename(__FILE__), "", str_replace("\\", "/", __FILE__));
   require_once(substr($path_translated, 0, strpos($path_translated, "javascript"))."addin/basic_php_functions.php");	
   require_once(substr($path_translated, 0, strpos($path_translated, "app"))."library/tools/addin_xml.php");	
   $tc_data["tracker"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:tracker/".$_SESSION["tracker"]."?open");
   if(isset($tc_data["tracker"]["basic"]["ref_pot_tmp"])) $tc_data["tracker"]["basic"]["ref_pot"][0] = $tc_data["tracker"]["basic"]["ref_pot_tmp"];

   foreach($tc_data["tracker"]["basic"]["ref_pot"] as $pot) {
      $tc_data["pot"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:pot/".$pot."?open");
      print "arr.push('".$tc_data["pot"]["unique"]."+".$tc_data["pot"]["part"]["number"]."+".$tc_data["pot"]["part"]["revision"]."+".$tc_data["pot"]["tool"]["number"]."+".$tc_data["pot"]["is_proposal"]."+".$tc_data["pot"]["pot_status"]."');\r\n";
   }
   ?>
   for(key in arr) {
      content = arr[key].split("+");
      potlist_html(content[1] + "+" + content[2] + "+" + content[3] + "+" + content[4], content[0]);
   }
}


function potlist_html() {
   data = arguments[0].split("+");
   img = (data[3] == "0") ? "../../../../library/images/16x16/hardware-40.png" : "../../../../library/images/icons/plug.png";
   html = "<table ondrop=\"add_pot.drop(this, event, this.getAttribute('ref_pot'), '" + data[3] + "');\" ondragover=\"allow_drop(event);\" ref_pot=\"" + arguments[1] + "\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\" style=\"table-layout:fixed; width:250px; font:normal 13px Open Sans; position:relative; left:-4px;\"><tr>" + 
   "<td style=\"width:19px;padding-left:2px;\"><img src=\"" + img + "\" style=\"width:16px; height:16px;\" /></td>" +
   "<td style=\"width:80px;padding-left:2px; white-space:nowrap; overflow:hidden;\">" + unescape(data[0]) + "</td>" +
   "<td style=\"width:23px;padding-left:2px; text-align:center;\">" + unescape(data[1]) + "</td>" + 
   "<td style=\"width:130px;padding-left:2px;\">" + unescape(data[2]) + "</td>" + 
   "</tr></table>";
   document.getElementById("include_add_pot:pot_area").innerHTML += html;
}











