function include_m_t13__edit_tracker_small() {

   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_t13.edit_tracker_small.php");
   $load = array("tracker", "project");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   require_once($path."/application.ini");

   $html = rawurlencode(m_t13__edit_tracker_small_generate_html($_application));
   print "html = \"".$html."\";\r\n";   
   ?>

   document.getElementById("m_t13__edit_tracker_small_innerhtml").innerHTML = unescape(html);
   uroles.nest("m_t13__edit_tracker_small", document.getElementById("tbl_m_t13__edit_tracker_data_small").outerHTML, "check");
   m_t13.nest_editors();
      
}







