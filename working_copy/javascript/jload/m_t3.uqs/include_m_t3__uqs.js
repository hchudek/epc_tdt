function include_m_t3__uqs() {

   
   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/addin/basic_php_functions.php");
   require_once($path."/modules/m_t3.uqs.php");
   require_once($path."/modules/Elements/PROCESS/rules.php");
   require_once($path."/modules/Elements/PROCESS/types.php");
   $load = array("tracker", "uqs", "meta", "pot");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   require_once($path."/application.ini");

   $html = rawurlencode(m_t3__uqs_generate_html($_application));
   print "html = \"".$html."\";\r\n";   
   ?>

   document.getElementById("m_t3__uqs_innerhtml").innerHTML = unescape(html); 
   
}


m_t3__tracking_case = {
   unid: {
<?php

print "      meta: \"".$tc_data["meta"]["unid"]."\",\r\n";
print "      tracker: \"".$tc_data["tracker"]["unid"]."\",\r\n";
print "      pot: \"".$tc_data["pot"]["unid"]."\",\r\n";
print "   },\r\n";

print "   responsible: \"".$tc_data["tracker"]["responsible"]."\",\r\n";
print "   pot: \"".$_SESSION["pot"]."\",\r\n";

print "   inspection: function() {\r\n";
print "      arr = [];\r\n";
foreach($tc_data["pot"]["inspection"]["tracker"] as $v) print "      arr.push(\"".$v."\");\r\n";
print "      return arr;\r\n";
print "   },\r\n";

?>
   delete: function() {
      inspection = m_t3__tracking_case.inspection();
      if(!arguments[0]) {
         arguments[1] = (arguments[1].trim() != "") ? "false" : "true";    
         if(inspection.length > 0 && arguments[1] == "true") {
            h = "Delete not possible";
            b = "<div style=\"margin-bottom:6px;\">This tracking case is inspected here:</div>";
            for(k in inspection) {
               val = inspection[k].split("=");
               b += "<div><a style=\"text-decoration:none; color:rgb(0,102,158);\" href=\"?&unique=" + val[0] + "&pot=" + m_t3__tracking_case.pot + "\" >Tracker " + val[1] + "</a></div>";
            }
         }
         else {
            if(m_t3__tracking_case.responsible.toLowerCase() != session.domino_user_cn.toLowerCase()) {
               h = "Error";
               b = "<span>You are not authorized to perform this action.</span>";
            }  
            else {
               h = "Are you sure?";
               b = "<div><span>This action wil permanently delete the tracking case<br>and all related date fields.</span><br><br>" + 
               "<span class=\"phpbutton\"><a href=\"javascript:m_t3__tracking_case.delete(true, '" + arguments[1] + "');\">OK</a></span>" + 
               "<span class=\"phpbutton\"><a href=\"javascript:infobox_handle.close(true, 'confirm_box');\">Cancel</a></span></div>";
            }
         }
         infobox(h, b, 380,230);
      }
      else {
         d = document.getElementById("confirm_box").getElementsByTagName("div");
         d[0].innerHTML = "Deleting";
         d[d.length - 1].innerHTML = "<img src=\"../../../../library/images/ajax/ajax-loader.gif\"><span style=\"position:relative;top:-2px; left:6px;\">Deleting ...</span>";
         js = "location.href='?&unique=<?php print $_SESSION["tracker"]; ?>'";
         url = "addin/run_agent.php?agent=a.delete_tracking_case&dates=" + arguments[1] + "&unique=" + m_t3__tracking_case.pot + "&pot=" + m_t3__tracking_case.unid.pot  + "&meta=" + m_t3__tracking_case.unid.meta + "&tracker=" + m_t3__tracking_case.unid.tracker;
         run_ajax("run_ajax", url, js);
      }
   }
}







