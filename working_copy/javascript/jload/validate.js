<?php
$p = str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
include_once(substr($p, 0, strpos($p, "apps"))."library/tools/javascript/validate.js");
?>
validate.table = {
   a2p: {
      code: [
      ],
      strlen: []
   },
   cavity: {
      code: [
         template.number,
         template.number,
         template.number
      ],
      strlen: [1, 2, 3]
   },
   convkitno: {
      code: [
         template.number,
         template.number
      ],
      strlen: [1, 2]
   },
   ctsn: {
      code: [
      ],
      strlen: []
   },
   currentoutput: {
      code: [
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number
      ],
      strlen: [1, 2, 3, 4, 5, 6, 7, 8]
   },
   datesuppevt: {
      code: [
      ],
      strlen: []
   },
   description: {
      code: [
      ],
      strlen: []
   },
   dualsource: {
      code: [
      ],
      strlen: []
   },
   guarantoutput: {
      code: [
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number
      ],
      strlen: [1, 2, 3, 4, 5, 6, 7, 8]
   },
   location_g3: {
      code: [
      ],
      strlen: []
   },
   num_drwrevision: {
      code: [
         template.letter,
         template.number,
         template.number
      ],
      strlen: [1, 2, 3]
   },
   oldtool: {
      code: [
      ],
      strlen: []
   },
   pono: {
      code: [
         [50],
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number
      ],
      strlen: [10]
   },
   position: {
      code: [
         template.number,
         template.number,
         template.number
      ],
      strlen: [1, 2, 3]
   },
   purch_req: {
      code: [
         [50],
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number
      ],
      strlen: [10]
   },
   supplier: {
      code: [
      ],
      strlen: []
   },
   te_cavities: {
      code: [
         template.number,
         template.number,
         template.number,
         template.number
      ],
      strlen: [1, 2, 3, 4]
   },
   te_part_num: {
      code: [
         template.number,
         template.number,
         [45],
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         [45],
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         [45],
         template.number
      ],
      strlen: [18]
   },
   toolspec: {
      code: [
      ],
      strlen: []
   },
   totalcost: {
      code: [
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number,
         template.number
      ],
      strlen: [1, 2, 3, 4, 5, 6, 7]
   },
   tpm: {
      code: [
      ],
      strlen: []
   },
   t_location: {
      code: [
      ],
      strlen: []
   },
}
