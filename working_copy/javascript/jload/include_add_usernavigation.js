<?php

session_start();

$oput = 
"function include_add_usernavigation() {\r\n".
"   html =\r\n".
"   \"<span>\" +\r\n".
"   \"<img id=\\\"img_embedded_usernavigator\\\" src=\\\"".$_SESSION["php_server"]."/library/images/16x16/white/chevron-big-4-01.png\\\" onmouseover=\\\"add_usernavigation.embed(this);\\\" style=\\\"margin-left:2px; position:relative; left:2px; top:2px; cursor:pointer;\\\" />\" +\r\n".
"   \"<br />\" +\r\n".
"   \"<span></span>\" +\r\n".
"   \"</span>\";\r\n".
"   with(document.getElementById(\"add_usernavigation\")) {\r\n".
"      innerHTML = html;\r\n".
"      setAttribute(\"style\", \"padding-right:6px;border-right:dotted 1px rgb(255,255,255); \");\r\n".
"   }\r\n".
"   \$(\"#tbl_nav_bottom\").fadeIn(100);\r\n".
"}\r\n";

print $oput;

?>

add_usernavigation = {
   embed: function(d) {
      d.src = session.php_server + "/library/images/16x16/white/chevron-big-1-01.png";
      linklist = document.createElement("div");
      linklist.setAttribute("id", "embedded_usernavigator");
      linklist.innerHTML = "<?php 
      session_start();
      $homepage = file_get_contents($_SESSION["remote_domino_path"]."/homepage.nsf/f.get_db?readform");
      $homepage = str_replace(array("\r", "\n", "\""), array("", "", "\\\""), $homepage);
      print utf8_encode($homepage);
      ?>";
      with(d.nextSibling.nextSibling) {
         innerHTML = "";
         setAttribute("style", "position:absolute; z-index:999999");
         appendChild(linklist);
      }
      document.getElementById("add_usernavigation").style.background = "#e98300";
   },

   dsp_section: function(e) {
      section = e.id.substr(e.id.lastIndexOf("_") - e.id.length + 1);
      cnt = 0;
      do {
         cnt++;
         d = document.getElementById("nav_section_" + String(cnt));
         d.style.display = (cnt == section) ? "block" : "none";
         with(document.getElementById("frame_section_" + String(cnt)).style) {
            border = (cnt == section) ? "solid 2px rgb(222,222,232)" : "solid 2px rgb(255,255,255)";
            background = (cnt == section) ? "rgb(243,243,247)" : "rgb(255,255,255)";
         }
      } while(d);
   },

   load_linklist: function(e) {
      linklist = e.getAttribute("linklist");
      if(linklist != "") {
         document.getElementById("embed_linklist").innerHTML = "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"position:relative;top:34px;left:25px;\" />";
         url = "addin/embed_linklist.php?linklist=" + escape(linklist);

         run_ajax("embed_linklist", url, "", true);
      }
      else {
         document.getElementById("embed_linklist").textContent = "";
      }
   }
}

document.onmousemover = function(event) {
   if(typeof document.getElementById("embedded_usernavigator") != "object") return false;
   with(document.getElementById("embedded_usernavigator")) {
      setAttribute("x", event.pageX);
      setAttribute("y", event.pageY);
   }
   embedded_usernavigator = document.getElementById("embedded_usernavigator");
   if(Number(embedded_usernavigator.getAttribute('x')) > ($("#embedded_usernavigator").width() + 7) || Number(embedded_usernavigator.getAttribute('y')) > ($("#embedded_usernavigator").height() + 140)  || Number(embedded_usernavigator.getAttribute('y')) < 105) { 
     $("#embedded_usernavigator").fadeOut(200);
      document.getElementById("img_embedded_usernavigator").src = session.php_server + "/library/images/16x16/white/chevron-big-4-01.png";
      document.getElementById("add_usernavigation").style.background = "#aeaeae";
   }
}