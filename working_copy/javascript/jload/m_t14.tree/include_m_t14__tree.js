var _reload_tree;
function include_m_t14__tree() {

   <?php
		   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_t14.tree.php");
   $load = array("pot", "part");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   require_once($path."/application.ini");

   $unique = "";
   $pot = "";
   try {
	   if( isset( $_REQUEST["params"]) ){
		   $hlp = urldecode($_REQUEST["params"]);
		   $params = explode( "&", $hlp );
	
		   foreach( $params as $param ){
			   $p = explode( "=", $param );
				   if( $p[0] == "unique" ){
					  $unique = $p[1]; 
				   }
				   if( $p[0] === "pot" ){
					  $pot = $p[1]; 
				   }
			   
		   }
		  
		   
	   }
   } catch (Exception $e) {
	    echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
   }

   $html = rawurlencode(m_t14__tree_generate_html($_application, $unique, $pot ));
   print "html = \"".$html."\";\r\n";   
   ?>

   document.getElementById("m_t14__tree_innerhtml").innerHTML = unescape(html);
   m_t14.initJSTree();
   
}

