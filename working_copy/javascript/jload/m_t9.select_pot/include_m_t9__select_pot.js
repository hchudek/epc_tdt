function include_m_t9__select_pot() {

   if(typeof arguments[0] == "undefined") arguments[0] = "tbl_m_t9__select_pot";
   table = document.getElementById(arguments[0]);
   table.parentNode.removeChild(table.parentNode.getElementsByTagName("load")[0]);

   arr = [];

   <?php
   session_start();

   require_once("../../../../library/tools/addin_xml.php");	
							

   $tc_data["tracker"] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:tracker/".$_SESSION["tracker"]."?open");
   if(isset($tc_data["tracker"]["basic"]["ref_pot_tmp"])) $tc_data["tracker"]["basic"]["ref_pot"][0] = $tc_data["tracker"]["basic"]["ref_pot_tmp"];

   $count = 0;
   foreach($tc_data["tracker"]["basic"]["ref_pot"] as $pot) {
      $tc_data["pot"][$count] = generate_xml($_SESSION["remote_domino_path_main"]."/v.xml:pot/".trim($pot)."?open");
      $load_pot[$count] = (!isset($tc_data["pot"][$count]["unid"])) ? array($pot, "?", "", "?") : array($pot, $tc_data["pot"][$count]["part"]["number"], $tc_data["pot"][$count]["part"]["revision"], $tc_data["pot"][$count]["tool"]["number"]);
      $count++;
   }

   foreach ($load_pot as $sort) $sort_array[] = $sort[intval($_SESSION["perpage"]["tab"]["tracker"]["m_t9__select_pot_sortkey"]) - 1];
   array_multisort($sort_array, SORT_ASC, SORT_STRING, $load_pot);


   foreach($load_pot as $key => $val) {
      print "arr.push(\"".implode("+", $val)."\");\r\n";
   }

   ?>


   m_over = 
   "if(this.className != 'selected_pot') {" +
   "   this.style.background='rgb(243,243,247)';" + 
   "   with(this.firstChild.childNodes[0].style) {borderLeft='solid 1px rgb(222,222,232)'; borderTop='solid 1px rgb(222,222,232)'; borderBottom='solid 1px rgb(222,222,232)'}" +
   "   with(this.firstChild.childNodes[1].style) {borderTop='solid 1px rgb(222,222,232)'; borderBottom='solid 1px rgb(222,222,232)'}" +
   "   with(this.firstChild.childNodes[2].style) {borderRight='solid 1px rgb(222,222,232)'; borderTop='solid 1px rgb(222,222,232)'; borderBottom='solid 1px rgb(222,222,232)'}" + 
   "}";

   m_out = 
   "if(this.className != 'selected_pot') {" +
   "   this.style.background='rgb(255,255,255)';" + 
   "   with(this.firstChild.childNodes[0].style) {borderLeft='solid 1px rgb(255,255,255)'; borderTop='solid 1px rgb(255,255,255)'; borderBottom='solid 1px rgb(255,255,255)'}" +
   "   with(this.firstChild.childNodes[1].style) {borderTop='solid 1px rgb(255,255,255)'; borderBottom='solid 1px rgb(255,255,255)'}" +
   "   with(this.firstChild.childNodes[2].style) {borderRight='solid 1px rgb(255,255,255)'; borderTop='solid 1px rgb(255,255,255)'; borderBottom='solid 1px rgb(255,255,255)'}" + 
   "}";

   for(key in arr) {
      content = arr[key].split("+");
      tbody = document.createElement("tbody");
      tbody.setAttribute("style", "display:table-row-group;");
      tbody.setAttribute("key", content.join(" ").toLowerCase());
      tbody.setAttribute("onmouseover", m_over);
      tbody.setAttribute("onmouseout", m_out);
      if(content[0] == "<?php print $_SESSION["pot"]; ?>" && "<?php print $_SESSION["pot"]; ?>" != "") tbody.setAttribute("class", "selected_pot");
      else tbody.setAttribute("class", "unselected_pot");
      tbody.setAttribute("onclick", "do_fade(true, true); location.href='?&unique=<?php print $_SESSION["tracker"]; ?>&pot=" + content[0] + "';");
      tr = document.createElement("tr");
      for(c in content) { 
         if(c > 0) {
            ct = (content[c].substr(0, 8) == "PROPOSAL") ? "<span style=\"color:rgb(0,102,158);\">"+content[c].substr(8, content[c].length) + "</span>": content[c];
            td = document.createElement("td");
            td.innerHTML = ct;
            tr.appendChild(td);
         }
      }
      tbody.appendChild(tr);
      table.appendChild(tbody);
   }
}





