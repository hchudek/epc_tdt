function include_m_d1__cr_v1() {

   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_d1.cr_v1.php");
   $load = array("pot", "part");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   require_once($path."/application.ini");

   $html = rawurlencode(m_d1__cr_v1_generate_html($_application));
   print "html = \"".$html."\";\r\n";   
   ?>

   document.getElementById("m_d1__cr_v1").innerHTML = unescape(html);
   
}







