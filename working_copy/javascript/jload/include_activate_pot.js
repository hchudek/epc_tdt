<?php
header("Content-Type: application/javascript");
?>

activate_pot = [];

activate_pot[0] = function () {
   if(!document.getElementById("tbl_activate_pot_0")) {
      process = [];  
      ptype = [];
      process_is_active = [];
      dsp_process_name = [];

      <?php
      session_start();
      $file = str_replace("\\", "/", __FILE__);
      $path = substr($file, 0, strpos($file, "javascript"));
      require_once($path."/modules/elements/PROCESS/rules.php");
      require_once($path."/modules/elements/PROCESS/subsprocesses.php");
      require_once($path."/modules/elements/PROCESS/types".rawurlencode($_SESSION["process_type"]).".php");
      require_once($path."/addin/application_corrections.php");

      $keys = array("is_active", "displayed_name", "prv_process", "bind_to_rule", "active_next", "active_button_text", "active_date", "type");

      //foreach($get_keys as $val) print "alert('".$val."');\r\n";

      foreach($_application["process"]["bind_to_rule"] as $key => $val) {
         $bind_to_rule = explode(":", $val);         
         if(!in_array(strtolower($_SESSION["process_rule"]), $bind_to_rule) && $val != "") {
            foreach($keys as $k) unset($_application["process"][$k][$key]);
         }
      }



      foreach($_application["process"]["subprocess"] as $key => $val) {
         print "   process[\"".$key."\"] = \"".substr($val, 0, strrpos($val, ":"))."\"\r\n";
      }
      foreach($_application["process"]["type"] as $key => $val) {
         print "ptype[\"".$key."\"] = \"".implode(":",$val)."\";\r\n";
         print "process_is_active[\"".$key."\"] = \"".$_application["process"]["is_active"][$key]."\";\r\n";
         print "dsp_process_name[\"".$key."\"] = \"".$_application["process"]["displayed_name"][$key]."\";\r\n";
      }
      ?>

      for(key in ptype) ptype[key] = ptype[key].split(":");

      tbl_frm = document.createElement("table");
      tbl_frm.setAttribute("id", "tbl_activate_pot_0");
      tbl_frm.setAttribute("style", "display:none;");
      tbl_frm.setAttribute("border", "0");
      tbl_frm.setAttribute("cellpadding", "0");
      tbl_frm.setAttribute("cellspacing", "0");
      tbl_frm.setAttribute("class", "tbl_frm");

      tr_frm = document.createElement("tr");
      td_frm = document.createElement("td");
      html = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"tbl_inner\" style=\"border:solid 2px rgb(255,255,255);width:210px;\"><tr><th align=\"left\" style=\"padding-left:2px;font-weight:bold;\">Subprocess&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr>";
      for(key in process) {
         css = (key.substr(-3) == "990") ? "dsp_process_high" : "dsp_process_reg";
         html += "<tr><td align=\"left\" style=\"height:21px;\" class=\"" + css + "\"><span style=\"position:relative;top:1px;left:2px;\">[" + key.replace(/G0/g, "G") + "]" + process[key] + "</span></td></tr>";
      }
      html += "</table>";

      td_frm.setAttribute("style", "position:relative;top:2px;");
      td_frm.innerHTML = html;
      tr_frm.appendChild(td_frm);
      tbl_frm.appendChild(tr_frm);

      for(process_type in ptype) {
         if(process_is_active[process_type] != "0") {
               if(process_type.indexOf("v->") != -1) {
               process_version = (process_type.split("v->")).reverse();
               dsp_process_type = process_version[process_version.length - 1];
               bgcol = "background-color:rgb(239,228,176);"
            }
            else {
               process_version = new Array("1");
               dsp_process_type = process_type;
               bgcol = "background-color:transparent;"
            }

            td_frm = document.createElement("td");

            a = document.createElement("f");
            a.setAttribute("style", bgcol);
            a.setAttribute("onclick", "activate_pot.handle_workspace(1, \"process_type\", \"" + escape(dsp_process_type) + "\", \"" + escape(process_type) + "\")");

            tbl_inner = document.createElement("table");
            tbl_inner.setAttribute("border", "0");
            tbl_inner.setAttribute("cellpadding", "0");
            tbl_inner.setAttribute("cellspacing", "0");
            tbl_inner.setAttribute("class", "tbl_inner");

            tr_inner = document.createElement("tr");
            tbl_inner.appendChild(tr_inner);

            th_inner = document.createElement("th");
            th_inner.innerHTML = "<nobr id=\"" + escape(dsp_process_type).replace(/\//g, "").replace(/%/g, "").toLowerCase() + "\">" + dsp_process_name[process_type].trim() + "</nobr>";
            th_inner.setAttribute("style", "font-weight:bold; border-color:transparent;");
            tr_inner.appendChild(th_inner);

            for(key in process) {
               tr_inner = document.createElement("tr");
               tbl_inner.appendChild(tr_inner);
               td_inner = document.createElement("td");
               td_inner.setAttribute("align", "center");
               style = (in_array(key, ptype[process_type])) ? "background-color:rgb(40,215,92)" : "background-color:rgb(209,85,82)";
               td_inner.innerHTML = "<div style=\"border:solid 1px rgb(99,99,99);" + style + ";width:14px;height:14px;border-radius:8px;position:relative;top:4px;\">&nbsp;</div>";
               td_inner.setAttribute("style", "font-weight:normal; border-color:transparent;");
               tr_inner.appendChild(td_inner);
            }
            a.appendChild(tbl_inner);
            td_frm.appendChild(a);
            tr_frm.appendChild(td_frm);
         }
      }
      document.getElementById("workspace_activate_pot").appendChild(tbl_frm);
   }
   return "Select Process Type"; 
}

activate_pot[1] = function () {

   if(!document.getElementById("tbl_activate_pot_1")) {
      process = [];  
      ptype = [];
      get_ptype = [];
      prule = [];
      rbound = [];
      is_active = [];
      tool_base = [];
      dsp_process_rule = [];

      <?php
      session_start();
      $file = str_replace("\\", "/", __FILE__);
      $path = substr($file, 0, strpos($file, "javascript"));
      require_once($path."/modules/elements/PROCESS/rules.php");
      require_once($path."/modules/elements/PROCESS/subsprocesses.php");
      require_once($path."/modules/elements/PROCESS/types".rawurlencode($_SESSION["process_type"]).".php");
      require_once($path."/addin/application_corrections.php");
      foreach($_application["process"]["subprocess"] as $key => $val) {
         print "   process[\"".$key."\"] = \"".substr($val, 0, strrpos($val, ":"))."\"\r\n";
      }

      $arr = array_keys($_application["process"]["rules"]);
      foreach($arr as $val) {
         unset($tmp);
         foreach($_application["process"]["rules"][$val] as $key => $v) {
            $tmp[] = $key."=".str_replace(array("+ ", "G0"), array("+", "G"), $v["from_id"])." [".$v["operation"]."]";
         }

         print "prule[\"".$val."\"] = \"".implode("//", $tmp)."//\";\r\n";
         print "ptype[\"".$val."\"] = \"".implode(":", array_keys($_application["process"]["rules"][$val]))."\";\r\n";
         print "rbound[\"".$val."\"] = \"".implode(":::", $_application["process"]["rules"][$val]["bound_to"])."\";\r\n";
         print "is_active[\"".$val."\"] = \"".$_application["process"]["rules"][$val]["filter"]["is_active"]."\";\r\n";
         print "tool_base[\"".$val."\"] = \"".$_application["process"]["rules"][$val]["filter"]["process_tool"]."\";\r\n";
         print "dsp_process_rule[\"".$val."\"] = \"".$_application["process"]["rules"][$val]["displayed_name"]."\";\r\n";
      }

      foreach($_application["process"]["type"] as $key => $val) {
         print "get_ptype[\"".$key."\"] = \"".implode(":",$val)."\";\r\n";
      }

      ?>

      for(key in ptype) {
         ptype[key] = ptype[key].split(":");
      }
      tbl_frm = document.createElement("table");
      tbl_frm.setAttribute("id", "tbl_activate_pot_1");
      tbl_frm.setAttribute("style", "display:none;");
      tbl_frm.setAttribute("border", "0");
      tbl_frm.setAttribute("cellpadding", "0");
      tbl_frm.setAttribute("cellspacing", "0");
      tbl_frm.setAttribute("class", "tbl_frm");

      tr_frm = document.createElement("tr");
      td_frm = document.createElement("td");
      html = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"tbl_inner\" style=\"border:solid 2px rgb(255,255,255);width:210px;\"><tr><th align=\"left\" style=\"padding-left:2px;font-weight:bold;\">Subprocess&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr>";
      for(key in process) {
         css = (key.substr(-3) == "990") ? "dsp_process_high" : "dsp_process_reg";
         style = (in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")].split(":"))) ? "" : "display:none;";
         html += "<tr style=\"" + style + "\"><td align=\"left\" style=\"height:21px;\" class=\"" + css + "\"><span style=\"position:relative;top:1px;left:2px;\">[" + key.replace(/G0/g, "G") + "] " + process[key] + "</span></td></tr>";
      }
      html += "</table>";

      td_frm.setAttribute("style", "position:relative;top:2px;");
      td_frm.innerHTML = html;
      tr_frm.appendChild(td_frm);
      tbl_frm.appendChild(tr_frm);

      for(process_rule in ptype) {

         t_process_type = document.getElementById("process_type").getAttribute("value");
         t_tool = session.tool_base;

         if(in_array(t_process_type, rbound[process_rule].split(":::")) && is_active[process_rule] != "0" && (in_array(t_tool, tool_base[process_rule].split(";")))) {

            td_frm = document.createElement("td");
   
            a = document.createElement("f");
            a.setAttribute("onclick", "activate_pot.handle_workspace(2, \"process_rule\", \"" + escape(process_rule) + "\")");

            tbl_inner = document.createElement("table");
            tbl_inner.setAttribute("border", "0");
            tbl_inner.setAttribute("cellpadding", "0");
            tbl_inner.setAttribute("cellspacing", "0");
            tbl_inner.setAttribute("class", "tbl_inner");

            tr_inner = document.createElement("tr");
            tbl_inner.appendChild(tr_inner);

            th_inner = document.createElement("th");
            th_inner.innerHTML = "<nobr><b value=\"" + process_rule + "\" id=\"" + escape(process_rule).replace(/\//g, "").replace(/%/g, "").toLowerCase() + "\">" + dsp_process_rule[process_rule] + "</b></nobr>";
            tr_inner.appendChild(th_inner);

            for(key in process) {
               style = (in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")].split(":"))) ? "" : "display:none;";
               tr_inner = document.createElement("tr");
               tr_inner.setAttribute("style", style);
               tbl_inner.appendChild(tr_inner);
               td_inner = document.createElement("td");
               td_inner.setAttribute("align", "center");
               strng = prule[process_rule].substr(prule[process_rule].indexOf(key + "=") + key.length + 1, prule[process_rule].length);
               strng = strng.substr(0, strng.indexOf("//"));
               strng = (strng.indexOf("[+ 0]") > - 1)  ? "-" : strng; 
               td_inner.innerHTML = "<div style=\"height:21px;position:relative;top:1px;\">" + strng + "</div>";
               tr_inner.appendChild(td_inner);
            }
            a.appendChild(tbl_inner);
            td_frm.appendChild(a);
            tr_frm.appendChild(td_frm);
         }
      }
      document.getElementById("workspace_activate_pot").appendChild(tbl_frm);
   }
   return "Select Process Rule"; 
}

activate_pot[2] = function () {

   if(!document.getElementById("tbl_activate_pot_2")) {
      process = [];  
      ptype = [];
      get_ptype = [];
      in_process = [];

      <?php
      session_start();
      $file = str_replace("\\", "/", __FILE__);
      $path = substr($file, 0, strpos($file, "javascript"));
      require_once($path."/modules/elements/PROCESS/rules.php");
      require_once($path."/modules/elements/PROCESS/subsprocesses.php");
      require_once($path."/modules/elements/PROCESS/types".rawurlencode($_SESSION["process_type"]).".php");
      require_once($path."/addin/application_corrections.php");
      foreach($_application["process"]["subprocess"] as $key => $val) {
         print "   process[\"".$key."\"] = \"".substr($val, 0, strrpos($val, ":"))."\"\r\n";
      }
      foreach($_application["process"]["type"] as $key => $val) {
         print "ptype[\"".$key."\"] = \"".implode(":",$val)."\";\r\n";
      }
      ?>


      for(key in ptype) ptype[key] = ptype[key].split(":");
      get_ptype = ptype;
      tbl_frm = document.createElement("table");
      tbl_frm.setAttribute("id", "tbl_activate_pot_2");
      tbl_frm.setAttribute("style", "display:none;");
      tbl_frm.setAttribute("border", "0");
      tbl_frm.setAttribute("cellpadding", "0");
      tbl_frm.setAttribute("cellspacing", "0");
      tbl_frm.setAttribute("class", "tbl_frm");

      tr_frm = document.createElement("tr");
      td_frm = document.createElement("td");
      html = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"tbl_subprocess\" class=\"tbl_inner\" style=\"border:solid 2px rgb(255,255,255);width:210px;\"><tr><th align=\"left\" style=\"padding-left:2px;font-weight:bold;\">Subprocess&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr>";
      first_key = false;
      for(key in process) {
         css = (key.substr(-3) == "990") ? "dsp_process_high" : "dsp_process_reg";
         style = (in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")])) ? "" : "display:none;";
         gen_key = (in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")])) ? "<a style=\"display:none;\" name=\"_key\">" + key + "</a>" : "";
         html += "<tr style=\"" + style + "\"><td align=\"left\" style=\"height:21px;\" class=\"" + css + "\" subprocess=\"" + key + "\"><span style=\"position:relative;top:1px;left:2px;\">[" + key.replace(/G0/g, "G") + "] " + process[key] + "</span>" + gen_key + "</td></tr>";
      }
      html += "</table>";

      td_frm.setAttribute("style", "position:relative;top:2px;");
      td_frm.innerHTML = html;
      tr_frm.appendChild(td_frm);
      tbl_frm.appendChild(tr_frm);

      process_type = document.getElementById("process_type").getAttribute("value");
      bgcol = "background-color:transparent; margin-top:2px; display:inline-block;"
      if(process_type.indexOf("v->") != -1) {
         process_version = (process_type.split("v->")).reverse();
         dsp_process_type = process_version[process_version.length - 1];
      }
      else {
         process_version = new Array("1");
         dsp_process_type = process_type;
      }

      td_frm = document.createElement("td");

      a = document.createElement("c");
      a.setAttribute("style", bgcol);

      tbl_inner = document.createElement("table");
      tbl_inner.setAttribute("border", "0");
      tbl_inner.setAttribute("cellpadding", "0");
      tbl_inner.setAttribute("cellspacing", "0");
      tbl_inner.setAttribute("class", "tbl_inner");

      tr_inner = document.createElement("tr");
      tbl_inner.appendChild(tr_inner);

      th_inner = document.createElement("th");
      th_inner.innerHTML = "<nobr id=\"nest_process_type\">" + dsp_process_type.trim() + "</nobr>";
      th_inner.setAttribute("style", "font-weight:bold; border-color:transparent;");
      tr_inner.appendChild(th_inner);
      for(key in process) {

         tr_inner = document.createElement("tr");
         tbl_inner.appendChild(tr_inner);
         td_inner = document.createElement("td");
         td_inner.setAttribute("align", "center");
         style = (in_array(key, ptype[process_type])) ? "background-color:rgb(40,215,92)" : "background-color:rgb(209,85,82)";
         in_process[key] = (in_array(key, ptype[process_type])) ? "true" : "false";
         if(in_process[key] == "false") tr_inner.setAttribute("style", "display:none");
         td_inner.innerHTML = "<div style=\"border:solid 1px rgb(99,99,99);" + style + ";width:14px;height:14px;border-radius:8px;position:relative;top:4px;\" id=\"in_process_" + key + "\" in_process=\"" + in_process[key] + "\">&nbsp;</div>";
         td_inner.setAttribute("style", "font-weight:normal; border-color:transparent;");
         tr_inner.appendChild(td_inner);
      }
      a.appendChild(tbl_inner);
      td_frm.appendChild(a);
      tr_frm.appendChild(td_frm);

      process = [];  
      ptype = [];
      prule = [];
      rbound = [];
      t_process_rule = document.getElementById("process_rule").getAttribute("value");
      <?php
      session_start();
      $file = str_replace("\\", "/", __FILE__);
      $path = substr($file, 0, strpos($file, "javascript"));
      require_once($path."/modules/elements/PROCESS/rules.php");
      require_once($path."/modules/elements/PROCESS/subsprocesses.php");
      require_once($path."/modules/elements/PROCESS/types".rawurlencode($_SESSION["process_type"]).".php");
      require_once($path."/addin/application_corrections.php");
      foreach($_application["process"]["subprocess"] as $key => $val) {
         print "   process[\"".$key."\"] = \"".substr($val, 0, strrpos($val, ":"))."\"\r\n";
      }

      $arr = array_keys($_application["process"]["rules"]);
      foreach($arr as $val) {
         unset($tmp);
         foreach($_application["process"]["rules"][$val] as $key => $v) {
            $tmp[] = $key."=".str_replace(array("+ ", "G0"), array("+", "G"), $v["from_id"])." [".$v["operation"]."]";
         }
         print "prule[\"".$val."\"] = \"".implode("//", $tmp)."//\";\r\n";
         print "ptype[\"".$val."\"] = \"".implode(":", array_keys($_application["process"]["rules"][$val]))."\";\r\n";      
      }
      ?>

      for(key in ptype) ptype[key] = ptype[key].split(":");

      td_frm = document.createElement("td");
   
      a = document.createElement("c");
      bgcol = "background-color:transparent; margin-top:2px; display:inline-block;"
      a.setAttribute("style", bgcol);

      tbl_inner = document.createElement("table");
      tbl_inner.setAttribute("border", "0");
      tbl_inner.setAttribute("cellpadding", "0");
      tbl_inner.setAttribute("cellspacing", "0");
      tbl_inner.setAttribute("class", "tbl_inner");

      tr_inner = document.createElement("tr");
      tbl_inner.appendChild(tr_inner);

      th_inner = document.createElement("th");
      th_inner.innerHTML = "<nobr><b id=\"nest_process_rule\">" + t_process_rule + "</b></nobr>";
      tr_inner.appendChild(th_inner);
      for(key in process) {
         style = (in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")])) ? "" : "display:none;";
         tr_inner = document.createElement("tr");
         tr_inner.setAttribute("style", style);
         tbl_inner.appendChild(tr_inner);
         td_inner = document.createElement("td");
         td_inner.setAttribute("align", "center");
         strng = prule[t_process_rule].substr(prule[t_process_rule].indexOf(key + "=") + key.length + 1, prule[t_process_rule].length);
         strng = strng.substr(0, strng.indexOf("//"));
         strng = (strng.indexOf("[+ 0]") > - 1)  ? "-" : strng; 
         calc = strng.split("[").reverse();
         calc[0] = calc[0].replace("]", "").replace(/ /g, "");
         calc[1] = (calc[1]) ? calc[1].replace("G", "G0").trim() : "undefined";
         td_inner.innerHTML = "<div style=\"height:21px;position:relative;top:5px;\"><a style=\"border:solid 2px transparent;\" name=\"do_calc\" id=\"calc_" + key + "\" calc=\"" + key + "@" + calc[1] + "@" + calc[0].trim() + "\">" + strng + "</a></div>";
         tr_inner.appendChild(td_inner);
      }
      a.appendChild(tbl_inner);
      td_frm.appendChild(a);
      tr_frm.appendChild(td_frm);

      // Dates
      date = [];
      <?php
      $file = str_replace("\\", "/", __FILE__);
      $path = substr($file, 0, strpos($file, "apps"));
      require_once($path."/library/tools/addin_xml.php");
      $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.get_dates_by_project?open&restricttocategory=".$_SESSION["project_number"]."&function=plain");
      if(strpos("No documents found") == -1) print $file;
      ?>

      td_frm = document.createElement("td");
   
      a = document.createElement("c");
      bgcol = "background-color:transparent; margin-top:2px; display:inline-block;"
      a.setAttribute("style", bgcol);

      tbl_inner = document.createElement("table");
      tbl_inner.setAttribute("border", "0");
      tbl_inner.setAttribute("cellpadding", "0");
      tbl_inner.setAttribute("cellspacing", "0");
      tbl_inner.setAttribute("class", "tbl_inner");

      tr_inner = document.createElement("tr");
      tbl_inner.appendChild(tr_inner);

      th_inner = document.createElement("th");
      th_inner.setAttribute("style", "text-align:left;padding-left:59px;");
      th_inner.innerHTML = "<nobr><b>Timeline</b></nobr><div id=\"responsibles_container\" style=\"display:none;\"></div>";
      tr_inner.appendChild(th_inner);
      keys = [];
      first_date = false;
      for(key in process) {

         style = (in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")])) ? "" : "display:none;";
         tr_inner = document.createElement("tr");
         tr_inner.setAttribute("style", style);
         tbl_inner.appendChild(tr_inner);
         td_inner = document.createElement("td");
         td_inner.setAttribute("align", "center")
         v = (typeof date[key] == "string") ? date[key].trim() : "";
         if(key.substr(-3) != "990" && in_process[key] == "true"  && first_date == false) {
            first_date = true;
            tbl_frm.setAttribute("first_date", key);
            td_inner.innerHTML = 
            "<div id=\"calendar2\" style=\"position:absolute;display:none;border:none;margin:0px;padding:0px;z-index:99999;\"></div>" + 
            "<div style=\"height:21px;position:relative;top:1px;\">" + 
            "<img anchor=\"calendar_activate_tracking_case\" src=\"" + session.php_server + "/library/images/16x16/time-3.png\" style=\"cursor:pointer;margin-right:4px;position:relative;top:3px;left:-2px;visibility:hidden;\" key=\""+ key + "\" onclick=\"activate_pot.load_calendar(this, 'date_" + key + "', 'activate_pot.set_start_date', 'Select date', this, '', true);\">" + 
            "<input locked=\"false\" readonly=\"readonly\" value=\"\" style=\"position:relative;top:1px;font:normal 13px Open Sans; padding:1px; width:70px; border:none; color:rgb(0,102,158);\" id=\"date_" + key + "\" name=\"date_anchor\">" + 
            "</div>";
            do_eval = "activate_pot.set_start_date('date_" + key + "', '<?php print date("Y", mktime()); ?>', '<?php print date("m", mktime()); ?>', '<?php print date("d", mktime()); ?>', '0', '')";
         }
         else {
            td_inner.innerHTML = "<div style=\"height:21px;position:relative;top:1px;\">" + 
            "<b style=\"position:absolute;\"><img anchor=\"calendar_activate_tracking_case\" src=\"" + session.php_server + "/library/images/16x16/time-3.png\" style=\"cursor:pointer;position:relative;top:2px;left:28px;margin-right:4px;visibility:hidden;\" key=\""+ key + "\" onclick=\"activate_pot.load_calendar(this, 'date_" + key + "', 'activate_pot.set_start_date', 'Select date', this, '' , true);\"></b>" + 
            "<input locked=\"false\" readonly=\"readonly\" value=\"" + v + "\" style=\"position:relative;top:1px;margin-left:50px;font:normal 13px Open Sans; padding:1px; width:100px; border:none;\" id=\"date_" + key + "\">" + 
            "</div>";
         }
         tr_inner.appendChild(td_inner);
         keys.push(key);
      }
      a.appendChild(tbl_inner);
      td_frm.appendChild(a);
      tr_frm.appendChild(td_frm);
      document.getElementById("workspace_activate_pot").appendChild(tbl_frm);


      // RESPONSIBLES 
      td_frm = document.createElement("td");
   
      a = document.createElement("c");
      bgcol = "background-color:transparent; margin-top:2px; display:inline-block;"
      a.setAttribute("style", bgcol);

      tbl_inner = document.createElement("table");
      tbl_inner.setAttribute("border", "0");
      tbl_inner.setAttribute("cellpadding", "0");
      tbl_inner.setAttribute("cellspacing", "0");
      tbl_inner.setAttribute("class", "tbl_inner");

      tr_inner = document.createElement("tr");
      tbl_inner.appendChild(tr_inner);

      th_inner = document.createElement("th");
      th_inner.setAttribute("style", "text-align:left;");
      th_inner.innerHTML = "<nobr><b>Responsible</b></nobr><div id=\"responsibles_container\" style=\"display:none;\"></div>";
      tr_inner.appendChild(th_inner);
      keys = [];
      for(key in process) {

         style = (in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")])) ? "" : "display:none;";
         tr_inner = document.createElement("tr");
         tr_inner.setAttribute("style", style);
         tbl_inner.appendChild(tr_inner);
         td_inner = document.createElement("td");
         td_inner.setAttribute("align", "center");

         td_inner.innerHTML = "<div style=\"height:21px;position:relative;top:1px;\" class=\"ui-widget\"><input onkeyup=\"activate_pot.evaluate(true);\" value=\"\" readonly=\"readonly\" style=\"font:normal 13px Open Sans; padding:1px; width:200px;border:solid 1px rgb(142,142,147);border:solid 1px rgB(222,222,222);\" id=\"responsible_" + key + "\"></div>";
         tr_inner.appendChild(td_inner);
         keys.push(key);

      }
      a.appendChild(tbl_inner);
      td_frm.appendChild(a);
      tr_frm.appendChild(td_frm);

      d = document.getElementById("process_type").getAttribute("value");
      if(typeof d == "string") {
         if(d.substr(-4) != "v->2") document.getElementById("workspace_activate_pot").style.visibility = "hidden";
      }

      document.getElementById("workspace_activate_pot").appendChild(tbl_frm);

      nest_autocomplete(process, keys, 0);

   }
   if(typeof do_eval == "string") {

      eval(do_eval);
      a = document.querySelectorAll("[anchor='calendar_activate_tracking_case']");
      for(k in a) if(!isNaN(k)) {
         with(a[k]) {
            style.visibility = (String(app_timeline[t_process_rule][getAttribute("key")]) == "1") ? "visible" : "hidden";
         }
      }
   }
   document.getElementById("nest_process_type").textContent = document.getElementById("dsp_process_type").textContent.replace("[", "").replace("]", "");
   document.getElementById("nest_process_rule").textContent = document.getElementById("dsp_process_rule").textContent.replace("[", "").replace("]", "");
   return "Activate"; 
}

<?php
$arr = array();
foreach($_application["process"]["rules"] as $key => $type) {
   foreach($type as $phase => $timeline) if(substr($phase, 0 ,1) == "G") $arr[$key][$phase] = $timeline["edittimeline"];
}
print "app_timeline = ".json_encode($arr);

?>

nest_autocomplete_data = {};

nest_autocomplete = function() {

   process = (arguments[0] == false) ? process : arguments[0];
   keys = (arguments[1] == false) ? keys : arguments[1];

   count = arguments[2];
   if(count < keys.length) {
      key = keys[count];
      js = "nest_autocomplete(false, false, " + (count + 1) + ");";
      if(document.getElementById("in_process_" + key).getAttribute("in_process") == "true") {
      url = session.remote_database_path + "addin/run_ajax.php?" + escape(session.remote_domino_path_main +"/v.get_responsibilities?open&count=9999&restricttocategory=" + escape(key + " " + document.getElementById("process_type").textContent) + "&function=plain");
      if(keys[count].substr(-3) != "990") {
         js = 
           "$(function() {" + 
         "   data = [];" +
         "   d = (document.getElementById('responsibles_container').textContent).trim();" +
         "   document.getElementById('responsible_" + key + "').removeAttribute('readonly');" +
         "   document.getElementById('responsible_" + key + "').style.borderColor='rgb(142,142,147)';" +
         "   if(d.toLowerCase() != 'no documents found') eval(d);" +  
         "   document.getElementById('responsible_" + key + "').setAttribute('responsibles', data.join(';'));" +
         "   nest_autocomplete_data['" + key + "'] = data;" +
         "   if(nest_autocomplete_data['" + key + "'].length == 0) {" + 
         "      document.getElementById('responsible_" + key + "').value = '<?php session_start(); print "*CONFIG*"; ?>'; " +
         "      document.getElementById('responsible_" + key + "').setAttribute('readonly', 'readonly');" +
         "   }" + 
         "   document.getElementById('responsible_" + key + "').setAttribute('onkeyup', 'this.style.background = (in_array(this.value, nest_autocomplete_data[\\'" + key + "\\'])) ? \\'rgb(214,227,66)\\' : \\'rgb(255,255,255)\\'');" +
         "   $('#responsible_" + key + "').autocomplete({" + 
         "      source: nest_autocomplete_data['" + key + "']," +
         "      select: function() { document.getElementById('responsible_" + key + "').style.background = 'rgb(214,227,66)'; }," +
         "      close: function() {" + 
         "         activate_pot.evaluate(true);" + 
         "      }" +  
         "   });" + 
         "});" + js;
      }
      else {
         with(document.getElementById("responsible_" + keys[count]).style) {
            backgroundColor = "rgb(247,247,247)";
         }
      }
         run_ajax("responsibles_container", url, js);
      }
      else {
         eval(js);
      }
   }   
}


activate_pot.evaluate = function() {
   d = document.getElementById("process_activate");
   if(d) {
      chk = true;
      pr = document.getElementById("process_rule").getAttribute("value");
      if(typeof pr != "string") chk = false;
      pt = document.getElementById("process_type").getAttribute("value");
      if(typeof pt != "string") chk = false;
      else {
        if(pt.substr(-4) == "v->2") {
           if(arguments[0]) {
              dk = document.getElementById("tbl_subprocess").getElementsByTagName("td");
              for(i = 0; i < dk.length; i++) {
                 kid = dk[i].getAttribute("subprocess");
                 if(typeof document.getElementById("responsible_" + kid).getAttribute("readonly") != "string") {
                    if(!in_array((document.getElementById("responsible_" + kid).value).trim(), document.getElementById("responsible_" + kid).getAttribute("responsibles").split(";"))) chk = false;
                 }
              }
              if(document.getElementsByName("date_anchor")[0].value.trim() == "") chk = false;
           }
           else chk = false;
         }
      }
      dsp = (chk) ? "inline-block": "none";
      window.setTimeout("document.getElementById('process_activate').style.display = '" + dsp + "'", 800);
   }
}



activate_pot.handle_workspace = function () {
   f = (typeof arguments[1] == "undefined") ? false : arguments[1];
   txt = (typeof arguments[2] == "undefined") ? false : unescape(arguments[2]);
   v = (typeof arguments[3] == "undefined") ? txt : unescape(arguments[3]);
   if(v != false && f != false) {
      with(document.getElementById(f)) {
         textContent = txt;
         setAttribute("is_set", "1");
         setAttribute("value", v);
      }
   }
   if(f == "process_type") {
      txt_id = escape(txt).replace(/\//g, "").replace(/%/g, "").toLowerCase();
      document.getElementById("dsp_" + f).textContent = "[" +document.getElementById(txt_id).textContent + "]";
      v = v.split("v->").reverse();
      with(document.getElementById("process_responsibles")) {
         style.color = (v[0] == "2") ? "rgb(0,0,0)" : "rgb(142,142,147)";
         previousSibling.style.color = (v[0] == "2") ? "rgb(0,0,0)" : "rgb(142,142,147)";
      }
   }
   if(f == "process_rule") {
      txt_id = escape(txt).replace(/\//g, "").replace(/%/g, "").toLowerCase();
      document.getElementById("dsp_" + f).textContent = "[" +document.getElementById(txt_id).textContent + "]";
   }
   for(i = 0; i <= 2; i++) {
      dr = document.getElementById("tbl_activate_pot_" + String(i));
      if(dr) dr.parentNode.removeChild(dr);
   }
   workspace = document.getElementById("workspace");
   fld = workspace.getElementsByTagName("span");
   do_activate = true;
   for(key in fld) {
      if(!isNaN(key)) {
         if(fld[key].getAttribute("is_set") == null) {
            do_activate = false;
            break;
         }
      }
   }
   document.getElementById("workspace_step").textContent = activate_pot[arguments[0]]();
   for(i = 0; i <= 100; i++) {
      if(document.getElementById("tbl_activate_pot_" + String(i))) {
         $("#tbl_activate_pot_" + String(i)).fadeOut(100);
      }
   }
   if(document.getElementById("tbl_activate_pot_" + String(arguments[0]))) {
      window.setTimeout('$("#tbl_activate_pot_' + String(arguments[0]) + '").fadeIn(400);', 400);
   }
   activate_pot.evaluate();
}






activate_pot.init_workspace = function () {
   document.getElementById("workspace").innerHTML = 
   "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%;\">" +
   "<tr>" +
   "<td colspan=\"2\" style=\"font:normal 27px Open Sans;padding-bottom:16px; height:54px;\"><div style=\"z-index:999999; width:1270px; height:60px; padding-top:10px; background-color:rgb(255,255,255); position:fixed;\"><?php $dsp = (trim($_SESSION["process_type"]) == "") ? "Activate Tracking Case" : "Finalize Tracking Case"; print $dsp; ?>:&nbsp;<span id=\"workspace_step\">Select Process Type</span></div></td>" +
   "</tr>" +
   "<tr>" +
   "<td style=\"vertical-align:top;white-space:nowrap;\">" +
   "<span style=\"position:relative;top:-10px;left:-10px;z-index:9999;\">" +
   "<span style=\"position:fixed;background-color:rgb(255,255,255);opacity:0.95;width:168px;height:620px;padding-left:20px;margin-top:5px;\">" + 
   "<div><b>Process type</b></div>" +
   "<span id=\"process_type\" onclick=\"\" style=\"cursor:default;\">Not set</span><br />" +
   "<span id=\"dsp_process_type\" onclick=\"\" style=\"cursor:default;\"></span>" +
   "<div style=\"height:16px;\">&nbsp;</div>" +
   "<div><b>Process rule</b></div>" +
   "<span id=\"process_rule\" onclick=\"\" style=\"cursor:default;\">Not set</span><br />" +
   "<span id=\"dsp_process_rule\" onclick=\"\" style=\"cursor:default;\"></span>" +
   "<div style=\"height:16px;\">&nbsp;</div>" +
   "<div style=\"color:rgb(142,142,147);display:none;\"><b>Activate</b></div>" +
   "<span style=\"color:rgb(142,142,147);display:none;\" id=\"process_responsibles\" onclick=\"activate_pot.handle_workspace(2);\" style=\"cursor:pointer;\">To be done</span>" +
   "<span class=\"phpbutton\" id=\"process_activate\" style=\"display:none;\"><a href=\"javascript:activate_pot.collect_data();\">Activate</a></span>" +
   "<div style=\"height:16px;\">&nbsp;</div>" +
   "</span>" +
   "<div style=\"width:180px;\">&nbsp;</div>" + 
   "</td>" +
   "<td id=\"workspace_activate_pot\" style=\"vertical-align:top;text-align:left;width:99%;\"></td>" +
   "</tr>" +
   "</table>";
   activate_pot.handle_workspace(0);
}




activate_pot.collect_data = function () {

   d = document.getElementById("tbl_subprocess").getElementsByTagName("td");
   save_data = [];
   today = "<?php print date("Y-m-d", mktime()); ?>";
   kill = [];
   for(i = 0; i < d.length; i++) {
      if(typeof d[i].getAttribute("subprocess") == "string") {
         key = d[i].getAttribute("subprocess");
         if(in_array(key, get_ptype[document.getElementById("process_type").getAttribute("value")])) {
            kill.push(key); 
         }
         process_previous = document.getElementById("calc_" + key).textContent.replace("G", "G0").trim();
         locked = document.getElementById("date_" + key).getAttribute("locked");
         date_value = document.getElementById("date_" + key).value.trim();
         date_type = "date_planned";
         if(date_value.indexOf("A") > -1) date_type = "date_actual";
         if(date_value.indexOf("R") > -1) date_type = "date_planned";
         date_value = date_value.substr(0, 10);
         process_status = (key == document.getElementById("tbl_activate_pot_2").getAttribute("first_date")) ? "20" : "10";
         if(document.getElementById("activate_next_process")) {
            a_date = document.getElementById("activate_next_process").getAttribute("activate_date");
            a_date = a_date.substr(a_date.lastIndexOf("_") + 1, a_date.length);
            g_start = (document.getElementById("calc_" + key).textContent.replace("G", "G0").trim()).split(" ");
            if(a_date.toUpperCase() == g_start[0].toUpperCase()) {
               process_status = "20";
            }
         }
         if(process_status == "20") {
            if(Number(date_value.replace(/-/g, "")) - Number(today.replace(/-/g, "")) < 0) process_status = "30";
         }
         pt = document.getElementById("process_type").textContent;
         pr = document.getElementById("process_rule").textContent;
         if(date_value != "") date_value += "@" + today + "@" + session.domino_user; 
         responsible = document.getElementById("responsible_" + key).value;
         save_data.push("&href=" + escape("<?php session_start(); print $_SESSION["remote_database_path"]; ?>") + "&process_type=" + escape(pt) + "&process_rule=" + escape(pr) + "&process_status=" + escape(process_status) + "&process_previous=" + escape(process_previous) + "&phase=" + escape(key) + "&" + date_type + "=" + escape(date_value) + "&responsible=" + escape(responsible) + "&ref$pot=" + escape(session.pot) + "&ref$tracker=" + escape(session.tracker) + "&locked=" + escape(locked));
      }
   }

   <?php
   if($_SESSION["process_type"] == "") {
      print "activate_pot.save_data(save_data.join(\";\"));\r\n";
   }
   else {
      print "js = \"activate_pot.save_data('\" + save_data.join(\";\") + \"');\";\r\n";
      print "url = \"addin/run_agent.php?agent=a.kill_date&kill=\" + kill.join(\":\") + \"&tracker=".$_SESSION["tracker"]."&pot=".$_SESSION["pot"]."\";\r\n";

      print "activate_pot.run_ajax(\"run_ajax\", url, js);\r\n";
   }
   ?>
}


activate_pot.save_data = function(save_data) {
   save_data = save_data.split(";");
   num = (typeof arguments[1] == "undefined") ? 0 : arguments[1];
   if(num == 0) {
      with(document.getElementById("workspace")) {
         innerHTML = "<div id=\"activate_pot.pt\" style=\"display:none;\">" + document.getElementById("process_type").getAttribute("value").trim() + "</div>" +
         "<div id=\"activate_pot.pr\" style=\"display:none;\" >" + document.getElementById("process_rule").getAttribute("value").trim() + "</div>" +
         "<img src=\"" + session.php_server + "/library/images/ajax/ajax-loader.gif\" style=\"vertical-align:top; position:relative; top:10px;\" /><span style=\"position:relative;top:8px;left:7px;\" id=\"process_info\"></span>";
         setAttribute("style", "background-color:rgb(255,255,255);color:rgb(0,0,0); display:block;");
         removeAttribute("class");
      }
   } 
   if(save_data.length > num) {
      document.getElementById("process_info").textContent = "Processing [" + ("00" + String(num + 1)).substr(-3) + " / " + ("00" + String(save_data.length)).substr(-3) + "] ...";
      url = session.remote_database_path + "addin/create_document.php?form=f.date";
      js = "activate_pot.save_data('" + save_data.join(";") + "', " + (num + 1) + ")";
      activate_pot.run_ajax("run_ajax", url, js, save_data[num]);
   }
   else {
      activate_pot.post_save_data();
   }
}


activate_pot.post_save_data = function() {
   chain_pt = document.getElementById("activate_pot.pt").textContent;
   chain_pr = document.getElementById("activate_pot.pr").textContent;
   if(document.getElementById("activate_next_process")) {
      chain_pt = document.getElementById("activate_next_process").getAttribute("chain_pt") + ",," + chain_pt;
      chain_pr = document.getElementById("activate_next_process").getAttribute("chain_pr") + ",," + chain_pr;
   }
   <?php
   session_start();
   require_once("../../../../library/tools/addin_xml.php");							
   $meta = process_xml($_SESSION["remote_domino_path_main"]."/v.meta_by_pot?open&count=1&restricttocategory=".$_SESSION["pot"]."@".$_SESSION["tracker"]."&function=xml:meta");
   print "      data = \"unid=".$meta["unid"]."\" +\r\n";
   ?>
   "&fld1=process_type/" + escape(document.getElementById("activate_pot.pt").textContent) +
   "&fld2=process_rules/" + escape(document.getElementById("activate_pot.pr").textContent) +
   "&fld3=chain_process_type/" + escape(chain_pt) +
   "&fld4=chain_process_rules/" + escape(chain_pr);
   form = "f.save_data";
   db = session.remote_domino_path_main;
   js = "$('#workspace').fadeOut(400); window.setTimeout('location.reload()', 1)";
   url = session.remote_database_path + "addin/save_multiple_fields.php?form=" + escape(form) + "&db=" + escape(db);
   activate_pot.run_ajax("run_ajax", url, js, data);  
}



activate_pot.run_ajax = function(_id,_file,_runJS) {
   _post = (typeof arguments[3] == "undefined") ? null : arguments[3]; 
   req = null;
   try { req = new XMLHttpRequest(); }
   catch (ms) {
      try { req = new ActiveXObject("Msxml2.XMLHTTP"); }
      finally {}
   }
   if (req == null) eval("document.getElementById(_id).innerHTML='error creating request object';");
   else req.open("post",_file, true);
   req.onreadystatechange = function() {
      switch(req.readyState) {
         case 4:
           document.getElementById(_id).innerHTML = req.responseText; 
           if(_runJS != "undefined") eval(_runJS); 
            break;
         default:
            return false; 
            break;
      }
   }
   req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
   req.send(_post);
}


activate_pot.load_calendar = function(e, f, fct, txt) {

   if(typeof arguments[4] == "undefined") {x = 0; y = 0}
   else {
      p = $(arguments[4]);
      position = $(arguments[4].parentNode).offset();
      x = position.left - 160;
      y = position.top - 178;
   }
   v = (typeof arguments[6] == "undefined") ? "" : arguments[6];
   d = document.getElementById("calendar2");
   d.innerHTML = "";  
   url = session.remote_database_path + "addin/calendar_new.php?p_function=" + fct + "&txt=" + escape(txt) + "&p_use_rule=0&p_id=" + f + "&p_data_type=" + v;
   pos = $(e).position();
   d.style.top = (pos.top + y) + "px";
   d.style.left = (pos.left + x) + "px";
   run_ajax("calendar2", url, "$('#calendar2').fadeIn(200);");
}

activate_pot.init_start_date = false;

activate_pot.set_start_date = function(f, y, m, d) {

   document.getElementById(f).value = y + "-" + m + "-" + d;
   if(activate_pot.init_start_date) {
      document.getElementById(f).style.color = "rgb(189,51,50)";
      document.getElementById(f).setAttribute("locked", "true");
   }
   else {
      activate_pot.init_start_date = true;
   }
   if(document.getElementById("activate_next_process")) {
      if(String(arguments[5]) == "") { 
         _y = document.getElementById(document.getElementById("activate_next_process").getAttribute("activate_date")).getAttribute("value");
         if(_y.length == 10) {
            add = document.getElementById("calc_" + document.getElementsByName("_key")[0].textContent).textContent;
            add = Number(add.substr(add.indexOf("["), add.length).replace(/\[/g, "").replace(/\]/, "").replace(/\+/g, "").trim());
            document.getElementById(f).value = calc_date(_y.split("-"), add).join("-");
         }
      }
   }
   $("#calendar2").fadeOut(200);
   activate_pot.calculate(f);
   activate_pot.evaluate(true);
}

activate_pot.calculate = function(f) {
   d = document.getElementsByName("do_calc");
   start = false;
   arr = [];

   for(k in d) {
      if(!isNaN(k)) {
         if(start == true) arr.push(d[k].getAttribute("calc"));
         if(d[k].getAttribute("id") == f.replace("date", "calc")) {
            start = true;
         }
      }
   }
   g_start ="none";
   if(document.getElementById("activate_next_process")) {
      g_start = (document.getElementById("calc_" + document.getElementsByName("_key")[0].textContent).textContent.replace("G", "G0")).split(" ");
      g_date = document.getElementById(document.getElementById("activate_next_process").getAttribute("activate_date")).getAttribute("value");
   }
   for(k in arr) {
      calc_timeline = arr[k].split("@");
      t = document.getElementById("date_" + calc_timeline[0]);
      if(calc_timeline[1] != "undefined" && calc_timeline[2].trim() != "") {
         if(calc_timeline[1].toUpperCase().trim() == g_start[0].toUpperCase().trim()) {
            g_add = document.getElementById("calc_" + calc_timeline[0]).textContent;
            g_add = Number(g_add.substr(g_add.indexOf("["), g_add.length).replace(/\[/g, "").replace(/\]/, "").replace(/\+/g, "").trim());
            t.value = calc_date(g_date.split("-"), g_add).join("-");
         }
         else {
            s_date = ((document.getElementById("date_" + calc_timeline[1]).value).substr(0, 10)).split("-");
            t.value = (calc_date(s_date, calc_timeline[2])).join("-").replace("NaN-aN-aN", "*CONFIG*");
         }
         if(t.style.color == "") t.style.color = "rgb(0,102,158)"; 
      }
   }

}


function calc_date() {
   calc_t_date_parse = Date.parse(arguments[0].join("-"));
   for(i = 0; i < Number(arguments[1]); i++) {
      calc_t_date_parse += 86400000;
      calc_t_date = new Date(calc_t_date_parse);
      if(calc_t_date.getDay() == 0 || calc_t_date.getDay() == 6) i--;
   }  
   return [String(calc_t_date.getFullYear()), ("0" + String(calc_t_date.getMonth() + 1)).substr(-2), ("0" + String(calc_t_date.getDate())).substr(-2)];
}



function include_activate_pot() {
   do_fade(true);
   prv_process = (typeof arguments[0] == "undefined") ? "" : arguments[0];
   include_css(session.php_server + "/library/css5/te.tools/workspace.css");
   infobox("<?php $dsp = (trim($_SESSION["process_type"]) == "") ? "Activate Tracking Case" : "Finalize Tracking Case"; print $dsp; ?>", "<div id=\"workspace\" prv_process=\"" + prv_process + "\"></div>", 1300,766);
   js = "include_css(session.php_server + '/library/css5/te.tools/jquery-ui.css'); activate_pot.init_workspace();";
   if(!document.getElementById("jquery-ui")) {
      head = document.getElementsByTagName("head")[0];   
      script = document.createElement("script");
      script.setAttribute("src", session.php_server + "/library/tools/javascript/jquery-ui.js");
      script.setAttribute("type", "text/javascript");
      script.setAttribute("id", "jquery-ui");
      script.setAttribute("onload", js);
      head.appendChild(script);
   }
   else {
      eval(js);
   }
}






