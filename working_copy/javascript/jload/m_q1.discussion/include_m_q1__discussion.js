function include_m_q1__discussion() {

   <?php
   session_start();

   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_q1.discussion.php");
   require_once($path."/addin/basic_php_functions.php");
   $load = array("project");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   $_SESSION["project"]  = $tc_data["project"]["number"];
   require_once($path."/application.ini");
   $html = rawurlencode(m_q1__discussion_generate_html($_application));
   print "html = \"".$html."\";\r\n";

   ?>
   document.getElementById("m_q1__discussion_innerhtml").innerHTML = unescape(html);
}


m_q1__discussion = {
   add: function() {
      resource = document.getElementById("tbl_m_q1__discussion").getAttribute("resource");
      url = (typeof arguments[3] == "undefined") ? session.php_server + "/library/tools/lotus/f.web_resource.php?h=" + base64_encode("Create web resource") + "&category=td," + resource : arguments[3];

      dsp_type = (typeof arguments[1] == "undefined" || typeof arguments[3] != "undefined") ? "" : "r";
      switch(dsp_type) {
         case "r":
            e = arguments[2];
            pos = $(e).position();
            s = $(document.getElementById('m_c2_container').parentNode).scrollTop()
            x = pos.top - s;
            y = pos.left - 288;
            style = "display:none;position:fixed;top:" + String(x) + "px;left:" + String(y) + "px;opacity:0.96;z-index:999999;width:610px;height:400px;";
           break;
         default:
           style = "display:none;position:fixed;top:20px;left:20px;opacity:0.96;z-index:999999;width:" + String(window.innerWidth - 60) + "px;height:" + String(window.innerHeight - 60) + "px;";
           break;
      }

      ifrm = document.createElement("iframe");
      ifrm.setAttribute("src", url);
      ifrm.setAttribute("style", style);
      ifrm.setAttribute("scrolling", "no");
      ifrm.setAttribute("marginheight", "0");
      ifrm.setAttribute("marginwidth", "0");
      ifrm.setAttribute("frameborder", "0");
      ifrm.setAttribute("id", "ifrm");
      document.getElementsByTagName("body")[0].appendChild(ifrm);
      do_fade(true);
      $(ifrm).fadeIn(800);

   },

   embed_resource: function(unid) {
      url = session.php_server + "/library/tools/lotus/f.dsp.web_resource.php?html=" + escape(session.remote_domino_path_resource + "/v.get_html/" + unid + "?open&php_server=" + session.php_server);
      m_q1__discussion.add("", "", "", url);
   },

   select_folder: function(e) {
      do_fade(true);
      document.getElementsByName(e.getAttribute("target"))[0].value = e[e.selectedIndex].value;
      save_perpage("", "location.reload()");
   }, 

   reply_resource: function(unid, s) {
      m_q1__discussion.close_iframe(true);
      url = session.php_server + "/library/tools/lotus/f.web_resource.php?unid=" + unid + "&s=" + s + "&h=" + base64_encode("Reply to web resource");
      m_q1__discussion.add("", "", "", url);
   }, 

   close_iframe: function() {
      ac = (typeof arguments[0] == "undefined") ? false : arguments[0];
      if(ac) {
         ifrm.parentNode.removeChild(ifrm);
         d = document.getElementById("confirm_box");
         if(d) {
            $(d).fadeOut(400, function() {d.parentNode.removeChild(d);});
         }
         do_fade(false);
      }
      else {
         $(ifrm).fadeOut(400, function() {
            embed_confirm("close_ifrm(true);", "d=document.getElementById('confirm_box');$('#ifrm').fadeIn(400);$(d).fadeOut(400, function() {d.parentNode.removeChild(d);})");
         });
      }
   },

   embed_confirm: function() {
      div = document.createElement("div");
      div.setAttribute("id", "confirm_box");
      document.getElementsByTagName("body")[0].appendChild(div);
      p_left = Math.round(window.innerWidth / 2) - Math.round(div.offsetWidth / 2);
      p_top = Math.round(window.innerHeight / 2) - Math.round(div.offsetHeight / 2);
      div.setAttribute("style", "left:" + p_left + "px;top:" + p_top + "px;");
      h = document.createElement("div");
      h.setAttribute("class", "header");
      h.textContent = "Are you sure?";
      div.appendChild(h);
      b = document.createElement("div");
      b.innerHTML = "<div class=\"body\">All changes will be lost.</div>" +
      "<div class=\"button\" style=\"position:relative; top:-11px;\">" +  
      "<span class=\"phpbutton\"><a href=\"javascript:" + arguments[0] + "\">Yes</a></span>" +
      "<span class=\"phpbutton\"><a href=\"javascript:" + arguments[1] + "\">No</a></span>" + 
      "</div>";
      div.appendChild(b);
   }

}

reply_resource = function() {
   m_q1__discussion.reply_resource(arguments[0], arguments[1]);
}


close_ifrm = function() {
    m_q1__discussion.close_iframe(arguments[0]);
}

embed_confirm = function() {
   m_q1__discussion.embed_confirm(arguments[0], arguments[1]);
}





