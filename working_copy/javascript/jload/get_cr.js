<?php
session_start();
ob_start("ob_gzhandler");											
header("Content-Type: text/html; charset=UTF-8"); 
require_once("../../../../../library/tools/addin_xml.php");

?>
m_d1.create = function() {
   for(k in arguments[0]) {
      table = document.createElement("table");
      table.setAttribute("border", "0");
      table.setAttribute("anchor", arguments[0][k]);
      tr = document.createElement("tr");
      table.appendChild(tr);
      for(e in m_d1.table[arguments[0][k]].h) {
         if(e > 1) {
            th = document.createElement("th");
            th.textContent = m_d1.table[arguments[0][k]].h[e];
            tr.appendChild(th);
         }
      }

      if(typeof m_d1.table[arguments[0][k]].b != "undefined") {
         for(e in m_d1.table[arguments[0][k]].b) {
            tr = document.createElement("tr");
            tr.setAttribute("searchkey", m_d1.table[arguments[0][k]].b[e][0]);
            table.appendChild(tr);
            for(t in m_d1.table[arguments[0][k]].b[e]) {
               if(t > 2) {
                  if(typeof m_d1.table[arguments[0][k]].h[t] == "string") {
                     if(m_d1.table[arguments[0][k]].h[t].toLowerCase() == "loop") {
                        if(unescape(m_d1.table[arguments[0][k]].b[e][Number(t) + 2]).toLowerCase() != "transfer error" && unescape(m_d1.table[arguments[0][k]].b[e][Number(t) + 2]).toLowerCase() != "withdrawn") {
                           n = Number(unescape(m_d1.table[arguments[0][k]].b[e][Number(t) + 1]));
                           l = Number(unescape(m_d1.table[arguments[0][k]].b[e][Number(t)]));
                           l = (isNaN(l)) ? 0 : l - 1;
                           if(!isNaN(n)) m_d1.cr_loop[l].push(n);
                        }
                     }
                  }
                  td = document.createElement("td");
                  td.innerHTML = unescape(m_d1.table[arguments[0][k]].b[e][t]);
                  if(td.innerHTML.toLowerCase() == "transfer error") td.innerHTML = "<span style=\"display:inline-block; width:100%; background:rgb(189,52,0); color:rgb(255,255,255);\">&nbsp;" + td.innerHTML + "</span>";
                  tr.appendChild(td);
               }
            }
         }
         with(document.querySelector("[form='m_d1__cr_v1']")) {
            appendChild(table);
            appendChild(document.createElement("br"));
         }
      }
   }
}

m_d1.nest = function() {
   js = (document.querySelector("[form='m_d1__cr_v1']").getAttribute("archived") != "") ? "infobox('ERROR', 'Tracker already archived.', 200, 70);" : "m_d1.cr_nest();";
   if(document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_rms") == "1" && js.indexOf("m_d1.cr_nest") != -1) {
      if(in_array(tool.plant.value, tool.workcenter.plant)) {
         if(tool.workcenter.value == "") js = "infobox('ERROR', 'Workcenter not set!', 200, 70);";
      }
   }
   input = document.createElement("input");
   input.setAttribute("value", "");
   input.setAttribute("onkeyup", "helper.search(this, document.querySelector('[form=\\'m_d1__cr_v1\\']').getElementsByTagName('table')[1]); helper.search(this, document.querySelector('[form=\\'m_d1__cr_v1\\']').getElementsByTagName('table')[2])");
   visibility = (document.getElementById("m_x3__workflow")) ? "visible" : "hidden";
   h = document.createElement("table");
   h.setAttribute("border", "0");
   h.setAttribute("cellpadding", "0");
   h.setAttribute("cellspacing", "0");
   h.setAttribute("style", "width:974px;");
   btn_txt = (document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_rms") == "1") ? "Create Control report (RMS)": "Create control report (ER)";
   h.innerHTML = "<tr><td style=\"width:24%;\">" + input.outerHTML + "</td>" +
   "<td><span anchor=\"create_new_control_report\" class=\"phpbutton\" style=\"position:relative;left:3px; top:-6px; visibility:" + visibility + ";\"><a href=\"javascript:" + js + "\" anchor=\"m_d1_click\">" + btn_txt + "</a></span></td></tr></table>";
   document.querySelector("[form='m_d1__cr_v1']").appendChild(h);
   m_d1.create(arguments[0]);
}

<?php
$file = trim(file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.unid_by_unique/".$_REQUEST["pot"]."?open&count=9999"));
if($file == "" || strpos($file, "No documents found")) { print "m_d1.create = function() { return false; }"; die; };
$arr = (array) json_decode($file) ;
$h = explode(":", $arr["tmp"]);
$th = explode(";", $h[0]);

foreach($th as $v) {
   $cr[] = "\"".trim(substr($v, 0, strpos($v, "|")))."\"";
   $rms[] = "\"".trim(substr($v, strpos($v, "|") + 1, strlen($v)))."\"";
}
unset($h[0]);
print "m_d1.table.rms = {};\r\n";
print "m_d1.table.creport = {};\r\n";
print "m_d1.table.rms.h = [".implode($rms, ", ") ."];\r\n";
print "m_d1.table.creport.h = [".implode($cr, ", ") ."];\r\n";

foreach($h as $t) {
   $td = explode(";", $t);
   $add = $td[1];
   unset($searchkey);
   foreach($td as $k => $v) {
      $td[$k] = "\"".$v."\"";
      if($k != "UNID") $searchkey[] = strip_tags(rawurldecode($v));
   }
   if(trim($add) != "") $d[$add][] = "[\"".rawurlencode(strtolower(trim(implode(" ", $searchkey))))."\", ".implode(", ", $td)."]";
}

foreach($d as $k => $v) {
   print "m_d1.table.".$k.".b = [\r\n   ".implode($v, ",\r\n   ")."\r\n];\r\n";
}
unset($arr["tmp"]);
foreach($arr as $k => $v) print "m_d1.".$k." = \"".$v."\";\r\n";
?>

