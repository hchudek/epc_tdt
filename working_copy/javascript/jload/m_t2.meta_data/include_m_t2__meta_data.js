function include_m_t2__meta_data() {

   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_t2.meta_data.php");
   $load = array("meta");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   require_once($path."/application.ini");

   $html = rawurlencode(m_t2__meta_data_generate_html($_application));
   print "html = \"".$html."\";\r\n";   
   ?>

   document.getElementById("m_t2__meta_data_innerhtml").innerHTML = unescape(html);
   
}







