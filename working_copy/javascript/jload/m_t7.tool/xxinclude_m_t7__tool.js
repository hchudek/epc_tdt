<?php
session_start();
$file = str_replace("\\", "/", __FILE__);
$path = substr($file, 0, strpos($file, "javascript"));
require_once(substr($file, 0, strpos($file, "apps"))."library/tools/addin_xml.php");
?>
m_t7__tool = {
   set_checkbox: function(e) {
      p = session.php_server + "/library/images/16x16/";
      if(typeof arguments[1] == "object") {
         m_t7__tool[e.getAttribute("field")] = (e.firstChild.src.indexOf("status-11-1") > -1) ? arguments[1][0] : arguments[1][1];
      }
      else {
         m_t7__tool[e.getAttribute("field")] = (e.firstChild.src.indexOf("status-11-1") > -1) ? 1 : 0;
      }
      if(e.getAttribute("field") == "prodlocconf") {
         if(e.firstChild.src.indexOf("status-11-1") == -1 && !in_array("[superuser]", session.remote_userroles.split(":"))) return false;
         if(e.firstChild.src.indexOf("status-11-1") != -1 && !in_array("[procurement]", session.remote_userroles.split(":"))) return false;
      }
      e.firstChild.src = (e.firstChild.src.indexOf("status-11-1") > -1) ? p + "status-11.png" : p + "status-11-1.png";
      js = (e.getAttribute("field") == "a2p" && m_t7__tool[e.getAttribute("field")] == 1) ? "m_t7__tool.set_ctsn(true)" : ""; 
      if(e.getAttribute("field") == "dualsource") {
         js = "location.reload()";
         do_fade(true);
      }
      post.save_field(session.remote_domino_path_epcmain, e.getAttribute("unid"), e.getAttribute("field"), m_t7__tool[e.getAttribute("field")], js);
   },
  
   set_ctsn: function() {
      e = document.getElementById("tool:ctsn");
      e.firstChild.src = session.php_server + "/library/images/16x16/status-11.png";
      m_t7__tool[e.getAttribute("field")] = 1;
      post.save_field(session.remote_domino_path_epcmain, e.getAttribute("unid"), e.getAttribute("field"), m_t7__tool[e.getAttribute("field")]);
   },

   handle_container: function(e) {
      e.src = (e.src.indexOf("status-11-1") == -1) ? session.php_server + "/library/images/16x16/status-11-1.png" : session.php_server + "/library/images/16x16//status-11.png"; 
      arr = [];
      d = document.getElementsByName(e.getAttribute("name"));
      for(k = 0; k < d.length; k++) {
         if(d[k].src.indexOf("status-11-1") == -1) arr.push(k + 1);
      }
      document.getElementsByName("m_t7__tool_dsp")[0].value = arr.join(",");
      save_perpage("", true);
   },

<?php
require_once($path."modules/elements/plants/plants.php");
foreach($_application["plant"]["name"] as $key => $val) {
   if(strtolower($_application["plant"]["name"][$key]) != "select outside vendor2") {
      $oput["int"][] = "[\"".rawurlencode($_application["plant"]["name"][$key])."\", \"".rawurlencode($_application["plant"]["mail"][$key])."\"]";
      $oput["all"][] = "\"".rawurlencode($_application["plant"]["name"][$key])."\"";
   }
}
  print "   te_supplier: [".implode($oput["int"], ", ")."],\r\n\r\n";    
  print "   supplier: [".implode($oput["all"], ", ")."],\r\n";
   ?>

   nest_input: function() {
      t = [];
      for(k in m_t7__tool.supplier) if(!isNaN(k)) {
         m_t7__tool.supplier[k] = unescape(m_t7__tool.supplier[k]);
      }
      m_t7__tool.supplier_full = [];
      for(k in ssl.supplier) {
         m_t7__tool.supplier_full.push(ssl.supplier[k].vendor + " [" + ssl.supplier[k].id + "]");
      }
      m_t7__tool.supplier_full = m_t7__tool.supplier.concat(m_t7__tool.supplier_full);
      if(!document.getElementsByName("transferred_location")[0].hasAttribute("readonly")) {
         $("input[name='transferred_location']").autocomplete({
            source: m_t7__tool.supplier_full,      
            select: function(event, ui) {
               do_fade(true);
               post.save_field(session.remote_domino_path_epcmain, m_t7__tool.unid.tool, this.name, ui.item.value.trim());
               location.reload();
            },  
         });
      }
      if(!document.getElementsByName("production_location")[0].hasAttribute("readonly")) {
         $("input[name='production_location']").autocomplete({
            source: m_t7__tool.supplier_full,   
            select: function(event, ui) {
               do_fade(true); 
               m_t7__tool.g3mail.mail_c9 = ui.item.value.trim();
               m_t7__tool.g3mail.mail_sendto = m_t7__mail[ui.item.value.trim()];
alert(ui.item.value.trim());
               post.save_field(session.remote_domino_path_epcmain, m_t7__tool.unid.tool, this.name, ui.item.value.trim(), "m_t7__tool.g3mail.send()");
            },  
         });
      }
      if(document.getElementById("tbl_m_t7__tool").getAttribute("tpm") == session.domino_user_cn) {
         script = document.createElement("script");
         script.setAttribute("src", session.php_server + "/apps/epc/ssl/javascript/jload/json.supplier.js");
         script.setAttribute("onload", "m_t7__tool.nest_tool()");
         document.getElementsByTagName("head")[0].appendChild(script);
      }
   },

   nest_tool: function() {
      m_t7__tool.sslsupplier = [];
      for(k in ssl.supplier) {
         m_t7__tool.sslsupplier.push(ssl.supplier[k].vendor + " [" + ssl.supplier[k].id + "]");
      }
      d = document.getElementsByName("tool_supplier")[0];
      d.removeAttribute("readonly");
      d.style.border = "solid 1px rgb(169,169,169)";
      $(d).autocomplete({
         source: function(request, response) {
            results = $.ui.autocomplete.filter(m_t7__tool.sslsupplier, request.term);
            response(results.slice(0, 5));
         },        
         select: function(event, ui) {
            do_fade(true);
            post.save_field(session.remote_domino_path_epcmain, m_t7__tool.unid.tool, "supplier", ui.item.value.trim());
            location.reload();
         },  
      });

   },

   g3mail: {
      send: function() { 
         data = [];
         for(k in m_t7__tool.g3mail) {
            if(k.substr(0, 4) == "mail") data.push(k + "=" + unescape(m_t7__tool.g3mail[k]));
         }
         post.save_data("f.mail", "", data.join("&"), "location.reload()");
      },

   },

}

<?php
$file = file_get_authentificated_contents($_SESSION["remote_domino_path_ssl"]."/v.json.tdt.supplier?open&count=99999&function=plain");
$supplier = explode("/", $file);
foreach($supplier as $val) {
   if(trim($val) != "") print "m_t7__tool.supplier.push(".trim($val).");\r\n";
}
?>


function set_checkbox(e, unid, field) {
   p = session.php_server + "/library/images/16x16/";
   if(e.firstChild.src.indexOf("status-11-1") > -1) {
      e.firstChild.src = p + "status-11.png";
      val = "1";
   }
   else {
      e.firstChild.src = p + "status-11-1.png";
      val = "0";
   }
   js = "";
   if(field == "a2p") js = "do_ctsn(" + val + ")";
   handle_save_single_field_extdb(session.remote_domino_path_epcmain, unid, field, val, js);
}

function do_ctsn() {
   if(arguments[0] == 1) {
      e = document.getElementById("ctsn");
      e.firstChild.src = session.php_server + "/library/images/16x16/status-11-1.png";
      unid = e.getAttribute("unid");
      field = "ctsn";
      set_checkbox(e, unid, field);
   }
}



include_m_t7__tool = function() {

   <?php
   require_once($path."/modules/m_t7.tool.php");
   require_once($path."/addin/basic_php_functions.php");
   $load = array("tracker", "pot", "tool", "meta", "project");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   require_once($path."/application.ini");
   $html = rawurlencode(m_t7__tool_generate_html($_application));
   print "html = \"".$html."\";\r\n";
   print "   m_t7__tool.unid = {tool: \"".$tc_data["tool"]["unid"]."\", pot: \"".$tc_data["pot"]["unid"]."\"};\r\n";
   print "   m_t7__tool.g3mail.mail_do = escape(\"1\");\r\n";
   print "   m_t7__tool.g3mail.mail_delete = escape(\"1\");\r\n";
   print "   m_t7__tool.g3mail.mail_template = escape(\"t1\");\r\n";
   print "   m_t7__tool.g3mail.mail_sendfrom = \"".rawurlencode($_SESSION["domino_user_cn"])."\";\r\n";
   print "   m_t7__tool.g3mail.mail_c1 = \"".rawurlencode($tc_data["tool"]["number"])."\";\r\n";
   print "   m_t7__tool.g3mail.mail_c2 = \"".rawurlencode(str_replace("Array", "", $tc_data["tool"]["type"]))."\";\r\n";
   print "   m_t7__tool.g3mail.mail_c3 = \"".rawurlencode(str_replace("Array", "", $tc_data["tool"]["oldtool"]))."\";\r\n";
   print "   m_t7__tool.g3mail.mail_c4 = \"".rawurlencode($tc_data["part"]["number"])."\";\r\n";
   print "   m_t7__tool.g3mail.mail_c5 = \"".rawurlencode($tc_data["part"]["name"])."\";\r\n";
   print "   m_t7__tool.g3mail.mail_c6 = \"".rawurlencode($tc_data["tool"]["supplier"])."\";\r\n";
   print "   m_t7__tool.g3mail.mail_c7 = escape(location.href);\r\n";
   print "   m_t7__tool.g3mail.mail_c8 = \"".$tc_data["project"]["number"]."\";\r\n";
   ?>
   document.getElementById("m_t7__tool_innerhtml").innerHTML = unescape(html);
   uroles.nest("m_t7__tool", document.getElementById("tbl_m_t7__tool").outerHTML, "check");
   

   m_t7__tool.nest_input();
   toolspec = document.getElementsByName("toolspec")[0].getAttribute('select').split(";");
   $("input[name='toolspec']").autocomplete({
      source: toolspec
   });

   err = "2";
   base_number = document.getElementById("tbl_m_t7__tool").querySelector("[field='base_number']");
   if(base_number.textContent.trim().substr(0, 1) != err) {
      d = document.getElementById("tbl_m_t7__tool").querySelector("[field='dualsource']").parentNode.nextSibling;
      d.innerHTML = "<a href=\"javascript:infobox('ERROR: DUAL SOURCE NOT ALLOWED','Tool is not a mold.',280,80);\">" + d.getElementsByTagName("img")[0].outerHTML + "</a>";
      with(document.getElementsByName("resin")[0]) {
         setAttribute("readonly", "readonly");
         style.border = "solid 1px rgba(0,0,0,0.0)";
      }
   }

}