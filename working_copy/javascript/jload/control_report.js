<?php
session_start();
ob_start("ob_gzhandler");											 
require_once("../../../../../library/tools/addin_xml.php");
$css = file_get_contents("../../../../../library/css5/te.tools/control_report.css");


print "d = document.getElementById(\"css:cr\");\r\n";
print "if(!d) {\r\n";
print "   style = document.createElement(\"style\");\r\n";
print "   style.setAttribute(\"type\", \"text/css\");\r\n";
print "   style.setAttribute(\"id\", \"css:cr\");\r\n";
print "   style.textContent = '".str_replace(array("\r", "\n", "
   "), array("", "", ""), $css)."';\r\n";
print "   document.getElementsByTagName(\"head\")[0].appendChild(style);\r\n";
print "}\r\n";

$process_approver = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.get_process_approver?open");
print $process_approver;

$nab = file_get_authentificated_contents($_SESSION["remote_domino_path"]."/names.nsf/v.get.person?open&count=99999");
print utf8_encode($nab);
?>
include_css(session.php_server + "/library/css5/te.tools/jquery-ui-ssl.css");

rms = {

   submit: function() {
      rel_p27 = document.getElementsByName("rel_p27"); if(rel_p27) rel_p27[0].value = document.getElementsByName("rel_27p")[0].value;
      form = document.createElement("form");
      form.setAttribute("method", "post");
      form.setAttribute("action", session.remote_domino_path_epcmain + "/rms?create&type=tdt&r=" + escape(location.href));
      //new
      form.style.display = "none";
      data = document.getElementById("confirm_box").getElementsByClassName("body")[0].getElementsByTagName("input");
      for(k in data) if(!isNaN(k)) {
         input = document.createElement("input");
         input.setAttribute("name", data[k].name);
         input.setAttribute("value", data[k].value);
         form.appendChild(input);
      }
      document.getElementById("confirm_box").getElementsByClassName("body")[0].style.visibility = "hidden";
      //new
      document.getElementsByTagName("body")[0].appendChild(form);
      form.submit();
   },

   active: {
      "01": ["rel_27p", "rel_dimensional", "rel_laboratory", "rel_process", "rel_tool", "rel_supplierppap"],
      "02": ["rel_dimensional", "rel_laboratory"],
      "03": ["rel_27p", "rel_dimensional", "rel_laboratory", "rel_process", "rel_tool", "rel_supplierppap"],
      "08": ["rel_27p", "rel_dimensional", "rel_laboratory", "rel_process", "rel_tool", "rel_supplierppap"],
      "09": ["rel_dimensional", "rel_laboratory"],
      "10": ["rel_27p", "rel_dimensional", "rel_laboratory", "rel_process", "rel_tool", "rel_supplierppap"],
      "11": ["rel_27p", "rel_dimensional", "rel_laboratory", "rel_supplierppap"],
      "15": ["rel_27p", "rel_dimensional", "rel_laboratory", "rel_supplierppap"],
   },

   mail: {
     // rel_design: [<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rms_mail?open&count=9999&restricttocategory=rel_design&function=plain"); if(!strpos(strtolower($file), "no documents found")) print substr(trim($file), 0, strrpos($file, ",")); ?>],
     rel_27p: [<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rms_mail?open&count=9999&restricttocategory=rel_27p&function=plain"); if(!strpos(strtolower($file), "no documents found")) print substr(trim($file), 0, strrpos($file, ",")); ?>],
     rel_dimensional: [<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rms_mail?open&count=9999&restricttocategory=rel_dimensional&function=plain"); if(!strpos(strtolower($file), "no documents found")) print substr(trim($file), 0, strrpos($file, ",")); ?>],
     rel_laboratory: [<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rms_mail?open&count=9999&restricttocategory=rel_laboratory&function=plain"); if(!strpos(strtolower($file), "no documents found")) print substr(trim($file), 0, strrpos($file, ",")); ?>],
     rel_process: [<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rms_mail?open&count=9999&restricttocategory=rel_process&function=plain"); if(!strpos(strtolower($file), "no documents found")) print substr(trim($file), 0, strrpos($file, ",")); ?>],
     rel_tool: [<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rms_mail?open&count=9999&restricttocategory=rel_tool&function=plain"); if(!strpos(strtolower($file), "no documents found")) print substr(trim($file), 0, strrpos($file, ",")); ?>],
     rel_supplierppap: [<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.rms_mail?open&count=9999&restricttocategory=rel_ppap&function=plain"); if(!strpos(strtolower($file), "no documents found")) print substr(trim($file), 0, strrpos($file, ",")); ?>],

   },

   nest: function(key) {
    

     v = {
         number: {
            dsp: "Control report",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.number(),
               readonly: "readonly",
            },
         },
         control_report_status: {
            dsp: "Control report status",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: "To process",
               readonly: "readonly",
            },
         },
         loop: {
            dsp: "Loop",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.loop(key),
               readonly: "readonly",
            },
         },
         plant: {
            dsp: "Plant",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.maintplant,
               readonly: "readonly",
            },
         },
         project: {
            dsp: "Project",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: tdproject.number,
               readonly: "readonly",
            },
         },
         part_number: {
            dsp: "Part number",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: String(control_report.data.partnumber),
               readonly: "readonly",
            },
         },
         part_revision: {
            dsp: "Part revision",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.partrevision,
               readonly: "readonly",
            },
         },
         drawing_revision: {
            dsp: "Drawing revision",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.num_drwrev(),
               readonly: "readonly",
            },
         },

         num_drwnumber: {
            dsp: "Drawing number",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.num_drwnumber(),
               readonly: "readonly",
            },
         },
         tool_number: {
            dsp: "Tool number",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.toolnumber,
               readonly: "readonly",
            },
         },
         source: {
            dsp: "Source",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.source(key),
               readonly: "readonly",
            },
         },
         resin_number: {
            dsp: "Resin",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.resin,
               readonly: "readonly",
            },
         },
         created_date: {
            dsp: "Creation date",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.dateissued,
               readonly: "readonly",
            },
         },
         created_by: {
            dsp: "Creator",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: session.domino_user_cn,
               readonly: "readonly",
            },
         },

         crr: {
            dsp: "Assessment case",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: use_cases[key][0].replace(/LOOP/g, "").trim(),
               readonly: "readonly",
            },
         },
         assess_case: {
            dsp: "Assessment case ID",
            hidden: true,
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: key,
            },
         },

         supplier_tool: {
            dsp: "Tool supplier",
            attributes: {
               style: "width:409px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: unescape(control_report.data.supplier_tool),
               readonly: "readonly",
            },
         },

         supplier_tool_id: {
            dsp: "Tool supplier number",
            attributes: {
               style: "width:409px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: unescape(control_report.data.supplier_tool_id),
               readonly: "readonly",
            },
         },

         rel_27p: { 
            dsp: "Pre process",
            attributes: {
               style: "width:409px",
               type: "text",
               onkeyup: "rms.validate(this)",
            },
         },
         rel_dimensional: {
            dsp: "Dimensional release",
            attributes: {
               style: "width:409px",
               type: "text",
               onkeyup: "rms.validate(this)",
            },
         },
         rel_laboratory: {
            dsp: "Laboratory release",
            attributes: {
               style: "width:409px",
               type: "text",
               onkeyup: "rms.validate(this)",
            },
         },
         rel_process: {
            dsp: "Process release",
            attributes: {
               style: "width:409px",
               type: "text",
               onkeyup: "rms.validate(this)",
            },
         },
         rel_tool: {
            dsp: "Tool release",
            attributes: {
               style: "width:409px",
               type: "text",
               onkeyup: "rms.validate(this)",
            },
         },
         rel_supplierppap: {
            dsp: "Supplier PPAP release",
            attributes: {
               style: "width:409px",
               type: "text",
               onkeyup: "rms.validate(this)",
            },
         },
         date_dim: {
            dsp: "Dimensional report available",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.dateira.split(",")[0],
               readonly: "readonly",
            },
         },
         date_samples: {
            dsp: "Samples available",
            attributes: {
               style: "width:889px; color:rgb(0,0,0); border:solid 1px rgb(230,230,233);",
               type: "text",
               value: control_report.data.datesamples.split(",")[0],
               readonly: "readonly",
            },
         },
      }


      if(control_report.data.toolnumber.trim().substr(0, 1) == "2") {
         v.source.hidden = true;
         v.resin_number.hidden = true;
      }
      // ADD HIDDEN FIELDS
      control_report.hidden_fields.push("rel_p27");
      control_report.data["rel_p27"] = "";
      for(k in control_report.hidden_fields) {
         if(!in_array(control_report.hidden_fields[k], ["source", "supplier_id"])) {
            val = (typeof control_report.data[control_report.hidden_fields[k]] == "function") ? control_report.data[control_report.hidden_fields[k]]() : control_report.data[control_report.hidden_fields[k]];
            v[control_report.hidden_fields[k]] = {dsp: control_report.hidden_fields[k], hidden: true, attributes: {type: "text", value: unescape(val)},};
         }
      }

      table = document.createElement("table");
      table.setAttribute("border", "0");
      table.setAttribute("form", "control_report");
      for(k in v) {
        tr = document.createElement("tr");
        if(typeof v[k].hidden != "undefined") {
           if(v[k].hidden) tr.setAttribute("style", "display:none");
        }
        td = document.createElement("td");
        td.setAttribute("anchor", "td:rms");
        td.textContent = v[k].dsp;
        tr.appendChild(td);
        td = document.createElement("td");
        td.setAttribute("anchor", "td:rms");
        span = document.createElement("span");
        td.appendChild(span);        
        input = document.createElement("input");
        input.setAttribute("name", k);
        for(a in v[k].attributes) input.setAttribute(a, v[k].attributes[a]);
        span.appendChild(input);
        tr.appendChild(td);
        table.appendChild(tr);
      }
      document.getElementById("confirm_box").getElementsByClassName("body")[0].innerHTML = table.outerHTML + 
      "<span class=\"phpbutton\" style=\"margin-top:14px; margin-left:2px; display:none;\" anchor=\"rms_submit\"><a href=\"javascript:rms.submit();\">Submit RMS</a></span>";
      rms.include(key);
   },

   include: function(key) {

      if(wf_container) {
         v2_fot = wf_container.getAttribute("v2_fot");
         if(wf_container.hasAttribute("v2_fot")) {
            d = document.getElementsByName("date_samples")[0];
            d.value = wf_container.getAttribute("v2_fot").replace('-','/').replace('-','/');
         }
         if(wf_container.hasAttribute("v2_dimreport")) {
            d = document.getElementsByName("date_dim")[0];
            d.value = wf_container.getAttribute("v2_dimreport").replace('-','/').replace('-','/');
         }
      }
      rms.all = []; for(k in rms.mail) rms.all.push(k);
      if(control_report.data.supplier_tool_id.substr(0, 2).toLowerCase() == "te" || control_report.data.supplier_tool_id.trim() == "") {
         rms.active[key] = (rms.active[key].join(",").replace(/rel_supplierppap/g, "")).split(",");
      }    
      for(k in rms.all) {
         d = document.getElementsByName(rms.all[k])[0];
         if(!in_array(rms.all[k], rms.active[key])) {
            d.setAttribute("readonly", "readonly");
            d.style.border = "solid 1px rgb(230,230,233)";
         }
         else {
           d.setAttribute("is_mandatory", "1");
         }
      }
      $("input[name='rel_27p']").autocomplete({
         minLength: 0,
         source: rms.mail.rel_27p,
         focus: function() {return false;},
         select: function(event, ui) { this.value = ui.item.value; rms.validate(this); return false; },
      });
      $("input[name='rel_dimensional']").autocomplete({
         minLength: 0,
         source: rms.mail.rel_dimensional,
         focus: function() {return false;},
         select: function(event, ui) { this.value = ui.item.value; rms.validate(this); return false; },
      });
      $("input[name='rel_laboratory']").autocomplete({
         minLength: 0,
         source: rms.mail.rel_laboratory,
         focus: function() {return false;},
         select: function(event, ui) { this.value = ui.item.value; rms.validate(this); return false; },
      });
      $("input[name='rel_process']").autocomplete({
         minLength: 0,
         source: rms.mail.rel_process,
         focus: function() {return false;},
         select: function(event, ui) { this.value = ui.item.value; rms.validate(this); return false; },
      });
      $("input[name='rel_tool']").autocomplete({
         minLength: 0,
         source: rms.mail.rel_tool,
         focus: function() {return false;},
         select: function(event, ui) { this.value = ui.item.value; rms.validate(this); return false; },
      })
      $("input[name='rel_supplierppap']").autocomplete({
         minLength: 0,
         source: rms.mail.rel_supplierppap,
         focus: function() {return false;},
         select: function(event, ui) { this.value = ui.item.value; rms.validate(this); return false; },
      })

      d1 = document.querySelector("[form='m_d1__cr_v1']");

      deflt = { 
         rel_27p: d1.getAttribute("tpm"),
         rel_dimensional: d1.getAttribute("pemgr"),
         rel_laboratory: d1.getAttribute("pemgr"),
         rel_process: d1.getAttribute("tpm"),
         rel_tool: d1.getAttribute("tpm"),
         rel_supplierppap: d1.getAttribute("sq"),
      }

      for(k in deflt) {
         e = document.getElementsByName(k);
         if(e) {
            if(typeof e[0].getAttribute("readonly") != "string") {
               e[0].value = deflt[k].trim();
               if(deflt[k].trim() != "") {
                  span = document.createElement("span");
                  span.style = "margin-left:8px; display:none;";
                  span.innerHTML = "<img src=\"../../../../library/images/error.png\" style=\"position:absolute;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;User&nbsp;not&nbsp;listed";
                  e[0].parentNode.appendChild(span);
                  rms.validate(e[0]);
               }
            }
         }
      } 
      if(control_report.data.supplier_tool.substr(0, 2) != "TE" && !in_array(key, ["02", "03", "09", "10", "11"])) {
         document.getElementsByName("supplier_tool_id")[0].setAttribute("is_mandatory", "1");
         span = document.createElement("span");
         span.style = "margin-left:8px;";
         span.innerHTML = "<div style=\"position:relative;top:-16px;\"><img style=\"position:absolute;\" src=\"../../../../library/images/error.png\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No supplier id found. Can not create RMS.</div>" + 
         "<span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"infobox_handle.close(true, 'confirm_box');\">Close RMS</a></span>";
         document.getElementsByName("supplier_tool_id")[0].parentNode.appendChild(span);
         rms.validate(document.getElementsByName("supplier_tool_id")[0]);
         td = document.querySelectorAll("[anchor='td:rms']");

         if(document.getElementsByName("supplier_tool_id")[0].value == "") {
            for(k in td) if(!isNaN(k)) {
               if(td[k] != document.getElementsByName("supplier_tool_id")[0].parentNode.parentNode) {
                  td[k].style.display = "none";
               }
               else {
                  td[k].getElementsByTagName("input")[0].style.display = "none";
               }
           }
           span.style.display = "inline-block";
        }
        else span.style.display = "none";
      }
      s = { name: unescape(control_report.data.supplier_tool), id: unescape(control_report.data.supplier_tool_id) };
      use_supplier = (in_array(key, ["02", "03", "09", "10", "11", "15"])) ? ssl.supplier : ssl.internal_supplier;
      for(k in use_supplier) {
         if(use_supplier[k].vendor == s.name && (use_supplier[k].id == s.id || use_supplier[k].id == "internal")) {
            span = document.createElement("span");
            span.style = "margin-left:8px;";
            span.innerHTML = (use_supplier[k].mail.trim() != "") ? use_supplier[k].mail : "<div style=\"position:relative;top:-29px;\"><img style=\"position:absolute;\" src=\"../../../../library/images/error.png\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No supplier email for " + s.name + " found. Can not create RMS.</div><span class=\"phpbutton\"><a href=\"javascript:void(0);\" onclick=\"infobox_handle.close(true, 'confirm_box');\">Close RMS</a></span>";
            document.getElementsByName("supplier_tool")[0].parentNode.appendChild(span);
            if(use_supplier[k].mail.trim() == "") { 
               td = document.querySelectorAll("[anchor='td:rms']");
               for(k in td) {
                  if(td[k] != document.getElementsByName("supplier_tool")[0].parentNode.parentNode) {
                     td[k].style.display = "none";
                  }
                  else {
                     td[k].getElementsByTagName("input")[0].style.display = "none";
                  }
               }
            }
            else document.getElementsByName("supplier_mail")[0].value = use_supplier[k].mail.trim();
         }
      }
   },

   validate: function() {
      input = document.querySelector("[form='control_report']").getElementsByTagName("input");
      validated = true;
      for(i in input) if(!isNaN(i)) {
         if(input[i].hasAttribute("is_mandatory")) {
            if(input[i].getAttribute("is_mandatory") == "1") {
               v = input[i].value.trim();
               msg = input[i].parentNode.getElementsByTagName("span");
               bg = "rgb(255,255,255)";
               if(typeof rms.mail[input[i].name] == "object") {
                  if(v == "") { bg = "rgb(255,255,255)"; validated = false; }
                  else {
                     if(!in_array(v, rms.mail[input[i].name])) { 
                        bg = "rgb(230,171,170)"; 
                        validated = false;
                        if(msg[0]) msg[0].style.display = "inline-block";
                     }
                     else {
                        bg = "rgb(171,230,170)";
                        if(msg[0]) msg[0].style.display = "none";
                     }
                  }
               }
               if(input[i].name == "supplier_tool_id") {
                  if(input[i].value.trim() == "") { 
                     bg = "rgb(230,171,170)"; 
                     validated = false; 
                     if(msg[0]) msg[0].style.display = "inline-block";
                  }
               }
               input[i].style.background = bg;
            }   
         }
      }
      window.setTimeout('document.querySelector("[anchor=\'rms_submit\']").style.display = (!validated) ? "none" : "block"', 100);
   },

}



control_report = {

   td_purch: "controlreports@tycoelectronics.com",

   process_type: "<?php print $_REQUEST["process_type"]; ?>",

   hidden_fields: [
      "part_reftmp",
      "tool_reftmp",
      "author",
      "author_id",
      "authors",
      "check_process_release",
      "copied_fromno",
      "cr$copied_by",
      "cr$copied_date",
      "cr$copied_from",
      "cr$type",
      "cr_copied_from",
      "cr_parent",
      "cr_type",
      "dateaction",
      "dateidrw",
      "dateir",
      "dateissued",
      "db2mark",
      "drwrevision",
      "form",
      "is_draft",
      "is_obsolete",
      "key_cr",
      "numberdr",
      "originator",
      "parentid",
      "parentname",
      "partrevision",
      "proc_usr_id",
      "proc_usr_short",
      "process_type",
      "purch",
      "reason_code",
      "ref$assessment_date",
      "ref$internal_release_date",
      "ref$link",
      "ref$pot",
      "ref$part",
      "ref$tool",
      "ref$tracker",
      "reviewer_id",
      "select_supplier",
      "selectedtools",
      "source",
      "status",
      "statusir",
      "statusirp",
      "statusirs",
      "statusprocessrel",
      "subject",
      "supplier_db",
      "supplier_id",
      "supplier_mail",
      "supplier_new",
      "supplier_old",
      "td$pot$unique",
      "toolnumber",
      "type",
      "valid_from",
      "valid_to"
   ],

   nest: function(key) {

      if(document.querySelector("[form='m_d1__cr_v1']").getAttribute("is_rms") == "1") { rms.nest(key); return true; }

      v = [
         "CR number", 
         "Material source",
         "Process status",
         "CR issued",
         "Part Rev.",
         "Drawing Rev.", 
         "Part specification",
         "Resin No.",
         "Tool number",
         "Samples available", 
         "Dimensional report available",
         "Valid from", "Valid till",
         "IR date (product)",
         "IR assessment (spec./ tooling)",
         "IR assessment (production)",
         "IR assessment (stock)",
         "Assessment Reason",
         "CR Type",
         "CR Reason",
         "Supplier",
         "Responsible person(s)",
         "Reviewer(s) to be notified",
         "Process Approver (suggested)",
         "Process released by",
         "Remark"
      ];



      table = document.createElement("table");
      table.setAttribute("border", "0");
      table.setAttribute("form", "control_report");

      for(k in v) {
        tr = document.createElement("tr");
        table.appendChild(tr);
        td = document.createElement("td");
        td.textContent = v[k];
        tr.appendChild(td);

        switch(k) {
           case "0":
              td = document.createElement("td");
              input.setAttribute("name", "number");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "1":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "source");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;
              
           case "2":
              td = document.createElement("td");
              td.innerHTML = "<span style=\"padding-left:4px;\">To process</span>";
              input = document.createElement("input");
              input.setAttribute("name", "Status");
              input.setAttribute("type", "hidden");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "3":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "DateIssued");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;
              
           case "4":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "PartRevision");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "5":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "DrwRevision");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "6":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "ParentName");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;
              
           case "7":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "RawMaterial");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;
              
           case "8":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "toolnumber");
              input.setAttribute("readonly", "readonly");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "9":
              td = document.createElement("td");
              if(key == "0" || key=="3") td.innerHTML= "<img src=\"../../../../library/images/16x16/time-3.png\" style=\"position:relative;top:3px; left:2px; margin-right:3px; cursor:pointer;\" onclick=\"control_report.calendar('datesamples');\">";
              input = document.createElement("input");
              input.setAttribute("name", "DateSamples");
              input.setAttribute("value", "");
              if(key == "0" || key == "3") input.setAttribute("is_mandatory", "true");
              input.setAttribute("readonly", "readonly");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "10":
              td = document.createElement("td");
              if(key == "0" || key=="3") td.innerHTML= "<img src=\"../../../../library/images/16x16/time-3.png\" style=\"position:relative;top:3px; left:2px; margin-right:3px; cursor:pointer;\" onclick=\"control_report.calendar('dateira');\">";
              input = document.createElement("input");
              input.setAttribute("name", "DateIRA");
              input.setAttribute("value", "");
              if(key == "0" || key == "3") input.setAttribute("is_mandatory", "true");
              input.setAttribute("readonly", "readonly");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "11":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "Valid_From");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

           case "12":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "Valid_To");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "13":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "DateIR");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "14":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "StatusIR");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "15":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "StatusIRP");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "16":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "StatusIRS");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "17":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "reason_code");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "18":
              td = document.createElement("td");
              txt = (control_report.data["cr_type"] == "2") ? "Process and Product CR" : "Product CR";
              td.innerHTML = "<span style=\"padding-left:4px;\">" + txt + "</span>";
              input = document.createElement("input");
              input.setAttribute("name", "cr_type");
              input.setAttribute("type", "hidden");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "19":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "CRReason");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              input.setAttribute("style", "width:198px;");
              td.appendChild(input);
              input = document.createElement("input");
              input.setAttribute("name", "loop");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("value", "");
              input.setAttribute("style", "width:250px;");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "20":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "Supplier");
              input.setAttribute("value", "");
              input.setAttribute("readonly", "readonly");
              input.setAttribute("style", "width:500px;");
              td.appendChild(input);
              span = document.createElement("span");
              span.setAttribute("id", "cr_supplier_mail");
              span.setAttribute("style", "display:inline-block;width:320px;text-align:right;");
              td.appendChild(span);
              tr.appendChild(td);
              break;

          case "21":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "Author");
              input.setAttribute("value", "");
              input.setAttribute("is_mandatory", "true");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "22":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "REVIEWER");
              input.setAttribute("value", "");
              if(key == "0" || key == "2" || key == "3" || key == "5" ) input.setAttribute("is_mandatory", "true");
              input.setAttribute("style", "width:490px;");
              td.appendChild(input);
              a = document.createElement("a");
              a.setAttribute("href", "javascript:control_report.add_td_purch(true); control_report.validate(document.getElementsByName('author')[0]);");
              a.setAttribute("class", "href");
              a.textContent = "Add TD purchasing";
              td.appendChild(a);

              a = document.createElement("a");
              a.setAttribute("href", "javascript:control_report.add_td_purch(false); control_report.validate(document.getElementsByName('author')[0]);");
              a.setAttribute("class", "href");
              a.textContent = "Delete TD purchasing";
              td.appendChild(a);
              tr.appendChild(td);
              break;

          case "23":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "PROC_USR_NAME");
              input.setAttribute("value", "");
              if(key == "1" || key == "2" || key == "4" || key == "5") input.setAttribute("is_mandatory", "true");
              if(key =="0" || key=="3") input.setAttribute("style", "display:none;");
              td.appendChild(input);
              tr.appendChild(td);
              break;
              
          case "24":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "PROC_USR_ID");
              input.setAttribute("readonly", "readonly");
              if(key =="0" || key=="3") input.setAttribute("style", "display:none;");
              td.appendChild(input);
              tr.appendChild(td);
              break;

          case "25":
              td = document.createElement("td");
              td.textContent = "";
              input = document.createElement("input");
              input.setAttribute("name", "Remark");
              input.setAttribute("value", "");
              td.appendChild(input);
              tr.appendChild(td);
              break;
         }
      }

      t = table.getElementsByTagName("td");
      for(i = 1; i < t.length; i = i + 2) {
         inp = t[i].getElementsByTagName("input")[0];
         if(inp.hasAttribute("is_mandatory")) {
            t[i - 1].innerHTML = "*" + t[i - 1].innerHTML;
         }
      }   

      div = document.createElement("div");
      div.setAttribute("style", "display:none;");


      arr = control_report.hidden_fields;
      for(k in arr) {
         input = document.createElement("input");
         input.setAttribute("name", arr[k]);  
         div.appendChild(input);
      }


      
      input = document.createElement("input");
      input.setAttribute("name", "is_tdt");
      input.setAttribute("value", "1");
      div.appendChild(input);

      with(document.getElementById("confirm_box").getElementsByClassName("body")[0]) {
          innerHTML = table.outerHTML;
          appendChild(div);
          innerHTML += innerHTML = "<br><span id=\"control_report:save\" class=\"phpbutton\" style=\"display:none;\"><a href=\"javascript:control_report.submit();\">Submit control report</a></span>";
      }

      control_report.include("");
   },

   data: {
      ref$tracker: function() {
         return escape(document.querySelector("[form='m_d1__cr_v1']").getAttribute("tracker"));
      },
      ref$pot: function() {
         return escape(document.querySelector("[form='m_d1__cr_v1']").getAttribute("pot_unid"));
      },
      number: function() {
         return "<?php $file = file_get_authentificated_contents($_SESSION["remote_domino_path_epcmain"]."/a.get_cr_number?open"); print trim($file); ?>";
      },
      loop: function() {
         if(typeof arguments[0] == "undefined") {
            ret = (in_array(m_d1.key, [0, 1])) ? m_d1.loop[0] : m_d1.loop[1];
            if(in_array(m_d1.key, [2, 5])) ret = "";
         }
         else {
            ret = (in_array(arguments[0], [15])) ? m_d1.loop[1] : m_d1.loop[0];
            if(String(ret).trim() == "") ret = "0";
         } 
         return ret;
      },
      process_type: function() {
         return control_report.process_type;
      },
      source: function(key) {
         return (key == "15") ? "2" : "1";
      },
      num_drwnumber: function() {
         r = (typeof control_report.data.r[1] == "string") ? control_report.data.r[1] :  control_report.data.drwnumber;
         return r;
      },
      num_drwrev: function() {
         r = (typeof control_report.data.r[0] == "string") ? control_report.data.r[0] : control_report.data.drwrevision;
         if(r.length == 0 && control_report.data.drwrevision != "") r = control_report.data.drwrevision;
         return r;
      },
      r: "<?php session_start(); $file = file_get_contents($_SESSION["php_server"]."/apps/epc/main/cr/get_part.php?t=domino&q=".$_SESSION["request:part"]."&revision=".strtolower($_SESSION["request:rev"])); print $file; ?>".split("$$"),
   },

   calendar: function(fld) {
      control_report.embed_calendar = true;
      if(typeof arguments[1] == "undefined") {
         calendar_big_pick_date("control_report", fld, escape("Set " + fld), "embed_rc");
      }
      else {
         control_report.embed_calendar = false;
         d = document.getElementById("confirm_box_cal");
         d.parentNode.removeChild(d);
         document.getElementsByName(fld)[0].value = arguments[1].trim();
         control_report.validate(document.getElementsByName(fld)[0]);
      }
   },

   include: function(arr) {
      if(control_report.data["cr_parent"].trim() != "") {
         d = document.getElementById("confirm_box").getElementsByClassName("header")[0].getElementsByTagName("td")[0];
         d.innerHTML += " [this control report is copied from " + control_report.data["cr_parent"].trim() + "]";
      }
      d = document.getElementById("confirm_box").getElementsByClassName("body")[0].getElementsByTagName("input");
      for(k in d) if(!isNaN(k)) {
         d[k].name = d[k].name.toLowerCase();
         if(control_report.data[d[k].name]) {
            d[k].value = (typeof control_report.data[d[k].name] == "string") ? unescape(control_report.data[d[k].name]) : unescape(control_report.data[d[k].name](arr));
         }
         d[k].setAttribute("onkeyup", "control_report.validate(this)");
      }
      $("input[name='author']")
      .autocomplete({
         minLength: 0,
         source: function(request, response) {
            response($.ui.autocomplete.filter(nab.person.join("$").replace(control_report.td_purch, "").split("$"), jqui.extractLast(request.term)));},
         focus: function() {return false;},
         select: function(event, ui) {
            terms = jqui.split(this.value);
            terms.pop();
            terms.push(ui.item.value);
            terms.push( "" );
            this.value = terms.join(", ");
            control_report.validate(this);
            return false;
         },
      })
      .bind("keydown", function(event) {
         if( event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
         }
      });
      $("input[name='reviewer']")
      .autocomplete({
         minLength: 0,
         source: function(request, response) {
            response($.ui.autocomplete.filter(nab.arr, jqui.extractLast(request.term)));},
         focus: function() {return false;},
         select: function(event, ui) {
            terms = jqui.split( this.value );
            terms.pop();
            terms.push(ui.item.value);
            terms.push( "" );
            this.value = terms.join(", ");
            control_report.validate(this);
            return false;
         },
      })
      .bind("keydown", function(event) {
         if( event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
         }
      });    
      $("input[name='proc_usr_name']")
      .autocomplete({
         minLength: 0,
         source: function(request, response) {
            response($.ui.autocomplete.filter(process_approver, jqui.extractLast(request.term)));},
         focus: function() {return false;},
         select: function(event, ui) {
            this.value = ui.item.value;
            control_report.validate(this);
            return false;
         },
      })
      .bind("keydown", function(event) {
         if( event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
         }
      }); 

      d = document.getElementsByName("author")[0];
      if(d.value.trim() != "") control_report.validate(d);

      wf_container = document.querySelector("[form='m_x3__workflow']");
      if(wf_container) {
         v2_fot = wf_container.getAttribute("v2_fot");
         if(wf_container.hasAttribute("v2_fot")) {
            d = document.getElementsByName("datesamples")[0];
            d.value = wf_container.getAttribute("v2_fot");
            control_report.validate(d);
         }
         if(wf_container.hasAttribute("v2_dimreport")) {
            d = document.getElementsByName("dateira")[0];
            d.value = wf_container.getAttribute("v2_dimreport");
            control_report.validate(d);
         }
      }

      if(m_d1.transfer) {
         document.getElementsByName("crreason")[0].value = "final release";
         document.getElementsByName("loop")[0].value = "";
         document.getElementsByName("datesamples")[0].value = "";
         document.getElementsByName("dateira")[0].value = "";
      }

      document.getElementById("cr_supplier_mail").innerHTML = "<a style=\"text-decoration:none;color:rgb(116,118,120);\" href=\"mailto:" + document.getElementsByName("supplier_mail")[0].value + "\">" + document.getElementsByName("supplier_mail")[0].value + "</a>";

      loop = document.getElementsByName("loop")[0].value.trim(); if(loop == "") loop = "0";

      ds = document.getElementsByName("datesamples")[0];
      if(ds.value.trim() == "") {
         d = document.querySelector("[anchor='g03030_loop" + loop + "']");
         if(d) if(d.getAttribute("value").trim() != "") ds.value = d.getAttribute("value").trim();
         control_report.validate(ds);
      }
      if(ds.value.trim() == "") {
         d = document.querySelector("[anchor='g04070_loop" + loop + "']");
         if(d) if(d.getAttribute("value").trim() != "") ds.value = d.getAttribute("value").trim();
         control_report.validate(ds);
      }

      ds = document.getElementsByName("dateira")[0];
      if(ds.value.trim() == "") {
         d = document.querySelector("[anchor='g03040_loop" + loop + "']");
         if(d) if(d.getAttribute("value").trim() != "") ds.value = d.getAttribute("value").trim();
         control_report.validate(ds);
      }
      if(ds.value.trim() == "") {
         d = document.querySelector("[anchor='g04080_loop" + loop + "']");
         if(d) if(d.getAttribute("value").trim() != "") ds.value = d.getAttribute("value").trim();
         control_report.validate(ds);
      }

   },

   submit: function() {
      form = document.createElement("form");
      form.setAttribute("method", "post");
      form.setAttribute("action", session.remote_domino_path_epcmain + "/jsoncreport?create&r=" + escape(location.href));
      //new
      form.style.display = "none";
      data = document.getElementById("confirm_box").getElementsByClassName("body")[0].getElementsByTagName("input");
      for(k in data) if(!isNaN(k)) {
         input = document.createElement("input");
         input.setAttribute("name", data[k].name);
         input.setAttribute("value", data[k].value);
         form.appendChild(input);
      }

      document.getElementById("confirm_box").getElementsByClassName("body")[0].style.visibility = "hidden";
      //new
      document.getElementsByTagName("body")[0].appendChild(form);
      form.submit();
   },

   validate: function(e) {
      if(e.getAttribute("is_mandatory") != "true") return true;
      check = true;
      switch(e.getAttribute("name")) {
         case "author":
            v = e.value.split(",");
            for(k in v) {
               if(v[k].trim() != "" && !in_array(v[k].trim(), nab.person)) check = false;
            }
            break;
         case "reviewer":
            v = e.value.split(",");
            for(k in v) {
               if(v[k].trim() != "" && !in_array(v[k].trim(), nab.person)) check = false;
            }
            break;
         case "proc_usr_id":
            v = e.value.split(",");
            for(k in v) {
               if(v[k].trim() != "" && !in_array(v[k].trim(), process_approver)) check = false;
            }
            break;
         case "datesamples":
            v = e.value;
            if(v.length != 10) check = false;
            break;
         case "dateira":
            v = e.value;
            if(v.length != 10) check = false;
            break;
      }
      bg = ["rgba(214,227,66, 0.3)", "rgba(205,32,44,0.5)"];
      e.style.background = (check) ? bg[0] : bg[1]; 
      e.setAttribute("validation", check);
      dsp = true;
      d = document.getElementById("confirm_box").getElementsByClassName("body")[0].querySelectorAll("[is_mandatory='true']");
      for(k in d) if(!isNaN(k)) {
         if(d[k].getAttribute("validation") != "true") dsp = false;
      }
      document.getElementById("control_report:save").style.display = (dsp) ? "block" : "none";
   },

   add_td_purch: function(t) {
      d = document.getElementsByName("reviewer")[0];
      if(t) {
         d.value = control_report.td_purch + ", " + d.value.replace(control_report.td_purch + ", ", "");
      }
      else {
         d.value = d.value.replace(control_report.td_purch + ", ", "");
      }
   }

}

nab.arr = nab.person;
nab.arr.push(control_report.td_purch);

jqui = {
   split: function(val) {return val.split( /,\s*/ );},
   extractLast: function(term) {return jqui.split(term).pop();}
}

<?php
$file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.link_by_unique/".$_REQUEST["pot"]."?open&is_pilot=".$_REQUEST["is_pilot"]."&is_rms=".$_REQUEST["is_rms"]."&unique=".$_REQUEST["tracker"]."&k=".$_REQUEST["k"]."&cr=".$_REQUEST["cr"]);
print $file;
?>






