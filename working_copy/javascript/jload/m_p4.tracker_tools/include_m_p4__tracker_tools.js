function include_m_p4__tracker_tools() {

   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_p4.tracker_tools.php");
   require_once($path."/addin/basic_php_functions.php");
   $load = array("project:ext", "case");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);

   $html = rawurlencode(m_p4__tracker_tools_generate_html($_application));
   print "html = \"".$html."\";\r\n";

   ?>

   document.getElementById("m_p4__tracker_tools_innerhtml").innerHTML = unescape(html);
   
}




