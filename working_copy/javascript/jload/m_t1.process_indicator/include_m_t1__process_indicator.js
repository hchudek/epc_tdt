function include_m_t1__process_indicator() {

   
   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_t1.process_indicator.php");
   require_once($path."/modules/elements/process/rules.php");
   require_once($path."/modules/elements/process/subsprocesses.php");
   require_once($path."/modules/elements/process/types.php");
   require_once($path."/addin/application_corrections.php");
   require_once($path."/addin/basic_php_functions.php");

   require_once($path."/application.ini");

   $load = array("tracker", "project", "pot", "tool", "meta");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);
   $html = rawurlencode(m_t1__process_indicator_generate_html($_application));
   print "html = \"".$html."\";\r\n";   
   ?>

   document.getElementById("m_t1__process_indicator_innerhtml").innerHTML = unescape(html); 
    m_t1__process_indicator.add_attribute();
   
}







