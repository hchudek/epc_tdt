function include_m_v10__select_tracker() {

   if(typeof arguments[0] == "undefined") arguments[0] = "tbl_m_v10__select_tracker";
   table = document.getElementById(arguments[0]);
   table.parentNode.removeChild(table.parentNode.getElementsByTagName("load")[0]);

   <?php
   session_start();
   $path = str_replace("\\", "/", substr($_SERVER["PATH_TRANSLATED"], 0 , strpos($_SERVER["PATH_TRANSLATED"], "wwwroot") + 7));
   require_once($path."/library/tools/addin_xml.php");
   $tpm = $_SESSION["domino_user_cn"];
   //$tpm = "_";
   $tracker = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.my_tracker?open&count=9999&restricttocategory=".rawurlencode($tpm)."&function=plain");
   $tracker = str_replace("<h2>No documents found</h2>", "", $tracker);

   print $tracker;

   ?>

   unique = location.search.substr(location.search.indexOf("unique=") + 7, 16);

   m_over = 
   "if(this.className != 'selected_tracker') {" +
   "   this.style.background='rgb(243,243,247)';" + 
   "   with(this.firstChild.childNodes[0].style) {borderLeft='solid 1px rgb(222,222,232)'; borderTop='solid 1px rgb(222,222,232)'; borderBottom='solid 1px rgb(222,222,232)'}" +
   "   with(this.firstChild.childNodes[1].style) {borderTop='solid 1px rgb(222,222,232)'; borderBottom='solid 1px rgb(222,222,232)'}" +
   "   with(this.firstChild.childNodes[2].style) {borderRight='solid 1px rgb(222,222,232)'; borderTop='solid 1px rgb(222,222,232)'; borderBottom='solid 1px rgb(222,222,232)'}" + 
   "}";

   m_out = 
   "if(this.className != 'selected_tracker') {" +
   "   this.style.background='rgb(255,255,255)';" + 
   "   with(this.firstChild.childNodes[0].style) {borderLeft='solid 1px rgb(255,255,255)'; borderTop='solid 1px rgb(255,255,255)'; borderBottom='solid 1px rgb(255,255,255)'}" +
   "   with(this.firstChild.childNodes[1].style) {borderTop='solid 1px rgb(255,255,255)'; borderBottom='solid 1px rgb(255,255,255)'}" +
   "   with(this.firstChild.childNodes[2].style) {borderRight='solid 1px rgb(255,255,255)'; borderTop='solid 1px rgb(255,255,255)'; borderBottom='solid 1px rgb(255,255,255)'}" + 
   "}";

   if(m_v10__my_tracker.tracker.length == 0) {
      v9 = document.getElementById("m_v9__tpm_pot").getElementsByClassName("inner_box_x")[0];
      v10 = document.getElementById("m_v10__select_tracker").getElementsByClassName("inner_box")[0];    
      //if(v9) v9.innerHTML = "<div>No tracker found for <?php print $_SESSION["domino_user_cn"]; ?></div>";
      if(v10) v10.innerHTML = "<div>No tracker found for <?php print $_SESSION["domino_user_cn"]; ?></div>";
   }
   else {
      for(k in m_v10__my_tracker.tracker) {
         content = [unescape(m_v10__my_tracker.tracker[k].number), unescape(m_v10__my_tracker.tracker[k].description)];
         tbody = document.createElement("tbody");
         tbody.setAttribute("style", "display:table-row-group;");
         tbody.setAttribute("key", content.join(" ").toLowerCase());
         tbody.setAttribute("onmouseover", m_over);
         tbody.setAttribute("onmouseout", m_out);

         if(m_v10__my_tracker.tracker[k].unique == unique) tbody.setAttribute("class", "selected_tracker");
         else tbody.setAttribute("class", "unselected_pot");
         tbody.setAttribute("onclick", "location.href='?&unique=" + m_v10__my_tracker.tracker[k].unique + "&d=" + base64_encode(m_v10__my_tracker.tracker[k].number) + "';");
         tr = document.createElement("tr");
         for(c in content) { 
            td = document.createElement("td");
            td.innerHTML = content[c];
            tr.appendChild(td);
         }
         tbody.appendChild(tr);
         table.appendChild(tbody);
      }
   }
}





