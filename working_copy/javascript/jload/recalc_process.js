<?php
ob_start("ob_gzhandler");
session_start();
$p = str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
require_once(substr($p, 0, strpos($p, "apps"))."library/tools/addin_xml.php");			
require_once(substr($p, 0, strpos($p, "tdtracking") + 11)."/modules/elements/process/rules.php");


if($_REQUEST["key"] != "") {
   $attr = explode("@",$_REQUEST["key"]);
   $xml = process_xml($_SESSION["remote_domino_path_main"]."/p.get_ref?open&key=".$attr[0]);

   $_REQUEST["process_rule"] = $xml["process_rule"];
   $_SESSION["request:pot"] = $attr[1]; 
   $_SESSION["request:unique"] = $attr[2];
   print "<script type=\"text/javascript\">\r\n";

   print "session = {\r\n";
   foreach($_SESSION as $key => $val) {
      if(!is_array($val) && $key != "__load") print "   ".str_replace(":", "_", $key).": \"".str_replace("\n", "", $val)."\",\r\n";
   }
   print "}\r\n";

   //require_once("../../addin/basic_php_functions.php");
   //$target = $_SESSION["remote_domino_path_main"]."/f.mail?create";
   //$P["mail_do"] = "1";
   //foreach($P as $key => $val) {
   //   $data[] = $key."=".rawurlencode($val); 
   //}
   //$do = http_post($target, implode("&", $data));
   
}
?>

recalc_process = {
 
   key: [],  
  
   result: [],
 
   start: function() {
// HIER DATUM SETZEN WENN NICHT DATUM = HEUTE
// arguments[2] = "2011-08-09";
      if(arguments[2].length > 10) {
         arr = arguments[2].split("/");
         arguments[2] = arr[0];
         recalc_process.reason_code = arr[arr.length - 1];
      }
      else {
         recalc_process.reason_code = "";
      }

      recalc_process.result.push([arguments[3], arguments[2], "date_actual", true]);
      //recalc_process.result.push([arguments[3], "true", "locked", false]);
      recalc_process.result.push([arguments[3], "40", "process_status", false]);
      flag = false;

      for(k in recalc_process.rules) {
         if(k == arguments[3]) flag = true;
         if(flag) recalc_process.key.push(k);
      }

      for(gate in recalc_process.key) {
         date = (recalc_process.key[gate] == arguments[3]) ? arguments[2] : recalc_process.pdate[recalc_process.key[gate]];  
         //alert(recalc_process.pdate["G01040"] + ";" + recalc_process.key[gate] +" / " + date + " / "+ gate + "/" +recalc_process.pdate[recalc_process.key[gate] + "_locked"] + "/"+recalc_process.pdate[recalc_process.key[gate]]);
         for(k in recalc_process.rules) {
            if(k.substr(0, 1) == "G") {
               if(recalc_process.rules[k].from_id == recalc_process.key[gate]) {
                  operation = Number(recalc_process.rules[k].operation.replace(/\+/g, "").replace(/\-/g, ""));
                  
                  if(operation > 0) {
                     calc_date = recalc_process.calc_date(date.split("-"), operation).join("-");
                     if(calc_date != date && date.trim() != "") {
                        if(recalc_process.pdate[k + "_locked"] != "1") recalc_process.pdate[k] = calc_date;
                        recalc_process.result.push([k, calc_date, "date_planned", true]);
                     }
                     if(recalc_process.key[gate] == arguments[3]) {
                        recalc_process.result.push([k, "20", "process_status", false]);
                     }
                  }
               }
            }
         }
      }
      recalc_process.save();
   },

   rules: <?php 
      unset($_application["process"]["rules"][$_REQUEST["process_rule"]]["bound_to"]);
      unset($_application["process"]["rules"][$_REQUEST["process_rule"]]["filter"]);
      unset($_application["process"]["rules"][$_REQUEST["process_rule"]]["displayed_name"]);
      foreach($_application["process"]["rules"][$_REQUEST["process_rule"]] as $key => $v) unset($_application["process"]["rules"][$_REQUEST["process_rule"]][$key][edittimeline]);
      print json_encode($_application["process"]["rules"][$_REQUEST["process_rule"]]);
   ?>,


   calc_date: function calc_date() {
      calc_t_date_parse = Date.parse(arguments[0].join("-"));
      for(i = 0; i < Number(arguments[1]); i++) {
         calc_t_date_parse += 86400000;
         calc_t_date = new Date(calc_t_date_parse);
         if(calc_t_date.getDay() == 0 || calc_t_date.getDay() == 6) i--;
      }  
      return [String(calc_t_date.getFullYear()), ("0" + String(calc_t_date.getMonth() + 1)).substr(-2), ("0" + String(calc_t_date.getDate())).substr(-2)];
   },


   pdate: <?php
      $category = $_SESSION["request:pot"]."@".$_SESSION["request:unique"];
      $pdate = process_xml($_SESSION["remote_domino_path_main"]."/v.dates_planned_by_pot_and_tracker?open&count=9999&restricttocategory=".$category."&function=xml:data");
      print json_encode($pdate); 
   ?>,

   save: function() {
      if(typeof do_fade == "function") do_fade(true);
      form = document.createElement("form");
      form.setAttribute("method", "post");
      form.setAttribute("action", session.remote_domino_path_main + "/f.save_process_indicator?create");
      //new
      form.style.display = "none";

      form.innerHTML =
      "<input name=\"ref_pot\" value=\"<?php print $_SESSION["request:pot"];?>\">" +
      <?php if($_REQUEST["key"] == "") print "\"<input name=\\\"r\\\" value=\\\"\" + location.href + \"\\\">\" +\r\n"; else print "\"<input name=\\\"e\\\" value=\\\"".rawurldecode($attr[4])."\\\">\" +\r\n"; ?>
      "<input name=\"ref_tracker\" value=\"<?php print $_SESSION["request:unique"];?>\">";
      cnt = [0, 0];
      if(recalc_process.reason_code != "") {
         form.innerHTML += "<input name=\"fld1\" value=\"" + recalc_process.pdate[recalc_process.result[0][0] + "_unid"] + ";reason_code_actual;" + recalc_process.reason_code + "\">";
         cnt[0] = 1;
      } 
      for(k in recalc_process.result) {
         if(!(recalc_process.pdate[recalc_process.result[k][0] + "_locked"] == "1" && recalc_process.result[k][2] == "date_planned")) {
            input = document.createElement("input");
            if(recalc_process.result[k][3]) {
               cnt[0]++;
               input.setAttribute("name", "fld" + String(cnt[0]));
            }
            else {
               cnt[1]++;
               input.setAttribute("name", "v" + String(cnt[1]));
            }
            input.setAttribute("value", recalc_process.pdate[recalc_process.result[k][0] + "_unid"] + ";" + recalc_process.result[k][2] + ";" + recalc_process.result[k][1]);
            form.appendChild(input);
         }
      }
      //new
      b = document.getElementsByTagName("body");
      if(b.length > 0) b[0].appendChild(form);
      else document.getElementsByTagName("script")[0].appendChild(form);
      form.submit();
   },

}


<?php
if($_REQUEST["key"] != "") {

   print "_phase = \"".$xml["phase"]."\";\r\n";
   print "_date = \"".$attr[3]."\";\r\n";
   if(!is_array($attr[5]) && $attr[5] != "") print "_date +=\"/".rawurldecode($attr[count($attr) - 1])."\";\r\n";
   print "recalc_process.start(\"\", \"\", _date, _phase);\r\n";
   print "</script>\r\n";
}
?>



   