function include_m_p3__tracker_parts() {

   <?php
   session_start();
   $file = str_replace("\\", "/", __FILE__);
   $path = substr($file, 0, strpos($file, "javascript"));
   require_once("../../../../library/tools/addin_xml.php");
   require_once($path."/modules/m_p3.tracker_parts.php");
   require_once($path."/addin/basic_php_functions.php");
   $load = array("project:ext", "case");
   require_once($path."/addin/load_tracker_data.php");
   $tc_data = create_tc_data($load);

   $html = rawurlencode(m_p3__tracker_parts_generate_html($_application));
   print "html = \"".$html."\";\r\n";
   ?>
 

   document.getElementById("m_p3__tracker_parts_innerhtml").innerHTML = unescape(html);
   
}




