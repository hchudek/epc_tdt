m_c2__create_tracker = {
   get_project: function() {
      url = "addin/run_ajax.php?" + escape(session.remote_domino_path_leanpd + "/v.get_projects_for_smartcombo/" + arguments[0].replace(/ /g, "_").toLowerCase());
      js = "m_c2__create_tracker.embed_project_info(document.getElementById('run_ajax').textContent.trim())";
      run_ajax("run_ajax", url, js);
   },

   embed_project_info: function() {
      eval(arguments[0]);
      for(k in project) {
         d = document.getElementById("m_c2__create_tracker_" + k);
         if(d) {
            d.textContent = unescape(project[k]);
         }
      }
      with(document.getElementById("m_c2__create_tracker_create_tracker")) {
         style.display = "block";
         getElementsByTagName("a")[0].href = "javascript:m_c2__create_tracker.create('" + unescape(project["number"]) + "');";
      }

      if(document.getElementById("m_v1__select_pot")) {
         m_v1__select_pot.load(unescape(project["number"]).trim());

      }
   },

   create: function() {
      f = [];
      if(document.getElementById("m_v1__select_pot_body")) {
         f["ref$pot"] = m_v1__select_pot.get_selected_pot().join(";");
      }
      f["PROJECT_NUMBER"] = arguments[0];
      f["project_descr"] = document.getElementById("m_c2__create_tracker_description").textContent;
      f["doc$status"] = "1";
      f["responsible"] = session.domino_user.substring(3,session.domino_user.indexOf("OU")-1);
      f["tracker_active"] = "1";
      form = document.createElement("form");
      form.setAttribute("method", "post");
      form.setAttribute("action", session.remote_domino_path_main + "/f.tracker?create&redirect=" + escape(session.remote_database_path));
      //new
      form.style.display = "none";
      for(k in f) {
         input = document.createElement("input");
         input.setAttribute("name", k);
         input.setAttribute("value", f[k]);
         form.appendChild(input);
      }
      //new
      document.getElementsByTagName("body")[0].appendChild(form);
      form.submit();
   },

   clear: function() {
      if(document.getElementById("m_c2__create_tracker_create_tracker").style.display != "none") {
         arr = ["_description", "_phase", "_manufacturing_city", "_date_plmc"];
         for(k in arr) document.getElementById("m_c2__create_tracker" + arr[k]).textContent = "";
         document.getElementById("m_c2__create_tracker_create_tracker").style.display = "none";
         document.getElementById("m_v1__select_pot_body").textContent = "";
      }
   },
}

