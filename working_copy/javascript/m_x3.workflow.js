m_x3 = {

   nest: function() {

      m_x3.source = Number(document.getElementsByName("m_x3__workflow_source")[0].value);
      m_x3.gate = (m_x3.source == 1) ? "g04100" : "g04120";

      d = document.querySelector("[form='m_x3__workflow']");

      if(d.getAttribute("dualsource") == "false" && m_x3.source == 2) {
         d.innerHTML = "<span>No dual source.</span>";
         return false;
      }

      if(unescape(d.getAttribute("process_type")).indexOf("v->2") == -1) {
         d.innerHTML = "<span>Process type without workflow.</span>";
         return false;
      }

      tpm = document.querySelector("[form='m_x3__workflow']").getAttribute("tpm");
      
      if(tpm == session.domino_user_cn) {
         d.innerHTML = 
         "<span class=\"phpbutton\" style=\"margin-right:3px;\"><a href=\"javascript:m_x3.edit_dates(m_x3.source, 'planned');\">planned</a></span>" +
         "<span class=\"phpbutton\" style=\"margin-right:3px;\"><a href=\"javascript:m_x3.edit_dates(m_x3.source, 'actual');\">actual</a></span>" +
         "<span class=\"phpbutton\" style=\"margin-right:3px;\"><a href=\"javascript:m_x3.create_loop(String(m_x3.source));\">new Loop</a></span>" +
         "<br><br>";
      }
      arr = ["Planned", "Actual"];
      wf_container = document.querySelector("[form='m_x3__workflow']");

      wf_s1 = (typeof workflow.g04100 != "undefined") ? workflow["g04100"].loop : "";
      wf_s2 = (typeof workflow.g04120 != "undefined") ? workflow["g04120"].loop : "";
      wf_container.setAttribute("v2_loop", wf_s1 + "," + wf_s2);
      if(typeof workflow[m_x3.gate] == "undefined") return false;
      for(i = workflow[m_x3.gate].loop; i >= 0; i--) {
         table = document.createElement("table");
         tr = document.createElement("tr");
         table.appendChild(tr);  
         th = document.createElement("th");
         th.textContent = "Loop " + i;
         tr.appendChild(th);
         for(k in arr) {
            th = document.createElement("th");
            th.textContent = arr[k];
            tr.appendChild(th); 
         }

         for(k in workflow[m_x3.gate].definition) {
            tr = document.createElement("tr"); 
            table.appendChild(tr); 
            td = document.createElement("td"); 
            td.textContent = unescape(workflow[m_x3.gate].definition[k].text);
            tr.appendChild(td);
            for(m in arr) {
               if(i == Number(workflow[m_x3.gate].loop) && arr[m].toLowerCase() == "actual") {
                  attr = "v2_" + unescape(workflow[m_x3.gate].definition[k].text).replace(/ /g, "_").toLowerCase();
                  wf_container.setAttribute(attr, workflow[m_x3.gate][arr[m].toLowerCase()][workflow[m_x3.gate].definition[k].fieldname][i].replace(/\//g, ""));
               }

               td = document.createElement("td"); 
               td.textContent = workflow[m_x3.gate][arr[m].toLowerCase()][workflow[m_x3.gate].definition[k].fieldname][i].replace(/\//g, "");
               tr.appendChild(td);
            }
         }
         d.appendChild(table);
      }

   },

   edit_dates: function() {

      tpe = m_x3.gate;

      gadget.calendar.unid = workflow[tpe].unid;
      gadget.calendar.elements = {};
     
      for(k in workflow[tpe].definition) {
         editable = (workflow[tpe]["actual"][workflow[tpe].definition[k].fieldname][workflow[tpe].loop].replace(/\//g, "") == "") ? true : false;
         gadget.calendar.elements[("wf$" + arguments[1] + "_" + workflow[tpe].definition[k].fieldname + "_loop_" + workflow[tpe].loop).toLowerCase()] = [
            unescape(workflow[tpe].definition[k].text),
            workflow[tpe][arguments[1]][workflow[tpe].definition[k].fieldname][workflow[tpe].loop].replace(/\//g, ""),
            workflow[tpe][arguments[1]][workflow[tpe].definition[k].fieldname][workflow[tpe].loop].replace(/\//g, ""),
            editable
         ];
         rule = [];
         if(arguments[1] == "planned") {
           ad = 0;
           for(i = (Number(k) + 1); i < workflow[tpe].definition.length; i++) {
              if(Number(workflow[tpe].calculate[workflow[tpe].definition[i].fieldname]) != 0) {
                 ad += Number(workflow[tpe].calculate[workflow[tpe].definition[i].fieldname]);
                 rule.push("wf$" + arguments[1] + "_" + workflow[tpe].definition[i].fieldname + "_loop_" + workflow[tpe].loop + ":+" + ad);
              }
           }
         }
         if(rule.length > 0) {
            gadget.calendar.elements[("wf$" + arguments[1] + "_" + workflow[tpe].definition[k].fieldname + "_loop_" + workflow[tpe].loop).toLowerCase()][4] = rule;
         }
      }


      infobox("Edit " + arguments[1] + " dates", "<div id=\"gadget.calendar.small\"></div>", 1250, 800);
      window.setTimeout("gadget.calendar.nest('gadget.calendar.small')", 100);
   },

   create_loop: function() {
      if(arguments[0].length == 1) {
         style = [];
         style[0] = (arguments[0] == "1") ? "opacity:1.0;" : "opacity:0.5; cursor:default;";
         style[1] = (arguments[0] == "2") ? "opacity:1.0;" : "opacity:0.5; cursor:default;";
         html = 
         "<button class=\"tebutton\" style=\"" + style[0] + "\" onclick=\"m_x3.create_loop(workflow.g04100.unid, workflow.g04100.loop);\">Create new loop for source 1</button>" + 
         "<button class=\"tebutton\" style=\"" + style[1] + "\" onclick=\"m_x3.create_loop(workflow.g04120.unid, workflow.g04120.loop);\">Create new loop for source 2</button>";
         infobox("Create new loop", html, 462, 80);
      }
      else {
         data = "unid=" + arguments[0] + "&fld1=wf$loop/" + escape(Number(arguments[1]) + 1); 
         form = "f.save_data";
         db = session.remote_domino_path_main;
         js = "location.reload()";
         url = session.remote_database_path + "addin/save_multiple_fields.php?form=" + escape(form) + "&db=" + escape(db);
         run_ajax("run_ajax", url, js, data);  
      }
   },

   change_source: function(e) {
      document.perpage.m_x3__workflow_source.value = e[e.selectedIndex].value;
      save_perpage("", true);
   },
}

workflow = {};
<?php
session_start();
require_once("../../../../library/tools/addin_xml.php");
$file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.json.workflow/".$_SESSION["pot"]."@".$_SESSION["tracker"]."@g04100");

print $file;
$file = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/v.json.workflow/".$_SESSION["pot"]."@".$_SESSION["tracker"]."@g04120");
print $file;

?>
   