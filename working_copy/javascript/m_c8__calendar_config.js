embed_calendar_config = function() {

   $(function() {
      $( '#accordion_filters' ).accordion({ heightStyle: 'content'}); 
      $( '#accordion_content' ).accordion({ heightStyle: 'content'});
      $( '#tabs' ).tabs(); 
    });

}


function change_value(f, v) {
   d = document.getElementsByName(f);
   if(d) {
      d[0].value = v;
   }
   save_perpage(true);
}

function handle_dropCalendar(e, id) {
	
   e.preventDefault();
   d = e.dataTransfer.getData("Text");
   drag_type = ($("#" + d).attr("type"));   
   drop_allow = (typeof arguments[1] == "undefined") ? e.target.getAttribute("allow_type").split(",") : $("#" + id).attr("allow_type").split(",");

   c = $.inArray(drag_type, drop_allow);
   if(c > -1) {
	   e.stopPropagation();
	   window["handle_drop_create_filter"](e, document.getElementById(d));
   }
}
function handle_dropSubject(e, id) {
	
	   e.preventDefault();
	   d = e.dataTransfer.getData("Text"); 
	   
	   drag_type = ($("#" + d).attr("type"));   
	   drop_allow = (typeof arguments[1] == "undefined") ? e.target.getAttribute("allow_type").split(",") : $("#" + id).attr("allow_type").split(",");

	   c = $.inArray(drag_type, drop_allow);
	   if(c > -1) {
		   e.stopPropagation();
		   window["handle_drop_subject_added"](e, document.getElementById(d));
	   }
} 
function handle_dropBody(e, id) {
	
	   e.preventDefault();
	   d = e.dataTransfer.getData("Text"); 
	   
	   drag_type = ($("#" + d).attr("type"));   
	   drop_allow = (typeof arguments[1] == "undefined") ? e.target.getAttribute("allow_type").split(",") : $("#" + id).attr("allow_type").split(",");

	   c = $.inArray(drag_type, drop_allow);
	   if(c > -1) {
		   e.stopPropagation();
		   window["handle_drop_body_added"](e, document.getElementById(d));
	   }
} 
function handle_dropLocation(e, id) {
	
	   e.preventDefault();
	   d = e.dataTransfer.getData("Text"); 
	   
	   drag_type = ($("#" + d).attr("type"));   
	   drop_allow = (typeof arguments[1] == "undefined") ? e.target.getAttribute("allow_type").split(",") : $("#" + id).attr("allow_type").split(",");

	   c = $.inArray(drag_type, drop_allow);
	   if(c > -1) {
		   e.stopPropagation();
		   window["handle_drop_location_added"](e, document.getElementById(d));
	   };
} 
function handle_dropCategory(e, id) {
	
	   e.preventDefault();
	   d = e.dataTransfer.getData("Text"); 
	   
	   drag_type = ($("#" + d).attr("type"));   
	   drop_allow = (typeof arguments[1] == "undefined") ? e.target.getAttribute("allow_type").split(",") : $("#" + id).attr("allow_type").split(",");

	   c = $.inArray(drag_type, drop_allow);
	   if(c > -1) {
		   e.stopPropagation();
		   window["handle_drop_category_added"](e, document.getElementById(d));
	   }
} 
function handle_dropField(e, id) {
	
   
   e.preventDefault();
   var d = e.dataTransfer.getData("Text"); 

   
   drag_type = ($("#" + d).attr("type"));   
   drop_allow = (typeof arguments[1] == "undefined") ? e.target.getAttribute("allow_type").split(",") : $("#" + id).attr("allow_type").split(",");

   c = $.inArray(drag_type, drop_allow);

   if(c > -1) {
	   window["handle_drop_field_added"](e, document.getElementById(d));
   }
}

function handle_drop_field_added(e, d) {
		// d.setAttribute("style", "display:none");
		var o = d.parentNode;
		var filtertype = d.getAttribute("filtertype");
		var filterformula = d.getAttribute("filterformula");
		container = document.getElementById("container_body");
		var newId = "m_c8_filter_" + getUUID();
		  var div = document.createElement( "div" );

		    div.setAttribute( "id", newId  );
		    div.setAttribute( "draggable", "true");
		    div.setAttribute( "ondragstart", "drag(event)");
		    addDropAttributes( div );
		    
		    div.innerHTML = filtertype;

		    container.appendChild( div );
	
		arr = new Array;
		for(var i = 0; i < container.getElementsByTagName("div").length; i++) { 
			arr.push(o.getElementsByTagName("div")[i].textContent);
		 }
		var data =  arr.join(",");
		m_c8_set_calendar_value("m_c8_options_dump", data );
		
	} 


function handle_drop_subject_added(e, d) {
	d.setAttribute("style", "display:none");
	var o = d.parentNode;
// var table = document.getElementById( "m_c8_subject" );
	var table = $( "#m_c8_subject" );
	var tbody = table.find( "tbody")[0];
	var filtertype = d.getAttribute("filtertype");
	var filterformula = d.getAttribute("filterformula");
	var filtername = d.getAttribute("filtername");
	
	container = document.getElementById("container_subject");
	var newId = "m_c8_subject";// + getUUID();
	
	var cell = createNewRowNoValue( newId, tbody, filtertype, filterformula , filtername, addDropAttributesSubject  );
	newId += "_" + filtertype;
	
	  var div = document.createElement( "div" );

	    div.setAttribute( "id", newId  );
	    div.setAttribute( "draggable", "true");
	    div.setAttribute( "ondragstart", "drag(event)");
	    addDropAttributesSubject( div );
	    
	   // div.innerHTML = filtertype;

	    cell.appendChild( div );

	arr = new Array;
	for(var i = 0; i < container.getElementsByTagName("div").length; i++) { 
		arr.push(o.getElementsByTagName("div")[i].textContent);
	 }
	var data =  arr.join(",");
	m_c8_set_calendar_value("m_c8_options_dump", data );
} 
function handle_drop_body_added(e, d) {
	d.setAttribute("style", "display:none");
	var o = d.parentNode;
	var table = document.getElementById( "m_c8_body" );
	var table = $( "#m_c8_body" );
	var tbody = table.find( "tbody")[0];
	var filtertype = d.getAttribute("filtertype");
	var filterformula = d.getAttribute("filterformula");
	var filtername = d.getAttribute("filtername");
	
	container = document.getElementById("container_body");
	var newId = "m_c8_body";// + getUUID();
	
	var cell = createNewRowNoValue( newId, tbody, filtertype, filterformula, filtername, addDropAttributesBody );
	newId += "_" + filtertype;
	
	  var div = document.createElement( "div" );

	    div.setAttribute( "id", newId  );
	    div.setAttribute( "draggable", "true");
	    div.setAttribute( "ondragstart", "drag(event)");
	    addDropAttributesBody( div );
	    
	   // div.innerHTML = filtertype;

	    cell.appendChild( div );

	arr = new Array;
	for(var i = 0; i < container.getElementsByTagName("div").length; i++) { 
		arr.push(o.getElementsByTagName("div")[i].textContent);
	 }
	var data =  arr.join(",");
	m_c8_set_calendar_value("m_c8_options_dump", data );
}
function handle_drop_location_added(e, d) {
	d.setAttribute("style", "display:none");
	var o = d.parentNode;
	var table = $( "#m_c8_location" );
	var tbody = table.find( "tbody")[0];
	var filtertype = d.getAttribute("filtertype");
	var filterformula = d.getAttribute("filterformula");
	var filtername = d.getAttribute("filtername");
	
	container = document.getElementById("container_location");
	var newId = "m_c8_location";// + getUUID();
	
	var cell = createNewRowNoValue( newId, tbody, filtertype, filterformula, filtername , addDropAttributesLocation);
	newId += "_" + filtertype;
	
	  var div = document.createElement( "div" );

	    div.setAttribute( "id", newId  );
	    div.setAttribute( "draggable", "true");
	    div.setAttribute( "ondragstart", "drag(event)");
	    addDropAttributesLocation( div );
	    
	   // div.innerHTML = filtertype;

	    cell.appendChild( div );

	arr = new Array;
	for(var i = 0; i < container.getElementsByTagName("div").length; i++) { 
		arr.push(o.getElementsByTagName("div")[i].textContent);
	 }
	var data =  arr.join(",");
	m_c8_set_calendar_value("m_c8_options_dump", data );
}
function handle_drop_category_added(e, d) {
	d.setAttribute("style", "display:none");
	var o = d.parentNode;
	var table = $( "#m_c8_category" );
	var tbody = table.find( "tbody")[0];
	var filtertype = d.getAttribute("filtertype");
	var filterformula = d.getAttribute("filterformula");
	var filtername = d.getAttribute("filtername");
	
	container = document.getElementById("container_category");
	var newId = "m_c8_category";// + getUUID();
	
	var cell = createNewRowCategory( newId, tbody, filtertype, filterformula, filtername , addDropAttributesCategory);
	newId += "_" + filtertype;
	
	  var div = document.createElement( "div" );

	    div.setAttribute( "id", newId  );
	    div.setAttribute( "draggable", "true");
	    div.setAttribute( "ondragstart", "drag(event)");
	    addDropAttributesCategory( div );
	 
	    cell.appendChild( div );


		
	arr = new Array;
	for(var i = 0; i < container.getElementsByTagName("div").length; i++) { 
		arr.push(o.getElementsByTagName("div")[i].textContent);
	 }
	var data =  arr.join(",");
	m_c8_set_calendar_value("m_c8_options_dump", data );
}

function handle_drop_create_filter( e, d ) {

	d.setAttribute("style", "display:none");
	
	var table = document.getElementById( "m_c8_filter" );
	var newId = "m_c8_filter_" + getUUID();
	var filtertype = d.getAttribute("filtertype");

	var filterformula = d.getAttribute("filterformula");
	var cell;
	
	switch( filtertype ){
	case "tpm":
		cell = createNewRow( newId, table, filtertype, filterformula , "TP&M");
	break;
	case "spprp":
		cell = createNewRow( newId, table, filtertype, filterformula , "Sub process phase responsible person");
	break;
	case "locationfilter":
		cell = createNewRow( newId, table, filtertype, filterformula , "Product Location G3");
	break;
	case "toolsupplier":
		cell = createNewRow( newId, table, filtertype, filterformula , "Tool Supplier");
	break;	
	case "phase":
		cell = createNewRow( newId, table, filtertype, filterformula , "Reportable Date");
	break;	
	default:
		cell = createNewRow( newId, table, filtertype, filterformula);
	break;
	}

	newId += "_" + filtertype;

	
	switch( filtertype ){
	case "text":
	    var inputText = document.createElement( "input" );
	    inputText.setAttribute( "type", "text" );
	    inputText.setAttribute( "value", filterformula);
	    inputText.setAttribute( "id", newId );
	    inputText.setAttribute( "name", newId );
	    m_c8_set_calendar_value( newId, filterformula );
		cell.appendChild( inputText );
		save_calendarToNotes();

	break;
	case "date":
	case "startdate":
	case "enddate":
	    var img = document.createElement( "img" );
	    img.setAttribute( "src", "/library/images/16x16/time-3.png" );
	    img.setAttribute( "onclick", "s = $(document.getElementById('m_c8_container').parentNode).scrollTop();load_calendar(this, '" + newId + "', 'm_c8_set_calendar_date', 'Select start date', 79, (255 - s));" );
	    img.setAttribute( "style", "cursor:pointer;margin:3px;vertical-align:top;" );
	    
	    var div = document.createElement( "div" );
	    div.setAttribute( "class", "textcontent" );
	    div.setAttribute( "id", newId );
	    addDropAttributes( div );
		cell.appendChild( img );
		cell.appendChild( div );
		save_calendarToNotes();
	break;
	case "resource":
	case "tpm":
	case "spprp":
	case "locationfilter":
	case "toolsupplier":
	case "phase":
	    var div = document.createElement( "div" );
	    div.setAttribute( "id", newId  );
	    addDropAttributes( div );
	    div.innerHTML = filterformula;
		cell.appendChild( div );
		m_c8_set_calendar_value( newId, filterformula );
		
		d.setAttribute("style", "display:none;");
        
	break;

		
	default:
	break;
	}
	

}
function addDropAttributes( elem , value ){
	
	elem.setAttribute( "ondrop", "handle_dropCalendar(event);");
	elem.setAttribute( "ondragover", "allow_drop(event);");
	elem.setAttribute( "allow_type", "create_filter,resource,tpm,spprp,locationfilter,toolsupplier,phase");
}
function addDropAttributesSubject( elem , value ){
	
	elem.setAttribute( "ondrop", "handle_dropSubject(event);");
	elem.setAttribute( "ondragover", "allow_drop(event);");
	elem.setAttribute( "allow_type", "subject");
}
function addDropAttributesBody( elem , value ){
	
	elem.setAttribute( "ondrop", "handle_dropBody(event);");
	elem.setAttribute( "ondragover", "allow_drop(event);");
	elem.setAttribute( "allow_type", "body");
}
function addDropAttributesLocation( elem , value ){
	
	elem.setAttribute( "ondrop", "handle_dropLocation(event);");
	elem.setAttribute( "ondragover", "allow_drop(event);");
	elem.setAttribute( "allow_type", "location");
}
function addDropAttributesCategory( elem , value ){
	
	elem.setAttribute( "ondrop", "handle_dropCategory(event);");
	elem.setAttribute( "ondragover", "allow_drop(event);");
	elem.setAttribute( "allow_type", "category");
}
function createNewRow( id, table, filtertype, filterformula ){
	
	// create a new value container
	var idValue = id + "_" + filtertype + "_value";
    var input = document.getElementById( idValue );
    
    if( input == null ){
	    input = document.createElement( "input" );
	    input.setAttribute("type", "hidden");
	    input.setAttribute( "id", idValue );
	    input.setAttribute( "name", idValue );
		document.forms['calendarprofile'].appendChild( input );
	 }
    
    // create delete Link
    var delLink = document.createElement( "img");
    delLink.setAttribute("src", "/library/images/16x16/edition-43.png" );
    delLink.setAttribute("onclick", "deleteCalendarEntry('" + id +"_" + filtertype + "');");
    
    // create table row
    var thead = document.createElement( "thead");
    thead.setAttribute("id", "thead_" + id + "_" + filtertype );
    thead.setAttribute("filterformula", filterformula );
    thead.setAttribute("filtertype", filtertype );
    
	var tr = document.createElement( "tr" );
	var td1 = document.createElement( "td" );
	td1.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;' );
		
	var td2 = document.createElement( "td" );
	td2.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:center;padding:2px 0px 0px 0px;' );
	
	var td3 = document.createElement( "td" );
	td3.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;' );

	td1.textContent = arguments[4]?arguments[4]:filtertype;
	td2.appendChild( delLink );
	
	addDropAttributes( td1 );
	addDropAttributes( td2 );
	tr.appendChild( td1 );
	tr.appendChild( td2 );
	tr.appendChild( td3 );
	thead.appendChild( tr );
	table.appendChild( thead );
	
	return td3;
	
}
function createNewRowNoValue( id, table, filtertype, filterformula, filtername, addDropAttrFunction ){
	
	// create a new value container
	var idValue = id + "_" + filtertype + "_value";
    var input = document.getElementById( idValue );
    
    if( input == null ){
	    input = document.createElement( "input" );
	    input.setAttribute("type", "hidden");
	    input.setAttribute( "id", idValue );
	    input.setAttribute( "name", idValue );
		document.forms['calendarprofile'].appendChild( input );
	 }
    
    // create delete Link
    var delLink = document.createElement( "img");
    delLink.setAttribute("src", "/library/images/16x16/edition-43.png" );
    delLink.setAttribute("onclick", "deleteEntry('" + id +"_" + filtertype + "');");
    
    // create table row
	var tr = document.createElement( "tr" );
	tr.id = id + "_" + filtertype + "_row";
	tr.setAttribute("filtertype", filtertype);
	tr.setAttribute("filterformula", filterformula);
	
	var td1 = document.createElement( "td" );
	td1.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;' );
		
	var td2 = document.createElement( "td" );
	td2.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:center;padding:2px 0px 0px 0px;' );
	
	td1.textContent = filtername;
	td2.appendChild( delLink );
	
	addDropAttrFunction( td1 );
	addDropAttrFunction( td2 );
	tr.appendChild( td1 );
	tr.appendChild( td2 );

	table.appendChild( tr );
	
	return td2;
	
}

function createNewRowCategory( id, table, filtertype, filterformula, filtername, addDropAttrFunction ){
	
	// create a new value container
	var idValue = id + "_" + filtername + "_value";
    var input = document.getElementById( idValue );
   if( input == null ){
	    input = document.createElement( "input" );
	    input.setAttribute("type", "hidden");
	    input.setAttribute( "id", idValue );
	    input.setAttribute( "name", idValue );
	    input.setAttribute( "value", "");
		document.forms['calendarprofile'].appendChild( input );
	 }
    
    // create delete Link
    var delLink = document.createElement( "img");
    delLink.setAttribute("src", "/library/images/16x16/edition-43.png" );
    delLink.setAttribute("onclick", "deleteEntry('" + id +"_" + filtertype + "');");
    
    // create table row
	var tr = document.createElement( "tr" );
	tr.id = id + "_" + filtertype + "_row";
	tr.setAttribute("filtertype", filtertype);
	tr.setAttribute("filterformula", filterformula);
	
	var td1 = document.createElement( "td" );
	td1.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:left;padding:2px 0px 0px 0px;' );
		
	var td2 = document.createElement( "td" );
	td2.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);padding:2px 0px 0px 0px;' );
	
	var td3 = document.createElement( "td" );
	td3.setAttribute('style', 'border:solid 1px rgb(255,255,255);background-color:rgb(220,220,220);text-align:center;padding:2px 0px 0px 0px;' );
	
	
	td1.textContent = filtername;
	var arr = [
	           {val : "red", text: 'Red'},
	           {val : "yellow", text: 'Yellow'},
	           {val : "blue", text: "Blue"},
	           {val : "green", text: "Green"},
	           {val : "orange", text: "Orange"},
	         ];

	         var sel = $('<select>').appendTo(td2);
	         sel.attr('id', id + "_select" );
	         sel.attr('idValue', idValue );
	         sel.attr('name', idValue );
	         sel.change( function() {
	        	 value = $(this).val();
	        	id = $(this).attr('idValue');
	        	$('#' + id ).val( value );
	        	save_calendarToNotes();
	         });
	         
	         $(arr).each(function() {
	          sel.append($("<option>").attr('value',this.val).text(this.text));
	         });
    // m_c8_set_calendar_value( newId, filterformula );
	// td1.appendChild( inputText );
	
	td3.appendChild( delLink );
	
	addDropAttrFunction( td1 );
	addDropAttrFunction( td2 );
	addDropAttrFunction( td3 );
	tr.appendChild( td1 );
	tr.appendChild( td2 );
	tr.appendChild( td3 );
	
	table.appendChild( tr );
	
	return td3;
	
}

Date.prototype.adjust = function(yr,mn,dy,hr,mi,se) {
	var m,t;
	this.setYear(this.getFullYear() + yr);
	m = this.getMonth() + mn;
	if(m != 0) this.setYear(this.getFullYear() + Math.floor(m/12));
	if(m < 0) {
	this.setMonth(12 + (m%12));	
	} else if(m > 0) {
	this.setMonth(m%12);
	}
	t = this.getTime();
	t += (dy * 86400000);
	t += (hr * 3600000);
	t += (mi * 60000);
	t += (se * 1000);
	this.setTime(t);
}

function m_c8_set_calendar_date(f, y, m , d) {

   v = y + "-" + m + "-" + d;
   var dateSelected = new Date( v );
   var datePre = new Date();
   datePre.adjust( 0, -3, 0,0,0,0 );
   var dateNext = new Date();
   dateNext.adjust( 1, 0, 0,0,0,0 );

 

   if ( dateSelected.valueOf() < datePre.valueOf() ){
	   alert( "You can only select a date 3 monthes before today.");
	   return false;
   }
   if ( dateSelected.valueOf() > dateNext.valueOf() ){
	   alert( "You can only select a date 12 monthes after today.");
	   return false;
   }
   if( f == "m_c8_startdate"){
	   var txt =  document.getElementById('m_c8_enddate').textContent;
	   var dateEnd = new Date( txt );
	   
	   if( dateEnd.valueOf() < dateSelected.valueOf() ){
		   alert( "The selected start date must be before end date.");
		   return false;
	   }
   }else{
	   var txt =  document.getElementById('m_c8_startdate').textContent;
	   var dateStart = new Date( txt );
	   
	   if( dateStart.valueOf() > dateSelected.valueOf() ){
		   alert( "The selected end date must be after start date.");
		   return false;
	   }
   }


   document.getElementById('calendar').style.display = 'none';
   o = document.getElementById(f);
   o.textContent = v;
   o.style.display = 'block';
   save_calendarToNotes();
   do_fade(false);
   
}



function m_c8_set_calendar_value(f, v) {
	try{
		o = document.getElementById( f + "_value" );
		o.value = v;
	}catch(e){
		console.log(e);
	}
	save_calendarToNotes();
}

function deleteEntry( id ){

	var filtertype, filtervalue, hlpid;
	
	try{
		hlpid = id.replace( "_value", "" );
		hlpid = hlpid;
		
		var elem = $("#" + hlpid +"_row" );
		filtertype = elem.attr("filtertype");
		filtervalue = elem.attr("filterformula");
		elem.remove();
		
	}catch(e){ console.log(e); }
	
	try{
		var elem = $("#" + hlpid + "_value" );
		elem.remove();
	}catch(e){ console.log(e);  }
		
	if( filtertype == "subprocessphaseresponsibleperson" ){
		$("[filtertype=" + filtertype + "]").attr("style", "height:48px;display:block");
	}else{
		$("[filtertype=" + filtertype + "]").attr("style", "display:block");
	}
	
	// save_calendar();
	save_calendarToNotes();
	
}

function deleteCalendarEntry( id ){

	var filtertype, filtervalue;
	
	try{
		var elem = document.getElementById( id );
		var parent = elem.parentNode;
		parent.removeChild(elem);
	}catch(e){ console.log(e); }
	
	try{
		var elem = document.getElementById( id + "_value" );
		var parent = elem.parentNode;
		parent.removeChild(elem);
	}catch(e){ console.log(e);  }
	
	try{
		elem = document.getElementById("thead_" + id );
		filtertype = elem.getAttribute("filtertype");
		filtervalue = elem.getAttribute("filterformula");
		
		parent = elem.parentNode;
		parent.removeChild(elem);
	}catch(e){ console.log(e); }
	
	
	switch( filtertype ){
	case "startdate":
	case "enddate":
		$("[filtertype=" + filtertype + "]").attr("style", "display:block");
	break;
	case "resource":
	case "tpm":
	case "spprp":
	case "locationfilter":
	case "phase":
		$("[filterformula=\"" + filtervalue + "\"]").attr("style", "display:block");
	break;
	default:
	break;
	}
	save_calendarToNotes();
	
}

function save_calendar() {
	
	   js = (typeof arguments[0] == "undefined") ? "" : arguments[0];
	   if(js == true) js = "location.href=location.href";
	   f = document.getElementsByName("calendarprofile")[0];
	   d = f.getElementsByTagName("input");
	   r = new Array;
	   for(var i = 0; i < d.length; i++) {
	      r.push(d[i].name + "=" + escape(d[i].value));
	   }
	   url =  "addin/ajax_updateCalendar.php?t=" + new Date().getTime();

	   do_fade(true); 
	   js= "do_fade(false)";
	   
	   run_ajax("run_ajax", url, js, r.join("&"));
} 


function save_calendarToNotes() {
	try{
	var fieldList = new Array;
	var data = new Array;
	var entry = null;
	var profileUNID = ""; 
	var target = "";
	var r = new Array;
	
	var js = (typeof arguments[0] == "undefined") ? "" : arguments[0];
	if(js == true) js = "location.href=location.href";
	
	var f = document.getElementsByName("calendarprofile")[0];
	var d = f.getElementsByTagName("input");

	today = new Date(<?php print date("Y, m, d"); ?>, 0, 0, 0, 0);

	for(var i = 0; i < d.length; i++) {
		if( d[i].name === 'v')
			continue;

		if( fieldList.indexOf( d[i].name ) > -1 ){
			continue; 
		}else{
			fieldList.push( d[i].name );
		} 
		
		entry = d[i].name + "=" + escape(d[i].value);
		   
		if( $.inArray(entry, r) === (-1) )
			r.push(entry);
	       
	       
		switch( d[i].name ){
		case "db":
	       	target = d[i].value;
	    break;
		case "unid":
			profileUNID = d[i].value;
		break;
		case "v":
	    case "f":
	    case "redirect":
	    break;
	    default:
	    	if(d[i].name.indexOf("enddate_value") == -1 && d[i].name.indexOf("startdate_value") == -1) {
	    		if( $.inArray(entry, data) === (-1) ){
	    			data.push( entry );
	    		}
	    	}
	       	
	       	t_date = (document.getElementById("m_c8_startdate").textContent).split("-");
	       	entry = "m_c8_filter_" + session.remote_perportal_unid.toLowerCase() + "_startdate_value=" + t_date.join("-");
				
			if( $.inArray(entry, data) === (-1) ){
				data.push( entry );
			}
			if( $.inArray(entry, r) === (-1) ){
				r.push( entry );
			}
                
			startdate = new Date(t_date[0], t_date[1], t_date[2], 0, 0, 0, 0);
			entry = ("m_c8_filter_" + session.remote_perportal_unid.toLowerCase() + "_start_from_now") + "=" + (((startdate - today) / 86400) / 1000);
			if( $.inArray(entry, data) === (-1) ){
				data.push( entry );
			}
			if( $.inArray(entry, r) === (-1) ){
				r.push( entry );
			}

			t_date = (document.getElementById("m_c8_enddate").textContent).split("-");
			entry = "m_c8_filter_" + session.remote_perportal_unid.toLowerCase() + "_enddate_value=" + t_date.join("-");
			if( $.inArray(entry, data) === (-1) ){
				data.push( entry );
			}
			if( $.inArray(entry, r) === (-1) ){
				r.push( entry );
			}
			
			enddate = new Date(t_date[0], t_date[1], t_date[2], 0, 0, 0, 0);
			entry = ("m_c8_filter_" + session.remote_perportal_unid.toLowerCase() + "_end_from_now") + "=" + (((enddate - today) / 86400) / 1000)
			if( $.inArray( entry, data) === (-1) ){
				data.push( entry );
			}
			if( $.inArray(entry, r) === (-1) ){
				r.push( entry );
			}
			
	      }
	  }

	r.push("saved=<?php print date("Y-m-d"); ?>");
	data.push("saved=<?php print date("Y-m-d"); ?>");
	r.push( 'v=' + escape( data.join("&")));

	url = "addin/save_calendar.php?&t=" +  new Date().getTime();// + "&" +
																	// r.join("&");
	js = "do_fade(false)";
	run_ajax("run_ajax", url, js, r.join("&") );


	   
	// generate calendar
	// if( profileUNID !== ""){
	// url = "addin/ajax_generateCalendar.php?unid=" + profileUNID + "&target="
	// + nsfPath + "&t=" + new Date().getTime();;
	// calendar_run_ajax("calendar_saveresult_link", url, "do_fade(false);" ,
	// "");
	//		     
	// }
	}catch(e){
		console.log("save_calendarToNotes()::" + e)
	}
} 

function getUUID(){
	
	return 'xxxx'.replace(/[xy]/g, function(c) {
	    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
	    return v.toString(16);
	});
	
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
	    return v.toString(16);
	});
	
}
function change_option_reminderEnabled() {
	   var v = (arguments[0].src.indexOf("status-5-1") > 0) ? 1 : 0;
	   arguments[0].src = (v == 1) ? session.php_server + "/library/images/16x16/status-5.png" : session.php_server + "/library/images/16x16/status-5-1.png";

;
	   var forms =  document.getElementsByName("calendarprofile");
	   var form = forms[0];
	   var field = form['m_c8_options_reminderenabled_value'];

	   if( !field ){
	    field = document.createElement( "input" );
	    field.setAttribute("type", "hidden");
	    field.setAttribute( "id", "m_c8_options_reminderenabled_value" );
	    field.setAttribute( "name", "m_c8_options_reminderenabled_value" );
		document.forms['calendarprofile'].appendChild( field );
	   }
	   if( v === 1 ){
		field.value = "1";
	   }else{
		 field.value = "0";
	   }
	   save_calendarToNotes();
				   
	   // save_calendar();
	} 


function change_option_reminder(){
	var f = arguments[0];
	var v = f.options[f.selectedIndex].value;

	var forms =  document.getElementsByName("calendarprofile");
	var form = forms[0];
	var field = form['m_c8_options_reminder_value'];
	   if( !field ){
		    field = document.createElement( "input" );
		    field.setAttribute("type", "hidden");
		    field.setAttribute( "id", "m_c8_options_reminder_value" );
		    field.setAttribute( "name", "m_c8_options_reminder_value" );
			document.forms['calendarprofile'].appendChild( field );
		   }
	field.value = v;
	// save_calendar();
	save_calendarToNotes();
}

function change_option_showAsRangeEnabled() {
   var v = (arguments[0].src.indexOf("status-5-1") > 0) ? 1 : 0;
   arguments[0].src = (v == 1) ? session.php_server + "/library/images/16x16/status-5.png" : session.php_server + "/library/images/16x16/status-5-1.png";

   var forms =  document.getElementsByName("calendarprofile");
   var form = forms[0];
   var field = form['m_c8_options_showasrange_value'];
   if( !field ){
	    field = document.createElement( "input" );
	    field.setAttribute("type", "hidden");
	    field.setAttribute( "id", "m_c8_options_showasrange_value" );
	    field.setAttribute( "name", "m_c8_options_showasrange_value" );
		document.forms['calendarprofile'].appendChild( field );
	   }
   if( v === 1 ){
	field.value = "1";
   }else{
	 field.value = "0";
   }
			   
  // save_calendar();
   save_calendarToNotes();
} 

function calendar_run_ajax(_id,_file,_runJS, _post) {
	   _post = (typeof arguments[3] == "undefined") ? null : arguments[3]; 
	   req = null;
	   try { req = new XMLHttpRequest(); }
	   catch (ms) {
	      try { req = new ActiveXObject("Msxml2.XMLHTTP"); }
	      finally {}
	   }
	   if (req == null) eval("document.getElementById(_id).innerHTML='error creating request object';");
	   else req.open("post",_file, true);
	   req.onreadystatechange = function() {
	      switch(req.readyState) {
	         case 4:
	       
	        	 var jsonResponse = JSON.parse(req.responseText);
	        	 if( jsonResponse.type == "success" ){
	        		 var elem =  document.getElementById(_id);
	        	 	elem.href = "calendar/" + jsonResponse.filename;
	        	 	elem.setAttribute('style', "");
	        	 }
	           if(_runJS != "undefined") eval(_runJS); 
	            break;
	         default:
	            return false; 
	            break;
	      }
	   }
	   req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	   req.send(_post);
	} 


function load_calendar(e, f, fct, txt) {
	   x = (typeof arguments[4] == "undefined") ? 0 : arguments[4];
	   y = (typeof arguments[5] == "undefined") ? 0 : arguments[5];
	   v = (typeof arguments[6] == "undefined") ? "" : arguments[6];
	   if(document.getElementById("calendar").style.display == "block") {
	      document.getElementById("calendar_" + id).style.display = "none";
	}   
	   else {
	      do_fade(true);
	      d = document.getElementById("calendar");
	      d.innerHTML = "";  
	      d.style.display = "block";
	      url = session.remote_database_path + "addin/calendar2.php?p_function=" + fct + "&txt=" + escape(txt) + "&p_use_rule=0&p_id=" + f + "&p_data_type=" + v + "&override_id=calendar";

	      pos = $(e).position();
	      d.style.top = (pos.top ) + "px";
	      d.style.left = (pos.left ) + "px";
	      run_ajax("calendar", url, "");
	   }
	}