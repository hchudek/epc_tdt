var post_it = null;

var move_x = 0;
var move_y = 0;

var pos_x = 0;
var pos_y = 0;

function init_post_it() {
   document.onmousemove = move_post_it;
   document.onmouseup = stop_post_it;
}

function start_post_it(e) {
   post_it = e;
   move_x = pos_x - post_it.offsetLeft;
   move_y = pos_y - post_it.offsetTop;

}

function stop_post_it() {
   unid = post_it.id;
   save = new container(
   "pos_x => " + post_it.style.left.replace("px", ""), 
   "pos_y => " + post_it.style.top.replace("px", "")
   );
   document.getElementsByTagName("html")[0].style.display = "none";
   save_data(unid, "post_it = null; location.href=location.href", 0);
}

function move_post_it(e) {
   pos_x = document.all ? window.event.clientX : ereignis.pageX;
   pos_y = document.all ? window.event.clientY : ereignis.pageY;
   if(post_it != null) {
      post_it.style.left = (pos_x - move_x) + "px";
      post_it.style.top = (pos_y - move_y) + "px";
   }
}

function delete_post_it(unid) {
   handle_save_single_field(unid, "is_deleted", "1", "location.href=location.href");
}

function create_post_it(page_id, query_string) {
   url = (session.remote_domino_path_main + "/f.post_it?open&user=" + escape(session.domino_user) + "&page_id=" + page_id + "&query=" + escape(query_string));
   ifrm_post_it = document.getElementsByName("post_it")[0];
   ifrm_post_it.src = url;
   ifrm_post_it.style.display = "block";
}

