peditor = {

   add: function(p_type, page_id, tab, dsp_name, added_modules, custom_id) {
      added_modules = (!added_modules) ? [] : added_modules.split(";");
      dsp_name = (!dsp_name) ? "" : dsp_name; 
      custom_id = (!custom_id) ? tab : custom_id; 
      url = "";
      m = [];
      m["value"] = [];
      m["dsp"] = [];

      <?php
  
      session_start();
      require_once("../application.ini");

      $modules = $_application["page"][$_SESSION["page_id"]]["template"]["add"];
      foreach($modules as $val) {
         if(trim(substr($val, strrpos($val, ",") + 1, strlen($val))) != "") {
            $m["value"][] = trim(substr($val, strrpos($val, ",") + 1, strlen($val)));
            $m["descr"][] = rawurldecode($_application["module"]["description"][trim(substr($val, strrpos($val, ",") + 1, strlen($val)))][0]);
         }
      }
      array_multisort($m["descr"], $m["value"]);
      $m = $m["value"];
      for($i = 0; $i < count($m); $i++) {
         $descr = rawurldecode($_application["module"]["description"][$m[$i]][0]);
         if($descr != "") {
            print "      m[\"value\"].push(\"".$m[$i]."\")\r\n";
            print "      m[\"dsp\"].push(\"".$descr."\")\r\n";
         }
      }

      ?>
 
      do_fade(true);
      div = document.createElement("div");
      div.setAttribute("id", "confirm_box");
      document.getElementsByTagName("body")[0].appendChild(div);
      p_left = Math.round(window.innerWidth - 60);
      p_top = Math.round(window.innerHeight - 60);

      h = document.createElement("div");
      h.setAttribute("class", "header");
      h.setAttribute("style", "width:" + (p_left - 5) + "px;");
      h.textContent = (p_type) ? "Create perspective" : "Edit perspective";
      div.appendChild(h);
      b = document.createElement("div");

      inner_html = "<form name=\"edit_perspective\" method=\"post\" action=\"addin/edit_perspective.php\" style=\"margin:0px;padding:0px;\">" + 
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top:20px;\">" + 
      "<tr>" + 
      "<td style=\"vertical-align:top; \">" + 
      "<div class=\"body\" style=\"position:relative;top:-10px;\">" + 
      "<div><b>Perspective name:</b></div>" + 
      "<input type=\"hidden\" name=\"page_id\" value =\"" + document.getElementById("tdt4grid").getAttribute("page_id") + "\" />" +
      "<input type=\"hidden\" name=\"tab\" value =\"" + tab + "\" />" +
      "<input type=\"hidden\" name=\"delete\" value =\"\" />" +
      "<input type=\"hidden\" name=\"do\" value =\"" + String(p_type) + "\" />" +
      "<input type=\"hidden\" name=\"r\" value=\"\" />" + 
      "<input type=\"hidden\" name=\"custom_id\" value=\"" + custom_id + "\" />" + 
      "<input type=\"text\" name=\"perspective\" value=\"" + dsp_name + "\" onkeyup=\"if(this.value !='' && event.keyCode == 13) location.href='" + url + "&pname='+escape(this.value); peditor.check_ok_button();\" style=\"font:normal 13px Open Sans;width:377px;height:17px;border:solid 1px rgb(200,200,200);position:relative;top:4px;\" /></div>" +
      "</div>" +
      "<div class=\"button\" style=\"position:relative;top:3px;\">";
      if(!p_type) {
         inner_html += "<span class=\"phpbutton\" id=\"cancel_button\"><a href=\"javascript:void(0);\" onclick=\"peditor.delete('" + tab + "');\">Delete</a></span>"; 
      }
      inner_html += 
      "<span class=\"phpbutton\" id=\"cancel_button\"><a href=\"javascript:void(0);\" onclick=\"location.reload();\">Cancel</a></span>" + 
      "<span class=\"phpbutton\" id=\"ok_button\" style=\"display:none;\"><a href=\"javascript:void(0);\" onclick=\"d=document.edit_perspective;d.r.value=location.href;d.submit();\">OK</a></span>" +
      "</div>" + 
      "</td>" + 
      "<td style=\"vertical-align:top; margin-left:20px; border-left:solid 2px rgb(222,222,232); padding-left:20px;\">" +  
      "<div style=\"position:relative; top:2px; height:" + (p_top - 70) + "px;\">" + 
      "<div style=\"margin-bottom:6px;\"><b>Include modules:</b></div>" +
      "<input type=\"hidden\" name=\"modules\" value=\"" + added_modules.join(";") + "\" />";
      for(module in m["value"]) {
         img = "status-5-1";
         if(!p_type) {
            img = (in_array(m["value"][module], added_modules)) ? "status-5" : img;
         }
         inner_html += "<div><img src=\"" + session.php_server + "/library/images/16x16/" + img + ".png\" style=\"position:relative; left:-5px; vertical-align:top;margin:0px 4px 0px 10px;\" name=\"include_perspective\" value=\"" + m["value"][module] + "\" onclick=\"peditor.handle_include(this);\" />" + m["dsp"][module] + "</div>";
      }
      inner_html += "</div></td></tr></table>" + 
      "</form>" +
      "<br />" +
      "<br />";
      b.innerHTML = inner_html;
      div.appendChild(b);
      div.setAttribute("style", "left:30px;top:30px; width:" + p_left + "px; height:" + p_top + "px;");
      document.getElementsByName("perspective")[0].focus();
      peditor.check_ok_button();
   },

   handle_include: function(e) {
      dsp = (e.src.indexOf("status-5-1") > -1) ? "status-5" : "status-5-1";
      e.src = session.php_server + "/library/images/16x16/" + dsp + ".png";
      d = document.getElementsByName(e.getAttribute("name"));
      arr = [];
      for(i = 0; i < d.length; i++) {
         if(d[i].src.indexOf("status-5-1") == -1) arr.push(d[i].getAttribute("value"));
      }
      document.getElementsByName("modules")[0].value = arr.join(";");
      peditor.check_ok_button();
   },

   check_ok_button: function() {
      d = document.getElementsByName("include_perspective")
      dsp = false;
      for(k in d) if(!isNaN(k)) {
         if(d[k].src.indexOf("status-5-1") == -1) dsp = true;
      }
      if(document.getElementsByName("perspective")[0].value.trim() == "") dsp = false;
      document.getElementById("ok_button").style.display = (!dsp) ? "none" : "block";
   }, 

   delete: function() {
      d = document.edit_perspective;
      d.delete.value = arguments[0];
      d.custom_id.value = "SYSTEM_1";
      d.r.value = location.href;
      d.submit();
   }
}


add_perspective = function() {
   for(i = 0; i < 6; i++) arguments[i] = (typeof arguments[i] == "undefined") ? false : arguments[i];
   peditor.add(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
}






