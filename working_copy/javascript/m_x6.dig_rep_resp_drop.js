m_x6 = {
   drop: function() {
      area = document.getElementById("m_x6__dig_rep_resp_drop").querySelector("[class='inner_box']");
      area.ondragover = function(ev) {
         ev.preventDefault();
      }
      area.ondrop = function(ev) {
         ev.preventDefault();
         rms = document.getElementById(ev.dataTransfer.getData("text"));
         con = document.createElement("div");
         con.setAttribute("unid", ev.dataTransfer.getData("text"));
         img = document.createElement("img");
         img.src = session.php_server + "/library/images/16x16/edition-43.png";
         img.name = "img_" + rms.getAttribute("do");
         img.style.cursor = "pointer";
         img.onclick = function() {
            rms = document.getElementById(this.parentNode.getAttribute("unid"));
            this.parentNode.parentNode.removeChild(this.parentNode);
            if(typeof m_x5 == "object") m_x5.mark(rms, false);
         }
         con.appendChild(img);
         div = document.createElement("div");
         div.textContent = rms.firstChild.textContent;
         div.setAttribute("save", rms.getAttribute("do"));
         con.appendChild(div);
         document.getElementById("m_x6__dig_rep_resp_drop").querySelector("[class='inner_box']").appendChild(con);
         if(typeof m_x5 == "object") m_x5.mark(rms, true);
      }
      button = area.getElementsByTagName("button")[0];
      button.onclick = function() {
         s = document.getElementById("m_x6__dig_rep_resp_drop").querySelectorAll("[save]");
         if(s.length == 0) {
            infobox("Error", "Nothing selected", 200, 70);
         }
         else {
            arr = [];
            for(k in s) if(typeof s[k] == "object") {
               arr.push(s[k].getAttribute("save"));
            }
            m_x6.save(arr);
         }
      }
   }, 
   save: function(arr) {
      now = "<?php print date("Y-m-d H:i:s", time()); ?>";
      data = "&unid=" + arr[0] + "&fld1=" + escape("dig_rep_date_actual/" + session.domino_user_cn + "@" + now);
      document.getElementsByName("img_" + arr[0])[0].src = session.php_server + "/library/images/16x16/green/vote-36.png";
      arr.shift();
      js = (arr.length > 0) ? "m_x6.save([\"" + arr.join("\",\"") + "\"])" : "window.setTimeout('location.reload()',2000)";
      post.save_data("f.save_data", session.remote_domino_path_epcmain, data, js);  
   }
}
