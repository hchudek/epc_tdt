search = {
   view: function() {
      q = (document.getElementsByName(arguments[0])[0].value.trim()).replace(/ /g, "+");
      do_fade(true);
      q_f = [arguments[0], arguments[0].replace("_q", "_key"), arguments[0].replace("_q", "_sortkey")];
      q_t = [q, "", ""];
      location.href = query_string.generate(q_f, q_t);
   },

   checkevent: function() {
      if(arguments[1].keyCode == 13) search.view(arguments[0].getAttribute("name"));
   },

   clear: function() {
      document.getElementsByName(arguments[0])[0].value = "";
      search.view(arguments[0]);
   },

   lookup: {
      nest: function() {
         d = document.getElementsByName("disp_" + arguments[0] + "_lookup");
         select = document.createElement("select");
         option = document.createElement("option");
         selected = (query_string.get(arguments[0] + "_sortkey")).toLowerCase();
         select.appendChild(option);
         for(k in d) if(!isNaN(k)) {
            option = document.createElement("option");
            option.textContent = d[k].textContent;
            if(d[k].textContent.toLowerCase().replace(/ /g, "+") == selected) option.setAttribute("selected", "selected");
            select.appendChild(option);
         }
         document.getElementById(arguments[0] + "_select").appendChild(select);
      },
      execute: function() {
         select = document.getElementById(arguments[0] + "_select").getElementsByTagName("select");
         value = (select[0][select[0].selectedIndex].text.trim()).replace(/ /g, "+");
         q_f = [arguments[0] + "_q", arguments[0] + "_sortkey", arguments[0] + "_key"];
         q_t = ["", value, (document.getElementsByName(arguments[0] + "_q")[0].value.trim()).replace(/ /g, "+")];
         location.href = query_string.generate(q_f, q_t);
      }
   }
}

query_string = {
   get: function() {
      r = "";
      attr = (location.search.replace("?", "")).split("&");
      for(k in attr) if(!isNaN(k)) {
         if(attr[k].substr(0, attr[k].indexOf("=")).toLowerCase() == arguments[0]) r = attr[k].substr(attr[k].indexOf("=") + 1, attr[k].length);
      }
      return r;
   },

   generate: function() {
      attr = (location.search.replace("?", "")).split("&");
      for(i = 0; i < arguments[0].length; i++) {
         in_qs = false;
         qs = [];
         for(k in attr) if(!isNaN(k) && attr[k].trim() != "") {
            if(attr[k].substr(0, attr[k].indexOf("=")) == arguments[0][i]) {
               if(arguments[1]!= "") qs.push(arguments[0][i] + "=" + arguments[1][i]);
               in_qs = true;
            }
            else qs.push(attr[k]);
         }
         if(!in_qs) qs.push(arguments[0][i] + "=" + arguments[1][i]);
         qs.sort();
         attr = qs;
      }
      return "?&" + attr.join("&");
   }
}


function view_add_nav() {
   // NAVIGATOR
   id = arguments[0] + "_navigator";
   d = document.getElementById(id);
   if(d) {
      d.setAttribute("style", "width:280px;display:none;position:absolute;padding-top:12px;box-shadow: 6px 6px 15px -9px #999999;");
      d.setAttribute("onmouseover", "document.getElementById('" + id + "').setAttribute('dsp', 'true'); view_tdo('" + id + "');");
      d.setAttribute("onmouseout", "document.getElementById('" + id + "').setAttribute('dsp', 'false');window.setTimeout('view_tdo(\\'" + id + "\\')', 10);");
      d.previousElementSibling.setAttribute("style", "background-color:rgb(255,255,255);white-space:nowrap;cursor:pointer;");
      d.previousElementSibling.setAttribute("onmouseover", d.getAttribute("onmouseover"));
      d.previousElementSibling.setAttribute("onmouseout", d.getAttribute("onmouseout"));
   
      for(i = 1; i <= Number(d.getAttribute("nav")); i++) {
         num = (i * Number(d.getAttribute("step")) - Number(d.getAttribute("step")) + 1);
         a = document.createElement("a");
         a.setAttribute("href", "?" + d.getAttribute("attr") + "=" + num + d.getAttribute("query_string"));
         a.setAttribute("onmouseover", "run_ajax('run_ajax', '?" + d.getAttribute("query_string") + "&get_first=true&' + d.getAttribute('attr') + '=" + num + "', 'document.getElementById(\\'" + id + "\\').getElementsByTagName(\\'div\\')[0].innerHTML = document.getElementById(\\'run_ajax\\').innerHTML')");
         a.setAttribute("onmouseout", "document.getElementById('" + id + "').getElementsByTagName('div')[0].textContent=''");
         if(d.getAttribute("val") == String(num)) a.setAttribute("style", "background-color:rgb(99,99,99);color:rgb(255,255,255);");
         a.textContent = String(i);
         d.appendChild(a);
         if((i / 10) == Math.floor(i / 10)) {
            br = document.createElement("br");
            d.appendChild(br);
         }
      }
      f = Number(d.getAttribute("nav")) - Math.floor(Number(d.getAttribute("nav")) / 10) * 10;
      if(f > 0) {
         for(i = f; i <= 9; i++) {
            span = document.createElement("span");
            span.innerHTML = "&nbsp;";
            d.appendChild(span);
         }
      }
      div = document.createElement("div");
      div.setAttribute("class", "prv");
      d.appendChild(div);
   }
   // PAGE DESIGN
   id = arguments[0] + "_pagedesign";
   d = document.getElementById(id);
   if(d) {
      d.setAttribute("style", "width:" + String(d.getAttribute("columns") * 44) + "px;display:none;position:absolute;padding-top:12px;box-shadow: 6px 6px 15px -9px #999999;");
      d.setAttribute("onmouseover", "document.getElementById('" + id + "').setAttribute('dsp', 'true'); view_tdo('" + id + "');");
      d.setAttribute("onmouseout", "document.getElementById('" + id + "').setAttribute('dsp', 'false');window.setTimeout('view_tdo(\\'" + id + "\\')', 10);");
      d.previousElementSibling.setAttribute("style", "background-color:rgb(255,255,255);white-space:nowrap;cursor:pointer;");
      d.previousElementSibling.setAttribute("onmouseover", d.getAttribute("onmouseover"));
      d.previousElementSibling.setAttribute("onmouseout", d.getAttribute("onmouseout"));

      count =(d.getAttribute("count")).split(";");
      columns = Number(d.getAttribute("columns"));
      for(y = 0; y < count.length; y++) {
         for(x = 0; x < columns; x++) {
            a = document.createElement("a");
            a.setAttribute("href", "?" + d.getAttribute("query_string") + "&" + d.getAttribute("attr_columns") + "=" + String(x + 1) + "&" + d.getAttribute("attr_count") + "=" + count[y]);
            a.setAttribute("id", id + "___" + (y + 1) + "_" + (x + 1));
            c = (x < Number(d.getAttribute("val_columns")) && count[y] <=  Number(d.getAttribute("val_count"))) ? "sel" : "reg";
            a.setAttribute("class", c);
            a.setAttribute("baseclass", a.getAttribute("class"));
            a.setAttribute("onmouseover", "handle_pagedesign(this, " + (y + 1) + "," + (x + 1) + ", " + count.length + ", " + columns + ")");
            a.setAttribute("onmouseout", "handle_pagedesign(this, 0, 0, " + count.length + ", " + columns + ")");
            a.innerHTML = count[y] + "/" + (x + 1);
            d.appendChild(a);
         } 
         br = document.createElement("br");
         d.appendChild(br);
      }
   }
   // EXPORT
   id = arguments[0] + "_export";
   d = document.getElementById(id);
   if(d) {
      d.setAttribute("style", "width:160px;display:none;position:absolute;padding-top:12px;box-shadow: 6px 6px 15px -9px #999999;");
      d.setAttribute("onmouseover", "document.getElementById('" + id + "').setAttribute('dsp', 'true'); view_tdo('" + id + "');");
      d.setAttribute("onmouseout", "document.getElementById('" + id + "').setAttribute('dsp', 'false');window.setTimeout('view_tdo(\\'" + id + "\\')', 10);");
      d.previousElementSibling.setAttribute("style", "background-color:rgb(255,255,255);white-space:nowrap;cursor:pointer;");
      d.previousElementSibling.setAttribute("onmouseover", d.getAttribute("onmouseover"));
      d.previousElementSibling.setAttribute("onmouseout", d.getAttribute("onmouseout"));
   }

}


function handle_pagedesign(e, y_pos, x_pos, count, columns) {
   id = e.id.substr(0, e.id.indexOf("___"));
   for(y = 1; y <= count; y++) {
      for(x = 1; x <= columns; x++) {
         d = document.getElementById(id + "___" + String(y) + "_" + String(x));
         if(x_pos > 0 && y_pos >0) {
            c = (y <= y_pos && x <= x_pos) ? "high" : d.getAttribute("baseclass");
         }
         else {
            c = d.getAttribute("baseclass"); 
         }
         d.setAttribute("class", c);
      }
   }
}

function view_tdo(id) {
   d = document.getElementById(id);
   d.style.display = (d.getAttribute("dsp") == "true") ? "block" : "none";
   img = (d.getAttribute("dsp") == "true") ? "triangle-big-1-01.png" : "triangle-big-4-01.png";
   d.previousElementSibling.getElementsByTagName("img")[0].src = session.php_server + "/library/images/16x16/" + img;
}

function get_file(url) {
   do_fade(true);
   js = "do_fade(false); url = (document.getElementById('run_ajax').textContent).trim(); location.href = url;";
   run_ajax("run_ajax", url, js);
}


function handle_lookup() {
   arguments[1]= (unescape(arguments[1]).replace("+"," "));
   name="disp_"+arguments[0]+"_lookup";
   id="selector_lookup_"+arguments[0];
   target=arguments[0]+"_selected_value";
   is_selected=document.getElementsByName(name)[0].textContent;
   html='<select onchange="document.getElementById(\''+target+'\').textContent=this[this.selectedIndex].text;">';
   for(i=0;i<document.getElementsByName(name).length;i++) {
      if(document.getElementsByName(name)[i].textContent==arguments[1]) {
         is_selected=document.getElementsByName(name)[i].textContent;
         selected=" selected";
      }
      else selected="";
      html+='<option'+selected+'>'+document.getElementsByName(name)[i].textContent+'</option>';
   }
   html+='</select><div style="display:none;" id="'+target+'">'+is_selected+'</div>';
   document.getElementById(id).innerHTML=html;
}

function embedStatus(i) {
   if(document.getElementsByName("status")[i]) {
      unid=document.getElementsByName("status")[i].textContent;
      includeAJAX("id_"+unid,"../a.w.showsymbols?open&r="+document.getElementById("supplier").textContent+"&unid="+unid,"embedStatus("+(i+1)+")");      
   }
   else document.getElementById("supplier_flag").textContent="true";
}

function handleNavigationBar(id) {
   dsp=(document.getElementById(id).style.display=="none") ? "block" : "none";
   if(dsp=="block") {
      lid=new Array("filter","dselector","columnselector","navigator","colorizer");
      for(i=0;i<lid.length;i++) if(document.getElementById(lid[i])) document.getElementById(lid[i]).style.display="none";
   }
   document.getElementById(id).style.display=dsp;
}

function handleTbl(dsp) {
   if(dsp==undefined) dsp="none";
   if(document.getElementById("hideline").textContent!="") {
      handle=(document.getElementById("hideline").textContent=="") ? "7,8,9,10,11,12,13,14,15,16,17,18" : document.getElementById("hideline").textContent;
      handle=handle.split(",");
      i=1; while(document.getElementById("v$"+String(i)+"$1")) {
         for(e=0;e<handle.length;e++) {
            document.getElementById("v$"+String(handle[e])).style.display=dsp;
            document.getElementById("v$"+String(i)+"$"+String(handle[e])).style.display=dsp;
         }
         i++;
      }
   }
}

function handleColumn(col) {
   flag=document.getElementsByName("selected_colorizer")[col].textContent;
   img=(flag==0) ? "../../../../library/images/sel_yes.gif" : "../../../../library/images/sel_no.gif";
   newflag=(flag==0) ? "1" : "0";
   document.getElementsByName("img_colorizer")[col].src="../"+img;
   document.getElementsByName("selected_colorizer")[col].textContent=newflag;
}

function embed_filter(t_selected, t_module, t_column) {
   t_id = t_module + "_" + t_column;
   if(document.getElementById(t_id).style.visibility == "visible") handle_visibility(t_id);
   else {
      t_path = document.getElementById(t_module + "_path").textContent;
      t_url = session.remote_database_path + "addin/generate_extended_view.php?path=" + t_path + "&module_id=" + t_module + "&column=" + t_column + "&selected=" + t_selected;
      document.getElementsByTagName("body")[0].style.display = "none";
      run_ajax(t_id, t_url, "document.getElementsByTagName('body')[0].style.display = 'block';handle_visibility('" + t_id + "')");
      filters = (document.getElementsByName(t_module + "_ext_filter").length);
      for(i = 0; i < filters; i++) {
         r_id = document.getElementsByName(t_module + "_ext_filter")[i].r_id;
         if(r_id != t_id) document.getElementById(r_id).style.visibility = "hidden";
      }
   }
}

function handle_visibility(t_id) {
   dsp = (document.getElementById(t_id).style.visibility == "visible") ? "hidden" : "visible";
   document.getElementById(t_id).style.visibility = dsp;

}

function handle_selector(e) {
   dsp = (e.src.indexOf("chkbox_reg") > 0) ? "chkbox_high" : "chkbox_reg";
   src = session.php_server + "/library/images/icons/" + dsp + ".png";
   e.src = src;
}


function apply_extended_filter(t_module, t_column, t_column_b64_complete) {
   t_res = new Array;
   t_id = t_module + "_selector_" + t_column;
   l = document.getElementsByName(t_id).length;
   for(i = 0; i < l; i++) {
      t_val = document.getElementsByName(t_id)[i].src;
      if(t_val.indexOf("chkbox_reg") == -1) t_res.push(document.getElementsByName(t_id)[i].getAttribute("value"));
   }
   if(t_res.length == 0) alert("Nothing selected");
   else {
      query_string = "?" + (document.getElementById(t_module + "_url_" + t_column).textContent);
      if(t_res.length < l) location.href = session.php_server + location.pathname + query_string + "&" + t_module + "_ef_" + t_column + "=" + t_column_b64_complete + "$$" + base64_encode(t_res.join("$$"));
      else location.href = session.php_server + location.pathname + query_string;
   }
}

function do_ext_filter() {
   dsp = (arguments[0] == true) ? "chkbox_high" : "chkbox_reg";
   t_sel = document.getElementsByName(arguments[1]).length;
   for(i = 0; i < t_sel; i++) document.getElementsByName(arguments[1])[i].src = session.php_server + "/library/images/icons/" + dsp + ".png";
}

function select_field(e) {
   dsp = (e.src.indexOf("status-5-1") == -1) ? "status-5-1" : "status-5";
   e.src = (session.php_server + "/library/images/16x16/" + dsp + ".png");
}


function select_all(t_name, t_do) {
   count = document.getElementsByName(t_name).length;
   dsp = (t_do == true) ? "status-5" : "status-5-1";
   for(i = 0; i < count; i++) {
      document.getElementsByName(t_name)[i].src = (session.php_server + "/library/images/16x16/" + dsp + ".png");
   }
}


submit_selected_fields = function(module_id) {
   view = (typeof arguments[1] == "undefined") ? "" : arguments[1];
   count = document.getElementsByName("selected_field").length;
   selected_fields = [];
   for(i = 0; i < count; i++) {
      if(document.getElementsByName("selected_field")[i].src.indexOf("status-5-1") == -1) selected_fields.push(String(i + 1));
   }
   if(selected_fields.length == 0) {
      infobox("Error", "Nothing selected.", 200, 70);
   }
   else {
      unique = session.domino_user.substr(3, session.domino_user.indexOf("/") - 3) + "@" + session.page_id + "@" + String(new Date().getTime());
      url = session.remote_domino_path_main + "/a.inject_get_active_pot?open&view=" + view + "&load=" + selected_fields.join(",") + "&module=" + module_id + "&unique=" + escape(unique); 
      add = "&" + module_id + "_fields=" + selected_fields.join(",");
      if(document.getElementById("g_df")) {
         if(document.getElementById("g_df").textContent != "") add += "&" + module_id + "_df=" + document.getElementById("g_df").textContent;
      }
      if(document.getElementById("g_dt")) {
         if(document.getElementById("g_dt").textContent != "") add += "&" + module_id + "_dt=" + document.getElementById("g_dt").textContent;
      }
      infobox("Calculating data", "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"vertical-align:top;\"><span style=\"position:relative;top:-2px;left:7px;\">Processing ...</span>", 232,70);      
      iframe = document.createElement("iframe");
      iframe.setAttribute("style", "display:none;");
      iframe.setAttribute("src", url);
      iframe.setAttribute("onload", "location.href = '?&" + module_id + "_add=" + escape(unique) + add + "'");
      document.getElementsByTagName("body")[0].appendChild(iframe);
   }
}









