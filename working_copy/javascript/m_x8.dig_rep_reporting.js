m_x8 = {
   min: 99999,
   max: 0,
   kill: false,
   nest: function() {
      labels = [];
      data = [];
      e = document.getElementById("m_x8__dig_rep_reporting_tbl");
      json = JSON.parse(e.textContent);
      table = document.createElement("table");
      table.id = "m_x8__datasheet";
      table.className = "m_x8__dig_rep_reporting_tbl";
      tr = document.createElement("tr");
      table.appendChild(tr);
      thead = ["RDO", "Sum Days", "CR's", "Average", "Min", "Max"];
      type = document.getElementsByName("m_x8__dig_rep_reporting_cockpit_type");
      if(type) {
         if(type[0].value == "1") thead[0] = "Date";
      }
      for(k in thead) {
         if(k != 1) {
            th = document.createElement("th");
            th.textContent = thead[k];
            tr.appendChild(th);
         }
      }
      for(trow in json) {
         tr = document.createElement("tr");
         table.appendChild(tr);
         for(tdata in json[trow]) {
            if(tdata != 1) {
               td = document.createElement("td");
               td.textContent = (Number(tdata) == 3) ? String(Number(json[trow][tdata]).toFixed(2)).replace(".", ",") : String(json[trow][tdata]).replace(".", ",");
               td.textContent;
               tr.appendChild(td);
            }
         }
         data.push([Number(json[trow][3]).toFixed(2), Number(json[trow][4]).toFixed(2), Number(json[trow][5]).toFixed(2)]);
         labels.push(json[trow][0]);
         if(m_x8.min > Number(json[trow][3])) m_x8.min = Number(json[trow][3]);
         if(m_x8.max < Number(json[trow][3])) m_x8.max = Number(json[trow][3]);
      }
      xls = "<div>&nbsp;<img src=\"../../../../library/images/excel-icon.png\" style=\"position:relative; top:3px;\">&nbsp;Get Excel</div>";
      eval("m_x8.categories = " + e.getAttribute("categories"));
      select = document.createElement("select");
      select.id = "m_x8-selector";
      option = document.createElement("option");
      select.appendChild(option);
      select.onchange = function() {
         location.href = "?get=" + this[this.selectedIndex].value;
      }
      if(type[0].value == "1") {
         m_x8.categories = [""];
         rdo = e.getAttribute("rdo").split(",");
         for(k in rdo) m_x8.categories.push(rdo[k]);
         txt = "&nbsp;|&nbsp;Select RDO";
      }
      else {
         txt = "&nbsp;|&nbsp;Select date";
      }
      for(k in m_x8.categories) {
         option = document.createElement("option");
         t = m_x8.categories[k].split(":");
         if(typeof t[1] != "undefined" && t[1].length == 7 || document.getElementsByName("m_x8__dig_rep_reporting_cockpit_type")[0].value != "0") {
            option.text = (typeof t[1] == "undefined") ? m_x8.categories[k] : t[1] + " (" + t[0].toUpperCase() + ")";
            option.setAttribute("type", t[0]);
            option.value = m_x8.categories[k];
            if(e.getAttribute("selected") == m_x8.categories[k]) option.setAttribute("selected", "");
            select.appendChild(option);
         }
      }
      span = document.createElement("span");
      span.innerHTML = txt;
      with(document.getElementById("m_x8__dig_rep_reporting").getElementsByTagName("header")[0]) {
         appendChild(span);
         appendChild(select);
      }
      m_x8.chkmdp();

      target = document.getElementById("m_x8__dig_rep_reporting_datasheet");
      if(target && json != null) target.getElementsByTagName("p")[0].outerHTML = table.outerHTML + "<span style=\"color:rgb(0,102,158); cursor:pointer; padding:7px; display:inline-block;\" onclick=\"selectElementContents(document.getElementById('m_x8__datasheet'))\">Select Table</span>";

      width = ($(document.getElementById("m_x8__dig_rep_reporting")).width());
      document.getElementById("canvas").style.width = width + "px";

      m_x8.chart(data, labels);
      if(document.getElementById("m_x8__dig_rep_reporting_fulltable")) m_x8.fulltable();
   },

   chkmdp: function() {
      if(document.getElementsByName("m_x8__dig_rep_reporting_cockpit_type")[0].value != "0") return false;
      select = document.getElementById("m_x8-selector");
      mdp = ((document.getElementsByName("m_x8__dig_rep_reporting_cockpit_mdp")[0].value).trim() == "0") ? "none-mdp" : "mdp";
      option = select.getElementsByTagName("option");
      for(i = 0; i < option.length; i++) {
         option[i].style.display = (mdp == option[i].getAttribute("type")) ? "block" : "none";
      }
   },

   fulltable: function() {
      d = document.getElementById("m_x8__dig_rep_reporting_fulltable").getElementsByTagName("p")[0];
      json = JSON.parse(document.getElementById("m_x8__dig_rep_reporting_full").textContent);
      table = document.createElement("table");
      table.id = "m_x8__full";
      for(k in json) {
         tr = document.createElement("tr");
         table.appendChild(tr);
         for(e in json[k]) {
            td = (Number(k) == 0) ? document.createElement("th") : document.createElement("td");
            td.textContent = json[k][e];
            tr.appendChild(td);
         }
      }
      if(Number(k) > 0) d.outerHTML = table.outerHTML + "<span style=\"color:rgb(0,102,158); cursor:pointer; padding:7px; display:inline-block;\" onclick=\"selectElementContents(document.getElementById('m_x8__full'))\">Select Table</span>";
   },

   chart: function() {
      Chart.defaults.global.legend.display = false;
      color = Chart.helpers.color;
      ln = document.getElementsByName("m_x8__dig_rep_reporting_cockpit_target")[0].value;
      adjust = document.getElementsByName("m_x8__dig_rep_reporting_cockpit_adjust")[0].value;
      average = [];
      min = [];
      max = [];
      target = [];
      backgroundColor = [];
      borderColor = [];
      for(k in arguments[0]) {
         v = (adjust == "1") ? arguments[0][k][0]  - Number(ln) : arguments[0][k][0]; average.push(v);
         if(ln != "0") {
            bg = (Number(arguments[0][k][0]) < Number(ln)) ? "rgba(0,255,0,0.5)" : "rgba(255,0,0,0.5)";
            backgroundColor.push(bg);
            bg = (Number(arguments[0][k][0]) < Number(ln)) ? "rgba(0,255,0,0.8)" : "rgba(255,0,0,0.8)";
            borderColor.push(bg);
         }
         else {
            backgroundColor = window.chartColors.blue;
            borderColor = window.chartColors.blue;
         }
         v = (adjust == "1") ? arguments[0][k][1]  - Number(ln) : arguments[0][k][1]; min.push(v);
         v = (adjust == "1") ? arguments[0][k][2]  - Number(ln) : arguments[0][k][2]; max.push(v);
         target.push(ln);
      }
      barChartData = {
         labels: arguments[1],
         datasets: [],
      };
      
      if(document.getElementsByName("m_x8__dig_rep_reporting_cockpit_average")[0].value == "1") {
         barChartData.datasets.push({
            label: "",
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            borderWidth: 2,
            data: average
         });
      }

      if(document.getElementsByName("m_x8__dig_rep_reporting_cockpit_min")[0].value == "1") {
         barChartData.datasets.push({
            label: "",
            type: "bar",
            radius: 0,
            backgroundColor: "rgba(122,122,142,0.6)",
            borderColor: "rgba(122,122,222,0.0)",
            borderWidth: 2,
            data: min
         });
      }

      if(document.getElementsByName("m_x8__dig_rep_reporting_cockpit_max")[0].value == "1") {
         barChartData.datasets.push({
            label: "",
            type: "bar",
            radius: 0,
            backgroundColor:"rgba(122,122,162,0.4)",
            borderColor: "rgba(122,122,122,0.0)",
            borderWidth: 2,
            data: max
         });
      }

      if(ln != "0" && adjust != "1") {
         barChartData.datasets.push({
            label: "",
            type: "line",
            radius: 0,
            backgroundColor:"rgba(0,0,0,0.0)",
            borderColor:"rgba(189,0,0,0.4)",
            borderWidth: 2,
            data: target
         });
      }

      (function() {
         ctx = document.getElementById("canvas").getContext("2d");
         window.myBar = new Chart(ctx, {
            type: "bar",
            data: barChartData,
            options: {

               responsive: true,
               legend: {
                  position: 'top',
               },
            }
         });

      })();
   },

   perpage: function() {
      document.getElementsByName(arguments[0].getAttribute("perpage"))[0].value = arguments[1];
      save_perpage();
   },

   switch() {
      e = document.querySelectorAll("[perpage='" + arguments[0].getAttribute("perpage") + "']");
      for(k in e) {
         e[k].src = (e[k] == arguments[0]) ? session.php_server + "/library/images/16x16/status-11.png" : session.php_server + "/library/images/16x16/status-11-1.png";
      }
   }


}



function selectElementContents(el) {
   var body = document.body, range, sel;
   if (document.createRange && window.getSelection) {
   range = document.createRange();
   sel = window.getSelection();
   sel.removeAllRanges();
   try {
      range.selectNodeContents(el);
      sel.addRange(range);
      } catch (e) {
         range.selectNode(el);
         sel.addRange(range);
      }
   } 
   else if (body.createTextRange) {
      range = body.createTextRange();
      range.moveToElementText(el);
      range.select();
      range.execCommand("Copy");
   }
}