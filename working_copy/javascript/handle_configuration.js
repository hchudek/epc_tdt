function set_workingday(t_day, t_id) {
   t_wd = (document.getElementsByName("workingdays")[0].value).substr(t_day - 1, 1);
   t_val = (t_wd == "0") ? "1" : "0";
   n_val = (document.getElementsByName("workingdays")[0].value).substr(0, t_day - 1);
   n_val += t_val;
   n_val += (document.getElementsByName("workingdays")[0].value).substr(t_day, document.getElementsByName("workingdays")[0].value,length);
   document.getElementsByName("workingdays")[0].value = n_val;
   use_class = (t_val == "1") ? "is_workingday" : "no_workingday_individual";
   document.getElementById(t_id).className = use_class;
   document.getElementById("save_changes").style.display = "inline";
}

function save_changes() {
   t_url = (session.remote_domino_path_perportal + "/a.set_field?open&unid=" + session.remote_perportal_unid + "&field=td$wd_" + document.getElementById("calendar_year").innerText + "&value=" + document.getElementsByName("workingdays")[0].value);
   t_js = "location.reload()";
   run_ajax("run_ajax", t_url, t_js);
}


function save_perportal_field() {
   t_url = (session.remote_domino_path_perportal + "/a.set_field?open&unid=" + session.remote_perportal_unid + "&field=td$" + arguments[0] + "&value=" + arguments[1]);
   t_js = "location.reload()";
   run_ajax("run_ajax", t_url, t_js);
}

