prepare = {

   color: {
      pantone: [
         "rgba(246,152,82,A)",
         "rgba(173,94,97,A)",
         "rgba(0,185,206,A)",
         "rgba(0,98,161,A)",
         "rgba(244,140,147,A)",
         "rgba(244,230,142,A)",
         "rgba(202,212,214,A)",
         "rgba(160,206,222,A)",
         "rgba(131,205,188,A)",
         "rgba(218,188,164,A)",
      ],
      material: [
         "rgba(62,80,180,A)",
         "rgba(254,151,0,A)",
         "rgba(0,149,135,A)",
         "rgba(254,86,33,A)",
         "rgba(157,157,157,A)",
         "rgba(95,124,138,A)",
         "rgba(155,38,175,A)",
         "rgba(54,63,69,A)",
         "rgba(0,187,211,A)",
         "rgba(254,234,58,A)",
      ],
   },

   my_chart: [],
   my_data: [],
   k: 0,

   av_datasets: [],

   dataset: {
      merge: [
         {name: "Customer Engineering GE", combine: ["RDO 0736", "RDO 0737", "RDO 0738", "RDO 0739", "RDO 0740", "RDO 0742", "RDO 0852"], calculate: "average"},
         {name: "Customer Engineering India", combine: ["RDO 0243", "RDO 0751", "RDO 0973", "RDO 0983"], calculate: "average"},
         {name: "Customer Engineering France", combine: ["RDO 0743", "RDO 0744", "RDO 0745", "RDO 0746"], calculate: "average"},
      ],
   },

   use_dataset: function() {
      prepare.usedata = prepare.dataset[arguments[0]];
      for(k in prepare.dataset.merge) {
         mergedata = [];
         labels = [];
         for(e in prepare.dataset.merge[k].combine) {
            for(t in prepare.usedata) {
               if(prepare.usedata[t].name == prepare.dataset.merge[k].combine[e]) {
                  mergedata.push(prepare.usedata[t].data);
                  if(labels.length == 0) labels = prepare.usedata[t].labels;
               }
            }
         }
         data = [];

         if(mergedata.length > 0) {
            for(m = 0; m < mergedata[0].length; m++) {
               data[m] = 0;
               for(arr in mergedata) {
                  data[m] += mergedata[arr][m];
               }
               if(prepare.dataset.merge[k].calculate == "average") data[m] = data[m] / mergedata.length;
            }
            prepare.usedata.push({name: prepare.dataset.merge[k].name, labels: labels, data: data});
         }
      }
   },

   nest: function(obj) {
      document.querySelector("[anchor='charts']").style.display = "block";
      window.scrollTo(0, 0);
      table = document.createElement("table");
      table.setAttribute("style", "width:" + document.getElementsByName("use_size")[0].value + "px;");
      with(document.querySelector("[anchor='charts']")) {
         innerHTML = "";
         appendChild(table);
      }
      for(prepare.k in obj) {
         type = (obj[prepare.k].type.substr(0, 1).toUpperCase() + obj[prepare.k].type.substr(1, obj[prepare.k].type.length).toLowerCase()).trim();
         opacity = (obj[prepare.k].type == "bar") ? "0.68" : "0.8";
         tr = document.createElement("tr");
         table.appendChild(tr);
         th = document.createElement("th");
         hline = [];
         for(_k in obj[prepare.k].dataset) hline.push(prepare.usedata[obj[prepare.k].dataset[_k]].name);
         th.innerHTML = "<img onclick=\"prepare.get_excel(" + prepare.k + ");\" src=\"" + session.php_server + "/library/addin/chartjs/" + obj[prepare.k].type + ".png\" style=\"cursor:pointer; opacity:" + opacity + "; margin-right:20px; position:relative; top:1px; left:7px;\">" + hline.join(", ");
         tr.appendChild(th);
         tr = document.createElement("tr");
         table.appendChild(tr);
         td = document.createElement("td");
         td.setAttribute("class", "td");
         tr.appendChild(td);
         div = document.createElement("div");
         div.setAttribute("form", "legend:" + String(prepare.k));
         td.appendChild(div);
         canvas = document.createElement("canvas");
         canvas.setAttribute("anchor", "chart:" + String(prepare.k));
         td.appendChild(canvas);
         prepare.my_data[prepare.k] = new prepare.chart_data({regression: obj[prepare.k].regression, chart: obj[prepare.k].dataset, type: obj[prepare.k].type,});
         if(type.toLowerCase() == "pie" || type.toLowerCase() == "doughnut" || type.toLowerCase() == "polararea") {
            prepare.my_chart[prepare.k] = new Chart(canvas.getContext("2d"))[type.replace(/area/, "Area")](prepare.my_data[prepare.k].datasets);
         }
         else {
            prepare.my_chart[prepare.k] = new Chart(
               canvas.getContext("2d"))[type](prepare.my_data[prepare.k], 
               {responsive : true, pointDot : true, legendTemplate : "<% for(i=0; i<datasets.length; i++){%>prepare.legend.push([\"<%if(datasets[i].label){%><%=datasets[i].label%><%}%>\", \"<%=datasets[i].strokeColor%>\"]);<%}%>"}
            );
            tr = document.createElement("tr");
            table.appendChild(tr);
            legend = document.createElement("td");
            tr.appendChild(legend);
            legend.appendChild(prepare.add_legend(prepare.k));
            legend.innerHTML += "<div>&nbsp;</div>";
         }
      }

   },

   get_excel: function(k) {
      html = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"3\"><tr><td>X</td>";
      for(i = 0; i < prepare.my_data[k].datasets.length; i++) html += "<td><nowrap>" + prepare.my_data[k].datasets[i].label + "</nowrap></td>";
      //for(i in prepare.my_data[k].datasets[0]) alert(i);
      for(e = 0; e < prepare.my_data[k].labels.length; e++) {
         html += "<tr><td>" + prepare.my_data[k].labels[e] + "</td>";
         for(i = 0; i < prepare.my_data[k].datasets.length; i++) {
            html += "<td style=\"text-align:center; background:rgb(222,222,222);\">" + prepare.my_data[k].datasets[i].data[e] + "</td>"; 
         }
         html += "<tr>";
      }
      html += "<table>";
      infobox("Table data", html, 1100, 700);
   },

   add_legend: function() {
      prepare.legend = [];
      eval(prepare.my_chart[prepare.k].generateLegend());
      container = document.createElement("div");
      container.setAttribute("style", "margin-left:33px;");
      div =[];
      for(k in prepare.legend) {
         div[k] = document.createElement("div");
         div[k].setAttribute("style", "white-space:nowrap; margin-left:5px;");
         div[k].innerHTML = "<span style=\"position:relative; top:3px; background:" + prepare.legend[k][1] + "; width:12px; display:inline-block; height:15px; margin-left:6px; margin-right:8px;\">&nbsp;</span><span style=\"font:normal 11px open sans\">" + prepare.legend[k][0] + "</span>";
      }
      for(k in div) container.appendChild(div[k]);
      return container;
   },
 
   chart_data: function() {
      this.labels = prepare.usedata[arguments[0].chart[0]].labels; 
      this.datasets = [];
      fillColorT = (arguments[0].type == "bar") ? "0.8" : "0.0";
      theme = 0;
      regr = 1.0;
      for(_k in arguments[0].chart) {
         if(arguments[0].type == "line" || arguments[0].type == "bar"  || arguments[0].type == "radar") { 
           this.datasets.push({
               label: prepare.usedata[arguments[0].chart[_k]].name,
               fillColor: prepare.color[prepare.my_color][theme].replace(/A/, fillColorT),
               strokeColor: prepare.color[prepare.my_color][theme].replace(/A/, "0.99"),
               pointColor: prepare.color[prepare.my_color][theme].replace(/A/, "0.99"),
               pointStrokeColor: "rgba(255,255,255,1.0)",
               pointHighlightFill: prepare.color[prepare.my_color][theme].replace(/A/, "0.99"),
               pointHighlightStroke: "rgba(220,220,220,1.0)",
               highlightFill: prepare.color[prepare.my_color][theme].replace(/A/, "0.4"),
               highlightStroke: prepare.color[prepare.my_color][theme].replace(/A/, "0.6"),
               data: prepare.usedata[arguments[0].chart[_k]].data,
            });
            theme++; if(theme >= 10) theme = 1;
            if(arguments[0].regression) {
               prepare.data = [];
               for(k in prepare.usedata[arguments[0].chart[_k]].data) prepare.data.push([prepare.usedata[arguments[0].chart[_k]].data[k], (Number(k))]);
               regression_equation = prepare.get_regression_equation();
               this.datasets.push({
                  label: "Regression&nbsp;&nbsp;b<span style='vertical-align:sub;'>1</span> = " + regression_equation[1].toFixed(3) + 
                         "&nbsp;&nbsp;b<span style='vertical-align:sub;'>2</span> = " + regression_equation[0].toFixed(3) + 
                         "&nbsp;&nbsp;R<span style='vertical-align:super;'>2</span> = " + regression_equation[2].toFixed(3) +
                         "&nbsp;&nbsp;s = " + regression_equation[6].toFixed(3) +
                         "&nbsp;&nbsp;F<span style='vertical-align:sub;'>e</span> = " + regression_equation[3].toFixed(3) + 
                         "&nbsp;&nbsp;T<span style='vertical-align:sub;'>e</span> = " + regression_equation[5].toFixed(3),
                  fillColor: "transparent",
                  strokeColor: "rgba(232,29,98," + regr + ")",
                  pointColor: "transparent",
                  pointStrokeColor: "transparent",
                  pointHighlightFill: "rgba(232,29,98," + regr + ")",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  highlightFill: "rgba(220,220,220,0.75)",
                  highlightStroke: "rgba(220,220,220,1)",
                  data: [],
               });
               for(k in prepare.data) {
                  this.datasets[this.datasets.length - 1].data.push(regression_equation[1] + prepare.data[k][1] * regression_equation[0]);
               }    
               regr -= 0.2; if(regr <= 0.2) regr = 1.0;        
            }
         }
         else {
            theme = 0;
            for(_k in prepare.usedata[arguments[0].chart[0]].data) if(!isNaN(_k)) {
               this.datasets.push({
                  value: prepare.usedata[arguments[0].chart[0]].data[_k],
                  label: prepare.usedata[arguments[0].chart[0]].labels[_k],
                  color: prepare.color[prepare.my_color][theme].replace(/A/, "1.0"),
                  highlight: prepare.color[prepare.my_color][theme].replace(/A/, "0.6"),
               });
               theme++; if(theme >= 10) theme = 1;
            }
         }
      }
   },

    get_regression_equation: function() {
      f_table_expl_1 = [161.448, 18.513, 10.128, 7.709, 6.608, 5.987, 5.591, 5.318, 5.117, 4.965, 4.844, 4.747, 4.667, 4.600, 4.543, 4.494, 4.451, 4.414, 4.381];
      b = [];
      x_sum = 0;
      y_sum = 0;
      xy_sum = 0;
      x_square_sum = 0;
      K = prepare.data.length;
      for(k in prepare.data) {
         x_sum += prepare.data[k][1];
         y_sum += prepare.data[k][0];
         xy_sum += prepare.data[k][0] * prepare.data[k][1];
         x_square_sum += Math.pow(prepare.data[k][1], 2);
      }
      x_av = x_sum / K;
      y_av = y_sum / K;
      b[0] = ((K * xy_sum) - (x_sum * y_sum)) / (K * x_square_sum - Math.pow(x_sum, 2));
      b[1] = y_av - b[0] * x_av;
      res = [];
      explained = [];
      v_squared_sum = 0;
      explained_squared_sum = 0;
      res_squared_sum = 0;
      x_x_av_squared_sum = 0;
      for(k in prepare.data) {
         res[k] = prepare.data[k][0] - (b[1] + prepare.data[k][1] * b[0]);
         res_squared_sum += Math.pow(res[k], 2);
         explained[k] = (prepare.data[k][0] - y_av) - res[k];
         v_squared_sum += Math.pow(res[k] + explained[k], 2);
         explained_squared_sum += Math.pow(explained[k], 2);
         x_x_av_squared_sum += Math.pow(prepare.data[k][1] - x_av, 2);
      }
      sd = Math.pow(res_squared_sum / (prepare.data.length - 2), 0.5);
      b[2] = explained_squared_sum / v_squared_sum;			// R^2
      b[3] = b[2] / (1 - b[2]) * (prepare.data.length - 2);		// F-Test
      b[4] = f_table_expl_1[prepare.data.length - 3];			// F-Theoretical
      b[5] = b[0] / (sd * Math.pow(1 / x_x_av_squared_sum, 0.5));	// T-Test
      b[6] = sd;							// SE
      return b;
   },

   selector: {
      nest: function() {
         select = document.querySelector("[name='use_dataset']");
         select.onchange = function() {
            document.querySelector("[anchor='menu']").getElementsByTagName("tbody")[0].style.display = (this.selectedIndex >  0) ? "table-row-group" : "none";
            with(document.querySelector("[anchor='charts']")) {innerHTML = ""; style.display = "none";}
            if(this.selectedIndex >  0) {
               prepare.use_dataset(this[this.selectedIndex].value);
            }               
            prepare.selector.menu(this.selectedIndex);
         }
         for(k in prepare.av_datasets) {
            option = document.createElement("option");
            option.setAttribute("value", prepare.av_datasets[k].id);
            option.textContent = prepare.av_datasets[k].name;
            select.appendChild(option);
         }
      },
  
      menu: function() {
         dragbox = document.querySelector("[anchor='dragbox']");
         dragbox.innerHTML = "<c>Datasets:</c>";
         if(arguments[0] > 0) {
            for(k in prepare.usedata) {
               span = document.createElement("span");
               span.draggable = true;
               span.setAttribute("prepare_data", k);
               span.ondragstart = function(event) {event.dataTransfer.setData("text", this.getAttribute("prepare_data"));}
               span.textContent = prepare.usedata[k].name;
               dragbox.appendChild(span);
            }
         }
         prepare.selector.add_dropbox(false, arguments[0]);
      },

      add_dropbox: function() {
         dropbox = document.querySelector("[anchor='dropbox']");
         if(!arguments[0] || arguments[1] == 0) dropbox.innerHTML = "<c>Charts:</c><a style=\"position:relative; left:-16px;\" href=\"javascript:prepare.selector.add_dropbox(true, true);\">Add new chart</a>";
         if(arguments[1] > 0) {
            select = document.createElement("select");
            select.innerHTML = 
            "<option value=\"line:false\">Line</option>" +
            "<option value=\"line:true\">Line (with regression)</option>" +
            "<option value=\"bar:false\">Bar</option>" +
            "<option value=\"radar:false\">Radar</option>" + 
            "<option value=\"pie:false\">Pie (only one dataset)</option>" +
            "<option value=\"doughnut:false\">Doughnut (only one dataset)</option>" + 
            "<option value=\"polararea:false\">PolarArea (only one dataset)</option>";
            select.onchange = function() {
               if(this[this.selectedIndex].text.indexOf("only one dataset") != -1) {
                  p = this.nextSibling.getElementsByTagName("p");
                  for(k = p.length - 1; k > 0; k--) p[k].parentNode.removeChild(p[k]);
               }
            }
            dropbox.insertBefore(select, dropbox.getElementsByTagName("a")[0]);
            div = document.createElement("div");
            div.ondragover = function(event) {event.preventDefault();}
            div.ondrop = function(event) {
               event.preventDefault();
               if(this.previousSibling[this.previousSibling.selectedIndex].text.indexOf("only one dataset") != -1) {
                  if(this.getElementsByTagName("p").length > 0) return false;
               }
               k = document.querySelector("[prepare_data='" + event.dataTransfer.getData("text") + "']");
               d = this.getElementsByTagName("p");
               for(e in d) if(!isNaN(e)) if(d[e].getAttribute("data") == event.dataTransfer.getData("text")) return false;
               p = document.createElement("p");
               p.setAttribute("data", event.dataTransfer.getData("text"));
               p.innerHTML = "<img src=\"data:image/png;base64,<?php $img = file_get_contents("../../../../library/images/16x16/edition-43.png"); print base64_encode($img); ?>\" style=\"cursor:pointer; margin-right:4px; position:relative; top:2px;\" onclick=\"this.parentNode.parentNode.removeChild(this.parentNode);\">" + k.textContent;
               this.appendChild(p);

            }
            dropbox.insertBefore(div, dropbox.getElementsByTagName("a")[0]);
            img = document.createElement("img");
            img.setAttribute("src", "data:image/png;base64,<?php $img = file_get_contents("../../../../library/images/16x16/interface-88.png"); print base64_encode($img); ?>"); 
            if(dropbox.getElementsByTagName("div").length > 1) {
               img.onclick = function() {
                  this.previousSibling.previousSibling.parentNode.removeChild(this.previousSibling.previousSibling);
                  this.previousSibling.parentNode.removeChild(this.previousSibling);
                  this.parentNode.removeChild(this);
               }
               img.onmouseover = function() {this.style.opacity = "0.8";}
               img.onmouseout = function() {this.style.opacity = "0.0";}
               img.setAttribute("style", "position:relative; top:-15px; left:292px; cursor:pointer; opacity:0.0;");
            }
            else img.setAttribute("style", "visibility:hidden;");
            dropbox.insertBefore(img, dropbox.getElementsByTagName("a")[0]);
         }
      },
      execute: function() {
         use_style = document.getElementsByName("use_style")[0];
         prepare.my_color = use_style[use_style.selectedIndex].value;
         dropboxes = document.querySelector("[anchor='dropbox']").getElementsByTagName("div");
         nest = [];
         for(k in dropboxes) if(!isNaN(k)) {
            attr = dropboxes[k].previousSibling.value.split(":");
            type = attr[0];
            regression = (attr[1] == "true") ? true : false;
            p = dropboxes[k].getElementsByTagName("p");
            dataset = [];
            for(e in p) if(!isNaN(e)) {
               dataset.push(p[e].getAttribute("data"));
            }
            nest.push({type: type, dataset: dataset, regression: regression});
         }         
         if(nest.length > 0) prepare.nest(nest);
      },
   },
}

prepare.nest_data = function(e) {
   c = e.childNodes;
   for(k in c) if(!isNaN(k) && typeof c[k].tagName != "undefined") {
      if(c[k].tagName.toLowerCase() == "av_datasets") {
         _n = c[k].getElementsByTagName("name")[0].textContent;
         _i = c[k].getElementsByTagName("id")[0].textContent;
         if(_n != "" && _i != "") prepare[c[k].tagName.toLowerCase()].push({name: _n, id: _i});    
         dataset = c[k].getElementsByTagName("dataset");
         if(dataset.length > 0) {
            prepare.dataset[_i] = [];
            for(e in dataset) if(!isNaN(e)) eval("prepare.dataset[\"" + _i + "\"].push(" + base64_decode(dataset[e].textContent) + ")"); 
         } 
      }
   }
}

prepare.load = function() {
   prepare.nest_data(document.querySelector("[form='m_x4.rdo_report:data']"));
   Chart.defaults.global.responsive = true;
   Chart.defaults.global.scaleFontFamily = "open sans";
   Chart.defaults.global.animationSteps = 30;
   Chart.defaults.global.scaleFontSize = 11;
   Chart.defaults.global.animationEasing = "easeInQuint";
   Chart.defaults.global.tooltipFillColor = "rgba(0,0,0,0.8)";
   Chart.defaults.global.tooltipFontFamily = "open sans";
   Chart.defaults.global.tooltipFontSize = 12;
   Chart.defaults.global.tooltipFontStyle = "normal";
   Chart.defaults.global.tooltipCornerRadius = 3;
   Chart.defaults.global.tooltipTitleFontSize = 12;
   Chart.defaults.global.tooltipTitleFontStyle = "normal";
   Chart.defaults.global.scaleLineColor = "rgba(0,0,0,0.1)";
   Chart.defaults.global.scaleLineWidth = 1;
   Chart.defaults.global.scaleFontColor = "rgba(66,66,66,1.0)";
   Chart.defaults.global.multiTooltipTemplate = " <%= (Math.round(value * 1000) / 1000) %>       ";
   prepare.selector.nest();
}
