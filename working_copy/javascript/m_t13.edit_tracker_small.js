<?php
$path_translated =  str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
print "include_m_t13__edit_tracker_small = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/m_t13.edit_tracker_small/")."\");}\r\n";
?>



edit_tracker_small = {
   save_tpm: function() {
      do_fade(true);
      data = "unid=" + arguments[1] + "&fld1=responsible/" + arguments[0];
      js = "indicate_save('T&PM saved'); location.reload()";
      post.save_data("f.save_data", "", data, js);   
   },

   save_descr: function() {
      d = document.getElementById(arguments[0]);
      if(new Date().getTime() - edit_tracker_small.clock > 2000 && edit_tracker_small.clock > 0) {
         edit_tracker_small.clock = 0;
         data = "unid=" + arguments[1] + "&fld1=tracker_descr/" + escape(d.textContent);
         js = "indicate_save('Tracker description')";
         do_fade(true, true);
         window.setTimeout("do_fade(false, false)", 700);
         post.save_data("f.save_data", "", data, js);
      }
   },

   set_clock: function() {
      edit_tracker_small.clock = new Date().getTime();
   }

}

m_t13 = {

   <?php
      session_start();
      $file = str_replace("\\", "/", __FILE__);
      $path = substr($file, 0, strpos($file, "javascript"));
      $load = array("tracker");
      require_once($path."addin/load_tracker_data.php");
      $tc_data = create_tc_data($load);
      foreach($tc_data["tracker"]["editors"]["name"] as $editor) {
         $tmp .= "\"".rawurldecode($editor)."\",";
      }
      print "editors: [".substr($tmp, 0, strlen($tmp) - 1)."],\r\n";
   ?>
   nest_editors: function() {
      link = document.createElement("link");
      link.setAttribute("rel", "stylesheet");
      link.setAttribute("type", "text/css");
      link.setAttribute("href", "../../../../library/css5/te.tools/jquery-ui-ssl.css");
      head.appendChild(link);
      $("input[name='m_t13__edit_tracker_small']").autocomplete({source: m_t13.editors, select: function(event, ui) {m_t13.save(document.getElementsByName(this.name)[0], ui)}});  
   },
   save: function(el, ui) {
      v = (typeof ui == "boolean") ? el.value : ui.item.value;
      el.style.background = (in_array(v, m_t13.editors)) ? "rgba(214,227,66, 0.9)" : "rgba(205,32,44,0.4)";
      if(in_array(v, m_t13.editors)) {
         edit_tracker_small.save_tpm(v, el.getAttribute("unid"));
      }
   },

}


