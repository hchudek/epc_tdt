handle_report = {
   apply: function() {
      arguments[1] = (typeof arguments[1] == "undefined") ? "" : arguments[1];
      if(arguments[1] == "") {
         arr = [];
         d = document.getElementsByName("use_field");
         for(i = 0; i < d.length; i++) {
            if(d[i].src.indexOf("status-5-1") == -1) arr.push(d[i].nextSibling.innerText);
         }
         add_field = base64_encode(arr.join(":"));
      }
      else {
         add_field = arguments[1];
      }
      d = document.getElementById('confirm_box');
      if(d) d.parentNode.removeChild(d); 
      infobox("Calculating data", "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"vertical-align:top;\"><span style=\"position:relative;top:-2px;left:7px;\">Processing ...</span>", 232,70);      
      window.setTimeout("handle_report.get_data('" + arguments[0] + "', '" + add_field + "')", 100);
   },

   get_data: function(page_id, add_field) {
      do_fade(true);
      location.href = "addin/reload_report.php?page_id=" + page_id + "&m_e1_dsp=" + add_field;
   },

   do_select: function(nm, t) {
      d = document.getElementsByName(nm);
      for(i = 0; i < d.length; i++) {
         dsp = (t == true) ? "status-5" : "status-5-1";
         d[i].src = "../../../../library/images/16x16/" + dsp + ".png";
      }
   },

   select: function() {
      d = document.getElementById("m_e1_select_fields");
      html = d.innerHTML.replace(/m_e1_use_field/g, "use_field");
      infobox("Select fields", "<div id=\"select_fields\">" + html + "</div>", 1065,640);      
   },

   auto_load: function() {
      d = document.getElementById("m_e1__ee_report_create");
      if(d) { 
         window.setTimeout(d.firstChild.getAttribute("href").replace("javascript:", ""), 1000);
      }
   },

   export: function() {
      if(!document.getElementsByName("ifrm_smartsearch_export")[0]) {
         infobox("Loading data", "<img src=\"../../../../library/images/ajax/ajax-loader.gif\" style=\"vertical-align:top;\"><span style=\"position:relative;top:-2px;left:7px;\">Processing ...</span>", 232,70);      
         url = location.href + "?";
         url = url.substr(0, url.indexOf("?")) + "?" + arguments[0] + "_count=99999&" + arguments[1];
         iframe = document.createElement("iframe");
         iframe.setAttribute("src", url);
         iframe.setAttribute("name", "ifrm_smartsearch_export");
         iframe.setAttribute("style", "display:none;");
         iframe.setAttribute("onload", "handle_report.export('" + arguments[0] + "', '" + arguments[1] + "', '" + arguments[2] + "')");
         document.getElementsByTagName("body")[0].appendChild(iframe);
      }
      else {
         box=document.getElementById("confirm_box");
         if(box) box.parentNode.removeChild(box);
         c = document.createElement("c");
         count = 0;
         num = 1;
         div_head = document.createElement("div");
         div_head.setAttribute("id", "smartsearch.head");
         while(count > -1) {
            count++;
            d = ifrm_smartsearch_export.document.getElementById("v$" + String(count));
            if(d) {
               if(d.getElementsByTagName("div")[0]) {
                  a = document.createElement("a");
                  a.textContent = d.getElementsByTagName("div")[d.getElementsByTagName("div").length - 1].textContent;
                  c.appendChild(a);
                  num++;
               }
            }
            else {
               break;
            }
         } 
         div_head.appendChild(c);

         div_body = document.createElement("div");
         div_body.setAttribute("id", "smartsearch.body");

         count = 0; while(count < 99999) {
            count++;
            if(!ifrm_smartsearch_export.document.getElementById("v$" + String(count) + "$2")) break;
            p = document.createElement("p");
            for(i = 2; i <= num; i++) {  
               d = ifrm_smartsearch_export.document.getElementById("v$" + String(count) + "$" + String(i));
               a = document.createElement("a");
               a.textContent = d.textContent;
               p.appendChild(a);
            }
            div_body.appendChild(p);
         }

         html = "<div id=\"smartsearch_results\">1/1</div>" + div_head.outerHTML + div_body.outerHTML;

         document.getElementById("run_ajax").innerHTML = html;
         do_smartsearch("excel:smartsearch.body:" + arguments[2]);
      }
   }
}

