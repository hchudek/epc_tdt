if(typeof allow_drop != "function") {
   function allow_drop(e) {
      e.preventDefault();
   }
}

if(typeof drag != "function") {
   function drag(e) {
      e.dataTransfer.setData("Text", e.target.id);
   }
}
if(typeof get_date != "function") {
   function get_date(strng, date) {
      date = new Date(date * 1000);
      strng = strng.replace(/Y/g, date.getFullYear());
      strng = strng.replace(/m/g, ("0" + (date.getMonth() + 1)).substr(-2));
      strng = strng.replace(/d/g, ("0" + date.getDate()).substr(-2));
      strng = strng.replace(/W/g, date.getDay());
      return strng;
   }
}


function move_to() {
   s = document.getElementById(arguments[0]);
   adjust = (typeof arguments[3] == "undefined") ? 0 : arguments[3];
   add_text = (typeof arguments[4] == "undefined") ? "" : arguments[4];
   arr = arguments[1].split("-");
   date = new Date(Number(arr[0]), Number(arr[1]) - 1, Number(arr[2]), 0 ,0, 0).getTime() / 1000;
   for(i = 0; i < adjust; i++) {
      date = date + 86400;
      if(Number(get_date('W', date)) == 0 || Number(get_date('W', date)) == 6) i--;
   }  
   id = "calendar_big_" + get_date('Y-m-d', date).replace(/-/g, "_");
   dropable = (typeof arguments[2] == "undefined") ? true : arguments[2];
   drop_calendar_big(false, s, id, dropable, add_text);
}

function drop_calendar_big(e) {
   dropable = (typeof arguments[3] == "undefined") ? true : arguments[3];
   add_text = (typeof arguments[4] == "undefined") ? "" : "&nbsp;[" + arguments[4] + "]";
   if(typeof arguments[1] != "undefined" && typeof arguments[2] != "undefined") {
      s = arguments[1];
      id = arguments[2];
   }
   else {
      e.preventDefault();
      s = document.getElementById(e.dataTransfer.getData("Text").replace("_", ""));
      if(typeof e.target.getAttribute("parentid") == "string") {
         if(typeof s.getAttribute("parentid") == "string") {
            if(e.target.getAttribute("parentid") == s.getAttribute("parentid")) return true;
         }
         id = e.target.getAttribute("parentid");
      }
      else {
         if(s.getAttribute("dragtype") == "calendar_date") {
            if(e.target.id != s.getAttribute("id")) s = document.getElementById(s.getAttribute("id").substr(1, s.getAttribute("id").length));  
         }
         sid = e.target; while(sid) {
            sid = sid.parentNode;
            if(sid.getAttribute("id") != null) break;
         }
         id = sid.getAttribute("id");
      }
   }
   if(id) {
      d = document.getElementById(id);
      clear = document.getElementsByName("_" + (s.getAttribute("id").replace("_", "")));   
      if(clear) {
         for(i = 0; i < clear.length; i++) clear[i].parentNode.removeChild(clear[i]);
      }
      edit_dates = (document.getElementById("edit_dates").getAttribute("init") == "true") ? true : false;
      a = document.createElement("a");
      css = (dropable) ? "calendar_date_entry" : "calendar_date_fixed_plmc";
      if(s.getAttribute("id").indexOf("actual") > -1) css += "_actual";
      a.setAttribute("class", css);
      a.setAttribute("name", "_" + (s.getAttribute("id").replace("_", "")));
      a.setAttribute("id", "_" + (s.getAttribute("id").replace("_", "")));
      a.setAttribute("parentid", id);
      a.setAttribute("onmouseover", "d=document.getElementById('" + s.getAttribute("id").replace("_", "") + "');d.style.color='rgb(189,54,50)';");
      a.setAttribute("onmouseout", "d=document.getElementById('" + s.getAttribute("id").replace("_", "") + "');d.style.color='';");
      if(dropable) {
         a.setAttribute("draggable", "true");
         a.setAttribute("ondragstart", "drag(event)");
         a.setAttribute("dragtype", "calendar_date");
         a.setAttribute("ondrop", "drop_calendar_big(event)");
         a.setAttribute("ondragover", "allow_drop(event)");
         if(!edit_dates) a.setAttribute("style", "color:rgb(0,102,158)");
      }
      a.appendChild(document.createTextNode(s.textContent.substr(0, 5)));
      document.getElementById("entry_" + id).appendChild(a);
      if(dropable) {
         s.nextSibling.style.color = (edit_dates) ? "rgb(0,0,0)" : "rgb(0,102,158)";
         if(!edit_dates) {
            revision = s.nextSibling.innerHTML.substr(s.nextSibling.innerHTML.indexOf("[") + 2, s.nextSibling.innerHTML.length);
            revision = revision.substr(0, revision.indexOf("]"));
            if(typeof s.nextSibling.getAttribute("changed") != "string") {
               revision = (revision != "") ? Number(revision) + 1 : 0;
            }
            add_text = (s.getAttribute("id").indexOf("actual") > -1) ? "&nbsp;[A]" : "&nbsp;[R" + String(revision) + "]";
            s.nextSibling.setAttribute("changed", "true");
         }
         s.nextSibling.innerHTML = d.getAttribute("date") + add_text;
      }
   }   
}


function embed_calendar() {

   arguments[1] = (typeof arguments[1] == "undefined") ? true : arguments[1];

   cf = (!arguments[1]) ? arguments[0] - 10 : arguments[0];
   ct = (!arguments[1]) ? arguments[0] + 10 : arguments[0];   

   for(calendar = cf; calendar <= ct; calendar++) {
      d = document.getElementsByClassName("calendar_big");
      embed = true; 
      if(d) {
         for(i = 0; i < d.length; i++) {
            if(d[i].getAttribute("id").substr(-4) == String(calendar)) {
               dsp = "block";
               embed = false;
            }
            else {
               dsp = "none";
            }
            d[i].setAttribute("style", "display:" + dsp);
         }
      }
      if(embed) embed_calendar_big(calendar);
   }
   if(!arguments[1]) {
      embed_calendar(arguments[0]);
   }
   else {
      embed_calendar_year(arguments[0]);
      add_fixed_dates("dates_list", arguments[0]);
   }
}


function add_fixed_dates(id) {
   if(document.getElementsByName("fixed_date")) {
      for(ii = 0; ii < document.getElementsByName("fixed_date").length; ii++) {
         y = (document.getElementsByName("fixed_date")[ii].getAttribute("value")).substr(0, 4);
         if(y == arguments[1]) {
            move_to(document.getElementsByName("fixed_date")[ii].getAttribute("id"), document.getElementsByName("fixed_date")[ii].getAttribute("value"), false);
         }
      }
   }
}

function embed_page(year) {
   d = document.getElementById("calendar_year_dates");
   d.innerHTML = "<input type=\"text\" onkeyup=\"do_search(this.value.toLowerCase(), document.getElementById('dates_list').getElementsByTagName('a'))\" value=\"\" />";
   
   span = document.createElement("span");
   span.setAttribute("id", "dates_list");
   js = "";
   g = document.getElementsByName("editable_date");
   for(i = 0; i < g.length; i++) {
        tbl = g[i]; while(tbl.tagName.toLowerCase() != "table") {
           tbl = tbl.parentNode; 
        }
      if(g[i].getAttribute("type") == "plmc") {
         status_revision = (g[i].getAttribute("value_revised") !="") ? "&nbsp[" + g[i].getAttribute("revision") + "]" : "";
         status_actual = (g[i].getAttribute("value_actual") !="") ? "[A]" : "";
         span.innerHTML += 
         "<div>" + 
         "<span></span>" + 
         "<a key=\"" + tbl.getAttribute("key") + "revised" + g[i].getAttribute("revision").toLowerCase() + "\" class=\"nodrag_plmc\" name=\"fixed_date\" id=\"fixed_r" + String(i) + "\" value=\"" + g[i].getAttribute("value_revised") + "\">" +
         "<span>" + g[i].getAttribute("descr") + "</span></a>" + 
         "<span class=\"date\">" + g[i].getAttribute("value_revised") + status_revision + "</span>" + 
         "</div>";
         span.innerHTML += 
         "<div>" + 
         "<span></span>" + 
         "<a key=\"" + tbl.getAttribute("key") + "actual\" class=\"nodrag_plmc_actual\" name=\"fixed_date\" id=\"fixed_a" + String(i) + "\" value=\"" + g[i].getAttribute("value_actual") + "\">" +
         "<span>" + g[i].getAttribute("descr") + "</span></a>" + 
         "<span class=\"date\">" + g[i].getAttribute("value_actual") + status_actual + "</span>" + 
         "</div>";
      }
      else { 
        span.innerHTML +=         
	"<div>" + 
        "<span style=\"cursor:pointer;\" onclick=\"move_to(this.nextSibling.id, '2014-03-01', true, +0);\"></span>" + 
        "<a key=\"" + tbl.getAttribute("key") + "dragable revised\" class=\"dragme\" id=\"drag" + String(i) + "\" draggable=\"true\" ondragstart=\"drag(event)\" dragtype=\"date\" value=\"" + g[i].getAttribute("value_revised") + "\">" + g[i].getAttribute("descr") + "</a>" + 
        "<span class=\"date\">" + g[i].getAttribute("value_revised") + "</span>" + 
        "</div>";
        if(g[i].getAttribute("value_revised") != "") {
            js += "move_to('drag" + String(i) + "', '" + g[i].getAttribute("value_revised") + "', true, +0, '" + g[i].getAttribute("revision") + "');";
        }
        span.innerHTML +=         
	"<div>" + 
        "<span></span>" + 
        "<a key=\"" + tbl.getAttribute("key") + "dragable actual\" class=\"dragme_actual\" id=\"dragactual" + String(i) + "\" draggable=\"true\" ondragstart=\"drag(event)\" dragtype=\"date\" value=\"" + g[i].getAttribute("value_actual") + "\">" + g[i].getAttribute("descr") + "</a>" + 
        "<span class=\"date\">" + g[i].getAttribute("value_actual") + "</span>" + 
        "</div>";
        if(g[i].getAttribute("value_actual") != "") {
            js += "move_to('dragactual" + String(i) + "', '" + g[i].getAttribute("value_actual") + "', true, +0, 'A');";
        }
      }
   }   
   d.appendChild(span);
   embed_calendar(year, false);
   if(js != "") {
      eval(js);
   }
   document.getElementById("edit_dates").setAttribute("init", "false");

}

function embed_calendar_year(year) {
   d = document.getElementById("calendar_year");
   while(d.firstChild) d.removeChild(d.firstChild);
   div = new Array;
   div[0] = document.createElement("div");
   div[0].innerHTML = "<a href=\"javascript:embed_calendar(" + (year - 1) + ");\"><img src=\"" + session.php_server + "/library/images/24x24/interface-76.png\" /></a>";
   div[0].setAttribute("class","calendar_year_nav");
   div[1] = document.createElement("div");
   div[1].appendChild(document.createTextNode(year));
   div[1].setAttribute("class","calendar_year_date");
   div[2] = document.createElement("div");
   div[2].innerHTML = "<a href=\"javascript:embed_calendar(" + (year + 1) + ");\"><img src=\"" + session.php_server + "/library/images/24x24/interface-75.png\" /></a>";
   div[2].setAttribute("class","calendar_year_nav");
   span = document.createElement("span");
   for(i = 0; i < div.length; i++) span.appendChild(div[i]);
   d.appendChild(span);
}

function embed_calendar_big(year) {
   date = new Array;
   month = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
   weekday = new Array("Su", "Mo", "Tu", "We", "Th", "Fr", "Sa");

   table = document.createElement("table");
   table.setAttribute("id", "calendar_big_" + String(year));
   table.setAttribute("border", "0");
   table.setAttribute("cellpadding", "0");
   table.setAttribute("cellspacing", "0");
   table.setAttribute("style", "display:block;");
   table.setAttribute("class", "calendar_big");
   table.setAttribute("unselectable", "on");

   for(d = -1; d < 31; d++) { 
      tr = document.createElement("tr");
      for(m = 0; m < 12; m++) {
         if(d == -1) {
            td = document.createElement("td");
            td.setAttribute("class", "month");
            td.appendChild(document.createTextNode(month[m]));
         }
         else { 
            date[m] = new Date(year, m, d + 1, 0 ,0, 0);
            html = (date[m].getMonth() == m) ? get_date("d", date[m].getTime() / 1000) + " " + weekday[date[m].getDay()]: "";
            brght = (m == 11) ? "border-right:solid 1px rgb(66,66,66);" : "";
            bbtm = (d == 30) ? "border-bottom:solid 1px rgb(66,66,66);" : "";
            td = document.createElement("td");
            td.setAttribute("style", "vertical-align:top;border-top:solid 1px rgb(66,66,66);border-left:solid 1px rgb(66,66,66);" + brght + bbtm);
            td.setAttribute("id", "calendar_big_" + get_date('Y_m_d', date[m].getTime() / 1000));
            if(html != "") {
               td.setAttribute("date", get_date('Y-m-d', date[m].getTime() / 1000));
               td.setAttribute("ondrop", "drop_calendar_big(event)");
               td.setAttribute("ondragover", "allow_drop(event)");
               td.setAttribute("class", weekday[date[m].getDay()].toLowerCase());
            }
            td.innerHTML = (html != "") ?"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"calendar_date\"><tr><td style=\"width:39px;vertical-align:top;\">" + html + "</td><td style=\"width:52px;\" id=\"entry_calendar_big_" + get_date('Y_m_d', date[m].getTime() / 1000) + "\"></td></tr></table>" : "&nbsp;";     
         }
         tr.appendChild(td); 
      }
      table.appendChild(tr);
   }
   document.getElementById("calendar_big").appendChild(table);
}


function do_search(strng, d) {
   arr = (strng.trim()).split(" ");
   for(i = 0; i < d.length; i++) {
      found = true;
      key = d[i].getAttribute("key");
      for(e in arr) {
         if(key.indexOf(arr[e]) == -1) { 
            found = false;
            break;
         }
      }
      dsp = (found) ? "block" : "none";
      d[i].parentNode.setAttribute("style", "display:" + dsp);
   }
}