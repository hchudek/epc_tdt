m_d3__workload = {
   settings: {
      type: "planned",
      adjust: "0",
      history: "1",
   },
   submit: function() {
      do_fade(true);
      url = session.remote_database_path + "addin/task_report.php?" + 
      "type=" + m_d3__workload.settings.type +
      "&adjust=" + m_d3__workload.settings.adjust +
      "&history=" + m_d3__workload.settings.history;
      ifrm = document.createElement("iframe");
      ifrm.setAttribute("style", "display:none");
      ifrm.setAttribute("src", url);
      ifrm.setAttribute("onload", "do_fade(false)");
      document.getElementsByTagName("body")[0].appendChild(ifrm);  
   },
}
