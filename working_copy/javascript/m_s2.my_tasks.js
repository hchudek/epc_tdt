m_s2__my_tasks = {
 
   nest: {
      dropbox: function() {
         tbl = document.getElementById("m_s2__my_tasks");
         document.getElementById("m_s2__my_tasks_dropbox").style.height = String(tbl.offsetHeight - 92) + "px";
      }
   },

   drag: function(event) {
      event.dataTransfer.setData("text", event.target.id);
   },

   allow_drop: function(event) {
      event.preventDefault();
   },
 

   add_all: function() {
      m_s2__my_tasks.add_all.do = (typeof arguments[0] == "undefined") ? 0 : arguments[0];
      d = document.getElementById("smartsearch.body").getElementsByTagName("p");
      if(d.length > m_s2__my_tasks.add_all.do) {
         if(d[m_s2__my_tasks.add_all.do].style.display.toLowerCase() != "none") {
            attr = d[m_s2__my_tasks.add_all.do].getAttribute("attr").split("~");
            if(in_array(attr[0], ["20", "30"])) {
               m_s2__my_tasks.drop(d[m_s2__my_tasks.add_all.do].getAttribute("id"));
            }
            else m_s2__my_tasks.add_all(m_s2__my_tasks.add_all.do + 1);
         }
         else m_s2__my_tasks.add_all(m_s2__my_tasks.add_all.do + 1);
      }
      else {
         delete m_s2__my_tasks.add_all.do;
      }
   },

   drop: function() {
      if(typeof arguments[0] == "string") {
         unid = arguments[0];
      }
      else {
         event = arguments[0];
         event.preventDefault();
         unid = event.dataTransfer.getData("text");
      }
      d = document.getElementById(unid);
      d.removeAttribute("draggable");
      d.removeAttribute("ondragstart");
      a = d.getElementsByTagName("a");
      for(k in a) if(!isNaN(k)) {
         a[k].style.textDecoration = "line-through";
         a[k].style.color = "rgb(153,153,153)";
      }
      txt = d.getAttribute("attr").split("~");
      txt_dsp = txt[1] + "/" + txt[2] + "/" + txt[3]
      table = document.createElement("table");
      table.setAttribute("border", "0");
      table.setAttribute("cellpadding", "0");
      table.setAttribute("cellspacing", "0");
      tr = document.createElement("tr");
      td = document.createElement("td");
      td.setAttribute("style", "width:20px; padding:0px; border:none; margin:0px;height:26px;");
      td.innerHTML = "<img id=\"img_" + unid + "\" src=\"../../../../library/images/16x16/red/hardware-40.png\">";
      tr.appendChild(td);
      td = document.createElement("td");
      td.setAttribute("style", "padding:0px; border:0px; white-space:nowrap; overflow-Y:hidden; margin:0px;");
      if(txt[0] == "20") {
         m_s2__my_tasks.save(txt, "Discharge", unid);
      }
      if(txt[0] == "30") {
         if(document.getElementById("rc:" + txt[1])) {
            rc = document.getElementById("rc:" + txt[1]).textContent.split(":");
            select = document.createElement("select");
            select.setAttribute("form", "smartselect");
            select.setAttribute("onchange", "m_s2__my_tasks.save_status30(this, '" + txt.join("~") + "', '" + unid + "')");
            option = document.createElement("option");
            option.textContent = txt_dsp;
            select.appendChild(option);
            for(k in rc) if(!isNaN(k)) {
               option = document.createElement("option");
               option.textContent = rc[k];
               option.setAttribute("value", txt_dsp);
               select.appendChild(option);
            }
            td.appendChild(select);
         }
      }
      else {
         td.textContent = txt_dsp;
      }
      tr.appendChild(td);
      table.appendChild(tr);
      document.getElementById("m_s2__my_tasks_dropbox").appendChild(table);
      if(in_array(txt[0], ["20", "30"]) && typeof m_s2__my_tasks.add_all.do == "number") m_s2__my_tasks.add_all(m_s2__my_tasks.add_all.do + 1);
      if(select) smartselect.populate(select);
   },

   save_status30: function() {
      if(arguments[0].selectedIndex > 0) {
         if(txt_dsp != "") arguments[0].outerHTML = "<span>" + arguments[0][0].text + "</span>";
         m_s2__my_tasks.save(arguments[1].split("~"), arguments[0][arguments[0].selectedIndex].text, arguments[2]);
      }
   },

   save: function() {
      today = "<?php print date("Y-m-d", mktime()); ?>";
      fld = {
         ref_tracker: arguments[0][4],
         ref_pot: arguments[0][5],
         ref_meta: "",
         v: arguments[1],
         run_agent: "a.recalc_process_v2",
         run_agent_unid: arguments[0][6],
         key: arguments[0][1].replace(/[/g, "").replace(/]/g, ""),
         type: "actual",
         fld1: arguments[0][6] + ";date_actual;" + today,
         fld2: arguments[0][6] + ";reason_code_actual;" + arguments[1],
         fld3: arguments[0][6] + ";do_recalc;true",
         date_actual: today,
         unid: arguments[2],
      }
      m_s2__my_tasks.run_agent.joblist.push(fld);
   },

   run_agent: {
      joblist: [],
      add: function() {
         arguments[0] = arguments[0].split("~");
         m_s2__my_tasks.run_agent.joblist.push({unid: arguments[0][0], save: arguments[0][1], img_unid: arguments[1]});
         if(typeof m_s2__my_tasks.add_all.do == "number") m_s2__my_tasks.add_all(m_s2__my_tasks.add_all.do + 1);
      },

      do: function() {
         dropbox = document.getElementById("m_s2__my_tasks_dropbox").getElementsByTagName("table");
         if(m_s2__my_tasks.run_agent.joblist.length == 0 || dropbox.length != m_s2__my_tasks.run_agent.joblist.length) {
            infobox("Error", "Some error occurred", 300, 70);
            return false;
         }

         do_fade(true, true);
         
         if(m_s2__my_tasks.run_agent.joblist.length > 0) {
            document.getElementById("m_s2__my_tasks_calculate").textContent = "Processing ...";

            for(k in m_s2__my_tasks.run_agent.joblist) {
               iframe = document.createElement("iframe");
               iframe.setAttribute("src", "javascript/jload/recalc_process.js?key=" + m_s2__my_tasks.run_agent.joblist[k].fld1.substr(0,32) + "@" + m_s2__my_tasks.run_agent.joblist[k].ref_pot + "@" + m_s2__my_tasks.run_agent.joblist[k].ref_tracker + "@" + m_s2__my_tasks.run_agent.joblist[k].date_actual + "@" + escape(m_s2__my_tasks.run_agent.joblist[k].unid) + "@" + escape(m_s2__my_tasks.run_agent.joblist[k].v));
               iframe.setAttribute("style", "display:1none;");
               iframe.setAttribute("onload", "post_save('" + m_s2__my_tasks.run_agent.joblist[k].unid + "')");
               document.getElementsByTagName("body")[0].appendChild(iframe);
             }
            window.setTimeout("post_done()", 3000);
         }        
      },

      start: function(unid, recalc, img_unid) {
         img = document.getElementById(img_unid);
         img.src = img.src.replace("red", "orange");
         script = document.createElement("script");
         script.setAttribute("src", "addin/run_agent.php?agent=a.recalc_process_v2&unid=" + unid + "&noscript=true&" + "&recalc=" + recalc);
         script.setAttribute("doc:unid", unid);
         script.setAttribute("save:unid", recalc);
         script.setAttribute("img:unid", img_unid);
         script.setAttribute("onload", "m_s2__my_tasks.run_agent.post(this);");
         document.getElementsByTagName("head")[0].appendChild(script);
      },

   },

   filter: function() {
      do_fade(true);
      document.getElementsByName("m_s2__my_tasks_generate_html_status")[0].value = arguments[0];
      save_perpage("", true);
   },

   smartsearch_manipulate: function() {
      d = document.getElementsByTagName("c")[0].getElementsByTagName("a");
      for(key in d) if(!isNaN(key)) {
         if(d[key].textContent.trim() == "S") break;
      }
      use_color = {
         10: "rgb(204,204,204)",
         20: "rgb(0,142,210)",
         30: "rgb(199,109,114)",
         40: "rgb(0,204,0)",
      }

      if(isNaN(key)) {
         document.getElementById("smartsearch.head").style.display = "none";
         document.getElementById("smartsearch.body").innerHTML = "<div style=\"margin-top:16px; font-weight:bold;\">Nothing found</div>";
      }
      else {
         d = document.getElementById("smartsearch.body").getElementsByTagName("p");
         for(k in d) if(!isNaN(k)) {
           d[k].children[key].style.color = use_color[Number(d[k].children[key].textContent)];
           d[k].children[key].innerHTML = "&nbsp;&nbsp;&nbsp;&#x25cf;&nbsp;&nbsp;&nbsp;"; 
         }
      }
   },

   open: function() {
      a = document.createElement("a");
      a.setAttribute("href", "tracker.php?" + arguments[0]);
      a.setAttribute("target", "_blank");
      a.click();
   },

}

post_save = function(unid) {
   e = document.getElementById("img_" + unid);
   e.src = e.src.replace(/red/, "orange");
}

post_done = function() {
   d = document.getElementsByTagName("img");
   p = 0;
   for(k in d) if(!isNaN(k)) {    
      if(d[k].hasAttribute("id")) {
         if(d[k].getAttribute("id").substr(0, 3) == "img" && d[k].getAttribute("src").indexOf("orange") > 0) {
            d[k].src = d[k].src.replace(/orange/, "green");
            p++;
         }
      }
   }
   document.getElementById("m_s2__my_tasks_calculate").innerHTML = "<span>Processed " + p + " tracking cases</span>";
}

