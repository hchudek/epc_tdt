m_x5 = {
   drag: function() {
      rms = document.getElementById("smartsearch.body").getElementsByTagName("p");
      for(k in rms) if(typeof rms[k] == "object") {
         rms[k].removeAttribute("onclick");
         rms[k].setAttribute("tp", "0");
         rms[k].draggable = true;
         rms[k].ondragstart = function(ev) {
            if(this.getAttribute("tp") == "0") {
               ev.dataTransfer.setData("text", ev.target.id);
            }
         }
      } 
   },
   mark: function(rms, tp) {
      rms.style.color = (!tp) ? "rgb(0,0,0)" : "rgb(0,102,204)";
      rms.setAttribute("tp", Number(tp));
   },
   download: function() {
      d = document.getElementsByName("dig_report")[0];
      y = d[d.selectedIndex].text;
      infobox("Processing report", "<img src=\"../../../../library/images/ajax/ajax-loader2.gif\"><span style=\"position:relative; left:9px; top:-11px;\">Processing ...</span>", 300, 100);
      js = "if(String(document.getElementById('run_ajax').textContent.toLowerCase()).trim() == 'no documents found')window.setTimeout('document.getElementById(\\'confirm_box\\').getElementsByClassName(\\'body\\')[0].innerHTML = \\'No documents found\\'', 999); else window.setTimeout('infobox_handle.close(true, \\'confirm_box\\')', 499);";
      run_ajax("run_ajax", "addin/dig_report.php?get=" + y, js);
   },

   check_selected: function() {
      view = document.getElementsByName("m_x5__dig_rep_resp_view");
      if(typeof view == "object") {
         if(view[0].value == "1") m_x5.drag();
         else {
            drop = document.getElementById("m_x6__dig_rep_resp_drop");
            if(typeof drop == "object") drop.style.display = "none"; 
            anchor = document.getElementById("smartsearch").querySelectorAll("a[anchor]");
            for(k in anchor) {
               anchor[k].onmouseover = function() {
                  this.setAttribute("restore", this.textContent);
                  this.textContent = "Click to edit";
               }
               anchor[k].onmouseout = function() {
                  this.textContent = this.getAttribute("restore");
               }
               anchor[k].onclick = function() {
                  if(typeof pick_date != "object") pick_date = {};
                  pick_date.execute = "save";
                  pick_date.save = function(unid, anchor, date) {
                     now = "<?php print date("Y-m-d H:i:s", time()); ?>";
                     data = "&unid=" + unid + "&fld1=" + escape("dig_rep_date_actual/" + session.domino_user_cn + "@" + date + anchor.substr(anchor.indexOf(" "), anchor.length));
                     js = "location.reload()";
                     post.save_data("f.save_data", session.remote_domino_path_epcmain, data, js);  
                  }
                  calendar_big_pick_date(this.parentNode.getAttribute("do"), this.getAttribute("anchor"), "Change date");
               } 
            }
         }
      }
   }
}
