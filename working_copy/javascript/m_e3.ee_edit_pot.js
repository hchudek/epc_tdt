<?php
require_once("../../../../library/tools/addin_xml.php");
$path_translated =  str_replace("\\", "/", $_SERVER["PATH_TRANSLATED"]);
print "include_m_e3__ee_edit_pot = function() {_(arguments, \"".base64_encode(substr($path_translated, 0, strrpos($path_translated, "/"))."/jload/m_e3.ee_edit_pot/")."\");}\r\n";
?>

m_e3__ee_edit_pot = {

   embed: function() {
      include_css(session.php_server + "/library/css5/te.tools/jquery-ui-ssl.css");
      unid = arguments[0].substr(0, 32);
      dis = arguments[0].substr(33, arguments[0].length);
      head = document.getElementsByTagName("body")[0];
      infobox("", "<div id=\"m_e3__ee_edit_pot__table\">Loading ...</div>", 1180,700, "edit_box");

      id = document.getElementById("m_e3__ee_edit_pot.data");
      if(id) id.parentNode.removeChild(id);
      script = document.createElement("script");
      script.setAttribute("src", session.remote_database_path + "ee_edit_pot.php?load=" + unid);
      script.setAttribute("type", "text/javascript");
      script.setAttribute("id", id); 
      script.setAttribute("onload", "m_e3__ee_edit_pot.nest('" + unid + "', '" + dis + "')");
      console.log(script.src);
      head.appendChild(script);
 
   },

   nest: function(unid, dis) {
      table = document.createElement("table");
      table.setAttribute("id", "tbl_m_e3__ee_edit_pot");
      table.setAttribute("border", "0");
      table.setAttribute("cellpadding", "0");
      table.setAttribute("cellspacing", "0");
      tr = document.createElement("tr");
      for(thead in m_e3__ee_edit_pot.data.head) if(!isNaN(thead) && thead > 1) {
         th = document.createElement("th");
         th.textContent = unescape(m_e3__ee_edit_pot.data.head[thead]);
         tr.appendChild(th);
      }
      table.appendChild(tr);

      headline = "Edit ";

      for(row in m_e3__ee_edit_pot.data) if(!isNaN(row)) {
         key = [];
         tr = document.createElement("tr");
         if(row == 0) {
            headline += 
            "<a target=\"_blank\" href=\"" + unescape(m_e3__ee_edit_pot.data[0][0].link) + "\" style=\"color:rgb(255,255,255); text-decoration:none;\">" + unescape(m_e3__ee_edit_pot.data[row][0].dsp) + "</a>" +
            "<img src=\"../../../../library/images/16x16/white/chevron-medium-big-2-01.png\" style=\"position:absolute; margin-left:2px; margin-top:1px;\">" + 
            "<a target=\"_blank\" href=\"" + m_e3__ee_edit_pot.data[0][1].link + "\" style=\"color:rgb(255,255,255); text-decoration:none; margin-left:20px;\">" + unescape(m_e3__ee_edit_pot.data[0][1].dsp) + "</a>";
         }
         for(column in m_e3__ee_edit_pot.data[row]) if(!isNaN(column)) {
            if(column > 1) {
               key.push(unescape(m_e3__ee_edit_pot.data[row][column].dsp));
               td = document.createElement("td");
               if(typeof m_e3__ee_edit_pot.data[row][column].link == "undefined") {
                  if(typeof m_e3__ee_edit_pot.data[row][column].unid == "undefined" || m_e3__ee_edit_pot.data[row][column].unid == "") {
                     td.textContent = unescape(m_e3__ee_edit_pot.data[row][column].dsp);
                  }
                  else {
                     switch(unescape(m_e3__ee_edit_pot.data.head[column]).toLowerCase()) {
                        case "drawing revision":
                           h = document.createElement("input");
                           h.setAttribute("value", unescape(m_e3__ee_edit_pot.data[row][column].dsp));
                           h.setAttribute("onkeyup", "m_e3__ee_edit_pot.mark(this)");
                           h.setAttribute("unid", m_e3__ee_edit_pot.data[row][column].unid);
                           h.setAttribute("name", "num_drwrevision");
                           break;
                        case "part supplier":
                           h = document.createElement("input");
                           h.setAttribute("value", unescape(m_e3__ee_edit_pot.data[row][column].dsp));
                           h.setAttribute("onkeyup", "m_e3__ee_edit_pot.mark(this)");
                           h.setAttribute("unid", m_e3__ee_edit_pot.data[row][column].unid);
                           h.setAttribute("name", "supplier_part");
                           break;
                        case "tool po num":
                           h = document.createElement("input");
                           h.setAttribute("value", unescape(m_e3__ee_edit_pot.data[row][column].dsp));
                           h.setAttribute("onkeyup", "m_e3__ee_edit_pot.mark(this)");
                           h.setAttribute("unid", m_e3__ee_edit_pot.data[row][column].unid);
                           h.setAttribute("name", "pono");
                           break;
                        case "position":
                           h = document.createElement("input");
                           h.setAttribute("value", unescape(m_e3__ee_edit_pot.data[row][column].dsp));
                           h.setAttribute("onkeyup", "m_e3__ee_edit_pot.mark(this)");
                           h.setAttribute("unid", m_e3__ee_edit_pot.data[row][column].unid);
                           h.setAttribute("name", "position");
                           break;
                        case "confirmed evt":
                           h = document.createElement("input");
                           h.setAttribute("value", unescape(m_e3__ee_edit_pot.data[row][column].dsp));
                           h.setAttribute("readonly", "readonly");
                           h.setAttribute("id", "evt_" + m_e3__ee_edit_pot.data[row][column].unid);
                           h.setAttribute("onclick", "m_e3__ee_edit_pot.calendar('', 'evt_" + m_e3__ee_edit_pot.data[row][column].unid + "', 'Confirmed%20EVT%20date')");
                           h.setAttribute("unid", m_e3__ee_edit_pot.data[row][column].unid);
                           h.setAttribute("name", "datesuppevt");
                           break;
                        case "tool supplier":
                           h = document.createElement("input");
                           h.setAttribute("value", unescape(m_e3__ee_edit_pot.data[row][column].dsp));
                           h.setAttribute("onkeyup", "m_e3__ee_edit_pot.mark(this)");
                           h.setAttribute("unid", m_e3__ee_edit_pot.data[row][column].unid);
                           h.setAttribute("name", "supplier_tool");
                           break;
                     }
                     td.appendChild(h);
                  }
               }
               else {
                  a = document.createElement("a");
                  a.setAttribute("href", m_e3__ee_edit_pot.data[row][column].link);
                  a.setAttribute("target", "_blank");
                  a.textContent = unescape(m_e3__ee_edit_pot.data[row][column].dsp);
                  td.appendChild(a);
               }
               tr.appendChild(td);
            }
            tr.setAttribute("key", key.join(" ").toLowerCase());
            table.appendChild(tr);
         }
      }
      h = document.getElementById("edit_box"); while(h.tagName.toLowerCase() != "td") h = h.firstChild;
      h.innerHTML = headline;

      button =
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td style=\"padding-right:4px;\">" + 
      "<input value=\"\" onkeyup=\"m_e3__ee_edit_pot.search(this);\" style=\"border:solid 1px rgb(153,153,153; font:normal 13px Open Sans;\"></td>" +
      "<td><span class=\"phpbutton\"><a href=\"javascript:infobox_handle.close(true, 'edit_box');\">Cancel</a></span></td>" + 
      "<td><span class=\"phpbutton\" id=\"m_e3__ee_edit_pot_save\" style=\"display:none;\"><a href=\"javascript:m_e3__ee_edit_pot.save();\">Save changes</a></span></td>" +
      "</tr></table><br>";
      document.getElementById("m_e3__ee_edit_pot__table").innerHTML = button + table.outerHTML;
      m_e3__ee_edit_pot.nest_autocomplete();
   },

   search: function() {
      v = (arguments[0].value.trim().toLowerCase()).split(" ");
      d = document.getElementById("tbl_m_e3__ee_edit_pot").getElementsByTagName("tr");

      for(k in d) if(!isNaN(k) && k > 0) {
         dsp = true;
         for(i = 0; i < v.length; i++) {
            key = d[k].getAttribute("key");
            if(key.indexOf(v[i]) == -1) dsp = false;
         }
         d[k].style.display = (dsp) ? "table-row" : "none";
      }
   },

   calendar: function() {
      with(document.getElementById("edit_box")) {
         m_e3__ee_edit_pot.html = style.display;
         style.display = "none";
      }
      calendar_big_pick_date(arguments[0], arguments[1], arguments[2]);
   },

   save_calendar_date: function() {
      d = document.getElementById(arguments[1]);
      d.value = arguments[2];
      m_e3__ee_edit_pot.mark(d);   
      document.getElementById("confirm_box_cal").parentNode.removeChild(document.getElementById("confirm_box_cal"));
      document.getElementById("edit_box").style.display = "";   
   },

   supplier: [
<?php 
$file = explode("/", file_get_authentificated_contents($_SESSION["remote_domino_path_ssl"]."/v.json.tdt.supplier_m_e3?open&count=99999&function=plain"));
foreach($file as $val) {
   if(trim($val) != "") $supplier[] = "      ".str_replace("\"", " ", rawurldecode(utf8_encode($val)));
}
print implode($supplier, ",\r\n");
?>],

   nest_autocomplete: function() {  
      supplier = [];
      for(k in m_e3__ee_edit_pot.supplier) {
         supplier.push({label: m_e3__ee_edit_pot.supplier[k].label, value: m_e3__ee_edit_pot.supplier[k].label + " [" + m_e3__ee_edit_pot.supplier[k].value + "]"});
      }

      $("[name=supplier_part]").autocomplete({   
         source: supplier,   
      }); 
      $("[name=supplier_tool]").autocomplete({   
         source: supplier,   
      });  

      arr = ["supplier_part", "supplier_tool"];
      for(a in arr) {
         st = document.getElementsByName(arr[a]);
         for(e in st) if(!isNaN(e)) {
            for(k in m_e3__ee_edit_pot.supplier) {
               if(m_e3__ee_edit_pot.supplier[k].value == st[e].value) {
                  st[e].value = m_e3__ee_edit_pot.supplier[k].label + " [" + st[e].value + "]";
               }
            }
         }
      }
   },

   mark: function() {
      with(arguments[0]) {
         style.backgroundColor = "rgb(243,243,247)";
         setAttribute("save", "true");
      }
      tr = arguments[0]; while(tr.tagName.toLowerCase() != "tr") tr = tr.parentNode;
      td = tr.getElementsByTagName("td");
      key = [];
      for(k in td) if(!isNaN(k)) {

         input = td[k].getElementsByTagName("input");
         if(input.length > 0) key.push(input[0].value);
         else key.push(td.textContent);
      }
      tr.setAttribute("key", key.join(" ").toLowerCase());
      document.getElementById("m_e3__ee_edit_pot_save").style.display = "block";
   },

   save: function() {       
      if(typeof arguments[0] == "undefined") {
         d = document.getElementById("tbl_m_e3__ee_edit_pot").getElementsByTagName("input");
         arr = [];
         for(k in d) if(!isNaN(k)) {
            n = (typeof d[k].getAttribute("name") == "string") ? d[k].getAttribute("name").toLowerCase() : "";
            if(!in_array(n, arr) && n != "") arr.push(n);
         }
         m_e3__ee_edit_pot.data = [];
         for(k in arr) if(!isNaN(k)) {
            input = document.getElementsByName(arr[k]);
            for(s in input) if(!isNaN(s)) {
               if(typeof input[s].getAttribute("save") == "string") {
                  db = session.remote_domino_path_epcmain;
                  fld = escape(input[s].getAttribute("name"));
                  if(fld == "supplier_tool" || fld == "supplier_part") fld = "supplier";
                  data = "&unid=" + input[s].getAttribute("unid") + "&fld1=" + fld + "/" + escape(input[s].value);
                  m_e3__ee_edit_pot.data.push({data: data, db: db}); 
               }
            }
         }
         document.getElementById("m_e3__ee_edit_pot__table").innerHTML = 
         "<div id=\"tbl_m_e3__ee_edit_pot\"><img src=\"../../../../library/images/ajax/ajax-loader.gif\"><span style=\"position:relative; top:-2px; left:5px;\" id=\"progression_save\"></span></div>";
         m_e3__ee_edit_pot.save(0);
      }
      else {
         if(m_e3__ee_edit_pot.data.length > arguments[0]) {
            document.getElementById("progression_save").textContent = "Processing [" + String(1 + arguments[0]) + " / " + String(m_e3__ee_edit_pot.data.length) + "] ...";
            form = "f.save_data";
            db = m_e3__ee_edit_pot.data[arguments[0]].db;
            data = m_e3__ee_edit_pot.data[arguments[0]].data;
            js = "m_e3__ee_edit_pot.save(" + (arguments[0] + 1) + ")";
            post.save_data(form, db, data, js);
         }
         else {
            $("#m_e3__ee_edit_pot__table").fadeOut(200);
            window.setTimeout("infobox_handle.close(true, 'edit_box')", 1);
         }
      }
   }
} 
