function handle_userpage(action) {
   if(action=="style") {
      c=3;
      embed_configuration(arguments[1]);
   }
   if(action=="isactive") c=1;
   for(i=0;i<=c;i++) {
      if(action=="style") type=((i+1)==arguments[1]) ? "select_high" : "select_reg";
      if(action=="isactive") type=((i==0 && arguments[1]=="active") || (i==1 && arguments[1]=="inactive")) ? "select_high" : "select_reg";
      document.getElementsByName("userpage_t_"+action)[i].src="../../../../library/images/icons/"+type+".png";
   }
   document.getElementById("userpage_"+action).innerText=arguments[1];
}

function save_userpage() {
   val=document.getElementsByName("userpage_name")[0].value+":"+document.getElementById("userpage_isactive").innerText+":"+document.getElementById("userpage_style").innerText
   document.getElementById("userpage").value=val;
   if(typeof arguments[0]=="string") document.getElementsByName('_DominoForm')[0].action=document.getElementsByName('_DominoForm')[0].action+escape("&configure=true");
   t_val="";
   for(i=1;i<=3;i++) {
      has_id=false; for(e=0;e<=9;e++) {
         id=document.getElementsByName("configurator_"+String(i))[e].value;
         id=id.substr(id.indexOf("["),id.length).replace("[","").replace("]","");
         if(id!="") {
            t_val+=document.getElementById("module_id_"+id).innerText+",";
            has_id=true;
         }
      }
      t_val=(has_id==true) ? t_val.substr(0,t_val.length-1)+":" : t_val+"<empty>:";
   }

   document.getElementById("userpage_blocks").value=t_val;
   run_ajax("run_ajax",arguments[0]+"/a.deletefield?open&unid="+arguments[1]+"&field=td%24"+arguments[2]+"_link","document._DominoForm.submit()");
}

function embed_configuration(template) {
   switch (template) {
      case "1":
         embed_block=new Array("1","2","3");
         break;
      case "2":
         embed_block=new Array("1","2");
         break;
      case "3":
         embed_block=new Array("2","3");
         break;
      case "4":
         embed_block=new Array("2");
         break;
   }
   for(i=1;i<=3;i++) {
      document.getElementById("configurator_block_"+String(i)).firstChild.innerText="false";
      document.getElementById("configurator_block_"+String(i)).style.backgroundColor="#eeeeee";
      document.getElementById("move_modules_"+String(i)).style.visibility="hidden";
      for(b=0;b<document.getElementsByName("configurator_"+String(i)).length;b++) {
         document.getElementsByName("configurator_"+String(i))[b].className="out";
         document.getElementsByName("configurator_"+String(i))[b].value="";
      }
   }

   for(i=0;i<embed_block.length;i++) {
      document.getElementById("configurator_block_"+String(embed_block[i])).firstChild.innerText="true";
      document.getElementById("configurator_block_"+String(embed_block[i])).style.backgroundColor="#ffffff";
      document.getElementById("move_modules_"+String(embed_block[i])).style.visibility="visible";
      for(b=0;b<document.getElementsByName("configurator_"+embed_block[i]).length;b++) document.getElementsByName("configurator_"+embed_block[i])[b].className="reg";
   }
   document.getElementById("select_module").innerText="";
   document.getElementById("select_module_number").innerText="";
   i=1; while(document.getElementById("module_"+String(i))) {
      document.getElementById("module_"+String(i)).className="reg";
      i++;
   }
}

function select_module(m_num) {
   line=(typeof arguments[1]=="undefined") ? 1 : arguments[1];
   css=(line==m_num) ? "high" : "reg";
   if(document.getElementById("module_"+String(line))) {
      if(document.getElementById("module_"+String(line)).className!="out") document.getElementById("module_"+String(line)).className=css;
      select_module(m_num, (line+1));
   }
   else {
      document.getElementById("select_module").innerText=document.getElementById("module_t_name_"+String(m_num)).innerText;
      document.getElementById("select_module_number").innerText=String(m_num);
   }
}

function select_active_module(block,t_line) {
   line=(typeof arguments[2]=="undefined") ? 1 : arguments[2];
   if(document.getElementsByName("configurator_"+String(block))[line-1]) {
      css=(t_line==line) ? "high" : "reg";
      if(document.getElementsByName("configurator_"+String(block))[t_line-1].value!="") {
         document.getElementsByName("configurator_"+String(block))[line-1].className=css;
      }
      select_active_module(block,t_line,(line+1));
   }
}

function add_to_module(m_num) {
   if(document.getElementById("configurator_block_"+String(m_num)).firstChild.innerText=="true") {
      if(document.getElementById("select_module").innerText!="" && document.getElementById("module_"+document.getElementById("select_module_number").innerText).className!="out") {
         exit=false; i=0; while(i<10 && exit==false) {
            if(document.getElementsByName("configurator_"+String(m_num))[i].value=="") exit=true;
            i++;
         }
         module=document.getElementById("select_module").innerText;
         module_number=document.getElementById("select_module_number").innerText;
         document.getElementsByName("configurator_"+String(m_num))[i-1].value=document.getElementById("module_name_"+module_number).innerText+" ["+module+"]";
         document.getElementById("select_module").innerText="";
         document.getElementById("module_"+document.getElementById("select_module_number").innerText).className="out";
      }
   }
}

function delete_from_module(block) {
   exit=false; i=0; while(i<10 && exit==false) {
      if(document.getElementsByName("configurator_"+String(block))[i].className=="high") exit=true;
      i++;
   }
   if(exit==true) {
      id=document.getElementsByName("configurator_"+String(block))[i-1].value;
      id="module_name_"+(id.substr(id.indexOf("["),id.length).replace("[","").replace("]",""));
      document.getElementById(id).parentNode.className="reg";
      for(e=i;e<10;e++) document.getElementsByName("configurator_"+String(block))[e-1].value=document.getElementsByName("configurator_"+String(block))[e].value;
      document.getElementsByName("configurator_"+String(block))[i-1].className="reg";
   }  
}

